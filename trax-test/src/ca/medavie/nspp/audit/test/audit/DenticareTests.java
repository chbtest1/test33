package ca.medavie.nspp.audit.test.audit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.repository.DenticareRepository;
import ca.medavie.nspp.audit.service.repository.DenticareRepositoryImpl;
import ca.medavie.nspp.audit.test.BaseTest;

/**
 * Places the DenticareRepository under test
 */
public class DenticareTests extends BaseTest {
	private static DenticareRepository denticareRepository;

	@BeforeClass
	public static void setUpClass() {
		denticareRepository = new DenticareRepositoryImpl(em);
	}

	/**
	 * Search Audit Letter Responses
	 * 
	 * Verify the correct ALRs return
	 */
	@Test
	public void getSearchedDenticareALRTest() {
		DenticareAuditLetterResponseSearchCriteria sc = new DenticareAuditLetterResponseSearchCriteria();
		sc.setPractitionerNumber(Long.valueOf(105267));
		sc.setPractitionerType("DE");

		List<DenticareAuditLetterResponse> results = denticareRepository.getSearchedDenticareALR(sc, 0, 20);
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Verify results
		for (DenticareAuditLetterResponse ar : results) {
			assertTrue(Long.valueOf(ar.getPractitionerNumber()).equals(sc.getPractitionerNumber()));
			assertTrue(ar.getClaimNumber().equals(Long.valueOf(401403)));
			assertTrue(ar.getPractitionerType().equals(sc.getPractitionerType()));
			assertTrue(ar.getHealthCardNumber().trim().equals("9900005514"));
		}
	}

	/**
	 * Save a new Audit Letter Response
	 * 
	 * Verify save by loading the new record
	 */
	@Test
	public void saveDenticareALRTest() {
		DenticareAuditLetterResponse denticareAudit = new DenticareAuditLetterResponse();
		denticareAudit.setClaimNumber(Long.valueOf(401402));
		denticareAudit.setItemCode(Long.valueOf(730280));
		denticareAudit.setAuditRunNumber(Long.valueOf(995));
		denticareAudit.setPractitionerNumber("104154");
		denticareAudit.setPractitionerType("DE");
		denticareAudit.setHealthCardNumber("9900005613");

		Calendar cal = Calendar.getInstance();
		denticareAudit.setClaimSelectionDate(cal.getTime());
		denticareAudit.setServiceDescription("JUNIT test");
		denticareAudit.setResponseStatus("APROV");
		denticareAudit.setLastModified(cal.getTime());
		denticareAudit.setModifiedBy("JUNIT");

		// Save the new Audit
		em.getTransaction().begin();
		denticareRepository.saveDenticareALR(denticareAudit);
		em.getTransaction().commit();

		// Verify save by loading the new record
		DenticareAuditLetterResponseSearchCriteria sc = new DenticareAuditLetterResponseSearchCriteria();
		sc.setPractitionerNumber(Long.valueOf(104154));
		sc.setPractitionerType("DE");

		List<DenticareAuditLetterResponse> results = denticareRepository.getSearchedDenticareALR(sc, 0, 20);
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Should only be one record
		assertTrue(results.size() == 1);
		// Verify record contents (PK values)
		DenticareAuditLetterResponse r = results.get(0);
		assertNotNull(r);
		assertTrue(r.getAuditRunNumber().equals(Long.valueOf(995)));
		assertTrue(r.getClaimNumber().equals(Long.valueOf(401402)));
		assertTrue(r.getItemCode().equals(Long.valueOf(730280)));
		assertTrue(r.getModifiedBy().trim().equals("JUNIT"));
	}

	/**
	 * Search for Audit Criterias
	 * 
	 * Verify we get the correct results
	 */
	@Test
	public void getSearchPractitionerAuditCriteriasTest() {
		AuditCriteriaSearchCriteria sc = new AuditCriteriaSearchCriteria();
		sc.setAuditCriteriaId(Long.valueOf(70));
		sc.setAuditType("DESPE");
		sc.setPractitionerId(Long.valueOf(104400));

		List<AuditCriteria> results = denticareRepository.getSearchPractitionerAuditCriterias(sc, 0, 20);
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Should only get 1 result
		assertTrue(results.size() == 1);
		// Verify results
		for (AuditCriteria ac : results) {
			assertTrue(ac.getAuditType().equals(sc.getAuditType()));
			assertTrue(ac.getAuditCriteriaId().equals(sc.getAuditCriteriaId()));
			assertTrue(ac.getProviderNumber().equals(sc.getPractitionerId()));
			assertTrue(ac.getProviderType().equals("DE"));
		}
	}

	/**
	 * Load an Audit Criteria by ID
	 * 
	 * Verify we get the correct Audit Criteria
	 */
	@Test
	public void getPractitionerAuditCriteriaByIdTest() {
		AuditCriteria ac = denticareRepository.getPractitionerAuditCriteriaById(Long.valueOf(92));
		// Verify we get an audit
		assertNotNull(ac);
		// Verify the audit returned
		assertTrue(ac.getAuditCriteriaId().equals(Long.valueOf(92)));
		assertTrue(ac.getAuditType().equals("DENOR"));
		assertTrue(ac.getProviderNumber().equals(Long.valueOf(107162)));
		assertTrue(ac.getProviderType().equals("DE"));
	}

	/**
	 * Load a default AuditCriteria by type
	 * 
	 * Verify the correct Audit loads
	 */
	@Test
	public void getDefaultAuditCriteriaByTypeTest() {
		AuditCriteria ac = denticareRepository.getDefaultAuditCriteriaByType("DESPE");
		// Verify we get a result
		assertNotNull(ac);
		// Verify audit
		assertTrue(ac.getAuditType().equals("DESPE"));
		assertTrue(ac.getAuditCriteriaId().equals(Long.valueOf(69)));
	}

	/**
	 * Save a new AuditCriteria
	 * 
	 * Verify save by loading the new record
	 */
	@Test
	public void saveDenticareAuditCriteriaTest() {
		AuditCriteria ac = new AuditCriteria();
		ac.setAuditType("DESPE");
		ac.setProviderNumber(Long.valueOf(999999));
		ac.setProviderType("PH");
		ac.setNumberOfServicesToAudit(Long.valueOf(85));
		ac.setOneTimeAuditIndicator("N");
		ac.setAuditFromLastIndicator(false);
		ac.setModifiedBy("JUNIT");
		ac.setLastModified(Calendar.getInstance().getTime());

		em.getTransaction().begin();
		denticareRepository.saveDenticareAuditCriteria(ac);
		em.getTransaction().commit();

		// Verify record saved by search for it
		AuditCriteria acReCheck = denticareRepository.getPractitionerAuditCriteriaById(ac.getAuditCriteriaId());
		assertNotNull(acReCheck);
		assertTrue(acReCheck.getAuditCriteriaId().equals(ac.getAuditCriteriaId()));
	}
}
