package ca.medavie.nspp.audit.test.audit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.DssMsiConstant;
import ca.medavie.nspp.audit.service.data.OutlierCriteria;
import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;
import ca.medavie.nspp.audit.service.repository.OutlierCriteriaRepository;
import ca.medavie.nspp.audit.service.repository.OutlierCriteriaRepositoryImpl;
import ca.medavie.nspp.audit.test.BaseTest;

/**
 * Places the OutlierCriteriaRepository under test
 */
public class OutlierCriteriaTests extends BaseTest {
	private static OutlierCriteriaRepository outlierCriteriaRepository;

	@BeforeClass
	public static void setUpClass() {
		outlierCriteriaRepository = new OutlierCriteriaRepositoryImpl(em);
	}

	/**
	 * Load current Outlier Criteria
	 * 
	 * Verify we get data back
	 */
	@Test
	public void getOutlierCriteriasTest() {
		List<OutlierCriteria> criterias = outlierCriteriaRepository.getOutlierCriterias();
		// Verify we get data
		assertNotNull(criterias);
		assertFalse(criterias.isEmpty());
	}

	/**
	 * Load all PractitionerPeriods
	 * 
	 * Verify we get data returned
	 */
	@Test
	public void getPractitionerPeriodsTest() {
		List<PractitionerProfilePeriod> periods = outlierCriteriaRepository.getPractitionerPeriods();
		// Verify we get results
		assertNotNull(periods);
		assertFalse(periods.isEmpty());
	}

	/**
	 * Load the current ProfileCriteria
	 * 
	 * Verify we get data returned
	 */
	@Test
	public void getPractitionerProfileCriteriaTest() {
		List<DssMsiConstant> criteria = outlierCriteriaRepository.getPractitionerProfileCriteria();
		// Verify results
		assertNotNull(criteria);
		for (DssMsiConstant constant : criteria) {
			if (constant.getConstantCode().equals("MPERIOD")) {
				assertFalse(constant.getConstantNumeric() == 0);
			}
		}
	}

	/**
	 * Save a new Profile
	 * 
	 * Verify save by loading new record
	 */
	@Test
	public void savePractitionerProfileCriteriaTest() {
		// Load the current PractitionerProfileCriteria record
		List<DssMsiConstant> ppc = outlierCriteriaRepository.getPractitionerProfileCriteria();
		// Verify we get results
		assertNotNull(ppc);

		// Modify the ProfileCriteria and save
		Calendar cal = Calendar.getInstance();
		for (DssMsiConstant dmc : ppc) {
			if (dmc.getConstantCode().equals("CONTINUE")) {
				dmc.setConstantChar("Y");
				dmc.setLastModified(cal.getTime());
				dmc.setModifiedBy("JUNIT");
			} else if (dmc.getConstantCode().equals("PROV_CNT")) {
				dmc.setConstantNumeric(new Integer(777));
				dmc.setLastModified(cal.getTime());
				dmc.setModifiedBy("JUNIT");
			} else if (dmc.getConstantCode().equals("PAYMT_DT")) {
				dmc.setConstantDate(cal.getTime());
				dmc.setLastModified(cal.getTime());
				dmc.setModifiedBy("JUNIT");
			} else if (dmc.getConstantCode().equals("UNITDOLR")) {
				dmc.setConstantCurrency(new BigDecimal(99.99));
				dmc.setLastModified(cal.getTime());
				dmc.setModifiedBy("JUNIT");
			}
		}

		em.getTransaction().begin();
		for (DssMsiConstant dmc : ppc) {
			outlierCriteriaRepository.savePractitionerProfileCriteria(dmc);
		}
		em.getTransaction().commit();

		// Verify changes by loading ProfileCriteria again and check values
		ppc = outlierCriteriaRepository.getPractitionerProfileCriteria();

		for (DssMsiConstant dmc : ppc) {
			if (dmc.getConstantCode().equals("CONTINUE")) {
				assertTrue(dmc.getModifiedBy().equals("JUNIT"));
				assertTrue(dmc.getLastModified().equals(cal.getTime()));
				assertTrue(dmc.getConstantChar().equals("Y"));
			} else if (dmc.getConstantCode().equals("PROV_CNT")) {
				assertTrue(dmc.getModifiedBy().equals("JUNIT"));
				assertTrue(dmc.getLastModified().equals(cal.getTime()));
				assertTrue(dmc.getConstantNumeric().equals(new Integer(777)));
			} else if (dmc.getConstantCode().equals("PAYMT_DT")) {
				assertTrue(dmc.getModifiedBy().equals("JUNIT"));
				assertTrue(dmc.getLastModified().equals(cal.getTime()));
				assertTrue(dmc.getConstantDate().equals(cal.getTime()));
			} else if (dmc.getConstantCode().equals("UNITDOLR")) {
				assertTrue(dmc.getModifiedBy().equals("JUNIT"));
				assertTrue(dmc.getLastModified().equals(cal.getTime()));
				assertTrue(dmc.getConstantCurrency().equals(new BigDecimal(99.99)));
			}
		}
	}

	/**
	 * Save a new Criteria
	 * 
	 * Verify save by loading new record
	 */
	@Test
	public void saveOutlierCriteriaTest() {
		// Load all currently OutlierCriteria records
		List<OutlierCriteria> criterias = outlierCriteriaRepository.getOutlierCriterias();
		// Verify we get record back
		assertNotNull(criterias);
		assertFalse(criterias.isEmpty());

		// Modify a OutlierCriteria
		OutlierCriteria oc = null;
		for (OutlierCriteria crit : criterias) {
			if (crit.getCode().equals("OTLR_DEV")) {
				oc = crit;
				break;
			}
		}
		// Verify we successfully get the specified Criteria
		assertNotNull(oc);
		Calendar cal = Calendar.getInstance();
		oc.setLastModified(cal.getTime());
		oc.setModifiedBy("JUNIT");

		// Save changes
		em.getTransaction().begin();
		outlierCriteriaRepository.saveOutlierCriteria(oc);
		em.getTransaction().commit();

		// Load records again and check that changes persisted.
		criterias = outlierCriteriaRepository.getOutlierCriterias();
		OutlierCriteria ocReCheck = null;
		for (OutlierCriteria crit : criterias) {
			if (crit.getCode().equals("OTLR_DEV")) {
				ocReCheck = crit;
				break;
			}
		}
		// Verify we successfully get the specified Criteria
		assertNotNull(ocReCheck);
		// Verify changes were saved
		assertTrue(ocReCheck.getModifiedBy().equals("JUNIT"));
		assertTrue(ocReCheck.getLastModified().equals(cal.getTime()));
	}

	/**
	 * Save a new Period
	 * 
	 * Verify save by checking if the Period ID was set (Done following successful save)
	 */
	@Test
	public void addNewPracitionerProfilePeriodTest() {
		PractitionerProfilePeriod ppp = new PractitionerProfilePeriod();
		Calendar cal = Calendar.getInstance();

		// Period start
		cal.set(2500, Calendar.JANUARY, 1);
		ppp.setPeriodStart(cal.getTime());
		// Period end
		cal.set(2501, Calendar.JANUARY, 1);
		ppp.setPeriodEnd(cal.getTime());
		// Year End
		cal.set(2502, Calendar.JANUARY, 1);
		ppp.setYearEnd(cal.getTime());
		// Last Modified
		cal = Calendar.getInstance();
		ppp.setLastModified(cal.getTime());

		ppp.setModifiedBy("JUNIT");

		// Attempt save-
		em.getTransaction().begin();
		ppp = outlierCriteriaRepository.addNewPracitionerProfilePeriod(ppp);
		em.getTransaction().commit();

		// Verify the ID was set
		assertNotNull(ppp.getPeriodId());
	}
}
