package ca.medavie.nspp.audit.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ca.medavie.nspp.audit.test.accounting.ManualDepositTests;

/**
 * Collection of all Junits for testing the Accounting portion of TRAX. Simply right click and run as junit
 */
@RunWith(Suite.class)
@SuiteClasses({ ManualDepositTests.class })
public class RunAccountingTestSuite {
}
