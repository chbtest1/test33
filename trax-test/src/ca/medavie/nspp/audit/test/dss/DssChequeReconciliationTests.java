package ca.medavie.nspp.audit.test.dss;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.ChequeReconciliation;
import ca.medavie.nspp.audit.service.data.ChequeReconciliationSearchCriteria;
import ca.medavie.nspp.audit.service.repository.DssChequeReconciliationRepository;
import ca.medavie.nspp.audit.service.repository.DssChequeReconciliationRepositoryImpl;
import ca.medavie.nspp.audit.test.EntityManagement;

/**
 * Places the DssChequeReconciliationRepository under test
 */
public class DssChequeReconciliationTests {
	private static DssChequeReconciliationRepository chequeReconciliationRepository;

	@BeforeClass
	public static void setUpClass() {
		chequeReconciliationRepository = new DssChequeReconciliationRepositoryImpl(EntityManagement.getEntityManager());
	}

	/**
	 * Search for specified Cheque Reconciliation records
	 * 
	 * Verify the correct data returns
	 */
	@Test
	public void getChequeReconciliationSearchResultsTest() {

		ChequeReconciliationSearchCriteria sc = new ChequeReconciliationSearchCriteria();
		sc.setChequeNumber(Long.valueOf(108054));

		List<ChequeReconciliation> results = chequeReconciliationRepository.getChequeReconciliationSearchResults(sc, 0,
				20);
		
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Should only get one record
		assertTrue(results.size() == 1);
		// Verify data
		for (ChequeReconciliation cr : results) {
			assertTrue(cr.getChequeNumber().equals(Long.valueOf(108054)));
			assertTrue(cr.getHealthCardNumber().trim().equals("9900070799"));
			assertTrue(cr.getGlNumber().equals(Long.valueOf(860)));
		}
	}

	/**
	 * Save a new Cheque Reconciliation record
	 * 
	 * Verify save by loading new record
	 */
	@Test
	public void saveChequeReconciliationTest() {
		// TODO not sure how we are going to implement saving tests (Stub?)
	}
}
