package ca.medavie.nspp.audit.test.dss;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.DenticareClaim;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferRequest;
import ca.medavie.nspp.audit.service.data.HealthCard;
import ca.medavie.nspp.audit.service.data.MedicareClaim;
import ca.medavie.nspp.audit.service.data.PharmacareClaim;
import ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepository;
import ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepositoryImpl;
import ca.medavie.nspp.audit.test.EntityManagement;

/**
 * Places the DssClaimsHistoryTransferRepository under test
 */
public class DssClaimsHistoryTransferTests {
	private static DssClaimsHistoryTransferRepository claimsHistoryTransferRepository;

	@BeforeClass
	public static void setUpClass() {
		claimsHistoryTransferRepository = new DssClaimsHistoryTransferRepositoryImpl(
				EntityManagement.getEntityManager());
	}

	/**
	 * Load the specified HealthCard
	 * 
	 * Verify details returned
	 */
	@Test
	public void getSearchedHealthCardTest() {
		HealthCard hc = claimsHistoryTransferRepository.getSearchedHealthCard("9900015216");
		// Verify we get a HealthCard back
		assertNotNull(hc);
		// Verify data
		assertTrue(hc.getHealthCardNumber().equals("9900015216"));
		assertTrue(hc.getGender().equals("M"));
	}

	/**
	 * Load all available MedicareSE claims for the specified transfer request
	 * 
	 * Verify the claims returned
	 */
	@Test
	public void getMedicareSeClaimsTest() {
		DssClaimsHistoryTransferRequest request = new DssClaimsHistoryTransferRequest();
		request.setSourceHealthCardNumber("0008865917");

		List<MedicareClaim> results = claimsHistoryTransferRepository.getMedicareSeClaims(request);
		// Verify we get claims
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Verify data
		for (MedicareClaim mc : results) {
			assertTrue(mc.getPatientHcn().trim().equals("0008865917"));
		}
	}

	/**
	 * Load all available MedicareMainframe claims for the specified transfer request
	 * 
	 * Verify the claims returned
	 */
	@Test
	public void getMedicareMainframeClaimsTest() {
		DssClaimsHistoryTransferRequest request = new DssClaimsHistoryTransferRequest();
		request.setSourceHealthCardNumber("9900007221");

		List<MedicareClaim> results = claimsHistoryTransferRepository.getMedicareMainframeClaims(request);
		// Verify we get claims
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Verify data
		for (MedicareClaim mc : results) {
			assertTrue(mc.getMainHealthCardNumber().trim().equals("9900007221"));
		}
	}

	/**
	 * Load all available PharmacareSE claims for the specified transfer request
	 * 
	 * Verify the claims returned
	 */
	@Test
	public void getPharmacareSeClaimsTest() {
		DssClaimsHistoryTransferRequest request = new DssClaimsHistoryTransferRequest();
		request.setSourceHealthCardNumber("9900033730");

		List<PharmacareClaim> results = claimsHistoryTransferRepository.getPharmacareSeClaims(request);
		// Verify we get claims
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Verify data
		for (PharmacareClaim pc : results) {
			assertTrue(pc.getRecipientHealthCardNumber().trim().equals("9900033730"));
		}
	}

	/**
	 * Load all available PharmacareMainframe claims for the specified transfer request
	 * 
	 * Verify the claims returned
	 */
	@Test
	public void getPharmacareMainframeClaimsTest() {
		DssClaimsHistoryTransferRequest request = new DssClaimsHistoryTransferRequest();
		request.setSourceHealthCardNumber("9900007247");

		List<PharmacareClaim> results = claimsHistoryTransferRepository.getPharmacareMainframeClaims(request);
		// Verify we get claims
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Verify data
		for (PharmacareClaim pc : results) {
			assertTrue(pc.getMainRecipientHealthCardNumber().trim().equals("9900007247"));
		}
	}

	/**
	 * Load all available Dental claims for the specified transfer request
	 * 
	 * Verify the claims returned
	 */
	@Test
	public void getDentalClaimsTest() {
		DssClaimsHistoryTransferRequest request = new DssClaimsHistoryTransferRequest();
		request.setSourceHealthCardNumber("0000025684");

		List<DenticareClaim> results = claimsHistoryTransferRepository.getDentalClaims(request);
		// Verify we get claims
		assertNotNull(results);
		assertFalse(results.isEmpty());
		// Verify data
		for (DenticareClaim dc : results) {
			assertTrue(dc.getHealthCardNumber().trim().equals("0000025684"));
		}
	}
}
