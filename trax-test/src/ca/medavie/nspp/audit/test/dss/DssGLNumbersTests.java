package ca.medavie.nspp.audit.test.dss;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.DssGLNumberBO;
import ca.medavie.nspp.audit.service.repository.DssGLNumbersRepository;
import ca.medavie.nspp.audit.service.repository.DssGLNumbersRepositoryImpl;
import ca.medavie.nspp.audit.test.EntityManagement;

/**
 * Places the DssGLNumbersRepository under test
 */
public class DssGLNumbersTests {
	private static DssGLNumbersRepository glNumbersRepository;

	@BeforeClass
	public static void setUpClass() {
		glNumbersRepository = new DssGLNumbersRepositoryImpl(EntityManagement.getEntityManager());
	}

	/**
	 * Load all GL numbers
	 * 
	 * Verify we get data back
	 */
	@Test
	public void getGLNumbersTest() {
		List<DssGLNumberBO> results = glNumbersRepository.getGLNumbers();
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
	}

	/**
	 * Load GL Number by its PK
	 * 
	 * TODO method will be converted to PK method once repo is updated..
	 * 
	 * Verify we get the correct GL number
	 */
	@Test
	public void getGLNumberByPKTest() {
		DssGLNumberBO result = glNumbersRepository.getGLNumberByGL(Long.valueOf(891));
		// Verify we get a result
		assertNotNull(result);
		// Verify data returned
		assertTrue(result.getGlNumber().equals(Long.valueOf(891)));
		assertTrue(result.getGlDescription().equals("GP COLLABORATIVE PRACTICE"));
	}

	/**
	 * Save a new GL Number record
	 * 
	 * Verify save by loading new GL record
	 */
	@Test
	public void saveGLNumberTest() {
		// TODO not sure how we are going to implement saving tests (Stub?)
	}

	/**
	 * Delete the specified GL number
	 * 
	 * Verify delete by attempting to load the deleted record
	 */
	@Test
	public void deleteGLNumbersTest() {
		// TODO not sure how we are going to implement deleting tests (Stub?)
	}
}
