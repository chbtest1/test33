package ca.medavie.nspp.audit.test.dss;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.DssUnitValueBO;
import ca.medavie.nspp.audit.service.repository.DssUnitValueRepository;
import ca.medavie.nspp.audit.service.repository.DssUnitValueRepositoryImpl;
import ca.medavie.nspp.audit.test.EntityManagement;

/**
 * Places the DssUnitValueRepository under test
 */
public class DssUnitValueTests {
	private static DssUnitValueRepository unitValueRepository;

	@BeforeClass
	public static void setUpClass() {
		unitValueRepository = new DssUnitValueRepositoryImpl(EntityManagement.getEntityManager());
	}

	/**
	 * Load all DSS Unit values
	 * 
	 * Verify we get data back
	 */
	@Test
	public void getDssUnitValuesTest() {
		List<DssUnitValueBO> results = unitValueRepository.getDssUnitValues();
		// Verify we get results
		assertNotNull(results);
		assertFalse(results.isEmpty());
	}

	/**
	 * Save changes to an existing DSS Unit value, then save a new one
	 * 
	 * Verify change save by loading and checking for changes, verify save new by loading new record
	 */
	@Test
	public void saveOrUpdateDssUnitValueTest() {
		// TODO not sure how we are going to implement saving tests (Stub?)
	}

	/**
	 * Delete the specified DSS Unit value
	 * 
	 * Verify delete by attempting to load the deleted record
	 */
	@Test
	public void deleteDssUnitValuesTest() {
		// TODO not sure how we are going to implement deleting tests (Stub?)
	}

	/**
	 * Check if the provided DSS Unit Value overlaps any others
	 * 
	 * 1) Overlap should return TRUE. 2) Overlap should return FALSE
	 * 
	 * Verify both overlap checks return what is expected
	 */
	@Test
	public void overlapsTest() {
		// Overlap check 1
		DssUnitValueBO unit1 = new DssUnitValueBO();
		unit1.setUnitValueTypeCode(BigDecimal.valueOf(4));
		unit1.setUnitValueLevel(BigDecimal.valueOf(1));

		Calendar cal = Calendar.getInstance();
		cal.set(2010, 3, 1);
		unit1.setEffectiveFromDate(cal.getTime());
		cal.set(2011, 2, 31);
		unit1.setEffectiveToDate(cal.getTime());

		// Check for overlap.. should return TRUE
		assertTrue(unitValueRepository.overlaps(unit1));

		// Overlap check 2
		DssUnitValueBO unit2 = new DssUnitValueBO();
		unit2.setUnitValueTypeCode(BigDecimal.valueOf(4));
		unit2.setUnitValueLevel(BigDecimal.valueOf(1));

		cal.set(9999, 0, 1);
		unit2.setEffectiveFromDate(cal.getTime());
		cal.set(9999, 1, 1);
		unit2.setEffectiveToDate(cal.getTime());

		// Check for overlap.. should return FALSE
		assertFalse(unitValueRepository.overlaps(unit2));
	}
}
