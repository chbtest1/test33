package ca.medavie.nspp.audit.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Provides static access to EJB EntityManager. EntityManager configured by XML in 'src/META-INF'
 */
public class EntityManagement {

	public static EntityManager getEntityManager() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("trax-ejb-test");
		return emf.createEntityManager();
	}
}
