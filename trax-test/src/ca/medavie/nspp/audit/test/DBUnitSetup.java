package ca.medavie.nspp.audit.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;

/**
 * Class responsible for instantiating DB Unit when requested. Set up is called via static method runSetup(EnvProps)
 */
public class DBUnitSetup {

	/**
	 * Set up DB unit. Sets the DB state
	 */
	public static void runSetup(EnvironmentProperties props) {
		Connection con = null;
		IDatabaseConnection connection = null;

		try {

			// Set up connection
			Properties connectionProps = new Properties();
			connectionProps.put("user", props.getDBUser());
			connectionProps.put("password", props.getDBPassword());
			con = DriverManager.getConnection(props.getDBUrl(), connectionProps);
			connection = new DatabaseConnection(con, props.getDBSchema());

			DatabaseConfig dbConfig = connection.getConfig();
			dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
			
			IDataSet dataSet = new XmlDataSet( DBUnitSetup.class.getResourceAsStream( props.getDatasetLocation() ) );
            ReplacementDataSet rDataSet = new ReplacementDataSet( dataSet );

			// Prepare DB for tests
            DatabaseOperation.REFRESH.execute( connection, rDataSet );

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Performs test clean up so there is no trace of tests.
	 */
	public static void dbUnitTearDown(EnvironmentProperties props) {
		Connection con = null;
		IDatabaseConnection connection = null;

		try {

			// Set up connection
			Properties connectionProps = new Properties();
			connectionProps.put("user", props.getDBUser());
			connectionProps.put("password", props.getDBPassword());
			con = DriverManager.getConnection(props.getDBUrl(), connectionProps);
			connection = new DatabaseConnection(con, props.getDBSchema());

			DatabaseConfig dbConfig = connection.getConfig();
			dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());

			// Clean up the data to ensure we have a fresh dataset
			QueryDataSet cleanupDataSet = new QueryDataSet(connection);
			final Map<String, String> cleanupTableMap = CleanupXMLReader.parseTables(props.getCleanupDataLocation());
			for (Entry<String, String> entry : cleanupTableMap.entrySet()) {
				cleanupDataSet.addTable(entry.getKey(), entry.getValue());
			}

			// Perform delete if required
			if (!cleanupTableMap.isEmpty()) {
				DatabaseOperation.DELETE.execute(connection, cleanupDataSet);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
