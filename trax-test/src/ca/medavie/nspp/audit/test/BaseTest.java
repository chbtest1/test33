package ca.medavie.nspp.audit.test;

import javax.persistence.EntityManager;

import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * Root of all tests. This provides a easy spot of maintaining what happens before all tests or between and after
 */
public class BaseTest {

	/** Environment properties **/
	private static EnvironmentProperties props;
	protected static EntityManager em;

	@BeforeClass
	public static void setUpTestSuite() {
		// Set up EntityManager
		em = EntityManagement.getEntityManager();

		// Run DBUnit set up
		props = EnvironmentProperties.getInstance();
		DBUnitSetup.runSetup(props);
	}

	@AfterClass
	public static void closeTestSuite() {
		// Perform the DB cleanup
		DBUnitSetup.dbUnitTearDown(props);
	}
}
