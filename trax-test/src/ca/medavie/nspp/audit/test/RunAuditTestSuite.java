package ca.medavie.nspp.audit.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ca.medavie.nspp.audit.test.audit.AuditResultTests;
import ca.medavie.nspp.audit.test.audit.DenticareTests;
import ca.medavie.nspp.audit.test.audit.HealthServiceGroupsTests;
import ca.medavie.nspp.audit.test.audit.MedicareTests;
import ca.medavie.nspp.audit.test.audit.OutlierCriteriaTests;
import ca.medavie.nspp.audit.test.audit.PeerGroupTests;
import ca.medavie.nspp.audit.test.audit.PharmacareTests;
import ca.medavie.nspp.audit.test.audit.ProfileInquiryTests;

/**
 * Collection of all Junits for testing the Audit portion of TRAX. Simply right click and run as junit
 */
@RunWith(Suite.class)
@SuiteClasses({ AuditResultTests.class, DenticareTests.class, HealthServiceGroupsTests.class, MedicareTests.class,
		OutlierCriteriaTests.class, PeerGroupTests.class, PharmacareTests.class, ProfileInquiryTests.class })
public class RunAuditTestSuite {
}
