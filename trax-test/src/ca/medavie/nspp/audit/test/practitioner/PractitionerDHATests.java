package ca.medavie.nspp.audit.test.practitioner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.medavie.nspp.audit.service.data.DistrictHealthAuthority;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.repository.DHARepository;
import ca.medavie.nspp.audit.service.repository.DHARepositoryImpl;
import ca.medavie.nspp.audit.test.EntityManagement;

/**
 * Places the DHARepository under test
 */
public class PractitionerDHATests {
	private static DHARepository dhaRepository;

	@BeforeClass
	public static void setUpClass() {
		dhaRepository = new DHARepositoryImpl(EntityManagement.getEntityManager());
	}

	/**
	 * Load all DHA records
	 * 
	 * Verify we get data back
	 */
	@Test
	public void getDHAsTest() {
		try {
			List<DistrictHealthAuthority> results = dhaRepository.getDHAs();
			// Verify we get results
			assertNotNull(results);
			assertFalse(results.isEmpty());

		} catch (Exception e) {
			fail("Exception caught while testing getDHAs() " + e.getMessage());
		}
	}

	/**
	 * Search for the specified Practitioner
	 * 
	 * Verify we get the expected return
	 */
	@Test
	public void getSearchedPractitionersTest() {
		try {
			PractitionerSearchCriteria sc = new PractitionerSearchCriteria();
			sc.setBusinessArrangementNumber(Long.valueOf(1239));
			sc.setSubcategory("PH");

			List<Practitioner> results = dhaRepository.getSearchedPractitioners(sc);
			// Verify we get results
			assertNotNull(results);
			assertFalse(results.isEmpty());
			// Verify results
			for (Practitioner p : results) {
				assertTrue(p.getPractitionerType().equals(sc.getSubcategory()));
				assertTrue(p.getBusinessArrangementNumber().equals(sc.getBusinessArrangementNumber()));
			}

		} catch (Exception e) {
			fail("Exception caught while testing getSearchedPractitioners() " + e.getMessage());
		}
	}

	/**
	 * Save a new DHA record
	 * 
	 * Verify save by loading new record
	 */
	@Test
	public void saveDHAChangesTest() {
		// TODO not sure how we are going to implement saving tests (Stub?)
	}

	/**
	 * Delete the specified DHA record
	 * 
	 * Verify delete by attempting to load the deleted record
	 */
	@Test
	public void deleteDHAsTest() {
		// TODO not sure how we are going to implement deleting tests (Stub?)
	}

	/**
	 * Update the specified DHA record
	 * 
	 * Verify update by loading record again and check for changes
	 */
	@Test
	public void updateDHAContractTownDetailsTest() {
		// TODO not sure how we are going to implement saving tests (Stub?)
	}

	/**
	 * Load Zone IDs for the specified Town
	 * 
	 * Verify we get the expected return
	 */
	@Test
	public void getZoneIdsTest() {
		try {
			Map<String, String> results = dhaRepository.getZoneIds("WESTPHAL");
			// Verify we get results
			assertNotNull(results);
			assertFalse(results.isEmpty());
			// Only one record should return
			assertTrue(results.size() == 1);
			// Verify results
			for (Entry<String, String> entry : results.entrySet()) {
				assertTrue(entry.getKey().trim().equals("4"));
				assertTrue(entry.getValue().trim().equals("CENTRAL"));
			}

		} catch (Exception e) {
			fail("Exception caught while testing getZoneIds() " + e.getMessage());
		}
	}

	/**
	 * Load DHA codes for the specified TownName and zone id
	 * 
	 * Verify we get the expected return
	 */
	@Test
	public void getDhaCodesTest() {
		try {
			Map<String, String> results = dhaRepository.getDhaCodes("UPPER CANARD", "1 ");
			// Verify we get results
			assertNotNull(results);
			assertFalse(results.isEmpty());
			// Only 1 record should return
			assertTrue(results.size() == 1);
			// Verify data
			for (Entry<String, String> entry : results.entrySet()) {
				assertTrue(entry.getKey().trim().equals("3"));
				assertTrue(entry.getValue().trim().equals("ANNAPOLIS VALLEY HEALTH"));
			}

		} catch (Exception e) {
			fail("Exception caught while testing getDhaCodes() " + e.getMessage());
		}
	}
}
