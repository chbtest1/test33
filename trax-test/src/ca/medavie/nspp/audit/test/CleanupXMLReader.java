package ca.medavie.nspp.audit.test;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class is used to parse table names and associated query strings. Used for DBUnit to build a dataset of database
 * rows to delete before test executions
 * 
 * The file must be in the format:
 * 
 * <cleanupdata>
 * <table name="[TABLE NAME]">
 * <query>[QUERY STRING]</query>
 * </table>
 * </cleanupdata>
 * 
 */
public class CleanupXMLReader {
	/** Constants for XML tag definitions */
	private static final String TABLE_TAG = "table";
	private static final String QUERY_TAG = "query";

	public static Map<String, String> parseTables(String aFileLocation) {
		final Map<String, String> tableMap = new HashMap<String, String>();

		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler() {
				String tableName = "";
				boolean queryFlag = false;

				public void startElement(String uri, String localName, String qName, Attributes attributes)
						throws SAXException {

					// Record table name
					if (qName.equals(TABLE_TAG)) {
						tableName = attributes.getValue(0);
						tableMap.put(tableName, "");
					}

					// If column
					if (qName.equals(QUERY_TAG)) {
						queryFlag = true;
					}

				}

				public void endElement(String uri, String localName, String qName) throws SAXException {

				}

				public void characters(char ch[], int start, int length) throws SAXException {
					if (queryFlag) {
						tableMap.put(tableName, new String(ch, start, length));
						queryFlag = false;
					}
				}

			};

			saxParser.parse(CleanupXMLReader.class.getResourceAsStream(aFileLocation), handler);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return tableMap;
	}
}
