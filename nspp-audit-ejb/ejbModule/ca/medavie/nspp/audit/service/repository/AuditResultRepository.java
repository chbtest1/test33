package ca.medavie.nspp.audit.service.repository;

import java.sql.SQLException;
import java.util.List;

import ca.medavie.nspp.audit.service.ServicePersistenceException;
import ca.medavie.nspp.audit.service.data.AuditResult;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.exception.AuditResultServiceException;

public interface AuditResultRepository {

	/**
	 * get practitioners
	 * 
	 * @param aPractitionerSearchCriteria
	 * @return List- A list of practitioner found by given searching criteria
	 */
	public List<Practitioner> getPractitionerSearchResult(PractitionerSearchCriteria aPractitionerSearchCriteria);

	/**
	 * Save or update Audit result
	 * 
	 * @param AuditResult
	 *            - a given AuditResult to save
	 * @throws ServicePersistenceException
	 * 
	 * **/
	public void setAuditResult(AuditResult anAuditedResultToSave);

	/**
	 * 
	 * get practitioner audit result by a given practitioner
	 * 
	 * @param aSelectedPractitioner
	 * @return
	 * @throws AuditResultServiceException
	 */
	public List<AuditResult> getSelectedPractitionerAuditResultDetails(Practitioner aSelectedPractitioner);

	/**
	 * Gets a list of all valid Health Service Codes
	 * 
	 * @return List of Health Service Codes
	 */
	public List<String> getHealthServiceCodes();

	/**
	 * Gets an attachment file binary
	 * 
	 * @param anAttachmentId
	 * @return
	 * @throws SQLException
	 */
	public byte[] getAttachmentBinary(Long anAttachmentId) throws SQLException;

	/**
	 * Saves an attachment file binary
	 * 
	 * @param anAttachmentId
	 *            - the attachment id for the record to save it in
	 * @param anAttachmentBinary
	 *            - a given attachment file binary to save
	 */
	public void setAttachmentBinary(Long anAttachmentId, byte[] anAttachmentBinary);

	public String getReason(String healthServiceCode, String qualifer);

	public void deleteAuditResult(AuditResult aSelectedAuditResult);

}
