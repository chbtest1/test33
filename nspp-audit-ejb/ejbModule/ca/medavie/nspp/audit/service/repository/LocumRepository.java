package ca.medavie.nspp.audit.service.repository;

import java.util.List;

import ca.medavie.nspp.audit.service.data.Locum;
import ca.medavie.nspp.audit.service.data.LocumSearchCriteria;

/**
 * Interface of the Locum Hosts repository layer
 */
public interface LocumRepository {

	/**
	 * Search for matching Locums based on the user defined search criteria
	 * 
	 * @param aLocumSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return list of matching Locums
	 */
	public List<Locum> getSearchedLocums(LocumSearchCriteria aLocumSearchCriteria, int firstRow, int maxRows);

	/**
	 * Counts all rows that return by an Locum search
	 * 
	 * @param aLocumSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedLocumsCount(LocumSearchCriteria aLocumSearchCriteria);
	
	/**
	 * Search for matching Practitioners based on the user defined search criteria. Practitioners found can be added to
	 * a Locum record as HOST or VISITOR
	 * 
	 * @param aLocumSearchCriteria
	 * @return list of matching LocumPractitioner
	 */
	public List<Locum> getSearchedLocumPractitioners(LocumSearchCriteria aLocumSearchCriteria);

	/**
	 * Loads the details of the specified Locum when selected from the search results
	 * 
	 * @param aLocum
	 *            - Locum record to load more details of
	 * @return populated Locum record
	 */
	public Locum getLocumDetails(Locum aLocum);

	/**
	 * Saves all changes made to the Locum record
	 * 
	 * @param aLocum
	 */
	public void saveLocum(Locum aLocum);

	/**
	 * Deletes the provided locums from the parent locum
	 * 
	 * @param aListOfLocumsForDelete
	 *            - Locums to delete from the parent
	 */
	public void deleteLocums(List<Locum> aListOfLocumsForDelete);
}
