package ca.medavie.nspp.audit.service.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.ServicePersistenceException;
import ca.medavie.nspp.audit.service.dao.DHADAO;
import ca.medavie.nspp.audit.service.dao.DHADAOImpl;
import ca.medavie.nspp.audit.service.data.DistrictHealthAuthority;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssProviderDhaMappedEntity;

public class DHARepositoryImpl extends BaseRepository implements DHARepository {

	private static final Logger logger = LoggerFactory.getLogger(DHARepositoryImpl.class);

	private DHADAO dhaDao;

	/**
	 * Constructor taking an EntityManager
	 * 
	 * @param em
	 */
	public DHARepositoryImpl(EntityManager em) {
		dhaDao = new DHADAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DHARepository#getDHAs()
	 */
	@Override
	public List<DistrictHealthAuthority> getDHAs() throws ServicePersistenceException {

		logger.debug("getDHAs() : Begin");

		// Holds found DHA
		List<DistrictHealthAuthority> dhas = new ArrayList<DistrictHealthAuthority>();

		// Load the DHA data
		List<DssProviderDhaMappedEntity> result = dhaDao.getDHAs();

		for (DssProviderDhaMappedEntity source : result) {

			DistrictHealthAuthority target = new DistrictHealthAuthority();
			BEAN_MAPPER.map(source, target);

			// Add district health authority to list
			dhas.add(target);
		}

		logger.debug("getDHAs() : End");
		return dhas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DHARepository#getSearchedPractitioners(ca.medavie.nspp.audit.service
	 * .data.PractitionerSearchCriteria)
	 */
	@Override
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aSearchCriteria)
			throws ServicePersistenceException {

		// Load the matching Practitioner data
		List<Object[]> result = dhaDao.getSearchedPractitioners(aSearchCriteria);

		// Holds the found Practitioners
		List<Practitioner> practitioners = new ArrayList<Practitioner>();

		// Loop over data and add Pracitioners to list
		for (Object[] row : result) {
			Practitioner p = new Practitioner();

			p.setPractitionerNumber(((BigDecimal) row[0]).longValue());
			p.setPractitionerType((String) row[1]);
			p.setPractitionerName((String) row[2]);
			//p.setBusinessArrangementNumber(((BigDecimal) row[3]).longValue());
			//p.setBusinessArrangementDescription((String) row[4]);
			if (row[3] != null) {
				p.setPractitionerGroupNumber(((BigDecimal) row[3]).longValue());
				p.setPractitionerGroupDescription((String) row[4]);
			} else {
				p.setPractitionerGroupNumber(null);
				p.setPractitionerGroupDescription(null);
			}
			
			// Add to list
			practitioners.add(p);
		}

		return practitioners;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DHARepository#saveDHAChanges(ca.medavie.nspp.audit.service.data.
	 * DistrictHealthAuthority)
	 */
	@Override
	public void saveDHAChanges(DistrictHealthAuthority aDha) throws ServicePersistenceException {
		dhaDao.saveDHAChanges(aDha);

		// Update the backup values to the new ones
		aDha.setOriginalContractTownName(aDha.getContractTownName());
		aDha.setOriginalZoneId(aDha.getZoneId());
		aDha.setOriginalZoneName(aDha.getZoneName());
		aDha.setOriginalDhaCode(aDha.getDhaCode());
		aDha.setOriginalDhaDescription(aDha.getDhaDescription());
		aDha.setOriginalFte(aDha.getFte());
		aDha.setFteString(aDha.getFte().toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DHARepository#deleteDHAs(java.util.List)
	 */
	@Override
	public void deleteDHAs(List<DistrictHealthAuthority> aListOfDhasToDelete) throws ServicePersistenceException {
		dhaDao.deleteDHAs(aListOfDhasToDelete);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DHARepository#updateDHAContractTownDetails(ca.medavie.nspp.audit.service
	 * .data.DistrictHealthAuthority)
	 */
	@Override
	public void updateDHAContractTownDetails(DistrictHealthAuthority aDha) throws ServicePersistenceException {
		aDha.setZoneName(dhaDao.getZoneName(aDha.getZoneId()));
		aDha.setDhaDescription(dhaDao.getDhaDescription(aDha.getDhaCode()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DHARepository#getZoneIds(java.lang.String)
	 */
	@Override
	public Map<String, String> getZoneIds(String aTownName) throws ServicePersistenceException {

		// Holds data for return
		Map<String, String> zoneIdMap = new HashMap<String, String>();
		List<Object[]> result = dhaDao.getZoneIds(aTownName);

		for (Object[] r : result) {
			zoneIdMap.put((String) r[0], (String) r[1]);
		}
		return zoneIdMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DHARepository#getDhaCodes(java.lang.String, java.lang.String)
	 */
	@Override
	public Map<String, String> getDhaCodes(String aTownName, String aZoneId) throws ServicePersistenceException {

		// Holds data for return
		Map<String, String> dhaCodeMap = new HashMap<String, String>();
		List<Object[]> result = dhaDao.getDhaCodes(aTownName, aZoneId);

		for (Object[] r : result) {
			dhaCodeMap.put((String) r[0], (String) r[1]);
		}
		return dhaCodeMap;
	}
}
