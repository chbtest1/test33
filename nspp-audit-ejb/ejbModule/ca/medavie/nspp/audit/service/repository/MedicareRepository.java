package ca.medavie.nspp.audit.service.repository;

import java.util.List;

import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.BusinessArrangement;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.IndividualExclusion;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;

/**
 * Interface of the Medicare repository layer
 */
public interface MedicareRepository {

	/**
	 * Loads matching HealthServiceCriteria records for Description update
	 * 
	 * @param aSearchCriteria
	 * @return list of matching HealthServiceCriterias for Description update
	 */
	public List<HealthServiceCriteria> getSearchedHealthServiceCriteriaDescriptions(
			HealthServiceCriteriaSearchCriteria aSearchCriteria);

	/**
	 * Saves the changes made to the HealthServiceCriteria description.
	 * 
	 * @param aListOfHsc
	 *            - HSC records to save description changes
	 */
	public void saveHealthServiceCriteriaDescriptions(List<HealthServiceCriteria> aListOfHsc);

	/**
	 * Loads the current HealthServiceCriteria Exclusions
	 * 
	 * @return current HealthServiceCriteria Exclusions
	 */
	public List<HealthServiceCriteria> getHealthServiceCriteriaExclusions();

	/**
	 * Saves the new and updated HealthServiceCriteria exclusions to the database
	 * 
	 * @param aListOfHscForSave
	 */
	public void saveHealthServiceCriteriaExclusions(List<HealthServiceCriteria> aListOfHscForSave);

	/**
	 * Deletes the specified HealthServiceExclusions from the database
	 * 
	 * @param aListOfHscForDelete
	 */
	public void deleteHealthServiceCriteriaExclusions(List<HealthServiceCriteria> aListOfHscForDelete);

	/**
	 * Load the current Individual Exclusions
	 * 
	 * @return current Individual Exclusions
	 */
	public List<IndividualExclusion> getIndividualExclusions();

	/**
	 * Saves the new and updated Individual Exclusions to the database
	 * 
	 * @param aListOfIndExclToSave
	 */
	public void saveIndividualExclusions(List<IndividualExclusion> aListOfIndExclToSave);

	/**
	 * Attempts the owners name of the provided Health Card Number
	 * 
	 * @param aHealthCardNumber
	 * @return matching name if found. Otherwise no value is returned
	 */
	public String getHealthCardNumberOwnerName(String aHealthCardNumber);

	/**
	 * Saves the specified AuditCriteria record
	 * 
	 * @param anAuditCriteria
	 */
	public void saveMedicareAuditCriteria(AuditCriteria anAuditCriteria);

	/**
	 * Attempts to load the Practitioner Name via specified arguments
	 * 
	 * @param aPractitionerId
	 * @param aSubcategory
	 * @return Practitioner name if found, otherwise null
	 */
	public String getPractitionerName(Long aPractitionerId, String aSubcategory);

	/**
	 * Attempts to locate an existing Audit Criteria via specified arguments
	 * 
	 * @param aPractitionerId
	 * @param aSubcategory
	 * @param anAuditType
	 * @return AuditCriteria if found, otherwise null
	 */
	public AuditCriteria getMedicarePractitionerAuditCriteria(Long aPractitionerId, String aSubcategory,
			String anAuditType);

	/**
	 * Loads any matching AuditCriteria records using the specified Search Criteria
	 * 
	 * @param aSearchCriteria
	 * @param startRow
	 *            - Row number the result set should begin
	 * @param maxRows
	 *            - Maximum number of results to return
	 * @return any matching AuditCriteria records
	 */
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Audit Criteria search
	 * 
	 * @param anAuditCriteriaSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria);

	/**
	 * Attempts to load AuditCriteria record by ID
	 * 
	 * @param anId
	 *            - ID of the desired AuditCriteria
	 * @return AuditCriteria with the specified ID
	 */
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId);

	/**
	 * Attempts to load a Default AuditCriteria by the specified AuditType
	 * 
	 * @param anAuditType
	 * @return the matching audit if it exists or a brand new AuditCriteria object if nothing is found
	 */
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType);

	/**
	 * Get list of medicare audit letter response results
	 * 
	 * @param medicareALRSearchCriteria
	 * @return
	 */
	public List<MedicareAuditLetterResponse> getSearchedMedicareALR(
			MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria, int startRow, int maxRows);

	/**
	 * Performs a record count of the entire possible dataset returned by the provided search criteria
	 * 
	 * @param medicareALRSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws MedicareServiceException
	 */
	public Integer getSearchedMedicareALRCount(MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria);

	/**
	 * Save medicare Audit letter response
	 * 
	 * @param selectedMCALR
	 */
	public void saveMedicareALR(MedicareAuditLetterResponse selectedMCALR);

	/**
	 * Retrieve the Number Of Letters Created by Audit Run Number and Audit Type. This is only used when the user
	 * provided the Audit Run Number and Audit Type in their search criteria. Displayed above the search results table.
	 * 
	 * To prevent duplicate code this is shared amongst of 3 parts of Service Verification (Medicare, Pharmacare and
	 * Denticare)
	 * 
	 * @param anAuditRunNumber
	 * @param anAuditType
	 * @return number of letters created
	 */
	public Long getNumberOfLettersCreated(Long anAuditRunNumber, String anAuditType);

	/**
	 * Load the specified Business Arrangement
	 * 
	 * @param aBusinessArrangementNumber
	 * @return Business Arrangement with the specified BA number
	 */
	public BusinessArrangement getBusinessArrangement(Long aBusinessArrangementNumber);

	/**
	 * Load all existing Business Arrangement Exclusions
	 * 
	 * @return all Business Arrangement Exclusions in the system
	 */
	public List<BusinessArrangement> getBusinessArrangementExclusions();

	/**
	 * Saves a new Exclusion record for the specified Business Arrangement
	 * 
	 * @param aBusinessArrangementExclusion
	 */
	public void saveBusinessArrangementExclusion(BusinessArrangement aBusinessArrangementExclusion);

	/**
	 * Deletes the specified Business Arrangement Exclusions from the system
	 * 
	 * @param aListOfBAExclusionsForDelete
	 */
	public void deleteBusinessArrangementExclusions(List<BusinessArrangement> aListOfBAExclusionsForDelete);
}
