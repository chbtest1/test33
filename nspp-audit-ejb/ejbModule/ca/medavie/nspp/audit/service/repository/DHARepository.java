package ca.medavie.nspp.audit.service.repository;

import java.util.List;
import java.util.Map;

import ca.medavie.nspp.audit.service.ServicePersistenceException;
import ca.medavie.nspp.audit.service.data.DistrictHealthAuthority;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;

/**
 * Interface of the DHA(District Health Authority) repository layer
 */
public interface DHARepository {
	/**
	 * Loads all existing DHA(District Health Authority) records
	 * 
	 * @return list of DHAs
	 * @throws ServicePersistenceException
	 */
	public List<DistrictHealthAuthority> getDHAs() throws ServicePersistenceException;

	/**
	 * Loads the matching Practitioner using the user search criteria
	 * 
	 * @param aSearchCriteria
	 * @return list of matching Practitioners
	 * @throws ServicePersistenceException
	 */
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aSearchCriteria)
			throws ServicePersistenceException;

	/**
	 * Save the changes made to a DHA record to the DB.
	 * 
	 * @param aDha
	 *            - the DHA record that was changed
	 * @throws ServicePersistenceException
	 */
	public void saveDHAChanges(DistrictHealthAuthority aDha) throws ServicePersistenceException;

	/**
	 * Deletes the provided DHA records from the database
	 * 
	 * @param aListOfDhasToDelete
	 *            - DHAs to be deleted
	 * @throws ServicePersistenceException
	 */
	public void deleteDHAs(List<DistrictHealthAuthority> aListOfDhasToDelete) throws ServicePersistenceException;

	/**
	 * Updates the provided DHA object with the correct ContractTown details. (Additional data is required from the
	 * database: Zone Name/DHA Description)
	 * 
	 * @param aDha
	 *            - the DistrictHealthAuthority object being updated with new ContractTown data
	 * @throws ServicePersistenceException
	 */
	public void updateDHAContractTownDetails(DistrictHealthAuthority aDha) throws ServicePersistenceException;

	/**
	 * Loads the available ZoneIds along with their description for the given town
	 * 
	 * @param aTownName
	 * @return map<ZoneId, ZoneDescription>
	 * @throws ServicePersistenceException
	 */
	public Map<String, String> getZoneIds(String aTownName) throws ServicePersistenceException;

	/**
	 * 
	 * Loads the available DhaCodes along with their description for the given town and zone
	 * 
	 * @param aTownName
	 * @param aZoneId
	 * @return map<dhaCode, dhaName>
	 * @throws ServicePersistenceException
	 */
	public Map<String, String> getDhaCodes(String aTownName, String aZoneId) throws ServicePersistenceException;
}
