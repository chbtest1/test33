package ca.medavie.nspp.audit.service.repository;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.persistence.EntityManager;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.dao.PeerGroupDAO;
import ca.medavie.nspp.audit.service.dao.PeerGroupDAOImpl;
import ca.medavie.nspp.audit.service.data.BaseSearchCriteria;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.DssProvider2;
import ca.medavie.nspp.audit.service.data.DssProviderPeerGroup;
import ca.medavie.nspp.audit.service.data.DssProviderPeerGroupPK;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssHealthServiceMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPeerGroupMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPostalCodeMappedEntity;
import ca.medavie.nspp.audit.service.exception.PeerGroupServiceException;

/**
 * Implementation of the PeerGroup repository layer
 */
public class PeerGroupRepositoryImpl extends BaseRepository implements PeerGroupRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(PeerGroupRepositoryImpl.class);

	private PeerGroupDAO peerGroupDAO;

	/** instance variable, only load once when this instance is created **/
	private List<DssPostalCodeMappedEntity> allUniqueDssPostalCodes;

	/**
	 * Constructor taking an EntityManager
	 * 
	 * @param em
	 */
	public PeerGroupRepositoryImpl(EntityManager em) {

		peerGroupDAO = new PeerGroupDAOImpl(em);

	}

	/**
	 * @param outlierCriteriaEJBDAO
	 *            the outlierCriteriaEJBDAO to set
	 */
	public void setPeerGroupEJBDAO(PeerGroupDAO peerGroupDAO) {
		this.peerGroupDAO = peerGroupDAO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PeerGroupRepository#getSearchedPeerGroups(ca.medavie.nspp.audit.service
	 * .data.PeerGroupSearchCriteria, int, int)
	 */
	@Override
	public List<PeerGroup> getSearchedPeerGroups(PeerGroupSearchCriteria peerGroupSearchCriteria, int startRow,
			int maxRows) {

		LOGGER.debug("PeerGroupRepositoryImpl- getSearchedPeerGroups(): Begin");

		List<DssPeerGroupMappedEntity> providerGroupDataObjects = peerGroupDAO.getBasePeerGroups(
				peerGroupSearchCriteria, startRow, maxRows);
		List<PeerGroup> mappedResults = new ArrayList<PeerGroup>();

		for (DssPeerGroupMappedEntity source : providerGroupDataObjects) {

			PeerGroup target = new PeerGroup();
			// only map base information for peergroup
			BEAN_MAPPER.map(source, target);
			mappedResults.add(target);
		}

		LOGGER.debug("PeerGroupRepositoryImpl- getSearchedPeerGroups(): End");
		return mappedResults;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PeerGroupRepository#getSearchedPeerGroupsCount(ca.medavie.nspp.audit
	 * .service.data.PeerGroupSearchCriteria)
	 */
	@Override
	public Integer getSearchedPeerGroupsCount(PeerGroupSearchCriteria peerGroupSearchCriteria) {
		LOGGER.debug("PeerGroupRepositoryImpl- getSearchedPeerGroupsCount(): Begin");
		Integer count = peerGroupDAO.getSearchedPeerGroupsCount(peerGroupSearchCriteria);
		LOGGER.debug("PeerGroupRepositoryImpl- getSearchedPeerGroupsCount(): End");
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PeerGroupRepository#getSearchedPractitioners(ca.medavie.nspp.audit.service
	 * .data.PractitionerSearchCriteria)
	 */
	@Override
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria) {

		// Holds all found Practitioners
		List<Practitioner> practitioners = new ArrayList<Practitioner>();

		// Search for matching DssProvider2 records
		List<DssProvider2> dp2s = peerGroupDAO.getSearchedDssProvider2(aPractitionerSearchCriteria);

		// Convert DssProvider2 objects to Practitioner BOs
		for (DssProvider2 dp2 : dp2s) {
			Practitioner practitioner = new Practitioner();
			// Set values
			practitioner.setPractitionerNumber(dp2.getId().getProviderNumber());
			practitioner.setPractitionerType(dp2.getId().getProviderType());
			practitioner.setLicenseNumber(dp2.getProviderLicenseNumber());
			practitioner.setPractitionerName(dp2.getFullName());

			practitioner.setProviderPeerGroupId(aPractitionerSearchCriteria.getPeerGroupId());
			practitioner.setYearEndDate(aPractitionerSearchCriteria.getYearEndDate());

			practitioners.add(practitioner);
		}
		return practitioners;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.PeerGroupRepository#getLatestPeerGroups()
	 */
	@Override
	public List<PeerGroup> getLatestPeerGroups() {

		// List<DssProviderPeerGroup> pgs = peerGroupDAO.getLatestDssProviderPeerGroup();

		List<DssPeerGroupMappedEntity> pgs = peerGroupDAO.getLatestDssProviderPeerGroup();
		List<PeerGroup> pgcs = new ArrayList<PeerGroup>();

		for (DssPeerGroupMappedEntity source : pgs) {
			PeerGroup target = new PeerGroup();
			BEAN_MAPPER.map(source, target);
			pgcs.add(target);
		}
		return pgcs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.PeerGroupRepository#copyPeerGroups(java.util.List,
	 * java.lang.String)
	 */
	@Override
	public void copyPeerGroups(List<PeerGroup> aListOfPeerGroupsToCopy, String aUserId) {

		List<DssProviderPeerGroup> providerPeerGroupsToSave = new ArrayList<DssProviderPeerGroup>();

		for (PeerGroup source : aListOfPeerGroupsToCopy) {

			DssProviderPeerGroupPK pk = new DssProviderPeerGroupPK();
			pk.setProviderPeerGroupId(source.getPeerGroupID());
			pk.setYearEndDate(source.getYearEndDate());
			// get source data object by searching by id
			DssProviderPeerGroup sourceData = peerGroupDAO.getDssProviderPeerGroupByPK(pk);

			BEAN_MAPPER.map(sourceData, source);

			// Set the user ID on the PeerGroup record
			source.setModifiedBy(aUserId);
			// update to new year end date
			DataUtilities.updateYearEndDate4PeerGroups(source, source.getNewYearEndDate());
			// update last modified date
			DataUtilities.updatePeerGroupsBulkCopyModifedMetadata(source);

			DssProviderPeerGroup target = new DssProviderPeerGroup();
			BEAN_MAPPER.map(source, target);

			providerPeerGroupsToSave.add(target);

		}
		peerGroupDAO.copyDssProviderPeerGroups(providerPeerGroupsToSave);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PeerGroupRepository#savePeerGroup(ca.medavie.nspp.audit.service.data
	 * .PeerGroup)
	 */
	@Override
	public void saveOrUpdatePeerGroup(PeerGroup aPeerGroup) throws PeerGroupServiceException {

		DssProviderPeerGroup peerGroupToSave = null;

		// if it is a new peer group
		if (aPeerGroup.isStateNew()) {

			// using new constructor to make the data entity as a detached entity.
			peerGroupToSave = new DssProviderPeerGroup();

			LOGGER.debug("PeerGroupRepositoryImpl -- saveOrUpdatePeerGroup(): Adding a new peer group.");

			// Get the next ID from the DB sequence, for some reason we gave up using sequence in this entity
			long nextID = peerGroupDAO.getNextValueFromDssProviderPeerGroupSeq();
			// we don't know the new id until this point
			DataUtilities.updateIDs4PeerGroups(aPeerGroup, nextID);
			// set year end date at this point instead of in web module
			DataUtilities.updateYearEndDate4PeerGroups(aPeerGroup, aPeerGroup.getYearEndDate());
			DataUtilities.updatePeerGroupsModifedMetadata(aPeerGroup);

			BEAN_MAPPER.map(aPeerGroup, peerGroupToSave);
		}

		// if it is an existing peer group
		else {
			LOGGER.debug("PeerGroupRepositoryImpl -- saveOrUpdatePeerGroup(): UPdating a peer group.");

			DssProviderPeerGroupPK pk = new DssProviderPeerGroupPK();
			pk.setProviderPeerGroupId(aPeerGroup.getPeerGroupID());
			pk.setYearEndDate(aPeerGroup.getYearEndDate());
			// do the search to make the data entity as an attached entity.
			peerGroupToSave = peerGroupDAO.getDssProviderPeerGroupByPK(pk);

			// before mapping update ids and year end dates to its children
			DataUtilities.updateIDs4PeerGroups(aPeerGroup, aPeerGroup.getPeerGroupID());
			DataUtilities.updateYearEndDate4PeerGroups(aPeerGroup, aPeerGroup.getYearEndDate());
			DataUtilities.updatePeerGroupsModifedMetadata(aPeerGroup);

			BEAN_MAPPER.map(aPeerGroup, peerGroupToSave);

			LOGGER.debug("PeerGroupRepositoryImpl -- savePeerGroup(): PeerGroup to save or update is"
					+ aPeerGroup.toString());
		}

		// Persist or Merge
		peerGroupDAO.saveOrUpdateDssProviderPeerGroup(peerGroupToSave);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PeerGroupRepository#getOtherDropDownsByTownCodeName(ca.medavie.nspp.
	 * audit.service.data.TownCriteriaSearchCriteria)
	 */
	@Override
	public Map<String, BigDecimal>[] getFilteredTownSCDropDowns(BaseSearchCriteria aSearchCriteria) {

		List<DssPostalCodeMappedEntity> allUniqueDssPostalCodes_Subs = new ArrayList<DssPostalCodeMappedEntity>();

		@SuppressWarnings("unchecked")
		SortedMap<String, BigDecimal>[] filteredDropDowns = new TreeMap[4];

		if (allUniqueDssPostalCodes == null || allUniqueDssPostalCodes.isEmpty()) {

			allUniqueDssPostalCodes = new ArrayList<DssPostalCodeMappedEntity>();
			allUniqueDssPostalCodes = peerGroupDAO.getAllUniqueDssPostalCodes();
		}

		if (aSearchCriteria instanceof TownCriteriaSearchCriteria) {

			final TownCriteriaSearchCriteria sc = (TownCriteriaSearchCriteria) aSearchCriteria;

			// JPA is not happy if you don't create new instance of DssPostalCodeMappedEntity
			for (int n = 0; n < allUniqueDssPostalCodes.size(); n++) {

				DssPostalCodeMappedEntity dpc = new DssPostalCodeMappedEntity();
				dpc.setCounty_name(((DssPostalCodeMappedEntity) allUniqueDssPostalCodes.get(n)).getCounty_name());
				dpc.setHealth_region_desc(((DssPostalCodeMappedEntity) allUniqueDssPostalCodes.get(n))
						.getHealth_region_desc());
				dpc.setId(((DssPostalCodeMappedEntity) allUniqueDssPostalCodes.get(n)).getId());
				dpc.setMunicipality_name(((DssPostalCodeMappedEntity) allUniqueDssPostalCodes.get(n))
						.getMunicipality_name());
				dpc.setTown_name(((DssPostalCodeMappedEntity) allUniqueDssPostalCodes.get(n)).getTown_name());

				allUniqueDssPostalCodes_Subs.add(n, dpc);
			}

			SortedMap<String, BigDecimal> filteredTownCodeNameDropDown = new TreeMap<String, BigDecimal>();
			SortedMap<String, BigDecimal> filteredCountyCodeNameDropDown = new TreeMap<String, BigDecimal>();
			SortedMap<String, BigDecimal> filteredMunicipalityCodeNameDropDown = new TreeMap<String, BigDecimal>();
			SortedMap<String, BigDecimal> filteredHealthRegionCodeNameDropDown = new TreeMap<String, BigDecimal>();

			SortedSet<String> townCodeNameSet = new TreeSet<String>();
			SortedSet<String> countyCodeNameSet = new TreeSet<String>();
			SortedSet<String> municipalityCodeNameSet = new TreeSet<String>();
			SortedSet<String> healthRegionCodeNameSet = new TreeSet<String>();

			CollectionUtils.filter(allUniqueDssPostalCodes_Subs, new Predicate<DssPostalCodeMappedEntity>() {
				@Override
				public boolean evaluate(DssPostalCodeMappedEntity dpc) {

					// 14 conditions applied
					// if nothing selected
					if (!sc.isTownCodeNameSelected() && !sc.isCountyCodeNameSelected()
							&& !sc.isMunicipalityCodeNameSelected() && !sc.isHealthRegionCodeNameSelected()) {

						return true;

					}

					// if only towncodeName is selected filter by town code name
					if (sc.isTownCodeNameSelected() && !sc.isCountyCodeNameSelected()
							&& !sc.isMunicipalityCodeNameSelected() && !sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getTown_code().compareTo(sc.getTownCode()) == 0
								&& dpc.getTown_name().equals(sc.getTownName()) ? true : false);
					}

					// if only county name code is selected filter by county code name
					if (!sc.isTownCodeNameSelected() && sc.isCountyCodeNameSelected()
							&& !sc.isMunicipalityCodeNameSelected() && !sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getCounty_code().compareTo(sc.getCountyCode()) == 0
								&& dpc.getCounty_name().equals(sc.getCountyName()) ? true : false);
					}

					// if only municipality name code is selected filter by municipality code name
					if (!sc.isTownCodeNameSelected() && !sc.isCountyCodeNameSelected()
							&& sc.isMunicipalityCodeNameSelected() && !sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getMunicipality_code().compareTo(sc.getMunicipalityCode()) == 0
								&& dpc.getMunicipality_name().equals(sc.getMunicipalityName()) ? true : false);
					}

					// if only health Region name code is selected filter by health Region code name
					if (!sc.isTownCodeNameSelected() && !sc.isCountyCodeNameSelected()
							&& !sc.isMunicipalityCodeNameSelected() && sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getHealth_region_code().compareTo(sc.getHealthRegionCode()) == 0
								&& dpc.getHealth_region_desc().equals(sc.getHealthRegionDesc()) ? true : false);
					}

					// if town name code and county code name is selected filter by them
					if (sc.isTownCodeNameSelected() && sc.isCountyCodeNameSelected()
							&& !sc.isMunicipalityCodeNameSelected() && !sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getTown_code().compareTo(sc.getTownCode()) == 0
								&& dpc.getTown_name().equals(sc.getTownName())
								&& dpc.getId().getCounty_code().compareTo(sc.getCountyCode()) == 0
								&& dpc.getCounty_name().equals(sc.getCountyName()) ? true : false);
					}

					// if town name code and municipality code name is selected filter by them
					if (sc.isTownCodeNameSelected() && !sc.isCountyCodeNameSelected()
							&& sc.isMunicipalityCodeNameSelected() && !sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getTown_code().compareTo(sc.getTownCode()) == 0
								&& dpc.getTown_name().equals(sc.getTownName())
								&& dpc.getId().getMunicipality_code().compareTo(sc.getMunicipalityCode()) == 0
								&& dpc.getMunicipality_name().equals(sc.getMunicipalityName()) ? true : false);
					}

					// if town name code and health region code name is selected filter by them
					if (sc.isTownCodeNameSelected() && !sc.isCountyCodeNameSelected()
							&& !sc.isMunicipalityCodeNameSelected() && sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getTown_code().compareTo(sc.getTownCode()) == 0
								&& dpc.getTown_name().equals(sc.getTownName())
								&& dpc.getId().getHealth_region_code().compareTo(sc.getHealthRegionCode()) == 0
								&& dpc.getHealth_region_desc().equals(sc.getHealthRegionDesc()) ? true : false);
					}

					// if county name code and municipality code name is selected filter by them
					if (!sc.isTownCodeNameSelected() && sc.isCountyCodeNameSelected()
							&& sc.isMunicipalityCodeNameSelected() && !sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getMunicipality_code().compareTo(sc.getMunicipalityCode()) == 0
								&& dpc.getMunicipality_name().equals(sc.getMunicipalityName())
								&& dpc.getId().getCounty_code().compareTo(sc.getCountyCode()) == 0
								&& dpc.getCounty_name().equals(sc.getCountyName()) ? true : false);
					}

					// if county name code and Health region is selected filter by them
					if (!sc.isTownCodeNameSelected() && sc.isCountyCodeNameSelected()
							&& !sc.isMunicipalityCodeNameSelected() && sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getHealth_region_code().compareTo(sc.getHealthRegionCode()) == 0
								&& dpc.getHealth_region_desc().equals(sc.getHealthRegionDesc())
								&& dpc.getId().getCounty_code().compareTo(sc.getCountyCode()) == 0
								&& dpc.getCounty_name().equals(sc.getCountyName()) ? true : false);
					}

					// if municipality name code and Health region is selected filter by them
					if (!sc.isTownCodeNameSelected() && !sc.isCountyCodeNameSelected()
							&& sc.isMunicipalityCodeNameSelected() && sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getHealth_region_code().compareTo(sc.getHealthRegionCode()) == 0
								&& dpc.getHealth_region_desc().equals(sc.getHealthRegionDesc())
								&& dpc.getId().getMunicipality_code().compareTo(sc.getMunicipalityCode()) == 0
								&& dpc.getMunicipality_name().equals(sc.getMunicipalityName()) ? true : false);
					}

					// if town, county and municipality selected, filter by them
					if (sc.isTownCodeNameSelected() && sc.isCountyCodeNameSelected()
							&& sc.isMunicipalityCodeNameSelected() && !sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getTown_code().compareTo(sc.getTownCode()) == 0
								&& dpc.getTown_name().equals(sc.getTownName())
								&& dpc.getId().getCounty_code().compareTo(sc.getCountyCode()) == 0
								&& dpc.getCounty_name().equals(sc.getCountyName())
								&& dpc.getId().getMunicipality_code().compareTo(sc.getMunicipalityCode()) == 0
								&& dpc.getMunicipality_name().equals(sc.getMunicipalityName()) ? true : false);
					}

					// if town, county and health region selected, filter by them
					if (sc.isTownCodeNameSelected() && sc.isCountyCodeNameSelected()
							&& !sc.isMunicipalityCodeNameSelected() && sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getTown_code().compareTo(sc.getTownCode()) == 0
								&& dpc.getTown_name().equals(sc.getTownName())
								&& dpc.getId().getCounty_code().compareTo(sc.getCountyCode()) == 0
								&& dpc.getCounty_name().equals(sc.getCountyName())
								&& dpc.getId().getHealth_region_code().compareTo(sc.getHealthRegionCode()) == 0
								&& dpc.getHealth_region_desc().equals(sc.getHealthRegionDesc()) ? true : false);
					}

					// if town, municipality and health region selected, filter by them
					if (sc.isTownCodeNameSelected() && !sc.isCountyCodeNameSelected()
							&& sc.isMunicipalityCodeNameSelected() && sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getTown_code().compareTo(sc.getTownCode()) == 0
								&& dpc.getTown_name().equals(sc.getTownName())
								&& dpc.getId().getHealth_region_code().compareTo(sc.getHealthRegionCode()) == 0
								&& dpc.getHealth_region_desc().equals(sc.getHealthRegionDesc())
								&& dpc.getId().getMunicipality_code().compareTo(sc.getMunicipalityCode()) == 0
								&& dpc.getMunicipality_name().equals(sc.getMunicipalityName()) ? true : false);
					}

					// if county, municipality and health region selected, filter by them
					if (!sc.isTownCodeNameSelected() && sc.isCountyCodeNameSelected()
							&& sc.isMunicipalityCodeNameSelected() && sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getMunicipality_code().compareTo(sc.getMunicipalityCode()) == 0
								&& dpc.getMunicipality_name().equals(sc.getMunicipalityName())
								&& dpc.getId().getCounty_code().compareTo(sc.getCountyCode()) == 0
								&& dpc.getCounty_name().equals(sc.getCountyName())
								&& dpc.getId().getHealth_region_code().compareTo(sc.getHealthRegionCode()) == 0
								&& dpc.getHealth_region_desc().equals(sc.getHealthRegionDesc()) ? true : false);
					}

					// if town, county, municipality and health region selected, filter by them
					if (sc.isTownCodeNameSelected() && sc.isCountyCodeNameSelected()
							&& sc.isMunicipalityCodeNameSelected() && sc.isHealthRegionCodeNameSelected()) {

						return (dpc.getId().getTown_code().compareTo(sc.getTownCode()) == 0
								&& dpc.getTown_name().equals(sc.getTownName())
								&& dpc.getId().getMunicipality_code().compareTo(sc.getMunicipalityCode()) == 0
								&& dpc.getMunicipality_name().equals(sc.getMunicipalityName())
								&& dpc.getId().getCounty_code().compareTo(sc.getCountyCode()) == 0
								&& dpc.getCounty_name().equals(sc.getCountyName())
								&& dpc.getId().getHealth_region_code().compareTo(sc.getHealthRegionCode()) == 0
								&& dpc.getHealth_region_desc().equals(sc.getHealthRegionDesc()) ? true : false);
					}
					return false;
				}
			});

			// get filtered drop downs
			for (DssPostalCodeMappedEntity dpc : allUniqueDssPostalCodes_Subs) {

				String townCodeName = dpc.getTown_name() + "-" + dpc.getId().getTown_code();
				String countyCodeName = dpc.getCounty_name() + "-" + dpc.getId().getCounty_code();
				String municipalityCodeName = dpc.getMunicipality_name() + "-" + dpc.getId().getMunicipality_code();
				String healthRegionCodeName = dpc.getHealth_region_desc() + "-" + dpc.getId().getHealth_region_code();

				townCodeNameSet.add(townCodeName);
				countyCodeNameSet.add(countyCodeName);
				municipalityCodeNameSet.add(municipalityCodeName);
				healthRegionCodeNameSet.add(healthRegionCodeName);
			}

			// set town code name drop down
			for (String tc : townCodeNameSet) {

				// Substring out the Town Code ID
				int subStringStart = (tc.lastIndexOf("-") + 1);
				int subStringEnd = tc.length();
				BigDecimal code = new BigDecimal(tc.substring(subStringStart, subStringEnd));

				// Add to map
				filteredTownCodeNameDropDown.put(tc, code);
			}
			// set county code name drop down
			for (String cc : countyCodeNameSet) {

				// Substring out the County Code ID
				int subStringStart = (cc.lastIndexOf("-") + 1);
				int subStringEnd = cc.length();
				BigDecimal code = new BigDecimal(cc.substring(subStringStart, subStringEnd));

				// Add to map
				filteredCountyCodeNameDropDown.put(cc, code);
			}

			// set municipality code name drop down
			for (String mc : municipalityCodeNameSet) {

				// Substring out the Municipality Code ID
				int subStringStart = (mc.lastIndexOf("-") + 1);
				int subStringEnd = mc.length();
				BigDecimal code = new BigDecimal(mc.substring(subStringStart, subStringEnd));

				// Add to map
				filteredMunicipalityCodeNameDropDown.put(mc, code);
			}

			// set health region code name drop down
			for (String hc : healthRegionCodeNameSet) {

				// Substring out the Region Code ID
				int subStringStart = (hc.lastIndexOf("-") + 1);
				int subStringEnd = hc.length();
				BigDecimal code = new BigDecimal(hc.substring(subStringStart, subStringEnd));

				// Add to map
				filteredHealthRegionCodeNameDropDown.put(hc, code);
			}

			filteredDropDowns[0] = filteredTownCodeNameDropDown;
			filteredDropDowns[1] = filteredCountyCodeNameDropDown;
			filteredDropDowns[2] = filteredMunicipalityCodeNameDropDown;
			filteredDropDowns[3] = filteredHealthRegionCodeNameDropDown;

			return filteredDropDowns;
		}

		return null;
	}

	@Override
	public List<TownCriteria> getTownCriteriaSearchResults(TownCriteriaSearchCriteria townSearchCriteria) {

		LOGGER.debug("PeerGroupRepositoryImpl -- getTownCriteriaSearchResults(): peerGroupSearchCriteria is"
				+ townSearchCriteria.toString());

		List<Object[]> dpcs = peerGroupDAO.getSearcheDssPostalCodeBySC(townSearchCriteria);

		List<TownCriteria> tcs = new ArrayList<TownCriteria>();

		for (Object[] dpc : dpcs) {

			TownCriteria tc = new TownCriteria();

			tc.setTownCode((BigDecimal) dpc[0]);
			tc.setTownName((String) dpc[1]);
			tc.setCountyCode((BigDecimal) dpc[2]);
			tc.setCountyName((String) dpc[3]);
			tc.setMunicipalityCode((BigDecimal) dpc[4]);
			tc.setMunicipalityName((String) dpc[5]);
			tc.setHealthRegionCode((BigDecimal) dpc[6]);
			tc.setHealthRegionDesc((String) dpc[7]);

			tcs.add(tc);
		}

		return tcs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.PeerGroupRepository#getHealthServiceEFDropDown()
	 */
	@Override
	public Map<String, String> getHealthServiceEFDropDown() {

		List<Date> effectiveFromDates = peerGroupDAO.getHealthServiceEFCodes();
		SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd");

		SortedMap<String, String> efs = new TreeMap<String, String>();

		for (Date ef : effectiveFromDates) {

			efs.put(formater.format(ef), formater.format(ef));

		}

		return efs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PeerGroupRepository#getHealthServiceCriteriaSearchResults(ca.medavie
	 * .nspp.audit.service.data.HealthServiceCriteriaSearchCriteria)
	 */
	@Override
	public List<HealthServiceCriteria> getHealthServiceCriteriaSearchResults(
			HealthServiceCriteriaSearchCriteria hscSearchCriteria) {

		LOGGER.debug("PeerGroupRepositoryImpl -- getHealthServiceCriteriaSearchResults(): hscSearchCriteria is"
				+ hscSearchCriteria.toString());

		List<HealthServiceCriteria> result = new ArrayList<HealthServiceCriteria>();
		List<DssHealthServiceMappedEntity> dataObjects = peerGroupDAO.getSearchedHealthServices(hscSearchCriteria);

		for (DssHealthServiceMappedEntity source : dataObjects) {

			HealthServiceCriteria hsc = new HealthServiceCriteria();
			BEAN_MAPPER.map(source, hsc);
			result.add(hsc);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PeerGroupRepository#getPeerGroupDetails(ca.medavie.nspp.audit.service
	 * .data.PeerGroup)
	 */
	@Override
	public PeerGroup getPeerGroupDetails(PeerGroup aPeerGroup) {
		LOGGER.debug("PeerGroupRepositoryImpl -- getPeerGroupDetails(): aPeerGroup is" + aPeerGroup.toString());

		PeerGroup target = new PeerGroup();

		DssProviderPeerGroupPK aPK = new DssProviderPeerGroupPK();
		aPK.setProviderPeerGroupId(aPeerGroup.getPeerGroupID());
		aPK.setYearEndDate(aPeerGroup.getYearEndDate());

		DssProviderPeerGroup source = peerGroupDAO.getDssProviderPeerGroupByPK(aPK);

		BEAN_MAPPER.map(source, target);

		// can't not grab the health code desc by jpa so, has to set health service description manually
		List<HealthServiceCriteria> hscs = target.getHealthServiceCriterias();
		for (HealthServiceCriteria hsc : hscs) {

			String serviceCode = hsc.getHealthServiceCode();
			String qualifier = hsc.getQualifier();

			List<String> desc = peerGroupDAO.getDssHealthServiceCodeDesc(serviceCode, qualifier);

			// Null (Empty) safety check
			if (!desc.isEmpty()) {
				hsc.setHealthServiceCodeDesc(desc.get(0));
			}
		}
		return target;
	}

	public List<DssPostalCodeMappedEntity> getAllUniqueDssPostalCodes() {
		return allUniqueDssPostalCodes;
	}

	public void setAllUniqueDssPostalCodes(List<DssPostalCodeMappedEntity> allUniqueDssPostalCodes) {
		this.allUniqueDssPostalCodes = allUniqueDssPostalCodes;
	}

}
