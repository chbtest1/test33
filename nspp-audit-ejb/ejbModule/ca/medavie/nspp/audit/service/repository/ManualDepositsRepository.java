package ca.medavie.nspp.audit.service.repository;

import java.util.List;

import ca.medavie.nspp.audit.service.ServicePersistenceException;
import ca.medavie.nspp.audit.service.data.BusinessArrangementCode;
import ca.medavie.nspp.audit.service.data.ManualDeposit;
import ca.medavie.nspp.audit.service.data.ManualDepositSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.ProviderGroup;
import ca.medavie.nspp.audit.service.data.ProviderGroupSearchCriteria;

/**
 * Interface of the Manual Deposits repository layer
 */
public interface ManualDepositsRepository {
	/**
	 * Retrieve list of business arrangements.
	 * 
	 * @param practitioner
	 * @param group
	 * @return
	 */
	public List<BusinessArrangementCode> getBusinessArrangements(Practitioner practitioner, ProviderGroup group);

	/**
	 * Retrieve list of business arrangements.
	 * 
	 * @param manualDeposit
	 * @return
	 */
	public List<BusinessArrangementCode> getBusinessArrangements(ManualDeposit manualDeposit);

	/**
	 * Return list of all manual deposits matching provider criteria.
	 * 
	 * @param aManualDepositSearchCriteria
	 * @param aPractitionerSearch
	 * @param startRow
	 * @param maxRows
	 * @return matching ManualDeposits
	 */
	public List<ManualDeposit> getSearchedManualDeposits(ManualDepositSearchCriteria aManualDepositSearchCriteria,
			boolean aPractitionerSearch, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an ManualDeposits search
	 * 
	 * @param aManualDepositSearchCriteria
	 * @param aPractitionerSearch
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedAuditCriteriaCount(ManualDepositSearchCriteria aManualDepositSearchCriteria,
			boolean aPractitionerSearch);

	/**
	 * Return manual deposit details for a given manual deposit.
	 * 
	 * @param aManualDeposit
	 * @return
	 */
	public ManualDeposit getManualDepositDetails(ManualDeposit aManualDeposit);

	/**
	 * Save or update a given manual deposit.
	 * 
	 * @param selectedManualDeposit
	 * @return
	 */
	public void saveOrUpdateManualDeposit(ManualDeposit selectedManualDeposit);

	/**
	 * Delete a given manual deposit.
	 * 
	 * @param selectedManualDeposit
	 */
	public void deleteManualDeposit(ManualDeposit selectedManualDeposit);

	/**
	 * Retrieves next sequence number for deposit number.
	 * 
	 * @return
	 * @throws ServicePersistenceException
	 */
	public Long getNextValueFromDssDepositNumberSeq();

	/**
	 * Checks that the practitioner info in given manual deposit is valid for a practitioner in the database. Used when
	 * the payor is a group and the practitioner info is optional.
	 * 
	 * @param selectedManualDeposit
	 * @return
	 */
	public boolean verifyPractitioner(ManualDeposit selectedManualDeposit);

	/**
	 * Searches for practitioners when adding a manual deposit.
	 * 
	 * @param aPractitionerSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return
	 */
	public List<Practitioner> getManualDepositSearchedPractitioners(
			PractitionerSearchCriteria aPractitionerSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Practitioner search
	 * 
	 * @param practitionerSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getManualDepositSearchedPractitionersCount(PractitionerSearchCriteria practitionerSearchCriteria);

	/**
	 * Return list of all manual deposits matching provider criteria.
	 * 
	 * @param aManualDepositSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return
	 */
	public List<ProviderGroup> getSearchedProviderGroups(ProviderGroupSearchCriteria aProviderGroupSearchCriteria,
			int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Group search
	 * 
	 * @param aProviderGroupSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedProviderGroupsCount(ProviderGroupSearchCriteria aProviderGroupSearchCriteria);
}
