package ca.medavie.nspp.audit.service.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.persistence.EntityManager;

import ca.medavie.nspp.audit.service.dao.InquiryDAO;
import ca.medavie.nspp.audit.service.dao.InquiryDAOImpl;
import ca.medavie.nspp.audit.service.data.DssProviderType;
import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaTotals;
import ca.medavie.nspp.audit.service.data.PaymentDetails;
import ca.medavie.nspp.audit.service.data.PaymentInformation;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.ProfilePeriod;
import ca.medavie.nspp.audit.service.data.SubcategoryType;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPeerGroupHSCTotalsMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPractitionerHSCTotalsMappedEntity;

/**
 * Implementation of the Inquiry repository layer
 */
public class InquiryRepositoryImpl extends BaseRepository implements InquiryRepository {

	private InquiryDAO inquiryDAO;

	/**
	 * Constructor for Inquiry Repository
	 */
	public InquiryRepositoryImpl(EntityManager em) {
		inquiryDAO = new InquiryDAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.InquiryRepository#getSubCategoryTypeDropDown()
	 */
	@Override
	public Map<String, SubcategoryType> getSubCategoryTypeDropDown() {

		List<DssProviderType> types = inquiryDAO.getDssProviderTypes();
		SortedMap<String, SubcategoryType> typeMap = new TreeMap<String, SubcategoryType>();

		for (DssProviderType type : types) {
			SubcategoryType aType = new SubcategoryType();
			aType.setLastModified(type.getLastModified());
			aType.setModifiedBy(type.getModifiedBy());
			aType.setTypeCode(type.getProviderType());
			aType.setTypeDesc(type.getProviderTypeDescription());

			// Add type to map
			typeMap.put(aType.getTypeDesc(), aType);
		}
		return typeMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.InquiryRepository#getSearchedPractitioners(ca.medavie.nspp.audit.service
	 * .data.PractitionerSearchCriteria, int, int)
	 */
	@Override
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria,
			int startRow, int maxRows) {

		List<Practitioner> practitioners = new ArrayList<Practitioner>();

		// Query for matching Practitioner records
		List<Object[]> results = inquiryDAO.getSearchedPractitioners(aPractitionerSearchCriteria, startRow, maxRows);

		// Convert DB entity to Business object
		for (Object[] resultRow : results) {
			Practitioner practitioner = new Practitioner();

			// Populate the PeerGroup details for the practitioner
			PeerGroup practitionerPeerGroup = new PeerGroup();
			practitionerPeerGroup.setPeerGroupID(((BigDecimal) resultRow[0]).longValue());
			practitionerPeerGroup.setPeerGroupName((String) resultRow[1]);
			practitionerPeerGroup.setYearEndDate((Date) resultRow[5]);
			practitionerPeerGroup.setShadowBillingIndicator((String) resultRow[7]);
			// Construct profile period object
			ProfilePeriod profilePeriod = new ProfilePeriod();
			profilePeriod.setPeriodId(((BigDecimal) resultRow[6]).longValue());
			practitionerPeerGroup.setProfilePeriod(profilePeriod);
			practitioner.setPeerGroup(practitionerPeerGroup);

			// Populate remaining Practitioner details
			practitioner.setPractitionerNumber(((BigDecimal) resultRow[2]).longValue());
			practitioner.setPractitionerType((String) resultRow[3]);
			practitioner.setPractitionerName((String) resultRow[4]);
			practitioner.setShadowBillingIndicator((String) resultRow[7]);
			practitioner.setBirthDate((Date) resultRow[8]);
			practitioner.setGender((String) resultRow[9]);
			practitioner.setAddressLine1((String) resultRow[10]);
			practitioner.setAddressLine2((String) resultRow[11]);
			practitioner.setCity((String) resultRow[12]);
			practitioner.setProvinceCode((String) resultRow[13]);
			practitioner.setCountry((String) resultRow[14]);
			practitioner.setPostalCode((String) resultRow[15]);
			practitioner.setNumberOfPatients(((BigDecimal) resultRow[16]).longValue());
			practitioner.setNumberOfServices(((BigDecimal) resultRow[17]).longValue());
			practitioner.setTotalUnits(((BigDecimal) resultRow[18]));
			practitioner.setTotalAmountPaid((BigDecimal) resultRow[19]);
			practitioner.setServicesPerPatient((BigDecimal) resultRow[20]);
			practitioner.setAmountPaidPerPatient((BigDecimal) resultRow[21]);

			// Add populated Practitioner to list of results
			practitioners.add(practitioner);
		}
		return practitioners;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.InquiryRepository#getSearchedPractitionersCount(ca.medavie.nspp.audit
	 * .service.data.PractitionerSearchCriteria)
	 */
	@Override
	public Integer getSearchedPractitionersCount(PractitionerSearchCriteria aPractitionerSearchCriteria) {
		return inquiryDAO.getSearchedPractitionersCount(aPractitionerSearchCriteria);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.InquiryRepository#getPractitionerDetails(ca.medavie.nspp.audit.service
	 * .data.Practitioner)
	 */
	@Override
	public Practitioner getPractitionerDetails(Practitioner aPractitioner) {

		// *****************************************
		// ***** Step 1 - Get Opt In/Out dates *****
		// *****************************************
		Object[] optDates = inquiryDAO.getOptDates(aPractitioner.getPractitionerNumber(),
				aPractitioner.getPractitionerType());

		aPractitioner.setOptInDate((Date) optDates[0]);
		aPractitioner.setOptOutDate((Date) optDates[1]);

		// ********************************************************************
		// ***** Step 2 - Get ProfilePeriod details for this Practitioner *****
		// ********************************************************************

		// If the profileID is '999999' then search for details by YearEnd.
		// If the profileId is not '999999' then search for details by PeriodID
		if (aPractitioner.getPeerGroup().getProfilePeriod().getPeriodId().compareTo(Long.valueOf("999999")) == 0) {
			Object[] periodDetails = inquiryDAO.getProfilePeriodDetailsByYearEnd(aPractitioner.getPeerGroup()
					.getYearEndDate());

			aPractitioner.getPeerGroup().getProfilePeriod().setPeriodStartDate((Date) periodDetails[0]);
			aPractitioner.getPeerGroup().getProfilePeriod().setPeriodEndDate((Date) periodDetails[1]);
			aPractitioner.getPeerGroup().getProfilePeriod()
					.setMinimumPeriodId(((BigDecimal) periodDetails[2]).longValue());
			aPractitioner.getPeerGroup().getProfilePeriod()
					.setMaximumPeriodId(((BigDecimal) periodDetails[3]).longValue());
		} else {
			Object[] periodDetails = inquiryDAO.getProfilePeriodDetailsByPeriodID(aPractitioner.getPeerGroup()
					.getProfilePeriod().getPeriodId());

			Long periodID = aPractitioner.getPeerGroup().getProfilePeriod().getPeriodId();

			aPractitioner.getPeerGroup().getProfilePeriod().setPeriodStartDate((Date) periodDetails[0]);
			aPractitioner.getPeerGroup().getProfilePeriod().setPeriodEndDate((Date) periodDetails[1]);
			aPractitioner.getPeerGroup().getProfilePeriod().setMinimumPeriodId(periodID);
			aPractitioner.getPeerGroup().getProfilePeriod().setMaximumPeriodId(periodID);
		}

		// *************************************************************************************
		// ***** Step 3 - Get Practitioner counts for the PeerGroup the practitioner is in *****
		// *************************************************************************************

		// First time with shad indicator as N to load practitioners users for average
		Object[] practitionerAverageCounts = inquiryDAO.getPractitionerCounts(aPractitioner.getPeerGroup()
				.getPeerGroupID(), aPractitioner.getPeerGroup().getYearEndDate(), "N");

		if (practitionerAverageCounts[1] != null) {
			aPractitioner.getPeerGroup().setNumberOfPractitionersUsedForAverage(
					((BigDecimal) practitionerAverageCounts[1]).longValue());
		} else {
			aPractitioner.getPeerGroup().setNumberOfPractitionersUsedForAverage(Long.valueOf("0"));
		}

		// Second time with true shad indicator to get providers in group, providers dropped
		Object[] practitionerCounts = inquiryDAO.getPractitionerCounts(aPractitioner.getPeerGroup().getPeerGroupID(),
				aPractitioner.getPeerGroup().getYearEndDate(), aPractitioner.getShadowBillingIndicator());

		aPractitioner.getPeerGroup().setNumberOfPractitionersInGroup(((BigDecimal) practitionerCounts[0]).longValue());

		if (practitionerCounts[1] != null) {
			aPractitioner.getPeerGroup().setNumberOfPractitionersUsedForAvgCalc(
					((BigDecimal) practitionerCounts[1]).longValue());
		} else {
			aPractitioner.getPeerGroup().setNumberOfPractitionersUsedForAvgCalc(Long.valueOf("0"));
		}

		if (practitionerCounts[2] != null) {
			aPractitioner.getPeerGroup().setNumberOfDroppedPractitionersInGroup(
					((BigDecimal) practitionerCounts[2]).longValue());
		} else {
			aPractitioner.getPeerGroup().setNumberOfDroppedPractitionersInGroup(Long.valueOf("0"));
		}

		// *********************************************************************
		// ***** Step 4 - Get the Payment Information for the Practitioner *****
		// *********************************************************************
		calculatePractitionerPaymentInformation(aPractitioner);

		// ******************************************************************************
		// ***** Step 5 - Get the Age Distribution Information for the Practitioner *****
		// ******************************************************************************
		inquiryDAO.getPractitionerAgeDistributionDetails(aPractitioner);

		// *********************************************************************************
		// ***** Step 6 - Get the Health Service Code Information for the Practitioner *****
		// *********************************************************************************
		List<Object[]> healthServicesResult = inquiryDAO.getPractitionerHealthServiceCodeDetails(aPractitioner);

		List<HealthServiceCode> healthServiceCodes = new ArrayList<HealthServiceCode>();
		for (Object[] hsRow : healthServicesResult) {

			HealthServiceCode hsc = new HealthServiceCode();
			// Base Info Of HS
			hsc.setHealthServiceGroupName((String) hsRow[0]);
			hsc.setHealthServiceGroupId((BigDecimal) hsRow[1]);
			// Patient Detail
			hsc.setNumberOfPatients((BigDecimal) hsRow[2]);
			hsc.setNumberOfPatientsIndex((BigDecimal) hsRow[10]);
			// Service Detail
			hsc.setNumberOfServices((BigDecimal) hsRow[3]);
			hsc.setNumberOfServicesIndex((BigDecimal) hsRow[11]);
			// Payment Detail
			hsc.setTotalAmountPaid((BigDecimal) hsRow[4]);
			hsc.setTotalAmountPaidIndex((BigDecimal) hsRow[12]);
			// % Payment Detail
			hsc.setPercentagePayment((BigDecimal) hsRow[15]);
			hsc.setPercentagePaymentIndex((BigDecimal) hsRow[5]);
			// Service Per Patient Detail
			hsc.setServicesPerPatient((BigDecimal) hsRow[6]);
			hsc.setServicesPerPatientIndex((BigDecimal) hsRow[13]);
			// Paid Per Patient Detail
			hsc.setAmountPaidPerPatient((BigDecimal) hsRow[7]);
			hsc.setAmountPaidPerPatientIndex((BigDecimal) hsRow[14]);
			// Services Per 100 Patient Detail
			hsc.setNumberOfServicesPer100((BigDecimal) hsRow[16]);
			hsc.setNumberOfServicesPer100Index((BigDecimal) hsRow[8]);
			// Paid Per 100 Patient Detail
			hsc.setAmountPaidPer100((BigDecimal) hsRow[17]);
			hsc.setAmountPaidPer100Index((BigDecimal) hsRow[9]);

			// Add HSC to list
			healthServiceCodes.add(hsc);
		}
		aPractitioner.setHealthServiceCodes(healthServiceCodes);

		// ****************************************************************************
		// ***** Step 7 - Get the Health Service Code totals for the Practitioner *****
		// ****************************************************************************
		DssPractitionerHSCTotalsMappedEntity source = inquiryDAO.getPractitionerHealthServiceCodeTotals(aPractitioner);

		// Map the results to our BO object
		HealthServiceCriteriaTotals totals = new HealthServiceCriteriaTotals();

		// Failsafe check if no totals object was found.
		if (source != null) {
			BEAN_MAPPER.map(source, totals);

			/**
			 * Percentage Payment & Percentage Payment Group is always 100.
			 */
			totals.setPercentPaymentsPractitionerTotal(BigDecimal.valueOf(100));
			totals.setPercentPaymentsIndexTotal(BigDecimal.valueOf(100));

			/**
			 * Number of Services Per 100 = number_of_se_per_patient * 100
			 */
			totals.setServicesPer100PractitionerTotal(totals.getServicesPerPractitionerTotal().multiply(
					BigDecimal.valueOf(100)));

			/**
			 * Amount Paid Per 100 = amount_paid_per_patient * 100
			 */
			totals.setPaidPer100PractitionerTotal(totals.getPaidPerPatientPractitionerTotal().multiply(
					BigDecimal.valueOf(100)));

			// Apply totals to Practitioner
			aPractitioner.setHealthServiceCriteriaTotals(totals);
		}

		return aPractitioner;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.InquiryRepository#getSearchPeerGroups(ca.medavie.nspp.audit.service.
	 * data.PeerGroupSearchCriteria, int, int)
	 */
	@Override
	public List<PeerGroup> getSearchPeerGroups(PeerGroupSearchCriteria aPeerGroupSearchCriteria, int startRow,
			int maxRows) {

		List<PeerGroup> peerGroups = new ArrayList<PeerGroup>();

		// Load matching PeerGroups
		List<Object[]> results = inquiryDAO.getSearchedPeerGroups(aPeerGroupSearchCriteria, startRow, maxRows);

		for (Object[] row : results) {
			PeerGroup pg = new PeerGroup();
			pg.setPeerGroupID(((BigDecimal) row[0]).longValue());
			pg.setPeerGroupName((String) row[1]);
			pg.setYearEndDate((Date) row[2]);

			// Create Profile Period shell object
			ProfilePeriod profilePeriod = new ProfilePeriod();
			profilePeriod.setPeriodId(((BigDecimal) row[3]).longValue());
			pg.setProfilePeriod(profilePeriod);

			pg.setShadowBillingIndicator((String) row[4]);
			pg.setNumberOfPatientsInGroup(((BigDecimal) row[5]).longValue());
			pg.setNumberOfServices(((BigDecimal) row[6]).longValue());
			pg.setTotalUnits((BigDecimal) row[7]);
			pg.setTotalAmountPaid((BigDecimal) row[8]);
			pg.setServicesPerPatient((BigDecimal) row[9]);
			pg.setAmountPaidPerPatient((BigDecimal) row[10]);

			peerGroups.add(pg);
		}
		return peerGroups;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.InquiryRepository#getSearchPeerGroupsCount(ca.medavie.nspp.audit.service
	 * .data.PeerGroupSearchCriteria)
	 */
	@Override
	public Integer getSearchPeerGroupsCount(PeerGroupSearchCriteria aPeerGroupSearchCriteria) {
		return inquiryDAO.getSearchPeerGroupsCount(aPeerGroupSearchCriteria);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.InquiryRepository#getPeerGroupDetails(ca.medavie.nspp.audit.service.
	 * data.PeerGroup)
	 */
	@Override
	public PeerGroup getPeerGroupDetails(PeerGroup aPeerGroup) {

		// *****************************************************
		// ***** Step 1 - Load Health Service Code Details *****
		// *****************************************************
		inquiryDAO.getPeerGroupHealthServiceCodeDetails(aPeerGroup);

		// *****************************************************************
		// ***** Step 2 - Get ProfilePeriod details for this PeerGroup *****
		// *****************************************************************

		// If the profileID is '999999' then search for details by YearEnd.
		// If the profileId is not '999999' then search for details by PeriodID
		if (aPeerGroup.getProfilePeriod().getPeriodId().compareTo(Long.valueOf("999999")) == 0) {
			Object[] periodDetails = inquiryDAO.getProfilePeriodDetailsByYearEnd(aPeerGroup.getYearEndDate());

			aPeerGroup.getProfilePeriod().setPeriodStartDate((Date) periodDetails[0]);
			aPeerGroup.getProfilePeriod().setPeriodEndDate((Date) periodDetails[1]);
			aPeerGroup.getProfilePeriod().setMinimumPeriodId(((BigDecimal) periodDetails[2]).longValue());
			aPeerGroup.getProfilePeriod().setMaximumPeriodId(((BigDecimal) periodDetails[3]).longValue());
		} else {
			Object[] periodDetails = inquiryDAO.getProfilePeriodDetailsByPeriodID(aPeerGroup.getProfilePeriod()
					.getPeriodId());

			Long periodID = aPeerGroup.getProfilePeriod().getPeriodId();

			aPeerGroup.getProfilePeriod().setPeriodStartDate((Date) periodDetails[0]);
			aPeerGroup.getProfilePeriod().setPeriodEndDate((Date) periodDetails[1]);
			aPeerGroup.getProfilePeriod().setMinimumPeriodId(periodID);
			aPeerGroup.getProfilePeriod().setMaximumPeriodId(periodID);
		}

		// ***********************************************************************
		// ***** Step 3 - Get Practitioner counts for the selected PeerGroup *****
		// **** *******************************************************************

		// First time with Shad indicator as N to load practitioner users for average
		Object[] practitionerAverageCounts = inquiryDAO.getPractitionerCounts(aPeerGroup.getPeerGroupID(),
				aPeerGroup.getYearEndDate(), "N");

		if (practitionerAverageCounts[1] != null) {
			aPeerGroup.setNumberOfPractitionersUsedForAverage(((BigDecimal) practitionerAverageCounts[1]).longValue());
		} else {
			aPeerGroup.setNumberOfPractitionersUsedForAverage(Long.valueOf("0"));
		}

		// Second time with true shad indicator to get providers in group, providers dropped
		Object[] practitionerCounts = inquiryDAO.getPractitionerCounts(aPeerGroup.getPeerGroupID(),
				aPeerGroup.getYearEndDate(), aPeerGroup.getShadowBillingIndicator());

		aPeerGroup.setNumberOfPractitionersInGroup(((BigDecimal) practitionerCounts[0]).longValue());

		if (practitionerCounts[1] != null) {
			aPeerGroup.setNumberOfPractitionersUsedForAvgCalc(((BigDecimal) practitionerCounts[1]).longValue());
		} else {
			aPeerGroup.setNumberOfPractitionersUsedForAvgCalc(Long.valueOf("0"));
		}

		if (practitionerCounts[2] != null) {
			aPeerGroup.setNumberOfDroppedPractitionersInGroup(((BigDecimal) practitionerCounts[2]).longValue());
		} else {
			aPeerGroup.setNumberOfDroppedPractitionersInGroup(Long.valueOf("0"));
		}

		// **************************************************
		// ***** Step 4 - Calculate Payment Information *****
		// **************************************************
		calculatePeerGroupPaymentInformation(aPeerGroup);

		// **************************************************************************
		// ***** Step 5 - Get the Health Service Code totals for the Peer Group *****
		// **************************************************************************
		DssPeerGroupHSCTotalsMappedEntity source = inquiryDAO.getPeerGroupHealthServiceCodeTotals(aPeerGroup);

		// Map the results to our BO object
		HealthServiceCriteriaTotals totals = new HealthServiceCriteriaTotals();

		// Failsafe check if no totals object was found.
		if (source != null) {
			BEAN_MAPPER.map(source, totals);

			// Apply totals to the PeerGroup
			aPeerGroup.setHealthServiceCriteriaTotals(totals);
		}
		return aPeerGroup;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.InquiryRepository#getHealthServiceDrillDownDetails(ca.medavie.nspp.audit
	 * .service.data.Practitioner, ca.medavie.nspp.audit.service.data.HealthServiceCode)
	 */
	@Override
	public List<HealthServiceCode> getHealthServiceDrillDownDetails(Practitioner aPractitioner,
			HealthServiceCode aHealthServiceCode) {
		// Load HSC details
		List<Object[]> result = inquiryDAO.getHealthServiceDrillDownDetails(aPractitioner, aHealthServiceCode);

		List<HealthServiceCode> hscDrilldown = new ArrayList<HealthServiceCode>();

		for (Object[] row : result) {
			HealthServiceCode hsc = new HealthServiceCode();

			hsc.setNumberOfPatients((BigDecimal) row[0]);
			hsc.setNumberOfServices((BigDecimal) row[1]);
			hsc.setTotalUnits((BigDecimal) row[2]);
			hsc.setTotalAmountPaid((BigDecimal) row[3]);
			hsc.setNumberOfServicesPer100((BigDecimal) row[4]);
			hsc.setAmountPaidPer100((BigDecimal) row[5]);
			hsc.setEffectiveFrom((Date) row[6]);
			hsc.setEffectiveTo((Date) row[7]);
			hsc.setDescription((String) row[8]);
			hsc.setHealthServiceCode((String) row[9]);
			hsc.setModifiers((String) row[10]);
			hsc.setImplicitModifiers((String) row[11]);

			hscDrilldown.add(hsc);
		}
		return hscDrilldown;
	}

	/**
	 * @param aPractitioner
	 */
	private void calculatePractitionerPaymentInformation(Practitioner aPractitioner) {

		// Holds all payment information
		PaymentInformation paymentInformation = new PaymentInformation();

		// Holds a running total for Practitioner
		BigDecimal runningTotalForPractitioner = new BigDecimal(0);
		// Holds a running total for Group
		BigDecimal runningTotalForGroup = new BigDecimal(0);

		// Load the required values for determining payment information
		PaymentDetails practitionerPaymentDetails = inquiryDAO.getPractitionerPaymentDetails(aPractitioner);
		PaymentDetails peerGroupPaymentDetails = inquiryDAO.getPeerGroupPaymentDetails(aPractitioner.getPeerGroup());

		// Set the number of patients in the group
		aPractitioner.getPeerGroup().setNumberOfPatientsInGroup(peerGroupPaymentDetails.getNumberOfPatients());

		// ***************************************************************************************************
		// ***** Step 1 - Extract number of Reimbursements and BottomLineAdjPayments from PaymentDetails *****
		// ***************************************************************************************************
		BigDecimal totalNumberOfReimbursments = practitionerPaymentDetails.getTotalNumberOfReimbursements();
		BigDecimal totalNumberOfBottomLineAdjPayments = practitionerPaymentDetails
				.getTotalNumberOfBottomLineAdjustments();

		// ***********************************************************
		// ***** Step 2 - Calculate Practitioner Fee For Service *****
		// ***********************************************************
		BigDecimal editNumber = aPractitioner.getTotalAmountPaidFFS();

		if (editNumber.compareTo(new BigDecimal("1")) == -1) {
			// Total amount paid appears to be less than 1. Need to select new value from DSS_MED_PROV_ACT_SUMMARY
			editNumber = inquiryDAO.getPractitionerTotalAmountPaid(aPractitioner);
		}
		paymentInformation.setPractitionerFeeForService(editNumber.setScale(2, BigDecimal.ROUND_HALF_UP));
		// Add to running practitioner total.
		runningTotalForPractitioner = runningTotalForPractitioner.add(editNumber);

		// *****************************************************
		// ***** Step 3 - Calculate Groups Fee For Service *****
		// *****************************************************
		paymentInformation.setGroupAverageFeeForService(peerGroupPaymentDetails.getTotalAmountPaidAverage());

		// Add to running group total
		runningTotalForGroup = runningTotalForGroup.add(peerGroupPaymentDetails.getTotalAmountPaidAverage());

		// ********************************************************************
		// ***** Step 4 - Calculate Practitioner Alternate Reimbursements *****
		// ********************************************************************
		BigDecimal decimalReimbursments = new BigDecimal("0");

		// Check that Sessional Pay is not null
		if (practitionerPaymentDetails.getSessionalPay() == null) {
			practitionerPaymentDetails.setSessionalPay(BigDecimal.ZERO);
		}
		// Check that Psychiatric Pay is not null
		if (practitionerPaymentDetails.getPsychiatricPay() == null) {
			practitionerPaymentDetails.setPsychiatricPay(BigDecimal.ZERO);
		}
		// Check that Salary Per Pay is not null
		if (practitionerPaymentDetails.getSalaryPerPay() == null) {
			practitionerPaymentDetails.setSalaryPerPay(BigDecimal.ZERO);
		}

		// Sum up for a sub total with 2 decimal places
		decimalReimbursments = decimalReimbursments.add(practitionerPaymentDetails.getSessionalPay().setScale(2,
				BigDecimal.ROUND_HALF_UP));
		decimalReimbursments = decimalReimbursments.add(practitionerPaymentDetails.getPsychiatricPay().setScale(2,
				BigDecimal.ROUND_HALF_UP));
		decimalReimbursments = decimalReimbursments.add(practitionerPaymentDetails.getSalaryPerPay().setScale(2,
				BigDecimal.ROUND_HALF_UP));

		BigDecimal altReimbursments = decimalReimbursments.setScale(2, BigDecimal.ROUND_HALF_UP);
		paymentInformation.setPractitionerAlternateReimbursement(altReimbursments);
		// Add to running practitioner total
		runningTotalForPractitioner = runningTotalForPractitioner.add(altReimbursments);

		// Reset variables for calculating Alternate Reimbursements so they can be used for Group
		decimalReimbursments = new BigDecimal(0);
		altReimbursments = new BigDecimal(0);

		// *************************************************************
		// ***** Step 5 - Calculate Group Alternate Reimbursements *****
		// *************************************************************

		// Check that Sessional Pay is not null
		if (peerGroupPaymentDetails.getSessionalPay() == null) {
			peerGroupPaymentDetails.setSessionalPay(BigDecimal.ZERO);
		}
		// Check that Psychiatric Pay is not null
		if (peerGroupPaymentDetails.getPsychiatricPay() == null) {
			peerGroupPaymentDetails.setPsychiatricPay(BigDecimal.ZERO);
		}
		// Check that Salary Per Pay is not null
		if (peerGroupPaymentDetails.getSalaryPerPay() == null) {
			peerGroupPaymentDetails.setSalaryPerPay(BigDecimal.ZERO);
		}

		// Sum up for a sub total with 2 decimal places
		decimalReimbursments = decimalReimbursments.add(peerGroupPaymentDetails.getSessionalPay().setScale(2,
				BigDecimal.ROUND_HALF_UP));
		decimalReimbursments = decimalReimbursments.add(peerGroupPaymentDetails.getPsychiatricPay().setScale(2,
				BigDecimal.ROUND_HALF_UP));
		decimalReimbursments = decimalReimbursments.add(peerGroupPaymentDetails.getSalaryPerPay().setScale(2,
				BigDecimal.ROUND_HALF_UP));

		altReimbursments = decimalReimbursments;

		// Determine final total for the Groups Alternate Reimbursements
		editNumber = new BigDecimal(0);
		// Avoid dividing by zero
		if (altReimbursments.compareTo(BigDecimal.ZERO) != 0) {
			editNumber = altReimbursments.divide(totalNumberOfReimbursments, 2, BigDecimal.ROUND_HALF_UP);
		}
		paymentInformation.setGroupAverageAlternateReimbursement(editNumber);
		// Add to running group total
		runningTotalForGroup = runningTotalForGroup.add(editNumber);

		// **************************************************************
		// ***** Step 6 - Calculate Practitioner Diagnostic Imaging *****
		// **************************************************************
		BigDecimal diagImagingTotal = new BigDecimal(0);

		// Check that Diagnostic Imaging is not null
		if (practitionerPaymentDetails.getDiagImaging() == null) {
			practitionerPaymentDetails.setDiagImaging(BigDecimal.ZERO);
		}

		diagImagingTotal = practitionerPaymentDetails.getDiagImaging().setScale(2, BigDecimal.ROUND_HALF_UP);
		paymentInformation.setPractitionerDiagnosticImage(diagImagingTotal);
		// Add to running Practitioner total
		runningTotalForPractitioner = runningTotalForPractitioner.add(diagImagingTotal);

		// Reset variable for calculating Diagnostic Imaging so they can be used for Group
		diagImagingTotal = BigDecimal.ZERO;

		// *******************************************************
		// ***** Step 7 - Calculate Group Diagnostic Imaging *****
		// *******************************************************
		// Check that Diagnostic Imaging is not null
		if (peerGroupPaymentDetails.getDiagImaging() == null) {
			peerGroupPaymentDetails.setDiagImaging(BigDecimal.ZERO);
		}

		diagImagingTotal = peerGroupPaymentDetails.getDiagImagingAverage().setScale(2, BigDecimal.ROUND_HALF_UP);
		paymentInformation.setGroupAverageDiagnosticImage(diagImagingTotal);
		// Add to running group total
		runningTotalForGroup = runningTotalForGroup.add(diagImagingTotal);

		// ******************************************************************
		// ***** Step 8 - Calculate Practitioner Bottom Line Adjustment *****
		// ******************************************************************
		BigDecimal bottomLineAdjDecimal = new BigDecimal(0);
		BigDecimal bottomLineAdjTotal = new BigDecimal(0);

		// Check that Bottom Line Adjustment is not null
		if (practitionerPaymentDetails.getBottomLineAdj() == null) {
			practitionerPaymentDetails.setBottomLineAdj(BigDecimal.ZERO);
		}
		// Check that Manual Deposits is not null
		if (practitionerPaymentDetails.getManualDeposits() == null) {
			practitionerPaymentDetails.setManualDeposits(BigDecimal.ZERO);
		}
		// Check that Cheque Reconciliation is not null
		if (practitionerPaymentDetails.getCheqReconciliation() == null) {
			practitionerPaymentDetails.setCheqReconciliation(BigDecimal.ZERO);
		}

		bottomLineAdjDecimal = bottomLineAdjDecimal.add(practitionerPaymentDetails.getBottomLineAdj().setScale(2,
				BigDecimal.ROUND_HALF_UP));
		// Subtract manual deposits
		bottomLineAdjDecimal = bottomLineAdjDecimal.subtract(practitionerPaymentDetails.getManualDeposits().setScale(2,
				BigDecimal.ROUND_HALF_UP));
		bottomLineAdjDecimal = bottomLineAdjDecimal.add(practitionerPaymentDetails.getCheqReconciliation().setScale(2,
				BigDecimal.ROUND_HALF_UP));

		bottomLineAdjTotal = bottomLineAdjDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
		paymentInformation.setPractitionerBottomLineAdj(bottomLineAdjTotal);
		// Add to running practitioner total
		runningTotalForPractitioner = runningTotalForPractitioner.add(bottomLineAdjTotal);

		// Reset variables for calculating Bottom Line Adjustment so they can be used for Group
		bottomLineAdjDecimal = BigDecimal.ZERO;
		bottomLineAdjTotal = BigDecimal.ZERO;

		// ***********************************************************
		// ***** Step 9 - Calculate Group Bottom Line Adjustment *****
		// ***********************************************************

		// Check that Bottom Line Adjustment is not null
		if (peerGroupPaymentDetails.getBottomLineAdj() == null) {
			peerGroupPaymentDetails.setBottomLineAdj(BigDecimal.ZERO);
		}
		// Check that Manual Deposits is not null
		if (peerGroupPaymentDetails.getManualDeposits() == null) {
			peerGroupPaymentDetails.setManualDeposits(BigDecimal.ZERO);
		}
		// Check that Cheque Reconciliation is not null
		if (peerGroupPaymentDetails.getCheqReconciliation() == null) {
			peerGroupPaymentDetails.setCheqReconciliation(BigDecimal.ZERO);
		}

		bottomLineAdjDecimal = bottomLineAdjDecimal.add(peerGroupPaymentDetails.getBottomLineAdj().setScale(2,
				BigDecimal.ROUND_HALF_UP));
		// Subtract manual deposits
		bottomLineAdjDecimal = bottomLineAdjDecimal.subtract(peerGroupPaymentDetails.getManualDeposits().setScale(2,
				BigDecimal.ROUND_HALF_UP));
		bottomLineAdjDecimal = bottomLineAdjDecimal.add(peerGroupPaymentDetails.getCheqReconciliation().setScale(2,
				BigDecimal.ROUND_HALF_UP));

		// Calculate the final Average Bottom Line Adjustment
		editNumber = BigDecimal.ZERO;
		// Avoid dividing by zero
		if (totalNumberOfBottomLineAdjPayments.compareTo(BigDecimal.ZERO) != 0) {
			editNumber = bottomLineAdjDecimal.divide(totalNumberOfBottomLineAdjPayments, 2, BigDecimal.ROUND_HALF_UP);
		}
		paymentInformation.setGroupAverageBottomLineAdj(editNumber);
		// Add to running Group total
		runningTotalForGroup = runningTotalForGroup.add(editNumber);

		// *********************************************************
		// ***** Step 10 - Apply totals to Payment Information *****
		// *********************************************************
		paymentInformation.setPractitionerTotalPayments(runningTotalForPractitioner);
		paymentInformation.setGroupAverageTotalPayments(runningTotalForGroup);

		// Set payment information to the Practitioner for UI display
		aPractitioner.setPaymentInformation(paymentInformation);
	}

	/**
	 * Calculates the payment information and applies the values to the provided PeerGroup
	 * 
	 * @param aPeerGroup
	 *            - the peerGroup used for payment calculations
	 */
	private void calculatePeerGroupPaymentInformation(PeerGroup aPeerGroup) {
		// Holds all payment information
		PaymentInformation paymentInformation = new PaymentInformation();

		// Holds the running totals of the payment values
		BigDecimal runningTotalOfGroup = new BigDecimal(0);
		BigDecimal runningAverageTotalOfGroup = new BigDecimal(0);

		// Load the required values for determining payment information
		PaymentDetails details = inquiryDAO.getPeerGroupPaymentDetails(aPeerGroup);

		// ***************************************************************************************************
		// ***** Step 1 - Extract number of Reimbursements and BottomLineAdjPayments from PaymentDetails *****
		// ***************************************************************************************************
		BigDecimal totalNumberOfReimbursments = details.getTotalNumberOfReimbursements();
		BigDecimal totalNumberOfBottomLineAdjPayments = details.getTotalNumberOfBottomLineAdjustments();

		// **********************************************
		// ***** Step 2 - Calculate Fee For Service *****
		// **********************************************
		BigDecimal editNumber = details.getTotalAmountPaid();
		if (editNumber == null) {
			editNumber = BigDecimal.ZERO;
		}
		paymentInformation.setGroupTotalFeeForService(editNumber.setScale(2, BigDecimal.ROUND_HALF_UP));
		// Add to running group total
		runningTotalOfGroup = runningTotalOfGroup.add(editNumber);

		// ******************************************************
		// ***** Step 3 - Calculate Average Fee For Service *****
		// ******************************************************
		editNumber = details.getTotalAmountPaidAverage();
		if (editNumber == null) {
			editNumber = BigDecimal.ZERO;
		}
		paymentInformation.setGroupAverageFeeForService(editNumber.setScale(2, BigDecimal.ROUND_HALF_UP));
		// Add to running group average total
		runningAverageTotalOfGroup = runningAverageTotalOfGroup.add(editNumber);

		// *******************************************************
		// ***** Step 4 - Calculate Alternate Reimbursements *****
		// *******************************************************
		BigDecimal totalDecimalReimbursements = new BigDecimal(0);
		BigDecimal totalReimbursements = new BigDecimal(0);

		editNumber = details.getSessionalPay();
		if (editNumber == null) {
			editNumber = BigDecimal.ZERO;
		}
		// Add Sessional Pay to reimbursement total
		totalDecimalReimbursements = totalDecimalReimbursements.add(editNumber.setScale(2, BigDecimal.ROUND_HALF_UP));

		editNumber = details.getPsychiatricPay();
		if (editNumber == null) {
			editNumber = BigDecimal.ZERO;
		}
		// Add Psychiatric Pay to reimbursement total
		totalDecimalReimbursements = totalDecimalReimbursements.add(editNumber.setScale(2, BigDecimal.ROUND_HALF_UP));

		editNumber = details.getSalaryPerPay();
		if (editNumber == null) {
			editNumber = BigDecimal.ZERO;
		}
		// Add Salary Per Pay to reimbursement total
		totalDecimalReimbursements = totalDecimalReimbursements.add(editNumber.setScale(2, BigDecimal.ROUND_HALF_UP));

		// Convert total to whole number
		totalReimbursements = totalDecimalReimbursements.setScale(2, BigDecimal.ROUND_HALF_UP);
		paymentInformation.setGroupTotalAlternateReimbursement(totalReimbursements);
		// Add to running group total
		runningTotalOfGroup = runningTotalOfGroup.add(totalReimbursements);

		// Clear out Edit number for division
		editNumber = BigDecimal.ZERO;

		// ***************************************************************
		// ***** Step 5 - Calculate Average Alternate Reimbursements *****
		// ***************************************************************
		if (totalNumberOfReimbursments.compareTo(BigDecimal.ZERO) != 0) {
			editNumber = totalDecimalReimbursements.divide(totalNumberOfReimbursments, 2, BigDecimal.ROUND_HALF_UP);
		}
		paymentInformation.setGroupAverageAlternateReimbursement(editNumber);
		// Add Average Alternate Reimbursements to running group average total
		runningAverageTotalOfGroup = runningAverageTotalOfGroup.add(editNumber);

		// *************************************************
		// ***** Step 6 - Calculate Diagnostic Imaging *****
		// *************************************************
		editNumber = details.getDiagImaging();
		if (editNumber == null) {
			editNumber = BigDecimal.ZERO;
		}
		paymentInformation.setGroupTotalDiagnosticImage(editNumber.setScale(2, BigDecimal.ROUND_HALF_UP));
		// Add to group running total
		runningTotalOfGroup = runningTotalOfGroup.add(editNumber);

		// *********************************************************
		// ***** Step 7 - Calculate Average Diagnostic Imaging *****
		// *********************************************************
		editNumber = details.getDiagImagingAverage();
		if (editNumber == null) {
			editNumber = BigDecimal.ZERO;
		}
		paymentInformation.setGroupAverageDiagnosticImage(editNumber.setScale(2, BigDecimal.ROUND_HALF_UP));
		// Add to group running average total
		runningAverageTotalOfGroup = runningAverageTotalOfGroup.add(editNumber);

		// ******************************************************
		// ***** Step 8 - Calculate Bottom Line Adjustments *****
		// ******************************************************
		BigDecimal totalDecimalBottomLineAdj = new BigDecimal(0);
		BigDecimal totalBottomLineAdj = new BigDecimal(0);

		editNumber = details.getBottomLineAdj();
		if (editNumber == null) {
			editNumber = BigDecimal.ZERO;
		}
		// Add BottomLineAdj to decimal total
		totalDecimalBottomLineAdj = totalDecimalBottomLineAdj.add(editNumber.setScale(2, BigDecimal.ROUND_HALF_UP));

		editNumber = details.getManualDeposits();
		if (editNumber == null) {
			editNumber = BigDecimal.ZERO;
		}
		// Subtract manual deposits from decimal total
		totalDecimalBottomLineAdj = totalDecimalBottomLineAdj
				.subtract(editNumber.setScale(2, BigDecimal.ROUND_HALF_UP));

		editNumber = details.getCheqReconciliation();
		if (editNumber == null) {
			editNumber = BigDecimal.ZERO;
		}
		// Add cheque recon to decimal total
		totalDecimalBottomLineAdj = totalDecimalBottomLineAdj.add(editNumber.setScale(2, BigDecimal.ROUND_HALF_UP));
		// Convert to whole number
		totalBottomLineAdj = totalDecimalBottomLineAdj.setScale(2, BigDecimal.ROUND_HALF_UP);
		paymentInformation.setGroupTotalBottomLineAdj(totalBottomLineAdj);
		// Add to group running total
		runningTotalOfGroup = runningTotalOfGroup.add(totalBottomLineAdj);

		// Clear out edit number for division
		editNumber = BigDecimal.ZERO;

		// **************************************************************
		// ***** Step 9 - Calculate Average Bottom Line Adjustments *****
		// **************************************************************
		if (totalNumberOfBottomLineAdjPayments.compareTo(BigDecimal.ZERO) != 0) {
			editNumber = totalDecimalBottomLineAdj.divide(totalNumberOfBottomLineAdjPayments, 2,
					BigDecimal.ROUND_HALF_UP);
		}
		paymentInformation.setGroupAverageBottomLineAdj(editNumber);
		// Add to group running average total
		runningAverageTotalOfGroup = runningAverageTotalOfGroup.add(editNumber);

		// ***************************************************************
		// ***** Step 10 - Apply grand totals to payment information *****
		// ***************************************************************
		paymentInformation.setGroupTotalPayments(runningTotalOfGroup);
		paymentInformation.setGroupAverageTotalPayments(runningAverageTotalOfGroup);

		// Set the payment information to the PeerGroup
		aPeerGroup.setPaymentInformation(paymentInformation);
	}
}