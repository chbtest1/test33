package ca.medavie.nspp.audit.service.repository;

import java.util.List;

import ca.medavie.nspp.audit.service.data.ChequeReconciliation;
import ca.medavie.nspp.audit.service.data.ChequeReconciliationSearchCriteria;

public interface DssChequeReconciliationRepository {

	/**
	 * Loads any Cheque Reconciliation records that match the search criteria
	 * 
	 * @param aChequeReconciliationSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return matching Cheque Reconciliation records
	 */
	public List<ChequeReconciliation> getChequeReconciliationSearchResults(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Cheque Reconciliation search
	 * 
	 * @param aChequeReconciliationSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getChequeReconciliationSearchResultsCount(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria);

	/**
	 * Saves any changes made to the ChequeReconciliation record
	 * 
	 * @param aChequeReconciliation
	 */
	public void saveChequeReconciliation(ChequeReconciliation aChequeReconciliation);
}
