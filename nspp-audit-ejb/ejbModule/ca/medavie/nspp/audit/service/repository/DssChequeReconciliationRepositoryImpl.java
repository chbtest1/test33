package ca.medavie.nspp.audit.service.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import ca.medavie.nspp.audit.service.dao.DssChequeReconciliationDAO;
import ca.medavie.nspp.audit.service.dao.DssChequeReconciliationDAOImpl;
import ca.medavie.nspp.audit.service.data.ChequeReconciliation;
import ca.medavie.nspp.audit.service.data.ChequeReconciliationSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssChequeReconciliationMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssChequeReconciliationMappedEntityPK;

public class DssChequeReconciliationRepositoryImpl extends BaseRepository implements DssChequeReconciliationRepository {

	private DssChequeReconciliationDAO dssChequeReconciliationDAO;

	public DssChequeReconciliationRepositoryImpl(EntityManager em) {
		dssChequeReconciliationDAO = new DssChequeReconciliationDAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssChequeReconciliationRepository#getChequeReconciliationSearchResults
	 * (ca.medavie.nspp.audit.service.data.ChequeReconciliationSearchCriteria, int, int)
	 */
	@Override
	public List<ChequeReconciliation> getChequeReconciliationSearchResults(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria, int startRow, int maxRows) {

		List<DssChequeReconciliationMappedEntity> sources = dssChequeReconciliationDAO
				.getChequeReconciliationSearchResults(aChequeReconciliationSearchCriteria, startRow, maxRows);

		List<ChequeReconciliation> result = new ArrayList<ChequeReconciliation>();

		for (DssChequeReconciliationMappedEntity source : sources) {
			ChequeReconciliation target = new ChequeReconciliation();
			BEAN_MAPPER.map(source, target);

			// get the payeeType value
			target.setPayeeType(dssChequeReconciliationDAO.getPayeeType(target.getPayeeTypeCode()));

			// Add to list
			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssChequeReconciliationRepository#getChequeReconciliationSearchResultsCount
	 * (ca.medavie.nspp.audit.service.data.ChequeReconciliationSearchCriteria)
	 */
	@Override
	public Integer getChequeReconciliationSearchResultsCount(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria) {
		return dssChequeReconciliationDAO
				.getChequeReconciliationSearchResultsCount(aChequeReconciliationSearchCriteria);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssChequeReconciliationRepository#saveChequeReconciliation(ca.medavie
	 * .nspp.audit.service.data.ChequeReconciliation)
	 */
	@Override
	public void saveChequeReconciliation(ChequeReconciliation aChequeReconciliation) {

		DssChequeReconciliationMappedEntity entity = new DssChequeReconciliationMappedEntity();
		entity.setId(new DssChequeReconciliationMappedEntityPK());

		// Map to entity object
		BEAN_MAPPER.map(aChequeReconciliation, entity);
		// Perform save.
		dssChequeReconciliationDAO.saveChequeReconciliation(entity);
	}
}
