package ca.medavie.nspp.audit.service.repository;

import java.util.List;
import java.util.Map;

import ca.medavie.nspp.audit.service.data.CodeEntry;
import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;
import ca.medavie.nspp.audit.service.data.SpecialtyCriteria;
import ca.medavie.nspp.audit.service.data.SubcategoryType;

public interface DropDownMenuItemRepository {

	/**
	 * @return
	 */
	public Map<String, SubcategoryType> getSubCategoryTypeDropDown();

	/**
	 * @return
	 */
	public Map<String, SpecialtyCriteria> getSpecialtyDropDown();

	/**
	 * @return
	 */
	public Map<String, CodeEntry> getAuditRXTypeDropDropDown();

	/**
	 * @return
	 */
	public Map<String, CodeEntry> getAuditNonRXTypeDropDropDown();

	/**
	 * @return list of contractTownNames
	 */
	public List<String> getContractTownNames();

	/**
	 * @return
	 */
	public Map<String, String> getQualifierDropDropDown();

	/**
	 * @return
	 */
	public Map<String, String> getAuditSourceMedCodeDropDown();

	/**
	 * @return
	 */
	public Map<String, String> getAuditSourcePharmCodeDropDown();

	/**
	 * @return
	 */
	public Map<String, String> getAuditPharmTypeCodeDropDown();

	/**
	 * @return
	 */
	public Map<String, String> getGroupTypeCodeDropDown();

	/**
	 * @return map of YearEndDates (String format)
	 */
	public Map<String, String> getPeriodYearEndDateDropdown();

	/**
	 * Get PeriodIds drop down menu items
	 * 
	 * @return map of PeriodIds
	 */
	public Map<Long, PractitionerProfilePeriod> getPeriodIdDropdown();

	/**
	 * @return DssUnitValueLevelDropdown
	 */
	public Map<Long, Long> getDssUnitValueLevelDropdown();

	/**
	 * @return
	 */
	public Map<String, Long> getDssUnitValueDescsDropdown();

	/**
	 * Get HealthService modifiers
	 * 
	 * @return HS modifiers
	 */
	public Map<String, String> getHsModifierDropDown();

	/**
	 * Get HealthService Implicit modifiers
	 * 
	 * @return HS Implicit modifiers
	 */
	public Map<String, String> getHsImplicitModifiersDropDown();

	/**
	 * Get HealthService Programs
	 * 
	 * @return HS Programs
	 */
	public Map<String, String> getHsProgramDropDown();

	/**
	 * Get HealthService Groups
	 * 
	 * @return HS Groups
	 */
	public Map<Long, String> getHsGroupDropDown();

	/**
	 * Get HealthService Qualifiers
	 * 
	 * @return HS Qualifiers
	 */
	public Map<String, String> getHsQualifiersDropDown();

	/**
	 * @return Denticare Audit type drop down map
	 */
	public Map<String, String> getDenticareAuditTypeDropDown();

	/**
	 * @return response status
	 */
	public Map<String, String> getResponseStatusDropDown();

	/**
	 * Get PayeeTypes
	 * 
	 * @return payeeTypes
	 */
	public Map<String, String> getPayeeTypeDropDown();

	/**
	 * Get Payment Responsibilities
	 * 
	 * @return Payment Responsibilities
	 */
	public Map<String, String> getPaymentResponsibilityDropDown();

	/**
	 * Get GL Numbers
	 * 
	 * @return
	 */
	public Map<String, String> getGlNumbers();

	/**
	 * Get Medicare Audit Types
	 * 
	 * @return medicare audit types drop down map
	 */
	public Map<String, String> getMedicareAuditTypeDropDown();

	/**
	 * @return pharmacare audit types drop down map
	 */
	public Map<String, String> getPharmacareAuditTypeDropDown();

	/**
	 * @return Audit Criteria types used for Medicare and Denticare
	 */
	public Map<String, String> getGeneralAuditCriteriaTypeDropDown();

	/**
	 * @return Audit Criteria Types used for Pharmacare
	 */
	public Map<String, String> getPharmacareAuditCriteriaTypeDropDown();

	/**
	 * @return Map containing Unique effective from data from dss health service table
	 */
	public Map<String, String> getHSEffectiveDateDropDown();
}
