package ca.medavie.nspp.audit.service.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO;
import ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAOImpl;
import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.HealthServiceCodeSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceGroup;
import ca.medavie.nspp.audit.service.data.HealthServiceGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceProgram;
import ca.medavie.nspp.audit.service.data.Modifier;

/**
 * Implementation of the Inquiry repository layer
 */
public class HealthServiceGroupRepositoryImpl implements HealthServiceGroupRepository {

	private HealthServiceGroupDAO healthServiceGroupDAO;

	public HealthServiceGroupRepositoryImpl(EntityManager em) {
		healthServiceGroupDAO = new HealthServiceGroupDAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#getSearchHealthServiceGroups(ca.medavie
	 * .nspp.audit.service.data.HealthServiceGroupSearchCriteria, int, int)
	 */
	@Override
	public List<HealthServiceGroup> getSearchHealthServiceGroups(HealthServiceGroupSearchCriteria aSearchCriteria,
			int startRow, int maxRows) {

		// Hold search results
		List<HealthServiceGroup> hsGroups = new ArrayList<HealthServiceGroup>();

		// Get matching records
		List<Object[]> results = healthServiceGroupDAO.getSearchHealthServiceGroups(aSearchCriteria, startRow, maxRows);

		// Convert results to HealthServiceGroup objects
		for (Object[] row : results) {
			HealthServiceGroup hsg = new HealthServiceGroup();
			hsg.setHealthServiceGroupId(((BigDecimal) row[0]).longValue());
			hsg.setHealthServiceGroupName((String) row[1]);
			hsg.setModifiedBy((String) row[2]);
			hsg.setLastModified((Date) row[3]);

			// Add group to list
			hsGroups.add(hsg);
		}

		return hsGroups;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#getSearchHealthServiceGroupsCount(ca.medavie
	 * .nspp.audit.service.data.HealthServiceGroupSearchCriteria)
	 */
	@Override
	public Integer getSearchHealthServiceGroupsCount(HealthServiceGroupSearchCriteria aSearchCriteria) {
		Integer count = healthServiceGroupDAO.getSearchHealthServiceGroupsCount(aSearchCriteria);
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#getSearchHealthServiceCodes(ca.medavie.
	 * nspp.audit.service.data.HealthServiceCodeSearchCriteria)
	 */
	@Override
	public List<HealthServiceCode> getSearchHealthServiceCodes(HealthServiceCodeSearchCriteria aSearchCriteria) {

		// Load matching HealthServiceCodes
		List<Object[]> result = healthServiceGroupDAO.getSearchHealthServiceCodes(aSearchCriteria);

		// Holds populated HSC objects
		List<HealthServiceCode> healthServiceCodes = new ArrayList<HealthServiceCode>();

		for (Object[] row : result) {
			HealthServiceCode hsc = new HealthServiceCode();
			hsc.setHealthServiceId((String) row[0]);
			hsc.setProgram((String) row[1]);
			hsc.setHealthServiceCode((String) row[2]);
			hsc.setQualifier((String) row[3]);
			hsc.setEffectiveFrom((Date) row[4]);
			hsc.setDescription((String) row[5]);
			hsc.setModifiers((String) row[6]);
			hsc.setImplicitModifiers((String) row[7]);
			hsc.setEffectiveTo((Date) row[8]);
			hsc.setCategory((String) row[9]);
			hsc.setUnitFormula((String) row[10]);

			healthServiceCodes.add(hsc);
		}
		return healthServiceCodes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#getHealthServiceCodes(java.lang.Long)
	 */
	@Override
	public List<HealthServiceCode> getHealthServiceCodes(Long aHealthServiceGroupId) {

		List<HealthServiceCode> healthServiceCodes = new ArrayList<HealthServiceCode>();

		// Results from DB
		List<Object[]> results = healthServiceGroupDAO.getHealthServiceCodes(aHealthServiceGroupId);

		// Convert DB data to HealthServiceCode objects
		for (Object[] row : results) {
			HealthServiceCode hsc = new HealthServiceCode();

			// populate the object
			hsc.setHealthServiceId((String) row[0]);
			hsc.setEffectiveFrom((Date) row[1]);
			hsc.setEffectiveTo((Date) row[2]);
			hsc.setModifiedBy((String) row[3]);
			hsc.setLastModified((Date) row[4]);
			hsc.setHealthServiceCode((String) row[5]);
			hsc.setDescription((String) row[6]);
			hsc.setModifiers((String) row[7]);
			hsc.setImplicitModifiers((String) row[8]);
			hsc.setProgram((String) row[9]);
			hsc.setCategory((String) row[10]);
			hsc.setUnitFormula((String) row[11]);
			hsc.setQualifier((String) row[12]);

			// Add to the list
			healthServiceCodes.add(hsc);
		}
		return healthServiceCodes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#saveHealthServiceGroup(ca.medavie.nspp.
	 * audit.service.data.HealthServiceGroup, java.util.List)
	 */
	@Override
	public void saveHealthServiceGroup(HealthServiceGroup aHealthServiceGroup, List<HealthServiceCode> aListOfHSCToAdd) {
		healthServiceGroupDAO.saveHealthServiceGroup(aHealthServiceGroup, aListOfHSCToAdd);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#saveNewHealthServiceGroup(ca.medavie.nspp
	 * .audit.service.data.HealthServiceGroup)
	 */
	@Override
	public void saveNewHealthServiceGroup(HealthServiceGroup aHealthServiceGroup) {
		healthServiceGroupDAO.saveNewHealthServiceGroup(aHealthServiceGroup);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#isHealthServiceGroupDuplicate(java.lang
	 * .Long)
	 */
	@Override
	public boolean isHealthServiceGroupDuplicate(Long aHealtherServiceGroupId) {
		return healthServiceGroupDAO.isHealthServiceGroupDuplicate(aHealtherServiceGroupId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#deleteHealthServiceCodesFromGroup(java.
	 * lang.Long, java.util.List)
	 */
	@Override
	public void deleteHealthServiceCodesFromGroup(Long aHealthServiceGroupId,
			List<HealthServiceCode> aListOfCodesForDelete) {
		healthServiceGroupDAO.deleteHealthServiceCodesFromGroup(aHealthServiceGroupId, aListOfCodesForDelete);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#getHealthServicePrograms()
	 */
	@Override
	public List<HealthServiceProgram> getHealthServicePrograms() {

		// Load data
		List<Object[]> results = healthServiceGroupDAO.getHealthServicePrograms();

		List<HealthServiceProgram> healthServicePrograms = new ArrayList<HealthServiceProgram>();

		// Populate Program objects
		for (Object[] row : results) {
			HealthServiceProgram hsp = new HealthServiceProgram();
			hsp.setProgramCode(((String) row[0]).trim());
			hsp.setProgramName(((String) row[1]).trim());

			// Add Program to list
			healthServicePrograms.add(hsp);
		}
		return healthServicePrograms;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#getModifiers()
	 */
	@Override
	public List<Modifier> getModifiers() {

		List<Modifier> modifiers = new ArrayList<Modifier>();
		List<Object[]> results = healthServiceGroupDAO.getModifiers();

		// Convert data to modifiers
		for (Object[] row : results) {
			Modifier modifier = new Modifier();
			modifier.setModifier((String) row[0]);
			modifier.setModifierValue((String) row[1]);

			// Add Modifiers to list
			modifiers.add(modifier);
		}

		return modifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#getImplicitModifiers()
	 */
	@Override
	public List<Modifier> getImplicitModifiers() {

		List<Modifier> modifiers = new ArrayList<Modifier>();
		List<Object[]> results = healthServiceGroupDAO.getImplicitModifiers();

		// Convert data to modifiers
		for (Object[] row : results) {
			Modifier modifier = new Modifier();
			modifier.setModifier((String) row[0]);
			modifier.setModifierValue((String) row[1]);

			// Add Implicit Modifier to list
			modifiers.add(modifier);
		}

		return modifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository#getServicesNotInGroup(java.util.Date)
	 */
	@Override
	public List<HealthServiceCode> getServicesNotInGroup(Date aYearEndDate) {

		List<HealthServiceCode> healthServiceCodes = new ArrayList<HealthServiceCode>();
		List<Object[]> results = healthServiceGroupDAO.getServicesNotInGroup(aYearEndDate);

		// Convert data to HealthServiceCodes
		for (Object[] row : results) {
			HealthServiceCode hsc = new HealthServiceCode();

			hsc.setHealthServiceId((String) row[0]);
			hsc.setProgram((String) row[1]);
			hsc.setHealthServiceCode((String) row[2]);
			hsc.setModifiers((String) row[3]);
			hsc.setImplicitModifiers((String) row[4]);
			hsc.setQualifier((String) row[5]);
			hsc.setEffectiveFrom((Date) row[6]);
			hsc.setEffectiveTo((Date) row[7]);
			hsc.setCategory((String) row[8]);
			hsc.setUnitFormula((String) row[9]);

			// Add HSC to list
			healthServiceCodes.add(hsc);
		}
		return healthServiceCodes;
	}
}
