package ca.medavie.nspp.audit.service.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.dao.DenticareDAO;
import ca.medavie.nspp.audit.service.dao.DenticareDAOImpl;
import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.data.DssDenAuditCriteria;
import ca.medavie.nspp.audit.service.data.DssDenClaimAudit;
import ca.medavie.nspp.audit.service.data.DssDenClaimAuditPK;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssDenAuditCriteriaMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssDenticareALRMappedEntity;

public class DenticareRepositoryImpl extends BaseRepository implements DenticareRepository {

	private static final Logger logger = LoggerFactory.getLogger(DenticareRepositoryImpl.class);

	private DenticareDAO denticareDAO;

	public DenticareRepositoryImpl(EntityManager em) {

		denticareDAO = new DenticareDAOImpl(em);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DenticareRepository#getSearchedDenticareALR(ca.medavie.nspp.audit.service
	 * .data.DenticareAuditLetterResponseSearchCriteria, int, int)
	 */
	@Override
	public List<DenticareAuditLetterResponse> getSearchedDenticareALR(
			DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria, int startRow, int maxRows) {
		logger.debug("DenticareRepository : getSearchedDenticareALR() : Begin");

		List<DssDenticareALRMappedEntity> sources = denticareDAO.getDenticareALRRecords(denticareALRSearchCriteria,
				startRow, maxRows);

		List<DenticareAuditLetterResponse> result = new ArrayList<DenticareAuditLetterResponse>();

		for (DssDenticareALRMappedEntity source : sources) {

			DenticareAuditLetterResponse target = new DenticareAuditLetterResponse();

			// transfer data object to business object
			BEAN_MAPPER.map(source, target);

			result.add(target);
		}

		logger.debug("DenticareRepository : getSearchedDenticareALR() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DenticareRepository#getSearchedDenticareALRCount(ca.medavie.nspp.audit
	 * .service.data.DenticareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public Integer getSearchedDenticareALRCount(DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria) {
		logger.debug("DenticareRepository : getSearchedDenticareALRCount() : Begin");
		Integer count = denticareDAO.getSearchedDenticareALRCount(denticareALRSearchCriteria);
		logger.debug("DenticareRepository : getSearchedDenticareALRCount() : Begin");
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DenticareRepository#saveDenticareALR(ca.medavie.nspp.audit.service.data
	 * .DenticareAuditLetterResponse)
	 */
	@Override
	public void saveDenticareALR(DenticareAuditLetterResponse selectedDCALR) {
		logger.debug("DenticareRepository : saveDenticareALR() : Begin");

		// Attempt to load existing Claim record
		DssDenClaimAuditPK pk = new DssDenClaimAuditPK();
		pk.setAuditRunNumber(selectedDCALR.getAuditRunNumber());
		pk.setClaimNum(selectedDCALR.getClaimNumber());
		pk.setItemCode(selectedDCALR.getItemCode());

		DssDenClaimAudit target = denticareDAO.getDenticareALRByID(pk);
		// Create new object if nothing is found
		if (target == null) {
			target = new DssDenClaimAudit();
		}
		// transfer business t data object
		BEAN_MAPPER.map(selectedDCALR, target);

		denticareDAO.saveDenticareALR(target);
		logger.debug("DenticareRepository : saveDenticareALR() : Begin");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DenticareRepository#getSearchPractitionerAuditCriterias(ca.medavie.nspp
	 * .audit.service.data.AuditCriteriaSearchCriteria)
	 */
	@Override
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows) {
		logger.debug("DenticareRepositoryImpl -- getSearchPractitionerAuditCriterias(): Begin");

		List<AuditCriteria> results = new ArrayList<AuditCriteria>();
		List<DssDenAuditCriteriaMappedEntity> dataEntities = denticareDAO.getSearchPractitionerAuditCriterias(
				aSearchCriteria, startRow, maxRows);

		for (DssDenAuditCriteriaMappedEntity entity : dataEntities) {

			AuditCriteria targetBo = new AuditCriteria();
			BEAN_MAPPER.map(entity, targetBo);
			results.add(targetBo);
		}

		logger.debug("DenticareRepositoryImpl -- getSearchPractitionerAuditCriterias(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DenticareRepository#getSearchedAuditCriteriaCount(ca.medavie.nspp.audit
	 * .service.data.AuditCriteriaSearchCriteria)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria) {
		logger.debug("DenticareRepositoryImpl -- getSearchedAuditCriteriaCount(): Begin");
		Integer count = denticareDAO.getSearchedAuditCriteriaCount(anAuditCriteriaSearchCriteria);
		logger.debug("DenticareRepositoryImpl -- getSearchedAuditCriteriaCount(): Begin");
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DenticareRepository#getPractitionerAuditCriteriaById(java.lang.Long)
	 */
	@Override
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId) {
		logger.debug("DenticareRepositoryImpl -- getPractitionerAuditCriteriaById(): Begin");

		AuditCriteria result = new AuditCriteria();
		DssDenAuditCriteria dataEntity = denticareDAO.getDssDenAuditCriteriaByPk(anId);

		// Perform map
		BEAN_MAPPER.map(dataEntity, result);

		// load the last Audit Date and set
		result.setLastAuditDate(getLastAuditDate(result.getAuditCriteriaId()));

		logger.debug("DenticareRepositoryImpl -- getPractitionerAuditCriteriaById(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DenticareRepository#getDefaultAuditCriteriaByType(java.lang.String)
	 */
	@Override
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType) {
		logger.debug("DenticareRepositoryImpl -- getDefaultAuditCriteriaByType(): Begin");

		AuditCriteria result = new AuditCriteria();
		DssDenAuditCriteria dataEntity = denticareDAO.getDefaultAuditCriteriaByType(anAuditType);

		if (dataEntity != null) {
			// Audit record found, map to BO
			BEAN_MAPPER.map(dataEntity, result);

			// load the last Audit Date and set
			result.setLastAuditDate(getLastAuditDate(result.getAuditCriteriaId()));
		} else {
			// No audit record exists for this audit type. Set the AuditType on the new object
			result.setAuditType(anAuditType);
		}

		logger.debug("DenticareRepositoryImpl -- getDefaultAuditCriteriaByType(): End");

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DenticareRepository#saveDenticareAuditCriteria(ca.medavie.nspp.audit
	 * .service.data.AuditCriteria)
	 */
	@Override
	public void saveDenticareAuditCriteria(AuditCriteria anAuditCriteria) {
		logger.debug("DenticareRepositoryImpl -- saveDenticareAuditCriteria(): Begin");
		/*
		 * If there is a Criteria ID set we are editing an existing record. Grab that record for update. If there is no
		 * ID we create a new entity to persist
		 */
		DssDenAuditCriteria dssDenAuditCriteria;

		if (anAuditCriteria.getAuditCriteriaId() == null) {
			// Call Sequence to generate a unique ID for this entity
			Long uniqueId = denticareDAO.getNextDenAudCriteriaSequenceValue();
			anAuditCriteria.setAuditCriteriaId(uniqueId);

			// Create new Entity
			dssDenAuditCriteria = new DssDenAuditCriteria();
			BEAN_MAPPER.map(anAuditCriteria, dssDenAuditCriteria);
		} else {
			// Load the existing record by PK
			dssDenAuditCriteria = denticareDAO.getDssDenAuditCriteriaByPk(anAuditCriteria.getAuditCriteriaId());
			BEAN_MAPPER.map(anAuditCriteria, dssDenAuditCriteria);
		}
		// Submit changes to DAO
		denticareDAO.saveDenticareAuditCriteria(dssDenAuditCriteria);

		logger.debug("DenticareRepositoryImpl -- saveDenticareAuditCriteria(): End");
	}

	/**
	 * Simple utility method used by the Repository to load the Last Audit Date value for an Audit Criteria
	 * 
	 * @param anAuditCriteriaId
	 * @return the lastAuditDate for the specified Audit Criteria. Null is returned if no date is found.
	 */
	private Date getLastAuditDate(Long anAuditCriteriaId) {
		return denticareDAO.getLastAuditDate(anAuditCriteriaId);
	}
}
