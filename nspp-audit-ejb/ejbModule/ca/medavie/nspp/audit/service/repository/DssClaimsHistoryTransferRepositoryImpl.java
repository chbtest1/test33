package ca.medavie.nspp.audit.service.repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import ca.medavie.nspp.audit.service.dao.DssClaimsHistoryTransferDAO;
import ca.medavie.nspp.audit.service.dao.DssClaimsHistoryTransferDAOImpl;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.DenticareClaim;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferRequest;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferSummary;
import ca.medavie.nspp.audit.service.data.DssDentalClaim;
import ca.medavie.nspp.audit.service.data.DssHistoryConversion;
import ca.medavie.nspp.audit.service.data.DssHistoryConversionPK;
import ca.medavie.nspp.audit.service.data.DssIndividual2;
import ca.medavie.nspp.audit.service.data.DssMedMainframeClaim;
import ca.medavie.nspp.audit.service.data.DssMedicareSe;
import ca.medavie.nspp.audit.service.data.DssPharmMainframeClaim;
import ca.medavie.nspp.audit.service.data.DssPharmacareSe;
import ca.medavie.nspp.audit.service.data.HealthCard;
import ca.medavie.nspp.audit.service.data.MedicareClaim;
import ca.medavie.nspp.audit.service.data.PharmacareClaim;
import ca.medavie.nspp.audit.service.data.TransferSummary;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssAuditHistoryTransferMappedEntity;

/**
 * Session Bean implementation class DssClaimsHistoryTransferRepositoryImpl
 */
public class DssClaimsHistoryTransferRepositoryImpl extends BaseRepository implements
		DssClaimsHistoryTransferRepository {

	private DssClaimsHistoryTransferDAO dssClaimsHistoryTransferDAO;

	public DssClaimsHistoryTransferRepositoryImpl(EntityManager em) {
		dssClaimsHistoryTransferDAO = new DssClaimsHistoryTransferDAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepository#getSearchedHealthCard(java.lang.String
	 * )
	 */
	@Override
	public HealthCard getSearchedHealthCard(String aHealthCardNumber) {
		HealthCard result = null;
		DssIndividual2 dataEntity = dssClaimsHistoryTransferDAO.getSearchedHealthCard(aHealthCardNumber);
		if (dataEntity != null) {
			result = new HealthCard();
			BEAN_MAPPER.map(dataEntity, result);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepository#getMedicareSeClaims(ca.medavie.nspp
	 * .audit.service.data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<MedicareClaim> getMedicareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest) {
		List<MedicareClaim> result = new ArrayList<MedicareClaim>();
		List<DssMedicareSe> dataEntities = dssClaimsHistoryTransferDAO.getMedicareSeClaims(aTransferRequest);

		for (DssMedicareSe source : dataEntities) {
			MedicareClaim target = new MedicareClaim();
			BEAN_MAPPER.map(source, target);

			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepository#getMedicareMainframeClaims(ca.medavie
	 * .nspp.audit.service.data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<MedicareClaim> getMedicareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest) {
		List<MedicareClaim> result = new ArrayList<MedicareClaim>();
		List<DssMedMainframeClaim> dataEntities = dssClaimsHistoryTransferDAO
				.getMedicareMainframeClaims(aTransferRequest);

		for (DssMedMainframeClaim source : dataEntities) {
			MedicareClaim target = new MedicareClaim();
			BEAN_MAPPER.map(source, target);

			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepository#getPharmacareSeClaims(ca.medavie.
	 * nspp.audit.service.data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<PharmacareClaim> getPharmacareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest) {
		List<PharmacareClaim> result = new ArrayList<PharmacareClaim>();
		List<DssPharmacareSe> dataEntities = dssClaimsHistoryTransferDAO.getPharmacareSeClaims(aTransferRequest);

		for (DssPharmacareSe source : dataEntities) {
			PharmacareClaim target = new PharmacareClaim();
			BEAN_MAPPER.map(source, target);

			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepository#getPharmacareMainframeClaims(ca.medavie
	 * .nspp.audit.service.data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<PharmacareClaim> getPharmacareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest) {
		List<PharmacareClaim> result = new ArrayList<PharmacareClaim>();
		List<DssPharmMainframeClaim> dataEntities = dssClaimsHistoryTransferDAO
				.getPharmacareMainframeClaims(aTransferRequest);

		for (DssPharmMainframeClaim source : dataEntities) {
			PharmacareClaim target = new PharmacareClaim();
			BEAN_MAPPER.map(source, target);

			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepository#getDentalClaims(ca.medavie.nspp.audit
	 * .service.data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<DenticareClaim> getDentalClaims(DssClaimsHistoryTransferRequest aTransferRequest) {
		List<DenticareClaim> result = new ArrayList<DenticareClaim>();
		List<DssDentalClaim> dataEntities = dssClaimsHistoryTransferDAO.getDentalClaims(aTransferRequest);

		for (DssDentalClaim source : dataEntities) {
			DenticareClaim target = new DenticareClaim();
			BEAN_MAPPER.map(source, target);

			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepository#saveClaimsHistoryTransferStagingData
	 * (ca.medavie. nspp.audit.service.data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public void saveClaimsHistoryTransferStagingData(DssClaimsHistoryTransferRequest aTransferRequest) {

		// Used for date ranges
		Calendar c = Calendar.getInstance();

		// Extract base information used on every row
		String sourceHcn = aTransferRequest.getSourceHealthCardNumber();
		String targetHcn = aTransferRequest.getTargetHealthCardNumber();
		String notes = aTransferRequest.getTransferNote();

		// If no dates were provided set default range to 01/01/1900 - 01/01/2100
		Date fromDate = aTransferRequest.getServiceFromDate();
		Date toDate = aTransferRequest.getServiceToDate();

		if (fromDate == null) {
			// Apply default from date
			c.set(1900, 01, 01);
			fromDate = c.getTime();
		}
		if (toDate == null) {
			// Apply default from date
			c.set(2100, 01, 01);
			toDate = c.getTime();
		}

		/**
		 * Holds all the History Conversion objects that will be saved.
		 * 
		 * Using set in this case due to Denticare Claims. Multiple rows can have the same ID due to multiple item
		 * numbers under one claim number. This prevents any errors with JPA detecting duplicate IDs
		 */
		Set<DssHistoryConversion> conversions = new HashSet<DssHistoryConversion>();

		// Process Medicare SE Claims
		if (aTransferRequest.isAllMedicareSeClaimsSelected()) {

			// User is copying all Medicare SE claims
			DssHistoryConversion conv = new DssHistoryConversion();
			DssHistoryConversionPK pk = new DssHistoryConversionPK();

			pk.setFromHealthCardNumber(sourceHcn);
			pk.setToHealthCardNumber(targetHcn);
			pk.setFromServiceDate(fromDate);
			pk.setToServiceDate(toDate);
			pk.setMessage(notes);
			pk.setMed("Y");

			conv.setId(pk);

			// Add to Conversion list
			conversions.add(conv);

		} else if (!aTransferRequest.getMedicareClaims().isEmpty()) {

			// Make a conversion row for each claim
			for (MedicareClaim claim : aTransferRequest.getMedicareClaims()) {

				DssHistoryConversion conv = new DssHistoryConversion();
				DssHistoryConversionPK pk = new DssHistoryConversionPK();

				pk.setSeNumber(claim.getSeNumber());
				pk.setFromHealthCardNumber(sourceHcn);
				pk.setToHealthCardNumber(targetHcn);
				pk.setMessage(notes);
				pk.setMed("Y");

				conv.setId(pk);

				// Add to Conversion list
				conversions.add(conv);
			}
		}

		// Process Medicare Mainframe Claims
		if (aTransferRequest.isAllMedicareMainframeClaimsSelected()) {

			// User is copying all Medicare Mainframe claims
			DssHistoryConversion conv = new DssHistoryConversion();
			DssHistoryConversionPK pk = new DssHistoryConversionPK();

			pk.setFromHealthCardNumber(sourceHcn);
			pk.setToHealthCardNumber(targetHcn);
			pk.setFromServiceDate(fromDate);
			pk.setToServiceDate(toDate);
			pk.setMessage(notes);
			pk.setMf("Y");

			conv.setId(pk);

			// Add to Conversion list
			conversions.add(conv);

		} else if (!aTransferRequest.getMedicareMainframeClaims().isEmpty()) {

			// Make a conversion row for each claim
			for (MedicareClaim claim : aTransferRequest.getMedicareMainframeClaims()) {

				DssHistoryConversion conv = new DssHistoryConversion();
				DssHistoryConversionPK pk = new DssHistoryConversionPK();

				pk.setSeNumber(claim.getMainSeqNumber().toString());
				pk.setFromHealthCardNumber(sourceHcn);
				pk.setToHealthCardNumber(targetHcn);
				pk.setMessage(notes);
				pk.setMf("Y");

				conv.setId(pk);

				// Add to Conversion list
				conversions.add(conv);
			}
		}

		// Process Pharmacare SE Claims
		if (aTransferRequest.isAllPharmacareSeClaimsSelected()) {

			// User is copying all Pharmacare SE claims
			DssHistoryConversion conv = new DssHistoryConversion();
			DssHistoryConversionPK pk = new DssHistoryConversionPK();

			pk.setFromHealthCardNumber(sourceHcn);
			pk.setToHealthCardNumber(targetHcn);
			pk.setFromServiceDate(fromDate);
			pk.setToServiceDate(toDate);
			pk.setMessage(notes);
			pk.setPharm("Y");

			conv.setId(pk);

			// Add to Conversion list
			conversions.add(conv);

		} else if (!aTransferRequest.getPharmacareClaims().isEmpty()) {

			// Make a conversion row for each claim
			for (PharmacareClaim claim : aTransferRequest.getPharmacareClaims()) {

				DssHistoryConversion conv = new DssHistoryConversion();
				DssHistoryConversionPK pk = new DssHistoryConversionPK();

				pk.setSeNumber(claim.getSeNumber());
				pk.setFromHealthCardNumber(sourceHcn);
				pk.setToHealthCardNumber(targetHcn);
				pk.setMessage(notes);
				pk.setPharm("Y");

				conv.setId(pk);

				// Add to Conversion list
				conversions.add(conv);
			}
		}

		// Process Pharmacare Mainframe Claims
		if (aTransferRequest.isAllPharmacareMainframeClaimsSelected()) {

			// User is copying all Pharmacare Mainframe claims
			DssHistoryConversion conv = new DssHistoryConversion();
			DssHistoryConversionPK pk = new DssHistoryConversionPK();

			pk.setFromHealthCardNumber(sourceHcn);
			pk.setToHealthCardNumber(targetHcn);
			pk.setFromServiceDate(fromDate);
			pk.setToServiceDate(toDate);
			pk.setMessage(notes);
			pk.setPf("Y");

			conv.setId(pk);

			// Add to Conversion list
			conversions.add(conv);

		} else if (!aTransferRequest.getPharmacareMainframeClaims().isEmpty()) {

			// Make a conversion row for each claim
			for (PharmacareClaim claim : aTransferRequest.getPharmacareMainframeClaims()) {

				DssHistoryConversion conv = new DssHistoryConversion();
				DssHistoryConversionPK pk = new DssHistoryConversionPK();

				pk.setSeNumber(claim.getMainSeqNumber().toString());
				pk.setFromHealthCardNumber(sourceHcn);
				pk.setToHealthCardNumber(targetHcn);
				pk.setMessage(notes);
				pk.setPf("Y");

				conv.setId(pk);

				// Add to Conversion list
				conversions.add(conv);
			}
		}

		// Process Denticare Claims
		if (aTransferRequest.isAllDentalClaimsSelected()) {

			// User is copying all Denticare claims
			DssHistoryConversion conv = new DssHistoryConversion();
			DssHistoryConversionPK pk = new DssHistoryConversionPK();

			pk.setFromHealthCardNumber(sourceHcn);
			pk.setToHealthCardNumber(targetHcn);
			pk.setFromServiceDate(fromDate);
			pk.setToServiceDate(toDate);
			pk.setMessage(notes);
			pk.setDent("Y");

			conv.setId(pk);

			// Add to Conversion list
			conversions.add(conv);

		} else if (!aTransferRequest.getDentalClaims().isEmpty()) {

			// Make a conversion row for each claim
			for (DenticareClaim claim : aTransferRequest.getDentalClaims()) {

				DssHistoryConversion conv = new DssHistoryConversion();
				DssHistoryConversionPK pk = new DssHistoryConversionPK();

				pk.setSeNumber(claim.getClaimNumber().toString());
				pk.setFromHealthCardNumber(sourceHcn);
				pk.setToHealthCardNumber(targetHcn);
				pk.setMessage(notes);
				pk.setDent("Y");

				conv.setId(pk);

				// Add to Conversion list
				conversions.add(conv);
			}
		}

		// Save conversion records to the DSS_HISTORY_CONVERSION table
		dssClaimsHistoryTransferDAO.saveClaimsHistoryTransferStagingData(conversions);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepository#executeClaimsHistoryTransferProcedure
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public void executeClaimsHistoryTransferProcedure(String aSourceHcn, String aTargetHcn) {
		dssClaimsHistoryTransferDAO.executeClaimsHistoryTransferProcedure(aSourceHcn, aTargetHcn);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepository#getClaimsHistoryTransferSummary(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public DssClaimsHistoryTransferSummary getClaimsHistoryTransferSummary(String aSourceHcn, String aTargetHcn) {
		DssClaimsHistoryTransferSummary transferSummary = new DssClaimsHistoryTransferSummary();

		List<DssAuditHistoryTransferMappedEntity> transferEntities = dssClaimsHistoryTransferDAO
				.getDssAuditHistoryTransfers(aSourceHcn, aTargetHcn);

		if (transferEntities != null && !transferEntities.isEmpty()) {

			// Grab the first record found to set base transfer details
			DssAuditHistoryTransferMappedEntity t = transferEntities.get(0);

			// Set details
			transferSummary.setSourceHcn(aSourceHcn);
			transferSummary.setSourceName(t.getId().getSource_name());

			transferSummary.setTargetHcn(aTargetHcn);
			transferSummary.setTargetName(t.getId().getTarget_name());

			transferSummary.setTransferRequestor(t.getId().getModified_by());
			transferSummary.setTransferComment(t.getId().getComments());

			transferSummary.setTransferDate(DataUtilities.SDF.format(t.getId().getTransfer_date()));

			// Init claims lists
			transferSummary.setMedicareSeClaimSummaries(new ArrayList<TransferSummary>());
			transferSummary.setMedicareMainClaimsSummaries(new ArrayList<TransferSummary>());
			transferSummary.setPharmSeClaimSummaries(new ArrayList<TransferSummary>());
			transferSummary.setPharmMainClaimSummaries(new ArrayList<TransferSummary>());
			transferSummary.setDenticareClaimSummaries(new ArrayList<TransferSummary>());

			// Now populate each claim list based on type indicators
			for (DssAuditHistoryTransferMappedEntity source : transferEntities) {

				TransferSummary summary = new TransferSummary();
				BEAN_MAPPER.map(source, summary);

				if (source.getId().getMed() != null && source.getId().getMed().equals("Y")) {
					transferSummary.getMedicareSeClaimSummaries().add(summary);
				}
				if (source.getId().getMf() != null && source.getId().getMf().equals("Y")) {
					transferSummary.getMedicareMainClaimsSummaries().add(summary);
				}
				if (source.getId().getPharm() != null && source.getId().getPharm().equals("Y")) {
					transferSummary.getPharmSeClaimSummaries().add(summary);
				}
				if (source.getId().getPf() != null && source.getId().getPf().equals("Y")) {
					transferSummary.getPharmMainClaimSummaries().add(summary);
				}
				if (source.getId().getDent() != null && source.getId().getDent().equals("Y")) {
					transferSummary.getDenticareClaimSummaries().add(summary);
				}
			}
		}

		return transferSummary;
	}
}
