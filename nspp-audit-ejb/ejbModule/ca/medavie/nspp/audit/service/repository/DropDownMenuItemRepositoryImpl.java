package ca.medavie.nspp.audit.service.repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.persistence.EntityManager;

import ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO;
import ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAOImpl;
import ca.medavie.nspp.audit.service.data.CodeEntry;
import ca.medavie.nspp.audit.service.data.DssCodeTableAlphaEntry;
import ca.medavie.nspp.audit.service.data.DssCodeTableEntry;
import ca.medavie.nspp.audit.service.data.DssHealthServiceGroup;
import ca.medavie.nspp.audit.service.data.DssProfilePeriod;
import ca.medavie.nspp.audit.service.data.DssProviderType;
import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;
import ca.medavie.nspp.audit.service.data.SpecialtyCriteria;
import ca.medavie.nspp.audit.service.data.SubcategoryType;

public class DropDownMenuItemRepositoryImpl extends BaseRepository implements DropDownMenuItemRepository {

	private DropDownMenuItemDAO dropDownMenuItemDAO;

	/**
	 * Constructor taking an EntityManager
	 * 
	 * @param em
	 */
	public DropDownMenuItemRepositoryImpl(EntityManager em) {

		dropDownMenuItemDAO = new DropDownMenuItemDAOImpl(em);

	}

	@Override
	public Map<String, SubcategoryType> getSubCategoryTypeDropDown() {

		List<DssProviderType> types = dropDownMenuItemDAO.getSubCategoryDataEntries();
		SortedMap<String, SubcategoryType> typeMap = new TreeMap<String, SubcategoryType>();

		for (DssProviderType type : types) {

			SubcategoryType aType = new SubcategoryType();
			BEAN_MAPPER.map(type, aType);
			typeMap.put(aType.getTypeCode(), aType);

		}

		return typeMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getSpecialtyDropDown()
	 */
	@Override
	public Map<String, SpecialtyCriteria> getSpecialtyDropDown() {

		List<DssCodeTableAlphaEntry> enties = dropDownMenuItemDAO.getSpecialtyDataEntries();

		SortedMap<String, SpecialtyCriteria> specialtyMap = new TreeMap<String, SpecialtyCriteria>();

		for (DssCodeTableAlphaEntry entry : enties) {

			SpecialtyCriteria sc = new SpecialtyCriteria();
			BEAN_MAPPER.map(entry, sc);
			specialtyMap.put(sc.getSpecialtyCode(), sc);

		}

		return specialtyMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getAuditRXTypeDropDropDown()
	 */
	@Override
	public Map<String, CodeEntry> getAuditRXTypeDropDropDown() {
		List<DssCodeTableEntry> enties = dropDownMenuItemDAO.getDssAuditRXCodeEntries();

		SortedMap<String, CodeEntry> map = new TreeMap<String, CodeEntry>();

		for (DssCodeTableEntry source : enties) {

			CodeEntry target = new CodeEntry();
			BEAN_MAPPER.map(source, target);
			map.put(target.getCodeTableEntryDescription(), target);
		}

		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getAuditNonRXTypeDropDropDown()
	 */
	@Override
	public Map<String, CodeEntry> getAuditNonRXTypeDropDropDown() {
		List<DssCodeTableEntry> enties = dropDownMenuItemDAO.getDssAuditNonRXCodeEntries();

		SortedMap<String, CodeEntry> map = new TreeMap<String, CodeEntry>();

		for (DssCodeTableEntry source : enties) {

			CodeEntry target = new CodeEntry();
			BEAN_MAPPER.map(source, target);
			map.put(target.getCodeTableEntryDescription(), target);
		}

		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getContractTownNames()
	 */
	@Override
	public List<String> getContractTownNames() {
		return dropDownMenuItemDAO.getContractTownNames();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getQualifierDropDropDown()
	 */
	@Override
	public Map<String, String> getQualifierDropDropDown() {
		List<String> qulifiers = dropDownMenuItemDAO.getQualifierCodes();

		SortedMap<String, String> map = new TreeMap<String, String>();

		for (String qulifier : qulifiers) {

			map.put(qulifier, qulifier);
		}

		return map;
	}

	@Override
	public Map<String, String> getAuditSourceMedCodeDropDown() {
		List<String> auditSources = dropDownMenuItemDAO.geAuditSourceMedCodes();

		SortedMap<String, String> map = new TreeMap<String, String>();

		for (String as : auditSources) {

			map.put(as, as);
		}

		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getAuditSourcePharmCodeDropDown()
	 */
	@Override
	public Map<String, String> getAuditSourcePharmCodeDropDown() {
		List<String> auditSources = dropDownMenuItemDAO.geAuditSourcePharmCodes();

		SortedMap<String, String> map = new TreeMap<String, String>();

		for (String as : auditSources) {

			map.put(as, as);
		}

		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getAuditPharmTypeCodeDropDown()
	 */
	@Override
	public Map<String, String> getAuditPharmTypeCodeDropDown() {
		List<String> auditSources = dropDownMenuItemDAO.geAuditPharmTypeCodes();

		SortedMap<String, String> map = new TreeMap<String, String>();

		for (String as : auditSources) {

			map.put(as, as);
		}

		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getGroupTypeCodeDropDown()
	 */
	@Override
	public Map<String, String> getGroupTypeCodeDropDown() {
		List<String> groupTypes = dropDownMenuItemDAO.getGroupTypeCodes();

		SortedMap<String, String> map = new TreeMap<String, String>();

		for (String as : groupTypes) {

			map.put(as, as);
		}

		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getPeriodYearEndDateDropdown()
	 */
	@Override
	public Map<String, String> getPeriodYearEndDateDropdown() {
		List<Date> profilePeriodYearEndDates = dropDownMenuItemDAO.getPeriodYearEndDates();
		Map<String, String> map = new LinkedHashMap<String, String>();

		// Used to create user friendly key for map (Used for label display in dropdown)
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

		for (Date yearEnd : profilePeriodYearEndDates) {
			String dateString = sdf.format(yearEnd);
			map.put(dateString, dateString);
		}
		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getPeriodIdDropdown()
	 */
	@Override
	public Map<Long, PractitionerProfilePeriod> getPeriodIdDropdown() {
		Map<Long, PractitionerProfilePeriod> ppps = new TreeMap<Long, PractitionerProfilePeriod>();
		List<DssProfilePeriod> dpps = dropDownMenuItemDAO.getPeriodIdDropdown();

		for (DssProfilePeriod dpp : dpps) {

			PractitionerProfilePeriod ppp = new PractitionerProfilePeriod();

			ppp.setPeriodId(dpp.getPeriodId());
			ppp.setPeriodStart(dpp.getPeriodStartDate());
			ppp.setPeriodEnd(dpp.getPeriodEndDate());
			ppp.setYearEnd(dpp.getYearEndDate());
			ppp.setLastModified(dpp.getLastModified());
			ppp.setModifiedBy(dpp.getModifiedBy());

			ppps.put(ppp.getPeriodId(), ppp);
		}
		return ppps;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getDssUnitValueLevelDropdown()
	 */
	@Override
	public Map<Long, Long> getDssUnitValueLevelDropdown() {

		List<Long> levels = dropDownMenuItemDAO.getDssUnitValueLevelCodes();

		SortedMap<Long, Long> map = new TreeMap<Long, Long>();

		for (Long l : levels) {

			map.put(l, l);
		}

		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getDssUnitValueDescsDropdown()
	 */
	@Override
	public Map<String, Long> getDssUnitValueDescsDropdown() {
		List<DssCodeTableEntry> enties = dropDownMenuItemDAO.getDssUnitValueDescsCodes();

		SortedMap<String, Long> map = new TreeMap<String, Long>();

		for (DssCodeTableEntry e : enties) {

			map.put(e.getCodeTableEntryDescription(), e.getId().getCodeTableEntry());
		}

		return map;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getHsModifierDropDown()
	 */
	@Override
	public Map<String, String> getHsModifierDropDown() {
		List<Object[]> modifierCodes = dropDownMenuItemDAO.getHsModifers();

		SortedMap<String, String> modifiers = new TreeMap<String, String>();

		for (Object[] group : modifierCodes) {
			modifiers.put((String) group[1] + "=" + (String) group[0], (String) group[1] + "=" + (String) group[0]);
		}
		return modifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getHsImplicitModifiersDropDown()
	 */
	@Override
	public Map<String, String> getHsImplicitModifiersDropDown() {
		List<Object[]> imcodes = dropDownMenuItemDAO.getHsImplicitModifiers();

		SortedMap<String, String> modifiers = new TreeMap<String, String>();

		for (Object[] im : imcodes) {
			modifiers.put((String) im[1] + "=" + (String) im[0], (String) im[1] + "=" + (String) im[0]);
		}
		return modifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getHsProgramDropDown()
	 */
	@Override
	public Map<String, String> getHsProgramDropDown() {
		List<Object[]> programs = dropDownMenuItemDAO.getHsProgramCodes();

		SortedMap<String, String> programsItems = new TreeMap<String, String>();

		for (Object[] program : programs) {
			programsItems.put((String) program[0], (String) program[1]);
		}
		return programsItems;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getHsGroupDropDown()
	 */
	@Override
	public Map<Long, String> getHsGroupDropDown() {
		List<DssHealthServiceGroup> groups = dropDownMenuItemDAO.getHsGroupCodes();

		SortedMap<Long, String> groupItems = new TreeMap<Long, String>();

		for (DssHealthServiceGroup group : groups) {
			groupItems.put(group.getHealthServiceGroupId(),
					group.getHealthServiceGroupId() + " - " + group.getHealthServiceGroupName());
		}
		return groupItems;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getHsQualifiersDropDown()
	 */
	@Override
	public Map<String, String> getHsQualifiersDropDown() {
		List<String> qulifierCodes = dropDownMenuItemDAO.getHsQualifiersCodes();

		SortedMap<String, String> qualifiers = new TreeMap<String, String>();

		for (String q : qulifierCodes) {
			qualifiers.put(q, q);
		}
		return qualifiers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getDenticareAuditTypeDropDown()
	 */
	@Override
	public Map<String, String> getDenticareAuditTypeDropDown() {
		List<DssCodeTableAlphaEntry> auditTypeCode = dropDownMenuItemDAO.getDenticareAuditTypeCodes();

		SortedMap<String, String> auditTypes = new TreeMap<String, String>();

		for (DssCodeTableAlphaEntry q : auditTypeCode) {
			auditTypes.put(q.getId().getCodeTableAlphaEntry().trim(), q.getCodeTableAlphaEntryDesc().trim());
		}
		return auditTypes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getMedicareAuditTypeDropDown()
	 */
	@Override
	public Map<String, String> getMedicareAuditTypeDropDown() {
		List<DssCodeTableAlphaEntry> auditTypeCode = dropDownMenuItemDAO.getMedicareAuditTypeCodes();

		SortedMap<String, String> auditTypes = new TreeMap<String, String>();

		for (DssCodeTableAlphaEntry q : auditTypeCode) {
			auditTypes.put(q.getId().getCodeTableAlphaEntry().trim(), q.getCodeTableAlphaEntryDesc().trim());
		}
		return auditTypes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getPharmacareAuditTypeDropDown()
	 */
	@Override
	public Map<String, String> getPharmacareAuditTypeDropDown() {
		List<DssCodeTableAlphaEntry> auditTypeCode = dropDownMenuItemDAO.getPharmacareAuditTypeDropDown();

		SortedMap<String, String> auditTypes = new TreeMap<String, String>();

		for (DssCodeTableAlphaEntry q : auditTypeCode) {
			auditTypes.put(q.getId().getCodeTableAlphaEntry().trim(), q.getCodeTableAlphaEntryDesc().trim());
		}
		return auditTypes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getResponseStatusDropDown()
	 */
	@Override
	public Map<String, String> getResponseStatusDropDown() {
		List<DssCodeTableAlphaEntry> respondsStatusCodes = dropDownMenuItemDAO.getResponseStatusCodes();

		SortedMap<String, String> respondsStatus = new TreeMap<String, String>();

		for (DssCodeTableAlphaEntry q : respondsStatusCodes) {
			respondsStatus.put(q.getId().getCodeTableAlphaEntry().replaceAll("\\s+", ""),
					q.getCodeTableAlphaEntryDesc());
		}
		return respondsStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getGlNumbers()
	 */
	@Override
	public Map<String, String> getGlNumbers() {
		List<String> rawGlNumbers = dropDownMenuItemDAO.getGlNumbers();

		SortedMap<String, String> glNumbers = new TreeMap<String, String>();

		for (String q : rawGlNumbers) {
			String[] values = q.split("-", 2);
			glNumbers.put(values[0], values[1]);
		}
		return glNumbers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getPayeeTypeDropDown()
	 */
	@Override
	public Map<String, String> getPayeeTypeDropDown() {

		List<DssCodeTableAlphaEntry> payeeTypes = dropDownMenuItemDAO.getPayeeTypeDropDown();

		SortedMap<String, String> result = new TreeMap<String, String>();

		for (DssCodeTableAlphaEntry q : payeeTypes) {
			result.put(q.getId().getCodeTableAlphaEntry().trim(), q.getCodeTableAlphaEntryDesc().trim());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getPaymentResponsibilityDropDown()
	 */
	@Override
	public Map<String, String> getPaymentResponsibilityDropDown() {

		List<DssCodeTableAlphaEntry> paymentResponsibilities = dropDownMenuItemDAO.getPaymentResponsibilityDropDown();

		SortedMap<String, String> result = new TreeMap<String, String>();

		for (DssCodeTableAlphaEntry q : paymentResponsibilities) {
			result.put(q.getId().getCodeTableAlphaEntry().trim(), q.getCodeTableAlphaEntryDesc().trim());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getGeneralAuditCriteriaTypeDropDown()
	 */
	@Override
	public Map<String, String> getGeneralAuditCriteriaTypeDropDown() {
		List<DssCodeTableAlphaEntry> generalAuditCriterias = dropDownMenuItemDAO.getGeneralAuditCriteriaTypeDropDown();

		SortedMap<String, String> result = new TreeMap<String, String>();

		for (DssCodeTableAlphaEntry q : generalAuditCriterias) {
			result.put(q.getCodeTableAlphaEntryDesc().trim(), q.getId().getCodeTableAlphaEntry().trim());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository#getPharmacareAuditCriteriaTypeDropDown()
	 */
	@Override
	public Map<String, String> getPharmacareAuditCriteriaTypeDropDown() {
		List<DssCodeTableAlphaEntry> pharmacareAuditCriterias = dropDownMenuItemDAO
				.getPharmacareAuditCriteriaTypeDropDown();

		SortedMap<String, String> result = new TreeMap<String, String>();

		for (DssCodeTableAlphaEntry q : pharmacareAuditCriterias) {
			result.put(q.getCodeTableAlphaEntryDesc().trim(), q.getId().getCodeTableAlphaEntry().trim());
		}
		return result;
	}

	@Override
	public Map<String, String> getHSEffectiveDateDropDown() {
		List<Date> hsEffectiveDates = dropDownMenuItemDAO.getUniqueHSEffectiveDates();
		
		//Collections.sort(hsEffectiveDates);

		Map<String, String> result = new LinkedHashMap<String, String>();
		// Used to create user friendly key for map (Used for label display in dropdown)
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

		for (Date from : hsEffectiveDates) {
			String dateString = sdf.format(from);			
			result.put(dateString, dateString);
		}
		return result;
	}
}
