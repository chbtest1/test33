package ca.medavie.nspp.audit.service.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import ca.medavie.nspp.audit.service.data.BaseSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.exception.PeerGroupServiceException;

/**
 * Interface of the PeerGroup repository layer
 */
public interface PeerGroupRepository {

	/**
	 * @param peerGroupSearchCriteria
	 *            given search criteria
	 * @param startRow
	 * @param maxRows
	 * @return A list of peer groups by given search criteria
	 */
	public List<PeerGroup> getSearchedPeerGroups(PeerGroupSearchCriteria peerGroupSearchCriteria, int startRow,
			int maxRows);

	/**
	 * Counts how many records the entire PeerGroup search will return without pagination
	 * 
	 * @param peerGroupSearchCriteria
	 * @return total PeerGroups found
	 */
	public Integer getSearchedPeerGroupsCount(PeerGroupSearchCriteria peerGroupSearchCriteria);

	/**
	 * Loads Practitioners that match the provided search criteria (Searches all existing Practitioners)
	 * 
	 * @param aPractitionerSearchCriteria
	 *            - User specified search Criteria
	 * @return list of matching Practitioners
	 */
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria);

	/**
	 * 
	 * Save or update a Peer Group
	 * 
	 * @param aPeerGroup
	 *            - a peer group to save
	 * @throws PeerGroupServiceException
	 * @throws ValidatorException
	 * 
	 */
	public void saveOrUpdatePeerGroup(PeerGroup aPeerGroup) throws PeerGroupServiceException;

	/**
	 * Gets a list of PeerGroups by their latest YearEndDate. All previous version are ignored
	 * 
	 * @return - list of unique PeerGroups
	 */
	public List<PeerGroup> getLatestPeerGroups();

	/**
	 * Performs Bulk Copy on the provided PeerGroups
	 * 
	 * @param aListOfPeerGroupsToCopy
	 *            - PeerGroups to copy
	 * @param aUserId
	 *            - ID of the user who requested the copy
	 */
	public void copyPeerGroups(List<PeerGroup> aListOfPeerGroupsToCopy, String aUserId);

	/**
	 * @param townSearchCriteria
	 *            - a given town search criteria
	 * @return A list of town criteria
	 */
	public List<TownCriteria> getTownCriteriaSearchResults(TownCriteriaSearchCriteria townSearchCriteria);

	/**
	 * @return Map contains health service effective from drop down menu
	 */
	public Map<String, String> getHealthServiceEFDropDown();

	/**
	 * @param hscSearchCriteria
	 * @return A list of health service result by given search criteria
	 */
	public List<HealthServiceCriteria> getHealthServiceCriteriaSearchResults(
			HealthServiceCriteriaSearchCriteria hscSearchCriteria);

	/**
	 * @param aPeerGroup
	 * @return a Peer group to be editing
	 */
	public PeerGroup getPeerGroupDetails(PeerGroup aPeerGroup);


	/**
	 * @param aSearchCriteria
	 * @param filtred
	 * @return
	 */
	public Map<String, BigDecimal>[] getFilteredTownSCDropDowns(BaseSearchCriteria aSearchCriteria);
}
