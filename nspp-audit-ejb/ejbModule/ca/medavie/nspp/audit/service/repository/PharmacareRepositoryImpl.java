package ca.medavie.nspp.audit.service.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.dao.PharmacareDAO;
import ca.medavie.nspp.audit.service.dao.PharmacareDAOImpl;
import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DIN;
import ca.medavie.nspp.audit.service.data.DinSearchCriteria;
import ca.medavie.nspp.audit.service.data.DssDin;
import ca.medavie.nspp.audit.service.data.DssPharmAuditCriteria;
import ca.medavie.nspp.audit.service.data.DssPharmAuditDinExcl;
import ca.medavie.nspp.audit.service.data.DssPharmClaimAudit;
import ca.medavie.nspp.audit.service.data.DssPharmClaimAuditPK;
import ca.medavie.nspp.audit.service.data.DssRxAudIndivExcl;
import ca.medavie.nspp.audit.service.data.DssRxAudIndivExclPK;
import ca.medavie.nspp.audit.service.data.IndividualExclusion;
import ca.medavie.nspp.audit.service.data.PharmAuditDinExcl;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssIndividualExclusion;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPharmAuditCriteriaMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPharmacareALRMappedEntity;

public class PharmacareRepositoryImpl extends BaseRepository implements PharmacareRepository {

	private static final Logger logger = LoggerFactory.getLogger(PharmacareRepositoryImpl.class);

	private PharmacareDAO pharmacareDAO;

	public PharmacareRepositoryImpl(EntityManager em) {

		pharmacareDAO = new PharmacareDAOImpl(em);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PharmacareRepository#getSearchedPharmacareALR(ca.medavie.nspp.audit.
	 * service.data.PharmacareAuditLetterResponseSearchCriteria, int, int)
	 */
	@Override
	public List<PharmacareAuditLetterResponse> getSearchedPharmacareALR(
			PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria, int startRow, int maxRows) {

		logger.debug("PharmacareRepositoryImpl -- getSearchedPharmacareALR(): Begin");

		List<DssPharmacareALRMappedEntity> sources = pharmacareDAO.getPharmacareALRRecords(pharmCareALRSearchCriteria,
				startRow, maxRows);
		List<PharmacareAuditLetterResponse> result = new ArrayList<PharmacareAuditLetterResponse>();

		for (DssPharmacareALRMappedEntity source : sources) {

			PharmacareAuditLetterResponse target = new PharmacareAuditLetterResponse();

			// transfer data object to business object
			BEAN_MAPPER.map(source, target);

			result.add(target);
		}

		logger.debug("PharmacareRepositoryImpl -- getSearchedPharmacareALR(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PharmacareRepository#getSearchedPharmacareALRCount(ca.medavie.nspp.audit
	 * .service.data.PharmacareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public Integer getSearchedPharmacareALRCount(PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria) {
		logger.debug("PharmacareRepositoryImpl -- getSearchedPharmacareALRCount(): Begin");
		Integer count = pharmacareDAO.getSearchedPharmacareALRCount(pharmCareALRSearchCriteria);
		logger.debug("PharmacareRepositoryImpl -- getSearchedPharmacareALRCount(): Begin");
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PharmacareRepository#savePharmacareALR(ca.medavie.nspp.audit.service
	 * .data.PharmacareAuditLetterResponse)
	 */
	@Override
	public void savePharmacareALR(PharmacareAuditLetterResponse selectedDCALR) {

		logger.debug("PharmacareRepositoryImpl -- savePharmacareALR(): Begin");

		// if it is an existing one
		DssPharmClaimAuditPK pk = new DssPharmClaimAuditPK();
		pk.setAuditRunNumber(selectedDCALR.getAudit_run_number());
		pk.setClaimRefNum(selectedDCALR.getClaim_ref_num());

		DssPharmClaimAudit target = pharmacareDAO.getPharmacareALRByID(pk);
		// transfer business t data object
		BEAN_MAPPER.map(selectedDCALR, target);

		pharmacareDAO.savePharmacareALR(target);
		logger.debug("PharmacareRepositoryImpl -- savePharmacareALR(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PharmacareRepository#getSearchPractitionerAuditCriterias(ca.medavie.
	 * nspp.audit.service.data.AuditCriteriaSearchCriteria, int, int)
	 */
	@Override
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows) {

		logger.debug("PharmacareRepositoryImpl -- getSearchPractitionerAuditCriterias(): Begin");

		List<AuditCriteria> results = new ArrayList<AuditCriteria>();
		List<DssPharmAuditCriteriaMappedEntity> dataEntities = pharmacareDAO.getSearchPractitionerAuditCriterias(
				aSearchCriteria, startRow, maxRows);

		for (DssPharmAuditCriteriaMappedEntity entity : dataEntities) {

			AuditCriteria targetBo = new AuditCriteria();
			BEAN_MAPPER.map(entity, targetBo);
			results.add(targetBo);
		}

		logger.debug("PharmacareRepositoryImpl -- getSearchPractitionerAuditCriterias(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PharmacareRepository#getSearchedAuditCriteriaCount(ca.medavie.nspp.audit
	 * .service.data.AuditCriteriaSearchCriteria)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria) {
		logger.debug("PharmacareRepositoryImpl -- getSearchedPharmacareALRCount(): Begin");
		Integer count = pharmacareDAO.getSearchedAuditCriteriaCount(anAuditCriteriaSearchCriteria);
		logger.debug("PharmacareRepositoryImpl -- getSearchedPharmacareALRCount(): Begin");
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PharmacareRepository#getDefaultAuditCriteriaByType(java.lang.String)
	 */
	@Override
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType) {

		logger.debug("PharmacareRepositoryImpl -- getDefaultAuditCriteriaByType(): Begin");

		AuditCriteria result = new AuditCriteria();
		DssPharmAuditCriteria dataEntity = pharmacareDAO.getDefaultAuditCriteriaByType(anAuditType);

		if (dataEntity != null) {
			// Audit record found, map to BO
			BEAN_MAPPER.map(dataEntity, result);

			// Get LastAuditDate if available and set
			result.setLastAuditDate(getLastAuditDate(result.getAuditCriteriaId()));

		} else {
			// No audit record exists for this audit type. Set the AuditType on the new object
			result.setAuditType(anAuditType);
		}

		logger.debug("PharmacareRepositoryImpl -- getDefaultAuditCriteriaByType(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PharmacareRepository#getPractitionerAuditCriteriaById(java.lang.Long)
	 */
	@Override
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId) {

		logger.debug("PharmacareRepositoryImpl -- getPractitionerAuditCriteriaById(): Begin");

		AuditCriteria result = new AuditCriteria();
		DssPharmAuditCriteria dataEntity = pharmacareDAO.getDssPharmAuditCriteriaByPk(anId);

		// Perform map
		BEAN_MAPPER.map(dataEntity, result);

		// Get LastAuditDate if available and set
		result.setLastAuditDate(getLastAuditDate(result.getAuditCriteriaId()));

		logger.debug("PharmacareRepositoryImpl -- getPractitionerAuditCriteriaById(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PharmacareRepository#savePharmacareAuditCriteria(ca.medavie.nspp.audit
	 * .service.data.AuditCriteria)
	 */
	@Override
	public void savePharmacareAuditCriteria(AuditCriteria anAuditCriteria) {
		logger.debug("PharmacareRepositoryImpl -- savePharmacareAuditCriteria(): Begin");

		/*
		 * If there is a Criteria ID set we are editing an existing record. Grab that record for update. If there is no
		 * ID we create a new entity to persist
		 */
		DssPharmAuditCriteria dssPharmAuditCriteria;

		if (anAuditCriteria.getAuditCriteriaId() == null) {
			// Call Sequence to generate a unique ID for this entity
			Long uniqueId = pharmacareDAO.getNextPharmAuditCriteriaSequenceValue();
			anAuditCriteria.setAuditCriteriaId(uniqueId);

			// Need to update all children to have the correct parent ID
			if (anAuditCriteria.getDins() != null) {
				for (DIN d : anAuditCriteria.getDins()) {
					d.setAuditCriteriaId(uniqueId);
				}
			}

			// Create new Entity
			dssPharmAuditCriteria = new DssPharmAuditCriteria();
			BEAN_MAPPER.map(anAuditCriteria, dssPharmAuditCriteria);
		} else {
			// Load the existing record by PK
			dssPharmAuditCriteria = pharmacareDAO.getDssPharmAuditCriteriaByPk(anAuditCriteria.getAuditCriteriaId());

			// Clear children lists
			dssPharmAuditCriteria.getDssPharmAuditDinXrefs().clear();

			BEAN_MAPPER.map(anAuditCriteria, dssPharmAuditCriteria);
		}
		// Submit changes to DAO
		pharmacareDAO.savePharmacareAuditCriteria(dssPharmAuditCriteria);

		logger.debug("PharmacareRepositoryImpl -- savePharmacareAuditCriteria(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.PharmacareRepository#getSearchDins(ca.medavie.nspp.audit.service.data
	 * .DinSearchCriteria)
	 */
	@Override
	public List<DIN> getSearchDins(DinSearchCriteria aSearchCriteria) {
		logger.debug("PharmacareRepositoryImpl -- getSearchDins(): Begin");

		List<DIN> results = new ArrayList<DIN>();
		List<DssDin> dataEntities = pharmacareDAO.getSearchDins(aSearchCriteria);

		for (DssDin d : dataEntities) {

			DIN targetBo = new DIN();
			BEAN_MAPPER.map(d, targetBo);
			results.add(targetBo);
		}

		logger.debug("PharmacareRepositoryImpl -- getSearchDins(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.PharmacareRepository#saveIndividualExclusions(java.util.List)
	 */
	@Override
	public void saveIndividualExclusions(List<IndividualExclusion> aListOfIndExclToSave) {
		logger.debug("PharmacareRepositoryImpl -- saveIndividualExclusions(): Begin");

		// Stage 1 of save.. Delete the records being updated
		List<DssRxAudIndivExcl> exclusionsForRemovalForUpdate = new ArrayList<DssRxAudIndivExcl>();
		for (IndividualExclusion excl : aListOfIndExclToSave) {

			/*
			 * Only attempt to delete existing records.. This is simply determined by the existence of the original
			 * Effective From value
			 */
			if (excl.getOriginalEffectiveFrom() != null) {
				/*
				 * Determine the records being saved are updates of existing, we must delete the record and re add it as
				 * a new row.. The reason for this is due to the Primary Key. The user is able to change part of the
				 * primary (Effective From) and if this is changed we are unable to complete the update successfully
				 * without an Optimistic lock exception.
				 */
				DssRxAudIndivExclPK pk = new DssRxAudIndivExclPK();
				pk.setEffectiveFromDate(excl.getOriginalEffectiveFrom());
				pk.setHealthCardNumber(excl.getHealthCardNumber());

				DssRxAudIndivExcl dssRxExcl = pharmacareDAO.getDssRxAudIndivExclByPK(pk);
				if (dssRxExcl != null) {
					exclusionsForRemovalForUpdate.add(dssRxExcl);
				}
			}
		}

		// Submit delete request
		pharmacareDAO.deleteIndividualExclusions(exclusionsForRemovalForUpdate);

		// Stage 2 of save. Create new entities for each updated/new record
		List<DssRxAudIndivExcl> exclusionsToSave = new ArrayList<DssRxAudIndivExcl>();
		for (IndividualExclusion excl : aListOfIndExclToSave) {

			DssRxAudIndivExcl newEntity = new DssRxAudIndivExcl();
			BEAN_MAPPER.map(excl, newEntity);

			// Add to list for save
			exclusionsToSave.add(newEntity);
		}

		// Save changes to database
		pharmacareDAO.saveIndividualExclusions(exclusionsToSave);

		logger.debug("PharmacareRepositoryImpl -- saveIndividualExclusions(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.PharmacareRepository#getIndividualExclusions()
	 */
	@Override
	public List<IndividualExclusion> getIndividualExclusions() {
		logger.debug("PharmacareRepositoryImpl -- getIndividualExclusions(): Begin");

		List<IndividualExclusion> result = new ArrayList<IndividualExclusion>();
		List<DssIndividualExclusion> dataEntities = pharmacareDAO.getIndividualExclusions();

		for (DssIndividualExclusion source : dataEntities) {

			IndividualExclusion target = new IndividualExclusion();
			BEAN_MAPPER.map(source, target);
			result.add(target);
		}

		logger.debug("PharmacareRepositoryImpl -- getIndividualExclusions(): End");
		return result;
	}

	/**
	 * Simple utility method used by the Repository to load the Last Audit Date value for an Audit Criteria
	 * 
	 * @param anAuditCriteriaId
	 * @return the lastAuditDate for the specified Audit Criteria. Null is returned if no date is found.
	 */
	private Date getLastAuditDate(Long anAuditCriteriaId) {
		return pharmacareDAO.getLastAuditDate(anAuditCriteriaId);
	}

	@Override
	public List<PharmAuditDinExcl> getDinExclusions() {
		logger.debug("PharmacareRepositoryImpl -- getDinExclusions(): Begin");
		List<PharmAuditDinExcl> result = new ArrayList<PharmAuditDinExcl>();
		List<DssPharmAuditDinExcl> dataEntities = pharmacareDAO.getDinExclusions();
		for (DssPharmAuditDinExcl source : dataEntities) {
			PharmAuditDinExcl target = new PharmAuditDinExcl();
			BEAN_MAPPER.map(source, target);
			result.add(target);
		}
		logger.debug("PharmacareRepositoryImpl -- getDinExclusions(): End");
		return result;		
		
	}

	@Override
	public void saveDinExclusions(List<PharmAuditDinExcl> pharmAuditDinExclResults) {
		logger.debug("PharmacareRepositoryImpl -- saveDinExclusions(): Begin");

		List<DssPharmAuditDinExcl> exclusionsToSave = new ArrayList<DssPharmAuditDinExcl>();
		for (PharmAuditDinExcl excl : pharmAuditDinExclResults) {

			DssPharmAuditDinExcl newEntity = new DssPharmAuditDinExcl();
			BEAN_MAPPER.map(excl, newEntity);

			// Add to list for save
			exclusionsToSave.add(newEntity);
		}

		// Save changes to database
		pharmacareDAO.saveDinExclusions(exclusionsToSave);

		logger.debug("PharmacareRepositoryImpl -- saveDinExclusions(): End");
		
	}

	@Override
	public void deleteDinExcl(PharmAuditDinExcl pharmAuditDinExcl) {
		pharmacareDAO.deleteDinExclusions(pharmAuditDinExcl);		
	}
}
