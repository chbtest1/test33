package ca.medavie.nspp.audit.service.repository;

import java.util.List;

import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponseSearchCriteria;

public interface DenticareRepository {

	/**
	 * @param denticareALRSearchCriteria
	 * @param startRow
	 *            - Row number the result set should begin
	 * @param maxRows
	 *            - Maximum number of results to return
	 * @return list of DenticareAuditLetterResponse
	 */
	public List<DenticareAuditLetterResponse> getSearchedDenticareALR(
			DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an ALR search
	 * 
	 * @param denticareALRSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedDenticareALRCount(DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria);

	/**
	 * save denticare audit letter responds
	 * 
	 * @param selectedDCALR
	 * 
	 */
	public void saveDenticareALR(DenticareAuditLetterResponse selectedDCALR);

	/**
	 * Loads any matching AuditCriteria records using the specified Search Criteria
	 * 
	 * @param aSearchCriteria
	 * @param startRow
	 *            - Row number the result set should begin
	 * @param maxRows
	 *            - Maximum number of results to return
	 * @return any matching AuditCriteria records
	 */
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Audit Criteria search
	 * 
	 * @param anAuditCriteriaSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria);

	/**
	 * Attempts to load AuditCriteria record by ID
	 * 
	 * @param anId
	 *            - ID of the desired AuditCriteria
	 * @return AuditCriteria with the specified ID
	 */
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId);

	/**
	 * Attempts to load a Default AuditCriteria by the specified AuditType
	 * 
	 * @param anAuditType
	 * @return the matching audit if it exists or a brand new AuditCriteria object if nothing is found
	 */
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType);

	/**
	 * Saves the specified AuditCriteria record
	 * 
	 * @param anAuditCriteria
	 */
	public void saveDenticareAuditCriteria(AuditCriteria anAuditCriteria);
}
