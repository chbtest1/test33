package ca.medavie.nspp.audit.service.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import ca.medavie.nspp.audit.service.dao.ManualDepositsDAO;
import ca.medavie.nspp.audit.service.dao.ManualDepositsDAOImpl;
import ca.medavie.nspp.audit.service.data.BusinessArrangementCode;
import ca.medavie.nspp.audit.service.data.DssManualDeposit;
import ca.medavie.nspp.audit.service.data.ManualDeposit;
import ca.medavie.nspp.audit.service.data.ManualDepositSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.ProviderGroup;
import ca.medavie.nspp.audit.service.data.ProviderGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssBusinessArrangementMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssManualDepositsMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssProviderGroupMappedEntity;

/**
 * Implementation of the Inquiry repository layer
 */
public class ManualDepositsRepositoryImpl extends BaseRepository implements ManualDepositsRepository {
	private ManualDepositsDAO manualDepositsDAO;

	/**
	 * Constructor for Inquiry Repository
	 */
	public ManualDepositsRepositoryImpl(EntityManager em) {
		manualDepositsDAO = new ManualDepositsDAOImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#getBusinessArrangements(ca.medavie.nspp.audit
	 * .service.data.Practitioner, ca.medavie.nspp.audit.service.data.ProviderGroup)
	 */
	@Override
	public List<BusinessArrangementCode> getBusinessArrangements(Practitioner practitioner, ProviderGroup group) {

		List<DssBusinessArrangementMappedEntity> sources = manualDepositsDAO.getBusinessArrangements(practitioner,
				group);
		List<BusinessArrangementCode> result = new ArrayList<BusinessArrangementCode>();

		for (DssBusinessArrangementMappedEntity source : sources) {
			BusinessArrangementCode target = new BusinessArrangementCode();
			target.setBusDesc(source.getBus_arr_description());
			target.setBusNumber(source.getBus_arr_number().toString());
			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#getBusinessArrangements(ca.medavie.nspp.audit
	 * .service.data.ManualDeposit)
	 */
	@Override
	public List<BusinessArrangementCode> getBusinessArrangements(ManualDeposit manualDeposit) {
		List<DssBusinessArrangementMappedEntity> sources = manualDepositsDAO.getBusinessArrangements(manualDeposit);
		List<BusinessArrangementCode> result = new ArrayList<BusinessArrangementCode>();

		for (DssBusinessArrangementMappedEntity source : sources) {
			BusinessArrangementCode target = new BusinessArrangementCode();
			target.setBusDesc(source.getBus_arr_description());
			target.setBusNumber(source.getBus_arr_number().toString());
			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#getSearchedManualDeposits(ca.medavie.nspp.audit
	 * .service.data.ManualDepositSearchCriteria, boolean, int, int)
	 */
	@Override
	public List<ManualDeposit> getSearchedManualDeposits(ManualDepositSearchCriteria aManualDepositSearchCriteria,
			boolean aPractitionerSearch, int startRow, int maxRows) {

		List<DssManualDepositsMappedEntity> sources = manualDepositsDAO.getSearchedManualDeposits(
				aManualDepositSearchCriteria, aPractitionerSearch, startRow, maxRows);
		List<ManualDeposit> result = new ArrayList<ManualDeposit>();

		for (DssManualDepositsMappedEntity source : sources) {
			ManualDeposit target = new ManualDeposit();
			BEAN_MAPPER.map(source, target);
			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#getSearchedAuditCriteriaCount(ca.medavie.nspp
	 * .audit.service.data.ManualDepositSearchCriteria, boolean)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(ManualDepositSearchCriteria aManualDepositSearchCriteria,
			boolean aPractitionerSearch) {
		return manualDepositsDAO.getSearchedAuditCriteriaCount(aManualDepositSearchCriteria, aPractitionerSearch);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#getManualDepositDetails(ca.medavie.nspp.audit
	 * .service.data.ManualDeposit)
	 */
	@Override
	public ManualDeposit getManualDepositDetails(ManualDeposit aManualDeposit) {

		DssManualDeposit dssManualDeposit = manualDepositsDAO
				.getManualDepositDetails(aManualDeposit.getDepositNumber());
		ManualDeposit target = new ManualDeposit();

		BEAN_MAPPER.map(dssManualDeposit, target);
		return target;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#saveOrUpdateManualDeposit(ca.medavie.nspp.audit
	 * .service.data.ManualDeposit)
	 */
	@Override
	public void saveOrUpdateManualDeposit(ManualDeposit selectedManualDeposit) {

		DssManualDeposit target = new DssManualDeposit();

		BEAN_MAPPER.map(selectedManualDeposit, target);
		manualDepositsDAO.saveOrUpdateManualDeposit(target);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#deleteManualDeposit(ca.medavie.nspp.audit.service
	 * .data.ManualDeposit)
	 */
	@Override
	public void deleteManualDeposit(ManualDeposit selectedManualDeposit) {
		manualDepositsDAO.deleteManualDeposit(selectedManualDeposit.getDepositNumber());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#getNextValueFromDssDepositNumberSeq()
	 */
	@Override
	public Long getNextValueFromDssDepositNumberSeq() {
		return manualDepositsDAO.getNextValueFromDssDepositNumberSeq();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#verifyPractitioner(ca.medavie.nspp.audit.service
	 * .data.ManualDeposit)
	 */
	@Override
	public boolean verifyPractitioner(ManualDeposit selectedManualDeposit) {
		return manualDepositsDAO.verifyPractitioner(selectedManualDeposit);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#getManualDepositSearchedPractitioners(ca.medavie
	 * .nspp.audit.service.data.PractitionerSearchCriteria, int, int)
	 */
	@Override
	public List<Practitioner> getManualDepositSearchedPractitioners(
			PractitionerSearchCriteria aPractitionerSearchCriteria, int startRow, int maxRows) {

		// Holds search results
		List<Practitioner> practitioners = new ArrayList<Practitioner>();

		// Query for matching Practitioner records
		List<Object[]> results = manualDepositsDAO.getManualDepositSearchedPractitioners(aPractitionerSearchCriteria,
				startRow, maxRows);

		// Convert DB entity to Business object
		for (Object[] resultRow : results) {
			Practitioner practitioner = new Practitioner();
			// Populate remaining Practitioner details
			practitioner.setPractitionerNumber(((BigDecimal) resultRow[0]).longValue());
			practitioner.setPractitionerType((String) resultRow[1]);
			practitioner.setPractitionerName((String) resultRow[2]);
			practitioner.setCity((String) resultRow[3]);

			// Add populated Practitioner to list of results
			practitioners.add(practitioner);
		}
		return practitioners;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#getManualDepositSearchedPractitionersCount(
	 * ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria)
	 */
	@Override
	public Integer getManualDepositSearchedPractitionersCount(PractitionerSearchCriteria practitionerSearchCriteria) {
		return manualDepositsDAO.getManualDepositSearchedPractitionersCount(practitionerSearchCriteria);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#getSearchedProviderGroups(ca.medavie.nspp.audit
	 * .service.data.ProviderGroupSearchCriteria, int, int)
	 */
	@Override
	public List<ProviderGroup> getSearchedProviderGroups(ProviderGroupSearchCriteria aProviderGroupSearchCriteria,
			int startRow, int maxRows) {

		List<DssProviderGroupMappedEntity> sources = manualDepositsDAO.getSearchedProviderGroups(
				aProviderGroupSearchCriteria, startRow, maxRows);

		List<ProviderGroup> result = new ArrayList<ProviderGroup>();

		for (DssProviderGroupMappedEntity source : sources) {
			ProviderGroup target = new ProviderGroup();
			BEAN_MAPPER.map(source, target);
			result.add(target);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.repository.ManualDepositsRepository#getSearchedProviderGroupsCount(ca.medavie.nspp
	 * .audit.service.data.ProviderGroupSearchCriteria)
	 */
	@Override
	public Integer getSearchedProviderGroupsCount(ProviderGroupSearchCriteria aProviderGroupSearchCriteria) {
		return manualDepositsDAO.getSearchedProviderGroupsCount(aProviderGroupSearchCriteria);
	}
}