package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DssHealthServiceExclusionMappedEntityPK implements Serializable {

	/** IDE Generated */
	private static final long serialVersionUID = 3850908593833680929L;

	@Column
	private String health_service_id;
	@Column
	private String health_service_code;
	@Column
	private String qualifier_code;
	@Column
	private String program_code;
	@Column
	private BigDecimal age_restriction;
	@Column
	private String gender_restriction;
	@Column
	private String modifiers;
	@Column
	private String implicit_modifiers;
	@Column
	private String unit_formula;
	@Column
	private String category_code;
	@Column
	private String modified_by;
	@Column
	private Date last_modified;

	/**
	 * @return the health_service_id
	 */
	public String getHealth_service_id() {
		return health_service_id;
	}

	/**
	 * @param health_service_id
	 *            the health_service_id to set
	 */
	public void setHealth_service_id(String health_service_id) {
		this.health_service_id = health_service_id;
	}

	/**
	 * @return the health_service_code
	 */
	public String getHealth_service_code() {
		return health_service_code;
	}

	/**
	 * @param health_service_code
	 *            the health_service_code to set
	 */
	public void setHealth_service_code(String health_service_code) {
		this.health_service_code = health_service_code;
	}

	/**
	 * @return the qualifier_code
	 */
	public String getQualifier_code() {
		return qualifier_code;
	}

	/**
	 * @param qualifier_code
	 *            the qualifier_code to set
	 */
	public void setQualifier_code(String qualifier_code) {
		this.qualifier_code = qualifier_code;
	}

	/**
	 * @return the program_code
	 */
	public String getProgram_code() {
		return program_code;
	}

	/**
	 * @param program_code
	 *            the program_code to set
	 */
	public void setProgram_code(String program_code) {
		this.program_code = program_code;
	}

	/**
	 * @return the age_restriction
	 */
	public BigDecimal getAge_restriction() {
		return age_restriction;
	}

	/**
	 * @param age_restriction
	 *            the age_restriction to set
	 */
	public void setAge_restriction(BigDecimal age_restriction) {
		this.age_restriction = age_restriction;
	}

	/**
	 * @return the gender_restriction
	 */
	public String getGender_restriction() {
		return gender_restriction;
	}

	/**
	 * @param gender_restriction
	 *            the gender_restriction to set
	 */
	public void setGender_restriction(String gender_restriction) {
		this.gender_restriction = gender_restriction;
	}

	/**
	 * @return the modifiers
	 */
	public String getModifiers() {
		return modifiers;
	}

	/**
	 * @param modifiers
	 *            the modifiers to set
	 */
	public void setModifiers(String modifiers) {
		this.modifiers = modifiers;
	}

	/**
	 * @return the implicit_modifiers
	 */
	public String getImplicit_modifiers() {
		return implicit_modifiers;
	}

	/**
	 * @param implicit_modifiers
	 *            the implicit_modifiers to set
	 */
	public void setImplicit_modifiers(String implicit_modifiers) {
		this.implicit_modifiers = implicit_modifiers;
	}

	/**
	 * @return the unit_formula
	 */
	public String getUnit_formula() {
		return unit_formula;
	}

	/**
	 * @param unit_formula
	 *            the unit_formula to set
	 */
	public void setUnit_formula(String unit_formula) {
		this.unit_formula = unit_formula;
	}

	/**
	 * @return the category_code
	 */
	public String getCategory_code() {
		return category_code;
	}

	/**
	 * @param category_code
	 *            the category_code to set
	 */
	public void setCategory_code(String category_code) {
		this.category_code = category_code;
	}

	/**
	 * @return the modified_by
	 */
	public String getModified_by() {
		return modified_by;
	}

	/**
	 * @param modified_by
	 *            the modified_by to set
	 */
	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	/**
	 * @return the last_modified
	 */
	public Date getLast_modified() {
		return last_modified;
	}

	/**
	 * @param last_modified
	 *            the last_modified to set
	 */
	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((age_restriction == null) ? 0 : age_restriction.hashCode());
		result = prime * result + ((category_code == null) ? 0 : category_code.hashCode());
		result = prime * result + ((gender_restriction == null) ? 0 : gender_restriction.hashCode());
		result = prime * result + ((health_service_code == null) ? 0 : health_service_code.hashCode());
		result = prime * result + ((health_service_id == null) ? 0 : health_service_id.hashCode());
		result = prime * result + ((implicit_modifiers == null) ? 0 : implicit_modifiers.hashCode());
		result = prime * result + ((last_modified == null) ? 0 : last_modified.hashCode());
		result = prime * result + ((modified_by == null) ? 0 : modified_by.hashCode());
		result = prime * result + ((modifiers == null) ? 0 : modifiers.hashCode());
		result = prime * result + ((program_code == null) ? 0 : program_code.hashCode());
		result = prime * result + ((qualifier_code == null) ? 0 : qualifier_code.hashCode());
		result = prime * result + ((unit_formula == null) ? 0 : unit_formula.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssHealthServiceExclusionMappedEntityPK other = (DssHealthServiceExclusionMappedEntityPK) obj;
		if (age_restriction == null) {
			if (other.age_restriction != null)
				return false;
		} else if (!age_restriction.equals(other.age_restriction))
			return false;
		if (category_code == null) {
			if (other.category_code != null)
				return false;
		} else if (!category_code.equals(other.category_code))
			return false;
		if (gender_restriction == null) {
			if (other.gender_restriction != null)
				return false;
		} else if (!gender_restriction.equals(other.gender_restriction))
			return false;
		if (health_service_code == null) {
			if (other.health_service_code != null)
				return false;
		} else if (!health_service_code.equals(other.health_service_code))
			return false;
		if (health_service_id == null) {
			if (other.health_service_id != null)
				return false;
		} else if (!health_service_id.equals(other.health_service_id))
			return false;
		if (implicit_modifiers == null) {
			if (other.implicit_modifiers != null)
				return false;
		} else if (!implicit_modifiers.equals(other.implicit_modifiers))
			return false;
		if (last_modified == null) {
			if (other.last_modified != null)
				return false;
		} else if (!last_modified.equals(other.last_modified))
			return false;
		if (modified_by == null) {
			if (other.modified_by != null)
				return false;
		} else if (!modified_by.equals(other.modified_by))
			return false;
		if (modifiers == null) {
			if (other.modifiers != null)
				return false;
		} else if (!modifiers.equals(other.modifiers))
			return false;
		if (program_code == null) {
			if (other.program_code != null)
				return false;
		} else if (!program_code.equals(other.program_code))
			return false;
		if (qualifier_code == null) {
			if (other.qualifier_code != null)
				return false;
		} else if (!qualifier_code.equals(other.qualifier_code))
			return false;
		if (unit_formula == null) {
			if (other.unit_formula != null)
				return false;
		} else if (!unit_formula.equals(other.unit_formula))
			return false;
		return true;
	}
}
