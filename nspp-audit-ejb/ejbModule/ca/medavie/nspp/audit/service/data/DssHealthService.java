package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the DSS_HEALTH_SERVICE database table.
 * 
 */
@Entity
@Table(name = "DSS_HEALTH_SERVICE")
public class DssHealthService implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumns({
			@JoinColumn(name = "QUALIFIER_CODE", referencedColumnName = "QUALIFIER_CODE", insertable = false, updatable = false),
			@JoinColumn(name = "HEALTH_SERVICE_CODE", referencedColumnName = "HEALTH_SERVICE_CODE", insertable = false, updatable = false) })
	private List<DssHealthServiceCode> dhscs;
	
	@Transient
	private DssHealthServiceCode dhsc;

	// There should be an attention for this colun as it is defiened as char(6) in the database
	// however, it has to be set as Long here to deal with JPA mapping problem. but it seems like breaking other part of the app!!!
	// http://issues.medavie.bluecross.ca:8080/browse/JMAAX-6605
	@Id
	@Column(name = "HEALTH_SERVICE_ID")
	private String healthServiceId;

	@Column(name = "CATEGORY_CODE")
	private String categoryCode;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_FROM_DATE")
	private Date effectiveFromDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	@Column(name = "HEALTH_SERVICE_CODE")
	private String healthServiceCode;

	@Column(name = "IMPLICIT_MODIFIER_TYPES")
	private String implicitModifierTypes;

	@Column(name = "IMPLICIT_MODIFIERS")
	private String implicitModifiers;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIERS")
	private String modifiers;

	@Column(name = "MSI_FEE_CODE")
	private String msiFeeCode;

	@Column(name = "MSI_SPECIALTY_CODE")
	private String msiSpecialtyCode;

	@Column(name = "PROGRAM_CODE")
	private String programCode;

	@Column(name = "QUALIFIER_CODE")
	private String qualifierCode;

	@Column(name = "UNIT_FORMULA")
	private String unitFormula;

	public DssHealthService() {
	}

	public String getHealthServiceId() {
		return this.healthServiceId;
	}

	public void setHealthServiceId(String healthServiceId) {
		this.healthServiceId = healthServiceId;
	}

	public String getCategoryCode() {
		return this.categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public Date getEffectiveFromDate() {
		return this.effectiveFromDate;
	}

	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public String getHealthServiceCode() {
		return this.healthServiceCode;
	}

	public void setHealthServiceCode(String healthServiceCode) {
		this.healthServiceCode = healthServiceCode;
	}

	public String getImplicitModifierTypes() {
		return this.implicitModifierTypes;
	}

	public void setImplicitModifierTypes(String implicitModifierTypes) {
		this.implicitModifierTypes = implicitModifierTypes;
	}

	public String getImplicitModifiers() {
		return this.implicitModifiers;
	}

	public void setImplicitModifiers(String implicitModifiers) {
		this.implicitModifiers = implicitModifiers;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiers() {
		return this.modifiers;
	}

	public void setModifiers(String modifiers) {
		this.modifiers = modifiers;
	}

	public String getMsiFeeCode() {
		return this.msiFeeCode;
	}

	public void setMsiFeeCode(String msiFeeCode) {
		this.msiFeeCode = msiFeeCode;
	}

	public String getMsiSpecialtyCode() {
		return this.msiSpecialtyCode;
	}

	public void setMsiSpecialtyCode(String msiSpecialtyCode) {
		this.msiSpecialtyCode = msiSpecialtyCode;
	}

	public String getProgramCode() {
		return this.programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getQualifierCode() {
		return this.qualifierCode;
	}

	public void setQualifierCode(String qualifierCode) {
		this.qualifierCode = qualifierCode;
	}

	public String getUnitFormula() {
		return this.unitFormula;
	}

	public void setUnitFormula(String unitFormula) {
		this.unitFormula = unitFormula;
	}

	/**
	 * @return the dhsc
	 */
	public DssHealthServiceCode getDhsc() {
		
		if (dhscs != null && !dhscs.isEmpty()) {

			return dhscs.get(0);
		}
		return dhsc;
	}

	/**
	 * @param dhsc the dhsc to set
	 */
	public void setDhsc(DssHealthServiceCode dhsc) {
		this.dhsc = dhsc;
	}

	/**
	 * @return the dhscs
	 */
	public List<DssHealthServiceCode> getDhscs() {
		return dhscs;
	}

	/**
	 * @param dhscs the dhscs to set
	 */
	public void setDhscs(List<DssHealthServiceCode> dhscs) {
		this.dhscs = dhscs;
	}

}