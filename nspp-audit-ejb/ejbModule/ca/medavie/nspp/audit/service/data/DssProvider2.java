package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the DSS_PROVIDER_2 database table.
 * 
 */
@Entity
@SqlResultSetMapping(name = "DssProvider2NativeQueryMapping", entities = { @EntityResult(entityClass = DssProvider2.class, fields = {
		@FieldResult(name = "id.providerNumber", column = "provider_number"),
		@FieldResult(name = "id.providerType", column = "provider_type"), @FieldResult(name = "city", column = "city") }) })
@Table(name = "DSS_PROVIDER_2")
public class DssProvider2 implements Serializable {

	private static final long serialVersionUID = 1L;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "PROVIDER_NUMBER", referencedColumnName = "PROVIDER_NUMBER", insertable = false, updatable = false),
			@JoinColumn(name = "PROVIDER_TYPE", referencedColumnName = "PROVIDER_TYPE", insertable = false, updatable = false) })
	private DssProviderPeerGroupXref dppgx;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "PROVIDER_NUMBER", referencedColumnName = "PROVIDER_NUMBER", insertable = false, updatable = false),
			@JoinColumn(name = "PROVIDER_TYPE", referencedColumnName = "PROVIDER_TYPE", insertable = false, updatable = false) })
	private DssShadProvPeerGroupXref dsppgx;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "PROVIDER_NUMBER", referencedColumnName = "PROVIDER_NUMBER", insertable = false, updatable = false),
			@JoinColumn(name = "PROVIDER_TYPE", referencedColumnName = "PROVIDER_TYPE", insertable = false, updatable = false) })
	private DssProviderGroupXref dpgx;

	// association with audit results
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.MERGE }, orphanRemoval = true, mappedBy = "aDSssProvider")
	private List<DssProviderAudit> dssProviderAudits;

	@EmbeddedId
	private DssProvider2PK id;

	@Column(name = "ADDRESS_LINE_1")
	private String addressLine1;

	@Column(name = "ADDRESS_LINE_2")
	private String addressLine2;

	@Temporal(TemporalType.DATE)
	@Column(name = "BIRTH_DATE")
	private Date birthDate;

	@Column(name = "BIRTH_LOCATION_CODE")
	private BigDecimal birthLocationCode;

	@Column(name = "BIRTH_LOCATION_DESC")
	private String birthLocationDesc;

	@Column(name = "CHAIN_NAME")
	private String chainName;
	@Column(name = "CITY")
	private String city;
	@Column(name = "COUNTRY")
	private String country;

	private BigDecimal extension;

	@Column(name = "FAX_NUMBER")
	private BigDecimal faxNumber;

	@Column(name = "FIRST_NAME")
	private String firstName;

	private String gender;

	@Column(name = "HEALTH_WELFARE_ID")
	private String healthWelfareId;

	@Column(name = "LAST_NAME")
	private String lastName;

	@Column(name = "MIDDLE_NAME")
	private String middleName;

	@Column(name = "OLD_MSI_BILLING_NUMBER")
	private BigDecimal oldMsiBillingNumber;

	@Column(name = "ORGANIZATION_NAME")
	private String organizationName;

	@Column(name = "PHONE_NUMBER")
	private BigDecimal phoneNumber;

	@Column(name = "POS_PROV_CODE")
	private String posProvCode;

	@Column(name = "POS_PRVR_ID")
	private String posPrvrId;

	@Column(name = "POS_PRVR_TYPE_CODE")
	private String posPrvrTypeCode;

	@Column(name = "POS_REGION_CODE")
	private String posRegionCode;

	@Column(name = "POSTAL_CODE")
	private String postalCode;

	@Column(name = "PRESCRIBER_INDICATOR")
	private String prescriberIndicator;

	@Column(name = "PROVIDER_LICENSE_NUMBER")
	private BigDecimal providerLicenseNumber;

	@Column(name = "PROVINCE_CODE")
	private String provinceCode;

	@Column(name = "VALID_ADDRESS_INFO_INDICATOR")
	private String validAddressInfoIndicator;

	@Transient
	private String fullName;

	public List<DssProviderAudit> getDssProviderAudits() {
		return dssProviderAudits;
	}

	public void setDssProviderAudits(List<DssProviderAudit> dssProviderAudits) {
		this.dssProviderAudits = dssProviderAudits;
	}

	public DssProvider2() {
	}

	public DssProviderPeerGroupXref getDppgx() {
		return dppgx;
	}

	public void setDppgx(DssProviderPeerGroupXref dppgx) {
		this.dppgx = dppgx;
	}

	/**
	 * @return the dsppgx
	 */
	public DssShadProvPeerGroupXref getDsppgx() {
		return dsppgx;
	}

	/**
	 * @param dsppgx
	 *            the dsppgx to set
	 */
	public void setDsppgx(DssShadProvPeerGroupXref dsppgx) {
		this.dsppgx = dsppgx;
	}

	/**
	 * @return the dpgx
	 */
	public DssProviderGroupXref getDpgx() {
		return dpgx;
	}

	/**
	 * @param dpgx
	 *            the dpgx to set
	 */
	public void setDpgx(DssProviderGroupXref dpgx) {
		this.dpgx = dpgx;
	}

	public DssProvider2PK getId() {
		return this.id;
	}

	public void setId(DssProvider2PK id) {
		this.id = id;
	}

	public String getAddressLine1() {
		return this.addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return this.addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public BigDecimal getBirthLocationCode() {
		return this.birthLocationCode;
	}

	public void setBirthLocationCode(BigDecimal birthLocationCode) {
		this.birthLocationCode = birthLocationCode;
	}

	public String getBirthLocationDesc() {
		return this.birthLocationDesc;
	}

	public void setBirthLocationDesc(String birthLocationDesc) {
		this.birthLocationDesc = birthLocationDesc;
	}

	public String getChainName() {
		return this.chainName;
	}

	public void setChainName(String chainName) {
		this.chainName = chainName;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public BigDecimal getExtension() {
		return this.extension;
	}

	public void setExtension(BigDecimal extension) {
		this.extension = extension;
	}

	public BigDecimal getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(BigDecimal faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHealthWelfareId() {
		return this.healthWelfareId;
	}

	public void setHealthWelfareId(String healthWelfareId) {
		this.healthWelfareId = healthWelfareId;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public BigDecimal getOldMsiBillingNumber() {
		return this.oldMsiBillingNumber;
	}

	public void setOldMsiBillingNumber(BigDecimal oldMsiBillingNumber) {
		this.oldMsiBillingNumber = oldMsiBillingNumber;
	}

	public String getOrganizationName() {
		return this.organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public BigDecimal getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(BigDecimal phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPosProvCode() {
		return this.posProvCode;
	}

	public void setPosProvCode(String posProvCode) {
		this.posProvCode = posProvCode;
	}

	public String getPosPrvrId() {
		return this.posPrvrId;
	}

	public void setPosPrvrId(String posPrvrId) {
		this.posPrvrId = posPrvrId;
	}

	public String getPosPrvrTypeCode() {
		return this.posPrvrTypeCode;
	}

	public void setPosPrvrTypeCode(String posPrvrTypeCode) {
		this.posPrvrTypeCode = posPrvrTypeCode;
	}

	public String getPosRegionCode() {
		return this.posRegionCode;
	}

	public void setPosRegionCode(String posRegionCode) {
		this.posRegionCode = posRegionCode;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPrescriberIndicator() {
		return this.prescriberIndicator;
	}

	public void setPrescriberIndicator(String prescriberIndicator) {
		this.prescriberIndicator = prescriberIndicator;
	}

	public BigDecimal getProviderLicenseNumber() {
		return this.providerLicenseNumber;
	}

	public void setProviderLicenseNumber(BigDecimal providerLicenseNumber) {
		this.providerLicenseNumber = providerLicenseNumber;
	}

	public String getProvinceCode() {
		return this.provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getValidAddressInfoIndicator() {
		return this.validAddressInfoIndicator;
	}

	public void setValidAddressInfoIndicator(String validAddressInfoIndicator) {
		this.validAddressInfoIndicator = validAddressInfoIndicator;
	}

	public String getFullName() {
		return ((organizationName != null) ? organizationName : "") + " " + ((lastName != null) ? lastName + ", " : "") 
				+ " " + ((firstName != null) ? firstName : "")  + " " + ((middleName != null) ? middleName.substring(0, 1) : "") ;
	}

	@Override
	public String toString() {
		return "DssProvider2 [dppgx=" + dppgx + ", dsppgx=" + dsppgx + ", dpgx=" + dpgx + ", id=" + id
				+ ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", birthDate=" + birthDate
				+ ", birthLocationCode=" + birthLocationCode + ", birthLocationDesc=" + birthLocationDesc
				+ ", chainName=" + chainName + ", city=" + city + ", country=" + country + ", extension=" + extension
				+ ", faxNumber=" + faxNumber + ", firstName=" + firstName + ", gender=" + gender + ", healthWelfareId="
				+ healthWelfareId + ", lastName=" + lastName + ", middleName=" + middleName + ", oldMsiBillingNumber="
				+ oldMsiBillingNumber + ", organizationName=" + organizationName + ", phoneNumber=" + phoneNumber
				+ ", posProvCode=" + posProvCode + ", posPrvrId=" + posPrvrId + ", posPrvrTypeCode=" + posPrvrTypeCode
				+ ", posRegionCode=" + posRegionCode + ", postalCode=" + postalCode + ", prescriberIndicator="
				+ prescriberIndicator + ", providerLicenseNumber=" + providerLicenseNumber + ", provinceCode="
				+ provinceCode + ", validAddressInfoIndicator=" + validAddressInfoIndicator + "]";
	}

}