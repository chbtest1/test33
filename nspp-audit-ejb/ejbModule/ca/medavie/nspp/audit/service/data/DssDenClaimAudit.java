package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_DEN_CLAIM_AUDIT database table.
 * 
 */
@Entity
@Table(name="DSS_DEN_CLAIM_AUDIT")
public class DssDenClaimAudit implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssDenClaimAuditPK id;

	@Temporal(TemporalType.DATE)
	@Column(name="AUDIT_LETTER_CREATION_DATE")
	private Date auditLetterCreationDate;

	@Column(name="AUDIT_NOTE")
	private String auditNote;

	@Column(name="CHARGEBACK_AMOUNT")
	private BigDecimal chargebackAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="CLAIM_SELECTION_DATE")
	private Date claimSelectionDate;

	@Column(name="FEE_CODE_DESCRIPTION")
	private String feeCodeDescription;

	@Column(name="HEALTH_CARD_NUMBER")
	private String healthCardNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="NUMBER_OF_CLAIMS_AFFECTED")
	private BigDecimal numberOfClaimsAffected;

	@Column(name="NUMBER_OF_TIMES_SENT")
	private BigDecimal numberOfTimesSent;

	@Column(name="PROVIDER_NUMBER")
	private BigDecimal providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	@Column(name="RESPONSE_STATUS_CODE")
	private String responseStatusCode;

	public DssDenClaimAudit() {
	}

	public DssDenClaimAuditPK getId() {
		return this.id;
	}

	public void setId(DssDenClaimAuditPK id) {
		this.id = id;
	}

	public Date getAuditLetterCreationDate() {
		return this.auditLetterCreationDate;
	}

	public void setAuditLetterCreationDate(Date auditLetterCreationDate) {
		this.auditLetterCreationDate = auditLetterCreationDate;
	}

	public String getAuditNote() {
		return this.auditNote;
	}

	public void setAuditNote(String auditNote) {
		this.auditNote = auditNote;
	}

	public BigDecimal getChargebackAmount() {
		return this.chargebackAmount;
	}

	public void setChargebackAmount(BigDecimal chargebackAmount) {
		this.chargebackAmount = chargebackAmount;
	}

	public Date getClaimSelectionDate() {
		return this.claimSelectionDate;
	}

	public void setClaimSelectionDate(Date claimSelectionDate) {
		this.claimSelectionDate = claimSelectionDate;
	}

	public String getFeeCodeDescription() {
		return this.feeCodeDescription;
	}

	public void setFeeCodeDescription(String feeCodeDescription) {
		this.feeCodeDescription = feeCodeDescription;
	}

	public String getHealthCardNumber() {
		return this.healthCardNumber;
	}

	public void setHealthCardNumber(String healthCardNumber) {
		this.healthCardNumber = healthCardNumber;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public BigDecimal getNumberOfClaimsAffected() {
		return this.numberOfClaimsAffected;
	}

	public void setNumberOfClaimsAffected(BigDecimal numberOfClaimsAffected) {
		this.numberOfClaimsAffected = numberOfClaimsAffected;
	}

	public BigDecimal getNumberOfTimesSent() {
		return this.numberOfTimesSent;
	}

	public void setNumberOfTimesSent(BigDecimal numberOfTimesSent) {
		this.numberOfTimesSent = numberOfTimesSent;
	}

	public BigDecimal getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(BigDecimal providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public String getResponseStatusCode() {
		return this.responseStatusCode;
	}

	public void setResponseStatusCode(String responseStatusCode) {
		this.responseStatusCode = responseStatusCode;
	}

}