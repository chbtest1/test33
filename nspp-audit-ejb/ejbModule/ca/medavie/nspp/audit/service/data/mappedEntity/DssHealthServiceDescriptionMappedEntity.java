package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.SqlResultSetMapping;

/**
 * The persistent class for the DSS_HEALTH_SERVICE description view
 */
@Entity
@SqlResultSetMapping(name = "DssHealthServiceDescriptionNativeQueryMapping", entities = { @EntityResult(entityClass = DssHealthServiceDescriptionMappedEntity.class, fields = {
		@FieldResult(name = "id.health_service_id", column = "health_service_id"),
		@FieldResult(name = "id.health_service_code", column = "health_service_code"),
		@FieldResult(name = "id.qualifier_code", column = "qualifier_code"),
		@FieldResult(name = "id.effective_from_date", column = "effective_from_date"),
		@FieldResult(name = "id.service_description", column = "service_description"),
		@FieldResult(name = "id.modifiers", column = "modifiers"),
		@FieldResult(name = "id.implicit_modifiers", column = "implicit_modifiers"),
		@FieldResult(name = "id.audit_description", column = "audit_description"),
		@FieldResult(name = "id.category_code", column = "category_code"),
		@FieldResult(name = "id.modified_by", column = "modified_by"),
		@FieldResult(name = "id.last_modified", column = "last_modified") }) })
public class DssHealthServiceDescriptionMappedEntity {

	/**
	 * Set all field values in ID to avoid duplicates
	 */
	@EmbeddedId
	private DssHealthServiceDescriptionMappedEntityPK id;

	/**
	 * @return the id
	 */
	public DssHealthServiceDescriptionMappedEntityPK getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(DssHealthServiceDescriptionMappedEntityPK id) {
		this.id = id;
	}

}
