package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_SHAD_PROV_PEER_GROUP_XREF database table.
 * 
 */
@Embeddable
public class DssShadProvPeerGroupXrefPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="PROVIDER_PEER_GROUP_ID")
	private long providerPeerGroupId;

	@Temporal(TemporalType.DATE)
	@Column(name="YEAR_END_DATE")
	private java.util.Date yearEndDate;

	@Column(name="PROVIDER_NUMBER")
	private long providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	public DssShadProvPeerGroupXrefPK() {
	}
	public long getProviderPeerGroupId() {
		return this.providerPeerGroupId;
	}
	public void setProviderPeerGroupId(long providerPeerGroupId) {
		this.providerPeerGroupId = providerPeerGroupId;
	}
	public java.util.Date getYearEndDate() {
		return this.yearEndDate;
	}
	public void setYearEndDate(java.util.Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}
	public long getProviderNumber() {
		return this.providerNumber;
	}
	public void setProviderNumber(long providerNumber) {
		this.providerNumber = providerNumber;
	}
	public String getProviderType() {
		return this.providerType;
	}
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssShadProvPeerGroupXrefPK)) {
			return false;
		}
		DssShadProvPeerGroupXrefPK castOther = (DssShadProvPeerGroupXrefPK)other;
		return 
			(this.providerPeerGroupId == castOther.providerPeerGroupId)
			&& this.yearEndDate.equals(castOther.yearEndDate)
			&& (this.providerNumber == castOther.providerNumber)
			&& this.providerType.equals(castOther.providerType);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.providerPeerGroupId ^ (this.providerPeerGroupId >>> 32)));
		hash = hash * prime + this.yearEndDate.hashCode();
		hash = hash * prime + ((int) (this.providerNumber ^ (this.providerNumber >>> 32)));
		hash = hash * prime + this.providerType.hashCode();
		
		return hash;
	}
}