package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the DSS_HEALTH_SERVICE_CODE database table.
 * 
 */
@Entity
@Table(name = "DSS_HEALTH_SERVICE_CODE")
@NamedQueries({
		@NamedQuery(name = QueryConstants.DSS_HEALTH_SERVICE_CODE_QUALIFIER_QUERY, query = "select distinct dhsc.id.qualifierCode from DssHealthServiceCode dhsc"),
		@NamedQuery(name = QueryConstants.DSS_HEALTH_SERVICE_CODE_HEALTH_SERIVCE_CODE_QUERY, query = "select distinct dhsc.id.healthServiceCode from DssHealthServiceCode dhsc") })
public class DssHealthServiceCode implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssHealthServiceCodePK id;

	@Column(name = "ALTERNATE_WORDING")
	private String alternateWording;

	@Column(name = "CODE_TYPE")
	private String codeType;

	@Column(name = "DESCRIPTION")
	private String description;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "NUM_DAYS_AFTER_DEATH")
	private BigDecimal numDaysAfterDeath;

	public DssHealthServiceCode() {
	}

	public DssHealthServiceCodePK getId() {
		return this.id;
	}

	public void setId(DssHealthServiceCodePK id) {
		this.id = id;
	}

	public String getAlternateWording() {
		return this.alternateWording;
	}

	public void setAlternateWording(String alternateWording) {
		this.alternateWording = alternateWording;
	}

	public String getCodeType() {
		return this.codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public BigDecimal getNumDaysAfterDeath() {
		return this.numDaysAfterDeath;
	}

	public void setNumDaysAfterDeath(BigDecimal numDaysAfterDeath) {
		this.numDaysAfterDeath = numDaysAfterDeath;
	}

}