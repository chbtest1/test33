package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DssMedicareALRMappedEntityPK {

	@Column
	private String se_number;
	@Column
	private Integer se_sequence_number;
	@Column
	private Integer response_tag_number;
	@Column
	private Long audit_run_number;

	/**
	 * @return the se_number
	 */
	public String getSe_number() {
		return se_number;
	}

	/**
	 * @param se_number
	 *            the se_number to set
	 */
	public void setSe_number(String se_number) {
		this.se_number = se_number;
	}

	/**
	 * @return the se_sequence_number
	 */
	public Integer getSe_sequence_number() {
		return se_sequence_number;
	}

	/**
	 * @param se_sequence_number
	 *            the se_sequence_number to set
	 */
	public void setSe_sequence_number(Integer se_sequence_number) {
		this.se_sequence_number = se_sequence_number;
	}

	/**
	 * @return the response_tag_number
	 */
	public Integer getResponse_tag_number() {
		return response_tag_number;
	}

	/**
	 * @param response_tag_number
	 *            the response_tag_number to set
	 */
	public void setResponse_tag_number(Integer response_tag_number) {
		this.response_tag_number = response_tag_number;
	}

	/**
	 * @return the audit_run_number
	 */
	public Long getAudit_run_number() {
		return audit_run_number;
	}

	/**
	 * @param audit_run_number
	 *            the audit_run_number to set
	 */
	public void setAudit_run_number(Long audit_run_number) {
		this.audit_run_number = audit_run_number;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((audit_run_number == null) ? 0 : audit_run_number.hashCode());
		result = prime * result + ((response_tag_number == null) ? 0 : response_tag_number.hashCode());
		result = prime * result + ((se_number == null) ? 0 : se_number.hashCode());
		result = prime * result + ((se_sequence_number == null) ? 0 : se_sequence_number.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssMedicareALRMappedEntityPK other = (DssMedicareALRMappedEntityPK) obj;
		if (audit_run_number == null) {
			if (other.audit_run_number != null)
				return false;
		} else if (!audit_run_number.equals(other.audit_run_number))
			return false;
		if (response_tag_number == null) {
			if (other.response_tag_number != null)
				return false;
		} else if (!response_tag_number.equals(other.response_tag_number))
			return false;
		if (se_number == null) {
			if (other.se_number != null)
				return false;
		} else if (!se_number.equals(other.se_number))
			return false;
		if (se_sequence_number == null) {
			if (other.se_sequence_number != null)
				return false;
		} else if (!se_sequence_number.equals(other.se_sequence_number))
			return false;
		return true;
	}

}
