package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the DSS_MED_SE_AUDIT database table.
 * 
 */

@Embeddable
public class DssMedSeAuditPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="SE_NUMBER")
	private String se_number;
	
	@Column(name="SE_SEQUENCE_NUMBER")
	private int se_sequence_number;
		
	@Column(name="RESPONSE_TAG_NUMBER")
	private int response_tag_number;
	
	@Column(name="AUDIT_RUN_NUMBER")
	private long audit_run_number;
	
	public DssMedSeAuditPK() {
	}

	public String getSe_number() {
		return se_number;
	}

	public void setSe_number(String se_number) {
		this.se_number = se_number;
	}

	public int getSe_sequence_number() {
		return se_sequence_number;
	}

	public void setSe_sequence_number(int se_sequence_number) {
		this.se_sequence_number = se_sequence_number;
	}

	public int getResponse_tag_number() {
		return response_tag_number;
	}

	public void setResponse_tag_number(int response_tag_number) {
		this.response_tag_number = response_tag_number;
	}

	public long getAudit_run_number() {
		return audit_run_number;
	}

	public void setAudit_run_number(long audit_run_number) {
		this.audit_run_number = audit_run_number;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (audit_run_number ^ (audit_run_number >>> 32));
		result = prime * result + response_tag_number;
		result = prime * result + ((se_number == null) ? 0 : se_number.hashCode());
		result = prime * result + se_sequence_number;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssMedSeAuditPK other = (DssMedSeAuditPK) obj;
		if (audit_run_number != other.audit_run_number)
			return false;
		if (response_tag_number != other.response_tag_number)
			return false;
		if (se_number == null) {
			if (other.se_number != null)
				return false;
		} else if (!se_number.equals(other.se_number))
			return false;
		if (se_sequence_number != other.se_sequence_number)
			return false;
		return true;
	}

	

}
