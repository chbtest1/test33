package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(name = "DssHealthServiceExclusionNativeQueryMapping", entities = { @EntityResult(entityClass = DssHealthServiceExclusionMappedEntity.class, fields = {
		@FieldResult(name = "id.health_service_id", column = "health_service_id"),
		@FieldResult(name = "id.health_service_code", column = "health_service_code"),
		@FieldResult(name = "id.qualifier_code", column = "qualifier_code"),
		@FieldResult(name = "id.program_code", column = "program_code"),
		@FieldResult(name = "id.age_restriction", column = "age_restriction"),
		@FieldResult(name = "id.gender_restriction", column = "gender_restriction"),
		@FieldResult(name = "id.modifiers", column = "modifiers"),
		@FieldResult(name = "id.implicit_modifiers", column = "implicit_modifiers"),
		@FieldResult(name = "id.unit_formula", column = "unit_formula"),
		@FieldResult(name = "id.category_code", column = "category_code"),
		@FieldResult(name = "id.modified_by", column = "modified_by"),
		@FieldResult(name = "id.last_modified", column = "last_modified") }) })
public class DssHealthServiceExclusionMappedEntity {

	@EmbeddedId
	private DssHealthServiceExclusionMappedEntityPK id;

	/**
	 * @return the id
	 */
	public DssHealthServiceExclusionMappedEntityPK getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(DssHealthServiceExclusionMappedEntityPK id) {
		this.id = id;
	}
}
