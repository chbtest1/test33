package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the DSS_PROV_TYPE_PEER_XREF database table.
 * 
 */
@Entity
@Table(name = "DSS_PROV_TYPE_PEER_XREF")
public class DssProvTypePeerXref implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "PROVIDER_PEER_GROUP_ID", referencedColumnName = "PROVIDER_PEER_GROUP_ID"),
			@JoinColumn(name = "YEAR_END_DATE", referencedColumnName = "YEAR_END_DATE") })
	private DssProviderPeerGroup dssProviderPeerGroup1;

	@OneToOne
	@JoinColumn(name="PROVIDER_TYPE") 
	private DssProviderType dptp;

	@EmbeddedId
	private DssProvTypePeerXrefPK id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssProvTypePeerXref() {
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public DssProvTypePeerXrefPK getId() {
		return id;
	}

	public void setId(DssProvTypePeerXrefPK id) {
		this.id = id;
	}

	public DssProviderPeerGroup getDssProviderPeerGroup1() {
		return dssProviderPeerGroup1;
	}

	public void setDssProviderPeerGroup1(DssProviderPeerGroup dppg) {
		this.dssProviderPeerGroup1 = dppg;
	}

	public DssProviderType getDptp() {
		return dptp;
	}

	public void setDptp(DssProviderType dptp) {
		this.dptp = dptp;
	}

}