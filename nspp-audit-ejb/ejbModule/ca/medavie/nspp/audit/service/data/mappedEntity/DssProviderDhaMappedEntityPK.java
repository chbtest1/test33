package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DssProviderDhaMappedEntityPK implements Serializable {

	/** IDE Generated */
	private static final long serialVersionUID = 6341051856931176126L;

	@Column
	private Long pdha_id;
	@Column
	private Long provider_number;
	@Column
	private String provider_type;
	@Column
	private String provider_name;
	@Column
	private String contract_town_name;
	@Column
	private String zone_id;
	@Column
	private String zone_name;
	@Column
	private String dha_code;
	@Column
	private String dha_desc;
	@Column
	private String emp_type;
	@Column
	private BigDecimal fte_value;
	@Column
	private Long bus_arr_number;	
	@Column
	private String modified_by;
	@Column
	private Date last_modified;
	@Column
	private Long provider_group_id;
	@Column
	private String provider_group_description;

	/**
	 * @return the pdha_id
	 */
	public Long getPdha_id() {
		return pdha_id;
	}

	/**
	 * @param pdha_id
	 *            the pdha_id to set
	 */
	public void setPdha_id(Long pdha_id) {
		this.pdha_id = pdha_id;
	}

	/**
	 * @return the provider_number
	 */
	public Long getProvider_number() {
		return provider_number;
	}

	/**
	 * @param provider_number
	 *            the provider_number to set
	 */
	public void setProvider_number(Long provider_number) {
		this.provider_number = provider_number;
	}

	/**
	 * @return the provider_type
	 */
	public String getProvider_type() {
		return provider_type;
	}

	/**
	 * @param provider_type
	 *            the provider_type to set
	 */
	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}

	/**
	 * @return the provider_name
	 */
	public String getProvider_name() {
		return provider_name;
	}

	/**
	 * @param provider_name
	 *            the provider_name to set
	 */
	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}

	public Long getProvider_group_id() {
		return provider_group_id;
	}

	public void setProvider_group_id(Long provider_group_id) {
		this.provider_group_id = provider_group_id;
	}

	public String getProvider_group_description() {
		return provider_group_description;
	}

	public void setProvider_group_description(String provider_group_description) {
		this.provider_group_description = provider_group_description;
	}

	/**
	 * @return the contract_town_name
	 */
	public String getContract_town_name() {
		return contract_town_name;
	}

	/**
	 * @param contract_town_name
	 *            the contract_town_name to set
	 */
	public void setContract_town_name(String contract_town_name) {
		this.contract_town_name = contract_town_name;
	}

	/**
	 * @return the zone_id
	 */
	public String getZone_id() {
		return zone_id;
	}

	/**
	 * @param zone_id
	 *            the zone_id to set
	 */
	public void setZone_id(String zone_id) {
		this.zone_id = zone_id;
	}

	/**
	 * @return the zone_name
	 */
	public String getZone_name() {
		return zone_name;
	}

	/**
	 * @param zone_name
	 *            the zone_name to set
	 */
	public void setZone_name(String zone_name) {
		this.zone_name = zone_name;
	}

	/**
	 * @return the dha_code
	 */
	public String getDha_code() {
		return dha_code;
	}

	/**
	 * @param dha_code
	 *            the dha_code to set
	 */
	public void setDha_code(String dha_code) {
		this.dha_code = dha_code;
	}

	/**
	 * @return the dha_desc
	 */
	public String getDha_desc() {
		return dha_desc;
	}

	/**
	 * @param dha_desc
	 *            the dha_desc to set
	 */
	public void setDha_desc(String dha_desc) {
		this.dha_desc = dha_desc;
	}

	/**
	 * @return the emp_type
	 */
	public String getEmp_type() {
		return emp_type;
	}

	/**
	 * @param emp_type
	 *            the emp_type to set
	 */
	public void setEmp_type(String emp_type) {
		this.emp_type = emp_type;
	}

	/**
	 * @return the fte_value
	 */
	public BigDecimal getFte_value() {
		return fte_value;
	}

	/**
	 * @param fte_value
	 *            the fte_value to set
	 */
	public void setFte_value(BigDecimal fte_value) {
		this.fte_value = fte_value;
	}

	public Long getBus_arr_number() {
		return bus_arr_number;
	}

	public void setBus_arr_number(Long bus_arr_number) {
		this.bus_arr_number = bus_arr_number;
	}

	/**
	 * @return the modified_by
	 */
	public String getModified_by() {
		return modified_by;
	}

	/**
	 * @param modified_by
	 *            the modified_by to set
	 */
	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	/**
	 * @return the last_modified
	 */
	public Date getLast_modified() {
		return last_modified;
	}

	/**
	 * @param last_modified
	 *            the last_modified to set
	 */
	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((provider_group_description == null) ? 0 : provider_group_description.hashCode());
		result = prime * result + ((provider_group_id == null) ? 0 : provider_group_id.hashCode());
		result = prime * result + ((contract_town_name == null) ? 0 : contract_town_name.hashCode());
		result = prime * result + ((dha_code == null) ? 0 : dha_code.hashCode());
		result = prime * result + ((dha_desc == null) ? 0 : dha_desc.hashCode());
		result = prime * result + ((emp_type == null) ? 0 : emp_type.hashCode());
		result = prime * result + ((fte_value == null) ? 0 : fte_value.hashCode());
		result = prime * result + ((last_modified == null) ? 0 : last_modified.hashCode());
		result = prime * result + ((modified_by == null) ? 0 : modified_by.hashCode());
		result = prime * result + ((pdha_id == null) ? 0 : pdha_id.hashCode());
		result = prime * result + ((provider_name == null) ? 0 : provider_name.hashCode());
		result = prime * result + ((provider_number == null) ? 0 : provider_number.hashCode());
		result = prime * result + ((provider_type == null) ? 0 : provider_type.hashCode());
		result = prime * result + ((zone_id == null) ? 0 : zone_id.hashCode());
		result = prime * result + ((zone_name == null) ? 0 : zone_name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssProviderDhaMappedEntityPK other = (DssProviderDhaMappedEntityPK) obj;
		if (provider_group_description == null) {
			if (other.provider_group_description != null)
				return false;
		} else if (!provider_group_description.equals(other.provider_group_description))
			return false;
		if (provider_group_id == null) {
			if (other.provider_group_id != null)
				return false;
		} else if (!provider_group_id.equals(other.provider_group_id))
			return false;
		if (contract_town_name == null) {
			if (other.contract_town_name != null)
				return false;
		} else if (!contract_town_name.equals(other.contract_town_name))
			return false;
		if (dha_code == null) {
			if (other.dha_code != null)
				return false;
		} else if (!dha_code.equals(other.dha_code))
			return false;
		if (dha_desc == null) {
			if (other.dha_desc != null)
				return false;
		} else if (!dha_desc.equals(other.dha_desc))
			return false;
		if (emp_type == null) {
			if (other.emp_type != null)
				return false;
		} else if (!emp_type.equals(other.emp_type))
			return false;
		if (fte_value == null) {
			if (other.fte_value != null)
				return false;
		} else if (!fte_value.equals(other.fte_value))
			return false;
		if (last_modified == null) {
			if (other.last_modified != null)
				return false;
		} else if (!last_modified.equals(other.last_modified))
			return false;
		if (modified_by == null) {
			if (other.modified_by != null)
				return false;
		} else if (!modified_by.equals(other.modified_by))
			return false;
		if (pdha_id == null) {
			if (other.pdha_id != null)
				return false;
		} else if (!pdha_id.equals(other.pdha_id))
			return false;
		if (provider_name == null) {
			if (other.provider_name != null)
				return false;
		} else if (!provider_name.equals(other.provider_name))
			return false;
		if (provider_number == null) {
			if (other.provider_number != null)
				return false;
		} else if (!provider_number.equals(other.provider_number))
			return false;
		if (provider_type == null) {
			if (other.provider_type != null)
				return false;
		} else if (!provider_type.equals(other.provider_type))
			return false;
		if (zone_id == null) {
			if (other.zone_id != null)
				return false;
		} else if (!zone_id.equals(other.zone_id))
			return false;
		if (zone_name == null) {
			if (other.zone_name != null)
				return false;
		} else if (!zone_name.equals(other.zone_name))
			return false;
		return true;
	}

}
