package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_HEALTH_SERVICE_CODE database table.
 * 
 */
@Embeddable
public class DssHealthServiceCodePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="HEALTH_SERVICE_CODE")
	private String healthServiceCode;

	@Column(name="QUALIFIER_CODE")
	private String qualifierCode;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_FROM_DATE")
	private java.util.Date effectiveFromDate;

	public DssHealthServiceCodePK() {
	}
	public String getHealthServiceCode() {
		return this.healthServiceCode;
	}
	public void setHealthServiceCode(String healthServiceCode) {
		this.healthServiceCode = healthServiceCode;
	}
	public String getQualifierCode() {
		return this.qualifierCode;
	}
	public void setQualifierCode(String qualifierCode) {
		this.qualifierCode = qualifierCode;
	}
	public java.util.Date getEffectiveFromDate() {
		return this.effectiveFromDate;
	}
	public void setEffectiveFromDate(java.util.Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssHealthServiceCodePK)) {
			return false;
		}
		DssHealthServiceCodePK castOther = (DssHealthServiceCodePK)other;
		return 
			this.healthServiceCode.equals(castOther.healthServiceCode)
			&& this.qualifierCode.equals(castOther.qualifierCode)
			&& this.effectiveFromDate.equals(castOther.effectiveFromDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.healthServiceCode.hashCode();
		hash = hash * prime + this.qualifierCode.hashCode();
		hash = hash * prime + this.effectiveFromDate.hashCode();
		
		return hash;
	}
}