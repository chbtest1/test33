package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_HEALTH_SERVICE_PEER_XREF database table.
 * 
 */
@Embeddable
public class DssHealthServicePeerXrefPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="PROVIDER_PEER_GROUP_ID")
	private long providerPeerGroupId;

	@Temporal(TemporalType.DATE)
	@Column(name="YEAR_END_DATE")
	private java.util.Date yearEndDate;

	// There should be an attention for this colun as it is defiened as char(6) in the database
	// however, it has to be set as Long here to deal with JPA mapping problem. but it seems like breaking other part of the app!!!
	// http://issues.medavie.bluecross.ca:8080/browse/JMAAX-6605
	@Column(name="HEALTH_SERVICE_ID")
	private String healthServiceId;

	public DssHealthServicePeerXrefPK() {
	}
	public long getProviderPeerGroupId() {
		return this.providerPeerGroupId;
	}
	public void setProviderPeerGroupId(long providerPeerGroupId) {
		this.providerPeerGroupId = providerPeerGroupId;
	}
	public java.util.Date getYearEndDate() {
		return this.yearEndDate;
	}
	public void setYearEndDate(java.util.Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}
	public String getHealthServiceId() {
		return this.healthServiceId;
	}
	public void setHealthServiceId(String healthServiceId) {
		this.healthServiceId = healthServiceId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((healthServiceId == null) ? 0 : healthServiceId.hashCode());
		result = prime * result + (int) (providerPeerGroupId ^ (providerPeerGroupId >>> 32));
		result = prime * result + ((yearEndDate == null) ? 0 : yearEndDate.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssHealthServicePeerXrefPK other = (DssHealthServicePeerXrefPK) obj;
		if (healthServiceId == null) {
			if (other.healthServiceId != null)
				return false;
		} else if (!healthServiceId.equals(other.healthServiceId))
			return false;
		if (providerPeerGroupId != other.providerPeerGroupId)
			return false;
		if (yearEndDate == null) {
			if (other.yearEndDate != null)
				return false;
		} else if (!yearEndDate.equals(other.yearEndDate))
			return false;
		return true;
	}
	
	
	
}