package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_MED_PROV_SUMMARY database table.
 * 
 */
@Embeddable
public class DssMedProvSummaryPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="PROVIDER_PEER_GROUP_ID")
	private long providerPeerGroupId;

	@Column(name="PROVIDER_NUMBER")
	private long providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	@Temporal(TemporalType.DATE)
	@Column(name="YEAR_END_DATE")
	private java.util.Date yearEndDate;

	@Column(name="PERIOD_ID")
	private long periodId;

	@Column(name="SHADOW_BILLING_INDICATOR")
	private String shadowBillingIndicator;

	public DssMedProvSummaryPK() {
	}
	public long getProviderPeerGroupId() {
		return this.providerPeerGroupId;
	}
	public void setProviderPeerGroupId(long providerPeerGroupId) {
		this.providerPeerGroupId = providerPeerGroupId;
	}
	public long getProviderNumber() {
		return this.providerNumber;
	}
	public void setProviderNumber(long providerNumber) {
		this.providerNumber = providerNumber;
	}
	public String getProviderType() {
		return this.providerType;
	}
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}
	public java.util.Date getYearEndDate() {
		return this.yearEndDate;
	}
	public void setYearEndDate(java.util.Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}
	public long getPeriodId() {
		return this.periodId;
	}
	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}
	public String getShadowBillingIndicator() {
		return this.shadowBillingIndicator;
	}
	public void setShadowBillingIndicator(String shadowBillingIndicator) {
		this.shadowBillingIndicator = shadowBillingIndicator;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssMedProvSummaryPK)) {
			return false;
		}
		DssMedProvSummaryPK castOther = (DssMedProvSummaryPK)other;
		return 
			(this.providerPeerGroupId == castOther.providerPeerGroupId)
			&& (this.providerNumber == castOther.providerNumber)
			&& this.providerType.equals(castOther.providerType)
			&& this.yearEndDate.equals(castOther.yearEndDate)
			&& (this.periodId == castOther.periodId)
			&& this.shadowBillingIndicator.equals(castOther.shadowBillingIndicator);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.providerPeerGroupId ^ (this.providerPeerGroupId >>> 32)));
		hash = hash * prime + ((int) (this.providerNumber ^ (this.providerNumber >>> 32)));
		hash = hash * prime + this.providerType.hashCode();
		hash = hash * prime + this.yearEndDate.hashCode();
		hash = hash * prime + ((int) (this.periodId ^ (this.periodId >>> 32)));
		hash = hash * prime + this.shadowBillingIndicator.hashCode();
		
		return hash;
	}
}