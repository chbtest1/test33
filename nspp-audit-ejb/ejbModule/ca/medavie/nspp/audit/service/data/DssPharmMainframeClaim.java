package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_PHARM_MAINFRAME_CLAIMS database table.
 * 
 */
@Entity
@Table(name="DSS_PHARM_MAINFRAME_CLAIMS")
public class DssPharmMainframeClaim implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MAIN_SEQ_NUMBER")
	private long mainSeqNumber;

	@Column(name="MAIN_ABBREVIATED_DRUG_NAME")
	private String mainAbbreviatedDrugName;

	@Column(name="MAIN_ADJUSTMENT_CODE")
	private BigDecimal mainAdjustmentCode;

	@Column(name="MAIN_AMOUNT_CLAIMED")
	private BigDecimal mainAmountClaimed;

	@Column(name="MAIN_APPROVED")
	private BigDecimal mainApproved;

	@Column(name="MAIN_APPROVED_DRUG_COST")
	private BigDecimal mainApprovedDrugCost;

	@Column(name="MAIN_CATEGORY_OF_ASSISTANCE")
	private BigDecimal mainCategoryOfAssistance;

	@Column(name="MAIN_CLAIM_NUMBER")
	private BigDecimal mainClaimNumber;

	@Column(name="MAIN_COPAY_PAID")
	private BigDecimal mainCopayPaid;

	@Temporal(TemporalType.DATE)
	@Column(name="MAIN_DATE_OF_SERVICE")
	private Date mainDateOfService;

	@Column(name="MAIN_DAYS_SUPPLY")
	private BigDecimal mainDaysSupply;

	@Column(name="MAIN_DIN")
	private BigDecimal mainDin;

	@Column(name="MAIN_DISPENSING_FEE")
	private BigDecimal mainDispensingFee;

	@Column(name="MAIN_DRUG_CLASS")
	private BigDecimal mainDrugClass;

	@Column(name="MAIN_DRUG_TYPE_CODE")
	private String mainDrugTypeCode;

	@Column(name="MAIN_GIS_INDICATOR")
	private BigDecimal mainGisIndicator;

	@Column(name="MAIN_HEALTH_CARD_NUMBER")
	private BigDecimal mainHealthCardNumber;

	@Column(name="MAIN_MAC_DRUG_INDICATOR")
	private String mainMacDrugIndicator;

	@Column(name="MAIN_OR_INDICATOR")
	private String mainOrIndicator;

	@Column(name="MAIN_PERIOD_ID")
	private String mainPeriodId;

	@Column(name="MAIN_PLAN_INDICATOR")
	private BigDecimal mainPlanIndicator;

	@Column(name="MAIN_PRESCRIBER_NUMBER")
	private BigDecimal mainPrescriberNumber;

	@Column(name="MAIN_PRESCRIPTION_NUMBER")
	private String mainPrescriptionNumber;

	@Column(name="MAIN_PROVIDER_COUNTY_CODE")
	private BigDecimal mainProviderCountyCode;

	@Column(name="MAIN_PROVIDER_NUMBER")
	private BigDecimal mainProviderNumber;

	@Column(name="MAIN_PROVIDER_STATUS_CODE")
	private BigDecimal mainProviderStatusCode;

	@Column(name="MAIN_QUANTITY")
	private BigDecimal mainQuantity;

	@Column(name="MAIN_RECIPIENT_BIRTH_YEAR")
	private BigDecimal mainRecipientBirthYear;

	@Column(name="MAIN_RECIPIENT_COUNTY_CODE")
	private BigDecimal mainRecipientCountyCode;

	@Column(name="MAIN_RECIPIENT_GENDER")
	private String mainRecipientGender;

	@Column(name="MAIN_RECIPIENT_NAME")
	private String mainRecipientName;

	@Column(name="MAIN_REFILLS")
	private BigDecimal mainRefills;

	@Column(name="MAIN_RUN_NUMBER")
	private String mainRunNumber;

	@Column(name="MAIN_RX_COUNT")
	private BigDecimal mainRxCount;

	@Column(name="MAIN_TRANSACTION_CODE")
	private BigDecimal mainTransactionCode;

	public DssPharmMainframeClaim() {
	}

	public long getMainSeqNumber() {
		return this.mainSeqNumber;
	}

	public void setMainSeqNumber(long mainSeqNumber) {
		this.mainSeqNumber = mainSeqNumber;
	}

	public String getMainAbbreviatedDrugName() {
		return this.mainAbbreviatedDrugName;
	}

	public void setMainAbbreviatedDrugName(String mainAbbreviatedDrugName) {
		this.mainAbbreviatedDrugName = mainAbbreviatedDrugName;
	}

	public BigDecimal getMainAdjustmentCode() {
		return this.mainAdjustmentCode;
	}

	public void setMainAdjustmentCode(BigDecimal mainAdjustmentCode) {
		this.mainAdjustmentCode = mainAdjustmentCode;
	}

	public BigDecimal getMainAmountClaimed() {
		return this.mainAmountClaimed;
	}

	public void setMainAmountClaimed(BigDecimal mainAmountClaimed) {
		this.mainAmountClaimed = mainAmountClaimed;
	}

	public BigDecimal getMainApproved() {
		return this.mainApproved;
	}

	public void setMainApproved(BigDecimal mainApproved) {
		this.mainApproved = mainApproved;
	}

	public BigDecimal getMainApprovedDrugCost() {
		return this.mainApprovedDrugCost;
	}

	public void setMainApprovedDrugCost(BigDecimal mainApprovedDrugCost) {
		this.mainApprovedDrugCost = mainApprovedDrugCost;
	}

	public BigDecimal getMainCategoryOfAssistance() {
		return this.mainCategoryOfAssistance;
	}

	public void setMainCategoryOfAssistance(BigDecimal mainCategoryOfAssistance) {
		this.mainCategoryOfAssistance = mainCategoryOfAssistance;
	}

	public BigDecimal getMainClaimNumber() {
		return this.mainClaimNumber;
	}

	public void setMainClaimNumber(BigDecimal mainClaimNumber) {
		this.mainClaimNumber = mainClaimNumber;
	}

	public BigDecimal getMainCopayPaid() {
		return this.mainCopayPaid;
	}

	public void setMainCopayPaid(BigDecimal mainCopayPaid) {
		this.mainCopayPaid = mainCopayPaid;
	}

	public Date getMainDateOfService() {
		return this.mainDateOfService;
	}

	public void setMainDateOfService(Date mainDateOfService) {
		this.mainDateOfService = mainDateOfService;
	}

	public BigDecimal getMainDaysSupply() {
		return this.mainDaysSupply;
	}

	public void setMainDaysSupply(BigDecimal mainDaysSupply) {
		this.mainDaysSupply = mainDaysSupply;
	}

	public BigDecimal getMainDin() {
		return this.mainDin;
	}

	public void setMainDin(BigDecimal mainDin) {
		this.mainDin = mainDin;
	}

	public BigDecimal getMainDispensingFee() {
		return this.mainDispensingFee;
	}

	public void setMainDispensingFee(BigDecimal mainDispensingFee) {
		this.mainDispensingFee = mainDispensingFee;
	}

	public BigDecimal getMainDrugClass() {
		return this.mainDrugClass;
	}

	public void setMainDrugClass(BigDecimal mainDrugClass) {
		this.mainDrugClass = mainDrugClass;
	}

	public String getMainDrugTypeCode() {
		return this.mainDrugTypeCode;
	}

	public void setMainDrugTypeCode(String mainDrugTypeCode) {
		this.mainDrugTypeCode = mainDrugTypeCode;
	}

	public BigDecimal getMainGisIndicator() {
		return this.mainGisIndicator;
	}

	public void setMainGisIndicator(BigDecimal mainGisIndicator) {
		this.mainGisIndicator = mainGisIndicator;
	}

	public BigDecimal getMainHealthCardNumber() {
		return this.mainHealthCardNumber;
	}

	public void setMainHealthCardNumber(BigDecimal mainHealthCardNumber) {
		this.mainHealthCardNumber = mainHealthCardNumber;
	}

	public String getMainMacDrugIndicator() {
		return this.mainMacDrugIndicator;
	}

	public void setMainMacDrugIndicator(String mainMacDrugIndicator) {
		this.mainMacDrugIndicator = mainMacDrugIndicator;
	}

	public String getMainOrIndicator() {
		return this.mainOrIndicator;
	}

	public void setMainOrIndicator(String mainOrIndicator) {
		this.mainOrIndicator = mainOrIndicator;
	}

	public String getMainPeriodId() {
		return this.mainPeriodId;
	}

	public void setMainPeriodId(String mainPeriodId) {
		this.mainPeriodId = mainPeriodId;
	}

	public BigDecimal getMainPlanIndicator() {
		return this.mainPlanIndicator;
	}

	public void setMainPlanIndicator(BigDecimal mainPlanIndicator) {
		this.mainPlanIndicator = mainPlanIndicator;
	}

	public BigDecimal getMainPrescriberNumber() {
		return this.mainPrescriberNumber;
	}

	public void setMainPrescriberNumber(BigDecimal mainPrescriberNumber) {
		this.mainPrescriberNumber = mainPrescriberNumber;
	}

	public String getMainPrescriptionNumber() {
		return this.mainPrescriptionNumber;
	}

	public void setMainPrescriptionNumber(String mainPrescriptionNumber) {
		this.mainPrescriptionNumber = mainPrescriptionNumber;
	}

	public BigDecimal getMainProviderCountyCode() {
		return this.mainProviderCountyCode;
	}

	public void setMainProviderCountyCode(BigDecimal mainProviderCountyCode) {
		this.mainProviderCountyCode = mainProviderCountyCode;
	}

	public BigDecimal getMainProviderNumber() {
		return this.mainProviderNumber;
	}

	public void setMainProviderNumber(BigDecimal mainProviderNumber) {
		this.mainProviderNumber = mainProviderNumber;
	}

	public BigDecimal getMainProviderStatusCode() {
		return this.mainProviderStatusCode;
	}

	public void setMainProviderStatusCode(BigDecimal mainProviderStatusCode) {
		this.mainProviderStatusCode = mainProviderStatusCode;
	}

	public BigDecimal getMainQuantity() {
		return this.mainQuantity;
	}

	public void setMainQuantity(BigDecimal mainQuantity) {
		this.mainQuantity = mainQuantity;
	}

	public BigDecimal getMainRecipientBirthYear() {
		return this.mainRecipientBirthYear;
	}

	public void setMainRecipientBirthYear(BigDecimal mainRecipientBirthYear) {
		this.mainRecipientBirthYear = mainRecipientBirthYear;
	}

	public BigDecimal getMainRecipientCountyCode() {
		return this.mainRecipientCountyCode;
	}

	public void setMainRecipientCountyCode(BigDecimal mainRecipientCountyCode) {
		this.mainRecipientCountyCode = mainRecipientCountyCode;
	}

	public String getMainRecipientGender() {
		return this.mainRecipientGender;
	}

	public void setMainRecipientGender(String mainRecipientGender) {
		this.mainRecipientGender = mainRecipientGender;
	}

	public String getMainRecipientName() {
		return this.mainRecipientName;
	}

	public void setMainRecipientName(String mainRecipientName) {
		this.mainRecipientName = mainRecipientName;
	}

	public BigDecimal getMainRefills() {
		return this.mainRefills;
	}

	public void setMainRefills(BigDecimal mainRefills) {
		this.mainRefills = mainRefills;
	}

	public String getMainRunNumber() {
		return this.mainRunNumber;
	}

	public void setMainRunNumber(String mainRunNumber) {
		this.mainRunNumber = mainRunNumber;
	}

	public BigDecimal getMainRxCount() {
		return this.mainRxCount;
	}

	public void setMainRxCount(BigDecimal mainRxCount) {
		this.mainRxCount = mainRxCount;
	}

	public BigDecimal getMainTransactionCode() {
		return this.mainTransactionCode;
	}

	public void setMainTransactionCode(BigDecimal mainTransactionCode) {
		this.mainTransactionCode = mainTransactionCode;
	}

}