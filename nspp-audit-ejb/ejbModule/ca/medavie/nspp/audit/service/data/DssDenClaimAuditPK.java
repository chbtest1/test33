package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_DEN_CLAIM_AUDIT database table.
 * 
 */
@Embeddable
public class DssDenClaimAuditPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CLAIM_NUM")
	private long claimNum;

	@Column(name="AUDIT_RUN_NUMBER")
	private long auditRunNumber;

	@Column(name="ITEM_CODE")
	private long itemCode;

	public DssDenClaimAuditPK() {
	}
	public long getClaimNum() {
		return this.claimNum;
	}
	public void setClaimNum(long claimNum) {
		this.claimNum = claimNum;
	}
	public long getAuditRunNumber() {
		return this.auditRunNumber;
	}
	public void setAuditRunNumber(long auditRunNumber) {
		this.auditRunNumber = auditRunNumber;
	}
	public long getItemCode() {
		return this.itemCode;
	}
	public void setItemCode(long itemCode) {
		this.itemCode = itemCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssDenClaimAuditPK)) {
			return false;
		}
		DssDenClaimAuditPK castOther = (DssDenClaimAuditPK)other;
		return 
			(this.claimNum == castOther.claimNum)
			&& (this.auditRunNumber == castOther.auditRunNumber)
			&& (this.itemCode == castOther.itemCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.claimNum ^ (this.claimNum >>> 32)));
		hash = hash * prime + ((int) (this.auditRunNumber ^ (this.auditRunNumber >>> 32)));
		hash = hash * prime + ((int) (this.itemCode ^ (this.itemCode >>> 32)));
		
		return hash;
	}
}