package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The primary key class for the DSS_HEALTH_SERVICE_GROUP_XREF database table.
 * 
 */
@Embeddable
public class DssHealthServiceGroupXrefPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="HEALTH_SERVICE_GROUP_ID")
	private long healthServiceGroupId;

	@Column(name="HEALTH_SERVICE_ID")
	private String healthServiceId;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_FROM_DATE")
	private java.util.Date effectiveFromDate;

	public DssHealthServiceGroupXrefPK() {
	}
	public long getHealthServiceGroupId() {
		return this.healthServiceGroupId;
	}
	public void setHealthServiceGroupId(long healthServiceGroupId) {
		this.healthServiceGroupId = healthServiceGroupId;
	}
	public String getHealthServiceId() {
		return this.healthServiceId;
	}
	public void setHealthServiceId(String healthServiceId) {
		this.healthServiceId = healthServiceId;
	}
	public java.util.Date getEffectiveFromDate() {
		return this.effectiveFromDate;
	}
	public void setEffectiveFromDate(java.util.Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssHealthServiceGroupXrefPK)) {
			return false;
		}
		DssHealthServiceGroupXrefPK castOther = (DssHealthServiceGroupXrefPK)other;
		return 
			(this.healthServiceGroupId == castOther.healthServiceGroupId)
			&& this.healthServiceId.equals(castOther.healthServiceId)
			&& this.effectiveFromDate.equals(castOther.effectiveFromDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.healthServiceGroupId ^ (this.healthServiceGroupId >>> 32)));
		hash = hash * prime + this.healthServiceId.hashCode();
		hash = hash * prime + this.effectiveFromDate.hashCode();
		
		return hash;
	}
}