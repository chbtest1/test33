package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the DSS_INDIVIDUAL_2 database table.
 * 
 */
@Entity
@Table(name = "DSS_INDIVIDUAL_2")
public class DssIndividual2 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "HEALTH_CARD_NUMBER")
	private String healthCardNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "ADDRESS_EFF_DATE")
	private Date addressEffDate;

	@Column(name = "ADDRESS_LINE1")
	private String addressLine1;

	@Column(name = "ADDRESS_LINE2")
	private String addressLine2;

	@Column(name = "ADDRESS_STATUS_CODE")
	private BigDecimal addressStatusCode;

	@Column(name = "ALTERNATE_FIRSTNAME")
	private String alternateFirstname;

	@Column(name = "ALTERNATE_SURNAME")
	private String alternateSurname;

	@Temporal(TemporalType.DATE)
	@Column(name = "APPLICATION_PRINT_DATE")
	private Date applicationPrintDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "APPLICATION_PROCESSED_DATE")
	private Date applicationProcessedDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "APPLICATION_SENT_DATE")
	private Date applicationSentDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "ARRIVAL_DATE")
	private Date arrivalDate;

	@Column(name = "CERTIFICATE_NUMBER")
	private String certificateNumber;

	@Column(name = "CITY")
	private String city;

	@Column(name = "COMMUNITY_SERVICES_ID")
	private String communityServicesId;

	@Column(name = "COUNTRY")
	private String country;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATE_OF_BIRTH")
	private Date dateOfBirth;

	@Temporal(TemporalType.DATE)
	@Column(name = "DEATH_DATE")
	private Date deathDate;

	@Column(name = "DEATH_DATE_SOURCE_CODE")
	private BigDecimal deathDateSourceCode;

	@Temporal(TemporalType.DATE)
	@Column(name = "DEPARTURE_DATE")
	private Date departureDate;

	@Column(name = "DO_NOT_UPDATE_CODE")
	private BigDecimal doNotUpdateCode;

	@Column(name = "DUPLICATE_HCN")
	private String duplicateHcn;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "GENDER")
	private String gender;

	@Column(name = "HEAD_OF_ADDRESS_HCN")
	private String headOfAddressHcn;

	@Temporal(TemporalType.DATE)
	@Column(name = "HEALTH_CARD_EXPIRY_DATE")
	private Date healthCardExpiryDate;

	@Column(name = "HEALTH_CARD_SEQ_NUMBER")
	private BigDecimal healthCardSeqNumber;

	@Column(name = "HOME_PHONE_NUMBER")
	private String homePhoneNumber;

	@Column(name = "INDIVIDUAL_NOTE")
	private String individualNote;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "MOVED_FROM_CARD_NUMBER")
	private String movedFromCardNumber;

	@Column(name = "MOVED_FROM_INDICATOR")
	private String movedFromIndicator;

	@Column(name = "MOVED_TO_CARD_NUMBER")
	private String movedToCardNumber;

	@Column(name = "OLD_MSI_NUMBER")
	private String oldMsiNumber;

	@Column(name = "ORGAN_DONOR_INDICATOR")
	private String organDonorIndicator;

	@Column(name = "POSTAL_CODE")
	private String postalCode;

	@Column(name = "PROVINCE_CODE")
	private String provinceCode;

	@Column(name = "PROVINCE_MOVED_FROM")
	private String provinceMovedFrom;

	@Column(name = "PROVINCE_MOVED_TO")
	private String provinceMovedTo;

	@Temporal(TemporalType.DATE)
	@Column(name = "RENEWAL_RECEIVED_DATE")
	private Date renewalReceivedDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "RENEWAL_SENT_DATE")
	private Date renewalSentDate;

	@Column(name = "SEARCH_NAME")
	private String searchName;

	@Column(name = "SURNAME")
	private String surname;

	@Column(name = "VALID_ADDRESS_INFO_INDICATOR")
	private String validAddressInfoIndicator;

	@Column(name = "WORK_PHONE_NUMBER")
	private String workPhoneNumber;

	public DssIndividual2() {
	}

	public String getHealthCardNumber() {
		return this.healthCardNumber;
	}

	public void setHealthCardNumber(String healthCardNumber) {
		this.healthCardNumber = healthCardNumber;
	}

	public Date getAddressEffDate() {
		return this.addressEffDate;
	}

	public void setAddressEffDate(Date addressEffDate) {
		this.addressEffDate = addressEffDate;
	}

	public String getAddressLine1() {
		return this.addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return this.addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public BigDecimal getAddressStatusCode() {
		return this.addressStatusCode;
	}

	public void setAddressStatusCode(BigDecimal addressStatusCode) {
		this.addressStatusCode = addressStatusCode;
	}

	public String getAlternateFirstname() {
		return this.alternateFirstname;
	}

	public void setAlternateFirstname(String alternateFirstname) {
		this.alternateFirstname = alternateFirstname;
	}

	public String getAlternateSurname() {
		return this.alternateSurname;
	}

	public void setAlternateSurname(String alternateSurname) {
		this.alternateSurname = alternateSurname;
	}

	public Date getApplicationPrintDate() {
		return this.applicationPrintDate;
	}

	public void setApplicationPrintDate(Date applicationPrintDate) {
		this.applicationPrintDate = applicationPrintDate;
	}

	public Date getApplicationProcessedDate() {
		return this.applicationProcessedDate;
	}

	public void setApplicationProcessedDate(Date applicationProcessedDate) {
		this.applicationProcessedDate = applicationProcessedDate;
	}

	public Date getApplicationSentDate() {
		return this.applicationSentDate;
	}

	public void setApplicationSentDate(Date applicationSentDate) {
		this.applicationSentDate = applicationSentDate;
	}

	public Date getArrivalDate() {
		return this.arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getCertificateNumber() {
		return this.certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCommunityServicesId() {
		return this.communityServicesId;
	}

	public void setCommunityServicesId(String communityServicesId) {
		this.communityServicesId = communityServicesId;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDeathDate() {
		return this.deathDate;
	}

	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}

	public BigDecimal getDeathDateSourceCode() {
		return this.deathDateSourceCode;
	}

	public void setDeathDateSourceCode(BigDecimal deathDateSourceCode) {
		this.deathDateSourceCode = deathDateSourceCode;
	}

	public Date getDepartureDate() {
		return this.departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public BigDecimal getDoNotUpdateCode() {
		return this.doNotUpdateCode;
	}

	public void setDoNotUpdateCode(BigDecimal doNotUpdateCode) {
		this.doNotUpdateCode = doNotUpdateCode;
	}

	public String getDuplicateHcn() {
		return this.duplicateHcn;
	}

	public void setDuplicateHcn(String duplicateHcn) {
		this.duplicateHcn = duplicateHcn;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHeadOfAddressHcn() {
		return this.headOfAddressHcn;
	}

	public void setHeadOfAddressHcn(String headOfAddressHcn) {
		this.headOfAddressHcn = headOfAddressHcn;
	}

	public Date getHealthCardExpiryDate() {
		return this.healthCardExpiryDate;
	}

	public void setHealthCardExpiryDate(Date healthCardExpiryDate) {
		this.healthCardExpiryDate = healthCardExpiryDate;
	}

	public BigDecimal getHealthCardSeqNumber() {
		return this.healthCardSeqNumber;
	}

	public void setHealthCardSeqNumber(BigDecimal healthCardSeqNumber) {
		this.healthCardSeqNumber = healthCardSeqNumber;
	}

	public String getHomePhoneNumber() {
		return this.homePhoneNumber;
	}

	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	public String getIndividualNote() {
		return this.individualNote;
	}

	public void setIndividualNote(String individualNote) {
		this.individualNote = individualNote;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getMovedFromCardNumber() {
		return this.movedFromCardNumber;
	}

	public void setMovedFromCardNumber(String movedFromCardNumber) {
		this.movedFromCardNumber = movedFromCardNumber;
	}

	public String getMovedFromIndicator() {
		return this.movedFromIndicator;
	}

	public void setMovedFromIndicator(String movedFromIndicator) {
		this.movedFromIndicator = movedFromIndicator;
	}

	public String getMovedToCardNumber() {
		return this.movedToCardNumber;
	}

	public void setMovedToCardNumber(String movedToCardNumber) {
		this.movedToCardNumber = movedToCardNumber;
	}

	public String getOldMsiNumber() {
		return this.oldMsiNumber;
	}

	public void setOldMsiNumber(String oldMsiNumber) {
		this.oldMsiNumber = oldMsiNumber;
	}

	public String getOrganDonorIndicator() {
		return this.organDonorIndicator;
	}

	public void setOrganDonorIndicator(String organDonorIndicator) {
		this.organDonorIndicator = organDonorIndicator;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getProvinceCode() {
		return this.provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getProvinceMovedFrom() {
		return this.provinceMovedFrom;
	}

	public void setProvinceMovedFrom(String provinceMovedFrom) {
		this.provinceMovedFrom = provinceMovedFrom;
	}

	public String getProvinceMovedTo() {
		return this.provinceMovedTo;
	}

	public void setProvinceMovedTo(String provinceMovedTo) {
		this.provinceMovedTo = provinceMovedTo;
	}

	public Date getRenewalReceivedDate() {
		return this.renewalReceivedDate;
	}

	public void setRenewalReceivedDate(Date renewalReceivedDate) {
		this.renewalReceivedDate = renewalReceivedDate;
	}

	public Date getRenewalSentDate() {
		return this.renewalSentDate;
	}

	public void setRenewalSentDate(Date renewalSentDate) {
		this.renewalSentDate = renewalSentDate;
	}

	public String getSearchName() {
		return this.searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getValidAddressInfoIndicator() {
		return this.validAddressInfoIndicator;
	}

	public void setValidAddressInfoIndicator(String validAddressInfoIndicator) {
		this.validAddressInfoIndicator = validAddressInfoIndicator;
	}

	public String getWorkPhoneNumber() {
		return this.workPhoneNumber;
	}

	public void setWorkPhoneNumber(String workPhoneNumber) {
		this.workPhoneNumber = workPhoneNumber;
	}
}