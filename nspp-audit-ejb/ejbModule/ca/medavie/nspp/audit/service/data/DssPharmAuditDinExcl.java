package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the DSS_PHARM_AUDIT_DIN_Excl database table.
 * 
 */
@Entity
@SqlResultSetMapping(name = "DssDinExclusionNativeQueryMapping", entities = { @EntityResult(entityClass = DssPharmAuditDinExcl.class, fields = {
	@FieldResult(name = "id.din", column = "din")}) })
@Table(name="DSS_PHARM_AUDIT_DIN_EXCL")
public class DssPharmAuditDinExcl implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssPharmAuditDinExclPK id;
	
	// bi-directional one-to-one association to DssDin
	@OneToOne
	@JoinColumn(name = "DIN")
	private DssDin dssDin;

	@Column
	private BigDecimal age_restriction;

	@Column
	private String gender_restriction;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date last_modified;

	@Column
	private String modified_by;
	
	public DssPharmAuditDinExcl() {
	}

	public DssPharmAuditDinExclPK getId() {
		return this.id;
	}

	public void setId(DssPharmAuditDinExclPK id) {
		this.id = id;
	}

	public DssDin getDssDin() {
		return dssDin;
	}

	public void setDssDin(DssDin dssDin) {
		this.dssDin = dssDin;
	}

	public BigDecimal getAge_restriction() {
		return age_restriction;
	}

	public void setAge_restriction(BigDecimal age_restriction) {
		this.age_restriction = age_restriction;
	}

	public String getGender_restriction() {
		return gender_restriction;
	}

	public void setGender_restriction(String gender_restriction) {
		this.gender_restriction = gender_restriction;
	}

	public Date getLast_modified() {
		return last_modified;
	}

	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}

	public String getModified_by() {
		return modified_by;
	}

	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}
}