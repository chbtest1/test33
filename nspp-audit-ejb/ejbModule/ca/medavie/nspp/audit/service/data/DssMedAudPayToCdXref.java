package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DSS_MED_AUD_PAY_TO_CD_XREF database table.
 * 
 */
@Entity
@Table(name="DSS_MED_AUD_PAY_TO_CD_XREF")
public class DssMedAudPayToCdXref implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssMedAudPayToCdXrefPK id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	//bi-directional many-to-one association to DssMedAudCriteria
	@ManyToOne
	@JoinColumn(name="AUDIT_CRITERIA_ID")
	private DssMedAudCriteria dssMedAudCriteria;

	public DssMedAudPayToCdXref() {
	}

	public DssMedAudPayToCdXrefPK getId() {
		return this.id;
	}

	public void setId(DssMedAudPayToCdXrefPK id) {
		this.id = id;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public DssMedAudCriteria getDssMedAudCriteria() {
		return this.dssMedAudCriteria;
	}

	public void setDssMedAudCriteria(DssMedAudCriteria dssMedAudCriteria) {
		this.dssMedAudCriteria = dssMedAudCriteria;
	}

}