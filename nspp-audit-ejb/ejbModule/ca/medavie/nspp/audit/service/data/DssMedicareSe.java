package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_MEDICARE_SE database table.
 * 
 */
@Entity
@Table(name="DSS_MEDICARE_SE")
public class DssMedicareSe implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssMedicareSePK id;

	@Column(name="ACTION_CODE")
	private String actionCode;

	@Column(name="AMOUNT_PAID")
	private BigDecimal amountPaid;

	@Column(name="BASE_AMOUNT")
	private BigDecimal baseAmount;

	@Column(name="BASE_UNITS")
	private BigDecimal baseUnits;

	@Column(name="BUS_ARR_NUMBER")
	private BigDecimal busArrNumber;

	@Column(name="CATEGORY_CODE")
	private String categoryCode;

	@Column(name="CHANGED_SEQUENCE_NUMBER")
	private BigDecimal changedSequenceNumber;

	@Column(name="CHART_NUMBER")
	private String chartNumber;

	@Column(name="DIAGNOSTIC_CODE1")
	private String diagnosticCode1;

	@Column(name="DIAGNOSTIC_CODE2")
	private String diagnosticCode2;

	@Column(name="DIAGNOSTIC_CODE3")
	private String diagnosticCode3;

	@Column(name="FACILITY_CODE")
	private BigDecimal facilityCode;

	@Column(name="FUNCTIONAL_CENTER_CODE")
	private String functionalCenterCode;

	@Column(name="HEALTH_SERVICE_CODE")
	private String healthServiceCode;

	@Column(name="HEALTH_SERVICE_ID")
	private String healthServiceId;

	@Temporal(TemporalType.DATE)
	@Column(name="HOSPITAL_ADMIT_DATE")
	private Date hospitalAdmitDate;

	@Temporal(TemporalType.DATE)
	@Column(name="ICU_ADMIT_DATE")
	private Date icuAdmitDate;

	@Column(name="INJURY_DIAGNOSTIC_CODE")
	private String injuryDiagnosticCode;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="LOCATION_CODE")
	private String locationCode;

	@Column(name="LOCATION_POSTAL_CODE")
	private String locationPostalCode;

	private BigDecimal multiples;

	@Column(name="OTHER_PAYEE_ID")
	private BigDecimal otherPayeeId;

	@Column(name="OUT_OF_PROV_REFERRAL_INDICATOR")
	private String outOfProvReferralIndicator;

	@Column(name="PATIENT_AGE_YEARS")
	private BigDecimal patientAgeYears;

	@Temporal(TemporalType.DATE)
	@Column(name="PATIENT_BIRTH_DATE")
	private Date patientBirthDate;

	@Column(name="PATIENT_GENDER")
	private String patientGender;

	@Column(name="PATIENT_HEALTH_CARD_NUMBER")
	private String patientHealthCardNumber;

	@Column(name="PATIENT_POSTAL_CODE")
	private String patientPostalCode;

	@Column(name="PAY_TO_CODE")
	private String payToCode;

	@Column(name="PAY_TO_HEALTH_CARD_NUMBER")
	private BigDecimal payToHealthCardNumber;

	@Column(name="PAY_TO_PROVIDER_GROUP_ID")
	private BigDecimal payToProviderGroupId;

	@Column(name="PAYABLE_ID")
	private BigDecimal payableId;

	@Temporal(TemporalType.DATE)
	@Column(name="PAYMENT_DATE")
	private Date paymentDate;

	@Column(name="PAYMENT_RESPONSIBILITY")
	private String paymentResponsibility;

	@Column(name="PREAUTHORIZATION_INDICATOR")
	private String preauthorizationIndicator;

	@Column(name="PROGRAM_CODE")
	private String programCode;

	@Column(name="PROVIDER_ACCUM_SPECIALTY_CODE")
	private String providerAccumSpecialtyCode;

	@Column(name="PROVIDER_NUMBER")
	private BigDecimal providerNumber;

	@Column(name="PROVIDER_POSTAL_CODE")
	private String providerPostalCode;

	@Column(name="PROVIDER_ROLE")
	private String providerRole;

	@Column(name="PROVIDER_SPECIALTY_CODE")
	private String providerSpecialtyCode;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	@Column(name="QUALIFIER_CODE")
	private String qualifierCode;

	@Column(name="QUARTER_NUMBER")
	private BigDecimal quarterNumber;

	@Column(name="REDUCED_UNIT_INDICATOR")
	private String reducedUnitIndicator;

	@Column(name="REFG_PROVIDER_NUMBER")
	private BigDecimal refgProviderNumber;

	@Column(name="REFG_PROVIDER_SPECIALTY_CODE")
	private String refgProviderSpecialtyCode;

	@Column(name="REFG_PROVIDER_TYPE")
	private String refgProviderType;

	@Column(name="REMUNERATION_METHOD")
	private BigDecimal remunerationMethod;

	@Column(name="SERVICE_OCCURENCE_NUMBER")
	private BigDecimal serviceOccurenceNumber;

	@Temporal(TemporalType.DATE)
	@Column(name="SERVICE_START_DATE")
	private Date serviceStartDate;

	@Column(name="SHADOW_EARNINGS")
	private BigDecimal shadowEarnings;

	@Column(name="SHADOW_UNITS")
	private BigDecimal shadowUnits;

	@Temporal(TemporalType.DATE)
	@Column(name="YEAR_END_DATE")
	private Date yearEndDate;

	public DssMedicareSe() {
	}

	public DssMedicareSePK getId() {
		return this.id;
	}

	public void setId(DssMedicareSePK id) {
		this.id = id;
	}

	public String getActionCode() {
		return this.actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public BigDecimal getAmountPaid() {
		return this.amountPaid;
	}

	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	public BigDecimal getBaseAmount() {
		return this.baseAmount;
	}

	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}

	public BigDecimal getBaseUnits() {
		return this.baseUnits;
	}

	public void setBaseUnits(BigDecimal baseUnits) {
		this.baseUnits = baseUnits;
	}

	public BigDecimal getBusArrNumber() {
		return this.busArrNumber;
	}

	public void setBusArrNumber(BigDecimal busArrNumber) {
		this.busArrNumber = busArrNumber;
	}

	public String getCategoryCode() {
		return this.categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public BigDecimal getChangedSequenceNumber() {
		return this.changedSequenceNumber;
	}

	public void setChangedSequenceNumber(BigDecimal changedSequenceNumber) {
		this.changedSequenceNumber = changedSequenceNumber;
	}

	public String getChartNumber() {
		return this.chartNumber;
	}

	public void setChartNumber(String chartNumber) {
		this.chartNumber = chartNumber;
	}

	public String getDiagnosticCode1() {
		return this.diagnosticCode1;
	}

	public void setDiagnosticCode1(String diagnosticCode1) {
		this.diagnosticCode1 = diagnosticCode1;
	}

	public String getDiagnosticCode2() {
		return this.diagnosticCode2;
	}

	public void setDiagnosticCode2(String diagnosticCode2) {
		this.diagnosticCode2 = diagnosticCode2;
	}

	public String getDiagnosticCode3() {
		return this.diagnosticCode3;
	}

	public void setDiagnosticCode3(String diagnosticCode3) {
		this.diagnosticCode3 = diagnosticCode3;
	}

	public BigDecimal getFacilityCode() {
		return this.facilityCode;
	}

	public void setFacilityCode(BigDecimal facilityCode) {
		this.facilityCode = facilityCode;
	}

	public String getFunctionalCenterCode() {
		return this.functionalCenterCode;
	}

	public void setFunctionalCenterCode(String functionalCenterCode) {
		this.functionalCenterCode = functionalCenterCode;
	}

	public String getHealthServiceCode() {
		return this.healthServiceCode;
	}

	public void setHealthServiceCode(String healthServiceCode) {
		this.healthServiceCode = healthServiceCode;
	}

	public String getHealthServiceId() {
		return this.healthServiceId;
	}

	public void setHealthServiceId(String healthServiceId) {
		this.healthServiceId = healthServiceId;
	}

	public Date getHospitalAdmitDate() {
		return this.hospitalAdmitDate;
	}

	public void setHospitalAdmitDate(Date hospitalAdmitDate) {
		this.hospitalAdmitDate = hospitalAdmitDate;
	}

	public Date getIcuAdmitDate() {
		return this.icuAdmitDate;
	}

	public void setIcuAdmitDate(Date icuAdmitDate) {
		this.icuAdmitDate = icuAdmitDate;
	}

	public String getInjuryDiagnosticCode() {
		return this.injuryDiagnosticCode;
	}

	public void setInjuryDiagnosticCode(String injuryDiagnosticCode) {
		this.injuryDiagnosticCode = injuryDiagnosticCode;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getLocationPostalCode() {
		return this.locationPostalCode;
	}

	public void setLocationPostalCode(String locationPostalCode) {
		this.locationPostalCode = locationPostalCode;
	}

	public BigDecimal getMultiples() {
		return this.multiples;
	}

	public void setMultiples(BigDecimal multiples) {
		this.multiples = multiples;
	}

	public BigDecimal getOtherPayeeId() {
		return this.otherPayeeId;
	}

	public void setOtherPayeeId(BigDecimal otherPayeeId) {
		this.otherPayeeId = otherPayeeId;
	}

	public String getOutOfProvReferralIndicator() {
		return this.outOfProvReferralIndicator;
	}

	public void setOutOfProvReferralIndicator(String outOfProvReferralIndicator) {
		this.outOfProvReferralIndicator = outOfProvReferralIndicator;
	}

	public BigDecimal getPatientAgeYears() {
		return this.patientAgeYears;
	}

	public void setPatientAgeYears(BigDecimal patientAgeYears) {
		this.patientAgeYears = patientAgeYears;
	}

	public Date getPatientBirthDate() {
		return this.patientBirthDate;
	}

	public void setPatientBirthDate(Date patientBirthDate) {
		this.patientBirthDate = patientBirthDate;
	}

	public String getPatientGender() {
		return this.patientGender;
	}

	public void setPatientGender(String patientGender) {
		this.patientGender = patientGender;
	}

	public String getPatientHealthCardNumber() {
		return this.patientHealthCardNumber;
	}

	public void setPatientHealthCardNumber(String patientHealthCardNumber) {
		this.patientHealthCardNumber = patientHealthCardNumber;
	}

	public String getPatientPostalCode() {
		return this.patientPostalCode;
	}

	public void setPatientPostalCode(String patientPostalCode) {
		this.patientPostalCode = patientPostalCode;
	}

	public String getPayToCode() {
		return this.payToCode;
	}

	public void setPayToCode(String payToCode) {
		this.payToCode = payToCode;
	}

	public BigDecimal getPayToHealthCardNumber() {
		return this.payToHealthCardNumber;
	}

	public void setPayToHealthCardNumber(BigDecimal payToHealthCardNumber) {
		this.payToHealthCardNumber = payToHealthCardNumber;
	}

	public BigDecimal getPayToProviderGroupId() {
		return this.payToProviderGroupId;
	}

	public void setPayToProviderGroupId(BigDecimal payToProviderGroupId) {
		this.payToProviderGroupId = payToProviderGroupId;
	}

	public BigDecimal getPayableId() {
		return this.payableId;
	}

	public void setPayableId(BigDecimal payableId) {
		this.payableId = payableId;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentResponsibility() {
		return this.paymentResponsibility;
	}

	public void setPaymentResponsibility(String paymentResponsibility) {
		this.paymentResponsibility = paymentResponsibility;
	}

	public String getPreauthorizationIndicator() {
		return this.preauthorizationIndicator;
	}

	public void setPreauthorizationIndicator(String preauthorizationIndicator) {
		this.preauthorizationIndicator = preauthorizationIndicator;
	}

	public String getProgramCode() {
		return this.programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getProviderAccumSpecialtyCode() {
		return this.providerAccumSpecialtyCode;
	}

	public void setProviderAccumSpecialtyCode(String providerAccumSpecialtyCode) {
		this.providerAccumSpecialtyCode = providerAccumSpecialtyCode;
	}

	public BigDecimal getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(BigDecimal providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderPostalCode() {
		return this.providerPostalCode;
	}

	public void setProviderPostalCode(String providerPostalCode) {
		this.providerPostalCode = providerPostalCode;
	}

	public String getProviderRole() {
		return this.providerRole;
	}

	public void setProviderRole(String providerRole) {
		this.providerRole = providerRole;
	}

	public String getProviderSpecialtyCode() {
		return this.providerSpecialtyCode;
	}

	public void setProviderSpecialtyCode(String providerSpecialtyCode) {
		this.providerSpecialtyCode = providerSpecialtyCode;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public String getQualifierCode() {
		return this.qualifierCode;
	}

	public void setQualifierCode(String qualifierCode) {
		this.qualifierCode = qualifierCode;
	}

	public BigDecimal getQuarterNumber() {
		return this.quarterNumber;
	}

	public void setQuarterNumber(BigDecimal quarterNumber) {
		this.quarterNumber = quarterNumber;
	}

	public String getReducedUnitIndicator() {
		return this.reducedUnitIndicator;
	}

	public void setReducedUnitIndicator(String reducedUnitIndicator) {
		this.reducedUnitIndicator = reducedUnitIndicator;
	}

	public BigDecimal getRefgProviderNumber() {
		return this.refgProviderNumber;
	}

	public void setRefgProviderNumber(BigDecimal refgProviderNumber) {
		this.refgProviderNumber = refgProviderNumber;
	}

	public String getRefgProviderSpecialtyCode() {
		return this.refgProviderSpecialtyCode;
	}

	public void setRefgProviderSpecialtyCode(String refgProviderSpecialtyCode) {
		this.refgProviderSpecialtyCode = refgProviderSpecialtyCode;
	}

	public String getRefgProviderType() {
		return this.refgProviderType;
	}

	public void setRefgProviderType(String refgProviderType) {
		this.refgProviderType = refgProviderType;
	}

	public BigDecimal getRemunerationMethod() {
		return this.remunerationMethod;
	}

	public void setRemunerationMethod(BigDecimal remunerationMethod) {
		this.remunerationMethod = remunerationMethod;
	}

	public BigDecimal getServiceOccurenceNumber() {
		return this.serviceOccurenceNumber;
	}

	public void setServiceOccurenceNumber(BigDecimal serviceOccurenceNumber) {
		this.serviceOccurenceNumber = serviceOccurenceNumber;
	}

	public Date getServiceStartDate() {
		return this.serviceStartDate;
	}

	public void setServiceStartDate(Date serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}

	public BigDecimal getShadowEarnings() {
		return this.shadowEarnings;
	}

	public void setShadowEarnings(BigDecimal shadowEarnings) {
		this.shadowEarnings = shadowEarnings;
	}

	public BigDecimal getShadowUnits() {
		return this.shadowUnits;
	}

	public void setShadowUnits(BigDecimal shadowUnits) {
		this.shadowUnits = shadowUnits;
	}

	public Date getYearEndDate() {
		return this.yearEndDate;
	}

	public void setYearEndDate(Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}

}