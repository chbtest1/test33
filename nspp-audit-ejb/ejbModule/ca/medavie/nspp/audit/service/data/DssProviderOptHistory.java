package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_PROVIDER_OPT_HISTORY database table.
 * 
 */
@Entity
@Table(name="DSS_PROVIDER_OPT_HISTORY")
public class DssProviderOptHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssProviderOptHistoryPK id;

	@Temporal(TemporalType.DATE)
	@Column(name="OPT_OUT_DATE")
	private Date optOutDate;

	@Column(name="OPT_OUT_REASON_CODE")
	private BigDecimal optOutReasonCode;

	@Column(name="OPT_OUT_REASON_DESC")
	private String optOutReasonDesc;

	@Column(name="POS_PROV_CODE")
	private String posProvCode;

	@Column(name="POS_PRVR_ID")
	private String posPrvrId;

	@Column(name="POS_PRVR_TYPE_CODE")
	private String posPrvrTypeCode;

	@Column(name="POS_REGION_CODE")
	private String posRegionCode;

	public DssProviderOptHistory() {
	}

	public DssProviderOptHistoryPK getId() {
		return this.id;
	}

	public void setId(DssProviderOptHistoryPK id) {
		this.id = id;
	}

	public Date getOptOutDate() {
		return this.optOutDate;
	}

	public void setOptOutDate(Date optOutDate) {
		this.optOutDate = optOutDate;
	}

	public BigDecimal getOptOutReasonCode() {
		return this.optOutReasonCode;
	}

	public void setOptOutReasonCode(BigDecimal optOutReasonCode) {
		this.optOutReasonCode = optOutReasonCode;
	}

	public String getOptOutReasonDesc() {
		return this.optOutReasonDesc;
	}

	public void setOptOutReasonDesc(String optOutReasonDesc) {
		this.optOutReasonDesc = optOutReasonDesc;
	}

	public String getPosProvCode() {
		return this.posProvCode;
	}

	public void setPosProvCode(String posProvCode) {
		this.posProvCode = posProvCode;
	}

	public String getPosPrvrId() {
		return this.posPrvrId;
	}

	public void setPosPrvrId(String posPrvrId) {
		this.posPrvrId = posPrvrId;
	}

	public String getPosPrvrTypeCode() {
		return this.posPrvrTypeCode;
	}

	public void setPosPrvrTypeCode(String posPrvrTypeCode) {
		this.posPrvrTypeCode = posPrvrTypeCode;
	}

	public String getPosRegionCode() {
		return this.posRegionCode;
	}

	public void setPosRegionCode(String posRegionCode) {
		this.posRegionCode = posRegionCode;
	}

}