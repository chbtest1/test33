package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_SPECIALTY_PEER_GROUP_XREF database table.
 * 
 */
@Embeddable
public class DssSpecialtyPeerGroupXrefPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="PROVIDER_PEER_GROUP_ID")
	private long providerPeerGroupId;

	@Temporal(TemporalType.DATE)
	@Column(name="YEAR_END_DATE")
	private java.util.Date yearEndDate;

	@Column(name="SPECIALTY_CODE")
	private String specialtyCode;

	public DssSpecialtyPeerGroupXrefPK() {
	}
	public long getProviderPeerGroupId() {
		return this.providerPeerGroupId;
	}
	public void setProviderPeerGroupId(long providerPeerGroupId) {
		this.providerPeerGroupId = providerPeerGroupId;
	}
	public java.util.Date getYearEndDate() {
		return this.yearEndDate;
	}
	public void setYearEndDate(java.util.Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}
	public String getSpecialtyCode() {
		return this.specialtyCode;
	}
	public void setSpecialtyCode(String specialtyCode) {
		this.specialtyCode = specialtyCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssSpecialtyPeerGroupXrefPK)) {
			return false;
		}
		DssSpecialtyPeerGroupXrefPK castOther = (DssSpecialtyPeerGroupXrefPK)other;
		return 
			(this.providerPeerGroupId == castOther.providerPeerGroupId)
			&& this.yearEndDate.equals(castOther.yearEndDate)
			&& this.specialtyCode.equals(castOther.specialtyCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.providerPeerGroupId ^ (this.providerPeerGroupId >>> 32)));
		hash = hash * prime + this.yearEndDate.hashCode();
		hash = hash * prime + this.specialtyCode.hashCode();
		
		return hash;
	}
}