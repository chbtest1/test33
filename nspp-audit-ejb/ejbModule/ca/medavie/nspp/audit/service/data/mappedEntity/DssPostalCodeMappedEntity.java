package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.*;

/**
 * The persistent class for the dss_postal_code database table.
 * 
 */
@Entity
@SqlResultSetMapping(name = "DssPostalCodeEntityMapping", entities = { @EntityResult(entityClass = DssPostalCodeMappedEntity.class, fields = {
		@FieldResult(name = "id.town_code", column = "town_code"), 
		@FieldResult(name = "town_name", column = "town_name"),
		@FieldResult(name = "id.county_code", column = "county_code"),
		@FieldResult(name = "county_name", column = "county_name"),
		@FieldResult(name = "id.municipality_code", column = "municipality_code"),
		@FieldResult(name = "municipality_name", column = "municipality_name"),
		@FieldResult(name = "id.health_region_code", column = "health_region_code"),
		@FieldResult(name = "health_region_desc", column = "health_region_desc"), }) })
public class DssPostalCodeMappedEntity {

	 @EmbeddedId
	 private DssPostalCodeMappedEntityPK id;

	@Column
	private String town_name;

	@Column
	private String county_name;

	@Column
	private String municipality_name;

	@Column
	private String health_region_desc;

	public DssPostalCodeMappedEntityPK getId() {
		return id;
	}

	public void setId(DssPostalCodeMappedEntityPK id) {
		this.id = id;
	}

	public String getTown_name() {
		return town_name;
	}

	public void setTown_name(String town_name) {
		this.town_name = town_name;
	}

	public String getCounty_name() {
		return county_name;
	}

	public void setCounty_name(String county_name) {
		this.county_name = county_name;
	}

	public String getMunicipality_name() {
		return municipality_name;
	}

	public void setMunicipality_name(String municipality_name) {
		this.municipality_name = municipality_name;
	}

	public String getHealth_region_desc() {
		return health_region_desc;
	}

	public void setHealth_region_desc(String health_region_desc) {
		this.health_region_desc = health_region_desc;
	}

}