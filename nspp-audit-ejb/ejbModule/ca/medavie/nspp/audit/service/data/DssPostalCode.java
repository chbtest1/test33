package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the DSS_POSTAL_CODE database table.
 * 
 */
@Entity
@Table(name = "DSS_POSTAL_CODE")
@NamedQueries({
	  @NamedQuery(name=QueryConstants.DSS_POSTAL_CODE_GET_TOWN_CODE_QUERY,
	              query="select distinct dptc.townCode FROM DssPostalCode dptc order by dptc.townCode ASC"),
	              
	   @NamedQuery(name=QueryConstants.DSS_POSTAL_CODE_GET_TOWN_NAME_BY_TOWN_CODE_QUERY,
	              query="select distinct dptc.townName FROM DssPostalCode dptc WHERE dptc.townCode = :selectedTownCode")
	})


public class DssPostalCode implements Serializable  {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssPostalCodePK id;

	@Column(name = "COMMUNITY_CODE")
	private BigDecimal communityCode;

	@Column(name = "COMMUNITY_NAME")
	private String communityName;

	@Column(name = "COMMUNITY_POPULATION")
	private BigDecimal communityPopulation;

	@Column(name = "COUNTY_CODE")
	private BigDecimal countyCode;

	@Column(name = "COUNTY_NAME")
	private String countyName;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	@Column(name = "HEALTH_REGION_CODE")
	private BigDecimal healthRegionCode;

	@Column(name = "HEALTH_REGION_DESC")
	private String healthRegionDesc;

	@Column(name = "MUNICIPALITY_CODE")
	private BigDecimal municipalityCode;

	@Column(name = "MUNICIPALITY_NAME")
	private String municipalityName;

	@Column(name = "PROVINCE_CODE")
	private String provinceCode;

	@Column(name = "TOWN_CODE")
	private BigDecimal townCode;

	@Column(name = "TOWN_NAME")
	private String townName;

	public DssPostalCode() {
	}

	public DssPostalCodePK getId() {
		return this.id;
	}

	public void setId(DssPostalCodePK id) {
		this.id = id;
	}

	public BigDecimal getCommunityCode() {
		return this.communityCode;
	}

	public void setCommunityCode(BigDecimal communityCode) {
		this.communityCode = communityCode;
	}

	public String getCommunityName() {
		return this.communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public BigDecimal getCommunityPopulation() {
		return this.communityPopulation;
	}

	public void setCommunityPopulation(BigDecimal communityPopulation) {
		this.communityPopulation = communityPopulation;
	}

	public BigDecimal getCountyCode() {
		return this.countyCode;
	}

	public void setCountyCode(BigDecimal countyCode) {
		this.countyCode = countyCode;
	}

	public String getCountyName() {
		return this.countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public BigDecimal getHealthRegionCode() {
		return this.healthRegionCode;
	}

	public void setHealthRegionCode(BigDecimal healthRegionCode) {
		this.healthRegionCode = healthRegionCode;
	}

	public String getHealthRegionDesc() {
		return this.healthRegionDesc;
	}

	public void setHealthRegionDesc(String healthRegionDesc) {
		this.healthRegionDesc = healthRegionDesc;
	}

	public BigDecimal getMunicipalityCode() {
		return this.municipalityCode;
	}

	public void setMunicipalityCode(BigDecimal municipalityCode) {
		this.municipalityCode = municipalityCode;
	}

	public String getMunicipalityName() {
		return this.municipalityName;
	}

	public void setMunicipalityName(String municipalityName) {
		this.municipalityName = municipalityName;
	}

	public String getProvinceCode() {
		return this.provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public BigDecimal getTownCode() {
		return this.townCode;
	}

	public void setTownCode(BigDecimal townCode) {
		this.townCode = townCode;
	}

	public String getTownName() {
		return this.townName;
	}

	public void setTownName(String townName) {
		this.townName = townName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DssPostalCode [id=" + id + ", communityCode=" + communityCode + ", communityName=" + communityName
				+ ", communityPopulation=" + communityPopulation + ", countyCode=" + countyCode + ", countyName="
				+ countyName + ", effectiveToDate=" + effectiveToDate + ", healthRegionCode=" + healthRegionCode
				+ ", healthRegionDesc=" + healthRegionDesc + ", municipalityCode=" + municipalityCode
				+ ", municipalityName=" + municipalityName + ", provinceCode=" + provinceCode + ", townCode="
				+ townCode + ", townName=" + townName + "]";
	}

}