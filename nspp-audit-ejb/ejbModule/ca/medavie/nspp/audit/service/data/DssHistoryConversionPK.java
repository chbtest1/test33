package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class DssHistoryConversionPK implements Serializable {

	/** IDE Generated */
	private static final long serialVersionUID = 5690345182296529370L;

	private String dent;

	@Column(name = "FROM_HEALTH_CARD_NUMBER")
	private String fromHealthCardNumber;

	@Column(name = "FROM_PROVIDER_NUMBER")
	private BigDecimal fromProviderNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "FROM_SERVICE_DATE")
	private Date fromServiceDate;

	private String med;

	@Column(name = "\"MESSAGE\"")
	private String message;

	private String mf;

	private String pf;

	private String pharm;

	@Temporal(TemporalType.DATE)
	@Column(name = "PROCESSED_DATE")
	private Date processedDate;

	@Column(name = "SE_NUMBER")
	private String seNumber;

	@Column(name = "TO_HEALTH_CARD_NUMBER")
	private String toHealthCardNumber;

	@Column(name = "TO_PROVIDER_NUMBER")
	private BigDecimal toProviderNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "TO_SERVICE_DATE")
	private Date toServiceDate;

	/**
	 * @return the dent
	 */
	public String getDent() {
		return dent;
	}

	/**
	 * @param dent
	 *            the dent to set
	 */
	public void setDent(String dent) {
		this.dent = dent;
	}

	/**
	 * @return the fromHealthCardNumber
	 */
	public String getFromHealthCardNumber() {
		return fromHealthCardNumber;
	}

	/**
	 * @param fromHealthCardNumber
	 *            the fromHealthCardNumber to set
	 */
	public void setFromHealthCardNumber(String fromHealthCardNumber) {
		this.fromHealthCardNumber = fromHealthCardNumber;
	}

	/**
	 * @return the fromProviderNumber
	 */
	public BigDecimal getFromProviderNumber() {
		return fromProviderNumber;
	}

	/**
	 * @param fromProviderNumber
	 *            the fromProviderNumber to set
	 */
	public void setFromProviderNumber(BigDecimal fromProviderNumber) {
		this.fromProviderNumber = fromProviderNumber;
	}

	/**
	 * @return the fromServiceDate
	 */
	public Date getFromServiceDate() {
		return fromServiceDate;
	}

	/**
	 * @param fromServiceDate
	 *            the fromServiceDate to set
	 */
	public void setFromServiceDate(Date fromServiceDate) {
		this.fromServiceDate = fromServiceDate;
	}

	/**
	 * @return the med
	 */
	public String getMed() {
		return med;
	}

	/**
	 * @param med
	 *            the med to set
	 */
	public void setMed(String med) {
		this.med = med;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the mf
	 */
	public String getMf() {
		return mf;
	}

	/**
	 * @param mf
	 *            the mf to set
	 */
	public void setMf(String mf) {
		this.mf = mf;
	}

	/**
	 * @return the pf
	 */
	public String getPf() {
		return pf;
	}

	/**
	 * @param pf
	 *            the pf to set
	 */
	public void setPf(String pf) {
		this.pf = pf;
	}

	/**
	 * @return the pharm
	 */
	public String getPharm() {
		return pharm;
	}

	/**
	 * @param pharm
	 *            the pharm to set
	 */
	public void setPharm(String pharm) {
		this.pharm = pharm;
	}

	/**
	 * @return the processedDate
	 */
	public Date getProcessedDate() {
		return processedDate;
	}

	/**
	 * @param processedDate
	 *            the processedDate to set
	 */
	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	/**
	 * @return the seNumber
	 */
	public String getSeNumber() {
		return seNumber;
	}

	/**
	 * @param seNumber
	 *            the seNumber to set
	 */
	public void setSeNumber(String seNumber) {
		this.seNumber = seNumber;
	}

	/**
	 * @return the toHealthCardNumber
	 */
	public String getToHealthCardNumber() {
		return toHealthCardNumber;
	}

	/**
	 * @param toHealthCardNumber
	 *            the toHealthCardNumber to set
	 */
	public void setToHealthCardNumber(String toHealthCardNumber) {
		this.toHealthCardNumber = toHealthCardNumber;
	}

	/**
	 * @return the toProviderNumber
	 */
	public BigDecimal getToProviderNumber() {
		return toProviderNumber;
	}

	/**
	 * @param toProviderNumber
	 *            the toProviderNumber to set
	 */
	public void setToProviderNumber(BigDecimal toProviderNumber) {
		this.toProviderNumber = toProviderNumber;
	}

	/**
	 * @return the toServiceDate
	 */
	public Date getToServiceDate() {
		return toServiceDate;
	}

	/**
	 * @param toServiceDate
	 *            the toServiceDate to set
	 */
	public void setToServiceDate(Date toServiceDate) {
		this.toServiceDate = toServiceDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dent == null) ? 0 : dent.hashCode());
		result = prime * result + ((fromHealthCardNumber == null) ? 0 : fromHealthCardNumber.hashCode());
		result = prime * result + ((fromProviderNumber == null) ? 0 : fromProviderNumber.hashCode());
		result = prime * result + ((fromServiceDate == null) ? 0 : fromServiceDate.hashCode());
		result = prime * result + ((med == null) ? 0 : med.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((mf == null) ? 0 : mf.hashCode());
		result = prime * result + ((pf == null) ? 0 : pf.hashCode());
		result = prime * result + ((pharm == null) ? 0 : pharm.hashCode());
		result = prime * result + ((processedDate == null) ? 0 : processedDate.hashCode());
		result = prime * result + ((seNumber == null) ? 0 : seNumber.hashCode());
		result = prime * result + ((toHealthCardNumber == null) ? 0 : toHealthCardNumber.hashCode());
		result = prime * result + ((toProviderNumber == null) ? 0 : toProviderNumber.hashCode());
		result = prime * result + ((toServiceDate == null) ? 0 : toServiceDate.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssHistoryConversionPK other = (DssHistoryConversionPK) obj;
		if (dent == null) {
			if (other.dent != null)
				return false;
		} else if (!dent.equals(other.dent))
			return false;
		if (fromHealthCardNumber == null) {
			if (other.fromHealthCardNumber != null)
				return false;
		} else if (!fromHealthCardNumber.equals(other.fromHealthCardNumber))
			return false;
		if (fromProviderNumber == null) {
			if (other.fromProviderNumber != null)
				return false;
		} else if (!fromProviderNumber.equals(other.fromProviderNumber))
			return false;
		if (fromServiceDate == null) {
			if (other.fromServiceDate != null)
				return false;
		} else if (!fromServiceDate.equals(other.fromServiceDate))
			return false;
		if (med == null) {
			if (other.med != null)
				return false;
		} else if (!med.equals(other.med))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (mf == null) {
			if (other.mf != null)
				return false;
		} else if (!mf.equals(other.mf))
			return false;
		if (pf == null) {
			if (other.pf != null)
				return false;
		} else if (!pf.equals(other.pf))
			return false;
		if (pharm == null) {
			if (other.pharm != null)
				return false;
		} else if (!pharm.equals(other.pharm))
			return false;
		if (processedDate == null) {
			if (other.processedDate != null)
				return false;
		} else if (!processedDate.equals(other.processedDate))
			return false;
		if (seNumber == null) {
			if (other.seNumber != null)
				return false;
		} else if (!seNumber.equals(other.seNumber))
			return false;
		if (toHealthCardNumber == null) {
			if (other.toHealthCardNumber != null)
				return false;
		} else if (!toHealthCardNumber.equals(other.toHealthCardNumber))
			return false;
		if (toProviderNumber == null) {
			if (other.toProviderNumber != null)
				return false;
		} else if (!toProviderNumber.equals(other.toProviderNumber))
			return false;
		if (toServiceDate == null) {
			if (other.toServiceDate != null)
				return false;
		} else if (!toServiceDate.equals(other.toServiceDate))
			return false;
		return true;
	}
}
