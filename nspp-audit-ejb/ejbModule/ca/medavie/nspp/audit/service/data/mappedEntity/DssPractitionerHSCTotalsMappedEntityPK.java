package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DssPractitionerHSCTotalsMappedEntityPK implements Serializable {

	/** IDE Generated */
	private static final long serialVersionUID = 5381548685362295569L;

	@Column
	private BigDecimal number_of_patients;
	@Column
	private BigDecimal idx_number_of_patients;

	@Column
	private BigDecimal number_of_se;
	@Column
	private BigDecimal idx_number_of_se;

	@Column
	private BigDecimal total_amount_paid;
	@Column
	private BigDecimal idx_total_amount_paid;

	@Column
	private BigDecimal number_of_se_per_patient;
	@Column
	private BigDecimal idx_number_of_se_per_patient;

	@Column
	private BigDecimal amount_paid_per_patient;
	@Column
	private BigDecimal idx_amount_paid_per_patient;

	/**
	 * @return the number_of_patients
	 */
	public BigDecimal getNumber_of_patients() {
		return number_of_patients;
	}

	/**
	 * @param number_of_patients
	 *            the number_of_patients to set
	 */
	public void setNumber_of_patients(BigDecimal number_of_patients) {
		this.number_of_patients = number_of_patients;
	}

	/**
	 * @return the idx_number_of_patients
	 */
	public BigDecimal getIdx_number_of_patients() {
		return idx_number_of_patients;
	}

	/**
	 * @param idx_number_of_patients
	 *            the idx_number_of_patients to set
	 */
	public void setIdx_number_of_patients(BigDecimal idx_number_of_patients) {
		this.idx_number_of_patients = idx_number_of_patients;
	}

	/**
	 * @return the number_of_se
	 */
	public BigDecimal getNumber_of_se() {
		return number_of_se;
	}

	/**
	 * @param number_of_se
	 *            the number_of_se to set
	 */
	public void setNumber_of_se(BigDecimal number_of_se) {
		this.number_of_se = number_of_se;
	}

	/**
	 * @return the idx_number_of_se
	 */
	public BigDecimal getIdx_number_of_se() {
		return idx_number_of_se;
	}

	/**
	 * @param idx_number_of_se
	 *            the idx_number_of_se to set
	 */
	public void setIdx_number_of_se(BigDecimal idx_number_of_se) {
		this.idx_number_of_se = idx_number_of_se;
	}

	/**
	 * @return the total_amount_paid
	 */
	public BigDecimal getTotal_amount_paid() {
		return total_amount_paid;
	}

	/**
	 * @param total_amount_paid
	 *            the total_amount_paid to set
	 */
	public void setTotal_amount_paid(BigDecimal total_amount_paid) {
		this.total_amount_paid = total_amount_paid;
	}

	/**
	 * @return the idx_total_amount_paid
	 */
	public BigDecimal getIdx_total_amount_paid() {
		return idx_total_amount_paid;
	}

	/**
	 * @param idx_total_amount_paid
	 *            the idx_total_amount_paid to set
	 */
	public void setIdx_total_amount_paid(BigDecimal idx_total_amount_paid) {
		this.idx_total_amount_paid = idx_total_amount_paid;
	}

	/**
	 * @return the number_of_se_per_patient
	 */
	public BigDecimal getNumber_of_se_per_patient() {
		return number_of_se_per_patient;
	}

	/**
	 * @param number_of_se_per_patient
	 *            the number_of_se_per_patient to set
	 */
	public void setNumber_of_se_per_patient(BigDecimal number_of_se_per_patient) {
		this.number_of_se_per_patient = number_of_se_per_patient;
	}

	/**
	 * @return the idx_number_of_se_per_patient
	 */
	public BigDecimal getIdx_number_of_se_per_patient() {
		return idx_number_of_se_per_patient;
	}

	/**
	 * @param idx_number_of_se_per_patient
	 *            the idx_number_of_se_per_patient to set
	 */
	public void setIdx_number_of_se_per_patient(BigDecimal idx_number_of_se_per_patient) {
		this.idx_number_of_se_per_patient = idx_number_of_se_per_patient;
	}

	/**
	 * @return the amount_paid_per_patient
	 */
	public BigDecimal getAmount_paid_per_patient() {
		return amount_paid_per_patient;
	}

	/**
	 * @param amount_paid_per_patient
	 *            the amount_paid_per_patient to set
	 */
	public void setAmount_paid_per_patient(BigDecimal amount_paid_per_patient) {
		this.amount_paid_per_patient = amount_paid_per_patient;
	}

	/**
	 * @return the idx_amount_paid_per_patient
	 */
	public BigDecimal getIdx_amount_paid_per_patient() {
		return idx_amount_paid_per_patient;
	}

	/**
	 * @param idx_amount_paid_per_patient
	 *            the idx_amount_paid_per_patient to set
	 */
	public void setIdx_amount_paid_per_patient(BigDecimal idx_amount_paid_per_patient) {
		this.idx_amount_paid_per_patient = idx_amount_paid_per_patient;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount_paid_per_patient == null) ? 0 : amount_paid_per_patient.hashCode());
		result = prime * result + ((idx_amount_paid_per_patient == null) ? 0 : idx_amount_paid_per_patient.hashCode());
		result = prime * result + ((idx_number_of_patients == null) ? 0 : idx_number_of_patients.hashCode());
		result = prime * result + ((idx_number_of_se == null) ? 0 : idx_number_of_se.hashCode());
		result = prime * result
				+ ((idx_number_of_se_per_patient == null) ? 0 : idx_number_of_se_per_patient.hashCode());
		result = prime * result + ((idx_total_amount_paid == null) ? 0 : idx_total_amount_paid.hashCode());
		result = prime * result + ((number_of_patients == null) ? 0 : number_of_patients.hashCode());
		result = prime * result + ((number_of_se == null) ? 0 : number_of_se.hashCode());
		result = prime * result + ((number_of_se_per_patient == null) ? 0 : number_of_se_per_patient.hashCode());
		result = prime * result + ((total_amount_paid == null) ? 0 : total_amount_paid.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssPractitionerHSCTotalsMappedEntityPK other = (DssPractitionerHSCTotalsMappedEntityPK) obj;
		if (amount_paid_per_patient == null) {
			if (other.amount_paid_per_patient != null)
				return false;
		} else if (!amount_paid_per_patient.equals(other.amount_paid_per_patient))
			return false;
		if (idx_amount_paid_per_patient == null) {
			if (other.idx_amount_paid_per_patient != null)
				return false;
		} else if (!idx_amount_paid_per_patient.equals(other.idx_amount_paid_per_patient))
			return false;
		if (idx_number_of_patients == null) {
			if (other.idx_number_of_patients != null)
				return false;
		} else if (!idx_number_of_patients.equals(other.idx_number_of_patients))
			return false;
		if (idx_number_of_se == null) {
			if (other.idx_number_of_se != null)
				return false;
		} else if (!idx_number_of_se.equals(other.idx_number_of_se))
			return false;
		if (idx_number_of_se_per_patient == null) {
			if (other.idx_number_of_se_per_patient != null)
				return false;
		} else if (!idx_number_of_se_per_patient.equals(other.idx_number_of_se_per_patient))
			return false;
		if (idx_total_amount_paid == null) {
			if (other.idx_total_amount_paid != null)
				return false;
		} else if (!idx_total_amount_paid.equals(other.idx_total_amount_paid))
			return false;
		if (number_of_patients == null) {
			if (other.number_of_patients != null)
				return false;
		} else if (!number_of_patients.equals(other.number_of_patients))
			return false;
		if (number_of_se == null) {
			if (other.number_of_se != null)
				return false;
		} else if (!number_of_se.equals(other.number_of_se))
			return false;
		if (number_of_se_per_patient == null) {
			if (other.number_of_se_per_patient != null)
				return false;
		} else if (!number_of_se_per_patient.equals(other.number_of_se_per_patient))
			return false;
		if (total_amount_paid == null) {
			if (other.total_amount_paid != null)
				return false;
		} else if (!total_amount_paid.equals(other.total_amount_paid))
			return false;
		return true;
	}
}
