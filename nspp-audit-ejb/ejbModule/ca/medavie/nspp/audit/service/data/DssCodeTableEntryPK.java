package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_CODE_TABLE_ENTRY database table.
 * 
 */
@Embeddable
public class DssCodeTableEntryPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CODE_TABLE_NUMBER")
	private long codeTableNumber;

	@Column(name="CODE_TABLE_ENTRY")
	private long codeTableEntry;

	public DssCodeTableEntryPK() {
	}
	public long getCodeTableNumber() {
		return this.codeTableNumber;
	}
	public void setCodeTableNumber(long codeTableNumber) {
		this.codeTableNumber = codeTableNumber;
	}
	public long getCodeTableEntry() {
		return this.codeTableEntry;
	}
	public void setCodeTableEntry(long codeTableEntry) {
		this.codeTableEntry = codeTableEntry;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssCodeTableEntryPK)) {
			return false;
		}
		DssCodeTableEntryPK castOther = (DssCodeTableEntryPK)other;
		return 
			(this.codeTableNumber == castOther.codeTableNumber)
			&& (this.codeTableEntry == castOther.codeTableEntry);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.codeTableNumber ^ (this.codeTableNumber >>> 32)));
		hash = hash * prime + ((int) (this.codeTableEntry ^ (this.codeTableEntry >>> 32)));
		
		return hash;
	}
}