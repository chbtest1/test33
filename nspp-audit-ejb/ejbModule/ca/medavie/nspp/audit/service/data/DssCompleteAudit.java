package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_COMPLETE_AUDIT database table.
 * 
 */
@Entity
@Table(name="DSS_COMPLETE_AUDIT")
public class DssCompleteAudit implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssCompleteAuditPK id;

	@Temporal(TemporalType.DATE)
	@Column(name="AUDIT_LETTER_CREATION_DATE")
	private Date auditLetterCreationDate;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="NUMBER_OF_LETTER_CREATED")
	private BigDecimal numberOfLetterCreated;

	@Column(name="NUMBER_OF_SE_SELECTED")
	private BigDecimal numberOfSeSelected;

	@Temporal(TemporalType.DATE)
	@Column(name="SE_SELECTION_DATE")
	private Date seSelectionDate;

	public DssCompleteAudit() {
	}

	public DssCompleteAuditPK getId() {
		return this.id;
	}

	public void setId(DssCompleteAuditPK id) {
		this.id = id;
	}

	public Date getAuditLetterCreationDate() {
		return this.auditLetterCreationDate;
	}

	public void setAuditLetterCreationDate(Date auditLetterCreationDate) {
		this.auditLetterCreationDate = auditLetterCreationDate;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public BigDecimal getNumberOfLetterCreated() {
		return this.numberOfLetterCreated;
	}

	public void setNumberOfLetterCreated(BigDecimal numberOfLetterCreated) {
		this.numberOfLetterCreated = numberOfLetterCreated;
	}

	public BigDecimal getNumberOfSeSelected() {
		return this.numberOfSeSelected;
	}

	public void setNumberOfSeSelected(BigDecimal numberOfSeSelected) {
		this.numberOfSeSelected = numberOfSeSelected;
	}

	public Date getSeSelectionDate() {
		return this.seSelectionDate;
	}

	public void setSeSelectionDate(Date seSelectionDate) {
		this.seSelectionDate = seSelectionDate;
	}

}