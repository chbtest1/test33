package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the DSS_PROVIDER_AUDIT_ATTACH database table.
 * 
 */
@Entity
@Table(name = "DSS_PROVIDER_AUDIT_ATTACH")
public class DssProviderAuditAttach implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ATTACHMENT_ID")
	private Long attachmentId;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "FILE_BINARY", nullable = true)
	private byte[] fileBinary;

	@Column(name = "FILENAME")
	private String filename;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MIME_TYPE")
	private String mimeType;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	// bi-directional many-to-one association to DssProviderAudit
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AUDIT_ID")
	private DssProviderAudit dssProviderAudit;

	public DssProviderAuditAttach() {
	}

	public Long getAttachmentId() {
		return this.attachmentId;
	}

	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}

	public byte[] getFileBinary() {
		return this.fileBinary;
	}

	public void setFileBinary(byte[] fileBinary) {
		this.fileBinary = fileBinary;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getMimeType() {
		return this.mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public DssProviderAudit getDssProviderAudit() {
		return this.dssProviderAudit;
	}

	public void setDssProviderAudit(DssProviderAudit dssProviderAudit) {
		this.dssProviderAudit = dssProviderAudit;
	}

}