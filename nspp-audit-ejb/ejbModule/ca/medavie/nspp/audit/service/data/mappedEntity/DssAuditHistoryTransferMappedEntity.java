package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.SqlResultSetMapping;

/**
 * Models a transfer record. Data stored in DSS_AUDIT_HISTORY_TRANSFER and DSS_INDIVIDUAL_2
 */
@Entity
@SqlResultSetMapping(name = "DssAuditHistoryTransferNativeQueryMapping", entities = { @EntityResult(entityClass = DssAuditHistoryTransferMappedEntity.class, fields = {
		@FieldResult(name = "id.se_number", column = "se_number"),
		@FieldResult(name = "id.se_sequence_number", column = "se_sequence_number"),
		@FieldResult(name = "id.response_tag_number", column = "response_tag_number"),
		@FieldResult(name = "id.from_health_card_number", column = "from_health_card_number"),
		@FieldResult(name = "id.to_health_card_number", column = "to_health_card_number"),
		@FieldResult(name = "id.comments", column = "comments"),
		@FieldResult(name = "id.transfer_date", column = "transfer_date"),
		@FieldResult(name = "id.modified_by", column = "modified_by"),
		@FieldResult(name = "id.med", column = "med"),
		@FieldResult(name = "id.pharm", column = "pharm"),
		@FieldResult(name = "id.dent", column = "dent"),
		@FieldResult(name = "id.mf", column = "mf"),
		@FieldResult(name = "id.pf", column = "pf"),
		@FieldResult(name = "id.source_name", column = "source_name"),
		@FieldResult(name = "id.target_name", column = "target_name") }) })
public class DssAuditHistoryTransferMappedEntity {

	@EmbeddedId
	private DssAuditHistoryTransferMappedEntityPK id;

	/**
	 * @return the id
	 */
	public DssAuditHistoryTransferMappedEntityPK getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(DssAuditHistoryTransferMappedEntityPK id) {
		this.id = id;
	}
}
