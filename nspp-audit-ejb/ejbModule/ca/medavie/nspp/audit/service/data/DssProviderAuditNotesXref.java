package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the DSS_PROVIDER_AUDIT_NOTES_XREF database table.
 * 
 */
@Entity
@Table(name = "DSS_PROVIDER_AUDIT_NOTES_XREF")
public class DssProviderAuditNotesXref implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AUDIT_ID")
	private DssProviderAudit dssProviderAudit;

	@EmbeddedId
	private DssProviderAuditNotesXrefPK id;

	@Column(name = "PROVIDER_NUMBER")
	private Long providerNumber;

	@Column(name = "PROVIDER_TYPE")
	private String providerType;

	

	@OneToOne(fetch = FetchType.LAZY,cascade = { CascadeType.ALL, CascadeType.MERGE })
	@JoinColumn(name = "NOTE_ID")
	private DssProviderAuditNote dssProviderAuditNote;

	public DssProviderAuditNotesXref() {
	}

	public DssProviderAuditNotesXrefPK getId() {
		return this.id;
	}

	public void setId(DssProviderAuditNotesXrefPK id) {
		this.id = id;
	}

	public Long getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public DssProviderAudit getDssProviderAudit() {
		return this.dssProviderAudit;
	}

	public void setDssProviderAudit(DssProviderAudit dssProviderAudit) {
		this.dssProviderAudit = dssProviderAudit;
	}

	public DssProviderAuditNote getDssProviderAuditNote() {
		return this.dssProviderAuditNote;
	}

	public void setDssProviderAuditNote(DssProviderAuditNote dssProviderAuditNote) {
		this.dssProviderAuditNote = dssProviderAuditNote;
	}

}