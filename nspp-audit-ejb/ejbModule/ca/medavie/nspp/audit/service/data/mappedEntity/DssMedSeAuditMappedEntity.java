package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ca.medavie.nspp.audit.service.data.DssMedSeAuditPK;

/**
 * The persistent class for the DSS_MED_SE_AUDIT database table.
 * 
 */
@Entity
@SqlResultSetMapping(name = "DssMedSeAuditNativeQueryMapping", entities = { @EntityResult(entityClass = DssMedSeAuditMappedEntity.class, fields = {
		@FieldResult(name = "id.health_service_id", column = "health_service_id"),
		@FieldResult(name = "id.program_code", column = "program_code"),
		@FieldResult(name = "id.health_service_code", column = "health_service_code"),
		@FieldResult(name = "id.qualifier_code", column = "qualifier_code"),
		@FieldResult(name = "id.effective_from_date", column = "effective_from_date"),
		@FieldResult(name = "id.effective_to_date", column = "effective_to_date"),
		@FieldResult(name = "id.description", column = "description") }) })
public class DssMedSeAuditMappedEntity {
	@EmbeddedId
	private DssMedSeAuditPK id;
	
	@Column(name="PROVIDER_NUMBER")	
	private int provider_number;
	
	@Column(name="PROVIDER_TYPE")
	private String provider_type;
	
	@Column(name="HEALTH_CARD_NUMBER")
	private String health_card_number;

	@Temporal(TemporalType.DATE)
	@Column(name="SE_SELECTION_DATE")	
	private Date se_selection_date;

	@Temporal(TemporalType.DATE)
	@Column(name="AUDIT_LETTER_CREATION_DATE")
	private Date audit_letter_creation_date;
	
	@Column(name="HEALTH_SERVICE_DESCRIPTION")
	private String health_service_description;
	
	@Column(name="RESPONSE_STATUS_CODE")
	private String response_status_code;
	
	@Column(name="NUMBER_OF_TIMES_SENT")
	private int number_of_times_sent;	
	
	@Column(name="CHARGEBACK_AMOUNT")
	private BigDecimal chargeback_amount;
	
	@Column(name="NUMBER_OF_SERVICES_AFFECTED")
	private int number_of_services_affected;
	
	@Column(name="AUDIT_NOTE")
	private String audit_note;
	
	@Column(name="MODIFIED_BY")
	private String modified_by;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date last_modified;
	
	public DssMedSeAuditPK getId() {
		return id;
	}

	public void setId(DssMedSeAuditPK id) {
		this.id = id;
	}

	public int getProvider_number() {
		return provider_number;
	}

	public void setProvider_number(int provider_number) {
		this.provider_number = provider_number;
	}

	public String getProvider_type() {
		return provider_type;
	}

	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}

	public String getHealth_card_number() {
		return health_card_number;
	}

	public void setHealth_card_number(String health_card_number) {
		this.health_card_number = health_card_number;
	}

	public Date getSe_selection_date() {
		return se_selection_date;
	}

	public void setSe_selection_date(Date se_selection_date) {
		this.se_selection_date = se_selection_date;
	}

	public Date getAudit_letter_creation_date() {
		return audit_letter_creation_date;
	}

	public void setAudit_letter_creation_date(Date audit_letter_creation_date) {
		this.audit_letter_creation_date = audit_letter_creation_date;
	}

	public String getHealth_service_description() {
		return health_service_description;
	}

	public void setHealth_service_description(String health_service_description) {
		this.health_service_description = health_service_description;
	}

	public String getResponse_status_code() {
		return response_status_code;
	}

	public void setResponse_status_code(String response_status_code) {
		this.response_status_code = response_status_code;
	}

	public int getNumber_of_times_sent() {
		return number_of_times_sent;
	}

	public void setNumber_of_times_sent(int number_of_times_sent) {
		this.number_of_times_sent = number_of_times_sent;
	}

	public BigDecimal getChargeback_amount() {
		return chargeback_amount;
	}

	public void setChargeback_amount(BigDecimal chargeback_amount) {
		this.chargeback_amount = chargeback_amount;
	}

	public int getNumber_of_services_affected() {
		return number_of_services_affected;
	}

	public void setNumber_of_services_affected(int number_of_services_affected) {
		this.number_of_services_affected = number_of_services_affected;
	}

	public String getAudit_note() {
		return audit_note;
	}

	public void setAudit_note(String audit_note) {
		this.audit_note = audit_note;
	}

	public String getModified_by() {
		return modified_by;
	}

	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	public Date getLast_modified() {
		return last_modified;
	}

	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}
}
