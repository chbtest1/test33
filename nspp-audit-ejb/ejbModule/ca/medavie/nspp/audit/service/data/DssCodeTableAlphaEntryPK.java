package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_CODE_TABLE_ALPHA_ENTRY database table.
 * 
 */
@Embeddable
public class DssCodeTableAlphaEntryPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CODE_TABLE_ALPHA_NUMBER")
	private long codeTableAlphaNumber;

	@Column(name="CODE_TABLE_ALPHA_ENTRY")
	private String codeTableAlphaEntry;

	public DssCodeTableAlphaEntryPK() {
	}
	public long getCodeTableAlphaNumber() {
		return this.codeTableAlphaNumber;
	}
	public void setCodeTableAlphaNumber(long codeTableAlphaNumber) {
		this.codeTableAlphaNumber = codeTableAlphaNumber;
	}
	public String getCodeTableAlphaEntry() {
		return this.codeTableAlphaEntry;
	}
	public void setCodeTableAlphaEntry(String codeTableAlphaEntry) {
		this.codeTableAlphaEntry = codeTableAlphaEntry;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssCodeTableAlphaEntryPK)) {
			return false;
		}
		DssCodeTableAlphaEntryPK castOther = (DssCodeTableAlphaEntryPK)other;
		return 
			(this.codeTableAlphaNumber == castOther.codeTableAlphaNumber)
			&& this.codeTableAlphaEntry.equals(castOther.codeTableAlphaEntry);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.codeTableAlphaNumber ^ (this.codeTableAlphaNumber >>> 32)));
		hash = hash * prime + this.codeTableAlphaEntry.hashCode();
		
		return hash;
	}
}