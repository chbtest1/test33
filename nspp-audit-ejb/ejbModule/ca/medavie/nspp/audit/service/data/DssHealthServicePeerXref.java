package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the DSS_HEALTH_SERVICE_PEER_XREF database table.
 * 
 */
@Entity
@Table(name = "DSS_HEALTH_SERVICE_PEER_XREF")
public class DssHealthServicePeerXref implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "PROVIDER_PEER_GROUP_ID", referencedColumnName = "PROVIDER_PEER_GROUP_ID"),
			@JoinColumn(name = "YEAR_END_DATE", referencedColumnName = "YEAR_END_DATE") })
	private DssProviderPeerGroup dssProviderPeerGroup6;

	// FetchType.EAGER has to be set in order to retrieve DssHealthService info
	@OneToOne(fetch = FetchType.EAGER,orphanRemoval=false)
	@JoinColumn(name = "HEALTH_SERVICE_ID", updatable = false )
	private DssHealthService dhss;

	@EmbeddedId
	private DssHealthServicePeerXrefPK id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssHealthServicePeerXref() {
	}

	public DssHealthServicePeerXrefPK getId() {
		return this.id;
	}

	public void setId(DssHealthServicePeerXrefPK id) {
		this.id = id;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public DssProviderPeerGroup getDssProviderPeerGroup6() {
		return dssProviderPeerGroup6;
	}

	public void setDssProviderPeerGroup6(DssProviderPeerGroup dssProviderPeerGroup) {
		this.dssProviderPeerGroup6 = dssProviderPeerGroup;
	}

	public DssHealthService getDhss() {
		return dhss;
	}

	public void setDhss(DssHealthService dhss) {
		this.dhss = dhss;
	}

}