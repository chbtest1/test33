package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the CPEND database table.
 * 
 */
@Entity
public class Cpend implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CpendPK id;

	@Column(name="ATC_CODE")
	private String atcCode;

	@Column(name="BENEFIT_CODE")
	private BigDecimal benefitCode;

	@Column(name="BT_BENEFIT_TYPE")
	private BigDecimal btBenefitType;

	@Column(name="CARDHOLDER_ID")
	private String cardholderId;

	@Column(name="CEXP_STATUS")
	private BigDecimal cexpStatus;

	@Column(name="CLIENT_ID")
	private String clientId;

	@Column(name="COMPOUND_INDICATOR")
	private String compoundIndicator;

	@Column(name="CURRENT_RX_NUMB")
	private String currentRxNumb;

	@Column(name="CVG_NBR")
	private String cvgNbr;

	@Column(name="DIN_NAME_ABBREV")
	private String dinNameAbbrev;

	@Column(name="EFT_REF_NUM")
	private String eftRefNum;

	@Column(name="GENERIC_INDICATOR")
	private String genericIndicator;

	@Column(name="GROUP_NUM")
	private String groupNum;

	@Column(name="INTERV_EXC_CODE")
	private String intervExcCode;

	@Column(name="LAST_CEXP_CODE")
	private String lastCexpCode;

	@Column(name="LAST_CEXP_STATUS")
	private BigDecimal lastCexpStatus;

	@Column(name="MED_COND")
	private String medCond;

	@Column(name="MED_REF")
	private String medRef;

	@Column(name="MQTY_CLAIMED")
	private BigDecimal mqtyClaimed;

	@Column(name="MQTY_PAID")
	private BigDecimal mqtyPaid;

	@Column(name="ORIGINAL_RX_NUMB")
	private String originalRxNumb;

	@Column(name="PAYMENT_METHOD")
	private BigDecimal paymentMethod;

	@Column(name="PHARM_NUM")
	private String pharmNum;

	@Column(name="PHARMACIST_ID")
	private String pharmacistId;

	@Column(name="PMT_STATUS")
	private BigDecimal pmtStatus;

	@Column(name="POS_COINS_PAID")
	private BigDecimal posCoinsPaid;

	@Column(name="PRC_METHOD")
	private String prcMethod;

	private String prescriber;

	@Column(name="PRESCRIBER_ID_REF")
	private String prescriberIdRef;

	@Column(name="PRESCRIBER_LICENSE_NUMBER")
	private String prescriberLicenseNumber;

	@Column(name="PREV_PAID")
	private BigDecimal prevPaid;

	@Column(name="PRICE_LIST_ID")
	private String priceListId;

	@Column(name="PRN_PRESCRIBER_ID")
	private String prnPrescriberId;

	@Column(name="PRN_PROVIDER_ID")
	private String prnProviderId;

	@Column(name="PRN_PROVIDER_TYPE")
	private String prnProviderType;

	@Column(name="PRODUCT_ID")
	private String productId;

	@Column(name="PRODUCT_TYPE_CODE")
	private String productTypeCode;

	@Column(name="PROV_ID_CODE")
	private String provIdCode;

	@Column(name="PROV_SOFT_VER")
	private String provSoftVer;

	@Column(name="PROVIDER_CODE")
	private BigDecimal providerCode;

	@Column(name="PROVIDER_ZONE")
	private BigDecimal providerZone;

	@Column(name="REIMBURSEMENT_FLAG")
	private String reimbursementFlag;

	@Column(name="RESPONSE_STATUS")
	private String responseStatus;

	@Column(name="RX_CODE")
	private String rxCode;

	@Column(name="SPEC_AUTH_NUM")
	private String specAuthNum;

	@Column(name="SS_CODE")
	private String ssCode;

	@Column(name="SUBMIT_METHOD")
	private BigDecimal submitMethod;

	@Column(name="TREAT_IND")
	private String treatInd;

	@Column(name="UNLISTED_COMPOUND")
	private String unlistedCompound;

	@Column(name="VERSION_NUM")
	private BigDecimal versionNum;

	public Cpend() {
	}

	public CpendPK getId() {
		return this.id;
	}

	public void setId(CpendPK id) {
		this.id = id;
	}

	public String getAtcCode() {
		return this.atcCode;
	}

	public void setAtcCode(String atcCode) {
		this.atcCode = atcCode;
	}

	public BigDecimal getBenefitCode() {
		return this.benefitCode;
	}

	public void setBenefitCode(BigDecimal benefitCode) {
		this.benefitCode = benefitCode;
	}

	public BigDecimal getBtBenefitType() {
		return this.btBenefitType;
	}

	public void setBtBenefitType(BigDecimal btBenefitType) {
		this.btBenefitType = btBenefitType;
	}

	public String getCardholderId() {
		return this.cardholderId;
	}

	public void setCardholderId(String cardholderId) {
		this.cardholderId = cardholderId;
	}

	public BigDecimal getCexpStatus() {
		return this.cexpStatus;
	}

	public void setCexpStatus(BigDecimal cexpStatus) {
		this.cexpStatus = cexpStatus;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getCompoundIndicator() {
		return this.compoundIndicator;
	}

	public void setCompoundIndicator(String compoundIndicator) {
		this.compoundIndicator = compoundIndicator;
	}

	public String getCurrentRxNumb() {
		return this.currentRxNumb;
	}

	public void setCurrentRxNumb(String currentRxNumb) {
		this.currentRxNumb = currentRxNumb;
	}

	public String getCvgNbr() {
		return this.cvgNbr;
	}

	public void setCvgNbr(String cvgNbr) {
		this.cvgNbr = cvgNbr;
	}

	public String getDinNameAbbrev() {
		return this.dinNameAbbrev;
	}

	public void setDinNameAbbrev(String dinNameAbbrev) {
		this.dinNameAbbrev = dinNameAbbrev;
	}

	public String getEftRefNum() {
		return this.eftRefNum;
	}

	public void setEftRefNum(String eftRefNum) {
		this.eftRefNum = eftRefNum;
	}

	public String getGenericIndicator() {
		return this.genericIndicator;
	}

	public void setGenericIndicator(String genericIndicator) {
		this.genericIndicator = genericIndicator;
	}

	public String getGroupNum() {
		return this.groupNum;
	}

	public void setGroupNum(String groupNum) {
		this.groupNum = groupNum;
	}

	public String getIntervExcCode() {
		return this.intervExcCode;
	}

	public void setIntervExcCode(String intervExcCode) {
		this.intervExcCode = intervExcCode;
	}

	public String getLastCexpCode() {
		return this.lastCexpCode;
	}

	public void setLastCexpCode(String lastCexpCode) {
		this.lastCexpCode = lastCexpCode;
	}

	public BigDecimal getLastCexpStatus() {
		return this.lastCexpStatus;
	}

	public void setLastCexpStatus(BigDecimal lastCexpStatus) {
		this.lastCexpStatus = lastCexpStatus;
	}

	public String getMedCond() {
		return this.medCond;
	}

	public void setMedCond(String medCond) {
		this.medCond = medCond;
	}

	public String getMedRef() {
		return this.medRef;
	}

	public void setMedRef(String medRef) {
		this.medRef = medRef;
	}

	public BigDecimal getMqtyClaimed() {
		return this.mqtyClaimed;
	}

	public void setMqtyClaimed(BigDecimal mqtyClaimed) {
		this.mqtyClaimed = mqtyClaimed;
	}

	public BigDecimal getMqtyPaid() {
		return this.mqtyPaid;
	}

	public void setMqtyPaid(BigDecimal mqtyPaid) {
		this.mqtyPaid = mqtyPaid;
	}

	public String getOriginalRxNumb() {
		return this.originalRxNumb;
	}

	public void setOriginalRxNumb(String originalRxNumb) {
		this.originalRxNumb = originalRxNumb;
	}

	public BigDecimal getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(BigDecimal paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getPharmNum() {
		return this.pharmNum;
	}

	public void setPharmNum(String pharmNum) {
		this.pharmNum = pharmNum;
	}

	public String getPharmacistId() {
		return this.pharmacistId;
	}

	public void setPharmacistId(String pharmacistId) {
		this.pharmacistId = pharmacistId;
	}

	public BigDecimal getPmtStatus() {
		return this.pmtStatus;
	}

	public void setPmtStatus(BigDecimal pmtStatus) {
		this.pmtStatus = pmtStatus;
	}

	public BigDecimal getPosCoinsPaid() {
		return this.posCoinsPaid;
	}

	public void setPosCoinsPaid(BigDecimal posCoinsPaid) {
		this.posCoinsPaid = posCoinsPaid;
	}

	public String getPrcMethod() {
		return this.prcMethod;
	}

	public void setPrcMethod(String prcMethod) {
		this.prcMethod = prcMethod;
	}

	public String getPrescriber() {
		return this.prescriber;
	}

	public void setPrescriber(String prescriber) {
		this.prescriber = prescriber;
	}

	public String getPrescriberIdRef() {
		return this.prescriberIdRef;
	}

	public void setPrescriberIdRef(String prescriberIdRef) {
		this.prescriberIdRef = prescriberIdRef;
	}

	public String getPrescriberLicenseNumber() {
		return this.prescriberLicenseNumber;
	}

	public void setPrescriberLicenseNumber(String prescriberLicenseNumber) {
		this.prescriberLicenseNumber = prescriberLicenseNumber;
	}

	public BigDecimal getPrevPaid() {
		return this.prevPaid;
	}

	public void setPrevPaid(BigDecimal prevPaid) {
		this.prevPaid = prevPaid;
	}

	public String getPriceListId() {
		return this.priceListId;
	}

	public void setPriceListId(String priceListId) {
		this.priceListId = priceListId;
	}

	public String getPrnPrescriberId() {
		return this.prnPrescriberId;
	}

	public void setPrnPrescriberId(String prnPrescriberId) {
		this.prnPrescriberId = prnPrescriberId;
	}

	public String getPrnProviderId() {
		return this.prnProviderId;
	}

	public void setPrnProviderId(String prnProviderId) {
		this.prnProviderId = prnProviderId;
	}

	public String getPrnProviderType() {
		return this.prnProviderType;
	}

	public void setPrnProviderType(String prnProviderType) {
		this.prnProviderType = prnProviderType;
	}

	public String getProductId() {
		return this.productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductTypeCode() {
		return this.productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}

	public String getProvIdCode() {
		return this.provIdCode;
	}

	public void setProvIdCode(String provIdCode) {
		this.provIdCode = provIdCode;
	}

	public String getProvSoftVer() {
		return this.provSoftVer;
	}

	public void setProvSoftVer(String provSoftVer) {
		this.provSoftVer = provSoftVer;
	}

	public BigDecimal getProviderCode() {
		return this.providerCode;
	}

	public void setProviderCode(BigDecimal providerCode) {
		this.providerCode = providerCode;
	}

	public BigDecimal getProviderZone() {
		return this.providerZone;
	}

	public void setProviderZone(BigDecimal providerZone) {
		this.providerZone = providerZone;
	}

	public String getReimbursementFlag() {
		return this.reimbursementFlag;
	}

	public void setReimbursementFlag(String reimbursementFlag) {
		this.reimbursementFlag = reimbursementFlag;
	}

	public String getResponseStatus() {
		return this.responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getRxCode() {
		return this.rxCode;
	}

	public void setRxCode(String rxCode) {
		this.rxCode = rxCode;
	}

	public String getSpecAuthNum() {
		return this.specAuthNum;
	}

	public void setSpecAuthNum(String specAuthNum) {
		this.specAuthNum = specAuthNum;
	}

	public String getSsCode() {
		return this.ssCode;
	}

	public void setSsCode(String ssCode) {
		this.ssCode = ssCode;
	}

	public BigDecimal getSubmitMethod() {
		return this.submitMethod;
	}

	public void setSubmitMethod(BigDecimal submitMethod) {
		this.submitMethod = submitMethod;
	}

	public String getTreatInd() {
		return this.treatInd;
	}

	public void setTreatInd(String treatInd) {
		this.treatInd = treatInd;
	}

	public String getUnlistedCompound() {
		return this.unlistedCompound;
	}

	public void setUnlistedCompound(String unlistedCompound) {
		this.unlistedCompound = unlistedCompound;
	}

	public BigDecimal getVersionNum() {
		return this.versionNum;
	}

	public void setVersionNum(BigDecimal versionNum) {
		this.versionNum = versionNum;
	}

}