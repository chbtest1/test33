package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the DSS_UNIT_VALUE database table.
 * 
 */
@Entity
@Table(name = "DSS_UNIT_VALUE")
@NamedQueries({ @NamedQuery(name = QueryConstants.DSS_UNIT_VALUE_GET_QUERY, query = "select distinct duv FROM DssUnitValue duv order by duv.effectiveFromDate DESC, duv.uvTypeCode ASC") })
public class DssUnitValue implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DSS_UV_IDENTIFIER_SEQ",sequenceName="DSS_UV_IDENTIFIER_SEQ" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DSS_UV_IDENTIFIER_SEQ")
	@Column(name = "UV_IDENTIFIER")
	private long uvIdentifier;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_FROM_DATE")
	private Date effectiveFromDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "UV_DOLLARS")
	private BigDecimal uvDollars;

	@Column(name = "UV_LEVEL")
	private BigDecimal uvLevel;

	@Column(name = "UV_LOWER_THRESHOLD")
	private BigDecimal uvLowerThreshold;

	@Column(name = "UV_TYPE_CODE")
	private BigDecimal uvTypeCode;

	@Column(name = "UV_TYPE_DESC")
	private String uvTypeDesc;

	@Column(name = "UV_UPPER_THRESHOLD")
	private BigDecimal uvUpperThreshold;

	public DssUnitValue() {
	}

	public long getUvIdentifier() {
		return this.uvIdentifier;
	}

	public void setUvIdentifier(long uvIdentifier) {
		this.uvIdentifier = uvIdentifier;
	}

	public Date getEffectiveFromDate() {
		return this.effectiveFromDate;
	}

	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public BigDecimal getUvDollars() {
		return this.uvDollars;
	}

	public void setUvDollars(BigDecimal uvDollars) {
		this.uvDollars = uvDollars;
	}

	public BigDecimal getUvLevel() {
		return this.uvLevel;
	}

	public void setUvLevel(BigDecimal uvLevel) {
		this.uvLevel = uvLevel;
	}

	public BigDecimal getUvLowerThreshold() {
		return this.uvLowerThreshold;
	}

	public void setUvLowerThreshold(BigDecimal uvLowerThreshold) {
		this.uvLowerThreshold = uvLowerThreshold;
	}

	public BigDecimal getUvTypeCode() {
		return this.uvTypeCode;
	}

	public void setUvTypeCode(BigDecimal uvTypeCode) {
		this.uvTypeCode = uvTypeCode;
	}

	public String getUvTypeDesc() {
		return this.uvTypeDesc;
	}

	public void setUvTypeDesc(String uvTypeDesc) {
		this.uvTypeDesc = uvTypeDesc;
	}

	public BigDecimal getUvUpperThreshold() {
		return this.uvUpperThreshold;
	}

	public void setUvUpperThreshold(BigDecimal uvUpperThreshold) {
		this.uvUpperThreshold = uvUpperThreshold;
	}

}