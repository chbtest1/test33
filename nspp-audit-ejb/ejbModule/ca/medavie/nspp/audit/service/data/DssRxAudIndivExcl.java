package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the DSS_RX_AUD_INDIV_EXCL database table.
 * 
 */
@Entity
@Table(name = "DSS_RX_AUD_INDIV_EXCL")
public class DssRxAudIndivExcl implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssRxAudIndivExclPK id;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssRxAudIndivExcl() {
	}

	public DssRxAudIndivExclPK getId() {
		return this.id;
	}

	public void setId(DssRxAudIndivExclPK id) {
		this.id = id;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}