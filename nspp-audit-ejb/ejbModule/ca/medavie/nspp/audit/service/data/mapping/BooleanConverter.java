package ca.medavie.nspp.audit.service.data.mapping;

import org.dozer.DozerConverter;

/**
 * 
 * Field mapper for mapping Boolean to Y or N or in the opposite way
 * 
 **/
public class BooleanConverter extends DozerConverter<String, Boolean> {

	public BooleanConverter() {
		super(String.class, Boolean.class);
	}

	@Override
	public Boolean convertTo(String source, Boolean destination) {
		if ("Y".equals(source)) {
			return Boolean.TRUE;
		} else if ("N".equals(source)) {
			return Boolean.FALSE;
		}
		throw new IllegalStateException("Unknown value!");
	}

	@Override
	public String convertFrom(Boolean source, String destination) {
		if (Boolean.TRUE.equals(source)) {
			return "Y";
		} else if (Boolean.FALSE.equals(source)) {
			return "N";
		}
		throw new IllegalStateException("Unknown value!");
	}

}