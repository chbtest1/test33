package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the DSS_CODE_TABLE_ALPHA_ENTRY database table.
 * 
 */
@Entity
@Table(name = "DSS_CODE_TABLE_ALPHA_ENTRY")
@NamedQueries({
		@NamedQuery(name = QueryConstants.DSS_CODE_TABLE_SPECIALTY_QUERY, query = "select distinct dctae FROM DssCodeTableAlphaEntry dctae where dctae.id.codeTableAlphaNumber=102 order by dctae.codeTableAlphaEntryDesc ASC"),
		@NamedQuery(name = QueryConstants.DSS_CODE_TABLE_ALPHA_ENTRY_PAYEE_TYPES, query = "select distinct dctae FROM DssCodeTableAlphaEntry dctae where dctae.id.codeTableAlphaNumber=124 order by dctae.codeTableAlphaEntryDesc ASC"),
		@NamedQuery(name = QueryConstants.DSS_CODE_TABLE_ALPHA_ENTRY_PAY_REPS, query = "select distinct dctae FROM DssCodeTableAlphaEntry dctae where dctae.id.codeTableAlphaNumber=122 order by dctae.codeTableAlphaEntryDesc ASC"),
		@NamedQuery(name = QueryConstants.DSS_CODE_TABLE_ALPHA_ENTRY_MEDICARE_AUDIT_TYPE, query = "select distinct dctae FROM DssCodeTableAlphaEntry dctae where dctae.id.codeTableAlphaNumber=121 order by dctae.codeTableAlphaEntryDesc ASC"),
		@NamedQuery(name = QueryConstants.DSS_CODE_TABLE_ALPHA_ENTRY_DENTICARE_AUDIT_TYPE, query = "select distinct dctae FROM DssCodeTableAlphaEntry dctae where dctae.id.codeTableAlphaNumber=125 order by dctae.codeTableAlphaEntryDesc ASC"),
		@NamedQuery(name = QueryConstants.DSS_CODE_TABLE_ALPHA_ENTRY_PHARM_AUDIT_TYPE, query = "select distinct dctae FROM DssCodeTableAlphaEntry dctae where dctae.id.codeTableAlphaNumber=123 order by dctae.codeTableAlphaEntryDesc ASC"),
		@NamedQuery(name = QueryConstants.DSS_CODE_TABLE_ALPHA_ENTRY_PHARM_AUDIT_CRITERIA_TYPE, query = "select distinct dctae FROM DssCodeTableAlphaEntry dctae where dctae.id.codeTableAlphaNumber=119 order by dctae.codeTableAlphaEntryDesc ASC"),
		@NamedQuery(name = QueryConstants.DSS_CODE_TABLE_ALPHA_ENTRY_GENERAL_AUDIT_CRITERIA_TYPE, query = "select distinct dctae FROM DssCodeTableAlphaEntry dctae where dctae.id.codeTableAlphaNumber=120 order by dctae.codeTableAlphaEntryDesc ASC"),
		@NamedQuery(name = QueryConstants.DSS_CODE_TABLE_ALPHA_ENTRY_RESPONSE_STATUS, query = "select distinct dctae FROM DssCodeTableAlphaEntry dctae where dctae.id.codeTableAlphaNumber=128 order by dctae.codeTableAlphaEntryDesc ASC") })
public class DssCodeTableAlphaEntry implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssCodeTableAlphaEntryPK id;

	@Column(name = "CODE_TABLE_ALPHA_ENTRY_DESC")
	private String codeTableAlphaEntryDesc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public DssCodeTableAlphaEntry() {
	}

	public DssCodeTableAlphaEntryPK getId() {
		return this.id;
	}

	public void setId(DssCodeTableAlphaEntryPK id) {
		this.id = id;
	}

	public String getCodeTableAlphaEntryDesc() {
		return this.codeTableAlphaEntryDesc;
	}

	public void setCodeTableAlphaEntryDesc(String codeTableAlphaEntryDesc) {
		this.codeTableAlphaEntryDesc = codeTableAlphaEntryDesc;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}