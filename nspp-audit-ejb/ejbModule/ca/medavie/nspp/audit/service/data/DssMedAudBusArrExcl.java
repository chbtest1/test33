package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the DSS_MED_AUD_BUS_ARR_EXCL database table.
 * 
 */
@Entity
@Table(name="DSS_MED_AUD_BUS_ARR_EXCL")
public class DssMedAudBusArrExcl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BUS_ARR_NUMBER")
	private long busArrNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="PROVIDER_GROUP_ID")
	private Long providerGroupId;

	@Column(name="PROVIDER_NUMBER")
	private Long providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	public DssMedAudBusArrExcl() {
	}

	public long getBusArrNumber() {
		return this.busArrNumber;
	}

	public void setBusArrNumber(long busArrNumber) {
		this.busArrNumber = busArrNumber;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Long getProviderGroupId() {
		return this.providerGroupId;
	}

	public void setProviderGroupId(Long providerGroupId) {
		this.providerGroupId = providerGroupId;
	}

	public Long getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

}