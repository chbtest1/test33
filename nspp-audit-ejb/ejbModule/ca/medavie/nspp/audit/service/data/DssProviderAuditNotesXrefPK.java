package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_PROVIDER_AUDIT_NOTES_XREF database table.
 * 
 */
@Embeddable
public class DssProviderAuditNotesXrefPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="NOTE_ID")
	private Long noteId;

	@Column(name="AUDIT_ID")
	private Long auditId;

	public DssProviderAuditNotesXrefPK() {
	}
	public Long getNoteId() {
		return this.noteId;
	}
	public void setNoteId(Long noteId) {
		this.noteId = noteId;
	}
	public Long getAuditId() {
		return this.auditId;
	}
	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssProviderAuditNotesXrefPK)) {
			return false;
		}
		DssProviderAuditNotesXrefPK castOther = (DssProviderAuditNotesXrefPK)other;
		return 
			(this.noteId == castOther.noteId)
			&& (this.auditId == castOther.auditId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.noteId ^ (this.noteId >>> 32)));
		hash = hash * prime + ((int) (this.auditId ^ (this.auditId >>> 32)));
		
		return hash;
	}
}