package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_MED_MAINFRAME_CLAIMS database table.
 * 
 */
@Entity
@Table(name="DSS_MED_MAINFRAME_CLAIMS")
public class DssMedMainframeClaim implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="MAIN_SEQ_NUMBER")
	private long mainSeqNumber;

	@Column(name="MAIN_AMOUNT_PAID")
	private BigDecimal mainAmountPaid;

	@Temporal(TemporalType.DATE)
	@Column(name="MAIN_BIRTH_DATE")
	private Date mainBirthDate;

	@Column(name="MAIN_COMMUNITY_SIZE")
	private String mainCommunitySize;

	@Column(name="MAIN_DIAGNOSTIC_CODE")
	private BigDecimal mainDiagnosticCode;

	@Column(name="MAIN_FEE_APPROVED")
	private BigDecimal mainFeeApproved;

	@Column(name="MAIN_FEE_CLAIMED")
	private BigDecimal mainFeeClaimed;

	@Column(name="MAIN_FEE_GROUP")
	private BigDecimal mainFeeGroup;

	@Column(name="MAIN_FEE_SERVICE_CODE")
	private BigDecimal mainFeeServiceCode;

	@Column(name="MAIN_FEE_SPECIALTY")
	private String mainFeeSpecialty;

	@Column(name="MAIN_GENDER")
	private String mainGender;

	@Column(name="MAIN_HEALTH_CARD_NUMBER")
	private BigDecimal mainHealthCardNumber;

	@Column(name="MAIN_NUMBER_OF_SERVICES")
	private BigDecimal mainNumberOfServices;

	@Column(name="MAIN_OLD_MSI_BILLING_NUMBER")
	private BigDecimal mainOldMsiBillingNumber;

	@Column(name="MAIN_PATIENT_COUNTY")
	private BigDecimal mainPatientCounty;

	@Column(name="MAIN_PERIOD_ID")
	private String mainPeriodId;

	@Column(name="MAIN_PHYSICIAN_AREA")
	private BigDecimal mainPhysicianArea;

	@Column(name="MAIN_PHYSICIAN_MAIN_NUMBER")
	private BigDecimal mainPhysicianMainNumber;

	@Column(name="MAIN_PHYSICIAN_SPECIALTY")
	private BigDecimal mainPhysicianSpecialty;

	@Column(name="MAIN_PROVIDER_GROUP")
	private BigDecimal mainProviderGroup;

	@Column(name="MAIN_PROVINCE_CODE")
	private String mainProvinceCode;

	@Column(name="MAIN_REFG_PROVIDER")
	private BigDecimal mainRefgProvider;

	@Column(name="MAIN_RUN_NUMBER")
	private String mainRunNumber;

	@Temporal(TemporalType.DATE)
	@Column(name="MAIN_SERVICE_DATE")
	private Date mainServiceDate;

	@Column(name="MAIN_TRANSACTION_CODE")
	private BigDecimal mainTransactionCode;

	public DssMedMainframeClaim() {
	}

	public long getMainSeqNumber() {
		return this.mainSeqNumber;
	}

	public void setMainSeqNumber(long mainSeqNumber) {
		this.mainSeqNumber = mainSeqNumber;
	}

	public BigDecimal getMainAmountPaid() {
		return this.mainAmountPaid;
	}

	public void setMainAmountPaid(BigDecimal mainAmountPaid) {
		this.mainAmountPaid = mainAmountPaid;
	}

	public Date getMainBirthDate() {
		return this.mainBirthDate;
	}

	public void setMainBirthDate(Date mainBirthDate) {
		this.mainBirthDate = mainBirthDate;
	}

	public String getMainCommunitySize() {
		return this.mainCommunitySize;
	}

	public void setMainCommunitySize(String mainCommunitySize) {
		this.mainCommunitySize = mainCommunitySize;
	}

	public BigDecimal getMainDiagnosticCode() {
		return this.mainDiagnosticCode;
	}

	public void setMainDiagnosticCode(BigDecimal mainDiagnosticCode) {
		this.mainDiagnosticCode = mainDiagnosticCode;
	}

	public BigDecimal getMainFeeApproved() {
		return this.mainFeeApproved;
	}

	public void setMainFeeApproved(BigDecimal mainFeeApproved) {
		this.mainFeeApproved = mainFeeApproved;
	}

	public BigDecimal getMainFeeClaimed() {
		return this.mainFeeClaimed;
	}

	public void setMainFeeClaimed(BigDecimal mainFeeClaimed) {
		this.mainFeeClaimed = mainFeeClaimed;
	}

	public BigDecimal getMainFeeGroup() {
		return this.mainFeeGroup;
	}

	public void setMainFeeGroup(BigDecimal mainFeeGroup) {
		this.mainFeeGroup = mainFeeGroup;
	}

	public BigDecimal getMainFeeServiceCode() {
		return this.mainFeeServiceCode;
	}

	public void setMainFeeServiceCode(BigDecimal mainFeeServiceCode) {
		this.mainFeeServiceCode = mainFeeServiceCode;
	}

	public String getMainFeeSpecialty() {
		return this.mainFeeSpecialty;
	}

	public void setMainFeeSpecialty(String mainFeeSpecialty) {
		this.mainFeeSpecialty = mainFeeSpecialty;
	}

	public String getMainGender() {
		return this.mainGender;
	}

	public void setMainGender(String mainGender) {
		this.mainGender = mainGender;
	}

	public BigDecimal getMainHealthCardNumber() {
		return this.mainHealthCardNumber;
	}

	public void setMainHealthCardNumber(BigDecimal mainHealthCardNumber) {
		this.mainHealthCardNumber = mainHealthCardNumber;
	}

	public BigDecimal getMainNumberOfServices() {
		return this.mainNumberOfServices;
	}

	public void setMainNumberOfServices(BigDecimal mainNumberOfServices) {
		this.mainNumberOfServices = mainNumberOfServices;
	}

	public BigDecimal getMainOldMsiBillingNumber() {
		return this.mainOldMsiBillingNumber;
	}

	public void setMainOldMsiBillingNumber(BigDecimal mainOldMsiBillingNumber) {
		this.mainOldMsiBillingNumber = mainOldMsiBillingNumber;
	}

	public BigDecimal getMainPatientCounty() {
		return this.mainPatientCounty;
	}

	public void setMainPatientCounty(BigDecimal mainPatientCounty) {
		this.mainPatientCounty = mainPatientCounty;
	}

	public String getMainPeriodId() {
		return this.mainPeriodId;
	}

	public void setMainPeriodId(String mainPeriodId) {
		this.mainPeriodId = mainPeriodId;
	}

	public BigDecimal getMainPhysicianArea() {
		return this.mainPhysicianArea;
	}

	public void setMainPhysicianArea(BigDecimal mainPhysicianArea) {
		this.mainPhysicianArea = mainPhysicianArea;
	}

	public BigDecimal getMainPhysicianMainNumber() {
		return this.mainPhysicianMainNumber;
	}

	public void setMainPhysicianMainNumber(BigDecimal mainPhysicianMainNumber) {
		this.mainPhysicianMainNumber = mainPhysicianMainNumber;
	}

	public BigDecimal getMainPhysicianSpecialty() {
		return this.mainPhysicianSpecialty;
	}

	public void setMainPhysicianSpecialty(BigDecimal mainPhysicianSpecialty) {
		this.mainPhysicianSpecialty = mainPhysicianSpecialty;
	}

	public BigDecimal getMainProviderGroup() {
		return this.mainProviderGroup;
	}

	public void setMainProviderGroup(BigDecimal mainProviderGroup) {
		this.mainProviderGroup = mainProviderGroup;
	}

	public String getMainProvinceCode() {
		return this.mainProvinceCode;
	}

	public void setMainProvinceCode(String mainProvinceCode) {
		this.mainProvinceCode = mainProvinceCode;
	}

	public BigDecimal getMainRefgProvider() {
		return this.mainRefgProvider;
	}

	public void setMainRefgProvider(BigDecimal mainRefgProvider) {
		this.mainRefgProvider = mainRefgProvider;
	}

	public String getMainRunNumber() {
		return this.mainRunNumber;
	}

	public void setMainRunNumber(String mainRunNumber) {
		this.mainRunNumber = mainRunNumber;
	}

	public Date getMainServiceDate() {
		return this.mainServiceDate;
	}

	public void setMainServiceDate(Date mainServiceDate) {
		this.mainServiceDate = mainServiceDate;
	}

	public BigDecimal getMainTransactionCode() {
		return this.mainTransactionCode;
	}

	public void setMainTransactionCode(BigDecimal mainTransactionCode) {
		this.mainTransactionCode = mainTransactionCode;
	}

}