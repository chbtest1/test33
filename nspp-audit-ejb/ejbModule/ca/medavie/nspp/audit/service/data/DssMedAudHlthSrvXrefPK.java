package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_MED_AUD_HLTH_SRV_XREF database table.
 * 
 */
@Embeddable
public class DssMedAudHlthSrvXrefPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "AUDIT_CRITERIA_ID")
	private long auditCriteriaId;

	@Column(name = "UNIQUE_NUMBER")
	private String uniqueNumber;

	public DssMedAudHlthSrvXrefPK() {
	}

	public long getAuditCriteriaId() {
		return this.auditCriteriaId;
	}

	public void setAuditCriteriaId(long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}

	public String getUniqueNumber() {
		return this.uniqueNumber;
	}

	public void setUniqueNumber(String uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (auditCriteriaId ^ (auditCriteriaId >>> 32));
		result = prime * result + ((uniqueNumber == null) ? 0 : uniqueNumber.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssMedAudHlthSrvXrefPK other = (DssMedAudHlthSrvXrefPK) obj;
		if (auditCriteriaId != other.auditCriteriaId)
			return false;
		if (uniqueNumber == null) {
			if (other.uniqueNumber != null)
				return false;
		} else if (!uniqueNumber.equals(other.uniqueNumber))
			return false;
		return true;
	}
}