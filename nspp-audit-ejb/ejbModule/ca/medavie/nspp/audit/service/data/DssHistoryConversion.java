package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The persistent class for the DSS_HISTORY_CONVERSION database table.
 * 
 */
@Entity
@Table(name = "DSS_HISTORY_CONVERSION")
public class DssHistoryConversion implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssHistoryConversionPK id;

	public DssHistoryConversion() {
	}

	/**
	 * @return the id
	 */
	public DssHistoryConversionPK getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(DssHistoryConversionPK id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssHistoryConversion other = (DssHistoryConversion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}