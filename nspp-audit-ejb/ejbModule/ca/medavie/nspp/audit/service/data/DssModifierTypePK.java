package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_MODIFIER_TYPE database table.
 * 
 */
@Embeddable
public class DssModifierTypePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="MODIFIER_TYPE")
	private String modifierType;

	@Column(name="MODIFIER_NAME")
	private String modifierName;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_FROM_DATE")
	private java.util.Date effectiveFromDate;

	public DssModifierTypePK() {
	}
	public String getModifierType() {
		return this.modifierType;
	}
	public void setModifierType(String modifierType) {
		this.modifierType = modifierType;
	}
	public String getModifierName() {
		return this.modifierName;
	}
	public void setModifierName(String modifierName) {
		this.modifierName = modifierName;
	}
	public java.util.Date getEffectiveFromDate() {
		return this.effectiveFromDate;
	}
	public void setEffectiveFromDate(java.util.Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssModifierTypePK)) {
			return false;
		}
		DssModifierTypePK castOther = (DssModifierTypePK)other;
		return 
			this.modifierType.equals(castOther.modifierType)
			&& this.modifierName.equals(castOther.modifierName)
			&& this.effectiveFromDate.equals(castOther.effectiveFromDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.modifierType.hashCode();
		hash = hash * prime + this.modifierName.hashCode();
		hash = hash * prime + this.effectiveFromDate.hashCode();
		
		return hash;
	}
}