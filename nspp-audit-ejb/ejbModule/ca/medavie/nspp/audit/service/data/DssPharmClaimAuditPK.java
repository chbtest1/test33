package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_PHARM_CLAIM_AUDIT database table.
 * 
 */
@Embeddable
public class DssPharmClaimAuditPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CLAIM_REF_NUM")
	private String claimRefNum;

	@Column(name="AUDIT_RUN_NUMBER")
	private long auditRunNumber;

	public DssPharmClaimAuditPK() {
	}
	public String getClaimRefNum() {
		return this.claimRefNum;
	}
	public void setClaimRefNum(String claimRefNum) {
		this.claimRefNum = claimRefNum;
	}
	public long getAuditRunNumber() {
		return this.auditRunNumber;
	}
	public void setAuditRunNumber(long auditRunNumber) {
		this.auditRunNumber = auditRunNumber;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssPharmClaimAuditPK)) {
			return false;
		}
		DssPharmClaimAuditPK castOther = (DssPharmClaimAuditPK)other;
		return 
			this.claimRefNum.equals(castOther.claimRefNum)
			&& (this.auditRunNumber == castOther.auditRunNumber);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.claimRefNum.hashCode();
		hash = hash * prime + ((int) (this.auditRunNumber ^ (this.auditRunNumber >>> 32)));
		
		return hash;
	}
}