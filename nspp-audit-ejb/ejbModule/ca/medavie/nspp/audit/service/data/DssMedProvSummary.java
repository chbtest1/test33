package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the DSS_MED_PROV_SUMMARY database table.
 * 
 */
@Entity
@Table(name="DSS_MED_PROV_SUMMARY")
public class DssMedProvSummary implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssMedProvSummaryPK id;

	@Column(name="EXP_AMOUNT_PAID_PER_100")
	private BigDecimal expAmountPaidPer100;

	@Column(name="EXP_NUMBER_OF_SERVICES_PER_100")
	private BigDecimal expNumberOfServicesPer100;

	@Column(name="IDX_AMOUNT_PAID_PER_PATIENT")
	private BigDecimal idxAmountPaidPerPatient;

	@Column(name="IDX_NUMBER_OF_PATIENTS")
	private BigDecimal idxNumberOfPatients;

	@Column(name="IDX_NUMBER_OF_SE")
	private BigDecimal idxNumberOfSe;

	@Column(name="IDX_NUMBER_OF_SE_PER_PATIENT")
	private BigDecimal idxNumberOfSePerPatient;

	@Column(name="IDX_TOTAL_AMOUNT_PAID")
	private BigDecimal idxTotalAmountPaid;

	@Column(name="IDX_TOTAL_UNITS")
	private BigDecimal idxTotalUnits;

	@Column(name="NUMBER_OF_PATIENTS")
	private BigDecimal numberOfPatients;

	@Column(name="NUMBER_OF_SE")
	private BigDecimal numberOfSe;

	@Column(name="TOTAL_AMOUNT_PAID")
	private BigDecimal totalAmountPaid;

	@Column(name="TOTAL_UNITS")
	private BigDecimal totalUnits;

	public DssMedProvSummary() {
	}

	public DssMedProvSummaryPK getId() {
		return this.id;
	}

	public void setId(DssMedProvSummaryPK id) {
		this.id = id;
	}

	public BigDecimal getExpAmountPaidPer100() {
		return this.expAmountPaidPer100;
	}

	public void setExpAmountPaidPer100(BigDecimal expAmountPaidPer100) {
		this.expAmountPaidPer100 = expAmountPaidPer100;
	}

	public BigDecimal getExpNumberOfServicesPer100() {
		return this.expNumberOfServicesPer100;
	}

	public void setExpNumberOfServicesPer100(BigDecimal expNumberOfServicesPer100) {
		this.expNumberOfServicesPer100 = expNumberOfServicesPer100;
	}

	public BigDecimal getIdxAmountPaidPerPatient() {
		return this.idxAmountPaidPerPatient;
	}

	public void setIdxAmountPaidPerPatient(BigDecimal idxAmountPaidPerPatient) {
		this.idxAmountPaidPerPatient = idxAmountPaidPerPatient;
	}

	public BigDecimal getIdxNumberOfPatients() {
		return this.idxNumberOfPatients;
	}

	public void setIdxNumberOfPatients(BigDecimal idxNumberOfPatients) {
		this.idxNumberOfPatients = idxNumberOfPatients;
	}

	public BigDecimal getIdxNumberOfSe() {
		return this.idxNumberOfSe;
	}

	public void setIdxNumberOfSe(BigDecimal idxNumberOfSe) {
		this.idxNumberOfSe = idxNumberOfSe;
	}

	public BigDecimal getIdxNumberOfSePerPatient() {
		return this.idxNumberOfSePerPatient;
	}

	public void setIdxNumberOfSePerPatient(BigDecimal idxNumberOfSePerPatient) {
		this.idxNumberOfSePerPatient = idxNumberOfSePerPatient;
	}

	public BigDecimal getIdxTotalAmountPaid() {
		return this.idxTotalAmountPaid;
	}

	public void setIdxTotalAmountPaid(BigDecimal idxTotalAmountPaid) {
		this.idxTotalAmountPaid = idxTotalAmountPaid;
	}

	public BigDecimal getIdxTotalUnits() {
		return this.idxTotalUnits;
	}

	public void setIdxTotalUnits(BigDecimal idxTotalUnits) {
		this.idxTotalUnits = idxTotalUnits;
	}

	public BigDecimal getNumberOfPatients() {
		return this.numberOfPatients;
	}

	public void setNumberOfPatients(BigDecimal numberOfPatients) {
		this.numberOfPatients = numberOfPatients;
	}

	public BigDecimal getNumberOfSe() {
		return this.numberOfSe;
	}

	public void setNumberOfSe(BigDecimal numberOfSe) {
		this.numberOfSe = numberOfSe;
	}

	public BigDecimal getTotalAmountPaid() {
		return this.totalAmountPaid;
	}

	public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}

	public BigDecimal getTotalUnits() {
		return this.totalUnits;
	}

	public void setTotalUnits(BigDecimal totalUnits) {
		this.totalUnits = totalUnits;
	}

}