package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DssIndividualExclusionPK implements Serializable {

	/** IDE Generated */
	private static final long serialVersionUID = 9017527605673352683L;

	@Column
	private String health_card_number;
	@Column
	private String name;
	@Column
	private Date effective_from_date;
	@Column
	private Date effective_to_date;
	@Column
	private Date last_modified;
	@Column
	private String modified_by;

	/**
	 * @return the health_card_number
	 */
	public String getHealth_card_number() {
		return health_card_number;
	}

	/**
	 * @param health_card_number
	 *            the health_card_number to set
	 */
	public void setHealth_card_number(String health_card_number) {
		this.health_card_number = health_card_number;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the effective_from_date
	 */
	public Date getEffective_from_date() {
		return effective_from_date;
	}

	/**
	 * @param effective_from_date
	 *            the effective_from_date to set
	 */
	public void setEffective_from_date(Date effective_from_date) {
		this.effective_from_date = effective_from_date;
	}

	/**
	 * @return the effective_to_date
	 */
	public Date getEffective_to_date() {
		return effective_to_date;
	}

	/**
	 * @param effective_to_date
	 *            the effective_to_date to set
	 */
	public void setEffective_to_date(Date effective_to_date) {
		this.effective_to_date = effective_to_date;
	}

	/**
	 * @return the last_modified
	 */
	public Date getLast_modified() {
		return last_modified;
	}

	/**
	 * @param last_modified
	 *            the last_modified to set
	 */
	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}

	/**
	 * @return the modified_by
	 */
	public String getModified_by() {
		return modified_by;
	}

	/**
	 * @param modified_by
	 *            the modified_by to set
	 */
	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((effective_from_date == null) ? 0 : effective_from_date.hashCode());
		result = prime * result + ((effective_to_date == null) ? 0 : effective_to_date.hashCode());
		result = prime * result + ((health_card_number == null) ? 0 : health_card_number.hashCode());
		result = prime * result + ((last_modified == null) ? 0 : last_modified.hashCode());
		result = prime * result + ((modified_by == null) ? 0 : modified_by.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssIndividualExclusionPK other = (DssIndividualExclusionPK) obj;
		if (effective_from_date == null) {
			if (other.effective_from_date != null)
				return false;
		} else if (!effective_from_date.equals(other.effective_from_date))
			return false;
		if (effective_to_date == null) {
			if (other.effective_to_date != null)
				return false;
		} else if (!effective_to_date.equals(other.effective_to_date))
			return false;
		if (health_card_number == null) {
			if (other.health_card_number != null)
				return false;
		} else if (!health_card_number.equals(other.health_card_number))
			return false;
		if (last_modified == null) {
			if (other.last_modified != null)
				return false;
		} else if (!last_modified.equals(other.last_modified))
			return false;
		if (modified_by == null) {
			if (other.modified_by != null)
				return false;
		} else if (!modified_by.equals(other.modified_by))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
