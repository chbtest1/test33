package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_RX_AUD_INDIV_EXCL database table.
 * 
 */
@Embeddable
public class DssRxAudIndivExclPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="HEALTH_CARD_NUMBER")
	private String healthCardNumber;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_FROM_DATE")
	private java.util.Date effectiveFromDate;

	public DssRxAudIndivExclPK() {
	}
	public String getHealthCardNumber() {
		return this.healthCardNumber;
	}
	public void setHealthCardNumber(String healthCardNumber) {
		this.healthCardNumber = healthCardNumber;
	}
	public java.util.Date getEffectiveFromDate() {
		return this.effectiveFromDate;
	}
	public void setEffectiveFromDate(java.util.Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssRxAudIndivExclPK)) {
			return false;
		}
		DssRxAudIndivExclPK castOther = (DssRxAudIndivExclPK)other;
		return 
			this.healthCardNumber.equals(castOther.healthCardNumber)
			&& this.effectiveFromDate.equals(castOther.effectiveFromDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.healthCardNumber.hashCode();
		hash = hash * prime + this.effectiveFromDate.hashCode();
		
		return hash;
	}
}