package ca.medavie.nspp.audit.service.data.mappedEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.SqlResultSetMapping;

/**
 * Maps the HSC totals for PeerGroup inquiry data
 */
@Entity
@SqlResultSetMapping(name = "DssPeerGroupHSCTotalsQueryMapping", entities = { @EntityResult(entityClass = DssPeerGroupHSCTotalsMappedEntity.class, fields = {
		@FieldResult(name = "id.number_of_providers", column = "number_of_providers"),
		@FieldResult(name = "id.number_of_patients", column = "number_of_patients"),
		@FieldResult(name = "id.avg_number_of_patients", column = "avg_number_of_patients"),
		@FieldResult(name = "id.std_dev_number_of_patients", column = "std_dev_number_of_patients"),
		@FieldResult(name = "id.number_of_se", column = "number_of_se"),
		@FieldResult(name = "id.avg_number_of_se", column = "avg_number_of_se"),
		@FieldResult(name = "id.std_dev_number_of_se", column = "std_dev_number_of_se"),
		@FieldResult(name = "id.total_amount_paid", column = "total_amount_paid"),
		@FieldResult(name = "id.avg_total_amount_paid", column = "avg_total_amount_paid"),
		@FieldResult(name = "id.std_dev_total_amount_paid", column = "std_dev_total_amount_paid"),
		@FieldResult(name = "id.percentage_payment", column = "percentage_payment"),
		@FieldResult(name = "id.avg_number_of_se_per_patient", column = "avg_number_of_se_per_patient"),
		@FieldResult(name = "id.std_dev_number_of_se_per_pat", column = "std_dev_number_of_se_per_pat"),
		@FieldResult(name = "id.avg_amount_paid_per_patient", column = "avg_amount_paid_per_patient"),
		@FieldResult(name = "id.std_dev_amount_paid_per_pat", column = "std_dev_amount_paid_per_pat"),
		@FieldResult(name = "id.avg_number_of_se_per_100", column = "avg_number_of_se_per_100"),
		@FieldResult(name = "id.std_dev_number_of_se_per_100", column = "std_dev_number_of_se_per_100"),
		@FieldResult(name = "id.avg_amount_paid_per_100", column = "avg_amount_paid_per_100"),
		@FieldResult(name = "id.std_dev_amount_paid_per_100", column = "std_dev_amount_paid_per_100") }) })
public class DssPeerGroupHSCTotalsMappedEntity {

	@EmbeddedId
	private DssPeerGroupHSCTotalsMappedEntityPK id;

	/**
	 * @return the id
	 */
	public DssPeerGroupHSCTotalsMappedEntityPK getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(DssPeerGroupHSCTotalsMappedEntityPK id) {
		this.id = id;
	}

}
