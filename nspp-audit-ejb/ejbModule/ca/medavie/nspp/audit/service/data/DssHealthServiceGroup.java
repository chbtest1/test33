package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DSS_HEALTH_SERVICE_GROUP database table.
 * 
 */
@Entity
@Table(name="DSS_HEALTH_SERVICE_GROUP")
public class DssHealthServiceGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="HEALTH_SERVICE_GROUP_ID")
	private long healthServiceGroupId;

	@Column(name="HEALTH_SERVICE_GROUP_NAME")
	private String healthServiceGroupName;

	@Column(name="HEALTH_SERVICE_GROUP_TYPE")
	private String healthServiceGroupType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	public DssHealthServiceGroup() {
	}

	public long getHealthServiceGroupId() {
		return this.healthServiceGroupId;
	}

	public void setHealthServiceGroupId(long healthServiceGroupId) {
		this.healthServiceGroupId = healthServiceGroupId;
	}

	public String getHealthServiceGroupName() {
		return this.healthServiceGroupName;
	}

	public void setHealthServiceGroupName(String healthServiceGroupName) {
		this.healthServiceGroupName = healthServiceGroupName;
	}

	public String getHealthServiceGroupType() {
		return this.healthServiceGroupType;
	}

	public void setHealthServiceGroupType(String healthServiceGroupType) {
		this.healthServiceGroupType = healthServiceGroupType;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}