package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the DSS_PROVIDER_GROUP_XREF database table.
 * 
 */
@Entity
@Table(name = "DSS_PROVIDER_GROUP_XREF")
public class DssProviderGroupXref implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssProviderGroupXrefPK id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "PROVIDER_NUMBER", referencedColumnName = "PROVIDER_NUMBER", insertable = false, updatable = false),
			@JoinColumn(name = "PROVIDER_TYPE", referencedColumnName = "PROVIDER_TYPE", insertable = false, updatable = false) })
	private DssProvider2 dp2;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PROVIDER_GROUP_ID", referencedColumnName = "PROVIDER_GROUP_ID", insertable = false, updatable = false)
	private DssProviderGroup dpg;

	@Temporal(TemporalType.DATE)
	@Column(name = "EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	public DssProviderGroupXref() {
	}

	public DssProviderGroupXrefPK getId() {
		return this.id;
	}

	public void setId(DssProviderGroupXrefPK id) {
		this.id = id;
	}

	/**
	 * @return the dp2
	 */
	public DssProvider2 getDp2() {
		return dp2;
	}

	/**
	 * @param dp2
	 *            the dp2 to set
	 */
	public void setDp2(DssProvider2 dp2) {
		this.dp2 = dp2;
	}

	/**
	 * @return the dpg
	 */
	public DssProviderGroup getDpg() {
		return dpg;
	}

	/**
	 * @param dpg
	 *            the dpg to set
	 */
	public void setDpg(DssProviderGroup dpg) {
		this.dpg = dpg;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

}