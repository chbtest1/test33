package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_MED_AUD_PAY_RESP_XREF database table.
 * 
 */
@Embeddable
public class DssMedAudPayRespXrefPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="AUDIT_CRITERIA_ID")
	private long auditCriteriaId;

	@Column(name="PAYMENT_RESPONSIBILITY")
	private String paymentResponsibility;

	public DssMedAudPayRespXrefPK() {
	}
	public long getAuditCriteriaId() {
		return this.auditCriteriaId;
	}
	public void setAuditCriteriaId(long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}
	public String getPaymentResponsibility() {
		return this.paymentResponsibility;
	}
	public void setPaymentResponsibility(String paymentResponsibility) {
		this.paymentResponsibility = paymentResponsibility;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssMedAudPayRespXrefPK)) {
			return false;
		}
		DssMedAudPayRespXrefPK castOther = (DssMedAudPayRespXrefPK)other;
		return 
			(this.auditCriteriaId == castOther.auditCriteriaId)
			&& this.paymentResponsibility.equals(castOther.paymentResponsibility);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.auditCriteriaId ^ (this.auditCriteriaId >>> 32)));
		hash = hash * prime + this.paymentResponsibility.hashCode();
		
		return hash;
	}
}