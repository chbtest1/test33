package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_MANUAL_DEPOSITS database table.
 * 
 */
@Entity
@Table(name="DSS_MANUAL_DEPOSITS")
public class DssManualDeposit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DEPOSIT_NUMBER")
	private long depositNumber;

	@Column(name="BUS_ARR_NUMBER")
	private BigDecimal busArrNumber;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_RECEIVED")
	private Date dateReceived;

	@Column(name="DEPOSIT_AMOUNT")
	private BigDecimal depositAmount;

	@Column(name="DEPOSIT_NOTE")
	private String depositNote;

	@Temporal(TemporalType.DATE)
	@Column(name="FISCAL_YEAR_END_DATE")
	private Date fiscalYearEndDate;

	@Column(name="GL_NUMBER")
	private BigDecimal glNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name="PROVIDER_GROUP_ID")
	private BigDecimal providerGroupId;

	@Column(name="PROVIDER_NUMBER")
	private BigDecimal providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;
	
	public DssManualDeposit() {
	}

	public long getDepositNumber() {
		return this.depositNumber;
	}

	public void setDepositNumber(long depositNumber) {
		this.depositNumber = depositNumber;
	}

	public BigDecimal getBusArrNumber() {
		return this.busArrNumber;
	}

	public void setBusArrNumber(BigDecimal busArrNumber) {
		this.busArrNumber = busArrNumber;
	}

	public Date getDateReceived() {
		return this.dateReceived;
	}

	public void setDateReceived(Date dateReceived) {
		this.dateReceived = dateReceived;
	}

	public BigDecimal getDepositAmount() {
		return this.depositAmount;
	}

	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	public String getDepositNote() {
		return this.depositNote;
	}

	public void setDepositNote(String depositNote) {
		this.depositNote = depositNote;
	}

	public Date getFiscalYearEndDate() {
		return this.fiscalYearEndDate;
	}

	public void setFiscalYearEndDate(Date fiscalYearEndDate) {
		this.fiscalYearEndDate = fiscalYearEndDate;
	}

	public BigDecimal getGlNumber() {
		return this.glNumber;
	}

	public void setGlNumber(BigDecimal glNumber) {
		this.glNumber = glNumber;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public BigDecimal getProviderGroupId() {
		return this.providerGroupId;
	}

	public void setProviderGroupId(BigDecimal providerGroupId) {
		this.providerGroupId = providerGroupId;
	}

	public BigDecimal getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(BigDecimal providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

}