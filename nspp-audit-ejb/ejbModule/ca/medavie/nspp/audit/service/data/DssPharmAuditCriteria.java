package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the DSS_PHARM_AUDIT_CRITERIA database table.
 * 
 */
@Entity
@Table(name = "DSS_PHARM_AUDIT_CRITERIA")
public class DssPharmAuditCriteria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "AUDIT_CRITERIA_ID")
	private long auditCriteriaId;

	@Temporal(TemporalType.DATE)
	@Column(name = "AUDIT_FROM_DATE")
	private Date auditFromDate;

	@Column(name = "AUDIT_FROM_LAST_INDICATOR")
	private String auditFromLastIndicator;

	@Temporal(TemporalType.DATE)
	@Column(name = "AUDIT_TO_DATE")
	private Date auditToDate;

	@Column(name = "AUDIT_TYPE")
	private String auditType;

	@Column(name = "DIN_INCLUSIVE")
	private String dinInclusive;

	@Column(name = "FROM_CEXP_CODE")
	private BigDecimal fromCexpCode;

	@Column(name = "LAST_AUDIT_CEXP_CODE")
	private BigDecimal lastAuditCexpCode;

	@Column(name = "LAST_AUDIT_RUN_NUMBER")
	private BigDecimal lastAuditRunNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED")
	private Date lastModified;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "NUMBER_OF_CLAIMS_TO_AUDIT")
	private BigDecimal numberOfClaimsToAudit;

	@Column(name = "ONE_TIME_AUDIT_INDICATOR")
	private String oneTimeAuditIndicator;

	@Column(name = "PROVIDER_NUMBER")
	private BigDecimal providerNumber;

	@Column(name = "PROVIDER_TYPE")
	private String providerType;

	@Column(name = "SCND_LAST_AUDIT_CEXP_CODE")
	private BigDecimal scndLastAuditCexpCode;

	@Column(name = "TO_CEXP_CODE")
	private BigDecimal toCexpCode;

	// bi-directional many-to-one association to DssPharmAuditDinXref
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "dssPharmAuditCriteria", cascade = { CascadeType.ALL,
			CascadeType.MERGE }, orphanRemoval = true)
	private List<DssPharmAuditDinXref> dssPharmAuditDinXrefs;

	public DssPharmAuditCriteria() {
	}

	public long getAuditCriteriaId() {
		return this.auditCriteriaId;
	}

	public void setAuditCriteriaId(long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}

	public Date getAuditFromDate() {
		return this.auditFromDate;
	}

	public void setAuditFromDate(Date auditFromDate) {
		this.auditFromDate = auditFromDate;
	}

	public String getAuditFromLastIndicator() {
		return this.auditFromLastIndicator;
	}

	public void setAuditFromLastIndicator(String auditFromLastIndicator) {
		this.auditFromLastIndicator = auditFromLastIndicator;
	}

	public Date getAuditToDate() {
		return this.auditToDate;
	}

	public void setAuditToDate(Date auditToDate) {
		this.auditToDate = auditToDate;
	}

	public String getAuditType() {
		return this.auditType;
	}

	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public String getDinInclusive() {
		return this.dinInclusive;
	}

	public void setDinInclusive(String dinInclusive) {
		this.dinInclusive = dinInclusive;
	}

	public BigDecimal getFromCexpCode() {
		return this.fromCexpCode;
	}

	public void setFromCexpCode(BigDecimal fromCexpCode) {
		this.fromCexpCode = fromCexpCode;
	}

	public BigDecimal getLastAuditCexpCode() {
		return this.lastAuditCexpCode;
	}

	public void setLastAuditCexpCode(BigDecimal lastAuditCexpCode) {
		this.lastAuditCexpCode = lastAuditCexpCode;
	}

	public BigDecimal getLastAuditRunNumber() {
		return this.lastAuditRunNumber;
	}

	public void setLastAuditRunNumber(BigDecimal lastAuditRunNumber) {
		this.lastAuditRunNumber = lastAuditRunNumber;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public BigDecimal getNumberOfClaimsToAudit() {
		return this.numberOfClaimsToAudit;
	}

	public void setNumberOfClaimsToAudit(BigDecimal numberOfClaimsToAudit) {
		this.numberOfClaimsToAudit = numberOfClaimsToAudit;
	}

	public String getOneTimeAuditIndicator() {
		return this.oneTimeAuditIndicator;
	}

	public void setOneTimeAuditIndicator(String oneTimeAuditIndicator) {
		this.oneTimeAuditIndicator = oneTimeAuditIndicator;
	}

	public BigDecimal getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(BigDecimal providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public BigDecimal getScndLastAuditCexpCode() {
		return this.scndLastAuditCexpCode;
	}

	public void setScndLastAuditCexpCode(BigDecimal scndLastAuditCexpCode) {
		this.scndLastAuditCexpCode = scndLastAuditCexpCode;
	}

	public BigDecimal getToCexpCode() {
		return this.toCexpCode;
	}

	public void setToCexpCode(BigDecimal toCexpCode) {
		this.toCexpCode = toCexpCode;
	}

	public List<DssPharmAuditDinXref> getDssPharmAuditDinXrefs() {
		return this.dssPharmAuditDinXrefs;
	}

	public void setDssPharmAuditDinXrefs(List<DssPharmAuditDinXref> dssPharmAuditDinXrefs) {
		this.dssPharmAuditDinXrefs = dssPharmAuditDinXrefs;
	}

	public DssPharmAuditDinXref addDssPharmAuditDinXref(DssPharmAuditDinXref dssPharmAuditDinXref) {
		getDssPharmAuditDinXrefs().add(dssPharmAuditDinXref);
		dssPharmAuditDinXref.setDssPharmAuditCriteria(this);

		return dssPharmAuditDinXref;
	}

	public DssPharmAuditDinXref removeDssPharmAuditDinXref(DssPharmAuditDinXref dssPharmAuditDinXref) {
		getDssPharmAuditDinXrefs().remove(dssPharmAuditDinXref);
		dssPharmAuditDinXref.setDssPharmAuditCriteria(null);

		return dssPharmAuditDinXref;
	}

}