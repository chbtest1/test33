package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_HEALTH_SERVICE_MODIFIERS database table.
 * 
 */
@Embeddable
public class DssHealthServiceModifierPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="HEALTH_SERVICE_ID")
	private String healthServiceId;

	@Column(name="MODIFIER_TYPE")
	private String modifierType;

	@Column(name="MODIFIER_VALUE")
	private String modifierValue;

	public DssHealthServiceModifierPK() {
	}
	public String getHealthServiceId() {
		return this.healthServiceId;
	}
	public void setHealthServiceId(String healthServiceId) {
		this.healthServiceId = healthServiceId;
	}
	public String getModifierType() {
		return this.modifierType;
	}
	public void setModifierType(String modifierType) {
		this.modifierType = modifierType;
	}
	public String getModifierValue() {
		return this.modifierValue;
	}
	public void setModifierValue(String modifierValue) {
		this.modifierValue = modifierValue;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssHealthServiceModifierPK)) {
			return false;
		}
		DssHealthServiceModifierPK castOther = (DssHealthServiceModifierPK)other;
		return 
			this.healthServiceId.equals(castOther.healthServiceId)
			&& this.modifierType.equals(castOther.modifierType)
			&& this.modifierValue.equals(castOther.modifierValue);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.healthServiceId.hashCode();
		hash = hash * prime + this.modifierType.hashCode();
		hash = hash * prime + this.modifierValue.hashCode();
		
		return hash;
	}
}