package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DSS_DEN_AUDIT_CRITERIA database table.
 * 
 */
@Entity
@Table(name="DSS_DEN_AUDIT_CRITERIA")
public class DssDenAuditCriteria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="AUDIT_CRITERIA_ID")
	private long auditCriteriaId;

	@Temporal(TemporalType.DATE)
	@Column(name="AUDIT_FROM_DATE")
	private Date auditFromDate;

	@Column(name="AUDIT_FROM_LAST_INDICATOR")
	private String auditFromLastIndicator;

	@Temporal(TemporalType.DATE)
	@Column(name="AUDIT_TO_DATE")
	private Date auditToDate;

	@Column(name="AUDIT_TYPE")
	private String auditType;

	@Temporal(TemporalType.DATE)
	@Column(name="FROM_PAYMENT_DATE")
	private Date fromPaymentDate;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_AUDIT_PAYMENT_DATE")
	private Date lastAuditPaymentDate;

	@Column(name="LAST_AUDIT_RUN_NUMBER")
	private BigDecimal lastAuditRunNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="MODIFIED_BY")
	private String modifiedBy;

	@Column(name="NUMBER_OF_CLAIMS_TO_AUDIT")
	private BigDecimal numberOfClaimsToAudit;

	@Column(name="ONE_TIME_AUDIT_INDICATOR")
	private String oneTimeAuditIndicator;

	@Column(name="PROVIDER_NUMBER")
	private BigDecimal providerNumber;

	@Column(name="PROVIDER_TYPE")
	private String providerType;

	@Temporal(TemporalType.DATE)
	@Column(name="SCND_LAST_AUDIT_PAYMENT_DATE")
	private Date scndLastAuditPaymentDate;

	@Temporal(TemporalType.DATE)
	@Column(name="TO_PAYMENT_DATE")
	private Date toPaymentDate;

	public DssDenAuditCriteria() {
	}

	public long getAuditCriteriaId() {
		return this.auditCriteriaId;
	}

	public void setAuditCriteriaId(long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}

	public Date getAuditFromDate() {
		return this.auditFromDate;
	}

	public void setAuditFromDate(Date auditFromDate) {
		this.auditFromDate = auditFromDate;
	}

	public String getAuditFromLastIndicator() {
		return this.auditFromLastIndicator;
	}

	public void setAuditFromLastIndicator(String auditFromLastIndicator) {
		this.auditFromLastIndicator = auditFromLastIndicator;
	}

	public Date getAuditToDate() {
		return this.auditToDate;
	}

	public void setAuditToDate(Date auditToDate) {
		this.auditToDate = auditToDate;
	}

	public String getAuditType() {
		return this.auditType;
	}

	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public Date getFromPaymentDate() {
		return this.fromPaymentDate;
	}

	public void setFromPaymentDate(Date fromPaymentDate) {
		this.fromPaymentDate = fromPaymentDate;
	}

	public Date getLastAuditPaymentDate() {
		return this.lastAuditPaymentDate;
	}

	public void setLastAuditPaymentDate(Date lastAuditPaymentDate) {
		this.lastAuditPaymentDate = lastAuditPaymentDate;
	}

	public BigDecimal getLastAuditRunNumber() {
		return this.lastAuditRunNumber;
	}

	public void setLastAuditRunNumber(BigDecimal lastAuditRunNumber) {
		this.lastAuditRunNumber = lastAuditRunNumber;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public BigDecimal getNumberOfClaimsToAudit() {
		return this.numberOfClaimsToAudit;
	}

	public void setNumberOfClaimsToAudit(BigDecimal numberOfClaimsToAudit) {
		this.numberOfClaimsToAudit = numberOfClaimsToAudit;
	}

	public String getOneTimeAuditIndicator() {
		return this.oneTimeAuditIndicator;
	}

	public void setOneTimeAuditIndicator(String oneTimeAuditIndicator) {
		this.oneTimeAuditIndicator = oneTimeAuditIndicator;
	}

	public BigDecimal getProviderNumber() {
		return this.providerNumber;
	}

	public void setProviderNumber(BigDecimal providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return this.providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public Date getScndLastAuditPaymentDate() {
		return this.scndLastAuditPaymentDate;
	}

	public void setScndLastAuditPaymentDate(Date scndLastAuditPaymentDate) {
		this.scndLastAuditPaymentDate = scndLastAuditPaymentDate;
	}

	public Date getToPaymentDate() {
		return this.toPaymentDate;
	}

	public void setToPaymentDate(Date toPaymentDate) {
		this.toPaymentDate = toPaymentDate;
	}

}