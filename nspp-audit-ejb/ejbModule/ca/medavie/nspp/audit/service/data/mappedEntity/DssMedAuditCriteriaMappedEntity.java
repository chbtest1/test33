package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

/**
 * The persistent class for DSS_MED_AUD_CRITERIA + DSS_PROVIDER_2 (Audit Criteria searching)
 */
@Entity
@SqlResultSetMapping(name = "DssMedAuditCriteriaNativeQueryMapping", entities = { @EntityResult(entityClass = DssMedAuditCriteriaMappedEntity.class, fields = {
		@FieldResult(name = "audit_criteria_id", column = "audit_criteria_id"),
		@FieldResult(name = "audit_type", column = "audit_type"),
		@FieldResult(name = "audit_from_date", column = "audit_from_date"),
		@FieldResult(name = "audit_to_date", column = "audit_to_date"),
		@FieldResult(name = "audit_from_payment_date", column = "audit_from_payment_date"),
		@FieldResult(name = "audit_to_payment_date", column = "audit_to_payment_date"),
		@FieldResult(name = "provider_number", column = "provider_number"),
		@FieldResult(name = "provider_type", column = "provider_type"),
		@FieldResult(name = "provider_name", column = "provider_name") }) })
public class DssMedAuditCriteriaMappedEntity {

	@Id
	@Column(name = "AUDIT_CRITERIA_ID")
	private long audit_criteria_id;
	@Column(name = "AUDIT_TYPE")
	private String audit_type;
	@Column(name = "AUDIT_FROM_DATE")
	private Date audit_from_date;
	@Column(name = "AUDIT_TO_DATE")
	private Date audit_to_date;
	@Column(name = "AUDIT_FROM_PAYMENT_DATE")
	private Date audit_from_payment_date;
	@Column(name = "AUDIT_TO_PAYMENT_DATE")
	private Date audit_to_payment_date;
	@Column(name = "PROVIDER_NUMBER")
	private Long provider_number;
	@Column(name = "PROVIDER_TYPE")
	private String provider_type;
	@Column(name = "PROVIDER_NAME")
	private String provider_name;

	/**
	 * @return the audit_criteria_id
	 */
	public long getAudit_criteria_id() {
		return audit_criteria_id;
	}

	/**
	 * @param audit_criteria_id
	 *            the audit_criteria_id to set
	 */
	public void setAudit_criteria_id(long audit_criteria_id) {
		this.audit_criteria_id = audit_criteria_id;
	}

	/**
	 * @return the audit_type
	 */
	public String getAudit_type() {
		return audit_type;
	}

	/**
	 * @param audit_type
	 *            the audit_type to set
	 */
	public void setAudit_type(String audit_type) {
		this.audit_type = audit_type;
	}

	/**
	 * @return the audit_from_date
	 */
	public Date getAudit_from_date() {
		return audit_from_date;
	}

	/**
	 * @param audit_from_date
	 *            the audit_from_date to set
	 */
	public void setAudit_from_date(Date audit_from_date) {
		this.audit_from_date = audit_from_date;
	}

	/**
	 * @return the audit_to_date
	 */
	public Date getAudit_to_date() {
		return audit_to_date;
	}

	/**
	 * @param audit_to_date
	 *            the audit_to_date to set
	 */
	public void setAudit_to_date(Date audit_to_date) {
		this.audit_to_date = audit_to_date;
	}

	/**
	 * @return the audit_from_payment_date
	 */
	public Date getAudit_from_payment_date() {
		return audit_from_payment_date;
	}

	/**
	 * @param audit_from_payment_date
	 *            the audit_from_payment_date to set
	 */
	public void setAudit_from_payment_date(Date audit_from_payment_date) {
		this.audit_from_payment_date = audit_from_payment_date;
	}

	/**
	 * @return the audit_to_payment_date
	 */
	public Date getAudit_to_payment_date() {
		return audit_to_payment_date;
	}

	/**
	 * @param audit_to_payment_date
	 *            the audit_to_payment_date to set
	 */
	public void setAudit_to_payment_date(Date audit_to_payment_date) {
		this.audit_to_payment_date = audit_to_payment_date;
	}

	/**
	 * @return the provider_number
	 */
	public Long getProvider_number() {
		return provider_number;
	}

	/**
	 * @param provider_number
	 *            the provider_number to set
	 */
	public void setProvider_number(Long provider_number) {
		this.provider_number = provider_number;
	}

	/**
	 * @return the provider_type
	 */
	public String getProvider_type() {
		return provider_type;
	}

	/**
	 * @param provider_type
	 *            the provider_type to set
	 */
	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}

	/**
	 * @return the provider_name
	 */
	public String getProvider_name() {
		return provider_name;
	}

	/**
	 * @param provider_name
	 *            the provider_name to set
	 */
	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}

}
