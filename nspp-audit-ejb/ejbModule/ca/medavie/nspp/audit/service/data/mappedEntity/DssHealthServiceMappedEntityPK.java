package ca.medavie.nspp.audit.service.data.mappedEntity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DssHealthServiceMappedEntityPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column
	private String health_service_id;
	@Column
	private String program_code;
	@Column
	private String health_service_code;
	@Column
	private String qualifier_code;
	@Column
	private Date effective_from_date;
	@Column
	private Date effective_to_date;
	@Column
	private String description;
	@Column
	private String modifiers;
	@Column
	private String implicit_modifiers;

	public String getHealth_service_id() {
		return health_service_id;
	}

	public void setHealth_service_id(String health_service_id) {
		this.health_service_id = health_service_id;
	}

	public String getProgram_code() {
		return program_code;
	}

	public void setProgram_code(String program_code) {
		this.program_code = program_code;
	}

	public String getHealth_service_code() {
		return health_service_code;
	}

	public void setHealth_service_code(String health_service_code) {
		this.health_service_code = health_service_code;
	}

	public String getQualifier_code() {
		return qualifier_code;
	}

	public void setQualifier_code(String qualifier_code) {
		this.qualifier_code = qualifier_code;
	}

	public Date getEffective_from_date() {
		return effective_from_date;
	}

	public void setEffective_from_date(Date effective_from_date) {
		this.effective_from_date = effective_from_date;
	}

	public Date getEffective_to_date() {
		return effective_to_date;
	}

	public void setEffective_to_date(Date effective_to_date) {
		this.effective_to_date = effective_to_date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModifiers() {
		return modifiers;
	}

	public void setModifiers(String modifiers) {
		this.modifiers = modifiers;
	}

	public String getImplicit_modifiers() {
		return implicit_modifiers;
	}

	public void setImplicit_modifiers(String implicit_modifiers) {
		this.implicit_modifiers = implicit_modifiers;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((effective_from_date == null) ? 0 : effective_from_date.hashCode());
		result = prime * result + ((effective_to_date == null) ? 0 : effective_to_date.hashCode());
		result = prime * result + ((health_service_code == null) ? 0 : health_service_code.hashCode());
		result = prime * result + ((health_service_id == null) ? 0 : health_service_id.hashCode());
		result = prime * result + ((implicit_modifiers == null) ? 0 : implicit_modifiers.hashCode());
		result = prime * result + ((modifiers == null) ? 0 : modifiers.hashCode());
		result = prime * result + ((program_code == null) ? 0 : program_code.hashCode());
		result = prime * result + ((qualifier_code == null) ? 0 : qualifier_code.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssHealthServiceMappedEntityPK other = (DssHealthServiceMappedEntityPK) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (effective_from_date == null) {
			if (other.effective_from_date != null)
				return false;
		} else if (!effective_from_date.equals(other.effective_from_date))
			return false;
		if (effective_to_date == null) {
			if (other.effective_to_date != null)
				return false;
		} else if (!effective_to_date.equals(other.effective_to_date))
			return false;
		if (health_service_code == null) {
			if (other.health_service_code != null)
				return false;
		} else if (!health_service_code.equals(other.health_service_code))
			return false;
		if (health_service_id == null) {
			if (other.health_service_id != null)
				return false;
		} else if (!health_service_id.equals(other.health_service_id))
			return false;
		if (implicit_modifiers == null) {
			if (other.implicit_modifiers != null)
				return false;
		} else if (!implicit_modifiers.equals(other.implicit_modifiers))
			return false;
		if (modifiers == null) {
			if (other.modifiers != null)
				return false;
		} else if (!modifiers.equals(other.modifiers))
			return false;
		if (program_code == null) {
			if (other.program_code != null)
				return false;
		} else if (!program_code.equals(other.program_code))
			return false;
		if (qualifier_code == null) {
			if (other.qualifier_code != null)
				return false;
		} else if (!qualifier_code.equals(other.qualifier_code))
			return false;
		return true;
	}
}
