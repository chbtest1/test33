package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DSS_MODIFIER_TYPE database table.
 * 
 */
@Entity
@Table(name="DSS_MODIFIER_TYPE")
public class DssModifierType implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DssModifierTypePK id;

	private String description;

	@Temporal(TemporalType.DATE)
	@Column(name="EFFECTIVE_TO_DATE")
	private Date effectiveToDate;

	@Column(name="IMPLICIT_MODIFIER_INDICATOR")
	private String implicitModifierIndicator;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFIED")
	private Date lastModified;

	@Column(name="VALUE_REQUIRED_INDICATOR")
	private String valueRequiredIndicator;

	public DssModifierType() {
	}

	public DssModifierTypePK getId() {
		return this.id;
	}

	public void setId(DssModifierTypePK id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEffectiveToDate() {
		return this.effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public String getImplicitModifierIndicator() {
		return this.implicitModifierIndicator;
	}

	public void setImplicitModifierIndicator(String implicitModifierIndicator) {
		this.implicitModifierIndicator = implicitModifierIndicator;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getValueRequiredIndicator() {
		return this.valueRequiredIndicator;
	}

	public void setValueRequiredIndicator(String valueRequiredIndicator) {
		this.valueRequiredIndicator = valueRequiredIndicator;
	}

}