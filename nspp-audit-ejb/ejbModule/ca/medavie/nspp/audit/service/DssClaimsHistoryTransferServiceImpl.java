package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.DenticareClaim;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferRequest;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferSummary;
import ca.medavie.nspp.audit.service.data.HealthCard;
import ca.medavie.nspp.audit.service.data.MedicareClaim;
import ca.medavie.nspp.audit.service.data.PharmacareClaim;
import ca.medavie.nspp.audit.service.exception.DssClaimsHistoryTransferException;
import ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepository;
import ca.medavie.nspp.audit.service.repository.DssClaimsHistoryTransferRepositoryImpl;

/**
 * Session Bean implementation class DssClaimsHistoryTransferServiceImpl
 */
@Stateless
@Local(DssClaimsHistoryTransferService.class)
@LocalBean
public class DssClaimsHistoryTransferServiceImpl implements DssClaimsHistoryTransferService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private DssClaimsHistoryTransferRepository dssClaimsHistoryTransferRepository;

	@PostConstruct
	public void doInitRepo() {
		dssClaimsHistoryTransferRepository = new DssClaimsHistoryTransferRepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService#getSearchedHealthCard(java.lang.String)
	 */
	@Override
	public HealthCard getSearchedHealthCard(String aHealthCardNumber) throws DssClaimsHistoryTransferException {
		try {
			return dssClaimsHistoryTransferRepository.getSearchedHealthCard(aHealthCardNumber);
		} catch (Exception e) {
			throw new DssClaimsHistoryTransferException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService#getMedicareSeClaims(ca.medavie.nspp.audit.service
	 * .data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<MedicareClaim> getMedicareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException {
		try {
			return dssClaimsHistoryTransferRepository.getMedicareSeClaims(aTransferRequest);
		} catch (Exception e) {
			throw new DssClaimsHistoryTransferException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService#getMedicareMainframeClaims(ca.medavie.nspp.audit
	 * .service.data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<MedicareClaim> getMedicareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException {
		try {
			return dssClaimsHistoryTransferRepository.getMedicareMainframeClaims(aTransferRequest);
		} catch (Exception e) {
			throw new DssClaimsHistoryTransferException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService#getPharmacareSeClaims(ca.medavie.nspp.audit.service
	 * .data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<PharmacareClaim> getPharmacareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException {
		try {
			return dssClaimsHistoryTransferRepository.getPharmacareSeClaims(aTransferRequest);
		} catch (Exception e) {
			throw new DssClaimsHistoryTransferException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService#getPharmacareMainframeClaims(ca.medavie.nspp.audit
	 * .service.data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<PharmacareClaim> getPharmacareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException {
		try {
			return dssClaimsHistoryTransferRepository.getPharmacareMainframeClaims(aTransferRequest);
		} catch (Exception e) {
			throw new DssClaimsHistoryTransferException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService#getDentalClaims(ca.medavie.nspp.audit.service.data
	 * .DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<DenticareClaim> getDentalClaims(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException {
		try {
			return dssClaimsHistoryTransferRepository.getDentalClaims(aTransferRequest);
		} catch (Exception e) {
			throw new DssClaimsHistoryTransferException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService#saveClaimsHistoryTransferStagingData(ca.medavie
	 * .nspp .audit. service.data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public void saveClaimsHistoryTransferStagingData(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException {
		try {
			dssClaimsHistoryTransferRepository.saveClaimsHistoryTransferStagingData(aTransferRequest);
		} catch (Exception e) {
			throw new DssClaimsHistoryTransferException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService#executeClaimsHistoryTransferProcedure(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public void executeClaimsHistoryTransferProcedure(String aSourceHcn, String aTargetHcn)
			throws DssClaimsHistoryTransferException {
		try {
			dssClaimsHistoryTransferRepository.executeClaimsHistoryTransferProcedure(aSourceHcn, aTargetHcn);
		} catch (Exception e) {
			throw new DssClaimsHistoryTransferException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService#getClaimsHistoryTransferSummary(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public DssClaimsHistoryTransferSummary getClaimsHistoryTransferSummary(String aSourceHcn, String aTargetHcn)
			throws DssClaimsHistoryTransferException {
		try {
			return dssClaimsHistoryTransferRepository.getClaimsHistoryTransferSummary(aSourceHcn, aTargetHcn);
		} catch (Exception e) {
			throw new DssClaimsHistoryTransferException(e.getMessage(), e);
		}
	}
}
