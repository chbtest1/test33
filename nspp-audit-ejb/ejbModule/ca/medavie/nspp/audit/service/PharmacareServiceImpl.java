package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DIN;
import ca.medavie.nspp.audit.service.data.DinSearchCriteria;
import ca.medavie.nspp.audit.service.data.IndividualExclusion;
import ca.medavie.nspp.audit.service.data.PharmAuditDinExcl;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponse;
import ca.medavie.nspp.audit.service.exception.PharmcareServiceException;
import ca.medavie.nspp.audit.service.repository.PharmacareRepository;
import ca.medavie.nspp.audit.service.repository.PharmacareRepositoryImpl;

/**
 * Session Bean implementation class AccountingServiceImpl
 */
@Stateless
@Local(PharmacareService.class)
@LocalBean
public class PharmacareServiceImpl implements PharmacareService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private PharmacareRepository pharmacareRepository;

	@PostConstruct
	public void doInitRepo() {
		pharmacareRepository = new PharmacareRepositoryImpl(em);
	}

	public PharmacareRepository getPharmCareRepository() {
		return pharmacareRepository;
	}

	public void setPharmCareRepository(PharmacareRepository pharmCareRepository) {
		this.pharmacareRepository = pharmCareRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.PharmacareService#getSearchedPharmacareALR(ca.medavie.nspp.audit.service.data.
	 * PharmacareAuditLetterResponseSearchCriteria, int, int)
	 */
	@Override
	public List<PharmacareAuditLetterResponse> getSearchedPharmacareALR(
			PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria, int startRow, int maxRows)
			throws PharmcareServiceException {
		try {
			return pharmacareRepository.getSearchedPharmacareALR(pharmCareALRSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.PharmacareService#getSearchedPharmacareALRCount(ca.medavie.nspp.audit.service.data
	 * .PharmacareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public Integer getSearchedPharmacareALRCount(PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria)
			throws PharmcareServiceException {
		try {
			return pharmacareRepository.getSearchedPharmacareALRCount(pharmCareALRSearchCriteria);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	@Override
	public void savePharmacareALR(PharmacareAuditLetterResponse selectedDCALR) throws PharmcareServiceException {
		try {
			pharmacareRepository.savePharmacareALR(selectedDCALR);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.PharmacareService#getSearchPractitionerAuditCriterias(ca.medavie.nspp.audit.service
	 * .data.AuditCriteriaSearchCriteria, int, int)
	 */
	@Override
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows) throws PharmcareServiceException {
		try {
			return pharmacareRepository.getSearchPractitionerAuditCriterias(aSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.PharmacareService#getSearchedAuditCriteriaCount(ca.medavie.nspp.audit.service.data
	 * .AuditCriteriaSearchCriteria)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria)
			throws PharmcareServiceException {
		try {
			return pharmacareRepository.getSearchedAuditCriteriaCount(anAuditCriteriaSearchCriteria);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.PharmacareService#getDefaultAuditCriteriaByType(java.lang.String)
	 */
	@Override
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType) throws PharmcareServiceException {
		try {
			return pharmacareRepository.getDefaultAuditCriteriaByType(anAuditType);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.PharmacareService#getPractitionerAuditCriteriaById(java.lang.Long)
	 */
	@Override
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId) throws PharmcareServiceException {
		try {
			return pharmacareRepository.getPractitionerAuditCriteriaById(anId);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.PharmacareService#savePharmicareAuditCriteria(ca.medavie.nspp.audit.service.data
	 * .AuditCriteria)
	 */
	@Override
	public void savePharmicareAuditCriteria(AuditCriteria anAuditCriteria) throws PharmcareServiceException {
		try {
			pharmacareRepository.savePharmacareAuditCriteria(anAuditCriteria);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.PharmacareService#getSearchDins(ca.medavie.nspp.audit.service.data.DinSearchCriteria
	 * )
	 */
	@Override
	public List<DIN> getSearchDins(DinSearchCriteria aSearchCriteria) throws PharmcareServiceException {
		try {
			return pharmacareRepository.getSearchDins(aSearchCriteria);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.PharmacareService#saveIndividualExclusions(java.util.List)
	 */
	@Override
	public void saveIndividualExclusions(List<IndividualExclusion> aListOfIndExclToSave)
			throws PharmcareServiceException {
		try {
			pharmacareRepository.saveIndividualExclusions(aListOfIndExclToSave);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.PharmacareService#getIndividualExclusions()
	 */
	@Override
	public List<IndividualExclusion> getIndividualExclusions() throws PharmcareServiceException {
		try {
			return pharmacareRepository.getIndividualExclusions();
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	@Override
	public List<PharmAuditDinExcl> getPharmAuditDinExcl() throws PharmcareServiceException {
		try {
			return pharmacareRepository.getDinExclusions();
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	@Override
	public void saveDinExclusions(List<PharmAuditDinExcl> pharmAuditDinExclResults)
			throws PharmcareServiceException {
		try {
			pharmacareRepository.saveDinExclusions(pharmAuditDinExclResults);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}

	
	@Override
	public void deleteDinExcl(PharmAuditDinExcl pharmAuditDinExcl)
			throws PharmcareServiceException {
		try {
			pharmacareRepository.deleteDinExcl(pharmAuditDinExcl);
		} catch (Exception e) {
			throw new PharmcareServiceException(e.getMessage(), e);
		}
	}
}
