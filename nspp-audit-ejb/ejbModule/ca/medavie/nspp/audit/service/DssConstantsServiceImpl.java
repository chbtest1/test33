package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.DssConstantBO;
import ca.medavie.nspp.audit.service.exception.DssConstantsServiceException;
import ca.medavie.nspp.audit.service.repository.DssConstantsRepository;
import ca.medavie.nspp.audit.service.repository.DssConstantsRepositoryImpl;

/**
 * Session Bean implementation class DssConstantsServiceImpl
 */
@Stateless
@Local(DssConstantsService.class)
@LocalBean
public class DssConstantsServiceImpl implements DssConstantsService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private DssConstantsRepository dssConstantsRepository;

	@PostConstruct
	public void doInitRepo() {
		dssConstantsRepository = new DssConstantsRepositoryImpl(em);
	}

	/* (non-Javadoc)
	 * @see ca.medavie.nspp.audit.service.DssConstantsService#getDssConstants()
	 */
	@Override
	public List<DssConstantBO> getDssConstants() throws DssConstantsServiceException {
		try {
			return dssConstantsRepository.getDssConstants();
		} catch (Exception e) {
			throw new DssConstantsServiceException(e.getMessage(), e);
		}
	}

	/* (non-Javadoc)
	 * @see ca.medavie.nspp.audit.service.DssConstantsService#updateDssConstant(ca.medavie.nspp.audit.service.data.DssConstantBO)
	 */
	@Override
	public void updateDssConstant(DssConstantBO dssConstant) throws DssConstantsServiceException {
		try {
			dssConstantsRepository.updateDssConstant(dssConstant);
		} catch (Exception e) {
			throw new DssConstantsServiceException(e.getMessage(), e);
		}
	}

}
