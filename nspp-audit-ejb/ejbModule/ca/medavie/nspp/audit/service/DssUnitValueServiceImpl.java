package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.DssUnitValueBO;
import ca.medavie.nspp.audit.service.exception.DssUnitValueServiceException;
import ca.medavie.nspp.audit.service.repository.DssUnitValueRepository;
import ca.medavie.nspp.audit.service.repository.DssUnitValueRepositoryImpl;

/**
 * Session Bean implementation class AccountingServiceImpl
 */
@Stateless
@Local(DssUnitValueService.class)
@LocalBean
public class DssUnitValueServiceImpl implements DssUnitValueService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private DssUnitValueRepository dssUnitValueRepository;

	@PostConstruct
	public void doInitRepo() {
		dssUnitValueRepository = new DssUnitValueRepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DssUnitValueService#getDssUnitValues()
	 */
	@Override
	public List<DssUnitValueBO> getDssUnitValues() throws DssUnitValueServiceException {

		try {
			return dssUnitValueRepository.getDssUnitValues();
		} catch (Exception e) {
			throw new DssUnitValueServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssUnitValueService#saveOrUpdateDssUnitValue(ca.medavie.nspp.audit.service.data
	 * .DssUnitValueBO)
	 */
	@Override
	public DssUnitValueBO saveOrUpdateDssUnitValue(DssUnitValueBO aDssUnitValueBO) throws DssUnitValueServiceException {
		try {
			return dssUnitValueRepository.saveOrUpdateDssUnitValue(aDssUnitValueBO);
		} catch (Exception e) {
			throw new DssUnitValueServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssUnitValueService#overlaps(ca.medavie.nspp.audit.service.data.DssUnitValueBO)
	 */
	@Override
	public Boolean overlaps(DssUnitValueBO aDssUnitValue) throws DssUnitValueServiceException {
		try {
			return dssUnitValueRepository.overlaps(aDssUnitValue);
		} catch (Exception e) {
			throw new DssUnitValueServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DssUnitValueService#deleteDssUnitValues(java.util.List)
	 */
	@Override
	public void deleteDssUnitValues(List<DssUnitValueBO> aListOfDssUnitValuesForDelete)
			throws DssUnitValueServiceException {
		try {
			dssUnitValueRepository.deleteDssUnitValues(aListOfDssUnitValuesForDelete);
		} catch (Exception e) {
			throw new DssUnitValueServiceException(e.getMessage(), e);
		}
	}
}
