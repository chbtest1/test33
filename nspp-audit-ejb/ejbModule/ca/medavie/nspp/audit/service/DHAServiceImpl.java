package ca.medavie.nspp.audit.service;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.DistrictHealthAuthority;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.exception.DHAServiceException;
import ca.medavie.nspp.audit.service.repository.DHARepository;
import ca.medavie.nspp.audit.service.repository.DHARepositoryImpl;

/**
 * Session Bean implementation class AuditResultServiceImpl
 */
@Stateless
@Local(DHAService.class)
@LocalBean
public class DHAServiceImpl implements DHAService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private DHARepository dhaRepository;

	@PostConstruct
	public void doInitRepo() {
		dhaRepository = new DHARepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DHAService#getDHAs()
	 */
	@Override
	public List<DistrictHealthAuthority> getDHAs() throws DHAServiceException {

		try {
			return dhaRepository.getDHAs();
		} catch (Exception e) {
			throw new DHAServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DHAService#getSearchedPractitioners(ca.medavie.nspp.audit.service.data.
	 * PractitionerSearchCriteria)
	 */
	@Override
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aSearchCriteria)
			throws DHAServiceException {

		try {
			return dhaRepository.getSearchedPractitioners(aSearchCriteria);
		} catch (Exception e) {
			throw new DHAServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DHAService#saveDHAChanges(ca.medavie.nspp.audit.service.data.DistrictHealthAuthority
	 * )
	 */
	@Override
	public void saveDHAChanges(DistrictHealthAuthority aDha) throws DHAServiceException {

		try {
			dhaRepository.saveDHAChanges(aDha);
		} catch (Exception e) {
			throw new DHAServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DHAService#deleteDHAs(java.util.List)
	 */
	@Override
	public void deleteDHAs(List<DistrictHealthAuthority> aListOfDhasToDelete) throws DHAServiceException {

		try {
			dhaRepository.deleteDHAs(aListOfDhasToDelete);
		} catch (Exception e) {
			throw new DHAServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DHAService#updateDHAContractTownDetails(ca.medavie.nspp.audit.service.data.
	 * DistrictHealthAuthority)
	 */
	@Override
	public void updateDHAContractTownDetails(DistrictHealthAuthority aDha) throws DHAServiceException {
		try {
			dhaRepository.updateDHAContractTownDetails(aDha);
		} catch (Exception e) {
			throw new DHAServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DHAService#getZoneIds(java.lang.String)
	 */
	@Override
	public Map<String, String> getZoneIds(String aTownName) throws DHAServiceException {
		
		try {
			return dhaRepository.getZoneIds(aTownName);
		} catch (Exception e) {
			throw new DHAServiceException(e.getMessage(), e);
		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DHAService#getDhaCodes(java.lang.String, java.lang.String)
	 */
	@Override
	public Map<String, String> getDhaCodes(String aTownName, String aZoneId) throws DHAServiceException {
		
		try {
			return dhaRepository.getDhaCodes(aTownName, aZoneId);
		} catch (Exception e) {
			throw new DHAServiceException(e.getMessage(), e);
		}
		
	}
}
