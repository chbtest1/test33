package ca.medavie.nspp.audit.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.BaseSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.exception.PeerGroupServiceException;
import ca.medavie.nspp.audit.service.repository.PeerGroupRepository;
import ca.medavie.nspp.audit.service.repository.PeerGroupRepositoryImpl;

/**
 * Session Bean implementation class PeerGroupServiceImpl
 */
@Stateless
@Local(PeerGroupService.class)
@LocalBean
public class PeerGroupServiceImpl implements PeerGroupService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private PeerGroupRepository peerGroupRepository;

	@PostConstruct
	public void doInitRepo() {
		peerGroupRepository = new PeerGroupRepositoryImpl(em);
	}

	public PeerGroupRepository getPeerGroupRepository() {
		return peerGroupRepository;
	}

	public void setPeerGroupRepository(PeerGroupRepository peerGroupRepository) {
		this.peerGroupRepository = peerGroupRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.PeerGroupService#getSearchedPeerGroups(ca.medavie.nspp.audit.service.data.
	 * PeerGroupSearchCriteria, int, int)
	 */
	@Override
	public List<PeerGroup> getSearchedPeerGroups(PeerGroupSearchCriteria peerGroupSearchCriteria, int startRow,
			int maxRows) throws PeerGroupServiceException {
		try {
			return peerGroupRepository.getSearchedPeerGroups(peerGroupSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new PeerGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.PeerGroupService#getSearchedPeerGroupsCount(ca.medavie.nspp.audit.service.data.
	 * PeerGroupSearchCriteria)
	 */
	@Override
	public Integer getSearchedPeerGroupsCount(PeerGroupSearchCriteria peerGroupSearchCriteria)
			throws PeerGroupServiceException {
		try {
			return peerGroupRepository.getSearchedPeerGroupsCount(peerGroupSearchCriteria);
		} catch (Exception e) {
			throw new PeerGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.PeerGroupService#getSearchedPractitioners(ca.medavie.nspp.audit.service.data.
	 * PractitionerSearchCriteria)
	 */
	@Override
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria)
			throws PeerGroupServiceException {
		try {
			return peerGroupRepository.getSearchedPractitioners(aPractitionerSearchCriteria);
		} catch (Exception e) {
			throw new PeerGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.PeerGroupService#getLatestPeerGroups()
	 */
	@Override
	public List<PeerGroup> getLatestPeerGroups() throws PeerGroupServiceException {
		try {
			return peerGroupRepository.getLatestPeerGroups();
		} catch (Exception e) {
			throw new PeerGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.PeerGroupService#copyPeerGroups(java.util.List, java.lang.String)
	 */
	@Override
	public void copyPeerGroups(List<PeerGroup> aListOfPeerGroupsToCopy, String aUserId)
			throws PeerGroupServiceException {
		try {
			peerGroupRepository.copyPeerGroups(aListOfPeerGroupsToCopy, aUserId);
		} catch (Exception e) {
			throw new PeerGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.PeerGroupService#saveOrUpdatePeerGroup(ca.medavie.nspp.audit.service.data.PeerGroup
	 * )
	 */
	@Override
	public void saveOrUpdatePeerGroup(PeerGroup aPeerGroup) throws PeerGroupServiceException, PeerGroupServiceException {

		try {
			peerGroupRepository.saveOrUpdatePeerGroup(aPeerGroup);
		} catch (Exception e) {
			throw new PeerGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.PeerGroupService#getTownCriteriaSearchResults(ca.medavie.nspp.audit.service.data
	 * .TownCriteriaSearchCriteria)
	 */
	@Override
	public List<TownCriteria> getTownCriteriaSearchResults(TownCriteriaSearchCriteria townSearchCriteria)
			throws PeerGroupServiceException {

		try {
			return peerGroupRepository.getTownCriteriaSearchResults(townSearchCriteria);
		} catch (Exception e) {
			throw new PeerGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.PeerGroupService#getHealthServiceEFDropDown()
	 */
	@Override
	public Map<String, String> getHealthServiceEFDropDown() throws PeerGroupServiceException {

		try {
			return peerGroupRepository.getHealthServiceEFDropDown();
		} catch (Exception e) {
			throw new PeerGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.PeerGroupService#getHealthServiceCriteriaSearchResults(ca.medavie.nspp.audit.service
	 * .data.HealthServiceCriteriaSearchCriteria)
	 */
	@Override
	public List<HealthServiceCriteria> getHealthServiceCriteriaSearchResults(
			HealthServiceCriteriaSearchCriteria hscSearchCriteria) throws PeerGroupServiceException {
		try {
			return peerGroupRepository.getHealthServiceCriteriaSearchResults(hscSearchCriteria);
		} catch (Exception e) {
			throw new PeerGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.PeerGroupService#getPeerGroupDetails(ca.medavie.nspp.audit.service.data.PeerGroup)
	 */
	@Override
	public PeerGroup getPeerGroupDetails(PeerGroup aPeerGroup) throws PeerGroupServiceException {
		try {
			return peerGroupRepository.getPeerGroupDetails(aPeerGroup);
		} catch (Exception e) {
			throw new PeerGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.PeerGroupService#getFilteredDropDowns(ca.medavie.nspp.audit.service.data.
	 * BaseSearchCriteria, boolean)
	 */
	@Override
	public Map<String, BigDecimal>[] getFilteredTownSCDropDowns(BaseSearchCriteria aSearchCriteria) throws PeerGroupServiceException {
		try {
			return peerGroupRepository.getFilteredTownSCDropDowns(aSearchCriteria);
		} catch (Exception e) {
			throw new PeerGroupServiceException(e.getMessage(), e);
		}
	}

}
