package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.BusinessArrangement;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.IndividualExclusion;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;
import ca.medavie.nspp.audit.service.repository.MedicareRepository;
import ca.medavie.nspp.audit.service.repository.MedicareRepositoryImpl;

/**
 * Session Bean implementation class MedicareServiceImpl
 */
@Stateless
@Local(MedicareService.class)
@LocalBean
public class MedicareServiceImpl implements MedicareService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private MedicareRepository medicareRepository;

	@PostConstruct
	public void doInitRepo() {
		medicareRepository = new MedicareRepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.MedicareService#getSearchedHealthServiceCriteriaDescriptions(ca.medavie.nspp.audit
	 * .service.data.HealthServiceCriteriaSearchCriteria)
	 */
	@Override
	public List<HealthServiceCriteria> getSearchedHealthServiceCriteriaDescriptions(
			HealthServiceCriteriaSearchCriteria aSearchCriteria) throws MedicareServiceException {
		try {
			return medicareRepository.getSearchedHealthServiceCriteriaDescriptions(aSearchCriteria);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#saveHealthServiceCriteriaDescriptions(java.util.List)
	 */
	@Override
	public void saveHealthServiceCriteriaDescriptions(List<HealthServiceCriteria> aListOfHsc)
			throws MedicareServiceException {
		try {
			medicareRepository.saveHealthServiceCriteriaDescriptions(aListOfHsc);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#getHealthServiceCriteriaExclusions()
	 */
	@Override
	public List<HealthServiceCriteria> getHealthServiceCriteriaExclusions() throws MedicareServiceException {
		try {
			return medicareRepository.getHealthServiceCriteriaExclusions();
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#saveHealthServiceCriteriaExclusions(java.util.List)
	 */
	@Override
	public void saveHealthServiceCriteriaExclusions(List<HealthServiceCriteria> aListOfHscForSave)
			throws MedicareServiceException {
		try {
			medicareRepository.saveHealthServiceCriteriaExclusions(aListOfHscForSave);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#deleteHealthServiceCriteriaExclusions(java.util.List)
	 */
	@Override
	public void deleteHealthServiceCriteriaExclusions(List<HealthServiceCriteria> aListOfHscForDelete)
			throws MedicareServiceException {
		try {
			medicareRepository.deleteHealthServiceCriteriaExclusions(aListOfHscForDelete);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#getIndividualExclusions()
	 */
	@Override
	public List<IndividualExclusion> getIndividualExclusions() throws MedicareServiceException {
		try {
			return medicareRepository.getIndividualExclusions();
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#saveIndividualExclusions(java.util.List)
	 */
	@Override
	public void saveIndividualExclusions(List<IndividualExclusion> aListOfIndExclToSave)
			throws MedicareServiceException {
		try {
			medicareRepository.saveIndividualExclusions(aListOfIndExclToSave);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#getHealthCardNumberOwnerName(java.lang.String)
	 */
	@Override
	public String getHealthCardNumberOwnerName(String aHealthCardNumber) throws MedicareServiceException {
		try {
			return medicareRepository.getHealthCardNumberOwnerName(aHealthCardNumber);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#saveMedicareAuditCriteria(ca.medavie.nspp.audit.service.data.
	 * AuditCriteria)
	 */
	@Override
	public void saveMedicareAuditCriteria(AuditCriteria anAuditCriteria) throws MedicareServiceException {
		try {
			medicareRepository.saveMedicareAuditCriteria(anAuditCriteria);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#getPractitionerName(java.lang.Long, java.lang.String)
	 */
	@Override
	public String getPractitionerName(Long aPractitionerId, String aSubcategory) throws MedicareServiceException {
		try {
			return medicareRepository.getPractitionerName(aPractitionerId, aSubcategory);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#getMedicarePractitionerAuditCriteria(java.lang.Long,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public AuditCriteria getMedicarePractitionerAuditCriteria(Long aPractitionerId, String aSubcategory,
			String anAuditType) throws MedicareServiceException {
		try {
			return medicareRepository.getMedicarePractitionerAuditCriteria(aPractitionerId, aSubcategory, anAuditType);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.MedicareService#getSearchPractitionerAuditCriterias(ca.medavie.nspp.audit.service
	 * .data.AuditCriteriaSearchCriteria, int, int)
	 */
	@Override
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows) throws MedicareServiceException {
		try {
			return medicareRepository.getSearchPractitionerAuditCriterias(aSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.MedicareService#getSearchedAuditCriteriaCount(ca.medavie.nspp.audit.service.data
	 * .AuditCriteriaSearchCriteria)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria)
			throws MedicareServiceException {
		try {
			return medicareRepository.getSearchedAuditCriteriaCount(anAuditCriteriaSearchCriteria);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#getPractitionerAuditCriteriaById(java.lang.Long)
	 */
	@Override
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId) throws MedicareServiceException {
		try {
			return medicareRepository.getPractitionerAuditCriteriaById(anId);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#getDefaultAuditCriteriaByType(java.lang.String)
	 */
	@Override
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType) throws MedicareServiceException {
		try {
			return medicareRepository.getDefaultAuditCriteriaByType(anAuditType);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#getSearchedMedicareALR(ca.medavie.nspp.audit.service.data.
	 * MedicareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public List<MedicareAuditLetterResponse> getSearchedMedicareALR(
			MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria, int startRow, int maxRows)
			throws MedicareServiceException {
		try {
			return medicareRepository.getSearchedMedicareALR(medicareALRSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.MedicareService#getSearchedMedicareALRCount(ca.medavie.nspp.audit.service.data.
	 * MedicareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public Integer getSearchedMedicareALRCount(MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria)
			throws MedicareServiceException {
		try {
			return medicareRepository.getSearchedMedicareALRCount(medicareALRSearchCriteria);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#saveMedicareALR(ca.medavie.nspp.audit.service.data.
	 * MedicareAuditLetterResponse)
	 */
	@Override
	public void saveMedicareALR(MedicareAuditLetterResponse selectedMCALR) throws MedicareServiceException {
		try {
			medicareRepository.saveMedicareALR(selectedMCALR);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#getNumberOfLettersCreated(java.lang.Long, java.lang.String)
	 */
	@Override
	public Long getNumberOfLettersCreated(Long anAuditRunNumber, String anAuditType) throws MedicareServiceException {
		try {
			return medicareRepository.getNumberOfLettersCreated(anAuditRunNumber, anAuditType);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#getBusinessArrangement(java.lang.Long)
	 */
	@Override
	public BusinessArrangement getBusinessArrangement(Long aBusinessArrangementNumber) throws MedicareServiceException {
		try {
			return medicareRepository.getBusinessArrangement(aBusinessArrangementNumber);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#getBusinessArrangementExclusions()
	 */
	@Override
	public List<BusinessArrangement> getBusinessArrangementExclusions() throws MedicareServiceException {
		try {
			return medicareRepository.getBusinessArrangementExclusions();
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.MedicareService#saveBusinessArrangementExclusion(ca.medavie.nspp.audit.service.
	 * data.BusinessArrangement)
	 */
	@Override
	public void saveBusinessArrangementExclusion(BusinessArrangement aBusinessArrangementExclusion)
			throws MedicareServiceException {
		try {
			medicareRepository.saveBusinessArrangementExclusion(aBusinessArrangementExclusion);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.MedicareService#deleteBusinessArrangementExclusions(java.util.List)
	 */
	@Override
	public void deleteBusinessArrangementExclusions(List<BusinessArrangement> aListOfBAExclusionsForDelete)
			throws MedicareServiceException {
		try {
			medicareRepository.deleteBusinessArrangementExclusions(aListOfBAExclusionsForDelete);
		} catch (Exception e) {
			throw new MedicareServiceException(e.getMessage(), e);
		}
	}
}
