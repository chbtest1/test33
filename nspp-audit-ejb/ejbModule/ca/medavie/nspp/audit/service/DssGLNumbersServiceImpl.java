package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.DssGLNumberBO;
import ca.medavie.nspp.audit.service.exception.DssGLNumbersServiceException;
import ca.medavie.nspp.audit.service.repository.DssGLNumbersRepository;
import ca.medavie.nspp.audit.service.repository.DssGLNumbersRepositoryImpl;

/**
 * Session Bean implementation class DssGLNumbersServiceImpl
 */
@Stateless
@Local(DssGLNumbersService.class)
@LocalBean
public class DssGLNumbersServiceImpl implements DssGLNumbersService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private DssGLNumbersRepository dssGLNumbersRepository;

	@PostConstruct
	public void doInitRepo() {
		dssGLNumbersRepository = new DssGLNumbersRepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DssGLNumbersService#getGLNumbers()
	 */
	@Override
	public List<DssGLNumberBO> getGLNumbers() throws DssGLNumbersServiceException {
		try {
			return dssGLNumbersRepository.getGLNumbers();
		} catch (Exception e) {
			throw new DssGLNumbersServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DssGLNumbersService#getGLNumberByGL(java.lang.Long)
	 */
	@Override
	public DssGLNumberBO getGLNumberByGL(Long aGlNumber) throws DssGLNumbersServiceException {
		try {
			return dssGLNumbersRepository.getGLNumberByGL(aGlNumber);
		} catch (Exception e) {
			throw new DssGLNumbersServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DssGLNumbersService#saveGLNumber(ca.medavie.nspp.audit.service.data.DssGLNumber)
	 */
	@Override
	public void saveGLNumber(DssGLNumberBO aDssGLNumber) throws DssGLNumbersServiceException {
		try {
			dssGLNumbersRepository.saveGLNumber(aDssGLNumber);
		} catch (Exception e) {
			throw new DssGLNumbersServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DssGLNumbersService#deleteGLNumbers(java.util.List)
	 */
	@Override
	public void deleteGLNumbers(List<DssGLNumberBO> aListOfGLNumbersForDelete) throws DssGLNumbersServiceException {
		try {
			dssGLNumbersRepository.deleteGLNumbers(aListOfGLNumbersForDelete);
		} catch (Exception e) {
			throw new DssGLNumbersServiceException(e.getMessage(), e);
		}
	}
}
