package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.exception.DenticareServiceException;
import ca.medavie.nspp.audit.service.repository.DenticareRepository;
import ca.medavie.nspp.audit.service.repository.DenticareRepositoryImpl;

/**
 * Session Bean implementation class DenticareServiceImpl
 */
@Stateless
@Local(DenticareService.class)
@LocalBean
public class DenticareServiceImpl implements DenticareService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private DenticareRepository denticareRepository;

	@PostConstruct
	public void doInitRepo() {
		denticareRepository = new DenticareRepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DenticareService#getSearchedDenticareALR(ca.medavie.nspp.audit.service.data.
	 * DenticareAuditLetterResponseSearchCriteria, int, int)
	 */
	@Override
	public List<DenticareAuditLetterResponse> getSearchedDenticareALR(
			DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria, int startRow, int maxRows)
			throws DenticareServiceException {
		try {
			return denticareRepository.getSearchedDenticareALR(denticareALRSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new DenticareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DenticareService#getSearchedDenticareALRCount(ca.medavie.nspp.audit.service.data
	 * .DenticareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public Integer getSearchedDenticareALRCount(DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria)
			throws DenticareServiceException {
		try {
			return denticareRepository.getSearchedDenticareALRCount(denticareALRSearchCriteria);
		} catch (Exception e) {
			throw new DenticareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DenticareService#saveDenticareALR(ca.medavie.nspp.audit.service.data.
	 * DenticareAuditLetterResponse)
	 */
	@Override
	public void saveDenticareALR(DenticareAuditLetterResponse selectedDCALR) throws DenticareServiceException {
		try {
			denticareRepository.saveDenticareALR(selectedDCALR);
		} catch (Exception e) {
			throw new DenticareServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DenticareService#getSearchPractitionerAuditCriterias(ca.medavie.nspp.audit.service
	 * .data.AuditCriteriaSearchCriteria, int, int)
	 */
	@Override
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows) throws DenticareServiceException {
		try {
			return denticareRepository.getSearchPractitionerAuditCriterias(aSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new DenticareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DenticareService#getSearchedAuditCriteriaCount(ca.medavie.nspp.audit.service.data
	 * .AuditCriteriaSearchCriteria)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria)
			throws DenticareServiceException {
		try {
			return denticareRepository.getSearchedAuditCriteriaCount(anAuditCriteriaSearchCriteria);
		} catch (Exception e) {
			throw new DenticareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DenticareService#getPractitionerAuditCriteriaById(java.lang.Long)
	 */
	@Override
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId) throws DenticareServiceException {
		try {
			return denticareRepository.getPractitionerAuditCriteriaById(anId);
		} catch (Exception e) {
			throw new DenticareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DenticareService#getDefaultAuditCriteriaByType(java.lang.String)
	 */
	@Override
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType) throws DenticareServiceException {
		try {
			return denticareRepository.getDefaultAuditCriteriaByType(anAuditType);
		} catch (Exception e) {
			throw new DenticareServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.DenticareService#saveDenticareAuditCriteria(ca.medavie.nspp.audit.service.data.
	 * AuditCriteria)
	 */
	@Override
	public void saveDenticareAuditCriteria(AuditCriteria anAuditCriteria) throws DenticareServiceException {
		try {
			denticareRepository.saveDenticareAuditCriteria(anAuditCriteria);
		} catch (Exception e) {
			throw new DenticareServiceException(e.getMessage(), e);
		}
	}
}
