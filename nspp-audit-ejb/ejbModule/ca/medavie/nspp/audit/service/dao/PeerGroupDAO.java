package ca.medavie.nspp.audit.service.dao;

import java.util.Date;
import java.util.List;

import ca.medavie.nspp.audit.service.data.DssCodeTableAlphaEntry;
import ca.medavie.nspp.audit.service.data.DssCodeTableAlphaEntryPK;
import ca.medavie.nspp.audit.service.data.DssProvider2;
import ca.medavie.nspp.audit.service.data.DssProviderPeerGroup;
import ca.medavie.nspp.audit.service.data.DssProviderPeerGroupPK;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.TownCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssHealthServiceMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPeerGroupMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPostalCodeMappedEntity;

/**
 * Interface of the PeerGroup DAO layer
 */
public interface PeerGroupDAO {

	/**
	 * @param peerGroupSearchCriteria
	 *            given search criteria
	 * @param startRow
	 * @param maxRows
	 * @return A list of DssPeerGroupMappedEntity Object based on the search criteria
	 */
	public List<DssPeerGroupMappedEntity> getBasePeerGroups(PeerGroupSearchCriteria peerGroupSearchCriteria,
			int startRow, int maxRows);

	/**
	 * Counts how many records the entire PeerGroup search will return without pagination
	 * 
	 * @param peerGroupSearchCriteria
	 * @return total PeerGroups found
	 */
	public Integer getSearchedPeerGroupsCount(PeerGroupSearchCriteria peerGroupSearchCriteria);

	/**
	 * @param DssProviderPeerGroupPK
	 *            given DssProviderPeerGroupPK
	 * @return A DssProviderPeerGroup Object based on the search DssProviderPeerGroupPK
	 */
	public DssProviderPeerGroup getDssProviderPeerGroupByPK(DssProviderPeerGroupPK aPK);

	/**
	 * Loads matching Providers(Practitioners) Each record contains the Practitioners Group details as well
	 * 
	 * Search used on the MaintainPractitioners in Groups page
	 * 
	 * @param aPractitionerSearchCriteria
	 * @return list of matching DssProvider2 records
	 */
	public List<DssProvider2> getSearchedDssProvider2(PractitionerSearchCriteria aPractitionerSearchCriteria);

	/**
	 * Get a list of DssProviderPeerGroup objects by their latest YearEndDate. Previous YearEndDate revisions will be
	 * ignored
	 * 
	 * @return a list of DssPeerGroupMappedEntity objects by their latest YearEndDate.
	 */
	public List<DssPeerGroupMappedEntity> getLatestDssProviderPeerGroup();

	/**
	 * save or update DssProviderPeerGroup
	 * 
	 * @param aDssProviderPeerGroupToSave
	 *            - a DssProviderPeerGroup to save
	 */
	public DssProviderPeerGroup saveOrUpdateDssProviderPeerGroup(DssProviderPeerGroup aDssProviderPeerGroupToSave);

	/**
	 * Bulk Save or update DssProviderPeerGroup records. Also saves children elements containing the new year end date
	 * 
	 * @param aListOfDssProviderPeerGroupsToSave
	 * @return
	 */
	public void copyDssProviderPeerGroups(List<DssProviderPeerGroup> aListOfDssProviderPeerGroupsToSave);

	/**
	 * @param DssProviderPeerGroupPK
	 *            - a given DssProviderPeerGroupPK
	 * @return DssProviderPeerGroup by given id
	 * 
	 */
	public DssProviderPeerGroup getDssProviderPeerGroupByID(DssProviderPeerGroupPK pk);

	/**
	 * @param - a DssCodeTableAlphaEntryPK aPK
	 * @return DssCodeTableAlphaEntry by 102 and given specialty code
	 */
	public DssCodeTableAlphaEntry getSpeciatyDesc(DssCodeTableAlphaEntryPK aPK);

	/**
	 * Requests the next value from the DSS_PROVIDER_PEER_GROUP_ID_SEQ
	 * 
	 * @return next id value
	 */
	public Long getNextValueFromDssProviderPeerGroupSeq();

	/**
	 * @param townSearchCriteria
	 *            - given town search criteria
	 * @return A list of DssPostalCode Objects by given certain search criteria
	 */
	public List<Object[]> getSearcheDssPostalCodeBySC(TownCriteriaSearchCriteria townSearchCriteria);

	/**
	 * @return effective from data objects
	 */
	public List<Date> getHealthServiceEFCodes();

	/**
	 * @param hscSearchCriteria
	 * @return a list of DssHealthServiceMappedEntity
	 */
	public List<DssHealthServiceMappedEntity> getSearchedHealthServices(
			HealthServiceCriteriaSearchCriteria hscSearchCriteria);

	/**
	 * @param healthServiceCode
	 * @param qualifierCode
	 * @return
	 */
	public List<String> getDssHealthServiceCodeDesc(String healthServiceCode, String qualifierCode);

//	/**
//	 * @param aTownCode
//	 * @param aTownName
//	 * @return
//	 */
//	List<DssPostalCode> getCountyAndMunicipalityCodeAndNameByTownCodeAndName(BigDecimal aTownCode, String aTownName);

	/**
	 * @return All Unique DssPostalCodes Objects
	 */
	public List<DssPostalCodeMappedEntity> getAllUniqueDssPostalCodes();

}