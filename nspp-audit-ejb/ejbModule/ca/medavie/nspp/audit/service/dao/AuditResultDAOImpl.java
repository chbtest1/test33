package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.AuditResult;
import ca.medavie.nspp.audit.service.data.DssProvider2;
import ca.medavie.nspp.audit.service.data.DssProviderAudit;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.QueryConstants;

public class AuditResultDAOImpl implements AuditResultDAO {

	private static final Logger logger = LoggerFactory.getLogger(AuditResultDAOImpl.class);

	public EntityManager entityManager;

	public AuditResultDAOImpl(EntityManager em) {

		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.AuditResultDAO#findPractitioners(ca.medavie.nspp.audit.service.data.
	 * PractitionerSearchCriteria)
	 */
	@Override
	public List<DssProvider2> findPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria) {

		// Begin with the base query that is used for all types
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dp2.provider_number, dp2.provider_type, dp2.city, "
						+ "CASE WHEN dp2.organization_name IS NULL OR LENGTH(TRIM(dp2.organization_name)) IS NULL THEN "
						+ "dp2.last_name || ', ' || dp2.first_name || ' ' || substr(dp2.middle_name, 1, 1) ELSE dp2.organization_name END as provider_name "
						+ "FROM dss_provider_2 dp2, dss_provider_address dpa ");

		// Append dss_provider_specialty table if type is neither 'RX' or 'PX'
		if (aPractitionerSearchCriteria.getSubcategory() == null
				|| !aPractitionerSearchCriteria.getSubcategory().equals("RX")
				&& !aPractitionerSearchCriteria.getSubcategory().equals("PX")) {
			queryString.append(", dss_provider_specialty dps ");
		}

		// Append the beginning of the WHERE statement
		queryString
				.append("WHERE (dp2.provider_number = dpa.provider_number) AND (dp2.provider_type = dpa.provider_type) AND ((dpa.address_type = 1) AND "
						+ "(dpa.effective_from_date <= Trunc(SYSDATE) + .99999) AND (dpa.effective_to_date >= Trunc(SYSDATE) OR (dpa.effective_to_date is NULL)))");

		// Append the additionally required AND statements if we are including the dss_provider_specialty table
		if (aPractitionerSearchCriteria.getSubcategory() == null
				|| !aPractitionerSearchCriteria.getSubcategory().equals("RX")
				&& !aPractitionerSearchCriteria.getSubcategory().equals("PX")) {
			queryString
					.append("AND dp2.provider_type = dps.provider_type AND dp2.provider_number = dps.provider_number ");
		}

		// if practitioner id is set
		if (aPractitionerSearchCriteria.isPractitionerNumberSet()) {
			queryString.append("AND dp2. provider_number=?1 ");
		}

		// if practitioner type is set
		if (aPractitionerSearchCriteria.isSubcategorySet()) {
			queryString.append("AND dp2.provider_type=?2 ");
		}
		// if practitioner name is set
		if (aPractitionerSearchCriteria.isPractitionerNameSet()) {
			queryString.append("AND ( UPPER(dp2.last_name) LIKE ?3 " + "OR UPPER(dp2.first_name) LIKE ?3 "
					+ "OR UPPER(dp2.middle_name) LIKE ?3 ");

			// parse string into individual words
			String[] arr = aPractitionerSearchCriteria.getPractitionerName().split(" ");

			// The clause comes out like this for multiple names entered
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(dp2.last_name) LIKE ?3" + i + " or upper(dp2.middle_name) LIKE ?3" + i
							+ " or upper(dp2.first_name) LIKE ?3" + i + ") ");
				}

				queryString.append("))");
			}
		}

		// if city is set
		if (aPractitionerSearchCriteria.isCitySet()) {
			queryString.append("AND UPPER (dp2.city) LIKE CONCAT('%', CONCAT(?4,'%')) ");

		}
		// if specialty is set
		if (aPractitionerSearchCriteria.isSpecialtySet()) {
			queryString.append("AND dps.specialty_code=?5 ");
		}

		// if License number is set
		if (aPractitionerSearchCriteria.isLiscenseNumberSet()) {
			queryString.append("AND dp2.provider_license_number = ?6 ");
		}

		// Append the ORDER BY clause
		queryString.append("ORDER BY dp2.provider_number ASC, dp2.provider_type ASC");

		logger.debug("AuditResultDAOImpl--findPractitioners(), SQL query is " + queryString.toString());

		Query query = entityManager.createNativeQuery(queryString.toString(), "DssProvider2NativeQueryMapping");

		if (aPractitionerSearchCriteria.isPractitionerNumberSet()) {

			query.setParameter(1, aPractitionerSearchCriteria.getPractitionerNumber());
		}

		if (aPractitionerSearchCriteria.isSubcategorySet()) {

			query.setParameter(2, aPractitionerSearchCriteria.getSubcategory());
		}

		if (aPractitionerSearchCriteria.isPractitionerNameSet()) {
			query.setParameter(3, "%" + aPractitionerSearchCriteria.getPractitionerName().toUpperCase() + "%");

			// parse string into individual words
			String[] arr = aPractitionerSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("3" + i);
					// remove any commas
					query.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}

		if (aPractitionerSearchCriteria.isCitySet()) {

			// ignore the cases
			query.setParameter(4, aPractitionerSearchCriteria.getCity().toUpperCase());
		}

		if (aPractitionerSearchCriteria.isSpecialtySet()) {

			query.setParameter(5, aPractitionerSearchCriteria.getSpecialty());
		}

		if (aPractitionerSearchCriteria.isLiscenseNumberSet()) {

			query.setParameter(6, aPractitionerSearchCriteria.getLiscenseNumber());
		}

		query.setMaxResults(500);
		@SuppressWarnings("unchecked")
		List<DssProvider2> results = query.getResultList();

		logger.debug("AuditResultDAOImpl--findPractitioners(): " + results.size()
				+ " results found from {findPractitioners}");

		return results;

	}

	@Override
	public DssProviderAudit saveOrUpdateDssProviderAudit(DssProviderAudit aDssProviderAudit) {

		logger.debug("AuditResultDAOImpl -- saveOrUpdateDssProviderAudit(): Start persisting DssProviderAudit");
		entityManager.merge(aDssProviderAudit);
		return aDssProviderAudit;
	}

	@Override
	public List<DssProviderAudit> findAuditResults(Practitioner aSelectedPractitioner) {

		logger.debug("PeerGroupDAOImpl--getCountyCodes(): finding distinct county code from DssPostalCode");
		Query query = entityManager
				.createQuery(" select distinct dpa FROM DssProviderAudit dpa " +
						     "  where dpa.providerNumber=:providerNumber and dpa.providerType=:providerType ORDER BY dpa.auditDate DESC");
		query.setParameter("providerNumber", aSelectedPractitioner.getPractitionerNumber());
		query.setParameter("providerType", aSelectedPractitioner.getPractitionerType());

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssProviderAudit> results = query.getResultList();

		logger.debug("AuditResultDAOImpl--findAuditResults(): there are " + results.size() + "  AuditResults found ");

		return results;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.AuditResultDAO#findAuditResultByID(java.lang.Long)
	 */
	@Override
	public DssProviderAudit findAuditResultByID(Long anAuditId) {
		logger.debug("Start findAuditResultByID in AuditResultDAOImpl");
		DssProviderAudit aDssProviderAudit = entityManager.find(DssProviderAudit.class, anAuditId);
		logger.debug("End findAuditResultByID in AuditResultDAOImpl");
		return aDssProviderAudit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.AuditResultDAO#getNextAuditResultSequenceVal()
	 */
	@Override
	public Long getNextAuditResultSequenceVal() {
		Query query = entityManager.createNativeQuery("SELECT DSS_PROVIDER_AUDIT_SEQ.nextVal FROM DUAL");
		BigDecimal nextval = (BigDecimal) query.getSingleResult();
		// Safe to call longValue() since the sequence will never have any decimal places
		return nextval.longValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.AuditResultDAO#getNextAuditNoteSequenceVal()
	 */
	@Override
	public Long getNextAuditNoteSequenceVal() {
		Query query = entityManager.createNativeQuery("SELECT dss_provider_audit_notes_seq.nextVal FROM DUAL");
		BigDecimal nextval = (BigDecimal) query.getSingleResult();
		// Safe to call longValue() since the sequence will never have any decimal places
		return nextval.longValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.AuditResultDAO#getNextAuditAttachmentSequenceVal()
	 */
	@Override
	public Long getNextAuditAttachmentSequenceVal() {
		Query query = entityManager.createNativeQuery("SELECT dss_provider_audit_attach_seq.nextVal FROM DUAL");
		BigDecimal nextval = (BigDecimal) query.getSingleResult();
		// Safe to call longValue() since the sequence will never have any decimal places
		return nextval.longValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.AuditResultDAO#getHealthServiceCodes()
	 */
	@Override
	public List<String> getHealthServiceCodes() {
		Query query = entityManager.createNamedQuery(QueryConstants.DSS_HEALTH_SERVICE_CODE_HEALTH_SERIVCE_CODE_QUERY);
		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.AuditResultDAO#findAttachmentBinary(java.lang.Long)
	 */
	@Override
	public byte[] findAttachmentBinary(Long anAttachmentId) throws SQLException {
		Query query = entityManager
				.createNativeQuery(" select dpaa.file_binary from Dss_Provider_Audit_Attach dpaa where dpaa.attachment_Id = "
						+ anAttachmentId);

		// Execute the Query
		Blob blob = (Blob) query.getSingleResult();
		return blob.getBytes(1, (int) blob.length());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.AuditResultDAO#saveAttachmentBinary(byte[])
	 */
	@Override
	public void saveAttachmentBinary(Long anAttachmentId, byte[] anAttachmentBinary) {
		Query query = entityManager
				.createQuery(" update DssProviderAuditAttach dpaa set dpaa.fileBinary=:fileBinary where dpaa.attachmentId=:attachmentId");

		query.setParameter("fileBinary", anAttachmentBinary);
		query.setParameter("attachmentId", anAttachmentId);

		query.executeUpdate();

	}

	// JTRAX-56 08-FEB-18 BCAINNE
	@Override
	public String getReason(String healthServiceCode, String qualifer) {
		String desc = null;		
		String queryString;
		queryString = "SELECT hsc.description FROM dss_health_service_code hsc WHERE 1=1";
		if ((healthServiceCode != null) && (!healthServiceCode.isEmpty())) queryString += " AND hsc.health_service_code LIKE CONCAT('%', CONCAT(?1,'%'))";
		if ((qualifer != null) && (!qualifer.isEmpty())) queryString += " AND hsc.qualifier_code = ?2";
		Query query = entityManager.createNativeQuery(queryString);
		if ((healthServiceCode != null) && (!healthServiceCode.isEmpty())) query.setParameter(1, healthServiceCode);
		if ((qualifer != null) && (!qualifer.isEmpty())) query.setParameter(2, qualifer);
		try {
			@SuppressWarnings("unchecked")
			List<String> results = query.getResultList();
			if (results.size() > 0) {
				desc = results.get(0);
			} else {
				desc = "";
			}
		} catch (javax.persistence.NoResultException ne) {
			desc = "";
		}
		return desc;
	}

	@Override
	public void deleteAuditResult(AuditResult aSelectedAuditResult) {
		DssProviderAudit dssProviderAudit = findAuditResultByID(aSelectedAuditResult.getAuditId());
		entityManager.remove(dssProviderAudit);
	}
}
