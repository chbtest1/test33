package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.HealthServiceCodeSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceGroup;
import ca.medavie.nspp.audit.service.data.HealthServiceGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceGroupSearchCriteria.HealthServiceGroupSearchTypes;

/**
 * Implementation of the HealthServiceGroup DAO layer
 */
public class HealthServiceGroupDAOImpl implements HealthServiceGroupDAO {

	/** Handles all DB connections and transactions */
	private EntityManager entityManager;

	/** Used to format the date correctly for query(s) */
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(HealthServiceGroupDAOImpl.class);

	public HealthServiceGroupDAOImpl(EntityManager em) {
		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO#getSearchHealthServiceGroups(ca.medavie.nspp.audit.service
	 * .data.HealthServiceGroupSearchCriteria, int, int)
	 */
	@Override
	public List<Object[]> getSearchHealthServiceGroups(HealthServiceGroupSearchCriteria aSearchCriteria, int startRow,
			int maxRows) {

		logger.debug("getSearchHealthServiceGroups() : Begin method");

		// Create the query string
		StringBuilder queryString = constructHealthServiceGroupSearchSql(aSearchCriteria);

		// Append order by clause
		if (aSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, aSearchCriteria);
		} else {
			// Use default sorting
			queryString.append("ORDER BY dhsg.health_service_group_id ASC");
		}

		// Create the query
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Set pagination settings on query
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		// Apply arguments
		setHealthServiceGroupSearchQueryParameters(query, aSearchCriteria);

		// Execute query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		logger.debug("getSearchHealthServiceGroups() : Exit method");

		return results;
	}

	@Override
	public Integer getSearchHealthServiceGroupsCount(HealthServiceGroupSearchCriteria aSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(*) FROM (");

		// Appending the remainder of the query
		queryString.append(constructHealthServiceGroupSearchSql(aSearchCriteria));
		queryString.append(")");

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setHealthServiceGroupSearchQueryParameters(query, aSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the HSG search SQL
	 * 
	 * @param aSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructHealthServiceGroupSearchSql(HealthServiceGroupSearchCriteria aSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dhsg.health_service_group_id, dhsg.health_service_group_name, dhsg.modified_by, dhsg.last_modified ");

		// Construct the rest of the query based on the search type selected by the user.
		if (aSearchCriteria.getSearchTypeSelection().equals(HealthServiceGroupSearchTypes.HEALTH_SERVICE_GROUP_SEARCH)) {

			// Select from the appropriate/required tables for Health Service Group Search
			queryString.append("FROM dss_health_service_group dhsg WHERE dhsg.health_service_group_type = 'PROVI' ");

			// Append query arguments where needed
			if (aSearchCriteria.isHealthServiceGroupIdSet()) {
				queryString.append("AND dhsg.health_service_group_id = ?1 ");
			}
			if (aSearchCriteria.isHealthServiceGroupNameSet()) {
				queryString.append("AND UPPER(dhsg.health_service_group_name) like ?2 ");
			}

		} else if (aSearchCriteria.getSearchTypeSelection()
				.equals(HealthServiceGroupSearchTypes.HEALTH_SERVICES_SEARCH)) {

			// Select from the appropriate/required tables for Health Services search
			queryString.append("FROM dss_health_service dhs, dss_health_service_code dhsc, "
					+ "dss_health_service_group_xref dhsgx, dss_health_service_group dhsg "
					+ "WHERE dhsg.health_service_group_id = dhsgx.health_service_group_id "
					+ "and dhsgx.health_service_id = dhs.health_service_id "
					+ "and dhs.health_service_code = dhsc.health_service_code "
					+ "and dhs.qualifier_code = dhsc.qualifier_code AND dhsg.health_service_group_type = 'PROVI' ");

			// Append query arguments where needed
			if (aSearchCriteria.isHealthServiceProgramSet()) {
				queryString.append("AND trim(dhs.program_code) = ?3 ");
			}
			if (aSearchCriteria.isHealthServiceCodeSet()) {
				// JTRAX-54 07-FEB-18 BCAINNE
				queryString.append("AND dhs.health_service_code LIKE CONCAT('%', CONCAT(?4,'%')) ");
			}
			if (aSearchCriteria.isHealthServiceQualifierSet()) {
				queryString.append("AND dhs.qualifier_code = ?5 ");
			}
			if (aSearchCriteria.isDescriptionWordSet()) {
				queryString.append("AND UPPER(dhsc.description) LIKE ?6 ");
			}
		}
		return queryString;
	}

	/**
	 * Applies query arguments for HSG search/count
	 * 
	 * @param aQuery
	 * @param aSearchCriteria
	 */
	private void setHealthServiceGroupSearchQueryParameters(Query aQuery,
			HealthServiceGroupSearchCriteria aSearchCriteria) {

		// Parameters that are set if user is searching by Health Service Group
		if (aSearchCriteria.isHealthServiceGroupIdSet()) {
			aQuery.setParameter(1, aSearchCriteria.getHealthServiceGroupId());
		}
		if (aSearchCriteria.isHealthServiceGroupNameSet()) {
			aQuery.setParameter(2, "%" + aSearchCriteria.getHealthServiceGroupName().toUpperCase() + "%");
		}

		// Parameters that are set if the user is searching by Health Service
		if (aSearchCriteria.isHealthServiceProgramSet()) {
			aQuery.setParameter(3, aSearchCriteria.getHealthServiceProgram());
		}
		if (aSearchCriteria.isHealthServiceCodeSet()) {
			aQuery.setParameter(4, aSearchCriteria.getHealthServiceCode().toUpperCase());
		}
		if (aSearchCriteria.isHealthServiceQualifierSet()) {
			aQuery.setParameter(5, aSearchCriteria.getHealthServiceQualifier().toUpperCase());
		}
		if (aSearchCriteria.isDescriptionWordSet()) {
			aQuery.setParameter(6, "%" + aSearchCriteria.getDescriptionWord().toUpperCase() + "%");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO#getSearchHealthServiceCodes(ca.medavie.nspp.audit.service
	 * .data.HealthServiceCodeSearchCriteria)
	 */
	@Override
	public List<Object[]> getSearchHealthServiceCodes(HealthServiceCodeSearchCriteria aSearchCriteria) {

		// Build the query string
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dhs.health_service_id, dhs.program_code, dhs.health_service_code, dhs.qualifier_code, "
						+ "dhs.effective_from_date, dhsc.description, dhs.modifiers, dhs.implicit_modifiers, dhs.effective_to_date, "
						+ "dhs.category_code, dhs.unit_formula "
						+ "FROM dss_health_service dhs, dss_health_service_code dhsc WHERE dhs.health_service_code = dhsc.health_service_code "
						+ "AND dhs.qualifier_code = dhsc.qualifier_code " + "AND trim(dhs.program_code) = ?1 ");

		// Append arguments where required
		// JTRAX-54 08-MAR-2018 BCAINNE
		if (aSearchCriteria.isHealthServiceCodeSet()) {
			queryString.append("AND trim(dhs.health_service_code) LIKE CONCAT('%', CONCAT(?2,'%')) ");
		}
		if (aSearchCriteria.isQualifierSet()) {
			queryString.append("AND dhs.qualifier_code = ?3 ");
		}
		if (aSearchCriteria.isEffectiveDateSet()) {
			queryString.append("AND dhs.effective_from_date = to_date(?4, 'dd/mm/yyyy') "
					+ "AND (dhs.effective_to_date >= to_Date(?4, 'dd/mm/yyyy') OR dhs.effective_to_date IS NULL) ");
		}
		if (aSearchCriteria.isMsiFeeCodeSet()) {
			queryString.append("AND trim(dhs.msi_fee_code) = ?5 ");
		}
		if (aSearchCriteria.isHealthServiceDescSet()) {
			queryString.append("AND (dhsc.description LIKE ?6 OR dhsc.alternate_wording LIKE ?6) ");
		}
		if (aSearchCriteria.isModifiersSet()) {
			queryString.append("AND dhs.modifiers LIKE ?7 ");
		}
		if (aSearchCriteria.isImplicitModifiersSet()) {
			queryString.append("AND dhs.implicit_modifiers LIKE ?8 ");
		}

		// Sevice not in group selected. Date range needs to be added to query
		if (!aSearchCriteria.isServiceInGroup()) {
			queryString
					.append("AND (dhs.HEALTH_SERVICE_ID NOT IN (SELECT DISTINCT dhsgx.health_service_id FROM dss_health_service_group_xref dhsgx, "
							+ "dss_health_service_group dhsg WHERE dhsgx.effective_from_date <= Trunc(to_date(?9, 'dd/mm/yyyy')) + .99999 "
							+ "and dhsgx.effective_to_date >= Trunc(To_Date(?10, 'dd/mm/yyyy')) and "
							+ "dhsgx.health_service_group_id = dhsg.health_service_group_id and dhsg.health_service_group_type = 'PROVI'))");
		}

		// Create the query
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Set required parameters
		query.setParameter(1, aSearchCriteria.getProgram().replaceAll("\\s+", ""));
		if (aSearchCriteria.isHealthServiceCodeSet()) {
			query.setParameter(2, aSearchCriteria.getHealthServiceCode().toUpperCase().replaceAll("\\s+", ""));
		}
		if (aSearchCriteria.isQualifierSet()) {
			query.setParameter(3, aSearchCriteria.getQualifier());
			// query.setParameter(3, " ");
		}
		if (aSearchCriteria.isEffectiveDateSet()) {
			String fromDate = null;
			try {
				Date fd = new SimpleDateFormat("dd-MMM-yyyy").parse((aSearchCriteria.getEffectiveDateStr()));
				fromDate = sdf.format(fd);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			query.setParameter(4, fromDate);
		}
		if (aSearchCriteria.isMsiFeeCodeSet()) {
			query.setParameter(5, aSearchCriteria.getMsiFeeCode().replaceAll("\\s+", ""));
		}
		if (aSearchCriteria.isHealthServiceDescSet()) {
			query.setParameter(6, "%" + aSearchCriteria.getHealthServiceCodeDesc().toUpperCase() + "%");
		}
		if (aSearchCriteria.isModifiersSet()) {
			StringBuilder modifiersCriteria = new StringBuilder();

			for (Iterator<String> iter = aSearchCriteria.getModifiers().iterator(); iter.hasNext();) {
				modifiersCriteria.append(iter.next());
				if (iter.hasNext()) {
					modifiersCriteria.append(";");
				}
			}
			query.setParameter(7, "%" + modifiersCriteria.toString().toUpperCase() + "%");
		}
		if (aSearchCriteria.isImplicitModifiersSet()) {
			StringBuilder implicitModifiersCriteria = new StringBuilder();

			for (Iterator<String> iter = aSearchCriteria.getImplicitModifiers().iterator(); iter.hasNext();) {
				implicitModifiersCriteria.append(iter.next());
				if (iter.hasNext()) {
					implicitModifiersCriteria.append(";");
				}
			}
			query.setParameter(8, "%" + implicitModifiersCriteria.toString().toUpperCase() + "%");
		}
		if (!aSearchCriteria.isServiceInGroup()) {
			query.setParameter(9, sdf.format(aSearchCriteria.getGroupFilterFromDate()));
			// Check if an effective To was provided.. If not use 01/01/3000
			if (aSearchCriteria.getGroupFilterToDate() != null) {
				query.setParameter(10, sdf.format(aSearchCriteria.getGroupFilterToDate()));
			} else {
				query.setParameter(10, "01/01/3000");
			}
		}

		// Set limit of results to 500
		query.setMaxResults(500);

		// Execute the query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO#getHealthServiceCodes(java.lang.Long)
	 */
	@Override
	public List<Object[]> getHealthServiceCodes(Long aHealthServiceGroupId) {

		// Create query string
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dhsgx.health_service_id, dhsgx.effective_from_date, dhsgx.effective_to_date, "
						+ "dhsgx.modified_by, dhsgx.last_modified, rtrim(dhs.health_service_code), "
						+ "dhsc.description, dhs.modifiers, dhs.implicit_modifiers, dhs.program_code, "
						+ "dhs.category_code, dhs.unit_formula, dhs.qualifier_code FROM dss_health_service dhs, dss_health_service_group_xref dhsgx, "
						+ "dss_health_service_code dhsc WHERE dhs.health_service_id = dhsgx.health_service_id "
						+ "and dhs.health_service_code = dhsc.health_service_code and dhs.qualifier_code = dhsc.qualifier_code "
						+ "and dhsgx.health_service_group_id = ?1 and dhs.effective_to_date >= trunc(dhsc.effective_from_date) and "
						+ "dhs.effective_from_date <= trunc(dhsc.effective_to_date) + .99999 ORDER BY 7 ASC, dhs.modifiers ASC, dhs.implicit_modifiers ASC");

		Query query = entityManager.createNativeQuery(queryString.toString());
		// Set required parameters
		query.setParameter(1, aHealthServiceGroupId);

		// Execute query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO#saveHealthServiceGroup(ca.medavie.nspp.audit.service.
	 * data.HealthServiceGroup, java.util.List)
	 */
	@Override
	public void saveHealthServiceGroup(HealthServiceGroup aHealthServiceGroup, List<HealthServiceCode> aListOfHSCToAdd) {

		logger.debug("saveHealthServiceGroup() : Begin method");

		// ****************************************************** /
		// ***** Step 1 - update the base info of the Group ***** /
		// ****************************************************** /

		// Create query string
		StringBuilder groupQueryString = new StringBuilder("UPDATE dss_health_service_group dhsg "
				+ "SET dhsg.health_service_group_name = ?1, dhsg.modified_by = ?2, dhsg.last_modified = ?3 "
				+ "WHERE dhsg.health_service_group_id = ?4");

		// Create Group update query
		Query groupQuery = entityManager.createNativeQuery(groupQueryString.toString());
		// Apply arguments
		groupQuery.setParameter(1, aHealthServiceGroup.getHealthServiceGroupName());
		groupQuery.setParameter(2, aHealthServiceGroup.getModifiedBy());
		groupQuery.setParameter(3, aHealthServiceGroup.getLastModified());
		groupQuery.setParameter(4, aHealthServiceGroup.getHealthServiceGroupId());
		// Execute Group update
		groupQuery.executeUpdate();

		// ***************************************************************** /
		// ***** Step 2 - add the new HealthServiceCodes to xref table ***** /
		// ***************************************************************** /

		// Only perform add if there is codes marked for addition
		if (!aListOfHSCToAdd.isEmpty()) {

			// Create query string
			StringBuilder hscAddQueryString = new StringBuilder("INSERT INTO dss_health_service_group_xref "
					+ "(health_service_group_id, health_service_id, effective_from_date, "
					+ "effective_to_date, modified_by, last_modified) VALUES (?1, ?2, ?3, ?4, ?5, ?6)");

			Query hscAddQuery = entityManager.createNativeQuery(hscAddQueryString.toString());

			// Append the details of the new HSC values
			for (Iterator<HealthServiceCode> iter = aListOfHSCToAdd.iterator(); iter.hasNext();) {

				HealthServiceCode hsc = iter.next();
				// Apply argument/values for INSERT
				hscAddQuery.setParameter(1, aHealthServiceGroup.getHealthServiceGroupId());
				hscAddQuery.setParameter(2, hsc.getHealthServiceId());
				hscAddQuery.setParameter(3, hsc.getEffectiveFrom());
				hscAddQuery.setParameter(4, hsc.getEffectiveTo());
				hscAddQuery.setParameter(5, hsc.getModifiedBy());
				hscAddQuery.setParameter(6, hsc.getLastModified());

				// Execute HSC Add query
				hscAddQuery.executeUpdate();
			}
		}
		logger.debug("saveHealthServiceGroup() : End method");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO#saveNewHealthServiceGroup(ca.medavie.nspp.audit.service
	 * .data.HealthServiceGroup)
	 */
	@Override
	public void saveNewHealthServiceGroup(HealthServiceGroup aHealthServiceGroup) {

		logger.debug("saveNewHealthServiceGroup() : Begin method");

		// Build the query string for insert
		StringBuilder queryString = new StringBuilder(
				"INSERT INTO dss_health_service_group (health_service_group_id, health_service_group_name, health_service_group_type, modified_by, last_modified) "
						+ "VALUES (?1, ?2, ?3, ?4, ?5)");

		Query query = entityManager.createNativeQuery(queryString.toString());
		// Apply query arguments
		query.setParameter(1, aHealthServiceGroup.getHealthServiceGroupId());
		query.setParameter(2, aHealthServiceGroup.getHealthServiceGroupName());
		// Type always PROVI
		query.setParameter(3, "PROVI");
		query.setParameter(4, aHealthServiceGroup.getModifiedBy());
		query.setParameter(5, aHealthServiceGroup.getLastModified());

		// Execute the insert
		query.executeUpdate();
		logger.debug("saveNewHealthServiceGroup() : End method");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO#isHealthServiceGroupDuplicate(java.lang.Long)
	 */
	@Override
	public boolean isHealthServiceGroupDuplicate(Long aHealtherServiceGroupId) {

		StringBuilder duplicateCheckQueryString = new StringBuilder(
				"SELECT dhsg.health_service_group_id FROM dss_health_service_group dhsg "
						+ "WHERE dhsg.health_service_group_id = ?1");

		Query duplicateCheckQuery = entityManager.createNativeQuery(duplicateCheckQueryString.toString());
		duplicateCheckQuery.setParameter(1, aHealtherServiceGroupId);

		// True if other groups were found
		return !duplicateCheckQuery.getResultList().isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO#deleteHealthServiceCodesFromGroup(java.lang.Long,
	 * java.util.List)
	 */
	@Override
	public void deleteHealthServiceCodesFromGroup(Long aHealthServiceGroupId,
			List<HealthServiceCode> aListOfCodesForDelete) {

		// Create query string
		StringBuilder hscDeleteQueryString = new StringBuilder("DELETE FROM dss_health_service_group_xref WHERE ");

		// Append the details targeting the rows that need to be deleted.
		for (Iterator<HealthServiceCode> iter = aListOfCodesForDelete.iterator(); iter.hasNext();) {

			HealthServiceCode hsc = iter.next();

			hscDeleteQueryString.append("(health_service_group_id = '" + aHealthServiceGroupId + "' AND ");
			hscDeleteQueryString.append("health_service_id = " + hsc.getHealthServiceId() + " AND ");
			hscDeleteQueryString.append("effective_from_date = to_date('" + sdf.format(hsc.getEffectiveFrom())
					+ "', 'dd/mm/yyyy')) ");

			// Append OR if required
			if (iter.hasNext()) {
				// Not in last place of the list.. Append OR
				hscDeleteQueryString.append("OR ");
			}
		}

		// Create delete query
		Query hscDeleteQuery = entityManager.createNativeQuery(hscDeleteQueryString.toString());
		// Execute HSC delete query
		hscDeleteQuery.executeUpdate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO#getHealthServicePrograms()
	 */
	@Override
	public List<Object[]> getHealthServicePrograms() {

		logger.debug("getHealthServicePrograms() : Begin method");

		// Build the query string
		StringBuilder queryString = new StringBuilder("SELECT DISTINCT dp.program_code, dp.program_name "
				+ "FROM DSS_PROGRAM dp WHERE dp.program_code IN "
				+ "(SELECT x.program_code FROM dss_health_service x WHERE x.program_code = dp.program_code) "
				+ "ORDER BY dp.program_code ASC");

		Query query = entityManager.createNativeQuery(queryString.toString());

		// Execute the query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		logger.debug("getHealthServicePrograms() : Exit method");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO#getModifiers()
	 */
	@Override
	public List<Object[]> getModifiers() {

		logger.debug("getModifiers() : Begin method");

		// Build the query string
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dss_modifier_value.modifier_type || '=' || dss_modifier_value.modifier_value as modifiers, "
						+ "dss_modifier_value.modifier_type || dss_modifier_value.modifier_value as modifier_type_and_value "
						+ "FROM dss_modifier_type, dss_modifier_value "
						+ "WHERE (dss_modifier_type.modifier_type = dss_modifier_value.modifier_type) and ((dss_modifier_type.implicit_modifier_indicator = 'N') and "
						+ "(dss_modifier_value.effective_from_date <= trunc(SYSDATE) + .99999) and (dss_modifier_value.effective_to_date >= trunc(SYSDATE))) ORDER BY 1 ASC");

		Query query = entityManager.createNativeQuery(queryString.toString());

		// Execute the query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		logger.debug("getModifiers() : End method");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO#getImplicitModifiers()
	 */
	@Override
	public List<Object[]> getImplicitModifiers() {

		logger.debug("getImplicitModifiers() : Begin method");

		// Build the query String
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dss_modifier_value.modifier_type || '=' || dss_modifier_value.modifier_value as implicit_modifiers, "
						+ "dss_modifier_value.modifier_type || dss_modifier_value.modifier_value as modifier_type_and_value "
						+ "FROM dss_modifier_type, dss_modifier_value WHERE (dss_modifier_type.modifier_type = dss_modifier_value.modifier_type) "
						+ "and ((dss_modifier_type.implicit_modifier_indicator = 'Y') and "
						+ "(dss_modifier_value.effective_from_date <= trunc(SYSDATE) + .99999) and (dss_modifier_value.effective_to_date >= trunc(SYSDATE))) "
						+ "ORDER BY 1 ASC");

		Query query = entityManager.createNativeQuery(queryString.toString());

		// Execute the query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		logger.debug("getImplicitModifiers() : End method");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.HealthServiceGroupDAO#getServicesNotInGroup(java.util.Date)
	 */
	@Override
	public List<Object[]> getServicesNotInGroup(Date aYearEndDate) {

		logger.debug("getServicesNotInGroup() : Begin method");

		// Build the query string
		StringBuilder queryString = new StringBuilder(
				"SELECT dhs.health_service_id, dhs.program_code, dhs.health_service_code, dhs.modifiers, dhs.implicit_modifiers, dhs.qualifier_code, dhs.effective_from_date, dhs.effective_to_date, "
						+ "dhs.category_code, dhs.unit_formula FROM dss_health_service dhs WHERE dhs.effective_from_date <= Trunc(?1) + .99999 and dhs.effective_to_date >= Trunc(?1) "
						+ "and dhs.health_service_id not in (SELECT dhsgx.health_service_id FROM dss_health_service_group_xref dhsgx, dss_health_service_group dhsg "
						+ "WHERE dhsgx.effective_from_date <= Trunc(?1) + .99999 and dhsgx.effective_to_date >= Trunc(?1) and dhsgx.health_service_group_id = dhsg.health_service_group_id "
						+ "and dhsg.health_service_group_type = 'PROVI') ORDER BY dhs.program_code ASC");

		// Create query and apply the yearEndDate argument
		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aYearEndDate);

		// Execute the query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		logger.debug("getServicesNotInGroup() : End method");

		return results;
	}
}
