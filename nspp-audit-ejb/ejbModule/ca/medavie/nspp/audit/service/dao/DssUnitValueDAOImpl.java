package ca.medavie.nspp.audit.service.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.DssUnitValue;
import ca.medavie.nspp.audit.service.data.DssUnitValueBO;
import ca.medavie.nspp.audit.service.data.QueryConstants;

public class DssUnitValueDAOImpl extends QueryConstants implements DssUnitValueDAO {

	/** Handles all DB connections and transactions */
	private EntityManager entityManager;

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(DssUnitValueDAOImpl.class);

	public DssUnitValueDAOImpl(EntityManager em) {
		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DssUnitValueDAO#getDssUnitValues()
	 */
	@Override
	public List<DssUnitValue> getDssUnitValues() {
		logger.debug("DssUnitValueDAOImpl - getDssUnitValues(): Begin");

		Query query = entityManager.createNamedQuery(DSS_UNIT_VALUE_GET_QUERY);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssUnitValue> results = query.getResultList();

		logger.debug("DssUnitValueDAOImpl - getDssUnitValues(): there are " + results.size() + " items found ");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DssUnitValueDAO#findDssUnitValueById(java.lang.Long)
	 */
	@Override
	public DssUnitValue findDssUnitValueById(Long unitValueID) {
		logger.debug("DssUnitValueDAOImpl - findDssUnitValueById(): Begin");
		DssUnitValue aDssUnitValue = entityManager.find(DssUnitValue.class, unitValueID);
		logger.debug("DssUnitValueDAOImpl - findDssUnitValueById(): End");
		return aDssUnitValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssUnitValueDAO#saveOrUPdateDssUnitValue(ca.medavie.nspp.audit.service.data
	 * .DssUnitValue)
	 */
	@Override
	public DssUnitValue saveOrUPdateDssUnitValue(DssUnitValue aDssUnitValue) {
		logger.debug("DssUnitValueDAOImpl - saveOrUPdateDssUnitValue(): Begin");
		DssUnitValue entity = entityManager.merge(aDssUnitValue);
		logger.debug("DssUnitValueDAOImpl - saveOrUPdateDssUnitValue(): End");
		return entity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DssUnitValueDAO#deleteDssUnitValues(java.util.List)
	 */
	@Override
	public void deleteDssUnitValues(List<DssUnitValue> aListOfDssUnitValuesForDelete) {
		logger.debug("DssUnitValueDAOImpl - deleteDssUnitValues(): Begin");

		for (DssUnitValue unitValue : aListOfDssUnitValuesForDelete) {
			
			// Find the record and delete it
			DssUnitValue aDssUnitValue = entityManager.find(DssUnitValue.class, unitValue.getUvIdentifier());
			entityManager.remove(aDssUnitValue);
		}

		logger.debug("DssUnitValueDAOImpl - deleteDssUnitValues(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssUnitValueDAO#overlaps(ca.medavie.nspp.audit.service.data.DssUnitValueBO)
	 */
	@Override
	public Boolean overlaps(DssUnitValueBO aDssUnitValue) {
		String sqlString = "SELECT COUNT(*) FROM dss_unit_value uv WHERE uv.uv_type_code = ?1 AND uv.uv_level = ?2 AND ((TRUNC(?3) BETWEEN TRUNC(uv.effective_from_date) AND TRUNC(uv.effective_to_date)) "
				+ "OR (TRUNC(?4) BETWEEN TRUNC(uv.effective_from_date) AND TRUNC(uv.effective_to_date)) OR (TRUNC(?3) <= TRUNC(uv.effective_from_date) AND TRUNC(?4) >= TRUNC(uv.effective_to_date))) ";

		if (aDssUnitValue.getUnitValueID() != null) {
			sqlString += "AND uv.uv_identifier != ?5";
		}

		Query query = entityManager.createNativeQuery(sqlString, Long.class);
		query.setParameter(1, aDssUnitValue.getUnitValueTypeCode());
		query.setParameter(2, aDssUnitValue.getUnitValueLevel());
		query.setParameter(3, aDssUnitValue.getEffectiveFromDate());
		query.setParameter(4, aDssUnitValue.getEffectiveToDate());

		if (aDssUnitValue.getUnitValueID() != null) {
			query.setParameter(5, aDssUnitValue.getUnitValueID());
		}

		try {
			Long count = (Long) query.getSingleResult();
			if (count > 0)
				return true;
		} catch (javax.persistence.NoResultException e) {
			return false;
		}
		return false;
	}
}
