package ca.medavie.nspp.audit.service.dao;

import java.sql.SQLException;
import java.util.List;

import ca.medavie.nspp.audit.service.ServicePersistenceException;
import ca.medavie.nspp.audit.service.data.AuditResult;
import ca.medavie.nspp.audit.service.data.DssProvider2;
import ca.medavie.nspp.audit.service.data.DssProviderAudit;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;

public interface AuditResultDAO {

	/**
	 * Get practitioner by given search criteria
	 * 
	 * @param PractitionerSearchCriteria
	 *            - a given PractitionerSearchCriteria
	 * @return List - a list of result of DssProvider2 by a given PractitionerSearchCriteria
	 * */
	public List<DssProvider2> findPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria);

	/**
	 * Save or update DssProviderAudit
	 * 
	 * @param DssProviderAudit
	 *            - a given DssProviderAudit to save
	 * @throws ServicePersistenceException
	 * @return an updated DssProviderAudit
	 * 
	 * **/
	public DssProviderAudit saveOrUpdateDssProviderAudit(DssProviderAudit aDssProviderAudit);

	/**
	 * Find the audit results for a given practitioner
	 * 
	 * @param aSelectedPractitioner
	 *            -- a selected practitioner
	 * @return List of Audit result data objects
	 */
	public List<DssProviderAudit> findAuditResults(Practitioner aSelectedPractitioner);

	/**
	 * Find audit result by pk
	 * 
	 * @param anAuditId
	 * @return
	 */
	public DssProviderAudit findAuditResultByID(Long anAuditId);

	/**
	 * Gets the next value of the DSS_PROVIDER_AUDIT_SEQ sequence. Used when saving new notes under an Audit Result
	 * 
	 * @return the next sequence value from DSS_PROVIDER_AUDIT_SEQ
	 */
	public Long getNextAuditResultSequenceVal();

	/**
	 * Gets the next value of the dss_provider_audit_notes_seq sequence. Used when saving new notes under an Audit
	 * Result
	 * 
	 * @return the next sequence value from dss_provider_audit_notes_seq
	 */
	public Long getNextAuditNoteSequenceVal();

	/**
	 * Gets the next value of the dss_provider_audit_attach_seq sequence. Used when saving new attachments under an
	 * Audit
	 * 
	 * @return the next sequence value from dss_provider_audit_attach_seq
	 */
	public Long getNextAuditAttachmentSequenceVal();

	/**
	 * Gets a list of all valid Health Service codes
	 * 
	 * @return List of Health Service codes
	 */
	public List<String> getHealthServiceCodes();

	/**
	 * Gets the attachment file binary
	 * 
	 * @param anAttachmentId
	 * @return the attachment file binary
	 * @throws SQLException
	 */
	public byte[] findAttachmentBinary(Long anAttachmentId) throws SQLException;

	/**
	 * Saves the attachment file binary
	 * 
	 * @param anAttachmentBinary
	 */
	public void saveAttachmentBinary(Long anAttachmentId, byte[] anAttachmentBinary);

	public String getReason(String healthServiceCode, String qualifer);

	public void deleteAuditResult(AuditResult aSelectedAuditResult);
}
