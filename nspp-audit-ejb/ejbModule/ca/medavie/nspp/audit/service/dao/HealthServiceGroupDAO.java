package ca.medavie.nspp.audit.service.dao;

import java.util.Date;
import java.util.List;

import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.HealthServiceCodeSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceGroup;
import ca.medavie.nspp.audit.service.data.HealthServiceGroupSearchCriteria;

/**
 * Interface of the HealthServiceGroup DAO layer
 */
public interface HealthServiceGroupDAO {
	/**
	 * Loads all matching HealthServiceGroups using the provided Search Criteria
	 * 
	 * Data loaded from: dss_health_service_group, dss_health_service_code, dss_health_service_group
	 * 
	 * Query:
	 * 
	 * Service Search:
	 * 
	 * SELECT DISTINCT dhsg.health_service_group_id, dhsg.health_service_group_name, dhsg.modified_by,
	 * dhsg.last_modified FROM dss_health_service dhs, dss_health_service_code dhsc, dss_health_service_group_xref
	 * dhsgx, dss_health_service_group dhsg WHERE dhsg.health_service_group_id = dhsgx.health_service_group_id and
	 * dhsgx.health_service_id = dhs.health_service_id and dhs.health_service_code = dhsc.health_service_code and
	 * dhs.qualifier_code = dhsc.qualifier_code
	 * 
	 * Group Search:
	 * 
	 * SELECT DISTINCT dhsg.health_service_group_id, dhsg.health_service_group_name, dhsg.modified_by,
	 * dhsg.last_modified FROM dss_health_service_group dhsg
	 * 
	 * Data returned: (0)Health Service Group ID, (1)Health Service Group Name, (2)Modified By, (3)Last Modified
	 * 
	 * @param aSearchCriteria
	 *            - user specified search criteria
	 * @param startRow
	 * @param maxRows
	 * @return list of matching HealthServiceGroups
	 */
	public List<Object[]> getSearchHealthServiceGroups(HealthServiceGroupSearchCriteria aSearchCriteria, int startRow,
			int maxRows);

	/**
	 * Counts all rows that return by an HSG search
	 * 
	 * @param aSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchHealthServiceGroupsCount(HealthServiceGroupSearchCriteria aSearchCriteria);

	/**
	 * Loads all matching HealthServiceCodes using the provided Search Criteria
	 * 
	 * Data loaded from: dss_health_service, dss_health_service_code
	 * 
	 * Query:
	 * 
	 * SELECT dhs.health_service_id, dhs.program_code, dhs.health_service_code, dhs.qualifier_code,
	 * dhs.effective_from_date, dhsc.description, dhs.modifiers, dhs.implicit_modifiers, dhs.effective_to_date,
	 * dhs.category_code, dhs.unit_formula FROM dss_health_service dhs, dss_health_service_code dhsc WHERE
	 * dhs.health_service_code = dhsc.health_service_code and dhs.qualifier_code = dhsc.qualifier_code and
	 * dhs.program_code = ? and ((dhs.effective_to_date >= trunc(dhsc.effective_from_date)) and (dhs.effective_from_date
	 * <= trunc(dhsc.effective_to_date) + .99999));
	 * 
	 * If the service in group boolean is false the following is also included in the query:
	 * 
	 * AND (dhs.HEALTH_SERVICE_ID NOT IN (SELECT DISTINCT dhsgx.health_service_id FROM dss_health_service_group_xref
	 * dhsgx, dss_health_service_group dhsg WHERE dhsgx.effective_from_date <= Trunc(to_date(&fromDate, 'dd/mm/yyyy')) +
	 * .99999 and dhsgx.effective_to_date >= Trunc(To_Date(&toDate, 'dd/mm/yyyy')) and dhsgx.health_service_group_id =
	 * dhsg.health_service_group_id and dhsg.health_service_group_type = 'PROVI'))
	 * 
	 * Data returned: (0)Health Service ID, (1)Program Code, (2)Health Service Code, (3)Qualifier Code, (4)Effective
	 * From Date, (5)Description, (6)Modifiers, (7)Implicit Modifiers, (8)Effective To Date, (9)Category Code, (10)Unit
	 * Formula
	 * 
	 * @param aSearchCriteria
	 *            - user specified search criteria
	 * @return list of matching HealthServiceCodes
	 */
	public List<Object[]> getSearchHealthServiceCodes(HealthServiceCodeSearchCriteria aSearchCriteria);

	/**
	 * Loads all matching HealthServiceCode data.
	 * 
	 * Data loaded from: dss_health_service, dss_health_service_group_xref, dss_health_service_code
	 * 
	 * Query:
	 * 
	 * SELECT dhsgx.health_service_id, dhsgx.effective_from_date, dhsgx.effective_to_date, dhsgx.modified_by,
	 * dhsgx.last_modified, rtrim(dhs.health_service_code) || dhs.qualifier_code, dhsc.description, dhs.modifiers,
	 * dhs.implicit_modifiers, dhs.program_code, dhs.category_code, dhs.unit_formula FROM dss_health_service dhs,
	 * dss_health_service_group_xref dhsgx, dss_health_service_code dhsc WHERE dhs.health_service_id =
	 * dhsgx.health_service_id and dhs.health_service_code = dhsc.health_service_code and dhs.qualifier_code =
	 * dhsc.qualifier_code and dhsgx.health_service_group_id = ? and dhs.effective_to_date >=
	 * trunc(dhsc.effective_from_date) and dhs.effective_from_date <= trunc(dhsc.effective_to_date) + .99999 ORDER BY 7
	 * ASC, dhs.modifiers ASC, dhs.implicit_modifiers ASC
	 * 
	 * Data returned: (0)Health Service Id, (1)Effective From, (2)Effective To, (3)Modified By, (4)Last Modified,
	 * (5)Health Service Code/Qualifier Code, (6)Description, (7)Modifiers, (8)Implicit Modifiers, (9) Program Code,
	 * (10)Category Code, (11)Unit Formula, (12) Qualifier Code
	 * 
	 * @param aHealthServiceGroupId
	 * @return list of matching HealthServiceCode details
	 */
	public List<Object[]> getHealthServiceCodes(Long aHealthServiceGroupId);

	/**
	 * Saves changes made to the HealthServiceGroup.
	 * 
	 * @param aHealthServiceGroup
	 *            - the healthServiceGroup record to update
	 * @param aListOfHSCToAdd
	 *            - HSC records to be added to the group
	 */
	public void saveHealthServiceGroup(HealthServiceGroup aHealthServiceGroup, List<HealthServiceCode> aListOfHSCToAdd);

	/**
	 * Save a brand new HealthServiceGroup
	 * 
	 * @param aHealthServiceGroup
	 *            - new healthServiceGroup to save
	 */
	public void saveNewHealthServiceGroup(HealthServiceGroup aHealthServiceGroup);

	/**
	 * Validates that a new HealthServiceGroup is not using an ID that is already used by another group
	 * 
	 * @param aHealtherServiceGroupId
	 * @return true if ID is already used
	 */
	public boolean isHealthServiceGroupDuplicate(Long aHealtherServiceGroupId);

	/**
	 * Deletes the specified Codes from the specified group
	 * 
	 * @param aHealthServiceGroupId
	 * @param aListOfCodesForDelete
	 */
	public void deleteHealthServiceCodesFromGroup(Long aHealthServiceGroupId,
			List<HealthServiceCode> aListOfCodesForDelete);

	/**
	 * Loads all Health Service Programs found in the DB
	 * 
	 * Data loaded from: dss_program
	 * 
	 * Query:
	 * 
	 * SELECT DISTINCT dp.program_code, dp.program_name FROM DSS_PROGRAM dp WHERE dp.program_code IN (SELECT
	 * x.program_code FROM dss_health_service x WHERE x.program_code = dp.program_code) ORDER BY dp.program_code ASC
	 * 
	 * Data returned: (0)Program Code, (1) Program Name
	 * 
	 * @return list of program details
	 */
	public List<Object[]> getHealthServicePrograms();

	/**
	 * Load all Modifiers
	 * 
	 * Data loaded from: dss_modifier_type, dss_modifier_value
	 * 
	 * Query:
	 * 
	 * SELECT DISTINCT dss_modifier_value.modifier_type || '=' || dss_modifier_value.modifier_value as modifiers,
	 * dss_modifier_value.modifier_type || dss_modifier_value.modifier_value as modifier_type_and_value FROM
	 * dss_modifier_type, dss_modifier_value WHERE (dss_modifier_type.modifier_type = dss_modifier_value.modifier_type)
	 * and ((dss_modifier_type.implicit_modifier_indicator = 'N') and (dss_modifier_value.effective_from_date <=
	 * trunc(SYSDATE) + .99999) and (dss_modifier_value.effective_to_date >= trunc(SYSDATE))) ORDER BY 1 ASC
	 * 
	 * Data returned: (0)modifier, (1)modifierValue
	 * 
	 * @return list of all Modifiers
	 */
	public List<Object[]> getModifiers();

	/**
	 * Load all Implicit Modifiers
	 * 
	 * Data loaded from: dss_modifier_type, dss_modifier_value
	 * 
	 * Query:
	 * 
	 * SELECT DISTINCT dss_modifier_value.modifier_type || '=' || dss_modifier_value.modifier_value as
	 * implicit_modifiers, dss_modifier_value.modifier_type || dss_modifier_value.modifier_value as
	 * modifier_type_and_value FROM dss_modifier_type, dss_modifier_value WHERE (dss_modifier_type.modifier_type =
	 * dss_modifier_value.modifier_type) and ((dss_modifier_type.implicit_modifier_indicator = 'Y') and
	 * (dss_modifier_value.effective_from_date <= trunc(SYSDATE) + .99999) and (dss_modifier_value.effective_to_date >=
	 * trunc(SYSDATE))) ORDER BY 1 ASC
	 * 
	 * Data returned: (0)modifier, (1)modifierValue
	 * 
	 * @return list of all available ImplicitModifiers
	 */
	public List<Object[]> getImplicitModifiers();

	/**
	 * Loads all the data required for creating HealthServiceCodes
	 * 
	 * Data loaded from: dss_health_service
	 * 
	 * Query:
	 * 
	 * SELECT dhs.program_code, dhs.health_service_code, dhs.modifiers, dhs.implicit_modifiers, dhs.qualifier_code,
	 * dhs.effective_from_date, dhs.effective_to_date, dhs.category_code, dhs.unit_formula FROM dss_health_service dhs
	 * WHERE dhs.effective_from_date <= Trunc(?) + .99999 and dhs.effective_to_date >= Trunc(?) and
	 * dhs.health_service_id not in (SELECT dhsgx.health_service_id FROM dss_health_service dhsgx,
	 * dss_health_service_group dhsg WHERE dhsgx.effective_from_date <= Trunc(?) + .99999 and dhsgx.effective_to_date >=
	 * Trunc(?) and dhsgx.health_service_group_id = dhsg.health_service_group_id and dhsg.health_service_group_type =
	 * 'PROVI')
	 * 
	 * 
	 * Data returned: (0)Program Code, (1)HealthServiceCode, (2)Modifiers, (3)Implicit Modifiers, (4)Qualifier Code,
	 * (5)Effective From, (6)Effective To, (7)Category Code, (8)Unit Formula
	 * 
	 * @param aYearEndDate
	 *            - yearEndDate used to filter on services not in groups
	 * 
	 * @return list of HealthSerivceCodes currently not in a HealthServiceGroup
	 */
	public List<Object[]> getServicesNotInGroup(Date aYearEndDate);
}
