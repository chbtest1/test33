package ca.medavie.nspp.audit.service.dao;

import java.util.List;

import ca.medavie.nspp.audit.service.data.DssManualDeposit;
import ca.medavie.nspp.audit.service.data.ManualDeposit;
import ca.medavie.nspp.audit.service.data.ManualDepositSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.ProviderGroup;
import ca.medavie.nspp.audit.service.data.ProviderGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssBusinessArrangementMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssManualDepositsMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssProviderGroupMappedEntity;

/**
 * Interface of the Inquiry DAO layer
 */
public interface ManualDepositsDAO {
	/**
	 * Retrieve list of business arrangements.
	 * 
	 * @param practitioner
	 * @param group
	 * @return
	 */
	public List<DssBusinessArrangementMappedEntity> getBusinessArrangements(Practitioner practitioner,
			ProviderGroup group);

	/**
	 * Retrieve list of business arrangements.
	 * 
	 * @param manualDeposit
	 * @return
	 */
	public List<DssBusinessArrangementMappedEntity> getBusinessArrangements(ManualDeposit manualDeposit);

	/**
	 * Get all manual deposits limited by search criteria (provider or group).
	 * 
	 * @param aManualDepositSearchCriteria
	 * @param aPractitionerSearch
	 * @param startRow
	 * @param maxRows
	 * @return matching DssManualDepositsMappedEntity
	 */
	public List<DssManualDepositsMappedEntity> getSearchedManualDeposits(
			ManualDepositSearchCriteria aManualDepositSearchCriteria, boolean aPractitionerSearch, int startRow,
			int maxRows);

	/**
	 * Counts all rows that return by an ManualDeposits search
	 * 
	 * @param aManualDepositSearchCriteria
	 * @param aPractitionerSearch
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedAuditCriteriaCount(ManualDepositSearchCriteria aManualDepositSearchCriteria,
			boolean aPractitionerSearch);

	/**
	 * Get manual deposit details for manual deposit given.
	 * 
	 * @param aManualDeposit
	 * @return
	 */
	public DssManualDeposit getManualDepositDetails(long depositNumber);

	/**
	 * Save or update manual deposit.
	 * 
	 * @param aManualDeposit
	 * @return
	 */
	public void saveOrUpdateManualDeposit(DssManualDeposit aManualDeposit);

	/**
	 * Delete manual deposit.
	 * 
	 * @param depositNumber
	 */
	public void deleteManualDeposit(long depositNumber);

	/**
	 * Retrieves next sequence number for deposit number.
	 * 
	 * @return
	 */
	public Long getNextValueFromDssDepositNumberSeq();

	/**
	 * Checks that the practitioner info in given manual deposit is valid for a practitioner in the database. Used when
	 * the payor is a group and the practitioner info is optional.
	 * 
	 * @param selectedManualDeposit
	 * @return
	 */
	public boolean verifyPractitioner(ManualDeposit selectedManualDeposit);

	/**
	 * Used for searching for a practitioner when creating a manual deposit.
	 * 
	 * @param aPractitionerSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return
	 */
	List<Object[]> getManualDepositSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria,
			int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Practitioner search
	 * 
	 * @param practitionerSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getManualDepositSearchedPractitionersCount(PractitionerSearchCriteria practitionerSearchCriteria);

	/**
	 * Get all manual deposits limited by search criteria (provider or group).
	 * 
	 * @param aManualDepositSearchCriteria
	 * @return
	 */
	public List<DssProviderGroupMappedEntity> getSearchedProviderGroups(
			ProviderGroupSearchCriteria aProviderGroupSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Groups search
	 * 
	 * @param aProviderGroupSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedProviderGroupsCount(ProviderGroupSearchCriteria aProviderGroupSearchCriteria);
}
