package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.Locum;
import ca.medavie.nspp.audit.service.data.LocumSearchCriteria;

/**
 * Implementation of the Locum DAO layer
 */
public class LocumDAOImpl implements LocumDAO {

	/** Handles all DB connections and transactions */
	private EntityManager entityManager;

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(LocumDAOImpl.class);

	public LocumDAOImpl(EntityManager em) {
		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.LocumDAO#getSearchedLocums(ca.medavie.nspp.audit.service.data.LocumSearchCriteria
	 * , int, int)
	 */
	@Override
	public List<Object[]> getSearchedLocums(LocumSearchCriteria aLocumSearchCriteria, int startRow, int maxRows) {

		logger.debug("getSearchedLocums() : Begin");

		// Create the query string
		StringBuilder locumSearchQueryString = constructLocumSqlStatement(aLocumSearchCriteria);

		// Append order by SQL if needed
		if (aLocumSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(locumSearchQueryString, aLocumSearchCriteria);
		}

		Query locumSearchQuery = entityManager.createNativeQuery(locumSearchQueryString.toString());

		// Apply pagination arguments
		locumSearchQuery.setFirstResult(startRow);
		locumSearchQuery.setMaxResults(maxRows);

		// Apply query arguments
		setLocumSearchQueryParameters(locumSearchQuery, aLocumSearchCriteria);

		// Execute the query
		@SuppressWarnings("unchecked")
		List<Object[]> result = locumSearchQuery.getResultList();

		logger.debug("getSearchedLocums() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.LocumDAO#getSearchedLocumsCount(ca.medavie.nspp.audit.service.data.
	 * LocumSearchCriteria)
	 */
	@Override
	public Integer getSearchedLocumsCount(LocumSearchCriteria aLocumSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(*) FROM (");
		queryString.append(constructLocumSqlStatement(aLocumSearchCriteria));
		queryString.append(")");

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setLocumSearchQueryParameters(query, aLocumSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the Locum search SQL
	 * 
	 * @param aLocumSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructLocumSqlStatement(LocumSearchCriteria aLocumSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT p.provider_number, p.provider_type, pa.city, p.birth_date, "
						+ "CASE WHEN p.organization_name IS NULL OR LENGTH(TRIM(p.organization_name)) IS NULL THEN p.last_name || ', ' || p.first_name || ' ' || substr(p.middle_name, 1, 1) ELSE p.organization_name END as provider_name, "
						+ "nvl(locum_a.host_provider_number / locum_a.host_provider_number, 0), "
						+ "nvl(locum_b.visiting_provider_number / locum_b.visiting_provider_number, 0),"
						+ "p.last_name FROM dss_provider_2 p LEFT OUTER JOIN dss_provider_specialty ps ON p.provider_type = ps.provider_type AND p.provider_number = ps.provider_number "
						+ "LEFT OUTER JOIN dss_provider_locum locum_a ON p.provider_number = locum_a.host_provider_number AND p.provider_type = locum_a.host_provider_type "
						+ "LEFT OUTER JOIN dss_provider_locum locum_b ON p.provider_number = locum_b.visiting_provider_number AND p.provider_type = locum_b.visiting_provider_type, "
						+ "dss_provider_address pa WHERE (p.provider_number = pa.provider_number) and (p.provider_type = pa.provider_type) and ((pa.address_type = 1) and "
						+ "(pa.effective_from_date <= Trunc(SYSDATE) + .99999) and (pa.effective_to_date >= Trunc(SYSDATE) or (pa.effective_to_date is NULL))) ");

		// Append SQL for provided search criteria
		if (aLocumSearchCriteria.isPractitionerNumberSet()) {
			queryString.append("and p.provider_number = ?1 ");
		}
		if (aLocumSearchCriteria.isPractitionerTypeSet()) {
			queryString.append("and p.provider_type = ?2 ");
		}
		if (aLocumSearchCriteria.isPractitionerNameSet()) {
			queryString.append("and (upper(p.first_name) LIKE ?3 or upper(p.last_name) LIKE ?3 ");
			queryString.append("or upper(p.organization_name) LIKE ?3 ");

			// parse string into individual words
			String[] arr = aLocumSearchCriteria.getPractitionerName().split(" ");

			// The clause comes out like this for multiple names entered
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(p.last_name) LIKE ?3" + i + " or upper(p.middle_name) LIKE ?3" + i
							+ " or upper(p.first_name) LIKE ?3" + i + ") ");
				}

				queryString.append("))");
			}
		}
		if (aLocumSearchCriteria.isCitySet()) {
			queryString.append("and pa.city LIKE ?4 ");
		}
		if (aLocumSearchCriteria.isSpecialtySet()) {
			queryString.append("and ps.specialty_code = ?5 ");
		}
		if (aLocumSearchCriteria.isLicenseNumberSet()) {
			queryString.append("and p.provider_license_number = ?6 ");
		}
		return queryString;
	}

	/**
	 * Applies query arguments for Locum search/count
	 * 
	 * @param aQuery
	 * @param aLocumSearchCriteria
	 */
	private void setLocumSearchQueryParameters(Query aQuery, LocumSearchCriteria aLocumSearchCriteria) {

		// Append the provided search criteria
		if (aLocumSearchCriteria.isPractitionerNumberSet()) {
			aQuery.setParameter(1, aLocumSearchCriteria.getPractitionerNumber());
		}
		if (aLocumSearchCriteria.isPractitionerTypeSet()) {
			aQuery.setParameter(2, aLocumSearchCriteria.getPractitionerType());
		}
		if (aLocumSearchCriteria.isPractitionerNameSet()) {
			aQuery.setParameter(3, "%" + aLocumSearchCriteria.getPractitionerName().toUpperCase() + "%");

			// parse string into individual words
			String[] arr = aLocumSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("3" + i);
					// remove any commas
					aQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}
		if (aLocumSearchCriteria.isCitySet()) {
			aQuery.setParameter(4, "%" + aLocumSearchCriteria.getCity().toUpperCase() + "%");
		}
		if (aLocumSearchCriteria.isSpecialtySet()) {
			aQuery.setParameter(5, aLocumSearchCriteria.getSpecialty());
		}
		if (aLocumSearchCriteria.isLicenseNumberSet()) {
			aQuery.setParameter(6, aLocumSearchCriteria.getLicenseNumber());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.LocumDAO#getSearchedLocumPractitioners(ca.medavie.nspp.audit.service.data.
	 * LocumSearchCriteria)
	 */
	@Override
	public List<Object[]> getSearchedLocumPractitioners(LocumSearchCriteria aLocumSearchCriteria) {
		// Create the query string
		StringBuilder locumPractitionerQueryString = new StringBuilder(
				"SELECT DISTINCT p.provider_number, p.provider_type, p.city, "
						+ "CASE WHEN p.organization_name IS NULL OR LENGTH(TRIM(p.organization_name)) IS NULL THEN "
						+ "p.last_name || ', ' || p.first_name || ' ' || substr(p.middle_name, 1, 1) ELSE p.organization_name END as provider_name "
						+ "FROM dss_provider_2 p ");

		// Conditionally apply the left outer join on dss_provider_specialty if user searched with specialty specified
		if (aLocumSearchCriteria.isSpecialtySet()) {
			locumPractitionerQueryString
					.append("LEFT OUTER JOIN  dss_provider_specialty dps ON  p.provider_type = dps.provider_type AND p.provider_number = dps.provider_number "
							+ "AND dps.specialty_code = ?1 ");
		}

		// Append the WHERE clause
		locumPractitionerQueryString.append("WHERE ");

		// Apply required arguments
		if (aLocumSearchCriteria.isPractitionerNumberSet()) {
			locumPractitionerQueryString.append("p.provider_number = ?2 AND ");
		}
		if (aLocumSearchCriteria.isPractitionerTypeSet()) {
			locumPractitionerQueryString.append("p.provider_type = ?3 AND ");
		}
		if (aLocumSearchCriteria.isPractitionerNameSet()) {
			locumPractitionerQueryString
					.append("(upper(p.organization_name) LIKE ?4 OR upper(p.first_name) LIKE ?4 OR ");
			locumPractitionerQueryString.append("upper(p.middle_name) LIKE ?4 OR upper(p.last_name) LIKE ?4 ");

			// parse string into individual words
			String[] arr = aLocumSearchCriteria.getPractitionerName().split(" ");

			// The clause comes out like this for multiple names entered
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				locumPractitionerQueryString.append(") AND ");
			} else {
				locumPractitionerQueryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						locumPractitionerQueryString.append(" and ");
					}

					locumPractitionerQueryString.append("(upper(p.last_name) LIKE ?4" + i
							+ " or upper(p.middle_name) LIKE ?4" + i + " or upper(p.first_name) LIKE ?4" + i + ") ");
				}

				locumPractitionerQueryString.append(")) AND ");
			}

		}
		if (aLocumSearchCriteria.isCitySet()) {
			locumPractitionerQueryString.append("p.city = ?5 AND ");
		}
		if (aLocumSearchCriteria.isLicenseNumberSet()) {
			locumPractitionerQueryString.append("p.provider_license_number = ?6 ");
		}

		// Check if the query string ends with WHERE or AND
		String query_end_with_and = locumPractitionerQueryString.substring(locumPractitionerQueryString.length() - 4,
				locumPractitionerQueryString.length() - 1);
		String query_end_with_where = locumPractitionerQueryString.substring(locumPractitionerQueryString.length() - 6,
				locumPractitionerQueryString.length() - 1);
		String stringQuery = null;

		if (query_end_with_and.trim().equals("AND")) {
			// If query string ends with "AND" remove it
			stringQuery = locumPractitionerQueryString.substring(0, locumPractitionerQueryString.length() - 4);
		} else if (query_end_with_where.trim().equals("WHERE")) {
			// If query string ends with "WHERE" remove it
			stringQuery = locumPractitionerQueryString.substring(0, locumPractitionerQueryString.length() - 6);
		} else {
			// Does not end with either AND or WHERE just use the constructed query
			stringQuery = locumPractitionerQueryString.toString();
		}

		// Finally append the order by portion to complete the query
		stringQuery = stringQuery + "  ORDER BY provider_name ASC";

		// Create query and apply required arguments.
		Query locumPractitionerQuery = entityManager.createNativeQuery(stringQuery);
		if (aLocumSearchCriteria.isSpecialtySet()) {
			locumPractitionerQuery.setParameter(1, aLocumSearchCriteria.getSpecialty());
		}
		if (aLocumSearchCriteria.isPractitionerNumberSet()) {
			locumPractitionerQuery.setParameter(2, aLocumSearchCriteria.getPractitionerNumber());
		}
		if (aLocumSearchCriteria.isPractitionerTypeSet()) {
			locumPractitionerQuery.setParameter(3, aLocumSearchCriteria.getPractitionerType());
		}
		if (aLocumSearchCriteria.isPractitionerNameSet()) {
			locumPractitionerQuery
					.setParameter(4, "%" + aLocumSearchCriteria.getPractitionerName().toUpperCase() + "%");

			// parse string into individual words
			String[] arr = aLocumSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("4" + i);
					// remove any commas
					locumPractitionerQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "")
							+ "%");
				}
			}
		}
		if (aLocumSearchCriteria.isCitySet()) {
			locumPractitionerQuery.setParameter(5, aLocumSearchCriteria.getCity().toUpperCase());
		}
		if (aLocumSearchCriteria.isLicenseNumberSet()) {
			locumPractitionerQuery.setParameter(6, aLocumSearchCriteria.getLicenseNumber());
		}

		// Execute query
		@SuppressWarnings("unchecked")
		List<Object[]> result = locumPractitionerQuery.getResultList();

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.LocumDAO#getLocumDetails(ca.medavie.nspp.audit.service.data.Locum)
	 */
	@Override
	public List<Object[]> getLocumDetails(Locum aLocum) {

		logger.debug("getLocumDetails() : Begin");

		// Query String (Can use single variable)
		StringBuilder locumQueryString = new StringBuilder(
				"SELECT locum.host_provider_number, locum.host_provider_type, locum.visiting_provider_number, locum.visiting_provider_type, locum.effective_from_date, locum.effective_to_date, "
						+ "CASE WHEN provider.organization_name IS NULL THEN provider.last_name || ', ' || provider.first_name || ' ' || substr(provider.middle_name, 1, 1) ELSE provider.organization_name END as provider_name, "
						+ "locum.modified_by, locum.last_modified FROM dss_provider_locum locum, dss_provider_2 provider ");

		// Determine the Locum record type and query for the required results
		if (aLocum.isLocumHost()) {

			// Complete query string for HOST record
			locumQueryString
					.append("WHERE (locum.visiting_provider_number = provider.provider_number) and (locum.visiting_provider_type = provider.provider_type) "
							+ "and ((locum.host_provider_number = ?1) and (locum.host_provider_type = ?2)) ORDER BY locum.effective_from_date DESC");
		} else {

			// Complete query string for VISITOR record
			locumQueryString
					.append("WHERE (locum.host_provider_number = provider.provider_number) and (locum.host_provider_type = provider.provider_type) "
							+ "and ((locum.visiting_provider_number = ?1) and (locum.visiting_provider_type = ?2)) ORDER BY locum.effective_from_date DESC");
		}

		Query locumQuery = entityManager.createNativeQuery(locumQueryString.toString());
		// Set required query parameters
		locumQuery.setParameter(1, aLocum.getPractitionerNumber());
		locumQuery.setParameter(2, aLocum.getPractitionerType());

		// Execute the query
		@SuppressWarnings("unchecked")
		List<Object[]> detailsResult = locumQuery.getResultList();

		logger.debug("getLocumDetails() : End");
		return detailsResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.LocumDAO#saveLocum(ca.medavie.nspp.audit.service.data.Locum)
	 */
	@Override
	public void saveLocum(Locum aLocum) {
		// Save is completed over 3 steps. Run update statement, delete statement, insert statement

		// Step 1: Apply Updates to Locum records
		StringBuilder locumUpdateQueryString = new StringBuilder(
				"UPDATE dss_provider_locum dpl SET effective_from_date = ?1, effective_to_date = ?2, modified_by = ?3, last_modified = ?4 WHERE dpl.host_provider_number = ?5 "
						+ "AND dpl.host_Provider_Type = ?6 AND dpl.visiting_provider_number = ?7 AND dpl.visiting_provider_type = ?8 AND dpl.effective_from_date = ?9");

		// Create query and execute for all required Updated records
		Query locumUpdateQuery = entityManager.createNativeQuery(locumUpdateQueryString.toString());
		// Update the Parent locum object prior to children
		locumUpdateQuery.setParameter(1, aLocum.getEffectiveFrom());
		locumUpdateQuery.setParameter(2, aLocum.getEffectiveTo());
		locumUpdateQuery.setParameter(3, aLocum.getModifiedBy());
		locumUpdateQuery.setParameter(4, aLocum.getLastModified());
		locumUpdateQuery.setParameter(5, aLocum.getHostPractitionerNumber());
		locumUpdateQuery.setParameter(6, aLocum.getHostPractitionerType());
		locumUpdateQuery.setParameter(7, aLocum.getVisitingPractitionerNumber());
		locumUpdateQuery.setParameter(8, aLocum.getVisitingPractitionerType());
		locumUpdateQuery.setParameter(9, aLocum.getEffectiveFrom());

		// Execute update for Parent record
		locumUpdateQuery.executeUpdate();

		for (Locum l : aLocum.getLocums()) {
			// Only update records that are not marked for Add
			if (!l.isMarkedForAdd()) {
				// New values for Effective From/To
				locumUpdateQuery.setParameter(1, l.getEffectiveFrom());
				locumUpdateQuery.setParameter(2, l.getEffectiveTo());
				locumUpdateQuery.setParameter(3, l.getModifiedBy());
				locumUpdateQuery.setParameter(4, l.getLastModified());
				locumUpdateQuery.setParameter(5, l.getHostPractitionerNumber());
				locumUpdateQuery.setParameter(6, l.getHostPractitionerType());
				locumUpdateQuery.setParameter(7, l.getVisitingPractitionerNumber());
				locumUpdateQuery.setParameter(8, l.getVisitingPractitionerType());
				locumUpdateQuery.setParameter(9, l.getOriginalEffectiveFrom());

				// Execute query
				locumUpdateQuery.executeUpdate();
			}
		}

		// Step 2: Insert newly added Locum records
		StringBuilder locumInsertQueryString = new StringBuilder(
				"INSERT INTO dss_provider_locum (host_provider_number, host_provider_type, visiting_provider_number, visiting_provider_type, "
						+ "effective_from_date, effective_to_date, modified_by, last_modified) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)");

		// Create query and execute for all new records
		Query locumInsertQuery = entityManager.createNativeQuery(locumInsertQueryString.toString());
		for (Locum l : aLocum.getLocums()) {
			// Only add the records marked for addition
			if (l.isMarkedForAdd()) {

				locumInsertQuery.setParameter(1, l.getHostPractitionerNumber());
				locumInsertQuery.setParameter(2, l.getHostPractitionerType());
				locumInsertQuery.setParameter(3, l.getVisitingPractitionerNumber());
				locumInsertQuery.setParameter(4, l.getVisitingPractitionerType());
				locumInsertQuery.setParameter(5, l.getEffectiveFrom());
				locumInsertQuery.setParameter(6, l.getEffectiveTo());
				locumInsertQuery.setParameter(7, l.getModifiedBy());
				locumInsertQuery.setParameter(8, l.getLastModified());

				// Execute query
				locumInsertQuery.executeUpdate();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.LocumDAO#deleteLocums(java.util.List)
	 */
	@Override
	public void deleteLocums(List<Locum> aListOfLocumsForDelete) {
		// Create delete query string
		StringBuilder locumDeleteQueryString = new StringBuilder(
				"DELETE FROM dss_provider_locum dpl WHERE dpl.host_provider_number = ?1 AND dpl.host_Provider_Type = ?2 AND dpl.visiting_provider_number = ?3 "
						+ "AND dpl.visiting_provider_type = ?4 AND dpl.effective_from_date = ?5");

		// Create query and execute for all deleted records
		Query locumDeleteQuery = entityManager.createNativeQuery(locumDeleteQueryString.toString());
		for (Locum l : aListOfLocumsForDelete) {

			// Where clause values
			locumDeleteQuery.setParameter(1, l.getHostPractitionerNumber());
			locumDeleteQuery.setParameter(2, l.getHostPractitionerType());
			locumDeleteQuery.setParameter(3, l.getVisitingPractitionerNumber());
			locumDeleteQuery.setParameter(4, l.getVisitingPractitionerType());
			locumDeleteQuery.setParameter(5, l.getEffectiveFrom());

			// Execute query
			locumDeleteQuery.executeUpdate();
		}
	}
}
