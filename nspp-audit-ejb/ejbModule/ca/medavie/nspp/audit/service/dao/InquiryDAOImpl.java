package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.AgeDistribution;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.DssProviderType;
import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.PaymentDetails;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria.PractitionerTypes;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPeerGroupHSCTotalsMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssPractitionerHSCTotalsMappedEntity;

/**
 * Implementation of the Inquiry DAO layer
 */
public class InquiryDAOImpl implements InquiryDAO {

	/** Handles all DB connections and transactions */
	private EntityManager entityManager;

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(InquiryDAOImpl.class);

	public InquiryDAOImpl(EntityManager em) {
		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.InquiryDAO#getDssProviderTypes()
	 */
	@Override
	public List<DssProviderType> getDssProviderTypes() {

		logger.debug("InquiryDAO getDssProviderTypes() : Begin");

		Query query = entityManager.createQuery(" select distinct dpt FROM  DssProviderType dpt"
				+ " ORDER BY dpt.providerType," + " dpt.providerTypeDescription ASC");

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssProviderType> results = query.getResultList();

		logger.debug("InquiryDAOImpl--getDssProviderTypes(): there are " + results.size()
				+ " Provider Types drop down item found from {getDssProviderTypes}");

		logger.debug("InquiryDAO getDssProviderTypes() : End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.InquiryDAO#getSearchedPractitioners(ca.medavie.nspp.audit.service.data.
	 * PractitionerSearchCriteria, int, int)
	 */
	@Override
	public List<Object[]> getSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria,
			int startRow, int maxRows) {

		logger.debug("InquiryDAO getSearchedPractitioners() : Begin");

		// Create query string
		StringBuilder queryString = constructPractitionerSearchSql(aPractitionerSearchCriteria);

		// Append order by
		if (aPractitionerSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, aPractitionerSearchCriteria);
		} else {
			// Append default sorting
			queryString.append("ORDER BY dmps.provider_peer_group_id ASC, dmps.year_end_date DESC");
		}

		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply pagination arguments
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		// Apply query parameters
		setPractitionerSearchQueryParameters(query, aPractitionerSearchCriteria);

		// Execute query and grab results
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		logger.debug("InquiryDAO getSearchedPractitioners() : End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.InquiryDAO#getSearchedPractitionersCount(ca.medavie.nspp.audit.service.data
	 * .PractitionerSearchCriteria)
	 */
	@Override
	public Integer getSearchedPractitionersCount(PractitionerSearchCriteria aPractitionerSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(*) FROM (");
		queryString.append(constructPractitionerSearchSql(aPractitionerSearchCriteria));
		queryString.append(")");

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setPractitionerSearchQueryParameters(query, aPractitionerSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the Practitioner search SQL
	 * 
	 * @param aPractitionerSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructPractitionerSearchSql(PractitionerSearchCriteria aPractitionerSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dmps.provider_peer_group_id, dppg.provider_peer_group_name, dmps.provider_number, dmps.provider_type, "
						+ "dp2.organization_name || dp2.last_name || ', ' || dp2.first_name || ' ' || dp2.middle_name as provider_name, dmps.year_end_date, "
						+ "dmps.period_id, dmps.shadow_billing_indicator, dp2.birth_date, dp2.gender, dp2.address_line_1, dp2.address_line_2, dp2.city, "
						+ "dp2.province_code, dp2.country, dp2.postal_code, dmps.number_of_patients, dmps.number_of_se, dmps.total_units, dmps.total_amount_paid, "
						+ "Round(decode(dmps.number_of_patients, 0, 0,  dmps.number_of_se / dmps.number_of_patients), 2) as services_per_patient, "
						+ "Round(decode(dmps.number_of_patients, 0, 0, dmps.total_amount_paid / dmps.number_of_patients), 2) as amount_paid_per_patient, dp2.last_name "
						+ "FROM dss_med_prov_summary dmps, dss_provider_peer_group dppg, dss_provider_2 dp2 "
						+ "LEFT OUTER JOIN dss_specialty s ON s.provider_number = dp2.provider_number AND s.provider_type = dp2.provider_type "
						+ "WHERE dmps.provider_peer_group_id = dppg.provider_peer_group_id "
						+ "and dmps.provider_number = dp2.provider_number and dmps.provider_type = dp2.provider_type "
						+ "and dppg.year_end_date = dmps.year_end_date and dppg.PROVIDER_PEER_GROUP_TYPE = 'PROVI' ");

		// Append Shadow Billing indicator statement (Profile Type boolean)
		if (aPractitionerSearchCriteria.getProfileType() != null) {
			if (aPractitionerSearchCriteria.getProfileType().equals(PractitionerTypes.FEE_FOR_SERVICE)) {
				queryString.append("and dmps.shadow_billing_indicator = 'N' ");
			} else {
				queryString.append("and dmps.shadow_billing_indicator = 'Y' ");
			}
		}

		// Append search criteria AND statements where required.
		if (aPractitionerSearchCriteria.isPractitionerNumberSet()) {
			queryString.append("and dmps.provider_number = ?1 ");
		}
		if (aPractitionerSearchCriteria.isSubcategorySet()) {
			queryString.append("and dmps.provider_type = ?2 ");
		}
		if (aPractitionerSearchCriteria.isPractitionerNameSet()) {
			// Check name as its entered
			queryString.append("and (upper(dp2.last_name) LIKE ?3 OR upper(dp2.organization_name) LIKE ?3 OR ");
			queryString.append("upper(dp2.first_name) LIKE ?3");

			// parse string into individual words
			String[] arr = aPractitionerSearchCriteria.getPractitionerName().split(" ");

			/*
			 * The clause comes out like this for multiple names entered (3 names in this example):
			 * 
			 * and (x.organization_name LIKE &3 OR x.last_name LIKE &3 OR x.first_name LIKE &3 OR ((x.last_name LIKE &30
			 * or x.first_name LIKE &30) and (x.last_name LIKE &31 or x.first_name LIKE &31) and (x.last_name LIKE &32
			 * or x.first_name LIKE &32)))
			 */
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(dp2.last_name) LIKE ?3" + i + " or upper(dp2.first_name) LIKE ?3" + i
							+ ") ");
				}

				queryString.append("))");
			}
		}
		if (aPractitionerSearchCriteria.isYearEndDateSet()) {
			queryString.append("and dmps.year_end_date = ?4 ");
		}
		if (aPractitionerSearchCriteria.isPeriodIdSet()) {
			queryString.append("and dmps.period_id = ?5 ");
		}
		if (aPractitionerSearchCriteria.isPeerGroupIdSet()) {
			queryString.append("and dmps.provider_peer_group_id = ?6 ");
		}
		if (aPractitionerSearchCriteria.isPeerGroupNameSet()) {
			queryString.append("and dppg.provider_peer_group_name LIKE ?7 ");
		}
		if (aPractitionerSearchCriteria.isCitySet()) {
			queryString.append("and dp2.city LIKE ?8 ");
		}
		if (aPractitionerSearchCriteria.isLiscenseNumberSet()) {
			queryString.append("and dp2.provider_license_number LIKE ?9 ");
		}
		if (aPractitionerSearchCriteria.isSpecialtySet()) {
			queryString.append("and s.specialty_code LIKE ?10 ");
		}
		return queryString;
	}

	/**
	 * Applies query arguments for Practitioner search/count
	 * 
	 * @param aQuery
	 * @param aPractitionerSearchCriteria
	 */
	private void setPractitionerSearchQueryParameters(Query aQuery,
			PractitionerSearchCriteria aPractitionerSearchCriteria) {

		if (aPractitionerSearchCriteria.isPractitionerNumberSet()) {
			aQuery.setParameter(1, aPractitionerSearchCriteria.getPractitionerNumber());
		}
		if (aPractitionerSearchCriteria.isSubcategorySet()) {
			aQuery.setParameter(2, aPractitionerSearchCriteria.getSubcategory());
		}
		if (aPractitionerSearchCriteria.isPractitionerNameSet()) {
			aQuery.setParameter(3, "%" + aPractitionerSearchCriteria.getPractitionerName().toUpperCase() + "%");

			// parse string into individual words
			String[] arr = aPractitionerSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("3" + i);

					aQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}
		if (aPractitionerSearchCriteria.isYearEndDateSet()) {
			aQuery.setParameter(4, aPractitionerSearchCriteria.getYearEndDate());
		}
		if (aPractitionerSearchCriteria.isPeriodIdSet()) {
			aQuery.setParameter(5, aPractitionerSearchCriteria.getPeriodId());
		}
		if (aPractitionerSearchCriteria.isPeerGroupIdSet()) {
			aQuery.setParameter(6, aPractitionerSearchCriteria.getPeerGroupId());
		}
		if (aPractitionerSearchCriteria.isPeerGroupNameSet()) {
			aQuery.setParameter(7, "%" + aPractitionerSearchCriteria.getPeerGroupName().toUpperCase() + "%");
		}
		if (aPractitionerSearchCriteria.isCitySet()) {
			aQuery.setParameter(8, "%" + aPractitionerSearchCriteria.getCity().toUpperCase() + "%");
		}
		if (aPractitionerSearchCriteria.isLiscenseNumberSet()) {
			aQuery.setParameter(9, '%' + aPractitionerSearchCriteria.getLiscenseNumber() + '%');
		}
		if (aPractitionerSearchCriteria.isSpecialtySet()) {
			aQuery.setParameter(10, '%' + aPractitionerSearchCriteria.getSpecialty() + '%');
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.InquiryDAO#getOptDates(java.lang.Long, java.lang.String)
	 */
	@Override
	public Object[] getOptDates(Long aPractitionerNumber, String aPractitionerType) {

		logger.debug("InquiryDAO getOptDates() : Begin");

		// Create query string
		StringBuilder queryString = new StringBuilder(
				"SELECT max(dpoh.opt_in_date) as opt_in_date, max(dpoh.opt_out_date) as opt_out_date "
						+ "FROM dss_provider_opt_history dpoh WHERE dpoh.provider_number = ?1 "
						+ "and dpoh.provider_type = ?2 and dpoh.program_code = 'MC'");

		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aPractitionerNumber);
		query.setParameter(2, aPractitionerType);

		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		// Check that we only got one record
		if (results.size() != 1) {
			// More than one record. Log occurrence to logs
			logger.error("To many opt rows returned: " + results.size());
		}

		logger.debug("InquiryDAO getOptDates() : End");
		return results.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.InquiryDAO#getPractitionerCounts(java.lang.Long, java.util.Date,
	 * java.lang.String)
	 */
	@Override
	public Object[] getPractitionerCounts(Long aPeerGroupId, Date aYearEndDate, String aShadowBillingIndicator) {

		logger.debug("InquiryDAO getPractitionerCounts() : Begin");

		// Create query String
		StringBuilder queryString = new StringBuilder("SELECT Count(*) as number_of_providers_in_group, "
				+ "Sum(decode(pgx.drop_indicator, 'N', 1, 'Y', 0, 0)) as providers_used_for_average, "
				+ "Sum(decode(pgx.drop_indicator, 'N', 0, 'Y', 1, 1)) as providers_dropped ");

		// Append the correct table to select from based on the ProfileType passed
		if (aShadowBillingIndicator.equals("Y")) {
			// Selecting from dss_shad_prov_peer_group_xref
			queryString.append("FROM dss_shad_prov_peer_group_xref pgx ");
		} else if (aShadowBillingIndicator.equals("N")) {
			// Selecting from dss_provider_peer_group_xref
			queryString.append("FROM dss_provider_peer_group_xref pgx ");
		}

		queryString.append("WHERE pgx.provider_peer_group_id = ?1 and pgx.year_end_date = ?2");

		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aPeerGroupId);
		query.setParameter(2, aYearEndDate);

		// Execute query. Should only ever get 1 result
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		if (results.size() != 1) {
			// More than one record. Log occurrence to logs
			logger.error("To many practitioner counts returned: " + results.size());
		}

		logger.debug("InquiryDAO getPractitionerCounts() : End");
		// return first record in list
		return results.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.InquiryDAO#getProfilePeriodDetailsByYearEnd(java.util.Date )
	 */
	@Override
	public Object[] getProfilePeriodDetailsByYearEnd(Date aYearEndDate) {

		logger.debug("InquiryDAO getProfilePeriodDetailsByYearEnd() : Begin");

		// Create query string
		StringBuilder queryString = new StringBuilder("SELECT min(dpp.period_start_date) as period_start_date, "
				+ "max(dpp.period_end_date) as period_end_date, min(period_id) as min_period_id, "
				+ "max(period_id) as max_period_id FROM dss_profile_period dpp "
				+ "WHERE dpp.year_end_date = ?1 and dpp.period_type = 'PROVI'");

		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aYearEndDate);

		// Execute query. Should only ever get 1 result
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		if (results.size() != 1) {
			// More than one record. Log occurrence to logs
			logger.error("To many ProfilePeriods returned: " + results.size());
		}

		logger.debug("InquiryDAO getProfilePeriodDetailsByYearEnd() : End");
		// return first record in list
		return results.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.InquiryDAO#getProfilePeriodDetailsByPeriodID(java.lang.Long)
	 */
	@Override
	public Object[] getProfilePeriodDetailsByPeriodID(Long aPeriodId) {

		logger.debug("InquiryDAO getProfilePeriodDetailsByPeriodID() : Begin");

		// Create query string
		StringBuilder queryString = new StringBuilder("SELECT dpp.period_start_date, dpp.period_end_date "
				+ "FROM dss_profile_period dpp WHERE dpp.period_id = ?1 and dpp.period_type = 'PROVI'");

		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aPeriodId);

		// Execute query. Should only ever get 1 result
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		if (results.size() != 1) {
			// More than one record. Log occurrence to logs
			logger.error("To many ProfilePeriods returned: " + results.size());
		}

		logger.debug("InquiryDAO getProfilePeriodDetailsByPeriodID() : End");
		// return first record in list
		return results.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.InquiryDAO#getPractitionerHealthServiceCodeDetails(ca.medavie.nspp.audit.service
	 * .data.Practitioner)
	 */
	@Override
	public List<Object[]> getPractitionerHealthServiceCodeDetails(Practitioner aPractitioner) {

		logger.debug("InquiryDAO getPractitionerHealthServiceCodeDetails() : Begin");

		// Create query string
		StringBuilder queryString = new StringBuilder(
				"SELECT dhsg.health_service_group_name, dmpds.health_service_group_id, "
						+ "dmpds.number_of_patients, dmpds.number_of_se, "
						+ "ROUND(dmpds.total_amount_paid, 0), NVL(ROUND(dmgds.percentage_payment, 1), 0) as percentage_payment_group, "
						+ "round(decode(dmpds.number_of_patients, 0, 0, dmpds.number_of_se / dmpds.number_of_patients), "
						+ "2) as number_of_se_per_patient, round(decode(dmpds.number_of_patients, 0, 0, "
						+ "dmpds.total_amount_paid / dmpds.number_of_patients), 2) as amount_paid_per_patient, "
						+ "NVL(dmpds.idx_amount_paid_per_100, 0) as idx_amount_paid_per_100, "
						+ "NVL(dmpds.idx_number_of_se_per_100, 0) as idx_number_of_se_per_100, "
						+ "NVL(dmpds.idx_number_of_patients, 0) as idx_number_of_patients, "
						+ "NVL(dmpds.idx_number_of_se, 0) as idx_number_of_se, "
						+ "NVL(dmpds.idx_total_amount_paid, 0) as idx_total_amount_paid, "
						+ "NVL(dmpds.idx_number_of_se_per_patient, 0) as idx_number_of_se_per_patient, "
						+ "NVL(dmpds.idx_amount_paid_per_patient, 0) as idx_amount_paid_per_patient, "
						+ "NVL(ROUND(dmpds.percentage_payment, 1), 0) as percentage_payment, "
						+ "NVL(dmpds.number_of_se_per_100, 0) as number_of_se_per_100, "
						+ "NVL(dmpds.amount_paid_per_100, 0) as amount_paid_per_100 FROM dss_med_prov_dtl_summary dmpds "
						+ "LEFT OUTER JOIN dss_med_group_dtl_summary dmgds "
						+ "ON dmpds.provider_peer_group_id = dmgds.provider_peer_group_id "
						+ "AND dmpds.health_service_group_id = dmgds.health_service_group_id "
						+ "AND dmpds.period_id = dmgds.period_id "
						+ "AND dmpds.shadow_billing_indicator = dmgds.shadow_billing_indicator "
						+ "AND dmpds.year_end_date = dmgds.year_end_date, dss_health_service_group dhsg "
						+ "WHERE dmpds.health_service_group_id = dhsg.health_service_group_id "
						// Append the parameter arguments
						+ "AND dmpds.provider_peer_group_id = ?1 "
						+ "AND dmpds.provider_number = ?2 AND dmpds.provider_type = ?3 "
						+ "AND dmpds.year_end_date = ?4 AND dmpds.period_id = ?5 "
						+ "AND dmpds.shadow_billing_indicator = ?6 ");

		// Create query
		Query query = entityManager.createNativeQuery(queryString.toString());
		// Apply parameters
		query.setParameter(1, aPractitioner.getPeerGroup().getPeerGroupID());
		query.setParameter(2, aPractitioner.getPractitionerNumber());
		query.setParameter(3, aPractitioner.getPractitionerType());
		query.setParameter(4, aPractitioner.getPeerGroup().getYearEndDate());
		query.setParameter(5, aPractitioner.getPeerGroup().getProfilePeriod().getPeriodId());
		query.setParameter(6, aPractitioner.getShadowBillingIndicator());

		// Execute query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		logger.debug("InquiryDAO getPractitionerHealthServiceCodeDetails() : End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.InquiryDAO#getPractitionerHealthServiceCodeTotals(ca.medavie.nspp.audit.service
	 * .data.Practitioner)
	 */
	@Override
	public DssPractitionerHSCTotalsMappedEntity getPractitionerHealthServiceCodeTotals(Practitioner aPractitioner) {

		logger.debug("InquiryDAO getPractitionerHealthServiceCodeTotals() : Begin");

		// Build query String
		StringBuilder queryString = new StringBuilder(
				"SELECT dmps.number_of_patients, dmps.idx_number_of_patients, dmps.number_of_se, dmps.idx_number_of_se, dmps.total_amount_paid, dmps.idx_total_amount_paid, "
						+ "round(decode(dmps.number_of_patients, 0, 0, dmps.number_of_se / dmps.number_of_patients), 2) as number_of_se_per_patient, dmps.idx_number_of_se_per_patient, "
						+ "round(decode(dmps.number_of_patients, 0, 0, dmps.total_amount_paid / dmps.number_of_patients), 2) as amount_paid_per_patient, dmps.idx_amount_paid_per_patient "
						+ "FROM dss_med_prov_summary dmps WHERE dmps.provider_peer_group_id = ?1 and dmps.provider_number = ?2 and dmps.provider_type = ?3 and dmps.year_end_date = ?4 "
						+ "and dmps.period_id = ?5 and dmps.shadow_billing_indicator = ?6 ");

		// Create query, set Parameters and execute
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssPractitionerHSCTotalsQueryMapping");
		query.setParameter(1, aPractitioner.getPeerGroup().getPeerGroupID());
		query.setParameter(2, aPractitioner.getPractitionerNumber());
		query.setParameter(3, aPractitioner.getPractitionerType());
		query.setParameter(4, aPractitioner.getPeerGroup().getYearEndDate());
		query.setParameter(5, aPractitioner.getPeerGroup().getProfilePeriod().getPeriodId());
		query.setParameter(6, aPractitioner.getShadowBillingIndicator());

		// Execute query
		DssPractitionerHSCTotalsMappedEntity result = (DssPractitionerHSCTotalsMappedEntity) query.getSingleResult();

		logger.debug("InquiryDAO getPractitionerHealthServiceCodeTotals() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.InquiryDAO#getSearchedPeerGroups(ca.medavie.nspp.audit.service.data.
	 * PeerGroupSearchCriteria, int, int)
	 */
	@Override
	public List<Object[]> getSearchedPeerGroups(PeerGroupSearchCriteria aPeerGroupSearchCriteria, int startRow,
			int maxRows) {

		logger.debug("InquiryDAO getSearchedPeerGroups() : Begin");

		// Create the query string
		StringBuilder queryString = constructPeerGroupSearchSql(aPeerGroupSearchCriteria);

		// Append order by arguments
		if (aPeerGroupSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, aPeerGroupSearchCriteria);
		} else {
			// Append default sorting
			queryString.append("ORDER BY dmgs.provider_peer_group_id ASC, dmgs.year_end_date DESC");
		}

		// Create query
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply pagination arguments
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		// Apply query arguments
		setPeerGroupSearchQueryParameters(query, aPeerGroupSearchCriteria);

		@SuppressWarnings("unchecked")
		List<Object[]> result = query.getResultList();

		logger.debug("InquiryDAO getSearchedPeerGroups() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.InquiryDAO#getSearchPeerGroupsCount(ca.medavie.nspp.audit.service.data.
	 * PeerGroupSearchCriteria)
	 */
	@Override
	public Integer getSearchPeerGroupsCount(PeerGroupSearchCriteria aPeerGroupSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(*) FROM (");
		queryString.append(constructPeerGroupSearchSql(aPeerGroupSearchCriteria));
		queryString.append(")");

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setPeerGroupSearchQueryParameters(query, aPeerGroupSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the PeerGroup search SQL
	 * 
	 * @param aPeerGroupSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructPeerGroupSearchSql(PeerGroupSearchCriteria aPeerGroupSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dmgs.provider_peer_group_id, dppg.provider_peer_group_name, dmgs.year_end_date, dmgs.period_id, dmgs.shadow_billing_indicator, "
						+ "dmgs.number_of_patients, dmgs.number_of_se, dmgs.total_units, dmgs.total_amount_paid, Round(decode(dmgs.number_of_patients, 0, 0, "
						+ "dmgs.number_of_se / dmgs.number_of_patients), 2) as services_per_patient, Round(decode(dmgs.number_of_patients, 0, 0, "
						+ "dmgs.total_amount_paid / dmgs.number_of_patients), 2) as amount_paid_per_patient, dmgs.number_of_providers_in_group "
						+ "FROM dss_provider_peer_group dppg, dss_med_group_summary dmgs, dss_provider_2 dp2, ");

		// Append either dss_provider_peer_group_xref or dss_shad_prov_peer_group_xref depending on Profile Type
		if (aPeerGroupSearchCriteria.getProfileType().equals(PractitionerTypes.FEE_FOR_SERVICE)) {
			queryString.append("dss_provider_peer_group_xref pgx ");
		} else {
			queryString.append("dss_shad_prov_peer_group_xref pgx ");
		}

		// Append the rest of the base query
		queryString.append("WHERE dppg.provider_peer_group_id = dmgs.provider_peer_group_id "
				+ "and dppg.year_end_date = dmgs.year_end_date "
				+ "and pgx.provider_peer_group_id = dppg.provider_peer_group_id "
				+ "and pgx.year_end_date = dppg.year_end_date and dp2.provider_number = pgx.provider_number "
				+ "and dp2.provider_type = pgx.provider_type ");

		// Append the shadow billing parameter (This is always set)
		if (aPeerGroupSearchCriteria.getProfileType().equals(PractitionerTypes.FEE_FOR_SERVICE)) {
			queryString.append("and dmgs.shadow_billing_indicator = 'N' ");
		} else {
			queryString.append("and dmgs.shadow_billing_indicator = 'Y' ");
		}

		// Determine what search criteria was provided and add required AND statements
		if (aPeerGroupSearchCriteria.isYearEndDateSet()) {
			queryString.append("and dmgs.year_end_date = ?1 ");
		}
		if (aPeerGroupSearchCriteria.isSubcategorySet()) {
			queryString.append("and pgx.provider_type = ?2 ");
		}
		if (aPeerGroupSearchCriteria.isPeerGroupIDSet()) {
			queryString.append("and dmgs.provider_peer_group_id = ?3 ");
		}
		if (aPeerGroupSearchCriteria.isPeerGroupNameSet()) {
			queryString.append("and dppg.provider_peer_group_name LIKE ?4 ");
		}
		if (aPeerGroupSearchCriteria.isPeriodIdSet()) {
			queryString.append("and dmgs.period_id = ?5 ");
		}
		if (aPeerGroupSearchCriteria.isPractitionerIdSet()) {
			queryString.append("and pgx.provider_number = ?6 ");
		}
		if (aPeerGroupSearchCriteria.isPractitionerNameSet()) {
			queryString.append("and (upper(dp2.organization_name) LIKE ?7 OR ");
			queryString.append("upper(dp2.first_name) LIKE ?7 OR ");
			queryString.append("upper(dp2.middle_name) LIKE ?7 OR ");
			queryString.append("upper(dp2.last_name) LIKE ?7 ");

			// parse string into individual words
			String[] arr = aPeerGroupSearchCriteria.getPractitionerName().split(" ");

			// The clause comes out like this for multiple names entered
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(dp2.last_name) LIKE ?7" + i + " or upper(dp2.middle_name) LIKE ?7" + i
							+ " or upper(dp2.first_name) LIKE ?7" + i + ") ");
				}

				queryString.append("))");
			}
		}
		return queryString;
	}

	/**
	 * Applies query arguments for PeerGroup search/count
	 * 
	 * @param aQuery
	 * @param aPeerGroupSearchCriteria
	 */
	private void setPeerGroupSearchQueryParameters(Query aQuery, PeerGroupSearchCriteria aPeerGroupSearchCriteria) {

		// Apply required arguments
		if (aPeerGroupSearchCriteria.isYearEndDateSet()) {
			aQuery.setParameter(1, aPeerGroupSearchCriteria.getYearEndDate());
		}
		if (aPeerGroupSearchCriteria.isSubcategorySet()) {
			aQuery.setParameter(2, aPeerGroupSearchCriteria.getSubcategory());
		}
		if (aPeerGroupSearchCriteria.isPeerGroupIDSet()) {
			aQuery.setParameter(3, aPeerGroupSearchCriteria.getPeerGroupId());
		}
		if (aPeerGroupSearchCriteria.isPeerGroupNameSet()) {
			aQuery.setParameter(4, "%" + aPeerGroupSearchCriteria.getPeerGroupName() + "%");
		}
		if (aPeerGroupSearchCriteria.isPeriodIdSet()) {
			aQuery.setParameter(5, aPeerGroupSearchCriteria.getPeriodId());
		}
		if (aPeerGroupSearchCriteria.isPractitionerIdSet()) {
			aQuery.setParameter(6, aPeerGroupSearchCriteria.getPractitionerId());
		}
		if (aPeerGroupSearchCriteria.isPractitionerNameSet()) {
			aQuery.setParameter(7, "%" + aPeerGroupSearchCriteria.getPractitionerName().toUpperCase() + "%");

			// parse string into individual words
			String[] arr = aPeerGroupSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("7" + i);
					// remove any commas
					aQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.InquiryDAO#getPeerGroupHealthServiceCodeDetails(ca.medavie.nspp.audit.service
	 * .data.PeerGroup)
	 */
	@Override
	public void getPeerGroupHealthServiceCodeDetails(PeerGroup aPeerGroup) {

		logger.debug("InquiryDAO getPeerGroupHealthServiceCodeDetails() : Begin");

		List<HealthServiceCode> result = new ArrayList<HealthServiceCode>();

		// Step 1 load all HealthServiceCodes performed under this Group
		StringBuilder hscQueryString = new StringBuilder(
				"SELECT a.health_service_group_id, MAX(a.health_service_group_name), MAX(b.number_of_patients), ROUND(MAX(b.avg_number_of_patients), 0), "
						+ "MAX(b.number_of_se) as number_of_se, ROUND(MAX(b.avg_number_of_se), 0), ROUND(MAX(b.total_amount_paid), 0), ROUND(MAX(b.avg_total_amount_paid), 0), "
						+ "MAX(b.percentage_payment), MAX(b.avg_number_of_se_per_patient), MAX(b.avg_amount_paid_per_patient), MAX(b.avg_number_of_se_per_100), "
						+ "MAX(b.avg_amount_paid_per_100) FROM dss_med_group_dtl_summary b, dss_health_service_group a WHERE b.provider_peer_group_id = ?1 "
						+ "AND b.health_service_group_id = a.health_service_group_id AND b.year_end_date = ?2 AND b.period_id = ?3 "
						+ "AND b.shadow_billing_indicator = ?4 GROUP BY a.health_service_group_id");

		Query hscQuery = entityManager.createNativeQuery(hscQueryString.toString());
		// Apply parameters
		hscQuery.setParameter(1, aPeerGroup.getPeerGroupID());
		hscQuery.setParameter(2, aPeerGroup.getYearEndDate());
		hscQuery.setParameter(3, aPeerGroup.getProfilePeriod().getPeriodId());
		hscQuery.setParameter(4, aPeerGroup.getShadowBillingIndicator());

		@SuppressWarnings("unchecked")
		List<Object[]> hscQueryResults = hscQuery.getResultList();

		// Convert data from DB to HealthServiceCode objects
		for (Object[] row : hscQueryResults) {

			HealthServiceCode hsc = new HealthServiceCode();
			hsc.setHealthServiceGroupId((BigDecimal) row[0]);
			hsc.setHealthServiceGroupName((String) row[1]);
			hsc.setNumberOfPatients((BigDecimal) row[2]);
			hsc.setAverageNumberOfPatients((BigDecimal) row[3]);
			hsc.setNumberOfServices((BigDecimal) row[4]);
			hsc.setAverageNumberOfServices((BigDecimal) row[5]);
			hsc.setTotalAmountPaid((BigDecimal) row[6]);
			hsc.setAverageAmountPaid((BigDecimal) row[7]);
			hsc.setAveragePercentagePayment((BigDecimal) row[8]);
			hsc.setAverageServicesPerPatient((BigDecimal) row[9]);
			hsc.setAverageAmountPaidPerPatient((BigDecimal) row[10]);
			hsc.setAverageNumberOfServicesPer100((BigDecimal) row[11]);
			hsc.setAverageAmountPaidPer100((BigDecimal) row[12]);

			// Add to list
			result.add(hsc);
		}

		// Step 2 load all deviation information for the services
		StringBuilder hsDeviationQueryString = new StringBuilder(
				"SELECT SUM(DECODE(b.provider_number, NULL, 0, 1)) as number_of_providers, "
						+ "ROUND(STDDEV(NVL(b.number_of_patients, 0)), 0) as std_dev_number_of_patients, ROUND(STDDEV(NVL(b.number_of_se, 0)), 0) as std_dev_number_of_se, "
						+ "ROUND(STDDEV(NVL(b.total_amount_paid, 0)), 0) as std_dev_total_amount_paid, ROUND(STDDEV(DECODE(b.number_of_patients, 0, 0, NULL, 0, NVL(b.number_of_se, 0) / b.number_of_patients)), "
						+ "2) as std_dev_number_of_se_per_pat, ROUND(STDDEV(DECODE(b.number_of_patients, 0, 0, NULL, 0, NVL(b.total_amount_paid, 0) / b.number_of_patients)), "
						+ "2) as std_dev_amount_paid_per_pat, ROUND(STDDEV(NVL(b.number_of_se_per_100, 0)), 2) as std_dev_number_of_se_per_100, "
						+ "ROUND(STDDEV(NVL(b.amount_paid_per_100, 0)), 2) as std_dev_amount_paid_per_100 FROM dss_med_prov_dtl_summary b, dss_provider_peer_group_xref a "
						+ "WHERE a.provider_peer_group_id = ?1 AND a.year_end_date = ?2 AND a.provider_number > 0 AND a.provider_type > ' ' "
						+ "AND a.drop_indicator = 'N' AND b.provider_peer_group_id(+) = a.provider_peer_group_id AND b.provider_number(+) = a.provider_number "
						+ "AND b.provider_type(+) = a.provider_type AND b.health_service_group_id(+) = ?3 AND b.year_end_date(+) = a.year_end_date AND b.period_id(+) = ?4 "
						+ "AND b.shadow_billing_indicator(+) = ?5");

		Query hsDeviationQuery = entityManager.createNativeQuery(hsDeviationQueryString.toString());

		// Apply parameters
		hsDeviationQuery.setParameter(1, aPeerGroup.getPeerGroupID());
		hsDeviationQuery.setParameter(2, aPeerGroup.getYearEndDate());
		hsDeviationQuery.setParameter(4, aPeerGroup.getProfilePeriod().getPeriodId());
		hsDeviationQuery.setParameter(5, aPeerGroup.getShadowBillingIndicator());

		for (HealthServiceCode hsc : result) {

			// Adjust the HealthServiceGroup ID prior to each query execution
			hsDeviationQuery.setParameter(3, hsc.getHealthServiceGroupId());

			// Only expect a single result
			// For some odd reason I need to type cast the first object in the list to Object[]....
			Object[] deviationResult = (Object[]) hsDeviationQuery.getResultList().get(0);

			// Update the HealthServiceCode record with the details loaded
			hsc.setNumberOfPractitioners((BigDecimal) deviationResult[0]);
			hsc.setStandardNumberOfPatientDeviation((BigDecimal) deviationResult[1]);
			hsc.setStandardNumberOfServicesDeviation((BigDecimal) deviationResult[2]);
			hsc.setStandardAmountPaidDeviation((BigDecimal) deviationResult[3]);
			hsc.setStandardServicesPerPatientDeviation((BigDecimal) deviationResult[4]);
			hsc.setStandardAmountPaidPerPatientDeviation((BigDecimal) deviationResult[5]);
			hsc.setStandardNumberOfServicesPer100Deviation((BigDecimal) deviationResult[6]);
			hsc.setStandardAmountPaidPer100Deviation((BigDecimal) deviationResult[7]);
		}
		// Add the HealthServicesCodes to the PeerGroup
		aPeerGroup.setHealthServiceCodes(result);

		logger.debug("InquiryDAO getPeerGroupHealthServiceCodeDetails() : Begin");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.InquiryDAO#getPeerGroupHealthServiceCodeTotals(ca.medavie.nspp.audit.service
	 * .data.PeerGroup)
	 */
	@Override
	public DssPeerGroupHSCTotalsMappedEntity getPeerGroupHealthServiceCodeTotals(PeerGroup aPeerGroup) {
		logger.debug("InquiryDAO getPeerGroupHealthServiceCodeTotals() : Begin");

		// Build query String
		StringBuilder queryString = new StringBuilder(
				"SELECT SUM(DECODE(c.provider_number, NULL, 0, 1)) as number_of_providers, MAX(b.number_of_patients) as number_of_patients, "
						+ "MAX(b.avg_number_of_patients) as avg_number_of_patients, ROUND(STDDEV(NVL(c.number_of_patients, 0)), 0) as std_dev_number_of_patients, "
						+ "MAX(b.number_of_se) as number_of_se, MAX(b.avg_number_of_se) as avg_number_of_se, ROUND(STDDEV(NVL(c.number_of_se, 0)), 0) as std_dev_number_of_se, "
						+ "MAX(b.total_amount_paid) as total_amount_paid, MAX(b.avg_total_amount_paid) as avg_total_amount_paid, "
						+ "ROUND(STDDEV(NVL(c.total_amount_paid, 0)), 0) as std_dev_total_amount_paid, 100 as percentage_payment, "
						+ "MAX(b.avg_number_of_se_per_patient) as avg_number_of_se_per_patient, ROUND(STDDEV(DECODE(c.number_of_patients, 0, 0, NULL, 0, "
						+ "NVL(c.number_of_se, 0) / c.number_of_patients)), 2) as std_dev_number_of_se_per_pat, MAX(b.avg_amount_paid_per_patient) as avg_amount_paid_per_patient, "
						+ "ROUND(STDDEV(DECODE(c.number_of_patients, 0, 0, NULL, 0, NVL(c.total_amount_paid, 0) / c.number_of_patients)), 2) as std_dev_amount_paid_per_pat, "
						+ "MAX(b.avg_number_of_se_per_patient) * 100 as avg_number_of_se_per_100, ROUND(STDDEV(DECODE(c.number_of_patients, 0, 0, NULL, 0, "
						+ "NVL(c.number_of_se, 0) / c.number_of_patients)) * 100, 2) as std_dev_number_of_se_per_100, MAX(b.avg_amount_paid_per_patient) * 100 as avg_amount_paid_per_100, "
						+ "ROUND(STDDEV(DECODE(c.number_of_patients, 0, 0, NULL, 0, NVL(c.total_amount_paid, 0) / c.number_of_patients)) * 100, 2) as std_dev_amount_paid_per_100 "
						+ "FROM dss_med_prov_summary c, dss_med_group_summary b, ");

		// Need to append the correct xref table based on the PeerGroups Shadow Indicator
		if (aPeerGroup.getShadowBillingIndicator().equals("Y")) {
			queryString.append("dss_shad_prov_peer_group_xref a ");
		} else {
			queryString.append("dss_provider_peer_group_xref a ");
		}

		// Append the remainder of the query
		queryString
				.append("WHERE a.provider_peer_group_id = ?1 and a.year_end_date = ?2 and a.provider_number > 0 and a.provider_type > ' ' and a.drop_indicator = 'N' "
						+ "and b.provider_peer_group_id = a.provider_peer_group_id and b.year_end_date = a.year_end_date and b.period_id = ?3 "
						+ "and b.shadow_billing_indicator = ?4 and c.provider_peer_group_id(+) = a.provider_peer_group_id and c.provider_number(+) = a.provider_number "
						+ "and c.provider_type(+) = a.provider_type and c.year_end_date(+) = a.year_end_date and c.period_id(+) = ?3 and c.shadow_billing_indicator(+) = ?4");

		// Create query, apply arguments and execute
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssPeerGroupHSCTotalsQueryMapping");
		query.setParameter(1, aPeerGroup.getPeerGroupID());
		query.setParameter(2, aPeerGroup.getYearEndDate());
		query.setParameter(3, aPeerGroup.getProfilePeriod().getPeriodId());
		query.setParameter(4, aPeerGroup.getShadowBillingIndicator());

		// Execute query
		DssPeerGroupHSCTotalsMappedEntity result = (DssPeerGroupHSCTotalsMappedEntity) query.getSingleResult();

		logger.debug("InquiryDAO getPeerGroupHealthServiceCodeTotals() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.InquiryDAO#getPractitionerPaymentDetails(ca.medavie.nspp.audit.service.data
	 * .Practitioner)
	 */
	@Override
	public PaymentDetails getPractitionerPaymentDetails(Practitioner aPractitioner) {

		logger.debug("InquiryDAO getPractitionerPaymentDetails() : Begin");

		// Holds payment details
		PaymentDetails details = new PaymentDetails();

		// Pull out required data from the Practitioner
		Date periodStartDate = aPractitioner.getPeerGroup().getProfilePeriod().getPeriodStartDate();
		Date periodEndDate = aPractitioner.getPeerGroup().getProfilePeriod().getPeriodEndDate();
		Date yearEndDate = aPractitioner.getPeerGroup().getYearEndDate();
		Long peerGroupId = aPractitioner.getPeerGroup().getPeerGroupID();

		Long practitionerNumber = aPractitioner.getPractitionerNumber();
		String practitionerType = aPractitioner.getPractitionerType();

		// ******************************************************
		// ***** Get Total Amount Paid for the Practitioner *****
		// ******************************************************
		aPractitioner.setTotalAmountPaidFFS(getPractitionerTotalFFS(aPractitioner));

		// *****************************************************************************
		// ***** Get Number of Paying Practitioners in the Alternate Reimbursement *****
		// *****************************************************************************
		details.setTotalNumberOfReimbursements(getTotalNumberOfReimbursements(periodStartDate, periodEndDate,
				peerGroupId, yearEndDate));

		// *****************************************************************************
		// ***** Get Number of Paying Practitioners in the Bottom Line Adjustments *****
		// *****************************************************************************
		details.setTotalNumberOfBottomLineAdjustments(getTotalNumberOfBottomLineAdjustments(periodStartDate,
				periodEndDate, peerGroupId, yearEndDate));

		// *****************************
		// ***** Get Sessional Pay *****
		// *****************************
		// Create the query string
		StringBuilder sessionalPayQueryString = new StringBuilder(
				"SELECT sum(vdspp.amount_paid) FROM vw_dss_sessional_per_pay vdspp "
						+ "WHERE vdspp.bus_arr_number > 0 and vdspp.sessional_sequence_number > 0 and vdspp.provider_number = ?1 "
						+ "and vdspp.provider_type = ?2 and vdspp.effective_to_date >= ?3 and vdspp.effective_from_date <= ?4");

		Query sessionalPayQuery = entityManager.createNativeQuery(sessionalPayQueryString.toString());
		// Add required arguments
		sessionalPayQuery.setParameter(1, practitionerNumber);
		sessionalPayQuery.setParameter(2, practitionerType);
		sessionalPayQuery.setParameter(3, periodStartDate);
		sessionalPayQuery.setParameter(4, periodEndDate);
		details.setSessionalPay((BigDecimal) sessionalPayQuery.getSingleResult());

		// *******************************
		// ***** Get Psychiatric Pay *****
		// *******************************
		StringBuilder psychiatricPayQueryString = new StringBuilder(
				"SELECT sum(vdpapp.amount_paid) FROM vw_dss_psych_act_per_pay vdpapp WHERE vdpapp.bus_arr_number > 0 "
						+ "and vdpapp.provider_number = ?1 and vdpapp.provider_type = ?2 and vdpapp.psych_act_to_date >= ?3 "
						+ "and vdpapp.psych_act_from_date <= ?4");

		Query psychiatricPayQuery = entityManager.createNativeQuery(psychiatricPayQueryString.toString());
		// Add required arguments
		psychiatricPayQuery.setParameter(1, practitionerNumber);
		psychiatricPayQuery.setParameter(2, practitionerType);
		psychiatricPayQuery.setParameter(3, periodStartDate);
		psychiatricPayQuery.setParameter(4, periodEndDate);
		details.setPsychiatricPay((BigDecimal) psychiatricPayQuery.getSingleResult());

		// **************************
		// ***** Get Salary Pay *****
		// **************************
		StringBuilder salaryPerPayQueryString = new StringBuilder(
				"SELECT sum(vdspp.amount_paid) FROM vw_dss_salary_per_pay vdspp WHERE vdspp.salary_sequence_number > 0 "
						+ "and vdspp.provider_number = ?1 and vdspp.provider_type = ?2 and vdspp.salary_to_date >= ?3 "
						+ "and vdspp.salary_from_date <= ?4");

		Query salaryPerPayQuery = entityManager.createNativeQuery(salaryPerPayQueryString.toString());
		// Add required arguments
		salaryPerPayQuery.setParameter(1, practitionerNumber);
		salaryPerPayQuery.setParameter(2, practitionerType);
		salaryPerPayQuery.setParameter(3, periodStartDate);
		salaryPerPayQuery.setParameter(4, periodEndDate);
		details.setSalaryPerPay((BigDecimal) salaryPerPayQuery.getSingleResult());

		// **********************************
		// ***** Get Diagnostic Imaging *****
		// **********************************
		StringBuilder diagImagingQueryString = new StringBuilder(
				"SELECT sum(vdnppp.amount_paid) FROM vw_dss_non_patient_per_pay vdnppp WHERE vdnppp.non_patient_per_pay_id > 0 "
						+ "and vdnppp.provider_number = ?1 and vdnppp.provider_type = ?2 and vdnppp.end_date >= ?3 and vdnppp.start_date <= ?4");

		Query diagImagingQuery = entityManager.createNativeQuery(diagImagingQueryString.toString());
		// Add required arguments
		diagImagingQuery.setParameter(1, practitionerNumber);
		diagImagingQuery.setParameter(2, practitionerType);
		diagImagingQuery.setParameter(3, periodStartDate);
		diagImagingQuery.setParameter(4, periodEndDate);
		details.setDiagImaging((BigDecimal) diagImagingQuery.getSingleResult());

		// **************************************
		// ***** Get Bottom Line Adjustment *****
		// **************************************
		StringBuilder bottomLineAdjQueryString = new StringBuilder(
				"SELECT sum(vdapp.adjustment_amount) FROM dss_bottom_line_adjustment dbla, vw_dss_adjustment_per_pay vdapp "
						+ "WHERE vdapp.adjustment_id = dbla.adjustment_id AND ((vdapp.adjustment_id > 0) AND (vdapp.provider_number = ?1) AND (vdapp.provider_type = ?2)) "
						+ "AND dbla.effective_from_date <= ?4 AND dbla.effective_to_date >= ?3 AND dbla.adjustment_type_code > 3 AND dbla.adjustment_type_code NOT IN (7, 9, 12, 16, 17) "
						+ "AND (vdapp.payment_number IN (SELECT DISTINCT dp.payment_number FROM dss_payment dp WHERE dp.payment_date <= ?4 AND dp.payment_date >= ?3))");

		Query bottomLineAdjQuery = entityManager.createNativeQuery(bottomLineAdjQueryString.toString());
		// Add required arguments
		bottomLineAdjQuery.setParameter(1, practitionerNumber);
		bottomLineAdjQuery.setParameter(2, practitionerType);
		bottomLineAdjQuery.setParameter(3, periodStartDate);
		bottomLineAdjQuery.setParameter(4, periodEndDate);
		details.setBottomLineAdj((BigDecimal) bottomLineAdjQuery.getSingleResult());

		// *******************************
		// ***** Get Manual Deposits *****
		// *******************************
		StringBuilder manualDepositsQueryString = new StringBuilder(
				"SELECT sum(dmp.deposit_amount) FROM dss_manual_deposits dmp "
						+ "WHERE (dmp.gl_number in (800, 803, 825, 830, 831, 832, 834, 836, 839, 840, 842, 844)) and (dmp.provider_type IN ('PH', 'OP')) "
						+ "and (dmp.provider_number = ?1) and (dmp.provider_type = ?2) and (dmp.date_received <= ?4) and (dmp.date_received >= ?3)");

		Query manualDepositsQuery = entityManager.createNativeQuery(manualDepositsQueryString.toString());
		// Add required arguments
		manualDepositsQuery.setParameter(1, practitionerNumber);
		manualDepositsQuery.setParameter(2, practitionerType);
		manualDepositsQuery.setParameter(3, periodStartDate);
		manualDepositsQuery.setParameter(4, periodEndDate);
		details.setManualDeposits((BigDecimal) manualDepositsQuery.getSingleResult());

		// *************************************
		// ***** Get Cheque Reconciliation *****
		// *************************************
		StringBuilder chequeReconiliationQueryString = new StringBuilder(
				"SELECT sum(dcr.cheque_amount) FROM dss_cheque_reconciliation dcr WHERE (dcr.gl_number IS NOT NULL) "
						+ "and (dcr.gl_number in (803, 825, 830, 831, 832, 834, 836, 839, 840, 842, 844)) and (dcr.provider_type IN ('PH', 'OP')) "
						+ "and (dcr.reconciliation_status NOT IN ('C', 'V')) and (dcr.provider_number = ?1) and (dcr.provider_type = ?2) "
						+ "and (dcr.payment_date <= ?4) and (dcr.payment_date >= ?3)");

		Query chequeReconiliationQuery = entityManager.createNativeQuery(chequeReconiliationQueryString.toString());
		// Add required arguments
		chequeReconiliationQuery.setParameter(1, practitionerNumber);
		chequeReconiliationQuery.setParameter(2, practitionerType);
		chequeReconiliationQuery.setParameter(3, periodStartDate);
		chequeReconiliationQuery.setParameter(4, periodEndDate);
		details.setCheqReconciliation((BigDecimal) chequeReconiliationQuery.getSingleResult());

		logger.debug("InquiryDAO getPractitionerPaymentDetails() : End");
		return details;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.InquiryDAO#getPractitionerTotalAmountPaid(ca.medavie.nspp.audit.service.data
	 * .Practitioner)
	 */
	@Override
	public BigDecimal getPractitionerTotalAmountPaid(Practitioner aPractitioner) {

		logger.debug("InquiryDAO getPractitionerTotalAmountPaid() : Begin");

		StringBuilder totalPaidQueryString = new StringBuilder(
				"SELECT SUM(dmpas.total_amount_paid) FROM dss_med_prov_act_summary dmpas WHERE dmpas.provider_number = ?1 "
						+ "AND dmpas.provider_type = ?2 AND dmpas.year_end_date = ?3");

		Query totalPaidQuery = entityManager.createNativeQuery(totalPaidQueryString.toString());
		// Apply required arguments
		totalPaidQuery.setParameter(1, aPractitioner.getPractitionerNumber());
		totalPaidQuery.setParameter(2, aPractitioner.getPractitionerType());
		totalPaidQuery.setParameter(3, aPractitioner.getPeerGroup().getYearEndDate());

		BigDecimal result = (BigDecimal) totalPaidQuery.getSingleResult();
		// Check that we got something back.. if not default is 0
		if (result == null) {
			result = BigDecimal.ZERO;
		}

		logger.debug("InquiryDAO getPractitionerTotalAmountPaid() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.InquiryDAO#getPeerGroupPaymentDetails(ca.medavie.nspp.audit.service.data.PeerGroup
	 * )
	 */
	@Override
	public PaymentDetails getPeerGroupPaymentDetails(PeerGroup aPeerGroup) {

		logger.debug("InquiryDAO getPeerGroupPaymentDetails() : Begin");

		// Holds payment details
		PaymentDetails details = new PaymentDetails();

		// Pull out required data from the PeerGroup
		Long peerGroupId = aPeerGroup.getPeerGroupID();
		Long periodId = aPeerGroup.getProfilePeriod().getPeriodId();
		Date periodStartDate = aPeerGroup.getProfilePeriod().getPeriodStartDate();
		Date periodEndDate = aPeerGroup.getProfilePeriod().getPeriodEndDate();
		Date yearEndDate = aPeerGroup.getYearEndDate();
		Long practitionersUsedForAvg = aPeerGroup.getNumberOfPractitionersUsedForAverage();
		String shadowBillingIndicator = aPeerGroup.getShadowBillingIndicator();

		// *****************************************************************************
		// ***** Get Number of Paying Practitioners in the Alternate Reimbursement *****
		// *****************************************************************************
		details.setTotalNumberOfReimbursements(getTotalNumberOfReimbursements(periodStartDate, periodEndDate,
				peerGroupId, yearEndDate));

		// *****************************************************************************
		// ***** Get Number of Paying Practitioners in the Bottom Line Adjustments *****
		// *****************************************************************************
		details.setTotalNumberOfBottomLineAdjustments(getTotalNumberOfBottomLineAdjustments(periodStartDate,
				periodEndDate, peerGroupId, yearEndDate));

		// ************************************
		// ***** Get Group Summary totals *****
		// ************************************
		StringBuilder groupSummaryQueryString = new StringBuilder(
				"SELECT dmgs.number_of_patients, dmgs.total_amount_paid, round(decode(?1, 0, 0, dmgs.total_amount_paid / ?1), "
						+ "2) as total_amount_paid_average FROM dss_med_group_summary dmgs WHERE dmgs.provider_peer_group_id = ?2 "
						+ "and dmgs.year_end_date = ?3 and dmgs.period_id = ?4 and dmgs.shadow_billing_indicator = ?5");
		Query groupSummaryQuery = entityManager.createNativeQuery(groupSummaryQueryString.toString());

		// First query run uses Shadow Billing hardcoded to 'N' to load total amount paid and avg
		// Apply required arguments
		groupSummaryQuery.setParameter(1, practitionersUsedForAvg);
		groupSummaryQuery.setParameter(2, peerGroupId);
		groupSummaryQuery.setParameter(3, yearEndDate);
		groupSummaryQuery.setParameter(4, periodId);
		groupSummaryQuery.setParameter(5, "N");

		// Execute query run 1.. Returns: Total Amount Paid, Total Amount Paid Avg
		@SuppressWarnings("unchecked")
		List<Object[]> summaryResultList = groupSummaryQuery.getResultList();
		if (summaryResultList != null && !summaryResultList.isEmpty()) {
			Object[] groupSummaryResult = summaryResultList.get(0);

			details.setTotalAmountPaid((BigDecimal) groupSummaryResult[1]);
			details.setTotalAmountPaidAverage((BigDecimal) groupSummaryResult[2]);
		} else {
			// No value was loaded... Set values to 0
			details.setTotalAmountPaid(BigDecimal.ZERO);
			details.setTotalAmountPaidAverage(BigDecimal.ZERO);
		}

		// Second query run uses the true Shadow Billing value to load the patientsInGroup total
		groupSummaryQuery = entityManager.createNativeQuery(groupSummaryQueryString.toString());
		// Apply required arguments
		groupSummaryQuery.setParameter(1, practitionersUsedForAvg);
		groupSummaryQuery.setParameter(2, peerGroupId);
		groupSummaryQuery.setParameter(3, yearEndDate);
		groupSummaryQuery.setParameter(4, periodId);
		groupSummaryQuery.setParameter(5, shadowBillingIndicator);

		// Execute query run 2.. Returns: Total Amount Paid, Total Amount Paid Avg
		@SuppressWarnings("unchecked")
		List<Object[]> patientCountSummaryList = groupSummaryQuery.getResultList();
		if (summaryResultList != null && !summaryResultList.isEmpty()) {
			Object[] groupSummaryResult = patientCountSummaryList.get(0);

			details.setNumberOfPatients(((BigDecimal) groupSummaryResult[0]).longValue());
		} else {
			// No value was loaded... Set value to 0
			details.setNumberOfPatients(Long.valueOf(0));
		}

		// *****************************
		// ***** Get Sessional Pay *****
		// *****************************
		StringBuilder sessionalPayQueryString = new StringBuilder(
				"SELECT sum(vdspp.amount_paid) FROM dss_provider_peer_group dppg, dss_provider_peer_group_xref dppgx, vw_dss_sessional_per_pay vdspp "
						+ "WHERE dppgx.provider_number = vdspp.provider_number and dppgx.provider_type = vdspp.provider_type and dppg.provider_peer_group_id = dppgx.provider_peer_group_id "
						+ "and dppgx.year_end_date = dppg.year_end_date and ((vdspp.bus_arr_number > 0) and (vdspp.sessional_sequence_number > 0) and (vdspp.effective_to_date >= ?1) "
						+ "and (vdspp.effective_from_date <= ?2) and (dppgx.provider_peer_group_id = ?3) and (dppgx.year_end_date = ?4) and "
						+ "(dppg.provider_peer_group_type = 'PROVI'))");
		Query sessionalPayQuery = entityManager.createNativeQuery(sessionalPayQueryString.toString());
		// Apply required arguments
		sessionalPayQuery.setParameter(1, periodStartDate);
		sessionalPayQuery.setParameter(2, periodEndDate);
		sessionalPayQuery.setParameter(3, peerGroupId);
		sessionalPayQuery.setParameter(4, yearEndDate);

		BigDecimal result = (BigDecimal) sessionalPayQuery.getSingleResult();
		// Check that we got something back.. if not default is 0
		if (result == null) {
			result = BigDecimal.ZERO;
		}
		details.setSessionalPay(result);

		// *******************************
		// ***** Get Psychiatric Pay *****
		// *******************************
		StringBuilder psychiatricQueryString = new StringBuilder(
				"SELECT sum(vdpapp.amount_paid) FROM dss_provider_peer_group dppg, dss_provider_peer_group_xref dppgx, vw_dss_psych_act_per_pay vdpapp "
						+ "WHERE dppgx.provider_number = vdpapp.provider_number and dppgx.provider_type = vdpapp.provider_type "
						+ "and dppg.provider_peer_group_id = dppgx.provider_peer_group_id and dppgx.year_end_date = dppg.year_end_date "
						+ "and ((vdpapp.bus_arr_number > 0) and (vdpapp.psych_act_to_date >= ?1) and (vdpapp.psych_act_from_date <= ?2) and "
						+ "(dppgx.provider_peer_group_id = ?3) and (dppgx.year_end_date = ?4) and (dppg.provider_peer_group_type = 'PROVI'))");
		Query psychiatricQuery = entityManager.createNativeQuery(psychiatricQueryString.toString());
		// Apply required arguments
		psychiatricQuery.setParameter(1, periodStartDate);
		psychiatricQuery.setParameter(2, periodEndDate);
		psychiatricQuery.setParameter(3, peerGroupId);
		psychiatricQuery.setParameter(4, yearEndDate);

		result = (BigDecimal) psychiatricQuery.getSingleResult();
		// Check that we got something back.. if not default is 0
		if (result == null) {
			result = BigDecimal.ZERO;
		}
		details.setPsychiatricPay(result);

		// ******************************
		// ***** Get Salary Per Pay *****
		// ******************************
		StringBuilder salaryPerPayQueryString = new StringBuilder(
				"SELECT sum(vdspp.amount_paid) FROM dss_provider_peer_group dppg, dss_provider_peer_group_xref dppgx, vw_dss_salary_per_pay vdspp "
						+ "WHERE dppgx.provider_number = vdspp.provider_number and dppgx.provider_type = vdspp.provider_type and dppg.provider_peer_group_id = dppgx.provider_peer_group_id "
						+ "and dppgx.year_end_date = dppg.year_end_date and ((vdspp.salary_sequence_number > 0) and (vdspp.salary_to_date >= ?1) and "
						+ "(vdspp.salary_from_date <= ?2) and (dppgx.provider_peer_group_id = ?3) and (dppgx.year_end_date = ?4) and (dppg.provider_peer_group_type = 'PROVI'))");
		Query salaryPerPayQuery = entityManager.createNativeQuery(salaryPerPayQueryString.toString());
		// Apply required arguments
		salaryPerPayQuery.setParameter(1, periodStartDate);
		salaryPerPayQuery.setParameter(2, periodEndDate);
		salaryPerPayQuery.setParameter(3, peerGroupId);
		salaryPerPayQuery.setParameter(4, yearEndDate);

		result = (BigDecimal) salaryPerPayQuery.getSingleResult();
		// Check that we got something back.. if not default is 0
		if (result == null) {
			result = BigDecimal.ZERO;
		}
		details.setSalaryPerPay(result);

		// **********************************
		// ***** Get Diagnostic Imaging *****
		// **********************************
		StringBuilder diagnosticImagingQueryString = new StringBuilder(
				"SELECT round(decode(count(distinct vdnppp.provider_number), 0, 0, sum(vdnppp.amount_paid) / count(distinct vdnppp.provider_number)), 0) as amount_paid_average, "
						+ "sum(vdnppp.amount_paid) as amount_paid FROM dss_provider_peer_group dppg, dss_provider_peer_group_xref dppgx, vw_dss_non_patient_per_pay vdnppp "
						+ "WHERE dppgx.provider_peer_group_id = dppg.provider_peer_group_id and dppgx.year_end_date = dppg.year_end_date and dppgx.provider_number = vdnppp.provider_number "
						+ "and dppgx.provider_type = vdnppp.provider_type and ((vdnppp.non_patient_per_pay_id > 0) and (vdnppp.start_date <= ?1) and (vdnppp.end_date >= ?2) and "
						+ "(dppgx.provider_peer_group_id = ?3) and (dppgx.year_end_date = ?4) and (dppg.provider_peer_group_type = 'PROVI'))");

		Query diagnosticImagingQuery = entityManager.createNativeQuery(diagnosticImagingQueryString.toString());
		// Apply required arguments
		diagnosticImagingQuery.setParameter(1, periodEndDate);
		diagnosticImagingQuery.setParameter(2, periodStartDate);
		diagnosticImagingQuery.setParameter(3, peerGroupId);
		diagnosticImagingQuery.setParameter(4, yearEndDate);

		// Execute query.. Returns: Diagnostic Imaging Average Paid, Diagnostic Imaging Paid
		@SuppressWarnings("unchecked")
		List<Object[]> diagImageResultList = diagnosticImagingQuery.getResultList();
		if (diagImageResultList != null && !diagImageResultList.isEmpty()) {
			Object[] diagResult = diagImageResultList.get(0);

			details.setDiagImagingAverage((BigDecimal) diagResult[0]);
			details.setDiagImaging((BigDecimal) diagResult[1]);
		} else {
			// No values were loaded.. Set to 0
			details.setDiagImagingAverage(BigDecimal.ZERO);
			details.setDiagImaging(BigDecimal.ZERO);
		}

		// ***********************************
		// ***** Get Adjustments Per Pay *****
		// ***********************************
		StringBuilder adjustmentsQueryString = new StringBuilder(
				"SELECT sum(vdapp.adjustment_amount) FROM DSS_PROVIDER_PEER_GROUP dppg, DSS_PROVIDER_PEER_GROUP_XREF dppgx, DSS_BOTTOM_LINE_ADJUSTMENT dbla, VW_DSS_ADJUSTMENT_PER_PAY vdapp "
						+ "WHERE dbla.ADJUSTMENT_ID = vdapp.ADJUSTMENT_ID and dppgx.PROVIDER_NUMBER = vdapp.PROVIDER_NUMBER and dppgx.PROVIDER_TYPE = vdapp.PROVIDER_TYPE "
						+ "and dppg.PROVIDER_PEER_GROUP_ID = dppgx.PROVIDER_PEER_GROUP_ID and dppgx.YEAR_END_DATE = dppg.YEAR_END_DATE and ((vdapp.adjustment_id > 0) AND (dbla.effective_from_date <= ?1) AND "
						+ "(dbla.effective_to_date >= ?2) AND (dbla.adjustment_type_code > 3) AND (dbla.adjustment_type_code NOT IN (7, 9, 12, 16, 17)) AND (dppgx.provider_peer_group_id = ?3) AND "
						+ "(dppgx.year_end_date = ?4) AND (dppg.provider_peer_group_type = 'PROVI')) AND (vdapp.payment_number IN (SELECT DISTINCT dp.payment_number FROM dss_payment dp "
						+ "WHERE dp.payment_date <= ?1 AND dp.payment_date >= ?2))");

		Query adjustmentsQuery = entityManager.createNativeQuery(adjustmentsQueryString.toString());
		adjustmentsQuery.setParameter(1, periodEndDate);
		adjustmentsQuery.setParameter(2, periodStartDate);
		adjustmentsQuery.setParameter(3, peerGroupId);
		adjustmentsQuery.setParameter(4, yearEndDate);

		result = (BigDecimal) adjustmentsQuery.getSingleResult();
		// Check that we got something back.. if not default is 0
		if (result == null) {
			result = BigDecimal.ZERO;
		}
		details.setBottomLineAdj(result);

		// *******************************
		// ***** Get Manual Deposits *****
		// *******************************
		StringBuilder manualDepositsQueryString = new StringBuilder(
				"SELECT sum(dmp.deposit_amount) FROM dss_provider_peer_group dppg, dss_provider_peer_group_xref dppgx, dss_manual_deposits dmp "
						+ "WHERE dppgx.provider_number = dmp.provider_number and dppgx.provider_type = dmp.provider_type and dppg.provider_peer_group_id = dppgx.provider_peer_group_id "
						+ "and dppgx.year_end_date = dppg.year_end_date and ((dmp.gl_number in (800, 803, 825, 830, 831, 832, 834, 836, 839, 840, 842, 844)) and "
						+ "(dmp.provider_type IN ('PH', 'OP')) and (dmp.date_received <= ?1) and (dmp.date_received >= ?2) and (dppgx.provider_peer_group_id = ?3) and "
						+ "(dppgx.year_end_date = ?4) and (dppg.provider_peer_group_type = 'PROVI'))");

		Query manualDepositsQuery = entityManager.createNativeQuery(manualDepositsQueryString.toString());
		// Apply required arguments
		manualDepositsQuery.setParameter(1, periodEndDate);
		manualDepositsQuery.setParameter(2, periodStartDate);
		manualDepositsQuery.setParameter(3, peerGroupId);
		manualDepositsQuery.setParameter(4, yearEndDate);

		result = (BigDecimal) manualDepositsQuery.getSingleResult();
		// Check that we got something back.. if not default is 0
		if (result == null) {
			result = BigDecimal.ZERO;
		}
		details.setManualDeposits(result);

		// *************************************
		// ***** Get Cheque Reconciliation *****
		// *************************************
		StringBuilder chequeReconQueryString = new StringBuilder(
				"SELECT sum(dcr.cheque_amount) FROM dss_provider_peer_group dppg, dss_provider_peer_group_xref dppgx, dss_cheque_reconciliation dcr "
						+ "WHERE dppgx.provider_number = dcr.provider_number and dppgx.provider_type = dcr.provider_type and dppg.provider_peer_group_id = dppgx.provider_peer_group_id "
						+ "and dppgx.year_end_date = dppg.year_end_date and ((dcr.gl_number IS NOT NULL) and (dcr.gl_number in (803, 825, 830, 831, 832, 834, 836, 839, 840, 842, 844)) and "
						+ "(dcr.provider_type IN ('PH', 'OP')) and (dcr.reconciliation_status NOT IN ('C', 'V')) and (dcr.payment_date <= ?1) and (dcr.payment_date >= ?2) and "
						+ "(dppgx.provider_peer_group_id = ?3) and (dppgx.year_end_date = ?4) and (dppg.provider_peer_group_type = 'PROVI'))");

		Query chequeReconQuery = entityManager.createNativeQuery(chequeReconQueryString.toString());
		// Apply required arguments
		chequeReconQuery.setParameter(1, periodEndDate);
		chequeReconQuery.setParameter(2, periodStartDate);
		chequeReconQuery.setParameter(3, peerGroupId);
		chequeReconQuery.setParameter(4, yearEndDate);

		result = (BigDecimal) chequeReconQuery.getSingleResult();
		// Check that we got something back.. if not default is 0
		if (result == null) {
			result = BigDecimal.ZERO;
		}
		details.setCheqReconciliation(result);

		logger.debug("InquiryDAO getPeerGroupPaymentDetails() : End");
		return details;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.InquiryDAO#getPractitionerAgeDistributionDetails(ca.medavie.nspp.audit.service
	 * .data.Practitioner)
	 */
	@Override
	public void getPractitionerAgeDistributionDetails(Practitioner aPractitioner) {

		logger.debug("InquiryDAO getPractitionerAgeDistributionDetails() : Begin");

		// Grab the values needed for query parameters from the Practitioner
		Long peerGroupId = aPractitioner.getPeerGroup().getPeerGroupID();
		Date yearEndDate = aPractitioner.getPeerGroup().getYearEndDate();
		Long periodId = aPractitioner.getPeerGroup().getProfilePeriod().getPeriodId();
		String shadowBillingIndicator = aPractitioner.getShadowBillingIndicator();
		Long numberOfPractitionerPatients = aPractitioner.getNumberOfPatients();
		Long numberOfPatientsInGroup = aPractitioner.getPeerGroup().getNumberOfPatientsInGroup();
		Long practitionerNumber = aPractitioner.getPractitionerNumber();
		String practitionerType = aPractitioner.getPractitionerType();

		// Query String for loading age ranks for PeerGroup
		StringBuilder groupAgeRanksQueryString = new StringBuilder(
				"SELECT dmpsa.age_rank, ROUND(DECODE(?1, 0, 0, (SUM(dmpsa.number_of_patients) / ?1) * 100), 1) as number_of_patients "
						+ "FROM dss_med_prov_summary_age dmpsa, dss_provider_peer_group dppg, ");

		// Query string for loading age ranks for the Practitioner
		StringBuilder practitionerAgeRanksQueryString = new StringBuilder(
				"SELECT dmpsa.age_rank, round(decode(?1, 0, 0, (sum(dmpsa.number_of_patients) / ?1) * 100), 1) as number_of_patients "
						+ "FROM dss_med_prov_summary_age dmpsa, ");

		// Determine if practitioner is of type shadow
		if (shadowBillingIndicator.equals("N")) {

			// Append the proper xref table
			groupAgeRanksQueryString.append("dss_provider_peer_group_xref gxref ");
			practitionerAgeRanksQueryString.append("dss_provider_peer_group_xref gxref ");

		} else {

			// Append the proper xref table
			groupAgeRanksQueryString.append("dss_shad_prov_peer_group_xref gxref ");
			practitionerAgeRanksQueryString.append("dss_shad_prov_peer_group_xref gxref ");
		}

		// Complete the SQL queries
		groupAgeRanksQueryString
				.append("WHERE gxref.provider_peer_group_id = ?2 and gxref.provider_number > 0 and gxref.provider_type > ' ' and gxref.year_end_date = ?3 and gxref.drop_indicator = 'N' "
						+ "and dppg.provider_peer_group_id = gxref.provider_peer_group_id and dppg.year_end_date = gxref.year_end_date "
						+ "and dppg.provider_peer_group_type = 'PROVI' and dmpsa.provider_number = gxref.provider_number and dmpsa.provider_type = gxref.provider_type and dmpsa.year_end_date = gxref.year_end_date "
						+ "and dmpsa.period_id = ?4 and dmpsa.shadow_billing_indicator = ?5 GROUP BY dmpsa.age_rank");

		practitionerAgeRanksQueryString
				.append("WHERE dmpsa.provider_number = gxref.provider_number and dmpsa.provider_type = gxref.provider_type and dmpsa.year_end_date = gxref.year_end_date "
						+ "and ((dmpsa.provider_number = ?2) and (dmpsa.provider_type = ?3) and (dmpsa.year_end_date = ?4) and (dmpsa.period_id = ?5) and "
						+ "(dmpsa.shadow_billing_indicator = ?6) and (gxref.provider_peer_group_id = ?7)) GROUP BY dmpsa.age_rank");

		Query groupAgeRanksQuery = entityManager.createNativeQuery(groupAgeRanksQueryString.toString());
		// Apply all required arguments
		groupAgeRanksQuery.setParameter(1, numberOfPatientsInGroup);
		groupAgeRanksQuery.setParameter(2, peerGroupId);
		groupAgeRanksQuery.setParameter(3, yearEndDate);
		groupAgeRanksQuery.setParameter(4, periodId);
		groupAgeRanksQuery.setParameter(5, shadowBillingIndicator);

		Query practitionerAgeRanksQuery = entityManager.createNativeQuery(practitionerAgeRanksQueryString.toString());
		// Apply all required arguments
		practitionerAgeRanksQuery.setParameter(1, numberOfPractitionerPatients);
		practitionerAgeRanksQuery.setParameter(2, practitionerNumber);
		practitionerAgeRanksQuery.setParameter(3, practitionerType);
		practitionerAgeRanksQuery.setParameter(4, yearEndDate);
		practitionerAgeRanksQuery.setParameter(5, periodId);
		practitionerAgeRanksQuery.setParameter(6, shadowBillingIndicator);
		practitionerAgeRanksQuery.setParameter(7, peerGroupId);

		@SuppressWarnings("unchecked")
		List<Object[]> groupAgeRanksResult = groupAgeRanksQuery.getResultList();
		@SuppressWarnings("unchecked")
		List<Object[]> practitionerAgeRanksResult = practitionerAgeRanksQuery.getResultList();

		// Holds age distribution objects that will be passed to the practitioner
		Map<Integer, AgeDistribution> ageDist = new TreeMap<Integer, AgeDistribution>();

		// Populate Group Age Ranks.
		for (Object[] o : groupAgeRanksResult) {
			// Get values
			String ageRank = (String) o[0];
			Integer ageKey = new Integer(0);
			BigDecimal patientCount = (BigDecimal) o[1];

			// Determine the rank key
			if (ageRank.contains("-")) {
				ageKey = new Integer(((String) o[0]).substring(0, ((String) o[0]).indexOf("-")));
			} else {
				ageKey = new Integer(((String) o[0]).substring(0, ((String) o[0]).indexOf("+")));
			}

			// Construct and populate new AgeDistribution object
			AgeDistribution ad = new AgeDistribution();
			ad.setAgeBracket(ageRank);
			ad.setGroupAverage(patientCount);

			// Add to map
			ageDist.put(ageKey, ad);
		}

		// Populate Practitioner Age Ranks
		for (Object[] o : practitionerAgeRanksResult) {
			// Get value
			String ageRank = (String) o[0];
			BigDecimal patientCount = (BigDecimal) o[1];

			// Determine the rank key
			Integer ageKey = new Integer(0);
			if (ageRank.contains("-")) {
				ageKey = new Integer(((String) o[0]).substring(0, ((String) o[0]).indexOf("-")));
			} else {
				ageKey = new Integer(((String) o[0]).substring(0, ((String) o[0]).indexOf("+")));
			}

			// Check if the ageRank already exists or not from Group Ranks
			if (!ageDist.containsKey(ageKey)) {
				// Construct and populate new AgeDistribution object
				AgeDistribution ad = new AgeDistribution();
				ad.setAgeBracket(ageRank);
				ad.setGroupAverage(patientCount);

				// Add new rank to map
				ageDist.put(ageKey, ad);
			} else {
				// Get the exist Rank and set the Practitioner value
				AgeDistribution existingAge = ageDist.get(ageKey);
				existingAge.setPractitionerAverage(patientCount);
			}
		}

		// Apply AgeDistribution map to Practitioner
		aPractitioner.setAgeDistribution(ageDist);

		logger.debug("InquiryDAO getPractitionerAgeDistributionDetails() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.InquiryDAO#getHealthServiceDrillDownDetails(ca.medavie.nspp.audit.service.data
	 * .Practitioner, ca.medavie.nspp.audit.service.data.HealthServiceCode)
	 */
	@Override
	public List<Object[]> getHealthServiceDrillDownDetails(Practitioner aPractitioner,
			HealthServiceCode aHealthServiceCode) {

		logger.debug("InquiryDAO getHealthServiceDrillDownDetails() : Begin");

		// Create the query string
		StringBuilder drillDownQueryString = new StringBuilder(
				"SELECT dmpads.NUMBER_OF_PATIENTS, dmpads.NUMBER_OF_SE, dmpads.TOTAL_UNITS, dmpads.TOTAL_AMOUNT_PAID, dmpads.NUMBER_OF_SE_PER_100, dmpads.AMOUNT_PAID_PER_100, "
						+ "dhsc.EFFECTIVE_FROM_DATE, dhsc.EFFECTIVE_TO_DATE, dhsc.DESCRIPTION, rtrim(dhsc.health_service_code) || rtrim(dhsc.qualifier_code) as health_service_code, "
						+ "dhs.MODIFIERS, dhs.IMPLICIT_MODIFIERS FROM DSS_HEALTH_SERVICE_CODE dhsc, DSS_HEALTH_SERVICE dhs, DSS_MED_PROV_ACT_DTL_SUMMARY dmpads "
						+ "WHERE dmpads.HEALTH_SERVICE_ID = dhs.HEALTH_SERVICE_ID and dhs.HEALTH_SERVICE_CODE = dhsc.HEALTH_SERVICE_CODE and dhs.QUALIFIER_CODE = dhsc.QUALIFIER_CODE "
						+ "and ((dmpads.provider_peer_group_id = ?1) and (dmpads.provider_number = ?2) and (dmpads.provider_type = ?3) and (dmpads.year_end_date = ?4) and "
						+ "(dmpads.period_id = ?5) and (dmpads.shadow_billing_indicator = ?6) and (dmpads.health_service_group_id = ?7) and (dhs.program_code = 'MC') and "
						+ "(dhsc.effective_to_date >= ?8) and (dhsc.effective_from_date <= ?9)) ORDER BY 10 ASC");

		// Create query and apply required arguments
		Query drillDownQuery = entityManager.createNativeQuery(drillDownQueryString.toString());
		drillDownQuery.setParameter(1, aPractitioner.getPeerGroup().getPeerGroupID());
		drillDownQuery.setParameter(2, aPractitioner.getPractitionerNumber());
		drillDownQuery.setParameter(3, aPractitioner.getPractitionerType());
		drillDownQuery.setParameter(4, aPractitioner.getPeerGroup().getYearEndDate());
		drillDownQuery.setParameter(5, aPractitioner.getPeerGroup().getProfilePeriod().getPeriodId());
		drillDownQuery.setParameter(6, aPractitioner.getShadowBillingIndicator());
		drillDownQuery.setParameter(7, aHealthServiceCode.getHealthServiceGroupId());
		drillDownQuery.setParameter(8, aPractitioner.getPeerGroup().getProfilePeriod().getPeriodStartDate());
		drillDownQuery.setParameter(9, aPractitioner.getPeerGroup().getProfilePeriod().getPeriodEndDate());

		// Execute query
		@SuppressWarnings("unchecked")
		List<Object[]> result = drillDownQuery.getResultList();

		logger.debug("InquiryDAO getHealthServiceDrillDownDetails() : End");
		return result;
	}

	private BigDecimal getTotalNumberOfReimbursements(Date aPeriodStartDate, Date aPeriodEndDate, Long aPeerGroupId,
			Date aYearEndDate) {

		logger.debug("InquiryDAO getTotalNumberOfReimbursements() : Begin");

		// Create the query string
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT COUNT(p.provider_number) AS number_of_ffs_rb_providers "
						+ "FROM dss_provider_2 p WHERE p.provider_number IN (SELECT a.provider_number "
						+ "FROM dss_provider_peer_group c, dss_provider_peer_group_xref b, dss_sessional_per_pay a "
						+ "WHERE a.bus_arr_number > 0 AND a.sessional_sequence_number > 0 AND a.effective_from_date <= ?1 "
						+ "AND a.effective_to_date >= ?2 AND b.provider_peer_group_id = ?3 AND b.provider_number = a.provider_number "
						+ "AND b.provider_type = a.provider_type AND b.year_end_date = ?4 "
						+ "AND c.provider_peer_group_id = b.provider_peer_group_id AND c.year_end_date = b.year_end_date "
						+ "UNION SELECT d.provider_number FROM dss_provider_peer_group c, dss_provider_peer_group_xref b, "
						+ "dss_psych_act_per_pay d WHERE d.bus_arr_number > 0 AND d.psych_act_from_date <= ?1 "
						+ "AND d.psych_act_to_date >= ?2 AND b.provider_peer_group_id = ?3 AND b.provider_number = d.provider_number "
						+ "AND b.provider_type = d.provider_type AND b.year_end_date = ?4 AND c.provider_peer_group_id = b.provider_peer_group_id "
						+ "AND c.year_end_date = b.year_end_date UNION SELECT e.provider_number FROM dss_provider_peer_group c, "
						+ "dss_provider_peer_group_xref b, dss_salary_per_pay e WHERE e.bus_arr_number > 0 AND e.salary_sequence_number > 0 "
						+ "AND e.salary_from_date <= ?1 AND e.salary_to_date >= ?2 AND b.provider_peer_group_id = ?3 "
						+ "AND b.provider_number = e.provider_number AND b.provider_type = e.provider_type AND b.year_end_date = ?4 "
						+ "AND c.provider_peer_group_id = b.provider_peer_group_id AND c.year_end_date = b.year_end_date)");

		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aPeriodEndDate);
		query.setParameter(2, aPeriodStartDate);
		query.setParameter(3, aPeerGroupId);
		query.setParameter(4, aYearEndDate);

		BigDecimal reimbursementCount = (BigDecimal) query.getSingleResult();

		logger.debug("InquiryDAO getTotalNumberOfReimbursements() : End");
		return reimbursementCount;
	}

	private BigDecimal getTotalNumberOfBottomLineAdjustments(Date aPeriodStartDate, Date aPeriodEndDate,
			Long aPeerGroupId, Date aYearEndDate) {

		logger.debug("InquiryDAO getTotalNumberOfBottomLineAdjustments() : Begin");

		// Create the query string
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT COUNT(p.provider_number) as num_of_ffs_bla_provs FROM dss_provider_2 p "
						+ "WHERE p.provider_number IN (SELECT a.provider_number FROM dss_provider_peer_group d, dss_provider_peer_group_xref c, "
						+ "dss_bottom_line_adjustment b, dss_adjustment_per_pay a WHERE a.adjustment_id > 0 AND b.adjustment_id = a.adjustment_id "
						+ "AND b.effective_from_date <= ?1 AND b.effective_to_date >= ?2 AND b.adjustment_type_code > 3 "
						+ "AND b.adjustment_type_code NOT IN (7, 9, 12, 16, 17) AND c.provider_peer_group_id = ?3 "
						+ "AND c.provider_number = a.provider_number AND c.provider_type = a.provider_type AND c.year_end_date = ?4 "
						+ "AND d.provider_peer_group_id = c.provider_peer_group_id AND d.year_end_date = c.year_end_date "
						+ "AND (a.payment_number IN (SELECT DISTINCT dss_payment.payment_number FROM dss_payment WHERE dss_payment.payment_date <= ?1 "
						+ "AND dss_payment.payment_date >= ?2)) UNION SELECT e.provider_number FROM dss_provider_peer_group c, dss_provider_peer_group_xref b, "
						+ "dss_manual_deposits e WHERE e.gl_number IN (800, 803, 825, 830, 831, 832, 834, 836, 839, 840, 842, 844) "
						+ "AND e.provider_type IN ('PH', 'OP') AND e.date_received <= ?1 AND e.date_received >= ?2 "
						+ "AND b.provider_peer_group_id = ?3 AND b.provider_number = e.provider_number AND b.provider_type = e.provider_type "
						+ "AND b.year_end_date = ?4 AND c.provider_peer_group_id = b.provider_peer_group_id AND c.year_end_date = b.year_end_date "
						+ "UNION SELECT f.provider_number FROM dss_provider_peer_group c, dss_provider_peer_group_xref b, dss_cheque_reconciliation f "
						+ "WHERE f.gl_number IS NOT NULL AND f.gl_number IN (803, 825, 830, 831, 832, 834, 836, 839, 840, 842, 844) "
						+ "AND f.provider_type IN ('PH', 'OP') AND f.reconciliation_status NOT IN ('C', 'V') AND f.payment_date <= ?1 "
						+ "AND f.payment_date >= ?2 AND b.provider_peer_group_id = ?3 AND b.provider_number = f.provider_number "
						+ "AND b.provider_type = f.provider_type AND b.year_end_date = ?4 AND c.provider_peer_group_id = b.provider_peer_group_id "
						+ "AND c.year_end_date = b.year_end_date)");

		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aPeriodEndDate);
		query.setParameter(2, aPeriodStartDate);
		query.setParameter(3, aPeerGroupId);
		query.setParameter(4, aYearEndDate);

		BigDecimal bottomLineAdjCount = (BigDecimal) query.getSingleResult();

		logger.debug("InquiryDAO getTotalNumberOfBottomLineAdjustments() : End");
		return bottomLineAdjCount;
	}

	/**
	 * Attempt to load the total Amount Paid under the provided Practitioner
	 * 
	 * @param aPractitioner
	 * @return totalAmount paid if any. Otherwise 0
	 */
	private BigDecimal getPractitionerTotalFFS(Practitioner aPractitioner) {

		// NOTE: For Total Amount Paid shadow indicator is always 'N'
		StringBuilder totalAmtPaidQueryString = new StringBuilder(
				"SELECT dmps.total_amount_paid FROM dss_med_prov_summary dmps WHERE dmps.provider_peer_group_id = ?1 "
						+ "and dmps.provider_number = ?2 and dmps.provider_type = ?3 and dmps.year_end_date = ?4 and dmps.period_id = ?5 and dmps.shadow_billing_indicator = 'N'");

		// Build query and apply arguments
		Query totalAmtPaidQuery = entityManager.createNativeQuery(totalAmtPaidQueryString.toString());
		totalAmtPaidQuery.setParameter(1, aPractitioner.getPeerGroup().getPeerGroupID());
		totalAmtPaidQuery.setParameter(2, aPractitioner.getPractitionerNumber());
		totalAmtPaidQuery.setParameter(3, aPractitioner.getPractitionerType());
		totalAmtPaidQuery.setParameter(4, aPractitioner.getPeerGroup().getYearEndDate());
		totalAmtPaidQuery.setParameter(5, aPractitioner.getPeerGroup().getProfilePeriod().getPeriodId());

		// Execute Query
		@SuppressWarnings("unchecked")
		List<Object> result = totalAmtPaidQuery.getResultList();
		BigDecimal totalAmt = BigDecimal.ZERO;

		// Apply value returned if available.
		if (!result.isEmpty()) {
			totalAmt = (BigDecimal) result.get(0);
		}

		return totalAmt;
	}
}