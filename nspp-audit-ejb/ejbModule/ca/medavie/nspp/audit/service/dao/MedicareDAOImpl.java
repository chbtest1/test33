package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.DssBusinessArrangement;
import ca.medavie.nspp.audit.service.data.DssIndividual2;
import ca.medavie.nspp.audit.service.data.DssMedAudBusArrExcl;
import ca.medavie.nspp.audit.service.data.DssMedAudCriteria;
import ca.medavie.nspp.audit.service.data.DssMedAudHlthSrvDesc;
import ca.medavie.nspp.audit.service.data.DssMedAudHlthSrvExcl;
import ca.medavie.nspp.audit.service.data.DssMedAudIndivExcl;
import ca.medavie.nspp.audit.service.data.DssMedAudIndivExclPK;
import ca.medavie.nspp.audit.service.data.DssMedSeAudit;
import ca.medavie.nspp.audit.service.data.DssMedSeAuditPK;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssBusinessArrangementExclusionMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssHealthServiceDescriptionMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssHealthServiceExclusionMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssIndividualExclusion;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssMedAuditCriteriaMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssMedicareALRMappedEntity;

public class MedicareDAOImpl implements MedicareDAO {

	/** Handles all DB connections and transactions */
	private EntityManager entityManager;

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(MedicareDAOImpl.class);

	public MedicareDAOImpl(EntityManager em) {
		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.MedicareDAO#getSearchedHealthServiceCriteriaDescriptions(ca.medavie.nspp.audit
	 * .service.data.HealthServiceCriteriaSearchCriteria)
	 */
	@Override
	public List<DssHealthServiceDescriptionMappedEntity> getSearchedHealthServiceCriteriaDescriptions(
			HealthServiceCriteriaSearchCriteria aSearchCriteria) {

		logger.debug("MedicareDAO getSearchedHealthServiceCriteriaDescriptions() : Begin");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

		StringBuilder queryString = new StringBuilder(
				"SELECT dhs.health_service_id, dhs.health_service_code, dhs.qualifier_code, dhs.effective_from_date, dhsc.description as service_description, "
						+ "dhs.modifiers, dhs.implicit_modifiers, dhsd.description as audit_description, dhs.category_code, dhsd.modified_by, dhsd.last_modified "
						+ "FROM dss_health_service dhs LEFT OUTER JOIN dss_med_aud_hlth_srv_desc dhsd ON dhs.health_service_id = dhsd.unique_number, dss_health_service_code dhsc "
						+ "WHERE (dhs.qualifier_code = dhsc.qualifier_code) and (dhs.health_service_code = dhsc.health_service_code) and (dhsc.effective_from_date = "
						+ "(SELECT max(dhsc2.effective_from_date) FROM dss_health_service_code dhsc2 WHERE (dhsc2.health_service_code = dhs.health_service_code) "
						+ "and (dhsc2.qualifier_code = dhs.qualifier_code))) ");

		// Append required AND statements
		if (aSearchCriteria.isModifierSet()) {
			queryString.append("AND UPPER (dhs.modifiers) LIKE CONCAT('%', CONCAT(?1,'%')) ");
		}
		if (aSearchCriteria.isImplicitModifierSet()) {
			queryString.append("AND UPPER (dhs.implicit_modifiers) LIKE CONCAT('%', CONCAT(?2,'%'))");
		}
		if (aSearchCriteria.isProgramSet()) {
			queryString.append("AND dhs.program_code = ?3 ");
		}
		if (aSearchCriteria.isEffectiveDateFromSet()) {
			queryString.append("AND dhs.effective_from_date <= to_date(?4,'yyyy/mm/dd') + .99999 ");
			queryString.append("AND (dhs.effective_to_date >= to_date(?4,'yyyy/mm/dd') ");
			queryString.append("OR dhs.effective_to_date is NULL) ");
		}
		if (aSearchCriteria.isHealthServiceCodeSet()) {
			// JTRAX-57 06-FEB-18 BCAINNE
			queryString.append("AND dhs.health_service_code LIKE CONCAT('%', CONCAT(UPPER(?5),'%'))");
		}
		if (aSearchCriteria.isQualifierSet()) {
			queryString.append("AND dhs.qualifier_code = ?6 ");
		}
		if (aSearchCriteria.isMsiFeeCodeSet()) {
			queryString.append("AND dhs.msi_fee_code = ?7 ");
		}
		if (aSearchCriteria.isHealthServiceDescSet()) {
			queryString.append("AND (dhsc.description like CONCAT('%', CONCAT(UPPER(?8),'%')) OR ");
			queryString.append("dhsc.alternate_wording like CONCAT('%', CONCAT(UPPER(?8),'%'))) ");
		}
		if (aSearchCriteria.isHealthServiceGroupSet()) {
			queryString.append("AND dhs.health_service_id in "
					+ "(select dhsgx.health_service_id from dss_health_service_group_xref dhsgx "
					+ "where dhsgx.health_service_group_id = ?9)");
		}

		// Append the Order by sql
		queryString.append("order by dhs.health_service_code ASC");

		// Create query and set any required parameters (Max results set to 500)
		Query query = entityManager.createNativeQuery(queryString.toString(),
				"DssHealthServiceDescriptionNativeQueryMapping");
		query.setMaxResults(500);

		// Apply required arguments
		if (aSearchCriteria.isModifierSet()) {
			query.setParameter(1, aSearchCriteria.getModifierString());
		}
		if (aSearchCriteria.isImplicitModifierSet()) {
			query.setParameter(2, aSearchCriteria.getImplicitModifierString());
		}
		if (aSearchCriteria.isProgramSet()) {
			query.setParameter(3, aSearchCriteria.getProgramCode());
		}
		if (aSearchCriteria.isEffectiveDateFromSet()) {
			query.setParameter(4, sdf.format(aSearchCriteria.getEffectiveFrom()));
		}
		if (aSearchCriteria.isHealthServiceCodeSet()) {
			query.setParameter(5, aSearchCriteria.getHealthServiceCode());
		}
		if (aSearchCriteria.isQualifierSet()) {
			query.setParameter(6, aSearchCriteria.getQualifier());
		}
		if (aSearchCriteria.isMsiFeeCodeSet()) {
			query.setParameter(7, aSearchCriteria.getMsiFeeCode());
		}
		if (aSearchCriteria.isHealthServiceDescSet()) {
			query.setParameter(8, aSearchCriteria.getHealthServiceCodeDescription());
		}
		if (aSearchCriteria.isHealthServiceGroupSet()) {
			query.setParameter(9, aSearchCriteria.getHealthServiceGroup());
		}

		// Execute query and return
		@SuppressWarnings("unchecked")
		List<DssHealthServiceDescriptionMappedEntity> result = query.getResultList();

		logger.debug("MedicareDAO getSearchedHealthServiceCriteriaDescriptions() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#saveHealthServiceCriteriaDescriptions(java.util.List)
	 */
	@Override
	public void saveHealthServiceCriteriaDescriptions(List<DssMedAudHlthSrvDesc> aListOfMedAudDesc) {
		logger.debug("MedicareDAO saveHealthServiceCriteriaDescriptions() : Begin");

		// Save each record
		for (DssMedAudHlthSrvDesc dssMedDesc : aListOfMedAudDesc) {
			entityManager.merge(dssMedDesc);
		}

		logger.debug("MedicareDAO saveHealthServiceCriteriaDescriptions() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getDssMedAudHlthSrvDeskByPK(java.lang.Long)
	 */
	@Override
	public DssMedAudHlthSrvDesc getDssMedAudHlthSrvDeskByPK(Long anId) {
		logger.debug("MedicareDAO getDssMedAudHlthSrvDeskByPK() : Begin");

		DssMedAudHlthSrvDesc dssMedDesc = entityManager.find(DssMedAudHlthSrvDesc.class, anId);

		logger.debug("MedicareDAO getDssMedAudHlthSrvDeskByPK() : End");
		return dssMedDesc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getHealthServiceCriteriaExclusions()
	 */
	@Override
	public List<DssHealthServiceExclusionMappedEntity> getHealthServiceCriteriaExclusions() {
		logger.debug("MedicareDAO getHealthServiceCriteriaExclusions() : Begin");

		// Create the query String
		StringBuilder queryString = new StringBuilder(
				"SELECT dhs.health_service_id, dhs.health_service_code, dhs.qualifier_code, dhs.program_code, excl.age_restriction, excl.gender_restriction, "
						+ "dhs.modifiers, dhs.implicit_modifiers, dhs.unit_formula, dhs.category_code, excl.modified_by, excl.last_modified "
						+ "FROM dss_health_service dhs, dss_med_aud_hlth_srv_excl excl WHERE excl.unique_number = dhs.health_service_id "
						+ "ORDER BY dhs.program_code ASC, 6 ASC, dhs.modifiers ASC, dhs.implicit_modifiers ASC");

		// Create query and execute
		Query query = entityManager.createNativeQuery(queryString.toString(),
				"DssHealthServiceExclusionNativeQueryMapping");
		@SuppressWarnings("unchecked")
		List<DssHealthServiceExclusionMappedEntity> result = query.getResultList();

		logger.debug("MedicareDAO getHealthServiceCriteriaExclusions() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#saveHealthServiceCriteriaExclusions(java.util.List)
	 */
	@Override
	public void saveHealthServiceCriteriaExclusions(List<DssMedAudHlthSrvExcl> aListOfExclusionsForSave) {
		logger.debug("MedicareDAO saveHealthServiceCriteriaExclusions() : Begin");

		// Save each record
		for (DssMedAudHlthSrvExcl exclusion : aListOfExclusionsForSave) {
			entityManager.merge(exclusion);
		}

		logger.debug("MedicareDAO saveHealthServiceCriteriaExclusions() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#deleteHealthServiceCriteriaExclusions(java.util.List)
	 */
	@Override
	public void deleteHealthServiceCriteriaExclusions(List<DssMedAudHlthSrvExcl> aListOfExclusionsForDelete) {
		logger.debug("MedicareDAO deleteHealthServiceCriteriaExclusions() : Begin");

		// Delete each record
		for (DssMedAudHlthSrvExcl exclusion : aListOfExclusionsForDelete) {
			entityManager.remove(exclusion);
		}

		logger.debug("MedicareDAO deleteHealthServiceCriteriaExclusions() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getDssMedAudHlthSrvExclByPK(java.lang.Long)
	 */
	@Override
	public DssMedAudHlthSrvExcl getDssMedAudHlthSrvExclByPK(Long anId) {
		logger.debug("MedicareDAO getDssMedAudHlthSrvExclByPK() : Begin");

		DssMedAudHlthSrvExcl dssMedExcl = entityManager.find(DssMedAudHlthSrvExcl.class, anId);

		logger.debug("MedicareDAO getDssMedAudHlthSrvExclByPK() : End");
		return dssMedExcl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getIndividualExclusions()
	 */
	@Override
	public List<DssIndividualExclusion> getIndividualExclusions() {
		logger.debug("MedicareDAO getIndividualExclusions() : Begin");

		// Create the query String
		StringBuilder queryString = new StringBuilder(
				"SELECT iex.health_card_number, ind.surname || ', ' || ind.first_name as name, iex.effective_from_date, iex.effective_to_date, "
						+ "iex.last_modified, iex.modified_by FROM dss_med_aud_indiv_excl iex, dss_individual_2 ind WHERE iex.health_card_number = ind.health_card_number "
						+ "ORDER BY 2");

		// Create query and execute
		Query query = entityManager.createNativeQuery(queryString.toString(),
				"DssIndividualExclusionNativeQueryMapping");
		@SuppressWarnings("unchecked")
		List<DssIndividualExclusion> result = query.getResultList();

		logger.debug("MedicareDAO getIndividualExclusions() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#saveIndividualExclusions(java.util.List)
	 */
	@Override
	public void saveIndividualExclusions(List<DssMedAudIndivExcl> aListOfIndExclToSave) {
		logger.debug("MedicareDAO saveIndividualExclusions() : Begin");

		// Save each record
		for (DssMedAudIndivExcl exclusion : aListOfIndExclToSave) {
			entityManager.merge(exclusion);
		}

		logger.debug("MedicareDAO saveIndividualExclusions() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getDssMedAudIndivExclByPK(ca.medavie.nspp.audit.service.data.
	 * DssMedAudIndivExclPK)
	 */
	@Override
	public DssMedAudIndivExcl getDssMedAudIndivExclByPK(DssMedAudIndivExclPK aPk) {
		logger.debug("MedicareDAO getDssMedAudIndivExclByPK() : Begin");

		DssMedAudIndivExcl dssMedExcl = entityManager.find(DssMedAudIndivExcl.class, aPk);

		logger.debug("MedicareDAO getDssMedAudIndivExclByPK() : End");
		return dssMedExcl;
	}

	@Override
	public void deleteIndividualExclusions(List<DssMedAudIndivExcl> aListOfIndExclToDelete) {
		logger.debug("MedicareDAO deleteIndividualExclusions() : Begin");

		for (DssMedAudIndivExcl excl : aListOfIndExclToDelete) {
			entityManager.remove(excl);
		}

		// Apply delete request immediately.
		entityManager.flush();
		entityManager.clear();

		logger.debug("MedicareDAO deleteIndividualExclusions() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getHealthCardNumberOwnerName(java.lang.String)
	 */
	@Override
	public String getHealthCardNumberOwnerName(String aHealthCardNumber) {
		logger.debug("MedicareDAO getHealthCardNumberOwnerName() : Begin");

		// Attempt to find a matching DSS_INDIVIDUAL_2 records by ID
		DssIndividual2 matchingRecord = entityManager.find(DssIndividual2.class, aHealthCardNumber);

		String result = "";
		if (matchingRecord != null) {
			// Populate name
			result = matchingRecord.getSurname() + ", " + matchingRecord.getFirstName();
		}

		logger.debug("MedicareDAO getHealthCardNumberOwnerName() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#saveMedicareAuditCriteria(ca.medavie.nspp.audit.service.data.
	 * DssMedAudCriteria)
	 */
	@Override
	public void saveMedicareAuditCriteria(DssMedAudCriteria anAuditCriteria) {
		logger.debug("MedicareDAO saveMedicareAuditCriteria() : Begin");

		// Submit changes to DB
		entityManager.merge(anAuditCriteria);

		logger.debug("MedicareDAO saveMedicareAuditCriteria() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getDssMedAudCriteriaByPk(java.lang.Long)
	 */
	@Override
	public DssMedAudCriteria getDssMedAudCriteriaByPk(Long aPk) {
		logger.debug("MedicareDAO getDssMedAudCriteriaByPk() : Begin");

		DssMedAudCriteria audCriteria = entityManager.find(DssMedAudCriteria.class, aPk);

		logger.debug("MedicareDAO getDssMedAudCriteriaByPk() : End");
		return audCriteria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getNextMedAudCriteriaSequenceValue()
	 */
	@Override
	public Long getNextMedAudCriteriaSequenceValue() {
		logger.debug("MedicareDAO getNextMedAudCriteriaSequenceValue() : Begin");

		// Simple select of next val from a sequence
		Query query = entityManager.createNativeQuery("select dss_med_audit_criteria_seq.nextval from dual");

		// Execute query and load result
		BigDecimal result = (BigDecimal) query.getSingleResult();

		logger.debug("MedicareDAO getNextMedAudCriteriaSequenceValue() : End");
		return result.longValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getPractitionerName(java.lang.Long, java.lang.String)
	 */
	@Override
	public String getPractitionerName(Long aPractitionerId, String aSubcategory) {

		logger.debug("MedicareDAO getPractitionerName() : Begin");

		Query query = entityManager
				.createNativeQuery("select x.first_name || ' ' || x.last_name || ' ' || x.middle_name || x.organization_name "
						+ "from dss_provider_2 x where x.provider_number = ?1 and x.provider_type = ?2");

		// Apply query arguments
		query.setParameter(1, aPractitionerId);
		query.setParameter(2, aSubcategory);

		// Execute the query and grab the results
		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();
		if (results.isEmpty()) {
			return null;
		}

		// Name found. Return it
		String result = results.get(0);

		logger.debug("MedicareDAO getPractitionerName() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getMedicarePractitionerAuditCriteria(java.lang.Long,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public DssMedAudCriteria getMedicarePractitionerAuditCriteria(Long aPractitionerId, String aSubcategory,
			String anAuditType) {

		logger.debug("MedicareDAO getMedicarePractitionerAuditCriteria() : Begin");

		// Create select query
		Query query = entityManager
				.createQuery("select dmac from DssMedAudCriteria dmac where dmac.auditType = :auditType and "
						+ "dmac.providerNumber = :providerNum and dmac.providerType = :providerType");

		// Apply parameter values
		query.setParameter("auditType", anAuditType);
		query.setParameter("providerNum", BigDecimal.valueOf(aPractitionerId)); // Need to convert Long to BigDecimal
		query.setParameter("providerType", aSubcategory);

		// Execute query
		@SuppressWarnings("unchecked")
		List<DssMedAudCriteria> results = query.getResultList();

		if (results.isEmpty()) {
			return null;
		}

		// only return the first result (Should only be one..)
		DssMedAudCriteria result = results.get(0);

		logger.debug("MedicareDAO getMedicarePractitionerAuditCriteria() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.MedicareDAO#getSearchPractitionerAuditCriterias(ca.medavie.nspp.audit.service
	 * .data.AuditCriteriaSearchCriteria, int, int)
	 */
	@Override
	public List<DssMedAuditCriteriaMappedEntity> getSearchPractitionerAuditCriterias(
			AuditCriteriaSearchCriteria aSearchCriteria, int startRow, int maxRows) {
		logger.debug("MedicareDAO getSearchPractitionerAuditCriterias() : Begin");

		// Create the query String
		StringBuilder queryString = new StringBuilder(
				"SELECT dmac.audit_criteria_id, dmac.audit_type, dmac.audit_from_date, dmac.audit_to_date, dmac.audit_from_payment_date,"
						+ "dmac.audit_to_payment_date, dmac.provider_number, dmac.provider_type, "
						+ "CASE WHEN dp2.organization_name IS NULL OR LENGTH(TRIM(dp2.organization_name)) IS NULL THEN dp2.last_name || ', ' || dp2.first_name || ' ' || substr(dp2.middle_name, 1, 1) ELSE dp2.organization_name END as provider_name ");

		// Append common portion of SQL along with parameter AND clauses
		queryString.append(constructAuditCriteriaSqlStatement(aSearchCriteria));

		// Append any sorting criteria..
		if (aSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, aSearchCriteria);
		}

		// Create query, set required parameters and and execute
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssMedAuditCriteriaNativeQueryMapping");

		// Append required arguments
		setAuditCriteriaSearchQueryParameters(query, aSearchCriteria);

		// Set pagination parameters
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		// Execute
		@SuppressWarnings("unchecked")
		List<DssMedAuditCriteriaMappedEntity> result = query.getResultList();

		logger.debug("MedicareDAO getSearchPractitionerAuditCriterias() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.MedicareDAO#getSearchedAuditCriteriaCount(ca.medavie.nspp.audit.service.data
	 * .AuditCriteriaSearchCriteria)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria aSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(1) ");

		// Append common portion of SQL along with parameter AND clauses
		queryString.append(constructAuditCriteriaSqlStatement(aSearchCriteria));

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Append required arguments
		setAuditCriteriaSearchQueryParameters(query, aSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the Audit Criteria search SQL
	 * 
	 * @param aSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructAuditCriteriaSqlStatement(AuditCriteriaSearchCriteria aSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"FROM dss_med_aud_criteria dmac, dss_provider_2 dp2 WHERE dmac.provider_number = dp2.provider_number and dmac.provider_type = dp2.provider_type ");

		// Append additional AND statements if parameters are set in the search criteria.
		if (aSearchCriteria.isAuditTypeSet()) {
			queryString.append("and dmac.audit_type = ?1 ");
		}
		if (aSearchCriteria.isAuditCriteriaIdSet()) {
			queryString.append("and dmac.audit_criteria_id = ?2 ");
		}
		if (aSearchCriteria.isPractitionerIdSet()) {
			queryString.append("and dmac.provider_number = ?3 ");
		}
		if (aSearchCriteria.isSubcategorySet()) {
			queryString.append("and dmac.provider_type = ?4 ");
		}
		if (aSearchCriteria.isPractitionerNameSet()) {
			queryString.append("and (UPPER(dp2.first_name) LIKE ?5 or UPPER(dp2.middle_name) LIKE ?5 or ");
			queryString.append("UPPER(dp2.last_name) LIKE ?5 or UPPER(dp2.organization_name) LIKE ?5 ");

			// parse string into individual words
			String[] arr = aSearchCriteria.getPractitionerName().split(" ");

			// The clause comes out like this for multiple names entered
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(dp2.last_name) LIKE ?5" + i + " or upper(dp2.middle_name) LIKE ?5" + i
							+ " or upper(dp2.first_name) LIKE ?5" + i + ") ");
				}

				queryString.append("))");
			}
		}
		return queryString;
	}

	/**
	 * Applies query arguments for Audit Criteria search/count
	 * 
	 * @param aQuery
	 * @param aSearchCriteria
	 */
	private void setAuditCriteriaSearchQueryParameters(Query aQuery, AuditCriteriaSearchCriteria aSearchCriteria) {

		if (aSearchCriteria.isAuditTypeSet()) {
			aQuery.setParameter(1, aSearchCriteria.getAuditType());
		}
		if (aSearchCriteria.isAuditCriteriaIdSet()) {
			aQuery.setParameter(2, aSearchCriteria.getAuditCriteriaId());
		}
		if (aSearchCriteria.isPractitionerIdSet()) {
			aQuery.setParameter(3, aSearchCriteria.getPractitionerId());
		}
		if (aSearchCriteria.isSubcategorySet()) {
			aQuery.setParameter(4, aSearchCriteria.getSubcategory());
		}
		if (aSearchCriteria.isPractitionerNameSet()) {
			aQuery.setParameter(5, "%" + aSearchCriteria.getPractitionerName().toUpperCase() + "%");

			// parse string into individual words
			String[] arr = aSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("5" + i);
					// remove any commas
					aQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getDefaultAuditCriteriaByType(java.lang.String)
	 */
	@Override
	public DssMedAudCriteria getDefaultAuditCriteriaByType(String anAuditType) {
		logger.debug("MedicareDAO getDefaultAuditCriteriaByType() : Begin");

		Query query = entityManager
				.createQuery("select dmac from DssMedAudCriteria dmac where dmac.auditType = :auditType and "
						+ "dmac.providerNumber is null and dmac.providerType is null");

		// Apply parameter values
		query.setParameter("auditType", anAuditType);

		// Execute query
		@SuppressWarnings("unchecked")
		List<DssMedAudCriteria> results = query.getResultList();

		// Return nothing if there are no results
		if (results.isEmpty()) {
			return null;
		}
		// Grab the result and return
		DssMedAudCriteria result = results.get(0);

		logger.debug("MedicareDAO getDefaultAuditCriteriaByType() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getMedicareALRRecords(ca.medavie.nspp.audit.service.data.
	 * MedicareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public List<DssMedicareALRMappedEntity> getMedicareALRRecords(
			MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria, int startRow, int maxRows) {
		logger.debug("MedicareDAOImpl--getMedicareALRRecords(): finding MedicareALRRecords ");

		StringBuilder queryString = new StringBuilder(
				"SELECT dms.payable_id, dmsa.se_number, dmsa.se_sequence_number, dmsa.response_tag_number, dmsa.audit_run_number, dmsa.provider_number, dmsa.provider_type, dmsa.health_card_number, "
						+ "dmsa.se_selection_date, dmsa.audit_letter_creation_date, dmsa.response_status_code, dmsa.number_of_times_sent, dmsa.chargeback_amount, dmsa.number_of_services_affected, "
						+ "dmsa.audit_note, dmsa.modified_by, dmsa.last_modified, cc.code_table_alpha_entry_desc as audit_type, dms.health_service_code, dms.service_start_date, dms.amount_paid, dhs.modifiers, "
						+ "dhs.implicit_modifiers, CASE WHEN dp2.organization_name IS NULL THEN dp2.last_name || ', ' || dp2.first_name || ' ' || substr(dp2.middle_name, 1, 1) ELSE dp2.organization_name END as provider_name, "
						+ "di2.surname || ', ' || di2.first_name as individual_name ");

		// Build the WHERE/FROM portion of the SQL
		queryString.append(constructMedicareALRSqlStatement(medicareALRSearchCriteria));

		// Add on order by SQL
		if (medicareALRSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, medicareALRSearchCriteria);
		} else {
			// Use the default sort
			queryString
					.append("ORDER BY CASE WHEN dmsa.audit_letter_creation_date IS NULL THEN 1 ELSE 0 END, dmsa.audit_letter_creation_date DESC");
		}

		// Create Query JPA object and set any required parameters
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssMedicareALRQueryMapping");

		// Apply arguments
		setALRSearchQueryParameters(query, medicareALRSearchCriteria);

		// Set pagination criteria
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		@SuppressWarnings("unchecked")
		List<DssMedicareALRMappedEntity> results = query.getResultList();
		logger.debug("MedicareDAOImpl--getDefaultAuditCriteriaByType(): " + results.size()
				+ " results found from {getDefaultAuditCriteriaByType}");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.MedicareDAO#getSearchedMedicareALRCount(ca.medavie.nspp.audit.service.data.
	 * MedicareAuditLetterResponseSearchCriteria)
	 */
	@Override
	public Integer getSearchedMedicareALRCount(MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria) {

		logger.debug("MedicareDAOImpl--getSearchedMedicareALRCount(): counting MedicareALRRecords ");

		StringBuilder queryString = new StringBuilder("SELECT COUNT(1) ");
		queryString.append(constructMedicareALRSqlStatement(medicareALRSearchCriteria));

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setALRSearchQueryParameters(query, medicareALRSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the ALR search SQL
	 * 
	 * @param medicareALRSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructMedicareALRSqlStatement(
			MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria) {

		StringBuilder queryString = new StringBuilder(
				"FROM dss_complete_audit dca, dss_med_se_audit dmsa, dss_medicare_se dms, dss_health_service dhs, dss_provider_2 dp2, "
						+ "dss_individual_2 di2, dss_code_table_alpha_entry cc "
						+ "WHERE dca.audit_run_number = dmsa.audit_run_number and dmsa.provider_number = dp2.provider_number and dmsa.provider_type = dp2.provider_type "
						+ "and dmsa.se_number = dms.se_number and dmsa.se_sequence_number = dms.se_sequence_number and dmsa.response_tag_number = dms.response_tag_number "
						+ "and dms.health_service_id = dhs.health_service_id and dmsa.health_card_number = di2.health_card_number and "
						+ "cc.code_table_alpha_number = 121 and cc.code_table_alpha_entry = dca.audit_type ");

		if (medicareALRSearchCriteria.isAuditRunNumberSet()) {
			queryString.append("AND dmsa.audit_run_number =?1 ");
		}

		if (medicareALRSearchCriteria.isSeNumberSet()) {
			queryString.append("AND (UPPER (dmsa.se_number) LIKE CONCAT('%', CONCAT(?2,'%'))) ");
		}
		if (medicareALRSearchCriteria.isSeSequenceNumberSet()) {
			queryString.append("AND dmsa.se_sequence_number =?3 ");

		}
		if (medicareALRSearchCriteria.isTagSet()) {
			queryString.append("AND dmsa.response_tag_number =?4 ");
		}

		if (medicareALRSearchCriteria.isProviderNumberSet()) {
			queryString.append("AND dmsa.provider_number =?5 ");
		}

		if (medicareALRSearchCriteria.isProviderTypeSet()) {
			queryString.append("AND dmsa.provider_type =?6 ");
		}

		if (medicareALRSearchCriteria.isProviderNameSet()) {
			queryString.append("AND (UPPER(dp2.organization_name) LIKE ?7 OR UPPER(dp2.last_name) LIKE ?7 ");
			queryString.append("OR UPPER(dp2.first_name) LIKE ?7 OR UPPER(dp2.middle_name) LIKE ?7 ");

			// parse string into individual words
			String[] arr = medicareALRSearchCriteria.getPractitionerName().split(" ");

			// The clause comes out like this for multiple names entered
			if (arr.length < 2) {
				// end the statement if there aren't multiple names
				queryString.append(") ");
			} else {
				queryString.append("or (");
				// add condition for each word
				for (int i = 0; i < arr.length; i++) {
					if (i > 0) {
						queryString.append(" and ");
					}

					queryString.append("(upper(dp2.last_name) LIKE ?7" + i + " or upper (dp2.middle_name) LIKE ?7" + i
							+ " or upper (dp2.first_name) LIKE ?7" + i + ") ");
				}

				queryString.append("))");
			}
		}

		if (medicareALRSearchCriteria.isHCNClientIDSet()) {
			queryString.append("AND (UPPER (dmsa.health_card_number) LIKE CONCAT('%', CONCAT(?8,'%'))) ");

		}
		if (medicareALRSearchCriteria.isAuditTypeSet()) {
			queryString.append("AND (UPPER (dca.audit_type) LIKE CONCAT('%', CONCAT(?9,'%'))) ");

		}
		if (medicareALRSearchCriteria.isResponseStatusSet()) {
			queryString.append("AND (UPPER (dmsa.response_status_code) LIKE CONCAT('%', CONCAT(?10,'%'))) ");
		}
		if (medicareALRSearchCriteria.isAuditFromDateSet()) {
			queryString.append("AND dmsa.audit_letter_creation_date >= ?11 ");
		}

		if (medicareALRSearchCriteria.isAuditToDateSet()) {
			queryString.append("AND dmsa.audit_letter_creation_date <= ?12 ");
		}
		// JTRAX-59 07-FEB-2017 BCAINNE
		if (medicareALRSearchCriteria.isPaymentFromDateSet()) {
			queryString.append("AND dms.payment_date >= ?13 ");
		}
		if (medicareALRSearchCriteria.isPaymentToDateSet()) {
			queryString.append("AND dms.payment_date <= ?14 ");
		}
		return queryString;
	}

	/**
	 * Applies query arguments for ALR search/count
	 * 
	 * @param aQuery
	 * @param medicareALRSearchCriteria
	 */
	private void setALRSearchQueryParameters(Query aQuery,
			MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria) {

		if (medicareALRSearchCriteria.isAuditRunNumberSet()) {
			aQuery.setParameter(1, medicareALRSearchCriteria.getAuditRunNumber());
		}

		if (medicareALRSearchCriteria.isSeNumberSet()) {
			aQuery.setParameter(2, medicareALRSearchCriteria.getSeNumber().replaceAll("\\s+", "").toUpperCase());
		}

		if (medicareALRSearchCriteria.isSeSequenceNumberSet()) {
			aQuery.setParameter(3, medicareALRSearchCriteria.getSeSequenceNumber());
		}

		if (medicareALRSearchCriteria.isTagSet()) {
			aQuery.setParameter(4, medicareALRSearchCriteria.getTag());
		}

		if (medicareALRSearchCriteria.isProviderNumberSet()) {
			aQuery.setParameter(5, medicareALRSearchCriteria.getPractitionerNumber());
		}

		if (medicareALRSearchCriteria.isProviderTypeSet()) {
			aQuery.setParameter(6, medicareALRSearchCriteria.getPractitionerType().replaceAll("\\s+", "").toUpperCase());
		}

		if (medicareALRSearchCriteria.isProviderNameSet()) {
			aQuery.setParameter(7, "%"
					+ medicareALRSearchCriteria.getPractitionerName().replaceAll("\\s+", "").toUpperCase() + "%");

			// parse string into individual words
			String[] arr = medicareALRSearchCriteria.getPractitionerName().split(" ");

			// add in individual word arguments if there are more then one
			if (arr.length > 1) {
				for (int i = 0; i < arr.length; i++) {
					int position = new Integer("7" + i);
					// remove any commas
					aQuery.setParameter(position, "%" + arr[i].trim().toUpperCase().replaceAll(",", "") + "%");
				}
			}
		}

		if (medicareALRSearchCriteria.isHCNClientIDSet()) {
			aQuery.setParameter(8, medicareALRSearchCriteria.getHcnClientID().replaceAll("\\s+", "").toUpperCase());
		}

		if (medicareALRSearchCriteria.isAuditTypeSet()) {
			aQuery.setParameter(9, medicareALRSearchCriteria.getAuditType().replaceAll("\\s+", "").toUpperCase());
		}
		if (medicareALRSearchCriteria.isResponseStatusSet()) {
			aQuery.setParameter(10, medicareALRSearchCriteria.getResponseStatus().replaceAll("\\s+", "").toUpperCase());
		}

		if (medicareALRSearchCriteria.isAuditFromDateSet()) {
			aQuery.setParameter(11, medicareALRSearchCriteria.getAuditFromDate());
		}
		if (medicareALRSearchCriteria.isAuditToDateSet()) {
			aQuery.setParameter(12, medicareALRSearchCriteria.getAuditToDate());
		}
		// JTRAX-59 07-FEB-2017 BCAINNE
		if (medicareALRSearchCriteria.isPaymentFromDateSet()) {
			aQuery.setParameter(13, medicareALRSearchCriteria.getPaymentFromDate());
		}
		if (medicareALRSearchCriteria.isPaymentToDateSet()) {
			aQuery.setParameter(14, medicareALRSearchCriteria.getPaymentToDate());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.MedicareDAO#getMedicareALRByID(ca.medavie.nspp.audit.service.data.DssMedSeAuditPK
	 * )
	 */
	@Override
	public DssMedSeAudit getMedicareALRByID(DssMedSeAuditPK aPK) {
		logger.debug("Start getMedicareALRByID in MedicareDAOImpl");

		DssMedSeAudit aDssMedSeAudit = entityManager.find(DssMedSeAudit.class, aPK);

		logger.debug("MedicareDAOImpl--getMedicareALRByID(): " + aDssMedSeAudit.toString());

		return aDssMedSeAudit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.MedicareDAO#saveMedicareALR(ca.medavie.nspp.audit.service.data.DssMedSeAudit)
	 */
	@Override
	public void saveMedicareALR(DssMedSeAudit aDssMedSeAudit) {
		logger.debug("MedicareDAOImpl -- saveMedicareALR(): Start persisting aDssMedSeAudit");
		entityManager.merge(aDssMedSeAudit);

		logger.debug("MedicareDAOImpl--saveMedicareALR(): " + " aDssMedSeAudit successfully saved! ");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getLastAuditDate(java.lang.Long)
	 */
	@Override
	public Date getLastAuditDate(Long anAuditCriteriaId) {

		logger.debug("MedicareDAO getLastAuditDate() : Begin");

		Query query = entityManager
				.createNativeQuery("select max(s.audit_letter_creation_date) from dss_med_se_audit s "
						+ "where s.audit_run_number = (select m.last_audit_run_number from dss_med_aud_criteria m "
						+ "where m.audit_criteria_id = ?1)");

		// Apply query argument
		query.setParameter(1, anAuditCriteriaId);

		// Execute the query
		Date result = (Date) query.getSingleResult();

		logger.debug("MedicareDAO getLastAuditDate() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getNumberOfLettersCreated(java.lang.Long, java.lang.String)
	 */
	@Override
	public Long getNumberOfLettersCreated(Long anAuditRunNumber, String anAuditType) {
		logger.debug("MedicareDAO getNumberOfLettersCreated() : Begin");

		// Simple select of next val from a sequence
		Query query = entityManager
				.createNativeQuery("SELECT c.number_of_letter_created FROM dss_complete_audit c WHERE c.audit_run_number = ?1 AND c.audit_type = ?2");

		// Apply query arguments
		query.setParameter(1, anAuditRunNumber);
		query.setParameter(2, anAuditType);

		// Execute query ( Catch any exception since there is a tiny chance there is no result found)
		BigDecimal result;
		try {
			result = (BigDecimal) query.getSingleResult();
		} catch (Exception e) {
			// No results found.. continuing on with the result of 0
			result = BigDecimal.ZERO;
		}

		logger.debug("MedicareDAO getNumberOfLettersCreated() : End");
		return result.longValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getBusinessArrangement(java.lang.Long)
	 */
	@Override
	public DssBusinessArrangement getBusinessArrangement(Long aBusinessArrangementNumber) {

		logger.debug("MedicareDAO getBusinessArrangement() : Begin");
		DssBusinessArrangement ba = entityManager.find(DssBusinessArrangement.class, aBusinessArrangementNumber);

		logger.debug("MedicareDAO getBusinessArrangement() : End");
		return ba;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getBusinessArrangementExclusions()
	 */
	@Override
	public List<DssBusinessArrangementExclusionMappedEntity> getBusinessArrangementExclusions() {
		logger.debug("MedicareDAO getBusinessArrangementExclusions() : Begin");

		// Construct query String
		StringBuilder queryString = new StringBuilder(
				"SELECT E.BUS_ARR_NUMBER, BA.BUS_ARR_DESCRIPTION, CASE "
						+ "WHEN E.PROVIDER_NUMBER IS NOT NULL AND E.PROVIDER_GROUP_ID IS NULL THEN "
						+ "(SELECT P.LAST_NAME || ', ' || P.FIRST_NAME || ' ' || P.MIDDLE_NAME FROM DSS_PROVIDER_2 P WHERE P.PROVIDER_NUMBER = E.PROVIDER_NUMBER AND P.PROVIDER_TYPE = E.PROVIDER_TYPE) "
						+ "WHEN E.PROVIDER_GROUP_ID IS NOT NULL AND E.PROVIDER_NUMBER IS NULL THEN "
						+ "(SELECT PG.PROVIDER_GROUP_NAME FROM DSS_PROVIDER_GROUP PG WHERE PG.PROVIDER_GROUP_ID = E.PROVIDER_GROUP_ID) END AS PROVIDER_NAME, "
						+ "E.PROVIDER_NUMBER, E.PROVIDER_TYPE, E.PROVIDER_GROUP_ID, E.LAST_MODIFIED, E.MODIFIED_BY FROM DSS_MED_AUD_BUS_ARR_EXCL E, DSS_BUSINESS_ARRANGEMENT BA "
						+ "WHERE E.BUS_ARR_NUMBER = BA.BUS_ARR_NUMBER ORDER BY E.LAST_MODIFIED DESC");

		// Create Query
		Query query = entityManager.createNativeQuery(queryString.toString(),
				"DssBusinessArrangementExclusionQueryMapping");
		// Execute query
		@SuppressWarnings("unchecked")
		List<DssBusinessArrangementExclusionMappedEntity> results = query.getResultList();

		logger.debug("MedicareDAO getBusinessArrangementExclusions() : End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.MedicareDAO#saveBusinessArrangementExclusion(ca.medavie.nspp.audit.service.
	 * data.DssMedAudBusArrExcl)
	 */
	@Override
	public void saveBusinessArrangementExclusion(DssMedAudBusArrExcl aDssMedAudBusArrExcl) {
		logger.debug("MedicareDAO saveBusinessArrangementExclusion() : Begin");

		// Save the exclusion record
		entityManager.merge(aDssMedAudBusArrExcl);

		logger.debug("MedicareDAO saveBusinessArrangementExclusion() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#deleteBusinessArrangementExclusions(java.util.List)
	 */
	@Override
	public void deleteBusinessArrangementExclusions(List<DssMedAudBusArrExcl> aListOfBAExclusionsForDelete) {
		logger.debug("MedicareDAO deleteBusinessArrangementExclusions() : Begin");

		for (DssMedAudBusArrExcl b : aListOfBAExclusionsForDelete) {
			// Delete the record
			entityManager.remove(b);
		}

		logger.debug("MedicareDAO deleteBusinessArrangementExclusions() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.MedicareDAO#getDssMedAudBusArrExclByPK(java.lang.Long)
	 */
	@Override
	public DssMedAudBusArrExcl getDssMedAudBusArrExclByPK(Long aBaExclusionId) {
		logger.debug("MedicareDAO getBusinessArrangementExclusion() : Begin");
		DssMedAudBusArrExcl excl = entityManager.find(DssMedAudBusArrExcl.class, aBaExclusionId);
		logger.debug("MedicareDAO getBusinessArrangementExclusion() : End");

		return excl;
	}
}
