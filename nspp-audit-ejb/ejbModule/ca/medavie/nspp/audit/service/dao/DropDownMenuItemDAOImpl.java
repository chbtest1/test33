package ca.medavie.nspp.audit.service.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.DssCodeTableAlphaEntry;
import ca.medavie.nspp.audit.service.data.DssCodeTableEntry;
import ca.medavie.nspp.audit.service.data.DssHealthServiceGroup;
import ca.medavie.nspp.audit.service.data.DssProfilePeriod;
import ca.medavie.nspp.audit.service.data.DssProviderType;
import ca.medavie.nspp.audit.service.data.QueryConstants;

/***
 * 
 * DAO class for getting some data list constants
 * 
 * **/
public class DropDownMenuItemDAOImpl extends QueryConstants implements DropDownMenuItemDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(DropDownMenuItemDAOImpl.class);

	public EntityManager entityManager;

	/**
	 * Constructor for OutlierCriteria with EntityManager provided
	 * 
	 * @param em
	 */
	public DropDownMenuItemDAOImpl(EntityManager em) {

		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getDssProviderTypes()
	 */
	@Override
	public List<DssProviderType> getSubCategoryDataEntries() {

		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_SUBCATEGORY_QUERY);

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssProviderType> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getDssProviderTypes(): there are " + results.size() + " item found");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getDssAlphaEntries()
	 */
	@Override
	public List<DssCodeTableAlphaEntry> getSpecialtyDataEntries() {

		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_SPECIALTY_QUERY);

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssCodeTableAlphaEntry> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getDssAlphaEntries(): there are " + results.size() + " items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getDssAuditRXCodeEntries()
	 */
	@Override
	public List<DssCodeTableEntry> getDssAuditRXCodeEntries() {

		LOGGER.debug("DropDownMenuItemDAOImpl--getDssAuditRXCodeEntries(): finding Audit RXs");

		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_ENTRY_GET_AUDIT_RX_TYPES);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssCodeTableEntry> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getDssAuditRXCodeEntries(): there are " + results.size()
				+ "  items found ");

		return results;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getDssAuditNonRXCodeEntries()
	 */
	@Override
	public List<DssCodeTableEntry> getDssAuditNonRXCodeEntries() {
		LOGGER.debug("DropDownMenuItemDAOImpl--getDssAuditNonRXCodeEntries(): finding Audit NON RX type from DssCodeEntry");

		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_ENTRY_GET_NON_AUDIT_RX_TYPES);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssCodeTableEntry> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getDssAuditNonRXCodeEntries(): there are " + results.size()
				+ " items found ");

		return results;
	}

	@Override
	public List<String> getContractTownNames() {
		LOGGER.debug("DropDownMenuItemDAOImpl--getContractTownNames(): Begin");

		// Create query string
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT p.placename FROM dss_placename p ORDER BY p.placename ASC");

		// Create query and execute
		Query query = entityManager.createNativeQuery(queryString.toString());
		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getContractTownNames(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getQualifierCodes()
	 */
	@Override
	public List<String> getQualifierCodes() {
		LOGGER.debug("DropDownMenuItemDAOImpl--getQualifierCodes(): finding quilifiers");

		Query query = entityManager.createNamedQuery(DSS_HEALTH_SERVICE_CODE_QUALIFIER_QUERY);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getQualifierCodes(): there are " + results.size() + " items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#geAuditSourceMedCodes()
	 */
	@Override
	public List<String> geAuditSourceMedCodes() {
		LOGGER.debug("DropDownMenuItemDAOImpl--geAuditSourceMedCodes(): finding audit source NON PHARMs");

		Query query = entityManager.createNamedQuery(DSS_HEALTH_SERVICE_CODE_HEALTH_AUDIT_SOURCE_MED_QUERY);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--geAuditSourceMedCodes(): there are " + results.size() + "  items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#geAuditSourcePharmCodes()
	 */
	@Override
	public List<String> geAuditSourcePharmCodes() {
		LOGGER.debug("DropDownMenuItemDAOImpl--geAuditSourcePharmCodes(): finding audit source PHARMs");

		Query query = entityManager.createNamedQuery(DSS_HEALTH_SERVICE_CODE_HEALTH_AUDIT_SOURCE_PHARM_QUERY);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--geAuditSourcePharmCodes(): there are " + results.size()
				+ " items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#geAuditPharmTypeCodes()
	 */
	@Override
	public List<String> geAuditPharmTypeCodes() {
		LOGGER.debug("DropDownMenuItemDAOImpl--geAuditPharmTypeCodes(): finding audit PHARM Types ");

		Query query = entityManager.createNamedQuery(DSS_HEALTH_SERVICE_CODE_HEALTH_PHARM_TYPE_QUERY);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--geAuditPharmTypeCodes(): there are " + results.size() + "  items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getGroupTypeCodes()
	 */
	@Override
	public List<String> getGroupTypeCodes() {
		LOGGER.debug("DropDownMenuItemDAOImpl--getGroupTypeCodes(): finding Provider Group Types ");

		Query query = entityManager.createNamedQuery(DSS_PROVIDER_GROUP_TYPE_QUERY);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getGroupTypeCodes(): there are " + results.size() + "  items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getPeriodYearEndDates()
	 */
	@Override
	public List<Date> getPeriodYearEndDates() {
		LOGGER.debug("getPeriodYearEndDates(): Begin ");

		// Create query string
		StringBuilder queryString = new StringBuilder("SELECT DISTINCT dpp.year_end_date FROM dss_profile_period dpp "
				+ "WHERE (dpp.period_type = 'PROVI') ORDER BY dpp.year_end_date DESC");

		Query query = entityManager.createNativeQuery(queryString.toString());
		// Execute the query
		@SuppressWarnings("unchecked")
		List<Date> results = query.getResultList();

		LOGGER.debug("getPeriodYearEndDates(): End ");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getPeriodIdDropdown()
	 */
	@Override
	public List<DssProfilePeriod> getPeriodIdDropdown() {
		LOGGER.debug("getPeriodIdDropdown(): Begin ");

		Query query = entityManager.createQuery("select dpp from DssProfilePeriod dpp order by dpp.periodId desc");

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssProfilePeriod> results = query.getResultList();
		// Detach objects from EM before passing up the chain. Removes the auto save affect
		for (DssProfilePeriod dpp : results) {
			entityManager.detach(dpp);
		}

		LOGGER.debug("getPeriodIdDropdown(): End ");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getDssUnitValueLevelCodes()
	 */
	@Override
	public List<Long> getDssUnitValueLevelCodes() {
		LOGGER.debug("DropDownMenuItemDAOImpl--getDssUnitValueLevelCodes(): finding dss Unit Value Levels ");

		Query query = entityManager.createNamedQuery(DSS_UNIT_VALUE_LEVEL_QUERY);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<Long> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getDssUnitValueLevelCodes(): there are " + results.size()
				+ "  items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getDssUnitValueDescsCodes()
	 */
	@Override
	public List<DssCodeTableEntry> getDssUnitValueDescsCodes() {
		LOGGER.debug("DropDownMenuItemDAOImpl--getDssUnitValueDescsCodes(): finding dss Unit Value Levels ");

		Query query = entityManager.createNamedQuery(DSS_UNIT_VALUE_DESC_QUERY);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssCodeTableEntry> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getDssUnitValueDescsCodes(): there are " + results.size()
				+ "  items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getHsModifers()
	 */
	@Override
	/**
	 * 
	 * Query pulled from power builder for this part
	 * 
	 *   SELECT dss_modifier_value.modifier_type || '=' ||
	     dss_modifier_value.modifier_value as modifiers,
	     dss_modifier_value.modifier_type ||
	     dss_modifier_value.modifier_value as modifier_type_and_value
	FROM dss_modifier_type, dss_modifier_value
	WHERE (dss_modifier_type.modifier_type =
	     dss_modifier_value.modifier_type)
	 and ((dss_modifier_type.implicit_modifier_indicator = 'N') and
	     (dss_modifier_value.effective_from_date <=
	     trunc(SYSDATE) + .99999) and
	     (dss_modifier_value.effective_to_date >= trunc(SYSDATE)))
	ORDER BY 1 ASC
	 * */
	public List<Object[]> getHsModifers() {
		LOGGER.debug("DropDownMenuItemDAO : getHsModifers(): Begin");

		Calendar cal = Calendar.getInstance(); // represents right now, i.e. today's date
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);

		Date todayMidnight = cal.getTime();
		Date today = Calendar.getInstance().getTime();

		// JPQL issues parameter for todayMidnight as something like "2015-11-17", could it be a bug?? we actually
		// setting todayMidnight as 2015-11-17 23:59:59
		Query query = entityManager
				.createQuery(" select dmfv.id.modifierValue,dmfv.id.modifierType FROM DssModifierValue dmfv, "
						+ "DssModifierType dmft where dmfv.id.modifierType = dmft.id.modifierType and dmft.implicitModifierIndicator='N' "
						+ "and dmfv.id.effectiveFromDate <= :todayMidnight" + " and dmfv.effectiveToDate >= :today");

		query.setParameter("todayMidnight", todayMidnight);
		query.setParameter("today", today);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAO : getHsModifers(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getHsImplicitModifiers()
	 */
	@Override
	/**
	 * 
	 * SQL query pulled from powerbuilder as follows:
	 * 
	 * 
	 *   SELECT dss_modifier_value.modifier_type || '=' ||
	     dss_modifier_value.modifier_value as implicit_modifiers,
	     dss_modifier_value.modifier_type ||
	     dss_modifier_value.modifier_value as modifier_type_and_value
	FROM dss_modifier_type, dss_modifier_value
	WHERE (dss_modifier_type.modifier_type =
	     dss_modifier_value.modifier_type)
	 and ((dss_modifier_type.implicit_modifier_indicator = 'Y') and
	     (dss_modifier_value.effective_from_date <=
	     trunc(SYSDATE) + .99999) and
	     (dss_modifier_value.effective_to_date >= trunc(SYSDATE)))
	ORDER BY 1 ASC
	 * **/
	public List<Object[]> getHsImplicitModifiers() {
		LOGGER.debug("DropDownMenuItemDAO : getHsImplicitModifiers(): Begin");

		Calendar cal = Calendar.getInstance(); // represents right now, i.e. today's date
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);

		Date todayMidnight = cal.getTime();
		Date today = Calendar.getInstance().getTime();

		// JPQL issues parameter for todayMidnight as something like "2015-11-17", could it be a bug?? we actually
		// setting todayMidnight as 2015-11-17 23:59:59
		Query query = entityManager
				.createQuery(" select dmfv.id.modifierValue,dmfv.id.modifierType FROM DssModifierValue dmfv, "
						+ "DssModifierType dmft where dmfv.id.modifierType = dmft.id.modifierType and dmft.implicitModifierIndicator='Y' "
						+ "and dmfv.id.effectiveFromDate <= :todayMidnight" + " and dmfv.effectiveToDate >= :today");

		query.setParameter("todayMidnight", todayMidnight);
		query.setParameter("today", today);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAO : getHsImplicitModifiers(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getHsProgramCodes()
	 */
	@Override
	public List<Object[]> getHsProgramCodes() {
		LOGGER.debug("DropDownMenuItemDAO : getHsProgramCodes(): Begin");
		// Query query = entityManager.createQuery(" select distinct dhs.programCode FROM DssHealthService dhs ");

		Query query = entityManager
				.createQuery(" select dpg.programCode, dpg.programName from DssProgram dpg where dpg.programCode in (select distinct dhs.programCode FROM DssHealthService dhs)");

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAO : getHsProgramCodes(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getHsGroupCodes()
	 */
	@Override
	public List<DssHealthServiceGroup> getHsGroupCodes() {
		LOGGER.debug("DropDownMenuItemDAO : getHsGroupCodes(): Begin");
		String queryString = "SELECT dhs.health_service_group_id, dhs.health_service_group_name, dhs.health_service_group_type, dhs.modified_by, dhs.last_modified"
				+ " FROM dss_health_service_group dhs " + " ORDER BY dhs.health_service_group_name";
		Query query = entityManager.createNativeQuery(queryString, DssHealthServiceGroup.class);
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssHealthServiceGroup> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAO : getHsGroupCodes(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getHsQualifiersCodes()
	 */
	@Override
	/** 
	 * SQL query pulled from powerbuilder as follows:
	 * 
	 * SELECT dss_modifier_value.modifier_type || '=' || dss_modifier_value.modifier_value as implicit_modifiers,
	 * dss_modifier_value.modifier_type || dss_modifier_value.modifier_value as modifier_type_and_value FROM
	 * dss_modifier_type, dss_modifier_value WHERE (dss_modifier_type.modifier_type = dss_modifier_value.modifier_type)
	 * and ((dss_modifier_type.implicit_modifier_indicator = 'Y') and (dss_modifier_value.effective_from_date <=
	 * trunc(SYSDATE) + .99999) and (dss_modifier_value.effective_to_date >= trunc(SYSDATE))) ORDER BY 1 ASC
	 * 
	 * **/
	public List<String> getHsQualifiersCodes() {
		LOGGER.debug("DropDownMenuItemDAO : getHsQualifiersCodes(): Begin");
		Query query = entityManager.createQuery(" select distinct dhs.qualifierCode FROM DssHealthService dhs");
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAO : getHsQualifiersCodes(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getDenticareAuditTypeCodes()
	 */
	@Override
	public List<DssCodeTableAlphaEntry> getDenticareAuditTypeCodes() {
		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_ALPHA_ENTRY_DENTICARE_AUDIT_TYPE);

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssCodeTableAlphaEntry> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getDenticareAuditTypeCodes(): there are " + results.size()
				+ " items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getMedicareAuditTypeCodes()
	 */
	@Override
	public List<DssCodeTableAlphaEntry> getMedicareAuditTypeCodes() {
		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_ALPHA_ENTRY_MEDICARE_AUDIT_TYPE);

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssCodeTableAlphaEntry> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getMedicareAuditTypeCodes(): there are " + results.size()
				+ " items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getPharmacareAuditTypeDropDown()
	 */
	@Override
	public List<DssCodeTableAlphaEntry> getPharmacareAuditTypeDropDown() {

		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_ALPHA_ENTRY_PHARM_AUDIT_TYPE);

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssCodeTableAlphaEntry> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--getPharmacareAuditTypeDropDown(): there are " + results.size()
				+ " items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getResponseStatusCodes()
	 */
	@Override
	public List<DssCodeTableAlphaEntry> getResponseStatusCodes() {
		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_ALPHA_ENTRY_RESPONSE_STATUS);

		// Execute the Query
		@SuppressWarnings("unchecked")
		List<DssCodeTableAlphaEntry> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAOImpl--ResponsStatusCodes(): there are " + results.size() + " items found ");

		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getGlNumbers()
	 */
	@Override
	public List<String> getGlNumbers() {
		LOGGER.debug("DropDownMenuItemDAO : getGlNumbers(): Begin");
		Query query = entityManager
				.createNativeQuery(" SELECT gl.gl_number||'-'||TRIM(gl.gl_number_description) FROM dss_gl_number gl ORDER BY TRIM(gl.gl_number_description)");
		// Execute the Query
		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAO : getGlNumbers(): End");
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getPayeeTypeDropDown()
	 */
	@Override
	public List<DssCodeTableAlphaEntry> getPayeeTypeDropDown() {
		LOGGER.debug("DropDownMenuItemDAO : getPayeeTypeDropDown(): Begin");

		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_ALPHA_ENTRY_PAYEE_TYPES);

		// Execute query
		@SuppressWarnings("unchecked")
		List<DssCodeTableAlphaEntry> result = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAO : getPayeeTypeDropDown(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getPaymentResponsibilityDropDown()
	 */
	@Override
	public List<DssCodeTableAlphaEntry> getPaymentResponsibilityDropDown() {
		LOGGER.debug("DropDownMenuItemDAO : getPaymentResponsibilityDropDown(): Begin");

		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_ALPHA_ENTRY_PAY_REPS);

		// Execute query
		@SuppressWarnings("unchecked")
		List<DssCodeTableAlphaEntry> result = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAO : getPaymentResponsibilityDropDown(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getGeneralAuditCriteriaTypeDropDown()
	 */
	@Override
	public List<DssCodeTableAlphaEntry> getGeneralAuditCriteriaTypeDropDown() {
		LOGGER.debug("DropDownMenuItemDAO : getGeneralAuditCriteriaTypeDropDown(): Begin");

		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_ALPHA_ENTRY_GENERAL_AUDIT_CRITERIA_TYPE);

		// Execute query
		@SuppressWarnings("unchecked")
		List<DssCodeTableAlphaEntry> result = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAO : getGeneralAuditCriteriaTypeDropDown(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getPharmacareAuditCriteriaTypeDropDown()
	 */
	@Override
	public List<DssCodeTableAlphaEntry> getPharmacareAuditCriteriaTypeDropDown() {
		LOGGER.debug("DropDownMenuItemDAO : getPharmacareAuditCriteriaTypeDropDown(): Begin");

		Query query = entityManager.createNamedQuery(DSS_CODE_TABLE_ALPHA_ENTRY_PHARM_AUDIT_CRITERIA_TYPE);

		// Execute query
		@SuppressWarnings("unchecked")
		List<DssCodeTableAlphaEntry> result = query.getResultList();

		LOGGER.debug("DropDownMenuItemDAO : getPharmacareAuditCriteriaTypeDropDown(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DropDownMenuItemDAO#getUniqueHSEffectiveDates()
	 */
	@Override
	public List<Date> getUniqueHSEffectiveDates() {
		LOGGER.debug("getUniqueHSEffectiveDates(): Begin ");

		// Create query string
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dhs.effective_from_date FROM dss_health_service dhs ORDER BY dhs.effective_from_date DESC");

		Query query = entityManager.createNativeQuery(queryString.toString());
		// Execute the query
		@SuppressWarnings("unchecked")
		List<Date> results = query.getResultList();

		LOGGER.debug("getUniqueHSEffectiveDates(): End " + results.size() + " items found");
		return results;
	}
}
