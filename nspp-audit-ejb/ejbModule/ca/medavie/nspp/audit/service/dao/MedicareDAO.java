package ca.medavie.nspp.audit.service.dao;

import java.util.Date;
import java.util.List;

import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DssBusinessArrangement;
import ca.medavie.nspp.audit.service.data.DssMedAudBusArrExcl;
import ca.medavie.nspp.audit.service.data.DssMedAudCriteria;
import ca.medavie.nspp.audit.service.data.DssMedAudHlthSrvDesc;
import ca.medavie.nspp.audit.service.data.DssMedAudHlthSrvExcl;
import ca.medavie.nspp.audit.service.data.DssMedAudIndivExcl;
import ca.medavie.nspp.audit.service.data.DssMedAudIndivExclPK;
import ca.medavie.nspp.audit.service.data.DssMedSeAudit;
import ca.medavie.nspp.audit.service.data.DssMedSeAuditPK;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssBusinessArrangementExclusionMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssHealthServiceDescriptionMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssHealthServiceExclusionMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssIndividualExclusion;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssMedAuditCriteriaMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssMedicareALRMappedEntity;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;

/**
 * Interface of the Medicare DAO layer
 */
public interface MedicareDAO {

	/**
	 * Loads matching DssHealthServiceDescriptionMappedEntity records for Description update
	 * 
	 * Data loaded from:
	 * 
	 * Query:
	 * 
	 * SELECT dhs.health_service_id, dhs.health_service_code, dhs.qualifier_code, dhs.effective_from_date,
	 * dhsc.description as service_description, dhs.modifiers, dhs.implicit_modifiers, dhsd.description as
	 * audit_description, dhs.category_code, dhsd.modified_by, dhsd.last_modified FROM dss_health_service dhs LEFT OUTER
	 * JOIN dss_med_aud_hlth_srv_desc dhsd ON dhs.health_service_id = dhsd.unique_number, dss_health_service_code dhsc
	 * WHERE (dhs.qualifier_code = dhsc.qualifier_code) and (dhs.health_service_code = dhsc.health_service_code) and
	 * (dhsc.effective_from_date = (SELECT max(dhsc2.effective_from_date) FROM dss_health_service_code dhsc2 WHERE
	 * (dhsc2.health_service_code = dhs.health_service_code) and (dhsc2.qualifier_code = dhs.qualifier_code))) order by
	 * dhs.health_service_code ASC
	 * 
	 * @param aSearchCriteria
	 * @return list of matching DssHealthServiceDescriptionMappedEntity for Description update
	 */
	public List<DssHealthServiceDescriptionMappedEntity> getSearchedHealthServiceCriteriaDescriptions(
			HealthServiceCriteriaSearchCriteria aSearchCriteria);

	/**
	 * Saves the changes made to the DssMedAudHlthSrvDesc description.
	 * 
	 * @param aListOfMedAudDesc
	 *            - DssMedAudHlthSrvDesc to be persisted
	 */
	public void saveHealthServiceCriteriaDescriptions(List<DssMedAudHlthSrvDesc> aListOfMedAudDesc);

	/**
	 * Attempts to load the DssMedAudHlthSrvDesc with matching ID. Null is returned if nothing is found.
	 * 
	 * @param anId
	 *            - ID (PK) for DssMedAudHlthSrvDesc
	 * @return matching DssMedAudHlthSrvDesc or null
	 */
	public DssMedAudHlthSrvDesc getDssMedAudHlthSrvDeskByPK(Long anId);

	/**
	 * Loads the current DssHealthServiceExclusionMappedEntity records
	 * 
	 * @return current DssHealthServiceExclusionMappedEntity records
	 */
	public List<DssHealthServiceExclusionMappedEntity> getHealthServiceCriteriaExclusions();

	/**
	 * Saves the new and updated DssMedAudHlthSrvExcl exclusions to the database
	 * 
	 * @param aListOfExclusionsForSave
	 */
	public void saveHealthServiceCriteriaExclusions(List<DssMedAudHlthSrvExcl> aListOfExclusionsForSave);

	/**
	 * Deletes the specified DssMedAudHlthSrvExcl from the database
	 * 
	 * @param aListOfExclusionsForDelete
	 */
	public void deleteHealthServiceCriteriaExclusions(List<DssMedAudHlthSrvExcl> aListOfExclusionsForDelete);

	/**
	 * Attempts to load the DssMedAudHlthSrvExcl with matching ID. Null is returned if nothing is found.
	 * 
	 * @param anId
	 *            - ID (PK) for DssMedAudHlthSrvExcl
	 * @return matching DssMedAudHlthSrvExcl or null
	 */
	public DssMedAudHlthSrvExcl getDssMedAudHlthSrvExclByPK(Long anId);

	/**
	 * Load the current DssIndividualExclusion records
	 * 
	 * @return current DssIndividualExclusion records
	 */
	public List<DssIndividualExclusion> getIndividualExclusions();

	/**
	 * Saves the new and updated DssMedAudIndivExcl records to the database
	 * 
	 * @param aListOfIndExclToSave
	 */
	public void saveIndividualExclusions(List<DssMedAudIndivExcl> aListOfIndExclToSave);

	/**
	 * Deletes the specified DssMedAudIndivExcl records from the database
	 * 
	 * @param aListOfIndExclToDelete
	 */
	public void deleteIndividualExclusions(List<DssMedAudIndivExcl> aListOfIndExclToDelete);

	/**
	 * Attempts to load a DssMedAudIndivExcl record by PK
	 * 
	 * @param aPk
	 * @return matching DssMedAudIndivExcl or null
	 */
	public DssMedAudIndivExcl getDssMedAudIndivExclByPK(DssMedAudIndivExclPK aPk);

	/**
	 * Attempts the owners name of the provided Health Card Number
	 * 
	 * @param aHealthCardNumber
	 * @return matching name if found. Otherwise no value is returned
	 */
	public String getHealthCardNumberOwnerName(String aHealthCardNumber);

	/**
	 * Saves the specified DssMedAudCriteria record
	 * 
	 * @param anAuditCriteria
	 * @throws MedicareServiceException
	 */
	public void saveMedicareAuditCriteria(DssMedAudCriteria anAuditCriteria);

	/**
	 * Attempts to load DssMedAudCriteria record by PK
	 * 
	 * @param aPk
	 *            - PK of the desired DssMedAudCriteria
	 * @return DssMedAudCriteria if found, null if not.
	 */
	public DssMedAudCriteria getDssMedAudCriteriaByPk(Long aPk);

	/**
	 * @return next value from dss_med_audit_criteria_seq
	 */
	public Long getNextMedAudCriteriaSequenceValue();

	/**
	 * Attempts to load the Practitioner Name via specified arguments
	 * 
	 * @param aPractitionerId
	 * @param aSubcategory
	 * @return Practitioner name if found, otherwise null
	 */
	public String getPractitionerName(Long aPractitionerId, String aSubcategory);

	/**
	 * Attempts to locate an existing DssMedAudCriteria via specified arguments
	 * 
	 * @param aPractitionerId
	 * @param aSubcategory
	 * @param anAuditType
	 * @return DssMedAudCriteria record if found, otherwise null
	 */
	public DssMedAudCriteria getMedicarePractitionerAuditCriteria(Long aPractitionerId, String aSubcategory,
			String anAuditType);

	/**
	 * Loads any matching DssMedAuditCriteriaMappedEntity records using the specified Search Criteria
	 * 
	 * @param aSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return any matching DssMedAuditCriteriaMappedEntity records
	 */
	public List<DssMedAuditCriteriaMappedEntity> getSearchPractitionerAuditCriterias(
			AuditCriteriaSearchCriteria aSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Audit Criteria search
	 * 
	 * @param anAuditCriteriaSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria);

	/**
	 * Attempts to load a DssMedAudCriteria by the specified AuditType
	 * 
	 * @param anAuditType
	 * @return the matching DssMedAudCriteria record if it exists or a brand new DssMedAudCriteria object if nothing is
	 *         found
	 */
	public DssMedAudCriteria getDefaultAuditCriteriaByType(String anAuditType);

	/**
	 * 
	 * Loads medicare audit letter response records
	 * 
	 * @param medicareALRSearchCriteria
	 * @return
	 */
	public List<DssMedicareALRMappedEntity> getMedicareALRRecords(
			MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria, int startRow, int maxRows);

	/**
	 * Performs a record count of the entire possible dataset returned by the provided search criteria
	 * 
	 * @param medicareALRSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws MedicareServiceException
	 */
	public Integer getSearchedMedicareALRCount(MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria);

	/**
	 * search by pk
	 * 
	 * @param pk
	 * @return
	 */
	public DssMedSeAudit getMedicareALRByID(DssMedSeAuditPK pk);

	/**
	 * Save DssMedSeAudit
	 * 
	 * @param target
	 */
	public void saveMedicareALR(DssMedSeAudit target);

	/**
	 * Loads the last Audit Date for the specified Audit Criteria.
	 * 
	 * @param anAuditCriteriaId
	 * @return the lastAudit date for the Audit Criteria ID provided
	 */
	public Date getLastAuditDate(Long anAuditCriteriaId);

	/**
	 * Retrieve the Number Of Letters Created by Audit Run Number and Audit Type. This is only used when the user
	 * provided the Audit Run Number and Audit Type in their search criteria. Displayed above the search results table.
	 * 
	 * To prevent duplicate code this is shared amongst of 3 parts of Service Verification (Medicare, Pharmacare and
	 * Denticare)
	 * 
	 * Query:
	 * 
	 * SELECT audit_run_number FROM dss_complete_audit c WHERE c.audit_run_number = ? and c.audit_type = ?
	 * 
	 * @param anAuditRunNumber
	 * @param anAuditType
	 * @return number of letters created
	 */
	public Long getNumberOfLettersCreated(Long anAuditRunNumber, String anAuditType);

	/**
	 * Load the specified Business Arrangement
	 * 
	 * @param aBusinessArrangementNumber
	 * @return Business Arrangement with the specified BA number
	 */
	public DssBusinessArrangement getBusinessArrangement(Long aBusinessArrangementNumber);

	/**
	 * Load Business Arrangement Exclusions
	 * 
	 * Query:
	 * 
	 * select E.BUS_ARR_NUMBER, CASE WHEN E.PROVIDER_NUMBER IS NOT NULL AND E.PROVIDER_GROUP_ID IS NULL THEN (SELECT
	 * P.LAST_NAME || ' ' || P.MIDDLE_NAME || ', ' || P.FIRST_NAME FROM DSS_PROVIDER_2 P WHERE P.PROVIDER_NUMBER =
	 * E.PROVIDER_NUMBER AND P.PROVIDER_TYPE = E.PROVIDER_TYPE) WHEN E.PROVIDER_GROUP_ID IS NOT NULL AND
	 * E.PROVIDER_NUMBER IS NULL THEN (SELECT PG.PROVIDER_GROUP_NAME FROM DSS_PROVIDER_GROUP PG WHERE
	 * PG.PROVIDER_GROUP_ID = E.PROVIDER_GROUP_ID) END AS PROVIDER_NAME, E.PROVIDER_NUMBER, E.PROVIDER_TYPE,
	 * E.PROVIDER_GROUP_ID, E.LAST_MODIFIED, E.MODIFIED_BY from DSS_MED_AUD_BUS_ARR_EXCL E
	 * 
	 * @return all Business Arrangement Exclusions in the system
	 */
	public List<DssBusinessArrangementExclusionMappedEntity> getBusinessArrangementExclusions();

	/**
	 * Saves a new Exclusion record for the specified Business Arrangement
	 * 
	 * @param aDssMedAudBusArrExcl
	 */
	public void saveBusinessArrangementExclusion(DssMedAudBusArrExcl aDssMedAudBusArrExcl);

	/**
	 * Delete the specified Business Arrangement Exclusions from the system
	 * 
	 * @param aListOfBAExclusionsForDelete
	 */
	public void deleteBusinessArrangementExclusions(List<DssMedAudBusArrExcl> aListOfBAExclusionsForDelete);

	/**
	 * Loads the specified Business Arrangement Exclusion
	 * 
	 * @param aBaExclusionId
	 * @return matching Business Arrangement exclusion
	 */
	public DssMedAudBusArrExcl getDssMedAudBusArrExclByPK(Long aBaExclusionId);
}
