package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferRequest;
import ca.medavie.nspp.audit.service.data.DssDentalClaim;
import ca.medavie.nspp.audit.service.data.DssHistoryConversion;
import ca.medavie.nspp.audit.service.data.DssIndividual2;
import ca.medavie.nspp.audit.service.data.DssMedMainframeClaim;
import ca.medavie.nspp.audit.service.data.DssMedicareSe;
import ca.medavie.nspp.audit.service.data.DssPharmMainframeClaim;
import ca.medavie.nspp.audit.service.data.DssPharmacareSe;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssAuditHistoryTransferMappedEntity;

public class DssClaimsHistoryTransferDAOImpl implements DssClaimsHistoryTransferDAO {

	public EntityManager entityManager;

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(DssClaimsHistoryTransferDAOImpl.class);

	public DssClaimsHistoryTransferDAOImpl(EntityManager em) {
		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DssClaimsHistoryTransferDAO#getSearchedHealthCard(java.lang.String)
	 */
	@Override
	public DssIndividual2 getSearchedHealthCard(String aHealthCardNumber) {

		logger.debug("DssClaimsHistoryTransferDAO getSearchedHealthCard() : Begin");

		// Find the Health Card with the specified HCN (PK)
		DssIndividual2 result = entityManager.find(DssIndividual2.class, aHealthCardNumber.trim());

		logger.debug("DssClaimsHistoryTransferDAO getSearchedHealthCard() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssClaimsHistoryTransferDAO#getMedicareSeClaims(ca.medavie.nspp.audit.service
	 * .data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<DssMedicareSe> getMedicareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest) {

		logger.debug("DssClaimsHistoryTransferDAO getMedicareSeClaims() : Begin");

		// Construct query String
		StringBuilder queryString = new StringBuilder(
				"select dmse from DssMedicareSe dmse where dmse.patientHealthCardNumber = :hcn ");

		// Append date range filtering AND statements if required
		if (aTransferRequest.isServiceFromDateSet()) {
			queryString.append("and dmse.serviceStartDate >= :serviceFrom ");
		}
		if (aTransferRequest.isServiceToDateSet()) {
			queryString.append("and dmse.serviceStartDate <= :serviceTo ");
		}

		// Append OrderBy
		queryString.append("order by dmse.serviceStartDate, dmse.healthServiceCode, dmse.qualifierCode");

		// Create and execute query
		Query query = entityManager.createQuery(queryString.toString());
		// Sadly we must add whitespace due to the horrible data structure in this table TODO (Look at alternatives)
		query.setParameter("hcn", aTransferRequest.getSourceHealthCardNumber() + "  ");

		// Append optional search values
		if (aTransferRequest.isServiceFromDateSet()) {
			query.setParameter("serviceFrom", aTransferRequest.getServiceFromDate());
		}
		if (aTransferRequest.isServiceToDateSet()) {
			query.setParameter("serviceTo", aTransferRequest.getServiceToDate());
		}

		@SuppressWarnings("unchecked")
		List<DssMedicareSe> result = query.getResultList();

		logger.debug("DssClaimsHistoryTransferDAO getMedicareSeClaims() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssClaimsHistoryTransferDAO#getMedicareMainframeClaims(ca.medavie.nspp.audit
	 * .service.data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<DssMedMainframeClaim> getMedicareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest) {

		logger.debug("DssClaimsHistoryTransferDAO getMedicareMainframeClaims() : Begin");

		// Construct query String
		StringBuilder queryString = new StringBuilder(
				"select dmm from DssMedMainframeClaim dmm where dmm.mainHealthCardNumber = :hcn ");

		// Append date range filtering AND statements if required
		if (aTransferRequest.isServiceFromDateSet()) {
			queryString.append("and dmm.mainServiceDate >= :serviceFrom ");
		}
		if (aTransferRequest.isServiceToDateSet()) {
			queryString.append("and dmm.mainServiceDate <= :serviceTo ");
		}

		// Append OrderBy
		queryString.append("order by dmm.mainServiceDate, dmm.mainFeeServiceCode");

		// Create and execute query
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("hcn", new BigDecimal(aTransferRequest.getSourceHealthCardNumber()));

		// Append optional search values
		if (aTransferRequest.isServiceFromDateSet()) {
			query.setParameter("serviceFrom", aTransferRequest.getServiceFromDate());
		}
		if (aTransferRequest.isServiceToDateSet()) {
			query.setParameter("serviceTo", aTransferRequest.getServiceToDate());
		}

		@SuppressWarnings("unchecked")
		List<DssMedMainframeClaim> result = query.getResultList();

		logger.debug("DssClaimsHistoryTransferDAO getMedicareMainframeClaims() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssClaimsHistoryTransferDAO#getPharmacareSeClaims(ca.medavie.nspp.audit.service
	 * .data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<DssPharmacareSe> getPharmacareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest) {

		logger.debug("DssClaimsHistoryTransferDAO getPharmacareSeClaims() : Begin");

		// Construct query String
		StringBuilder queryString = new StringBuilder(
				"select dpse from DssPharmacareSe dpse where dpse.recipientHealthCardNumber = :hcn ");

		// Append date range filtering AND statements if required
		if (aTransferRequest.isServiceFromDateSet()) {
			queryString.append("and dpse.dateOfService >= :serviceFrom ");
		}
		if (aTransferRequest.isServiceToDateSet()) {
			queryString.append("and dpse.dateOfService <= :serviceTo ");
		}

		// Append OrderBy
		queryString.append("order by dpse.dateOfService, dpse.din");

		// Create and execute query
		Query query = entityManager.createQuery(queryString.toString());
		// Sadly we must add whitespace due to the horrible data structure in this table TODO (Look at alternatives)
		query.setParameter("hcn", aTransferRequest.getSourceHealthCardNumber() + "     ");

		// Append optional search values
		if (aTransferRequest.isServiceFromDateSet()) {
			query.setParameter("serviceFrom", aTransferRequest.getServiceFromDate());
		}
		if (aTransferRequest.isServiceToDateSet()) {
			query.setParameter("serviceTo", aTransferRequest.getServiceToDate());
		}

		@SuppressWarnings("unchecked")
		List<DssPharmacareSe> result = query.getResultList();

		logger.debug("DssClaimsHistoryTransferDAO getPharmacareSeClaims() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssClaimsHistoryTransferDAO#getPharmacareMainframeClaims(ca.medavie.nspp.audit
	 * .service.data.DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<DssPharmMainframeClaim> getPharmacareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest) {

		logger.debug("DssClaimsHistoryTransferDAO getPharmacareMainframeClaims() : Begin");

		// Construct query String
		StringBuilder queryString = new StringBuilder(
				"select dpmc from DssPharmMainframeClaim dpmc where dpmc.mainHealthCardNumber = :hcn ");

		// Append date range filtering AND statements if required
		if (aTransferRequest.isServiceFromDateSet()) {
			queryString.append("and dpmc.mainDateOfService >= :serviceFrom ");
		}
		if (aTransferRequest.isServiceToDateSet()) {
			queryString.append("and dpmc.mainDateOfService <= :serviceTo ");
		}

		// Append OrderBy
		queryString.append("order by dpmc.mainDateOfService, dpmc.mainDin");

		// Create and execute query
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("hcn", new BigDecimal(aTransferRequest.getSourceHealthCardNumber()));

		// Append optional search values
		if (aTransferRequest.isServiceFromDateSet()) {
			query.setParameter("serviceFrom", aTransferRequest.getServiceFromDate());
		}
		if (aTransferRequest.isServiceToDateSet()) {
			query.setParameter("serviceTo", aTransferRequest.getServiceToDate());
		}

		@SuppressWarnings("unchecked")
		List<DssPharmMainframeClaim> result = query.getResultList();

		logger.debug("DssClaimsHistoryTransferDAO getPharmacareMainframeClaims() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssClaimsHistoryTransferDAO#getDentalClaims(ca.medavie.nspp.audit.service.data
	 * .DssClaimsHistoryTransferRequest)
	 */
	@Override
	public List<DssDentalClaim> getDentalClaims(DssClaimsHistoryTransferRequest aTransferRequest) {

		logger.debug("DssClaimsHistoryTransferDAO getDentalClaims() : Begin");

		// Construct query String
		StringBuilder queryString = new StringBuilder(
				"select de from DssDentalClaim de where de.healthCardNumber = :hcn ");

		// Append date range filtering AND statements if required
		if (aTransferRequest.isServiceFromDateSet()) {
			queryString.append("and de.serviceDate >= :serviceFrom ");
		}
		if (aTransferRequest.isServiceToDateSet()) {
			queryString.append("and de.serviceDate <= :serviceTo ");
		}

		// Append OrderBy
		queryString.append("order by de.serviceDate, de.procedureCode");

		// Create and execute query
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("hcn", aTransferRequest.getSourceHealthCardNumber());

		// Append optional search values
		if (aTransferRequest.isServiceFromDateSet()) {
			query.setParameter("serviceFrom", aTransferRequest.getServiceFromDate());
		}
		if (aTransferRequest.isServiceToDateSet()) {
			query.setParameter("serviceTo", aTransferRequest.getServiceToDate());
		}

		@SuppressWarnings("unchecked")
		List<DssDentalClaim> result = query.getResultList();

		logger.debug("DssClaimsHistoryTransferDAO getDentalClaims() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DssClaimsHistoryTransferDAO#saveClaimsHistoryTransferStagingData(java.util
	 * .List)
	 */
	@Override
	public void saveClaimsHistoryTransferStagingData(Set<DssHistoryConversion> aListOfConversions) {

		logger.debug("DssClaimsHistoryTransferDAO saveClaimsHistoryTransferStagingData() : Begin");

		// Save each Conversion to the DSS_HISTORY_CONVERSION table
		for (DssHistoryConversion conv : aListOfConversions) {
			entityManager.persist(conv);
		}

		logger.debug("DssClaimsHistoryTransferDAO saveClaimsHistoryTransferStagingData() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssClaimsHistoryTransferDAO#executeClaimsHistoryTransferProcedure(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public void executeClaimsHistoryTransferProcedure(String aSourceHcn, String aTargetHcn) {

		logger.debug("DssClaimsHistoryTransferDAO executeClaimsHistoryTransferProcedure() : Begin");

		CallableStatement statement = null;

		try {
			// Create procedure query
			Connection con = entityManager.unwrap(Connection.class);
			statement = con.prepareCall("{CALL P_EXE_DSS_HISTORY_TRANSFER (?,?,?)}");

			statement.setString(1, aSourceHcn);
			statement.setString(2, aTargetHcn);
			statement.registerOutParameter(3, Types.VARCHAR);

			// Call the procedure
			statement.execute();

			/**
			 * Log procedure error if any
			 */
			if (statement.getString(3) != null) {
				logger.error("Procedure message return : " + statement.getString(3));
				throw new SQLException("Procedure message return : " + statement.getString(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		logger.debug("DssClaimsHistoryTransferDAO executeClaimsHistoryTransferProcedure() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DssClaimsHistoryTransferDAO#getDssAuditHistoryTransfers(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<DssAuditHistoryTransferMappedEntity> getDssAuditHistoryTransfers(String aSourceHcn, String aTargetHcn) {

		logger.debug("DssClaimsHistoryTransferDAO getDssAuditHistoryTransfers() : Begin");

		// Build query string
		StringBuilder queryString = new StringBuilder(
				"SELECT daht.SE_NUMBER, daht.SE_SEQUENCE_NUMBER, daht.RESPONSE_TAG_NUMBER, daht.FROM_HEALTH_CARD_NUMBER, daht.TO_HEALTH_CARD_NUMBER, daht.COMMENTS, "
						+ "daht.TRANSFER_DATE, daht.MODIFIED_BY, daht.MED, daht.PHARM, daht.DENT, daht.MF, daht.PF, di1.SURNAME || ' ' || di1.FIRST_NAME  as source_name, "
						+ "di2.SURNAME || ' ' || di2.FIRST_NAME  as target_name FROM DSS_AUDIT_HISTORY_TRANSFER daht, DSS_INDIVIDUAL_2 di1, DSS_INDIVIDUAL_2 di2 "
						+ "WHERE TRIM(daht.FROM_HEALTH_CARD_NUMBER) = ?1 AND TRIM(daht.TO_HEALTH_CARD_NUMBER) = ?2 AND daht.FROM_HEALTH_CARD_NUMBER = di1.HEALTH_CARD_NUMBER "
						+ "AND daht.TO_HEALTH_CARD_NUMBER = di2.HEALTH_CARD_NUMBER AND TRUNC(daht.LAST_MODIFIED) = TRUNC(sysdate)");

		// Create query, set parameters and execute
		Query query = entityManager.createNativeQuery(queryString.toString(),
				"DssAuditHistoryTransferNativeQueryMapping");
		query.setParameter(1, aSourceHcn);
		query.setParameter(2, aTargetHcn);

		@SuppressWarnings("unchecked")
		List<DssAuditHistoryTransferMappedEntity> result = query.getResultList();

		logger.debug("DssClaimsHistoryTransferDAO getDssAuditHistoryTransfers() : End");
		return result;
	}
}
