package ca.medavie.nspp.audit.service.dao;

import java.util.List;

import ca.medavie.nspp.audit.service.data.ChequeReconciliationSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssChequeReconciliationMappedEntity;

public interface DssChequeReconciliationDAO {
	/**
	 * Loads any DssChequeReconciliation records that match the search criteria
	 * 
	 * @param aChequeReconciliationSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return matching DssChequeReconciliation records
	 */
	public List<DssChequeReconciliationMappedEntity> getChequeReconciliationSearchResults(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Cheque Reconciliation search
	 * 
	 * @param aChequeReconciliationSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getChequeReconciliationSearchResultsCount(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria);

	/**
	 * Loads the Payee Type by code from DSS_PAYEE_TYPE table
	 * 
	 * @param aPayeeTypeCode
	 * @return matching Payee Type
	 */
	public String getPayeeType(Long aPayeeTypeCode);

	/**
	 * Saves any changes made to the DssChequeReconciliation record
	 * 
	 * @param aChequeReconciliation
	 */
	public void saveChequeReconciliation(DssChequeReconciliationMappedEntity aChequeReconciliation);
}
