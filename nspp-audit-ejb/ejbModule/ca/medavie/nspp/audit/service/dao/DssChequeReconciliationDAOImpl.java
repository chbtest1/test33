package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.ChequeReconciliationSearchCriteria;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssChequeReconciliationMappedEntity;

/**
 * DO NOT FORMAT THIS FILE!!!!!!
 */
public class DssChequeReconciliationDAOImpl implements DssChequeReconciliationDAO {

	private EntityManager entityManager;

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(DssChequeReconciliationDAOImpl.class);

	public DssChequeReconciliationDAOImpl(EntityManager em) {
		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssChequeReconciliationDAO#getChequeReconciliationSearchResults(java.lang.Long)
	 */
	@Override
	public List<DssChequeReconciliationMappedEntity> getChequeReconciliationSearchResults(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria, int startRow, int maxRows) {
		logger.debug("DssChequeReconciliationDAO : getChequeReconciliationSearchResults() - Begin");

		// Build the query string
		StringBuilder queryString = constructChequeReconciliationSqlStatement(aChequeReconciliationSearchCriteria);

		// Append order by if required
		if (aChequeReconciliationSearchCriteria.isSortingSet()) {

			// Append required ORDER BY SQL
			DataUtilities.appendSQLOrderByStatements(queryString, aChequeReconciliationSearchCriteria);
		}

		// Create query and execute
		Query query = entityManager.createNativeQuery(queryString.toString(),
				"DssChequeReconciliationNativeQueryMapping");

		// Apply pagination parameters
		query.setFirstResult(startRow);
		query.setMaxResults(maxRows);

		// Apply required arguments
		setChequeReconciliationSearchParameters(query, aChequeReconciliationSearchCriteria);

		@SuppressWarnings("unchecked")
		List<DssChequeReconciliationMappedEntity> result = query.getResultList();

		logger.debug("DssChequeReconciliationDAO : getChequeReconciliationSearchResults() - End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssChequeReconciliationDAO#getChequeReconciliationSearchResultsCount(ca.medavie
	 * .nspp.audit.service.data.ChequeReconciliationSearchCriteria)
	 */
	@Override
	public Integer getChequeReconciliationSearchResultsCount(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT COUNT(*) FROM (");
		queryString.append(constructChequeReconciliationSqlStatement(aChequeReconciliationSearchCriteria));
		queryString.append(")");

		// Create query and set any required arguments
		Query query = entityManager.createNativeQuery(queryString.toString());

		// Apply arguments
		setChequeReconciliationSearchParameters(query, aChequeReconciliationSearchCriteria);

		// Load count result and convert to Integer (Null safe)
		BigDecimal count = (BigDecimal) query.getSingleResult();
		Integer result = new Integer(0);
		if (count != null) {
			result = count.intValue();
		}
		return result;
	}

	/**
	 * Utility method to generate the common FROM/WHERE portion of the Cheque Reconciliation search SQL
	 * 
	 * @param pharmCareALRSearchCriteria
	 * @return construct SQL based on arguments and FROM clause
	 */
	private StringBuilder constructChequeReconciliationSqlStatement(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria) {

		StringBuilder queryString = new StringBuilder("SELECT DISTINCT * FROM dss_cheque_reconciliation dcr ");

		// Append cheque where clause if provided
		if (aChequeReconciliationSearchCriteria.isChequeNumberSet()) {
			queryString.append("WHERE dcr.cheque_number = ?1 ");
		}

		return queryString;
	}

	/**
	 * Applies query arguments for Cheque Reconciliation search/count
	 * 
	 * @param aQuery
	 * @param aChequeReconciliationSearchCriteria
	 */
	private void setChequeReconciliationSearchParameters(Query aQuery,
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria) {

		// Apply argument if required
		if (aChequeReconciliationSearchCriteria.isChequeNumberSet()) {
			aQuery.setParameter(1, aChequeReconciliationSearchCriteria.getChequeNumber());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DssChequeReconciliationDAO#getPayeeType(java.lang.Long)
	 */
	@Override
	public String getPayeeType(Long aPayeeTypeCode) {

		// Build query string
		StringBuilder queryString = new StringBuilder(
				"SELECT DPT.DESCRIPTION FROM DSS_PAYEE_TYPE DPT WHERE DPT.PAYEE_TYPE = ?1");

		// Create query and execute
		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aPayeeTypeCode);

		// Need to handle the situation of no Types coming back
		String result = "";

		@SuppressWarnings("unchecked")
		List<String> typeResult = query.getResultList();

		if (typeResult != null && !typeResult.isEmpty()) {
			result = (String) query.getResultList().get(0);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssChequeReconciliationDAO#saveChequeReconciliation(ca.medavie.nspp.audit.service
	 * .data.DssChequeReconciliation)
	 */
	@Override
	public void saveChequeReconciliation(DssChequeReconciliationMappedEntity aChequeReconciliation) {

		// Update statement
		StringBuilder updateQueryString = new StringBuilder(
				"UPDATE DSS_CHEQUE_RECONCILIATION C SET C.GL_NUMBER = ?1, C.CHEQUE_NOTE = ?2, C.LAST_MODIFIED = ?3 WHERE ");

		// Append the appropriate WHERE clauses based on the value being NULL or not
		updateQueryString.append(aChequeReconciliation.getId().getSummaryNumber() == null ? "C.SUMMARY_NUMBER IS NULL AND " : "C.SUMMARY_NUMBER = ?4 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getBatchNumber() == null ? "C.BATCH_NUMBER IS NULL AND " : "C.BATCH_NUMBER = ?5 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getChequeNumber() == null ? "C.CHEQUE_NUMBER IS NULL AND " : "C.CHEQUE_NUMBER = ?6 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getPaymentRunNumber() == null ? "C.PAYMENT_RUN_NUMBER IS NULL AND " : "C.PAYMENT_RUN_NUMBER = ?7 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getPayeeType() == null ? "C.PAYEE_TYPE IS NULL AND " : "C.PAYEE_TYPE = ?8 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getPaymentDate() == null ? "C.PAYMENT_DATE IS NULL AND " : "C.PAYMENT_DATE = ?9 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getDateCashed() == null ? "C.DATE_CASHED IS NULL AND " : "C.DATE_CASHED = ?10 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getChequeAmount() == null ? "C.CHEQUE_AMOUNT IS NULL AND " : "C.CHEQUE_AMOUNT = ?11 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getClaimedAmount() == null ? "C.CLAIMED_AMOUNT IS NULL AND " : "C.CLAIMED_AMOUNT = ?12 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getOriginalGlNumber() == null ? "C.GL_NUMBER IS NULL AND " : "C.GL_NUMBER = ?13 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getProviderNumber() == null ? "C.PROVIDER_NUMBER IS NULL AND " : "C.PROVIDER_NUMBER = ?14 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getProviderType() == null ? "C.PROVIDER_TYPE IS NULL AND " : "C.PROVIDER_TYPE = ?15 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getProviderGroupId() == null ? "C.PROVIDER_GROUP_ID IS NULL AND " : "C.PROVIDER_GROUP_ID = ?16 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getHealthCardNumber() == null ? "C.HEALTH_CARD_NUMBER IS NULL AND " : "C.HEALTH_CARD_NUMBER = ?17 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getFamilyBenefitsNumber() == null ? "C.FAMILY_BENEFITS_NUMBER IS NULL AND " : "C.FAMILY_BENEFITS_NUMBER = ?18 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getAgencyId() == null ? "C.AGENCY_ID IS NULL AND " : "C.AGENCY_ID = ?19 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getProvinceCode() == null ? "C.PROVINCE_CODE IS NULL AND " : "C.PROVINCE_CODE = ?20 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getOtherPayeeId() == null ? "C.OTHER_PAYEE_ID IS NULL AND " : "C.OTHER_PAYEE_ID = ?21 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getPayeeName() == null ? "C.PAYEE_NAME IS NULL AND " : "C.PAYEE_NAME = ?22 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getOriginalChequeNote() == null || aChequeReconciliation.getId().getOriginalChequeNote().isEmpty() 
				? "C.CHEQUE_NOTE IS NULL AND " : "C.CHEQUE_NOTE = ?23 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getStubsVoided() == null ? "C.STUBS_VOIDED IS NULL AND " : "C.STUBS_VOIDED = ?24 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getPaidStatus() == null ? "C.PAID_STATUS IS NULL AND " : "C.PAID_STATUS = ?25 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getRegisterStatus() == null ? "C.REGISTER_STATUS IS NULL AND " : "C.REGISTER_STATUS = ?26 AND ");
		updateQueryString.append(aChequeReconciliation.getId().getReconciliationStatus() == null ? "C.RECONCILIATION_STATUS IS NULL " : "C.RECONCILIATION_STATUS = ?27 ");

		// Create query
		Query query = entityManager.createNativeQuery(updateQueryString.toString());
		query.setParameter(1, aChequeReconciliation.getId().getGlNumber());
		query.setParameter(2, aChequeReconciliation.getId().getChequeNote());
		query.setParameter(3, aChequeReconciliation.getId().getLastModified());

		// Apply arguments where required
		if (aChequeReconciliation.getId().getSummaryNumber() != null)
			query.setParameter(4, aChequeReconciliation.getId().getSummaryNumber());

		if (aChequeReconciliation.getId().getBatchNumber() != null)
			query.setParameter(5, aChequeReconciliation.getId().getBatchNumber());

		if (aChequeReconciliation.getId().getChequeNumber() != null)
			query.setParameter(6, aChequeReconciliation.getId().getChequeNumber());

		if (aChequeReconciliation.getId().getPaymentRunNumber() != null)
			query.setParameter(7, aChequeReconciliation.getId().getPaymentRunNumber());

		if (aChequeReconciliation.getId().getPayeeType() != null)
			query.setParameter(8, aChequeReconciliation.getId().getPayeeType());

		if (aChequeReconciliation.getId().getPaymentDate() != null)
			query.setParameter(9, aChequeReconciliation.getId().getPaymentDate());

		if (aChequeReconciliation.getId().getDateCashed() != null)
			query.setParameter(10, aChequeReconciliation.getId().getDateCashed());

		if (aChequeReconciliation.getId().getChequeAmount() != null)
			query.setParameter(11, aChequeReconciliation.getId().getChequeAmount());

		if (aChequeReconciliation.getId().getClaimedAmount() != null)
			query.setParameter(12, aChequeReconciliation.getId().getClaimedAmount());

		if (aChequeReconciliation.getId().getOriginalGlNumber() != null)
			query.setParameter(13, aChequeReconciliation.getId().getOriginalGlNumber());

		if (aChequeReconciliation.getId().getProviderNumber() != null)
			query.setParameter(14, aChequeReconciliation.getId().getProviderNumber());

		if (aChequeReconciliation.getId().getProviderType() != null)
			query.setParameter(15, aChequeReconciliation.getId().getProviderType());

		if (aChequeReconciliation.getId().getProviderGroupId() != null)
			query.setParameter(16, aChequeReconciliation.getId().getProviderGroupId());

		if (aChequeReconciliation.getId().getHealthCardNumber() != null)
			query.setParameter(17, aChequeReconciliation.getId().getHealthCardNumber());

		if (aChequeReconciliation.getId().getFamilyBenefitsNumber() != null)
			query.setParameter(18, aChequeReconciliation.getId().getFamilyBenefitsNumber());

		if (aChequeReconciliation.getId().getAgencyId() != null)
			query.setParameter(19, aChequeReconciliation.getId().getAgencyId());

		if (aChequeReconciliation.getId().getProvinceCode() != null)
			query.setParameter(20, aChequeReconciliation.getId().getProvinceCode());

		if (aChequeReconciliation.getId().getOtherPayeeId() != null)
			query.setParameter(21, aChequeReconciliation.getId().getOtherPayeeId());

		if (aChequeReconciliation.getId().getPayeeName() != null)
			query.setParameter(22, aChequeReconciliation.getId().getPayeeName());

		if (aChequeReconciliation.getId().getOriginalChequeNote() != null)
			query.setParameter(23, aChequeReconciliation.getId().getOriginalChequeNote());

		if (aChequeReconciliation.getId().getStubsVoided() != null)
			query.setParameter(24, aChequeReconciliation.getId().getStubsVoided());

		if (aChequeReconciliation.getId().getPaidStatus() != null)
			query.setParameter(25, aChequeReconciliation.getId().getPaidStatus());

		if (aChequeReconciliation.getId().getRegisterStatus() != null)
			query.setParameter(26, aChequeReconciliation.getId().getRegisterStatus());

		if (aChequeReconciliation.getId().getReconciliationStatus() != null)
			query.setParameter(27, aChequeReconciliation.getId().getReconciliationStatus());

		// Execute update
		query.executeUpdate();
	}
}
