package ca.medavie.nspp.audit.service.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.data.DssGlNumber;

public class DssGLNumbersDAOImpl implements DssGLNumbersDAO {

	/** Handles all DB connections and transactions */
	private EntityManager entityManager;

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(DssGLNumbersDAOImpl.class);

	public DssGLNumbersDAOImpl(EntityManager em) {
		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DssGLNumbersDAO#getGLNumbers()
	 */
	@Override
	public List<DssGlNumber> getGLNumbers() {

		logger.debug("DssGLNumbersDAO - getGLNumbers(): Begin");

		Query query = entityManager.createQuery("SELECT gl FROM DssGlNumber gl ORDER BY gl.glNumber DESC");
		@SuppressWarnings("unchecked")
		List<DssGlNumber> result = query.getResultList();

		logger.debug("DssGLNumbersDAO - getGLNumbers(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DssGLNumbersDAO#getGLNumberByGL(java.lang.Long)
	 */
	@Override
	public DssGlNumber getGLNumberByGL(Long aGlNumber) {
		logger.debug("DssGLNumbersDAO - getGLNumberByGL(): Begin");

		// Load the requested record by PK
		DssGlNumber result = entityManager.find(DssGlNumber.class, aGlNumber);

		logger.debug("DssGLNumbersDAO - getGLNumberByGL(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DssGLNumbersDAO#getGLNumbersByPK(java.util.List)
	 */
	@Override
	public List<DssGlNumber> getGLNumbersByGL(List<Long> aListOfGLNumbers) {

		logger.debug("DssGLNumbersDAO - getGLNumbersByGL(): Begin");

		List<DssGlNumber> result = new ArrayList<DssGlNumber>();

		for (Long aPk : aListOfGLNumbers) {
			result.add(getGLNumberByGL(aPk));
		}

		logger.debug("DssGLNumbersDAO - getGLNumbersByGL(): End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DssGLNumbersDAO#saveGLNumber(ca.medavie.nspp.audit.service.data.DssGlNumber)
	 */
	@Override
	public void saveGLNumber(DssGlNumber aDssGLNumber) {

		logger.debug("DssGLNumbersDAO - saveGLNumber(): Begin");
		entityManager.merge(aDssGLNumber);
		logger.debug("DssGLNumbersDAO - saveGLNumber(): End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DssGLNumbersDAO#deleteGLNumbers(java.util.List)
	 */
	@Override
	public void deleteGLNumbers(List<DssGlNumber> aListOfGLNumbersForDelete) {

		logger.debug("DssGLNumbersDAO - deleteGLNumbers(): Begin");

		for (DssGlNumber gl : aListOfGLNumbersForDelete) {
			entityManager.remove(gl);
		}
		logger.debug("DssGLNumbersDAO - deleteGLNumbers(): End");
	}
}
