package ca.medavie.nspp.audit.service.dao;

import java.util.List;

import ca.medavie.nspp.audit.service.data.DssMsiConstant;
import ca.medavie.nspp.audit.service.data.DssProfilePeriod;

/**
 * Interface of the OutlierCriteria DAO layer
 */
public interface OutlierCriteriaDAO {

	/**
	 * 
	 * Get list of DssMsiConstant. Only retrieves the matching subsystem rows. If no subsystem is provided all table
	 * rows are returned
	 * 
	 * @param aSubsystem
	 *            The subsystem of data required from MSI constants.
	 * @return A list of DssMsiConstant
	 */
	public List<DssMsiConstant> getDssMsiConstants(String aSubsystem);

	/**
	 * Get a list of DssMsiContants that match the provided subsystem and constantCode
	 * 
	 * @param aSubsystem
	 * @param aConstantCode
	 * @return A list of DssMsiConstants
	 */
	public List<DssMsiConstant> getDssMsiConstants(String aSubsystem, String aConstantCode);

	/**
	 * Saves (Updates) the provided DssMsiConstant rows to the DB
	 * 
	 * @param dssMsiContants
	 */
	public void saveDssMsiConstants(List<DssMsiConstant> dssMsiContants);
	
	/**
	 * Saves (Updates) the provided DssMsiConstant row to the DB
	 * 
	 * @param dssMsiConstant
	 */
	public void saveDssMsiConstant(DssMsiConstant dssMsiConstant);

	/**
	 * 
	 * Get list of DssProfilePeriod
	 * 
	 * @return A List of DssProfilePeriod
	 */
	public List<DssProfilePeriod> getDssProfilePeriods();

	/**
	 * Save (NEW) the provided DssProfilePeriod record to the database
	 * 
	 * @param aDssProfilePeriod
	 * @return updated DssProfilePeriod containing DB generated values after save
	 */
	public DssProfilePeriod saveDssProfilePeriod(DssProfilePeriod aDssProfilePeriod);
}