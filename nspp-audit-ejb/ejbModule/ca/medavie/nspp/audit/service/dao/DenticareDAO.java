package ca.medavie.nspp.audit.service.dao;

import java.util.Date;
import java.util.List;

import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.data.DssDenAuditCriteria;
import ca.medavie.nspp.audit.service.data.DssDenClaimAudit;
import ca.medavie.nspp.audit.service.data.DssDenClaimAuditPK;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssDenAuditCriteriaMappedEntity;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssDenticareALRMappedEntity;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;

public interface DenticareDAO {

	/**
	 * @param denticareALRSearchCriteria
	 * @param startRow
	 *            - Row number the result set should begin
	 * @param maxRows
	 *            - Maximum number of results to return
	 * @return
	 */
	public List<DssDenticareALRMappedEntity> getDenticareALRRecords(
			DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an ALR search
	 * 
	 * @param denticareALRSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedDenticareALRCount(DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria);

	/**
	 * Find DentiCaerALR By ID
	 * 
	 * @param pk
	 * @return a DssDenClaimAudit
	 */
	public DssDenClaimAudit getDenticareALRByID(DssDenClaimAuditPK pk);

	/**
	 * Save DssDenClaimAudit
	 * 
	 * @param aDssDenClaimAudit
	 */
	public void saveDenticareALR(DssDenClaimAudit aDssDenClaimAudit);

	/**
	 * Loads any matching AuditCriteria records using the specified Search Criteria
	 * 
	 * @param aSearchCriteria
	 * @param startRow
	 *            - Row number the result set should begin
	 * @param maxRows
	 *            - Maximum number of results to return
	 * @return any matching AuditCriteria records
	 */
	public List<DssDenAuditCriteriaMappedEntity> getSearchPractitionerAuditCriterias(
			AuditCriteriaSearchCriteria aSearchCriteria, int startRow, int maxRows);

	/**
	 * Counts all rows that return by an Audit Criteria search
	 * 
	 * @param anAuditCriteriaSearchCriteria
	 * @return - total row count for entire search dataset
	 */
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria);

	/**
	 * Attempts to load DssDenAuditCriteria record by PK
	 * 
	 * @param aPk
	 *            - PK of the desired DssDenAuditCriteria
	 * @return DssDenAuditCriteria if found, null if not.
	 */
	public DssDenAuditCriteria getDssDenAuditCriteriaByPk(Long aPk);

	/**
	 * Attempts to load a DssDenAuditCriteria by the specified AuditType
	 * 
	 * @param anAuditType
	 * @return the matching DssDenAuditCriteria record if it exists or a brand new DssDenAuditCriteria object if nothing
	 *         is found
	 */
	public DssDenAuditCriteria getDefaultAuditCriteriaByType(String anAuditType);

	/**
	 * @return next value from dss_den_audit_criteria_seq
	 */
	public Long getNextDenAudCriteriaSequenceValue();

	/**
	 * Saves the specified DssDenAuditCriteria record
	 * 
	 * @param anAuditCriteria
	 * @throws MedicareServiceException
	 */
	public void saveDenticareAuditCriteria(DssDenAuditCriteria anAuditCriteria);

	/**
	 * Loads the last Audit Date for the specified Audit Criteria.
	 * 
	 * @param anAuditCriteriaId
	 * @return the lastAudit date for the Audit Criteria ID provided
	 */
	public Date getLastAuditDate(Long anAuditCriteriaId);
}
