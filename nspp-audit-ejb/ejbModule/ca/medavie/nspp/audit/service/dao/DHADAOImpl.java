package ca.medavie.nspp.audit.service.dao;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.ServicePersistenceException;
import ca.medavie.nspp.audit.service.data.DistrictHealthAuthority;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.mappedEntity.DssProviderDhaMappedEntity;

public class DHADAOImpl implements DHADAO {

	/** Class logger */
	private static final Logger logger = LoggerFactory.getLogger(DHADAOImpl.class);

	/** Handles all DB connections and transactions */
	public EntityManager entityManager;

	public DHADAOImpl(EntityManager em) {
		entityManager = em;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DHADAO#getDHAs()
	 */
	@Override
	public List<DssProviderDhaMappedEntity> getDHAs() {

		logger.debug("getDHAs() : Begin");
		// Create the query string
		StringBuilder queryString = new StringBuilder(
				"SELECT dha.pdha_id, dha.provider_number, dha.provider_type, CASE WHEN dp.organization_name IS NULL OR LENGTH(TRIM(dp.organization_name)) IS NULL OR LENGTH(TRIM(dp.organization_name)) IS NULL THEN "
						+ "dp.last_name || ', ' || dp.first_name || ' ' || substr(dp.middle_name, 1, 1) ELSE dp.organization_name END as provider_name,  "
						+ "dha.contract_town_name, dha.health_region_code AS zone_id, (SELECT cc.code_table_entry_description FROM dss_code_table_entry cc "
						+ "WHERE dha.health_region_code = cc.code_table_entry AND cc.code_table_number = 135) AS zone_name, dha.dha_code, (SELECT cc.code_table_entry_description FROM dss_code_table_entry cc "
						+ "WHERE dha.dha_code = cc.code_table_entry AND cc.code_table_number = 157) AS dha_desc, dha.emp_type, dha.fte_value, dha.bus_arr_number, dha.modified_by, dha.last_modified, " 
						+ "dha.provider_group_id, gg.provider_group_name AS provider_group_description "
						+ " FROM DSS_PROVIDER_DHA dha "
						+ " JOIN dss_provider_2 dp ON (dha.provider_number = dp.provider_number AND dha.provider_type = dp.provider_type AND dp.provider_type != 'RX') "
						+ " LEFT OUTER JOIN dss_provider_group gg ON dha.provider_group_id = gg.provider_group_id "
						+ " ORDER BY dha.provider_group_id DESC ");
		// Create query and execute
		Query query = entityManager.createNativeQuery(queryString.toString(), "DssProviderDhaQueryMapping");
		@SuppressWarnings("unchecked")
		List<DssProviderDhaMappedEntity> result = query.getResultList();

		logger.debug("getDHAs() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DHADAO#getSearchedPractitioners(ca.medavie.nspp.audit.service.data.
	 * PractitionerSearchCriteria)
	 */
	@Override
	public List<Object[]> getSearchedPractitioners(PractitionerSearchCriteria aSearchCriteria) {

		logger.debug("getSearchedPractitioners(PractitionerSearchCriteria) : Begin");
		// Create query string
		// JTRAX-66 16-MAR-2018 BCAINNE
		// AND gx.effective_from_date <= SYSDATE AND gx.effective_to_date >= SYSDATE
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT dp.provider_number, dp.provider_type, CASE WHEN dp.organization_name IS NULL OR LENGTH(TRIM(dp.organization_name)) IS NULL THEN" +
				"				 dp.last_name || ', ' || dp.first_name || ' ' || substr(dp.middle_name, 1, 1) ELSE dp.organization_name END as provider_name," +
				"				 gg.provider_group_id, gg.provider_group_name" +
				"  FROM dss_provider_2 dp" +
				"  LEFT JOIN dss_provider_group_xref gx ON (gx.provider_number = dp.provider_number AND gx.provider_type = dp.provider_type )" +
				"  LEFT JOIN dss_provider_group gg ON (gx.provider_group_id = gg.provider_group_id)" +
				"  WHERE dp.provider_type NOT IN ('RX','PX')");
		// Add the required AND clauses if the user provided search arguments		
//		if (aSearchCriteria.isBusinessArrangementNumberSet()) {
	//		queryString.append(" AND ba.bus_arr_number = ?1 ");
		//}
		//if (aSearchCriteria.isBusinessArrangementDescriptionSet()) {
			//queryString.append(" AND ba.bus_arr_description LIKE CONCAT('%', CONCAT(?2,'%')) ");
		//}
		if (aSearchCriteria.isPractitionerNumberSet()) {
			queryString.append(" AND dp.provider_number = ?3 ");
		}
		if (aSearchCriteria.isSubcategorySet()) {
			queryString.append(" AND dp.provider_type = ?4 ");
		}		
		if (aSearchCriteria.isPractitionerGroupIdSet()) {
			queryString.append(" AND gg.provider_group_id = ?5 ");
		}
		if (aSearchCriteria.isPractitionerGroupNameSet()) {
			queryString.append(" AND UPPER(gg.provider_group_name) LIKE CONCAT('%', CONCAT(UPPER(?6),'%')) ");
		}		
		
		if (aSearchCriteria.isPractitionerNameSet()) {
			// parse string into individual words
			String[] arr = aSearchCriteria.getPractitionerName().replaceAll(",", "").split(" ");
			// The clause comes out like this for multiple names entered
			// add condition for each word
			for (int i = 0; i < arr.length; i++) {
				if (arr[i].trim().length() > 0) {
					if (i==0) {
						queryString.append(" AND (");
					} else {
						queryString.append(" OR");
					}
					queryString
					.append(" upper(dp.last_name) LIKE CONCAT('%', CONCAT(?"+(7+i)+",'%'))" +
							" or upper(dp.first_name) LIKE CONCAT('%', CONCAT(?"+(7+i)+",'%'))" +
							" or upper(dp.organization_name) LIKE CONCAT('%', CONCAT(?"+(7+i)+",'%'))");
				}
			}
			if (arr.length > 0) {
				queryString.append(")");
			}
		}		
		

		// Append order by clause
		queryString.append(" ORDER BY dp.provider_number ASC");
		
		//System.out.println(queryString.toString());

		// Create query, set arguments and execute
		Query query = entityManager.createNativeQuery(queryString.toString());
		//if (aSearchCriteria.isBusinessArrangementNumberSet()) {
			//query.setParameter(1, aSearchCriteria.getBusinessArrangementNumber());
		//}
		//if (aSearchCriteria.isBusinessArrangementDescriptionSet()) {
			//query.setParameter(2, "%" + aSearchCriteria.getBusinessArrangementDescription().toUpperCase() + "%");
		//}
		if (aSearchCriteria.isPractitionerNumberSet()) {
			query.setParameter(3, aSearchCriteria.getPractitionerNumber());
		}
		if (aSearchCriteria.isSubcategorySet()) {
			query.setParameter(4, aSearchCriteria.getSubcategory());
		}
		if (aSearchCriteria.isPractitionerGroupIdSet()) {
			query.setParameter(5, aSearchCriteria.getPractitionerGroupId());
		}
		if (aSearchCriteria.isPractitionerGroupNameSet()) {
			query.setParameter(6, aSearchCriteria.getPractitionerGroupName());
		}			
		if (aSearchCriteria.isPractitionerNameSet()) {
			// parse string into individual words
			String[] arr = aSearchCriteria.getPractitionerName().replaceAll(",", "").split(" ");
			// The clause comes out like this for multiple names entered
			// add condition for each word
			for (int i = 0; i < arr.length; i++) {
				if (arr[i].trim().length() > 0) {
					query.setParameter(7+i, arr[i].toUpperCase() );
				}
			}
		}

		@SuppressWarnings("unchecked")
		List<Object[]> result = query.getResultList();

		logger.debug("getSearchedPractitioners(PractitionerSearchCriteria) : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.dao.DHADAO#saveDHAChanges(ca.medavie.nspp.audit.service.data.DistrictHealthAuthority
	 * )
	 */
	@Override
	public void saveDHAChanges(DistrictHealthAuthority aDha) {

		logger.debug("saveDHAChanges() : Begin");

		// If new record we need to generate a new PK and use INSERT instead of UPDATE
		if (aDha.getDhaId() == null) {
			// Select the next value from the DSS_PROVIDER_DHA_SEQUENCE
			StringBuilder sequenceQueryString = new StringBuilder("SELECT dss_provider_dha_seq.NEXTVAL FROM DUAL");

			// Execute query and extract value
			Query sequenceQuery = entityManager.createNativeQuery(sequenceQueryString.toString());
			Long dhaSequenceValue = ((BigDecimal) sequenceQuery.getSingleResult()).longValue();

			// Apply new PK to DHA record
			aDha.setDhaId(dhaSequenceValue);

			// Create insert query string
			String insertQueryString = "INSERT INTO DSS_PROVIDER_DHA (PDHA_ID, PROVIDER_NUMBER, PROVIDER_TYPE, CONTRACT_TOWN_NAME, DHA_CODE, HEALTH_REGION_CODE, PROVIDER_GROUP_ID, " +
									   "FTE_VALUE, EMP_TYPE, MODIFIED_BY, LAST_MODIFIED, bus_arr_number) VALUES(?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12)";

			// Create query, apply arguments and execute
			Query insertQuery = entityManager.createNativeQuery(insertQueryString);
			insertQuery.setParameter(1, aDha.getDhaId());
			insertQuery.setParameter(2, aDha.getPractitionerNumber());
			insertQuery.setParameter(3, aDha.getPractitionerType());
			insertQuery.setParameter(4, aDha.getContractTownName());
			insertQuery.setParameter(5, aDha.getDhaCode());
			insertQuery.setParameter(6, aDha.getZoneId());
			insertQuery.setParameter(7, aDha.getPractitionerGroupNumber());
			insertQuery.setParameter(8, aDha.getFte());
			insertQuery.setParameter(9, aDha.getEmpType());
			insertQuery.setParameter(10, aDha.getModifiedBy());
			insertQuery.setParameter(11, aDha.getLastModified());
			insertQuery.setParameter(12, aDha.getBusinessArrangementNumber());
			insertQuery.executeUpdate();
		} else {
			// Create update query string
			StringBuilder updateQueryString = new StringBuilder(
					"UPDATE DSS_PROVIDER_DHA dha SET dha.contract_town_name = ?1, dha.dha_code = ?2, dha.health_region_code = ?3, dha.fte_value = ?4, "
							+ "dha.emp_type = ?5, dha.modified_by = ?6, dha.last_modified = ?7, bus_arr_number=?8 WHERE dha.pdha_id = ?9");

			// Create query, apply arguments and execute
			Query updateQuery = entityManager.createNativeQuery(updateQueryString.toString());
			updateQuery.setParameter(1, aDha.getContractTownName());
			updateQuery.setParameter(2, aDha.getDhaCode());
			updateQuery.setParameter(3, aDha.getZoneId());
			updateQuery.setParameter(4, aDha.getFte());
			updateQuery.setParameter(5, aDha.getEmpType());
			updateQuery.setParameter(6, aDha.getModifiedBy());
			updateQuery.setParameter(7, aDha.getLastModified());
			updateQuery.setParameter(8, aDha.getBusinessArrangementNumber());
			updateQuery.setParameter(9, aDha.getDhaId());
			updateQuery.executeUpdate();
		}

		logger.debug("saveDHAChanges() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DHADAO#deleteDHAs(java.util.List)
	 */
	@Override
	public void deleteDHAs(List<DistrictHealthAuthority> aListOfDhasToDelete) {
		logger.debug("aListOfDhasToDelete() : Begin");

		// Create delete query string
		StringBuilder deleteQueryString = new StringBuilder("DELETE FROM DSS_PROVIDER_DHA x WHERE x.Pdha_Id IN (");

		// Get the IDs of DHA to delete and append them into the query string
		for (Iterator<DistrictHealthAuthority> iter = aListOfDhasToDelete.iterator(); iter.hasNext();) {

			DistrictHealthAuthority dha = iter.next();
			deleteQueryString.append(dha.getDhaId());

			// Append a ',' if required
			if (iter.hasNext()) {
				deleteQueryString.append(",");
			}
		}
		// Append closing bracket
		deleteQueryString.append(")");

		// Create query and execute
		Query deleteQuery = entityManager.createNativeQuery(deleteQueryString.toString());
		deleteQuery.executeUpdate();

		logger.debug("aListOfDhasToDelete() : End");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DHADAO#getZoneName(java.lang.String)
	 */
	@Override
	public String getZoneName(String aZoneId) {

		logger.debug("getZoneName() : Begin");
		// Create the query string
		StringBuilder queryString = new StringBuilder(
				"SELECT cc.code_table_entry_description FROM dss_code_table_entry cc WHERE cc.code_table_number = 135 AND cc.code_table_entry = ?1");

		// Create query, set arguments and execute
		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aZoneId);
		String result = (String) query.getSingleResult();

		logger.debug("getZoneName() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DHADAO#getDhaDescription(java.lang.String)
	 */
	@Override
	public String getDhaDescription(String aDhaCode) {

		logger.debug("getDhaDescription() : Begin");
		// Create the query string
		StringBuilder queryString = new StringBuilder(
				"SELECT cc.code_table_entry_description FROM dss_code_table_entry cc WHERE cc.code_table_number = 157 AND cc.code_table_entry = ?1");

		// Create query, set arguments and execute
		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aDhaCode);
		String result = (String) query.getSingleResult();

		logger.debug("getDhaDescription() : End");
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DHADAO#getZoneIds(java.lang.String)
	 */
	@Override
	public List<Object[]> getZoneIds(String aTownName) {

		// Create query string
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT p.health_region_code, cc.code_table_entry_description FROM dss_placename p, dss_code_table_entry cc "
						+ "WHERE REGEXP_LIKE(p.health_region_code,'^[[:digit:]]$*') AND p.placename = ?1 "
						+ "AND cc.code_table_number = 135 AND cc.code_table_entry = p.health_region_code AND p.placename != 'UNKNOWN' ORDER BY p.health_region_code ASC");
		// Create query, set args and execute
		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aTownName);
		@SuppressWarnings("unchecked")
		List<Object[]> result = query.getResultList();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.dao.DHADAO#getDhaCodes(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Object[]> getDhaCodes(String aTownName, String aZoneId) throws ServicePersistenceException {

		// Create query string
		StringBuilder queryString = new StringBuilder(
				"SELECT DISTINCT p.dha_code, cc.code_table_entry_description FROM dss_placename p, dss_code_table_entry cc " 
						+ "WHERE REGEXP_LIKE(p.health_region_code,'^[[:digit:]]$*') AND p.placename = ?1 "
						+ "AND p.Health_Region_Code = ?2 AND cc.code_table_number = 157 AND cc.code_table_entry = p.dha_code");

		// Create query, set args and execute
		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, aTownName);
		query.setParameter(2, aZoneId);
		@SuppressWarnings("unchecked")
		List<Object[]> result = query.getResultList();

		return result;
	}
}
