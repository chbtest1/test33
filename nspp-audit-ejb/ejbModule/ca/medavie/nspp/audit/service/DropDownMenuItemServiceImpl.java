package ca.medavie.nspp.audit.service;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.CodeEntry;
import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;
import ca.medavie.nspp.audit.service.data.SpecialtyCriteria;
import ca.medavie.nspp.audit.service.data.SubcategoryType;
import ca.medavie.nspp.audit.service.exception.DropDownMenuItemServiceException;
import ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepository;
import ca.medavie.nspp.audit.service.repository.DropDownMenuItemRepositoryImpl;

@Stateless
@Local(DropDownMenuItemService.class)
@LocalBean
public class DropDownMenuItemServiceImpl implements DropDownMenuItemService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private DropDownMenuItemRepository dropDownMenuItemRepository;

	@PostConstruct
	public void doInitRepo() {
		dropDownMenuItemRepository = new DropDownMenuItemRepositoryImpl(em);
	}

	@Override
	public Map<String, SubcategoryType> getSubCategoryTypesDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getSubCategoryTypeDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	@Override
	public Map<String, SpecialtyCriteria> getSpecialtyDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getSpecialtyDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	public DropDownMenuItemRepository getDropDownMenuItemRepository() {
		return dropDownMenuItemRepository;
	}

	public void setDropDownMenuItemRepository(DropDownMenuItemRepository dropDownMenuItemRepository) {
		this.dropDownMenuItemRepository = dropDownMenuItemRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getAuditRXTypeDropDown()
	 */
	@Override
	public Map<String, CodeEntry> getAuditRXTypeDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getAuditRXTypeDropDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getAuditNonRXTypeDropDown()
	 */
	@Override
	public Map<String, CodeEntry> getAuditNonRXTypeDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getAuditNonRXTypeDropDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getContractTownNames()
	 */
	@Override
	public List<String> getContractTownNames() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getContractTownNames();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getQualifierDropDown()
	 */
	@Override
	public Map<String, String> getQualifierDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getQualifierDropDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getAuditSourceMedCodeDropDown()
	 */
	@Override
	public Map<String, String> getAuditSourceMedCodeDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getAuditSourceMedCodeDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getAuditSourcePharmCodeDropDown()
	 */
	@Override
	public Map<String, String> getAuditSourcePharmCodeDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getAuditSourcePharmCodeDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getAuditPharmTypeCodeDropDown()
	 */
	@Override
	public Map<String, String> getAuditPharmTypeCodeDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getAuditPharmTypeCodeDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getGroupTypeCodesDropDown()
	 */
	@Override
	public Map<String, String> getGroupTypeCodesDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getGroupTypeCodeDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getPeriodYearEndDateDropdown()
	 */
	@Override
	public Map<String, String> getPeriodYearEndDateDropdown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getPeriodYearEndDateDropdown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getPeriodIdDropdown()
	 */
	@Override
	public Map<Long, PractitionerProfilePeriod> getPeriodIdDropdown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getPeriodIdDropdown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getDssUnitValueLevelDropdown()
	 */
	@Override
	public Map<Long, Long> getDssUnitValueLevelDropdown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getDssUnitValueLevelDropdown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getDssUnitValueDescsDropdown()
	 */
	@Override
	public Map<String, Long> getDssUnitValueDescsDropdown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getDssUnitValueDescsDropdown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getHsModifierDropDown()
	 */
	@Override
	public Map<String, String> getHsModifierDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getHsModifierDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getHsImplicitModifiersDropDown()
	 */
	@Override
	public Map<String, String> getHsImplicitModifiersDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getHsImplicitModifiersDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getHsProgramDropDown()
	 */
	@Override
	public Map<String, String> getHsProgramDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getHsProgramDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getHsGroupDropDown()
	 */
	@Override
	public Map<Long, String> getHsGroupDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getHsGroupDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getHsQualifierDropDown()
	 */
	@Override
	public Map<String, String> getHsQualifierDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getHsQualifiersDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getDenticareAuditTypeDropDown()
	 */
	@Override
	public Map<String, String> getDenticareAuditTypeDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getDenticareAuditTypeDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getResponseStatusDropDown()
	 */
	@Override
	public Map<String, String> getResponseStatusDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getResponseStatusDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getGlNumbers()
	 */
	@Override
	public Map<String, String> getGlNumbers() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getGlNumbers();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getPayeeTypeDropDown()
	 */
	@Override
	public Map<String, String> getPayeeTypeDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getPayeeTypeDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getPaymentResponsibilityDropDown()
	 */
	@Override
	public Map<String, String> getPaymentResponsibilityDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getPaymentResponsibilityDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getMedicareAuditTypeDropDown()
	 */
	@Override
	public Map<String, String> getMedicareAuditTypeDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getMedicareAuditTypeDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getPharmacareAuditTypeDropDown()
	 */
	@Override
	public Map<String, String> getPharmacareAuditTypeDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getPharmacareAuditTypeDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getGeneralAuditCriteriaTypeDropDown()
	 */
	@Override
	public Map<String, String> getGeneralAuditCriteriaTypeDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getGeneralAuditCriteriaTypeDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.DropDownMenuItemService#getPharmacareAuditCriteriaTypeDropDown()
	 */
	@Override
	public Map<String, String> getPharmacareAuditCriteriaTypeDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getPharmacareAuditCriteriaTypeDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}

	@Override
	public Map<String, String> getHSEffectiveDateDropDown() throws DropDownMenuItemServiceException {
		try {
			return dropDownMenuItemRepository.getHSEffectiveDateDropDown();
		} catch (Exception e) {
			throw new DropDownMenuItemServiceException(e.getMessage(), e);
		}
	}
}
