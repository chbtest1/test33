package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.AuditResult;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.exception.AuditResultServiceException;
import ca.medavie.nspp.audit.service.repository.AuditResultRepository;
import ca.medavie.nspp.audit.service.repository.AuditResultRepositoryImpl;

/**
 * Session Bean implementation class AuditResultServiceImpl
 */
@Stateless
@Local(AuditResultService.class)
@LocalBean
public class AuditResultServiceImpl implements AuditResultService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private AuditResultRepository auditResultRepository;

	@PostConstruct
	public void doInitRepo() {
		auditResultRepository = new AuditResultRepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.AuditResultService#getPrctitionerSearchResult(ca.medavie.nspp.audit.service.data
	 * .PractitionerSearchCriteria)
	 */
	@Override
	public List<Practitioner> getPractitionerSearchResult(PractitionerSearchCriteria aPractitionerSearchCriteria)
			throws AuditResultServiceException {

		try {
			return auditResultRepository.getPractitionerSearchResult(aPractitionerSearchCriteria);
		} catch (Exception e) {
			throw new AuditResultServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.AuditResultService#setAuditResult(ca.medavie.nspp.audit.service.data.AuditResult)
	 */
	@Override
	public void setAuditResult(AuditResult anAuditedResultToSave) throws AuditResultServiceException {

		try {
			auditResultRepository.setAuditResult(anAuditedResultToSave);
		} catch (Exception e) {
			throw new AuditResultServiceException(e.getMessage(), e);
		}

	}

	@Override
	public List<AuditResult> getSelectedPractitionerAuditResultDetails(Practitioner aSelectedPractitioner)
			throws AuditResultServiceException {

		try {
			return auditResultRepository.getSelectedPractitionerAuditResultDetails(aSelectedPractitioner);
		} catch (Exception e) {
			throw new AuditResultServiceException(e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.AuditResultService#getHealthServiceCodes()
	 */
	@Override
	public List<String> getHealthServiceCodes() throws AuditResultServiceException {
		try {
			return auditResultRepository.getHealthServiceCodes();
		} catch (Exception e) {
			throw new AuditResultServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.AuditResultService#getAttachmentBinary(java.lang.Long)
	 */
	@Override
	public byte[] getAttachmentBinary(Long anAttachmentId) throws AuditResultServiceException {

		try {
			return auditResultRepository.getAttachmentBinary(anAttachmentId);
		} catch (Exception e) {
			throw new AuditResultServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.AuditResultService#setAttachmentBinary(java.lang.Long, byte[])
	 */
	@Override
	public void setAttachmentBinary(Long anAttachmentId, byte[] anAttachmentBinary) throws AuditResultServiceException {

		try {
			auditResultRepository.setAttachmentBinary(anAttachmentId, anAttachmentBinary);
		} catch (Exception e) {
			throw new AuditResultServiceException(e.getMessage(), e);
		}

	}

	// JTRAX-56 08-FEB-18 BCAINNE
	@Override
	public String getReason(String healthServiceCode, String qualifer)
			throws AuditResultServiceException {
		try {
			return auditResultRepository.getReason(healthServiceCode, qualifer);
		} catch (Exception e) {
			throw new AuditResultServiceException(e.getMessage(), e);
		}
	}

	@Override
	public void deleteAuditResult(AuditResult aSelectedAuditResult)
			throws AuditResultServiceException {
		try {
			auditResultRepository.deleteAuditResult(aSelectedAuditResult);
		} catch (Exception e) {
			throw new AuditResultServiceException(e.getMessage(), e);
		}
	}

}
