package ca.medavie.nspp.audit.service;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.HealthServiceCodeSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceGroup;
import ca.medavie.nspp.audit.service.data.HealthServiceGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceProgram;
import ca.medavie.nspp.audit.service.data.Modifier;
import ca.medavie.nspp.audit.service.exception.HealthServiceGroupServiceException;
import ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepository;
import ca.medavie.nspp.audit.service.repository.HealthServiceGroupRepositoryImpl;

/**
 * Session Bean implementation class HealthServiceGroupServiceImpl
 */
@Stateless
@Local(HealthServiceGroupService.class)
@LocalBean
public class HealthServiceGroupServiceImpl implements HealthServiceGroupService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private HealthServiceGroupRepository healthServiceGroupRepository;

	@PostConstruct
	private void doInitRepo() {
		healthServiceGroupRepository = new HealthServiceGroupRepositoryImpl(em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.HealthServiceGroupService#getSearchHealthServiceGroups(ca.medavie.nspp.audit.service
	 * .data.HealthServiceGroupSearchCriteria, int, int)
	 */
	@Override
	public List<HealthServiceGroup> getSearchHealthServiceGroups(HealthServiceGroupSearchCriteria aSearchCriteria,
			int startRow, int maxRows) throws HealthServiceGroupServiceException {
		try {
			return healthServiceGroupRepository.getSearchHealthServiceGroups(aSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.HealthServiceGroupService#getSearchHealthServiceGroupsCount(ca.medavie.nspp.audit
	 * .service.data.HealthServiceGroupSearchCriteria)
	 */
	@Override
	public Integer getSearchHealthServiceGroupsCount(HealthServiceGroupSearchCriteria aSearchCriteria)
			throws HealthServiceGroupServiceException {
		try {
			return healthServiceGroupRepository.getSearchHealthServiceGroupsCount(aSearchCriteria);
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.HealthServiceGroupService#getSearchHealthServiceCodes(ca.medavie.nspp.audit.service
	 * .data.HealthServiceCodeSearchCriteria)
	 */
	@Override
	public List<HealthServiceCode> getSearchHealthServiceCodes(HealthServiceCodeSearchCriteria aSearchCriteria)
			throws HealthServiceGroupServiceException {
		try {
			return healthServiceGroupRepository.getSearchHealthServiceCodes(aSearchCriteria);
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.HealthServiceGroupService#getHealthServiceCodes(java.lang.Long)
	 */
	@Override
	public List<HealthServiceCode> getHealthServiceCodes(Long aHealthServiceGroupId)
			throws HealthServiceGroupServiceException {
		try {
			return healthServiceGroupRepository.getHealthServiceCodes(aHealthServiceGroupId);
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.HealthServiceGroupService#saveHealthServiceGroup(ca.medavie.nspp.audit.service.
	 * data.HealthServiceGroup, java.util.List)
	 */
	@Override
	public void saveHealthServiceGroup(HealthServiceGroup aHealthServiceGroup, List<HealthServiceCode> aListOfHSCToAdd)
			throws HealthServiceGroupServiceException {
		try {
			healthServiceGroupRepository.saveHealthServiceGroup(aHealthServiceGroup, aListOfHSCToAdd);
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.HealthServiceGroupService#saveNewHealthServiceGroup(ca.medavie.nspp.audit.service
	 * .data.HealthServiceGroup)
	 */
	@Override
	public void saveNewHealthServiceGroup(HealthServiceGroup aHealthServiceGroup)
			throws HealthServiceGroupServiceException {
		try {
			healthServiceGroupRepository.saveNewHealthServiceGroup(aHealthServiceGroup);
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.HealthServiceGroupService#isHealthServiceGroupDuplicate(java.lang.Long)
	 */
	@Override
	public boolean isHealthServiceGroupDuplicate(Long aHealtherServiceGroupId)
			throws HealthServiceGroupServiceException {
		try {
			return healthServiceGroupRepository.isHealthServiceGroupDuplicate(aHealtherServiceGroupId);
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.HealthServiceGroupService#deleteHealthServiceCodesFromGroup(java.lang.Long,
	 * java.util.List)
	 */
	@Override
	public void deleteHealthServiceCodesFromGroup(Long aHealthServiceGroupId,
			List<HealthServiceCode> aListOfCodesForDelete) throws HealthServiceGroupServiceException {
		try {
			healthServiceGroupRepository
					.deleteHealthServiceCodesFromGroup(aHealthServiceGroupId, aListOfCodesForDelete);
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.HealthServiceGroupService#getHealthServicePrograms()
	 */
	@Override
	public List<HealthServiceProgram> getHealthServicePrograms() throws HealthServiceGroupServiceException {
		try {
			return healthServiceGroupRepository.getHealthServicePrograms();
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/**
	 * @return the healthServiceGroupRepository
	 */
	public HealthServiceGroupRepository getHealthServiceGroupRepository() throws HealthServiceGroupServiceException {
		try {
			return healthServiceGroupRepository;
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/**
	 * @param healthServiceGroupRepository
	 *            the healthServiceGroupRepository to set
	 */
	public void setHealthServiceGroupRepository(HealthServiceGroupRepository healthServiceGroupRepository)
			throws HealthServiceGroupServiceException {
		try {
			this.healthServiceGroupRepository = healthServiceGroupRepository;
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.HealthServiceGroupService#getModifiers()
	 */
	@Override
	public List<Modifier> getModifiers() throws HealthServiceGroupServiceException {
		try {
			return healthServiceGroupRepository.getModifiers();
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.HealthServiceGroupService#getImplicitModifiers()
	 */
	@Override
	public List<Modifier> getImplicitModifiers() throws HealthServiceGroupServiceException {
		try {
			return healthServiceGroupRepository.getImplicitModifiers();
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.HealthServiceGroupService#getServicesNotInGroup(java.util.Date)
	 */
	@Override
	public List<HealthServiceCode> getServicesNotInGroup(Date aYearEndDate) throws HealthServiceGroupServiceException {
		try {
			return healthServiceGroupRepository.getServicesNotInGroup(aYearEndDate);
		} catch (Exception e) {
			throw new HealthServiceGroupServiceException(e.getMessage(), e);
		}
	}
}
