package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ca.medavie.nspp.audit.service.data.BusinessArrangementCode;
import ca.medavie.nspp.audit.service.data.ManualDeposit;
import ca.medavie.nspp.audit.service.data.ManualDepositSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.ProviderGroup;
import ca.medavie.nspp.audit.service.data.ProviderGroupSearchCriteria;
import ca.medavie.nspp.audit.service.exception.AccountingServiceException;
import ca.medavie.nspp.audit.service.repository.ManualDepositsRepository;
import ca.medavie.nspp.audit.service.repository.ManualDepositsRepositoryImpl;

/**
 * Session Bean implementation class AccountingServiceImpl
 */
@Stateless
@Local(ManualDepositsService.class)
@LocalBean
public class ManualDepositsServiceImpl implements ManualDepositsService {

	@PersistenceContext(name = "nspp-audit-ejb")
	private EntityManager em;

	private ManualDepositsRepository manualDepositsRepository;

	@PostConstruct
	public void doInitRepo() {
		manualDepositsRepository = new ManualDepositsRepositoryImpl(em);
	}

	public ManualDepositsRepository getAccountingRepository() {
		return manualDepositsRepository;
	}

	public void setAccountingRepository(ManualDepositsRepository accountingRepository) {
		this.manualDepositsRepository = accountingRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.ManualDepositsService#getBusinessArrangements(ca.medavie.nspp.audit.service.data
	 * .Practitioner, ca.medavie.nspp.audit.service.data.ProviderGroup)
	 */
	@Override
	public List<BusinessArrangementCode> getBusinessArrangements(Practitioner practitioner, ProviderGroup group)
			throws AccountingServiceException {
		try {
			return manualDepositsRepository.getBusinessArrangements(practitioner, group);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.ManualDepositsService#getBusinessArrangements(ca.medavie.nspp.audit.service.data
	 * .ManualDeposit)
	 */
	@Override
	public List<BusinessArrangementCode> getBusinessArrangements(ManualDeposit manualDeposit)
			throws AccountingServiceException {
		try {
			return manualDepositsRepository.getBusinessArrangements(manualDeposit);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.ManualDepositsService#getSearchedManualDeposits(ca.medavie.nspp.audit.service.data
	 * .ManualDepositSearchCriteria, boolean, int, int)
	 */
	@Override
	public List<ManualDeposit> getSearchedManualDeposits(ManualDepositSearchCriteria aManualDepositSearchCriteria,
			boolean aPractitionerSearch, int startRow, int maxRows) throws AccountingServiceException {
		try {
			return manualDepositsRepository.getSearchedManualDeposits(aManualDepositSearchCriteria,
					aPractitionerSearch, startRow, maxRows);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.ManualDepositsService#getSearchedAuditCriteriaCount(ca.medavie.nspp.audit.service
	 * .data.ManualDepositSearchCriteria, boolean)
	 */
	@Override
	public Integer getSearchedAuditCriteriaCount(ManualDepositSearchCriteria aManualDepositSearchCriteria,
			boolean aPractitionerSearch) throws AccountingServiceException {
		try {
			return manualDepositsRepository.getSearchedAuditCriteriaCount(aManualDepositSearchCriteria,
					aPractitionerSearch);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.ManualDepositsService#getManualDepositDetails(ca.medavie.nspp.audit.service.data
	 * .ManualDeposit)
	 */
	@Override
	public ManualDeposit getManualDepositDetails(ManualDeposit aManualDeposit) throws AccountingServiceException {
		try {
			return manualDepositsRepository.getManualDepositDetails(aManualDeposit);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.ManualDepositsService#saveOrUpdateManualDeposit(ca.medavie.nspp.audit.service.data
	 * .ManualDeposit)
	 */
	@Override
	public void saveOrUpdateManualDeposit(ManualDeposit selectedManualDeposit) throws AccountingServiceException {
		try {
			manualDepositsRepository.saveOrUpdateManualDeposit(selectedManualDeposit);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.ManualDepositsService#deleteManualDeposit(ca.medavie.nspp.audit.service.data.
	 * ManualDeposit)
	 */
	@Override
	public void deleteManualDeposit(ManualDeposit selectedManualDeposit) throws AccountingServiceException {
		try {
			manualDepositsRepository.deleteManualDeposit(selectedManualDeposit);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.ManualDepositsService#getNextValueFromDssDepositNumberSeq()
	 */
	@Override
	public Long getNextValueFromDssDepositNumberSeq() throws AccountingServiceException {
		try {
			return manualDepositsRepository.getNextValueFromDssDepositNumberSeq();
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.ManualDepositsService#verifyPractitioner(ca.medavie.nspp.audit.service.data.
	 * ManualDeposit)
	 */
	@Override
	public boolean verifyPractitioner(ManualDeposit selectedManualDeposit) throws AccountingServiceException {
		try {
			return manualDepositsRepository.verifyPractitioner(selectedManualDeposit);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.ManualDepositsService#getManualDepositSearchedPractitioners(ca.medavie.nspp.audit
	 * .service.data.PractitionerSearchCriteria, int, int)
	 */
	@Override
	public List<Practitioner> getManualDepositSearchedPractitioners(
			PractitionerSearchCriteria aPractitionerSearchCriteria, int startRow, int maxRows)
			throws AccountingServiceException {
		try {
			return manualDepositsRepository.getManualDepositSearchedPractitioners(aPractitionerSearchCriteria,
					startRow, maxRows);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.ManualDepositsService#getManualDepositSearchedPractitionersCount(ca.medavie.nspp
	 * .audit.service.data.PractitionerSearchCriteria)
	 */
	@Override
	public Integer getManualDepositSearchedPractitionersCount(PractitionerSearchCriteria aPractitionerSearchCriteria)
			throws AccountingServiceException {
		try {
			return manualDepositsRepository.getManualDepositSearchedPractitionersCount(aPractitionerSearchCriteria);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.ManualDepositsService#getSearchedProviderGroups(ca.medavie.nspp.audit.service.data
	 * .ProviderGroupSearchCriteria, int, int)
	 */
	@Override
	public List<ProviderGroup> getSearchedProviderGroups(ProviderGroupSearchCriteria aProviderGroupSearchCriteria,
			int startRow, int maxRows) throws AccountingServiceException {
		try {
			return manualDepositsRepository.getSearchedProviderGroups(aProviderGroupSearchCriteria, startRow, maxRows);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ca.medavie.nspp.audit.service.ManualDepositsService#getSearchedProviderGroupsCount(ca.medavie.nspp.audit.service
	 * .data.ProviderGroupSearchCriteria)
	 */
	@Override
	public Integer getSearchedProviderGroupsCount(ProviderGroupSearchCriteria aProviderGroupSearchCriteria)
			throws AccountingServiceException {
		try {
			return manualDepositsRepository.getSearchedProviderGroupsCount(aProviderGroupSearchCriteria);
		} catch (Exception e) {
			throw new AccountingServiceException(e.getMessage(), e);
		}
	}
}
