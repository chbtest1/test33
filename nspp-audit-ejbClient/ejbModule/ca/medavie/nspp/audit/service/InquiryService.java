package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.exception.InquiryServiceException;

/**
 * Service layer interface for Inquiry business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface InquiryService {

	/**
	 * Loads Practitioners that match the provided search criteria (Searches only Practitioners currently in PeerGroups)
	 * 
	 * @param aPractitionerSearchCriteria
	 *            - User specified search Criteria
	 * @param startRow
	 * @param maxRows
	 * @return list of matching Practitioners
	 * @throws InquiryServiceException
	 */
	public List<Practitioner> getSearchedPractitioners(PractitionerSearchCriteria aPractitionerSearchCriteria,
			int startRow, int maxRows) throws InquiryServiceException;

	/**
	 * Loads the count of all records returned by a search without pagination enabled
	 * 
	 * @param aPractitionerSearchCriteria
	 * @return count of all records without pagination
	 * @throws InquiryServiceException
	 */
	public Integer getSearchedPractitionersCount(PractitionerSearchCriteria aPractitionerSearchCriteria)
			throws InquiryServiceException;

	/**
	 * Load in all details of the specified Practitioner
	 * 
	 * @param aPractitioner
	 *            - Shell of practitioner that will be populated. Also provides basic criteria for loading data
	 * @return the specified practitioner with in-depth details
	 * @throws InquiryServiceException
	 */
	public Practitioner getPractitionerDetails(Practitioner aPractitioner) throws InquiryServiceException;

	/**
	 * Loads PeerGroups that match the provider search criteria
	 * 
	 * @param aPeerGroupSearchCriteria
	 *            - User specified search Criteria
	 * @param startRow
	 * @param maxRows
	 * @return list of matching PeerGroups
	 * @throws InquiryServiceException
	 */
	public List<PeerGroup> getSearchPeerGroups(PeerGroupSearchCriteria aPeerGroupSearchCriteria, int startRow,
			int maxRows) throws InquiryServiceException;

	/**
	 * Loads the count of all records returned by a search without pagination enabled
	 * 
	 * @param aPeerGroupSearchCriteria
	 * @return count of all records without pagination
	 * @throws InquiryServiceException
	 */
	public Integer getSearchPeerGroupsCount(PeerGroupSearchCriteria aPeerGroupSearchCriteria)
			throws InquiryServiceException;

	/**
	 * Load in all details of the specified PeerGroup
	 * 
	 * @param aPeerGroup
	 *            - Shell of a PeerGroup that will be populated. Also provides basic criteria for loading data
	 * @return the specified PeerGroup with in-depth details
	 * @throws InquiryServiceException
	 */
	public PeerGroup getPeerGroupDetails(PeerGroup aPeerGroup) throws InquiryServiceException;

	/**
	 * Load all HSC found under the provided HealthServiceCode/Group
	 * 
	 * @param aPractitioner
	 * @param aHealthServiceCode
	 * @return list of HealthServiceCodes found during drill down search for the provided HealthServiceCode/Group
	 * @throws InquiryServiceException
	 */
	public List<HealthServiceCode> getHealthServiceDrillDownDetails(Practitioner aPractitioner,
			HealthServiceCode aHealthServiceCode) throws InquiryServiceException;
}
