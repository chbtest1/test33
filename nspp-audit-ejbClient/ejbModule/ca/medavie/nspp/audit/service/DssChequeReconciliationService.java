package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.ChequeReconciliation;
import ca.medavie.nspp.audit.service.data.ChequeReconciliationSearchCriteria;
import ca.medavie.nspp.audit.service.exception.DssChequeReconciliationException;

/**
 * Service layer interface for Cheque Reconciliation business logic. Connects the front end controllers to the EJB
 * layer.
 */
@Local
public interface DssChequeReconciliationService {

	/**
	 * Loads any Cheque Reconciliation records that match the search criteria
	 * 
	 * @param aChequeReconciliationSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return matching Cheque Reconciliation records
	 * @throws DssChequeReconciliationException
	 */
	public List<ChequeReconciliation> getChequeReconciliationSearchResults(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria, int startRow, int maxRows)
			throws DssChequeReconciliationException;

	/**
	 * Counts all rows that return by an Cheque Reconciliation search
	 * 
	 * @param aChequeReconciliationSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws DssChequeReconciliationException
	 */
	public Integer getChequeReconciliationSearchResultsCount(
			ChequeReconciliationSearchCriteria aChequeReconciliationSearchCriteria)
			throws DssChequeReconciliationException;

	/**
	 * Saves any changes made to the ChequeReconciliation record
	 * 
	 * @param aChequeReconciliation
	 * @throws DssChequeReconciliationException
	 */
	public void saveChequeReconciliation(ChequeReconciliation aChequeReconciliation)
			throws DssChequeReconciliationException;
}
