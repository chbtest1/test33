package ca.medavie.nspp.audit.service;

public class ServicePersistenceException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ServicePersistenceException(String message) {
		super(message);
	}

	public ServicePersistenceException(Exception pe) {
		super(pe);
	}
}
