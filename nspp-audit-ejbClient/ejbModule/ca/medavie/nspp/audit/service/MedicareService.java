package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.BusinessArrangement;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.IndividualExclusion;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;

/**
 * Service layer interface for Medicare business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface MedicareService {

	/**
	 * Loads matching HealthServiceCriteria records for Description update
	 * 
	 * @param aSearchCriteria
	 * @return list of matching HealthServiceCriterias for Description update
	 * @throws MedicareServiceException
	 */
	public List<HealthServiceCriteria> getSearchedHealthServiceCriteriaDescriptions(
			HealthServiceCriteriaSearchCriteria aSearchCriteria) throws MedicareServiceException;

	/**
	 * Saves the changes made to the HealthServiceCriteria description.
	 * 
	 * @param aListOfHsc
	 *            - HSC records to save description changes
	 * @throws MedicareServiceException
	 */
	public void saveHealthServiceCriteriaDescriptions(List<HealthServiceCriteria> aListOfHsc)
			throws MedicareServiceException;

	/**
	 * Loads the current HealthServiceCriteria Exclusions
	 * 
	 * @return current HealthServiceCriteria Exclusions
	 * @throws MedicareServiceException
	 */
	public List<HealthServiceCriteria> getHealthServiceCriteriaExclusions() throws MedicareServiceException;

	/**
	 * Saves the new and updated HealthServiceCriteria exclusions to the database
	 * 
	 * @param aListOfHscForSave
	 * @throws MedicareServiceException
	 */
	public void saveHealthServiceCriteriaExclusions(List<HealthServiceCriteria> aListOfHscForSave)
			throws MedicareServiceException;

	/**
	 * Deletes the specified HealthServiceExclusions from the database
	 * 
	 * @param aListOfHscForDelete
	 * @throws MedicareServiceException
	 */
	public void deleteHealthServiceCriteriaExclusions(List<HealthServiceCriteria> aListOfHscForDelete)
			throws MedicareServiceException;

	/**
	 * Load the current Individual Exclusions
	 * 
	 * @return current Individual Exclusions
	 * @throws MedicareServiceException
	 */
	public List<IndividualExclusion> getIndividualExclusions() throws MedicareServiceException;

	/**
	 * Saves the new and updated Individual Exclusions to the database
	 * 
	 * @param aListOfIndExclToSave
	 * @throws MedicareServiceException
	 */
	public void saveIndividualExclusions(List<IndividualExclusion> aListOfIndExclToSave)
			throws MedicareServiceException;

	/**
	 * Attempts the owners name of the provided Health Card Number
	 * 
	 * @param aHealthCardNumber
	 * @return matching name if found. Otherwise no value is returned
	 * @throws MedicareServiceException
	 */
	public String getHealthCardNumberOwnerName(String aHealthCardNumber) throws MedicareServiceException;

	/**
	 * Saves the specified AuditCriteria record
	 * 
	 * @param anAuditCriteria
	 * @throws MedicareServiceException
	 */
	public void saveMedicareAuditCriteria(AuditCriteria anAuditCriteria) throws MedicareServiceException;

	/**
	 * Attempts to load the Practitioner Name via specified arguments
	 * 
	 * @param aPractitionerId
	 * @param aSubcategory
	 * @return Practitioner name if found, otherwise null
	 * @throws MedicareServiceException
	 */
	public String getPractitionerName(Long aPractitionerId, String aSubcategory) throws MedicareServiceException;

	/**
	 * Attempts to locate an existing Audit Criteria via specified arguments
	 * 
	 * @param aPractitionerId
	 * @param aSubcategory
	 * @param anAuditType
	 * @return AuditCriteria if found, otherwise null
	 * @throws MedicareServiceException
	 */
	public AuditCriteria getMedicarePractitionerAuditCriteria(Long aPractitionerId, String aSubcategory,
			String anAuditType) throws MedicareServiceException;

	/**
	 * Loads any matching AuditCriteria records using the specified Search Criteria
	 * 
	 * @param aSearchCriteria
	 * @param startRow
	 *            - Row number the result set should begin
	 * @param maxRows
	 *            - Maximum number of results to return
	 * @return any matching AuditCriteria records
	 * @throws MedicareServiceException
	 */
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows) throws MedicareServiceException;

	/**
	 * Counts all rows that return by an Audit Criteria search
	 * 
	 * @param anAuditCriteriaSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws MedicareServiceException
	 */
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria)
			throws MedicareServiceException;

	/**
	 * Attempts to load AuditCriteria record by ID
	 * 
	 * @param anId
	 *            - ID of the desired AuditCriteria
	 * @return AuditCriteria with the specified ID
	 * @throws MedicareServiceException
	 */
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId) throws MedicareServiceException;

	/**
	 * Attempts to load a Default AuditCriteria by the specified AuditType
	 * 
	 * @param anAuditType
	 * @return the matching audit if it exists or a brand new AuditCriteria object if nothing is found
	 * @throws MedicareServiceException
	 */
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType) throws MedicareServiceException;

	/**
	 * Do search Medicare audit letter response
	 * 
	 * @param medicareALRSearchCriteria
	 * @param startRow
	 *            - Starting row the query should return
	 * @param maxRows
	 *            - Maximum number of rows that can be returned by search
	 * 
	 * @return List<DssMedSeAudit>
	 * @throws MedicareServiceException
	 */
	public List<MedicareAuditLetterResponse> getSearchedMedicareALR(
			MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria, int startRow, int maxRows)
			throws MedicareServiceException;

	/**
	 * Performs a record count of the entire possible dataset returned by the provided search criteria
	 * 
	 * @param medicareALRSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws MedicareServiceException
	 */
	public Integer getSearchedMedicareALRCount(MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria)
			throws MedicareServiceException;

	/**
	 * 
	 * Do save medicare audit letter response result
	 * 
	 * @param selectedMCALR
	 * @throws MedicareServiceException
	 */
	public void saveMedicareALR(MedicareAuditLetterResponse selectedMCALR) throws MedicareServiceException;

	/**
	 * Retrieve the Number Of Letters Created by Audit Run Number and Audit Type. This is only used when the user
	 * provided the Audit Run Number and Audit Type in their search criteria. Displayed above the search results table.
	 * 
	 * To prevent duplicate code this is shared amongst of 3 parts of Service Verification (Medicare, Pharmacare and
	 * Denticare)
	 * 
	 * @param anAuditRunNumber
	 * @param anAuditType
	 * @return number of letters created
	 */
	public Long getNumberOfLettersCreated(Long anAuditRunNumber, String anAuditType) throws MedicareServiceException;

	/**
	 * Load the specified Business Arrangement
	 * 
	 * @param aBusinessArrangementNumber
	 * @return Business Arrangement with the specified BA number
	 * @throws MedicareServiceException
	 */
	public BusinessArrangement getBusinessArrangement(Long aBusinessArrangementNumber) throws MedicareServiceException;

	/**
	 * Load all existing Business Arrangement Exclusions
	 * 
	 * @return all Business Arrangement Exclusions in the system
	 * @throws MedicareServiceException
	 */
	public List<BusinessArrangement> getBusinessArrangementExclusions() throws MedicareServiceException;

	/**
	 * Saves a new Exclusion record for the specified Business Arrangement
	 * 
	 * @param aBusinessArrangementExclusion
	 * @throws MedicareServiceException
	 */
	public void saveBusinessArrangementExclusion(BusinessArrangement aBusinessArrangementExclusion)
			throws MedicareServiceException;

	/**
	 * Deletes the specified Business Arrangement Exclusions from the system
	 * 
	 * @param aListOfBAExclusionsForDelete
	 * @throws MedicareServiceException
	 */
	public void deleteBusinessArrangementExclusions(List<BusinessArrangement> aListOfBAExclusionsForDelete)
			throws MedicareServiceException;
}
