package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.AuditResult;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.exception.AuditResultServiceException;

/**
 * Service layer interface for AuditResultService business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface AuditResultService {

	/**
	 * Get practitioner search result by given search criteria
	 * 
	 * @param PractitionerSearchCriteria
	 *            - a given PractitionerSearchCriteria
	 * @return List<Practitioner> - a List of Practitioner by a given PractitionerSearchCriteria
	 * @throws AuditResultServiceException
	 * 
	 * **/
	public List<Practitioner> getPractitionerSearchResult(PractitionerSearchCriteria aPractitionerSearchCriteria)
			throws AuditResultServiceException;

	/**
	 * Save or update Audit result
	 * 
	 * @param AuditResult
	 *            - a given AuditResult to save
	 * @throws AuditResultServiceException
	 * 
	 * **/
	public void setAuditResult(AuditResult anAuditedResultToSave) throws AuditResultServiceException;

	/**
	 * @param aSelectedPractitioner
	 * @return
	 * @throws AuditResultServiceException
	 */
	public List<AuditResult> getSelectedPractitionerAuditResultDetails(Practitioner aSelectedPractitioner)
			throws AuditResultServiceException;

	/**
	 * Gets a List of all valid Health Service Codes
	 * 
	 * @return List of Health Service Codes
	 * @throws AuditResultServiceException
	 */
	public List<String> getHealthServiceCodes() throws AuditResultServiceException;

	/**
	 * Gets an attachment file binary
	 * 
	 * @param anAttachmentId
	 * @return
	 * @throws AuditResultServiceException
	 */
	public byte[] getAttachmentBinary(Long anAttachmentId) throws AuditResultServiceException;

	/**
	 * Saves an attachment file binary
	 * 
	 * @param anAttachmentId
	 *            - the attachment id for the record to save it in
	 * @param anAttachmentBinary
	 *            - a given attachment file binary to save
	 * @throws AuditResultServiceException
	 */
	public void setAttachmentBinary(Long anAttachmentId, byte[] anAttachmentBinary) throws AuditResultServiceException;

	
	/**
	 * Looks up health service code description
	 * 
	 * @param healthServiceCode
	 * 			- the health service code
	 * @return
	 * 			- health service code description
	 * @throws AuditResultServiceException
	 */
	public String getReason(String healthServiceCode, String qualifer) throws AuditResultServiceException;

	public void deleteAuditResult(AuditResult aSelectedAuditResult) throws AuditResultServiceException;
}
