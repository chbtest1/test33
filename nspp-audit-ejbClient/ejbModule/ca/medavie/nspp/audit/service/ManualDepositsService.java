package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.BusinessArrangementCode;
import ca.medavie.nspp.audit.service.data.ManualDeposit;
import ca.medavie.nspp.audit.service.data.ManualDepositSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.ProviderGroup;
import ca.medavie.nspp.audit.service.data.ProviderGroupSearchCriteria;
import ca.medavie.nspp.audit.service.exception.AccountingServiceException;
import ca.medavie.nspp.audit.service.exception.InquiryServiceException;

/**
 * Service layer interface for Accounting business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface ManualDepositsService {
	/**
	 * Returns all business arrangements for a given practitioner and/or provider group.
	 * 
	 * @param practitioner
	 * @param group
	 * @return
	 * @throws AccountingServiceException
	 */
	public List<BusinessArrangementCode> getBusinessArrangements(Practitioner practitioner, ProviderGroup group)
			throws AccountingServiceException;

	/**
	 * Returns all possible business arrangements for a given manual deposit.
	 * 
	 * @param manualDeposit
	 * @return
	 * @throws AccountingServiceException
	 */
	public List<BusinessArrangementCode> getBusinessArrangements(ManualDeposit manualDeposit)
			throws AccountingServiceException;

	/**
	 * Get all manual deposits limited by search criteria.
	 * 
	 * @param aManualDepositSearchCriteria
	 * @param aPractitionerSearch
	 * @param startRow
	 * @param maxRows
	 * @return matching ManualDeposits
	 * @throws AccountingServiceException
	 */
	public List<ManualDeposit> getSearchedManualDeposits(ManualDepositSearchCriteria aManualDepositSearchCriteria,
			boolean aPractitionerSearch, int startRow, int maxRows) throws AccountingServiceException;

	/**
	 * Counts all rows that return by an ManualDeposits search
	 * 
	 * @param aManualDepositSearchCriteria
	 * @param aPractitionerSearch
	 * @return - total row count for entire search dataset
	 * @throws AccountingServiceException
	 */
	public Integer getSearchedAuditCriteriaCount(ManualDepositSearchCriteria aManualDepositSearchCriteria,
			boolean aPractitionerSearch) throws AccountingServiceException;

	/**
	 * Get manual deposit details for manual deposit given.
	 * 
	 * @param aManualDeposit
	 * @return
	 * @throws AccountingServiceException
	 */
	public ManualDeposit getManualDepositDetails(ManualDeposit aManualDeposit) throws AccountingServiceException;

	/**
	 * 
	 * @param selectedManualDeposit
	 * @throws AccountingServiceException
	 * @throws AccountingServiceException
	 */
	public void saveOrUpdateManualDeposit(ManualDeposit selectedManualDeposit) throws AccountingServiceException;

	/**
	 * Retrieves next sequence number for deposit number.
	 * 
	 * @return
	 * @throws AccountingServiceException
	 */
	public Long getNextValueFromDssDepositNumberSeq() throws AccountingServiceException;

	/**
	 * @param selectedManualDeposit
	 * @throws AccountingServiceException
	 * @throws AccountingServiceException
	 */
	public void deleteManualDeposit(ManualDeposit selectedManualDeposit) throws AccountingServiceException;

	/**
	 * Checks that the practitioner info in given manual deposit is valid for a practitioner in the database. Used when
	 * the payor is a group and the practitioner info is optional.
	 * 
	 * @param selectedManualDeposit
	 * @return
	 * @throws AccountingServiceException
	 */
	public boolean verifyPractitioner(ManualDeposit selectedManualDeposit) throws AccountingServiceException;

	/**
	 * Searches for practitioners when adding a manual deposit.
	 * 
	 * @param practitionerSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return
	 * @throws InquiryServiceException
	 */
	public List<Practitioner> getManualDepositSearchedPractitioners(
			PractitionerSearchCriteria practitionerSearchCriteria, int startRow, int maxRows)
			throws AccountingServiceException;

	/**
	 * Counts all rows that return by an Practitioner search
	 * 
	 * @param practitionerSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws AccountingServiceException
	 */
	public Integer getManualDepositSearchedPractitionersCount(PractitionerSearchCriteria practitionerSearchCriteria)
			throws AccountingServiceException;

	/**
	 * Get all provider groups limited by search criteria.
	 * 
	 * @param aProviderGroupSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return
	 * @throws AccountingServiceException
	 */
	public List<ProviderGroup> getSearchedProviderGroups(ProviderGroupSearchCriteria aProviderGroupSearchCriteria,
			int startRow, int maxRows) throws AccountingServiceException;

	/**
	 * Counts all rows that return by an Group search
	 * 
	 * @param aProviderGroupSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws AccountingServiceException
	 */
	public Integer getSearchedProviderGroupsCount(ProviderGroupSearchCriteria aProviderGroupSearchCriteria)
			throws AccountingServiceException;
}
