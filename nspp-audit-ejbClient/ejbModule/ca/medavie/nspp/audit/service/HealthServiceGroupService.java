package ca.medavie.nspp.audit.service;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.HealthServiceCodeSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceGroup;
import ca.medavie.nspp.audit.service.data.HealthServiceGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceProgram;
import ca.medavie.nspp.audit.service.data.Modifier;
import ca.medavie.nspp.audit.service.exception.HealthServiceGroupServiceException;

/**
 * Service layer interface for HS Codes business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface HealthServiceGroupService {

	/**
	 * Loads all matching HealthServiceGroups using the provided Search Criteria
	 * 
	 * @param aSearchCriteria
	 *            - user specified search criteria
	 * @param startRow
	 * @param maxRows
	 * @return list of matching HealthServiceGroups
	 */
	public List<HealthServiceGroup> getSearchHealthServiceGroups(HealthServiceGroupSearchCriteria aSearchCriteria,
			int startRow, int maxRows) throws HealthServiceGroupServiceException;

	/**
	 * Counts all rows that return by an HSG search
	 * 
	 * @param aSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws HealthServiceGroupServiceException
	 */
	public Integer getSearchHealthServiceGroupsCount(HealthServiceGroupSearchCriteria aSearchCriteria)
			throws HealthServiceGroupServiceException;

	/**
	 * Loads all matching HealthServiceCodes using the provided Search Criteria
	 * 
	 * @param aSearchCriteria
	 *            - user specified search criteria
	 * @return list of matching HealthServiceCodes
	 */
	public List<HealthServiceCode> getSearchHealthServiceCodes(HealthServiceCodeSearchCriteria aSearchCriteria)
			throws HealthServiceGroupServiceException;

	/**
	 * Loads all HealthServiceCodes that are found under the provided HealthServiceGroup id
	 * 
	 * @param aHealthServiceGroupId
	 * @return list of HealthServiceCodes that belong to the provider HealthServiceGroup id
	 */
	public List<HealthServiceCode> getHealthServiceCodes(Long aHealthServiceGroupId)
			throws HealthServiceGroupServiceException;

	/**
	 * Saves changes made to the HealthServiceGroup.
	 * 
	 * @param aHealthServiceGroup
	 *            - the healthServiceGroup record to update
	 * @param aListOfHSCToAdd
	 *            - HSC records to be added to the group
	 */
	public void saveHealthServiceGroup(HealthServiceGroup aHealthServiceGroup, List<HealthServiceCode> aListOfHSCToAdd)
			throws HealthServiceGroupServiceException;

	/**
	 * Save a brand new HealthServiceGroup
	 * 
	 * @param aHealthServiceGroup
	 *            - new healthServiceGroup to save
	 */
	public void saveNewHealthServiceGroup(HealthServiceGroup aHealthServiceGroup)
			throws HealthServiceGroupServiceException;

	/**
	 * Validates that a new HealthServiceGroup is not using an ID that is already used by another group
	 * 
	 * @param aHealtherServiceGroupId
	 * @return true if ID is already used
	 * @throws HealthServiceGroupServiceException
	 */
	public boolean isHealthServiceGroupDuplicate(Long aHealtherServiceGroupId)
			throws HealthServiceGroupServiceException;

	/**
	 * Deletes the specified Codes from the specified group
	 * 
	 * @param aHealthServiceGroupId
	 * @param aListOfCodesForDelete
	 * @throws HealthServiceGroupServiceException
	 */
	public void deleteHealthServiceCodesFromGroup(Long aHealthServiceGroupId,
			List<HealthServiceCode> aListOfCodesForDelete) throws HealthServiceGroupServiceException;

	/**
	 * Load available Programs used with HealthServiceGroups
	 * 
	 * @return healthService program list
	 */
	public List<HealthServiceProgram> getHealthServicePrograms() throws HealthServiceGroupServiceException;

	/**
	 * Load all Modifiers
	 * 
	 * @return list of all Modifiers
	 */
	public List<Modifier> getModifiers() throws HealthServiceGroupServiceException;

	/**
	 * Load all Implicit Modifiers
	 * 
	 * @return list of all available ImplicitModifiers
	 */
	public List<Modifier> getImplicitModifiers() throws HealthServiceGroupServiceException;

	/**
	 * Loads all the HealthServicesCodes that currently do not belong to a HealthServiceGroup
	 * 
	 * @param aYearEndDate
	 *            - yearEndDate used to filter on services not in groups
	 * 
	 * @return list of HealthSerivceCodes currently not in a HealthServiceGroup
	 * @throws HealthServiceGroupServiceException
	 */
	public List<HealthServiceCode> getServicesNotInGroup(Date aYearEndDate) throws HealthServiceGroupServiceException;
}
