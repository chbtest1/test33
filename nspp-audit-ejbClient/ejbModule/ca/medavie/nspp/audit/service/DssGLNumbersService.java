package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.DssGLNumberBO;
import ca.medavie.nspp.audit.service.exception.DssGLNumbersServiceException;

@Local
public interface DssGLNumbersService {

	/**
	 * Loads all GL Numbers in the systems
	 * 
	 * @return all GL Numbers currently available.
	 * @throws DssGLNumbersServiceException
	 */
	public List<DssGLNumberBO> getGLNumbers() throws DssGLNumbersServiceException;

	/**
	 * Attempt to load a GL Number record by the specified GL Number. If nothing is found return null
	 * 
	 * @param aGlNumber
	 * @return the GLNumber record that uses the specified GL Number
	 * @throws DssGLNumbersServiceException
	 */
	public DssGLNumberBO getGLNumberByGL(Long aGlNumber) throws DssGLNumbersServiceException;

	/**
	 * Saves an existing (Changes) or new GL Number to the system.
	 * 
	 * @param aDssGLNumber
	 *            - GL Number to update or save
	 * @throws DssGLNumbersServiceException
	 */
	public void saveGLNumber(DssGLNumberBO aDssGLNumber) throws DssGLNumbersServiceException;

	/**
	 * Deletes the selected GL Numbers from the system.
	 * 
	 * @param aListOfGLNumbersForDelete
	 *            - GL Numbers to delete
	 * @throws DssGLNumbersServiceException
	 */
	public void deleteGLNumbers(List<DssGLNumberBO> aListOfGLNumbersForDelete) throws DssGLNumbersServiceException;
}
