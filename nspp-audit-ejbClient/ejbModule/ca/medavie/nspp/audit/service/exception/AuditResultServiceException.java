package ca.medavie.nspp.audit.service.exception;

public class AuditResultServiceException extends ServiceException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5206925340119667531L;
	/**
	 * a message key linking to this exception
	 */
	private static final String MESSAGE_KEY = "ERROR.AUDIT.RESULT.SERVICE.EXCEPTION";

	/**
	 * Public constructor which takes a string message to be included in the exception message
	 * 
	 * @param aMessage
	 *            text message
	 */
	public AuditResultServiceException(String aMessage) {
		super(aMessage);
	}

	/**
	 * Public constructor which takes a string message to be included in the exception message and a instance of
	 * Throwable. Used when chaining exceptions
	 * 
	 * @param aMessage
	 *            text message
	 * @param anException
	 *            a Throwable exeception
	 */
	public AuditResultServiceException(String aMessage, Throwable anException) {
		super(aMessage, anException);
	}

	@Override
	public String getMessageCode() {

		return MESSAGE_KEY;
	}

}
