package ca.medavie.nspp.audit.service.exception;

public class DssGLNumbersServiceException extends ServiceException {

	/** IDE Generated */
	private static final long serialVersionUID = 1554827596031098494L;
	/**
	 * a message key linking to this exception
	 */
	private static final String MESSAGE_KEY = "ERROR.DSS.GL.NUMBERS.SERVICE";

	public DssGLNumbersServiceException(String aMessage) {
		super(aMessage);
	}

	/**
	 * Public constructor which takes a string message to be included in the exception message and a instance of
	 * Throwable. Used when chaining exceptions
	 * 
	 * @param aMessage
	 *            text message
	 * @param anException
	 *            a Throwable exeception
	 */
	public DssGLNumbersServiceException(String aMessage, Throwable anException) {
		super(aMessage, anException);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.exception.ServiceException#getMessageCode()
	 */
	@Override
	public String getMessageCode() {
		return MESSAGE_KEY;
	}
}
