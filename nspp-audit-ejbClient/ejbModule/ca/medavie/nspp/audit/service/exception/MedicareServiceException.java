package ca.medavie.nspp.audit.service.exception;

public class MedicareServiceException extends ServiceException {

	/** IDE Generated */
	private static final long serialVersionUID = 7363416254325306237L;

	/**
	 * Message key linking to this exception
	 */
	private static final String MESSAGE_KEY = "ERROR.MEDICARE.SERVICE";

	/**
	 * Public constructor which takes a string message to be included in the exception message
	 * 
	 * @param aMessage
	 *            text message
	 */
	public MedicareServiceException(String aMessage) {
		super(aMessage);
	}

	/**
	 * Public constructor which takes a string message to be included in the exception message and a instance of
	 * Throwable. Used when chaining exceptions
	 * 
	 * @param aMessage
	 *            text message
	 * @param anException
	 *            a Throwable exeception
	 */
	public MedicareServiceException(String aMessage, Throwable anException) {
		super(aMessage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.exception.ServiceException#getMessageCode()
	 */
	@Override
	public String getMessageCode() {
		return MESSAGE_KEY;
	}

}
