package ca.medavie.nspp.audit.service.exception;

public class DssChequeReconciliationException extends ServiceException {

	/** IDE Generated */
	private static final long serialVersionUID = 4917344549837646001L;

	/**
	 * a message key linking to this exception
	 */
	private static final String MESSAGE_KEY = "ERROR.DSS.CHEQUE.RECONCILIATION.SERVICE.EXCEPTION";

	/**
	 * Public constructor which takes a string message to be included in the exception message
	 * 
	 * @param aMessage
	 *            text message
	 */
	public DssChequeReconciliationException(String aMessage) {
		super(aMessage);
	}

	/**
	 * Public constructor which takes a string message to be included in the exception message and a instance of
	 * Throwable. Used when chaining exceptions
	 * 
	 * @param aMessage
	 *            text message
	 * @param anException
	 *            a Throwable exeception
	 */
	public DssChequeReconciliationException(String aMessage, Throwable anException) {
		super(aMessage, anException);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.exception.ServiceException#getMessageCode()
	 */
	@Override
	public String getMessageCode() {
		return MESSAGE_KEY;
	}
}
