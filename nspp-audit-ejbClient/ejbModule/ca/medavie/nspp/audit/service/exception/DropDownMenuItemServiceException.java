package ca.medavie.nspp.audit.service.exception;

public class DropDownMenuItemServiceException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3544397703169479766L;
	/**
	 * a message key linking to this exception
	 */
	private static final String MESSAGE_KEY = "ERROR.DROP.DOWN.MENU.SERVICE.EXCEPTION";

	/**
	 * Public constructor which takes a string message to be included in the exception message
	 * 
	 * @param aMessage
	 *            text message
	 */
	public DropDownMenuItemServiceException(String aMessage) {
		super(aMessage);
	}

	/**
	 * Public constructor which takes a string message to be included in the exception message and a instance of
	 * Throwable. Used when chaining exceptions
	 * 
	 * @param aMessage
	 *            text message
	 * @param anException
	 *            a Throwable exeception
	 */
	public DropDownMenuItemServiceException(String aMessage, Throwable anException) {
		super(aMessage, anException);
	}

	@Override
	public String getMessageCode() {

		return MESSAGE_KEY;
	}

}
