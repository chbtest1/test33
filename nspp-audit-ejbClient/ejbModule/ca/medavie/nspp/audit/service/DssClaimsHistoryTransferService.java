package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.DenticareClaim;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferRequest;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferSummary;
import ca.medavie.nspp.audit.service.data.HealthCard;
import ca.medavie.nspp.audit.service.data.MedicareClaim;
import ca.medavie.nspp.audit.service.data.PharmacareClaim;
import ca.medavie.nspp.audit.service.exception.DssClaimsHistoryTransferException;

/**
 * Service layer for DSS Claims History Transfer functionality
 */
@Local
public interface DssClaimsHistoryTransferService {

	/**
	 * Attempts to load the matching HealthCard record using the user specified search criteria
	 * 
	 * @param aHealthCardNumber
	 * @return matching HealthCard
	 * @throws DssClaimsHistoryTransferException
	 */
	public HealthCard getSearchedHealthCard(String aHealthCardNumber) throws DssClaimsHistoryTransferException;

	/**
	 * @param aTransferRequest
	 * @return list of Medicare SE Claims found under the specified HCN and dates if provided in initial search
	 * @throws DssClaimsHistoryTransferException
	 */
	public List<MedicareClaim> getMedicareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException;

	/**
	 * @param aTransferRequest
	 * @return list of Medicare Mainframe Claims found under the specified HCN and dates if provided in initial search
	 * @throws DssClaimsHistoryTransferException
	 */
	public List<MedicareClaim> getMedicareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException;

	/**
	 * @param aTransferRequest
	 * @return list of Pharmacare SE Claims found under the specified HCN and dates if provided in initial search
	 * @throws DssClaimsHistoryTransferException
	 */
	public List<PharmacareClaim> getPharmacareSeClaims(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException;

	/**
	 * @param aTransferRequest
	 * @return list of Pharmacare Mainframe Claims found under the specified HCN and dates if provided in initial search
	 * @throws DssClaimsHistoryTransferException
	 */
	public List<PharmacareClaim> getPharmacareMainframeClaims(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException;

	/**
	 * @param aTransferRequest
	 * @return list of Dental Claims found under the specified HCN and dates if provided in initial search
	 * @throws DssClaimsHistoryTransferException
	 */
	public List<DenticareClaim> getDentalClaims(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException;

	/**
	 * Loads the required data for the History Transfer to the staging table
	 * 
	 * @param aTransferRequest
	 * @throws DssClaimsHistoryTransferException
	 */
	public void saveClaimsHistoryTransferStagingData(DssClaimsHistoryTransferRequest aTransferRequest)
			throws DssClaimsHistoryTransferException;

	/**
	 * Triggers the stored procedure 'P_EXE_DSS_HISTORY_TRANSFER' to copy the claims data
	 * 
	 * @param aSourceHcn
	 * @param aTargetHcn
	 * @throws DssClaimsHistoryTransferException
	 */
	public void executeClaimsHistoryTransferProcedure(String aSourceHcn, String aTargetHcn)
			throws DssClaimsHistoryTransferException;

	/**
	 * Loads the summary details for the transfer made between the 2 specified Health Card Numbers
	 * 
	 * @param aSourceHcn
	 * @param aTargetHcn
	 * @return transfer summary
	 * @throws DssClaimsHistoryTransferException
	 */
	public DssClaimsHistoryTransferSummary getClaimsHistoryTransferSummary(String aSourceHcn, String aTargetHcn)
			throws DssClaimsHistoryTransferException;
}
