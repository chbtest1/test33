package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.Locum;
import ca.medavie.nspp.audit.service.data.LocumSearchCriteria;
import ca.medavie.nspp.audit.service.exception.LocumServiceException;

/**
 * Service layer interface for Locum Hosts business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface LocumService {

	/**
	 * Search for matching Locums based on the user defined search criteria
	 * 
	 * @param aLocumSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return list of matching Locums
	 */
	public List<Locum> getSearchedLocums(LocumSearchCriteria aLocumSearchCriteria, int firstRow, int maxRows)
			throws LocumServiceException;

	/**
	 * Counts all rows that return by an Locum search
	 * 
	 * @param aLocumSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws LocumServiceException
	 */
	public Integer getSearchedLocumsCount(LocumSearchCriteria aLocumSearchCriteria) throws LocumServiceException;

	/**
	 * Search for matching Practitioners based on the user defined search criteria. Practitioners found can be added to
	 * a Locum record as HOST or VISITOR
	 * 
	 * @param aLocumSearchCriteria
	 * @return list of matching LocumPractitioner
	 * @throws LocumServiceException
	 */
	public List<Locum> getSearchedLocumPractitioners(LocumSearchCriteria aLocumSearchCriteria)
			throws LocumServiceException;

	/**
	 * Loads the details of the specified Locum when selected from the search results
	 * 
	 * @param aLocum
	 *            - Locum record to load more details of
	 * @return populated Locum record
	 * @throws LocumServiceException
	 */
	public Locum getLocumDetails(Locum aLocum) throws LocumServiceException;

	/**
	 * Saves all changes made to the Locum record
	 * 
	 * @param aLocum
	 * @throws LocumServiceException
	 */
	public void saveLocum(Locum aLocum) throws LocumServiceException;

	/**
	 * Deletes the provided locums from the parent locum
	 * 
	 * @param aListOfLocumsForDelete
	 *            - Locums to delete from the parent
	 * @throws LocumServiceException
	 */
	public void deleteLocums(List<Locum> aListOfLocumsForDelete) throws LocumServiceException;
}
