package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DIN;
import ca.medavie.nspp.audit.service.data.DinSearchCriteria;
import ca.medavie.nspp.audit.service.data.IndividualExclusion;
import ca.medavie.nspp.audit.service.data.PharmAuditDinExcl;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.exception.PharmcareServiceException;

/**
 * Service layer interface for PharmacareService business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface PharmacareService {

	/**
	 * Get Pharm care audit letter responds search result
	 * 
	 * @param pharmCareALRSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return A list of PharmCareAuditLetterRespondse by given search criteria
	 */
	public List<PharmacareAuditLetterResponse> getSearchedPharmacareALR(
			PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria, int startRow, int maxRows)
			throws PharmcareServiceException;

	/**
	 * Counts all rows that return by an ALR search
	 * 
	 * @param pharmCareALRSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws PharmcareServiceException
	 */
	public Integer getSearchedPharmacareALRCount(PharmacareAuditLetterResponseSearchCriteria pharmCareALRSearchCriteria)
			throws PharmcareServiceException;

	/**
	 * Save pharm care audit letter responds changes
	 * 
	 * @param selectedDCALR
	 * @throws PharmcareServiceException
	 */
	public void savePharmacareALR(PharmacareAuditLetterResponse selectedDCALR) throws PharmcareServiceException;

	/**
	 * Loads any matching AuditCriteria records using the specified Search Criteria
	 * 
	 * @param aSearchCriteria
	 * @param startRow
	 * @param maxRows
	 * @return any matching AuditCriteria records
	 * @throws PharmcareServiceException
	 */
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows) throws PharmcareServiceException;

	/**
	 * Counts all rows that return by an Audit Criteria search
	 * 
	 * @param anAuditCriteriaSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws PharmcareServiceException
	 */
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria)
			throws PharmcareServiceException;

	/**
	 * Attempts to load a Default AuditCriteria by the specified AuditType
	 * 
	 * @param anAuditType
	 * @return the matching audit if it exists or a brand new AuditCriteria object if nothing is found
	 * @throws PharmcareServiceException
	 */
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType) throws PharmcareServiceException;

	/**
	 * Attempts to load AuditCriteria record by ID
	 * 
	 * @param anId
	 *            - ID of the desired AuditCriteria
	 * @return AuditCriteria with the specified ID
	 * @throws PharmcareServiceException
	 */
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId) throws PharmcareServiceException;

	/**
	 * Saves the specified AuditCriteria record
	 * 
	 * @param anAuditCriteria
	 * @throws PharmcareServiceException
	 */
	public void savePharmicareAuditCriteria(AuditCriteria anAuditCriteria) throws PharmcareServiceException;

	/**
	 * Loads any matching DINs using the search criteria
	 * 
	 * @param aSearchCriteria
	 * @return list of DIN records that matched the search criteria
	 * @throws PharmcareServiceException
	 */
	public List<DIN> getSearchDins(DinSearchCriteria aSearchCriteria) throws PharmcareServiceException;

	/**
	 * Load the current Individual Exclusions
	 * 
	 * @return current Individual Exclusions
	 * @throws PharmcareServiceException
	 */
	public List<IndividualExclusion> getIndividualExclusions() throws PharmcareServiceException;

	/**
	 * Saves the new and updated Individual Exclusions to the database
	 * 
	 * @param aListOfIndExclToSave
	 * @throws PharmcareServiceException
	 */
	public void saveIndividualExclusions(List<IndividualExclusion> aListOfIndExclToSave)
			throws PharmcareServiceException;
	
	/**
	 * Gets a list of Pharm Audit Din Excls
	 * @param din
	 * @throws PharmcareServiceException
	 */
	public List<PharmAuditDinExcl> getPharmAuditDinExcl() throws PharmcareServiceException;

	public void saveDinExclusions(List<PharmAuditDinExcl> pharmAuditDinExclResults)
			throws PharmcareServiceException;

	public void deleteDinExcl(PharmAuditDinExcl pharmAuditDinExcl)
			throws PharmcareServiceException;
}
