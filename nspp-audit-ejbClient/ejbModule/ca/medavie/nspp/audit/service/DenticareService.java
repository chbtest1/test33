package ca.medavie.nspp.audit.service;

import java.util.List;

import javax.ejb.Local;

import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.exception.DenticareServiceException;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;

/**
 * Service layer interface for DenticareService business logic. Connects the front end controllers to the EJB layer.
 */
@Local
public interface DenticareService {

	/**
	 * Loads any matching AuditCriteria records using the specified Search Criteria
	 * 
	 * @param aSearchCriteria
	 * @param startRow
	 *            - Row number the result set should begin
	 * @param maxRows
	 *            - Maximum number of results to return
	 * @return any matching AuditCriteria records
	 * @throws MedicareServiceException
	 */
	public List<AuditCriteria> getSearchPractitionerAuditCriterias(AuditCriteriaSearchCriteria aSearchCriteria,
			int startRow, int maxRows) throws DenticareServiceException;

	/**
	 * Counts all rows that return by an Audit Criteria search
	 * 
	 * @param anAuditCriteriaSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws DenticareServiceException
	 */
	public Integer getSearchedAuditCriteriaCount(AuditCriteriaSearchCriteria anAuditCriteriaSearchCriteria)
			throws DenticareServiceException;

	/**
	 * Get Denticare audit letter responds search result
	 * 
	 * @param denticareALRSearchCriteria
	 * @param startRow
	 *            - Row number the result set should begin
	 * @param maxRows
	 *            - Maximum number of results to return
	 * @return A list of DenticareAuditLetterResponse by given search criteria
	 */
	public List<DenticareAuditLetterResponse> getSearchedDenticareALR(
			DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria, int startRow, int maxRows)
			throws DenticareServiceException;

	/**
	 * Counts all rows that return by an ALR search
	 * 
	 * @param denticareALRSearchCriteria
	 * @return - total row count for entire search dataset
	 * @throws DenticareServiceException
	 */
	public Integer getSearchedDenticareALRCount(DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria)
			throws DenticareServiceException;

	/**
	 * Save denti care audit letter responds changes
	 * 
	 * @param selectedDCALR
	 * @throws DenticareServiceException
	 */
	public void saveDenticareALR(DenticareAuditLetterResponse selectedDCALR) throws DenticareServiceException;

	/**
	 * Attempts to load AuditCriteria record by ID
	 * 
	 * @param anId
	 *            - ID of the desired AuditCriteria
	 * @return AuditCriteria with the specified ID
	 * @throws DenticareServiceException
	 */
	public AuditCriteria getPractitionerAuditCriteriaById(Long anId) throws DenticareServiceException;

	/**
	 * Attempts to load a Default AuditCriteria by the specified AuditType
	 * 
	 * @param anAuditType
	 * @return the matching audit if it exists or a brand new AuditCriteria object if nothing is found
	 * @throws DenticareServiceException
	 */
	public AuditCriteria getDefaultAuditCriteriaByType(String anAuditType) throws DenticareServiceException;

	/**
	 * Saves the specified AuditCriteria record
	 * 
	 * @param anAuditCriteria
	 * @throws DenticareServiceException
	 */
	public void saveDenticareAuditCriteria(AuditCriteria anAuditCriteria) throws DenticareServiceException;
}
