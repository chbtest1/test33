package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

/** Business Object for medicare audit letter response **/
public class MedicareAuditLetterResponse {

	@ColumnName(name = "se_number")
	private String se_number;

	@ColumnName(name = "se_sequence_number")
	private Integer se_sequence_number;

	@ColumnName(name = "response_tag_number")
	private Integer response_tag_number;

	@ColumnName(name = "audit_run_number")
	private Long audit_run_number;

	@ColumnName(name = "provider_number")
	private String provider_number;

	@ColumnName(name = "provider_type")
	private String provider_type;

	@ColumnName(name = "health_card_number")
	private String health_card_number;

	@ColumnName(name = "se_selection_date")
	private Date se_selection_date;

	@ColumnName(name = "audit_letter_creation_date")
	private Date audit_letter_creation_date;

	@ColumnName(name = "response_status_code")
	private String response_status_code;

	@ColumnName(name = "number_of_times_sent")
	private Integer number_of_times_sent;

	@ColumnName(name = "chargeback_amount")
	private BigDecimal chargeback_amount;

	@ColumnName(name = "number_of_services_affected")
	private Integer number_of_services_affected;

	@ColumnName(name = "audit_note")
	private String audit_note;

	@ColumnName(name = "modified_by")
	private String modified_by;

	@ColumnName(name = "last_modified")
	private Date last_modified;

	@ColumnName(name = "audit_type")
	private String audit_type;

	@ColumnName(name = "health_service_code")
	private String health_service_code;

	@ColumnName(name = "service_start_date")
	private Date service_start_date;

	@ColumnName(name = "modifiers")
	private String modifiers;

	@ColumnName(name = "implicit_modifiers")
	private String implicit_modifiers;

	@ColumnName(name = "payable_amount")
	private BigDecimal payable_amount;

	@ColumnName(name = "provider_name")
	private String provider_name;

	@ColumnName(name = "individual_name")
	private String individual_name;

	public String getSe_number() {
		return se_number;
	}

	public void setSe_number(String se_number) {
		this.se_number = se_number;
	}

	public Integer getSe_sequence_number() {
		return se_sequence_number;
	}

	public void setSe_sequence_number(Integer se_sequence_number) {
		this.se_sequence_number = se_sequence_number;
	}

	public Integer getResponse_tag_number() {
		return response_tag_number;
	}

	public void setResponse_tag_number(Integer response_tag_number) {
		this.response_tag_number = response_tag_number;
	}

	public Long getAudit_run_number() {
		return audit_run_number;
	}

	public void setAudit_run_number(Long audit_run_number) {
		this.audit_run_number = audit_run_number;
	}

	public String getProvider_number() {
		return provider_number;
	}

	public void setProvider_number(String provider_number) {
		this.provider_number = provider_number;
	}

	public String getProvider_type() {
		return provider_type;
	}

	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}

	public String getHealth_card_number() {
		return health_card_number;
	}

	public void setHealth_card_number(String health_card_number) {
		this.health_card_number = health_card_number;
	}

	public Date getSe_selection_date() {
		return se_selection_date;
	}

	public void setSe_selection_date(Date se_selection_date) {
		this.se_selection_date = se_selection_date;
	}

	public Date getAudit_letter_creation_date() {
		return audit_letter_creation_date;
	}

	/**
	 * @return String format of audit_letter_creation_date
	 */
	public String getAudit_letter_creation_dateString() {
		if (audit_letter_creation_date != null) {
			return DataUtilities.SDF.format(audit_letter_creation_date);
		}
		return "";
	}

	public void setAudit_letter_creation_date(Date audit_letter_creation_date) {
		this.audit_letter_creation_date = audit_letter_creation_date;
	}

	public String getResponse_status_code() {
		// we don't want space for status code
		return response_status_code.replaceAll("\\s+", "");
	}

	public void setResponse_status_code(String response_status_code) {

		this.response_status_code = response_status_code;
	}

	public Integer getNumber_of_times_sent() {
		return number_of_times_sent;
	}

	public void setNumber_of_times_sent(Integer number_of_times_sent) {
		this.number_of_times_sent = number_of_times_sent;
	}

	public BigDecimal getChargeback_amount() {
		return chargeback_amount;
	}

	public void setChargeback_amount(BigDecimal chargeback_amount) {
		this.chargeback_amount = chargeback_amount;
	}

	public Integer getNumber_of_services_affected() {
		return number_of_services_affected;
	}

	public void setNumber_of_services_affected(Integer number_of_services_affected) {
		this.number_of_services_affected = number_of_services_affected;
	}

	public String getAudit_note() {
		return audit_note;
	}

	public void setAudit_note(String audit_note) {
		this.audit_note = audit_note;
	}

	public String getModified_by() {
		return modified_by;
	}

	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	public Date getLast_modified() {
		return last_modified;
	}

	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}

	public String getAudit_type() {
		return audit_type;
	}

	public void setAudit_type(String audit_type) {
		this.audit_type = audit_type;
	}

	public String getHealth_service_code() {
		return health_service_code;
	}

	public void setHealth_service_code(String health_service_code) {
		this.health_service_code = health_service_code;
	}

	public Date getService_start_date() {
		return service_start_date;
	}

	public void setService_start_date(Date service_start_date) {
		this.service_start_date = service_start_date;
	}

	public String getModifiers() {
		return modifiers;
	}

	public void setModifiers(String modifiers) {
		this.modifiers = modifiers;
	}

	public String getImplicit_modifiers() {
		return implicit_modifiers;
	}

	public void setImplicit_modifiers(String implicit_modifiers) {
		this.implicit_modifiers = implicit_modifiers;
	}

	public BigDecimal getPayable_amount() {
		return payable_amount;
	}

	public void setPayable_amount(BigDecimal payable_amount) {
		this.payable_amount = payable_amount;
	}

	public String getProvider_name() {
		return provider_name;
	}

	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}

	/**
	 * @return the individual_name
	 */
	public String getIndividual_name() {
		return individual_name;
	}

	/**
	 * @param individual_name
	 *            the individual_name to set
	 */
	public void setIndividual_name(String individual_name) {
		this.individual_name = individual_name;
	}

}
