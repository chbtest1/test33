package ca.medavie.nspp.audit.service.data;

import java.util.Date;

/**
 * Models a single Payee Type object
 */
public class PayeeType {
	// The ID of the Audit Criteria object this belongs to
	private Long auditCriteriaId;
	private String payToCode;
	private String modifiedBy;
	private Date lastModified;

	/**
	 * @return the auditCriteriaId
	 */
	public Long getAuditCriteriaId() {
		return auditCriteriaId;
	}

	/**
	 * @param auditCriteriaId
	 *            the auditCriteriaId to set
	 */
	public void setAuditCriteriaId(Long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}

	/**
	 * @return the payToCode
	 */
	public String getPayToCode() {
		return payToCode;
	}

	/**
	 * @param payToCode
	 *            the payToCode to set
	 */
	public void setPayToCode(String payToCode) {
		this.payToCode = payToCode;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return string format of lastModified
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
}
