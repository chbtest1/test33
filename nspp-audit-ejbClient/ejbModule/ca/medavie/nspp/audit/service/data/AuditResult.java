package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * Business object representing audit result
 * **/
public class AuditResult implements Serializable {

	private static final long serialVersionUID = -2506123745713212142L;

	/** Key information **/
	private Long auditId;
	private Long providerNumber;
	private String providerType;
	private Date auditDate;

	/** Basic information **/
	private String auditPerson;
	private String auditType;
	private String auditReason;
	private String healthServiceCode;
	private String qualifier;
	private String auditSource;
	private Date auditStartDate;
	private Date auditEndDate;
	private Date interimDate;
	private Date finalContactDate;
	private BigDecimal auditRecoveryAmount;
	private Integer numberOfServicesAudited;
	private Integer inappropriatlyBilledServices;
	private Date lastModified;
	private String modifiedBy;
	private String pharmType;
	private String pharmSource;

	private String auditIndicator;

	/** Audit notes binding to this audit result **/
	private List<AuditResultNote> notes = new ArrayList<AuditResultNote>();
	private List<AuditResultAttachment> attachments = new ArrayList<AuditResultAttachment>();

	// Constructor for copy only
	public AuditResult(AuditResult ar) {
		super();
		this.auditId = ar.auditId;
		this.providerNumber = ar.providerNumber;
		this.providerType = ar.providerType;
		this.auditDate = ar.auditDate;
		this.auditPerson = ar.auditPerson;
		this.auditType = ar.auditType;
		this.auditReason = ar.auditReason;
		this.healthServiceCode = ar.healthServiceCode;
		this.qualifier = ar.qualifier;
		this.auditSource = ar.auditSource;
		this.auditStartDate = ar.auditStartDate;
		this.auditEndDate = ar.auditEndDate;
		this.interimDate = ar.interimDate;
		this.finalContactDate = ar.finalContactDate;
		this.auditRecoveryAmount = ar.auditRecoveryAmount;
		this.numberOfServicesAudited = ar.numberOfServicesAudited;
		this.inappropriatlyBilledServices = ar.inappropriatlyBilledServices;
		this.lastModified = ar.lastModified;
		this.modifiedBy = ar.modifiedBy;
		this.pharmType = ar.pharmType;
		this.pharmSource = ar.pharmSource;
		this.auditIndicator = ar.auditIndicator;
		this.notes = ar.notes;
		this.attachments = ar.attachments;
	}

	/**
	 * Default Constructor
	 */
	public AuditResult() {
	}

	public Date getAuditDate() {
		return auditDate;
	}

	/**
	 * @return auditDate in string format
	 */
	public String getAuditDateString() {
		if (auditDate != null) {
			return DataUtilities.SDF.format(auditDate);
		}
		return "";
	}

	public void setAuditDate(Date date) {
		this.auditDate = date;
	}

	public String getAuditPerson() {
		return auditPerson;
	}

	public void setAuditPerson(String auditor) {
		this.auditPerson = auditor;
	}

	public String getType() {
		return auditType;
	}

	public void setType(String type) {
		this.auditType = type;
	}

	public String getAuditReason() {
		return auditReason;
	}

	public void setAuditReason(String description) {
		this.auditReason = description;
	}

	public String getHealthServiceCode() {
		return healthServiceCode;

	}

	public void setHealthServiceCode(String healthServiceCode) {
		this.healthServiceCode = healthServiceCode;
	}

	public String getQualifier() {
		// we have to take a white space into account
		if (qualifier != null && qualifier.trim().length() > 0) {
			return qualifier.trim();
		} else
			return qualifier;
	}

	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	public String getAuditSource() {

		if (auditSource != null) {
			return auditSource.trim();
		} else
			return null;

	}

	public void setAuditSource(String source) {
		this.auditSource = source;
	}

	public Date getAuditStartDate() {
		return auditStartDate;
	}

	/**
	 * @return auditStartDate in string format
	 */
	public String getAuditStartDateString() {
		if (auditStartDate != null) {
			return DataUtilities.SDF.format(auditStartDate);
		}
		return "";
	}

	public void setAuditStartDate(Date auditStartDate) {
		this.auditStartDate = auditStartDate;
	}

	public Date getAuditEndDate() {
		return auditEndDate;
	}

	/**
	 * @return auditEndDate in string format
	 */
	public String getAuditEndDateString() {
		if (auditEndDate != null) {
			return DataUtilities.SDF.format(auditEndDate);
		}
		return "";
	}

	public void setAuditEndDate(Date auditEndDate) {
		this.auditEndDate = auditEndDate;
	}

	public Date getInterimDate() {
		return interimDate;
	}

	/**
	 * @return interimDate in string format
	 */
	public String getInterimDateString() {
		if (interimDate != null) {
			return DataUtilities.SDF.format(interimDate);
		}
		return "";
	}

	public void setInterimDate(Date interimDate) {
		this.interimDate = interimDate;
	}

	public Date getFinalContactDate() {
		return finalContactDate;
	}

	/**
	 * @return finalContactDate in string format
	 */
	public String getFinalContactDateString() {
		if (finalContactDate != null) {
			return DataUtilities.SDF.format(finalContactDate);
		}
		return "";
	}

	public void setFinalContactDate(Date finalContactDate) {
		this.finalContactDate = finalContactDate;
	}

	public BigDecimal getAuditRecoveryAmount() {
		return auditRecoveryAmount;
	}

	public void setAuditRecoveryAmount(BigDecimal recoveryAmount) {
		this.auditRecoveryAmount = recoveryAmount;
	}

	public Integer getNumberOfServicesAudited() {
		return numberOfServicesAudited;
	}

	public void setNumberOfServicesAudited(Integer numberOfServicesAudited) {
		this.numberOfServicesAudited = numberOfServicesAudited;
	}

	public Integer getInappropriatlyBilledServices() {
		return inappropriatlyBilledServices;
	}

	public void setInappropriatlyBilledServices(Integer numberOfInappropriatelyBilledServices) {
		this.inappropriatlyBilledServices = numberOfInappropriatelyBilledServices;
	}

	public String getAuditIndicator() {
		return auditIndicator;
	}

	public void setAuditIndicator(String auditIndicator) {
		this.auditIndicator = auditIndicator;
	}

	public List<AuditResultNote> getNotes() {
		return notes;
	}

	public void setNotes(List<AuditResultNote> notes) {
		this.notes = notes;
	}

	public List<AuditResultAttachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<AuditResultAttachment> attachments) {
		this.attachments = attachments;
	}

	public Long getProviderNumber() {
		return providerNumber;
	}

	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public String getAuditType() {
		if (auditType != null) {
			return auditType.trim();
		}
		return "";
	}

	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return lastModified in string format
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF.format(lastModified);
		}
		return "";
	}

	public void setLastModified(Date lastModifed) {
		this.lastModified = lastModifed;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getPharmType() {

		if (pharmType != null) {
			return pharmType.trim();
		}
		return "";
	}

	public void setPharmType(String pharmType) {
		this.pharmType = pharmType;
	}

	public String getPharmSource() {

		if (pharmSource != null) {
			return pharmSource.trim();
		}
		return "";
	}

	public void setPharmSource(String pharmSource) {
		this.pharmSource = pharmSource;
	}

	/**
	 * @return the auditId
	 */
	public Long getAuditId() {
		return auditId;
	}

	/**
	 * @param auditId
	 *            the auditId to set
	 */
	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((auditId == null) ? 0 : auditId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuditResult other = (AuditResult) obj;
		if (auditId == null) {
			if (other.auditId != null)
				return false;
		} else if (!auditId.equals(other.auditId))
			return false;
		return true;
	}

}