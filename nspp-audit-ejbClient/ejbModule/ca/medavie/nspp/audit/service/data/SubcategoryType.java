package ca.medavie.nspp.audit.service.data;

import java.util.Date;

/**
 * Models the Subcategory Type object
 */
public class SubcategoryType {

	/** Holds the Type value.. Also know as SubcategoryType */
	private String typeCode;
	/** Holds the Type description */
	private String typeDesc;
	/** The user that last modified this record */
	private String modifiedBy;
	/** The Date this record was last modified */
	private Date lastModified;
	/** yearEndDate */
	private Date yearEndDate;
	/** providerPeerGroupId */
	private long providerPeerGroupId;

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return string format of lastModified
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getTypeDesc() {
		return typeDesc;
	}

	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}

	public Date getYearEndDate() {
		return yearEndDate;
	}

	public void setYearEndDate(Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}

	public long getProviderPeerGroupId() {
		return providerPeerGroupId;
	}

	public void setProviderPeerGroupId(long providerPeerGroupId) {
		this.providerPeerGroupId = providerPeerGroupId;
	}

	@Override
	public String toString() {
		return "SubcategoryType [typeCode=" + typeCode + ", typeDesc=" + typeDesc + ", modifiedBy=" + modifiedBy
				+ ", lastModified=" + lastModified + ", yearEndDate=" + yearEndDate + ", providerPeerGroupId="
				+ providerPeerGroupId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((typeCode == null) ? 0 : typeCode.hashCode());
		result = prime * result + ((typeDesc == null) ? 0 : typeDesc.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubcategoryType other = (SubcategoryType) obj;
		if (typeCode == null) {
			if (other.typeCode != null)
				return false;
		} else if (!typeCode.equals(other.typeCode))
			return false;
		if (typeDesc == null) {
			if (other.typeDesc != null)
				return false;
		} else if (!typeDesc.equals(other.typeDesc))
			return false;
		return true;
	}

}
