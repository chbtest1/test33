package ca.medavie.nspp.audit.service.data;

/**
 * Models the search criteria for Audit Criteria
 */
public class AuditCriteriaSearchCriteria extends BaseSearchCriteria {

	// Variables used to determine if the user is making a Practitioner specific search
	private boolean practitionerSearch;
	private String auditCriteriaType;

	private String auditType;
	private Long auditCriteriaId;
	private Long practitionerId;
	private String subcategory;
	private String practitionerName;

	// Audit Criteria type constants for Medicare and Denticare
	public static final String GENERAL_DEFAULT_AUDIT_CRIT = "AUD_CT_DEF";
	public static final String GENERAL_PRACT_AUDIT_CRIT = "AUD_CT_PRA";

	// Audit Criteria type constants for Pharmacare
	public static final String PHARM_DEFAULT_AUDIT_CRIT = "AUD_DEF_RX";
	public static final String PHARM_AUDIT_CRIT = "AUD_CT_RX";

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria(java.lang.Object)
	 */
	@Override
	public void resetSearchCriteria() {
		// Clear out values
		super.reset(this);
	}

	/**
	 * @return the practitionerSearch
	 */
	public boolean isPractitionerSearch() {
		return practitionerSearch;
	}

	/**
	 * @param practitionerSearch
	 *            the practitionerSearch to set
	 */
	public void setPractitionerSearch(boolean practitionerSearch) {
		this.practitionerSearch = practitionerSearch;
	}

	/**
	 * @return the auditCriteriaId
	 */
	public Long getAuditCriteriaId() {
		return auditCriteriaId;
	}

	/**
	 * @param auditCriteriaId
	 *            the auditCriteriaId to set
	 */
	public void setAuditCriteriaId(Long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}

	/**
	 * @return the practitionerId
	 */
	public Long getPractitionerId() {
		return practitionerId;
	}

	/**
	 * @param practitionerId
	 *            the practitionerId to set
	 */
	public void setPractitionerId(Long practitionerId) {
		this.practitionerId = practitionerId;
	}

	/**
	 * @return the subcategory
	 */
	public String getSubcategory() {
		return subcategory;
	}

	/**
	 * @param subcategory
	 *            the subcategory to set
	 */
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	/**
	 * @return the practitionerName
	 */
	public String getPractitionerName() {
		return practitionerName;
	}

	/**
	 * @param practitionerName
	 *            the practitionerName to set
	 */
	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	/**
	 * @return the auditCriteriaType
	 */
	public String getAuditCriteriaType() {
		return auditCriteriaType;
	}

	/**
	 * @param auditCriteriaType
	 *            the auditCriteriaType to set
	 */
	public void setAuditCriteriaType(String auditCriteriaType) {
		this.auditCriteriaType = auditCriteriaType;
	}

	/**
	 * @return the auditType
	 */
	public String getAuditType() {
		return auditType;
	}

	/**
	 * @param auditType
	 *            the auditType to set
	 */
	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public boolean isAuditTypeSet() {
		return (auditType != null && !auditType.isEmpty()) ? true : false;
	}

	public boolean isAuditCriteriaIdSet() {
		return (auditCriteriaId != null) ? true : false;
	}

	public boolean isPractitionerIdSet() {
		return (practitionerId != null) ? true : false;
	}

	public boolean isSubcategorySet() {
		return (subcategory != null && !subcategory.isEmpty()) ? true : false;
	}

	public boolean isPractitionerNameSet() {
		return (practitionerName != null && !practitionerName.isEmpty()) ? true : false;
	}
}
