package ca.medavie.nspp.audit.service.data;

import java.util.Date;
import java.util.List;

/**
 * Models a single Health Service Group
 */
public class HealthServiceGroup {

	@ColumnName(name = "health_service_group_id")
	private Long healthServiceGroupId;

	@ColumnName(name = "health_service_group_name")
	private String healthServiceGroupName;

	private List<HealthServiceCode> healthServiceCodes;

	@ColumnName(name = "modified_by")
	private String modifiedBy;

	@ColumnName(name = "last_modified")
	private Date lastModified;

	/**
	 * @return the healthServiceGroupId
	 */
	public Long getHealthServiceGroupId() {
		return healthServiceGroupId;
	}

	/**
	 * @param healthServiceGroupId
	 *            the healthServiceGroupId to set
	 */
	public void setHealthServiceGroupId(Long healthServiceGroupId) {
		this.healthServiceGroupId = healthServiceGroupId;
	}

	/**
	 * @return the healthServiceGroupName
	 */
	public String getHealthServiceGroupName() {
		return healthServiceGroupName;
	}

	/**
	 * @param healthServiceGroupName
	 *            the healthServiceGroupName to set
	 */
	public void setHealthServiceGroupName(String healthServiceGroupName) {
		this.healthServiceGroupName = healthServiceGroupName;
	}

	/**
	 * @return the healthServiceCodes
	 */
	public List<HealthServiceCode> getHealthServiceCodes() {
		return healthServiceCodes;
	}

	/**
	 * @param healthServiceCodes
	 *            the healthServiceCodes to set
	 */
	public void setHealthServiceCodes(List<HealthServiceCode> healthServiceCodes) {
		this.healthServiceCodes = healthServiceCodes;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return lastModified formatted as a string
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		} else {
			return "";
		}
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

}
