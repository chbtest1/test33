package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

public class DssUnitValueBO implements Comparable<DssUnitValueBO> {

	private Long unitValueID;
	private BigDecimal unitValueLevel;
	private BigDecimal unitValueTypeCode;
	private String unitValueTypeDesc;
	private BigDecimal unitValue;
	private BigDecimal lowerThreshold;
	private BigDecimal upperThreshold;
	private Date effectiveFromDate;
	private Date effectiveToDate;
	private String modifiedBy;
	private Date lastModified;

	/**
	 * Constructor for clone purpose
	 * **/
	public DssUnitValueBO(DssUnitValueBO aDssUnitValueBO) {

		this.unitValueID = aDssUnitValueBO.unitValueID;
		this.unitValueLevel = aDssUnitValueBO.unitValueLevel;
		this.unitValueTypeCode = aDssUnitValueBO.unitValueTypeCode;
		this.unitValueTypeDesc = aDssUnitValueBO.unitValueTypeDesc;
		this.unitValue = aDssUnitValueBO.unitValue;
		this.lowerThreshold = aDssUnitValueBO.lowerThreshold;
		this.upperThreshold = aDssUnitValueBO.upperThreshold;
		this.effectiveFromDate = aDssUnitValueBO.effectiveFromDate;
		this.effectiveToDate = aDssUnitValueBO.effectiveToDate;
		this.modifiedBy = aDssUnitValueBO.modifiedBy;
		this.lastModified = aDssUnitValueBO.lastModified;
	}

	public DssUnitValueBO() {
	}

	public Long getUnitValueID() {
		return unitValueID;
	}

	public void setUnitValueID(Long unitValueID) {
		this.unitValueID = unitValueID;
	}

	public BigDecimal getUnitValueLevel() {
		return unitValueLevel;
	}

	public void setUnitValueLevel(BigDecimal unitValueLevel) {
		this.unitValueLevel = unitValueLevel;
	}

	public BigDecimal getUnitValueTypeCode() {
		return unitValueTypeCode;
	}

	public void setUnitValueTypeCode(BigDecimal unitValueTypeCode) {
		this.unitValueTypeCode = unitValueTypeCode;
	}

	public String getUnitValueTypeDesc() {
		return unitValueTypeDesc;
	}

	public void setUnitValueTypeDesc(String muvTypeDesc) {
		this.unitValueTypeDesc = muvTypeDesc;
	}

	public BigDecimal getUnitValue() {
		return unitValue;
	}

	public void setUnitValue(BigDecimal unitValue) {
		this.unitValue = unitValue;
	}

	public BigDecimal getLowerThreshold() {
		return lowerThreshold;
	}

	public void setLowerThreshold(BigDecimal lowerThreshold) {
		this.lowerThreshold = lowerThreshold;
	}

	public BigDecimal getUpperThreshold() {
		return upperThreshold;
	}

	public void setUpperThreshold(BigDecimal upperThreshold) {
		this.upperThreshold = upperThreshold;
	}

	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}
	
	/**
	 * @return effectiveFromDate formatted a String
	 */
	public String getEffectiveFromDateString() {
		if (effectiveFromDate != null) {
			return DataUtilities.SDF.format(effectiveFromDate);
		}
		return "";
	}		

	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public Date getEffectiveToDate() {
		return effectiveToDate;
	}
	
	/**
	 * @return effectiveToDate formatted a String
	 */
	public String getEffectiveToDateString() {
		if (effectiveToDate != null) {
			return DataUtilities.SDF.format(effectiveToDate);
		}
		return "";
	}	

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getLastModified() {
		return lastModified;
	}
	
	/**
	 * @return effectiveFromDate formatted a String
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}			

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	@Override
	public int compareTo(DssUnitValueBO obj) {
		return this.unitValueID.compareTo(obj.getUnitValueID());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((unitValueID == null) ? 0 : unitValueID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DssUnitValueBO other = (DssUnitValueBO) obj;
		if (unitValueID == null) {
			if (other.unitValueID != null)
				return false;
		} else if (!unitValueID.equals(other.unitValueID))
			return false;
		return true;
	}

}
