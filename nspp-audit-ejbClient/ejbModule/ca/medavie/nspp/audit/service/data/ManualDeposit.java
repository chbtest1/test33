package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * Models a single HealthService. Details displayed on Inquiry pages
 */
public class ManualDeposit {
	@ColumnName(name = "deposit_number")
	private Long depositNumber;

	@ColumnName(name = "last_name")
	private String practitionerName;

	@ColumnName(name = "first_name")
	private String firstName;

	@ColumnName(name = "last_name")
	private String lastName;

	@ColumnName(name = "middle_name")
	private String middleName;

	@ColumnName(name = "provider_number")
	private Long practitionerNumber;
	
	@ColumnName(name = "provider_number")
	private String practitioner;

	@ColumnName(name = "provider_group_id")
	private Long groupNumber;

	@ColumnName(name = "provider_group_name")
	private String groupName;

	@ColumnName(name = "provider_type")
	private String practitionerType;

	@ColumnName(name = "organization_name")
	private String practitionerOrganization;

	@ColumnName(name = "bus_arr_number")
	private Integer busArrNumber;

	@ColumnName(name = "fiscal_year_end_date")
	private Date fiscalYearEnd;

	private String fiscalYearEndStr;

	@ColumnName(name = "gl_number")
	private Integer glNumber;

	@ColumnName(name = "deposit_amount")
	private BigDecimal depositAmount;

	@ColumnName(name = "deposit_note")
	private String depositNote;

	@ColumnName(name = "date_received")
	private Date dateReceived;

	@ColumnName(name = "last_modified")
	private Date lastModifiedDate;

	@ColumnName(name = "modified_by")
	private String lastModifiedBy;

	@ColumnName(name = "bus_arr_description")
	private String busArrDesc;

	private String remunerationMethod;

	public Long getDepositNumber() {
		return depositNumber;
	}

	public void setDepositNumber(Long depositNumber) {
		this.depositNumber = depositNumber;
	}

	public Long getPractitionerNumber() {
		return practitionerNumber;
	}

	public void setPractitionerNumber(Long practitionerNumber) {
		this.practitionerNumber = practitionerNumber;
	}

	public Long getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(Long groupNumber) {
		this.groupNumber = groupNumber;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getPractitionerType() {
		return practitionerType;
	}

	public void setPractitionerType(String practitionerType) {
		this.practitionerType = practitionerType;
	}

	public String getPractitionerOrganization() {
		return practitionerOrganization;
	}

	public void setPractitionerOrganization(String practitionerOrganization) {
		this.practitionerOrganization = practitionerOrganization;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Integer getBusArrNumber() {
		return busArrNumber;
	}

	public void setBusArrNumber(Integer busArrNumber) {
		this.busArrNumber = busArrNumber;
	}

	public Date getFiscalYearEnd() {
		return fiscalYearEnd;
	}

	public void setFiscalYearEnd(Date fiscalYearEnd) {
		this.fiscalYearEnd = fiscalYearEnd;
	}

	public String getFiscalYearEndStr() {
		if (this.fiscalYearEnd != null) {
			Calendar calNow = Calendar.getInstance();
			calNow.setTime(this.fiscalYearEnd);
			this.fiscalYearEndStr = String.valueOf(calNow.get(Calendar.YEAR));
		}
		return this.fiscalYearEndStr;
	}

	public void setFiscalYearEndStr(String fiscalYearEndStr) {
		Calendar calNow = Calendar.getInstance();
		calNow.set(Calendar.DAY_OF_MONTH, 31);
		calNow.set(Calendar.MONTH, 2);
		calNow.set(Calendar.YEAR, Integer.parseInt(fiscalYearEndStr));
		this.fiscalYearEnd = calNow.getTime();
		this.fiscalYearEndStr = fiscalYearEndStr;
	}

	public Integer getGlNumber() {
		return glNumber;
	}

	public void setGlNumber(Integer glNumber) {
		this.glNumber = glNumber;
	}

	public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	public String getDepositNote() {
		return depositNote;
	}

	public void setDepositNote(String depositNote) {
		this.depositNote = depositNote;
	}

	public Date getDateReceived() {
		return dateReceived;
	}

	public String getDateReceivedString() {
		if (dateReceived != null) {
			return DataUtilities.SDF.format(dateReceived);
		}
		return "";
	}

	public void setDateReceived(Date dateReceived) {
		this.dateReceived = dateReceived;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getPractitionerName() {

		if (practitionerName == null) {
			practitionerName = ((practitionerOrganization != null) ? practitionerOrganization : "") + " "
					+ ((lastName != null) ? lastName + ", " : "") + " " + ((firstName != null) ? firstName : "") + " "
					+ ((middleName != null) ? middleName.substring(0, 1) : "");
		}

		return practitionerName;
	}

	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	public String getBusArrDesc() {
		return busArrDesc;
	}

	public void setBusArrDesc(String busArrDesc) {
		this.busArrDesc = busArrDesc;
	}

	public String getPractitioner() {
		if ((this.practitionerNumber == null) || (this.practitionerType == null))
			return "";
		return this.practitionerNumber + " " + this.practitionerType;
	}

	public String getRemunerationMethod() {
		return remunerationMethod;
	}

	public void setRemunerationMethod(String remunerationMethod) {
		this.remunerationMethod = remunerationMethod;
	}

	@Override
	public String toString() {
		return "ManualDeposit [depositNumber=" + depositNumber + ", practitionerNumber=" + practitionerNumber
				+ ", groupNumber=" + groupNumber + ", groupName=" + groupName + ", practitionerType="
				+ practitionerType + ", busArrNumber=" + busArrNumber + ", fiscalYearEnd=" + fiscalYearEnd
				+ ", glNumber=" + glNumber + ", depositAmount=" + depositAmount + ", depositNote=" + depositNote
				+ ", dateReceived=" + dateReceived + ", lastModifiedDate=" + lastModifiedDate + ", lastModifiedBy="
				+ lastModifiedBy + ", busArrDesc=" + busArrDesc + ", practitionerName=" + practitionerName + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((busArrDesc == null) ? 0 : busArrDesc.hashCode());
		result = prime * result + ((busArrNumber == null) ? 0 : busArrNumber.hashCode());
		result = prime * result + ((dateReceived == null) ? 0 : dateReceived.hashCode());
		result = prime * result + ((depositAmount == null) ? 0 : depositAmount.hashCode());
		result = prime * result + ((depositNote == null) ? 0 : depositNote.hashCode());
		result = prime * result + ((depositNumber == null) ? 0 : depositNumber.hashCode());
		result = prime * result + ((practitionerName == null) ? 0 : practitionerName.hashCode());
		result = prime * result + ((fiscalYearEnd == null) ? 0 : fiscalYearEnd.hashCode());
		result = prime * result + ((glNumber == null) ? 0 : glNumber.hashCode());
		result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
		result = prime * result + ((groupNumber == null) ? 0 : groupNumber.hashCode());
		result = prime * result + ((lastModifiedBy == null) ? 0 : lastModifiedBy.hashCode());
		result = prime * result + ((lastModifiedDate == null) ? 0 : lastModifiedDate.hashCode());
		result = prime * result + ((practitionerNumber == null) ? 0 : practitionerNumber.hashCode());
		result = prime * result + ((practitionerType == null) ? 0 : practitionerType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ManualDeposit other = (ManualDeposit) obj;
		if (busArrDesc == null) {
			if (other.busArrDesc != null)
				return false;
		} else if (!busArrDesc.equals(other.busArrDesc))
			return false;
		if (busArrNumber == null) {
			if (other.busArrNumber != null)
				return false;
		} else if (!busArrNumber.equals(other.busArrNumber))
			return false;
		if (dateReceived == null) {
			if (other.dateReceived != null)
				return false;
		} else if (!dateReceived.equals(other.dateReceived))
			return false;
		if (depositAmount == null) {
			if (other.depositAmount != null)
				return false;
		} else if (!depositAmount.equals(other.depositAmount))
			return false;
		if (depositNote == null) {
			if (other.depositNote != null)
				return false;
		} else if (!depositNote.equals(other.depositNote))
			return false;
		if (depositNumber == null) {
			if (other.depositNumber != null)
				return false;
		} else if (!depositNumber.equals(other.depositNumber))
			return false;
		if (practitionerName == null) {
			if (other.practitionerName != null)
				return false;
		} else if (!practitionerName.equals(other.practitionerName))
			return false;
		if (fiscalYearEnd == null) {
			if (other.fiscalYearEnd != null)
				return false;
		} else if (!fiscalYearEnd.equals(other.fiscalYearEnd))
			return false;
		if (glNumber == null) {
			if (other.glNumber != null)
				return false;
		} else if (!glNumber.equals(other.glNumber))
			return false;
		if (groupName == null) {
			if (other.groupName != null)
				return false;
		} else if (!groupName.equals(other.groupName))
			return false;
		if (groupNumber == null) {
			if (other.groupNumber != null)
				return false;
		} else if (!groupNumber.equals(other.groupNumber))
			return false;
		if (lastModifiedBy == null) {
			if (other.lastModifiedBy != null)
				return false;
		} else if (!lastModifiedBy.equals(other.lastModifiedBy))
			return false;
		if (lastModifiedDate == null) {
			if (other.lastModifiedDate != null)
				return false;
		} else if (!lastModifiedDate.equals(other.lastModifiedDate))
			return false;
		if (practitionerNumber == null) {
			if (other.practitionerNumber != null)
				return false;
		} else if (!practitionerNumber.equals(other.practitionerNumber))
			return false;
		if (practitionerType == null) {
			if (other.practitionerType != null)
				return false;
		} else if (!practitionerType.equals(other.practitionerType))
			return false;
		return true;
	}

}
