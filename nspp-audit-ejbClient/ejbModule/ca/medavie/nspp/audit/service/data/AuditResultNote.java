package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.util.Date;

public class AuditResultNote implements Serializable {

	private static final long serialVersionUID = 5447739855392384765L;

	private Long id;
	private Long auditId;
	private Date noteDate;
	private String note;

	private Date lastModified;
	private String modifiedBy;

	/**
	 * Adding those properties(the information that connects the DssProviderAuditNotesXrefPK) for the purpose of mapping
	 * with DssProviderAuditNotesXrefPK
	 **/
	private long providerNumber;
	private String providerType;
	private Date auditDate;

	/**
	 * Default Constructor
	 * 
	 ***/
	public AuditResultNote() {
	}

	/**
	 * Constructor for setting up AuditResultNote using corresponding information from it binding AuditResultNote
	 * 
	 ***/
	public AuditResultNote(AuditResult aAuditResult) {

		this.providerNumber = aAuditResult.getProviderNumber();
		this.providerType = aAuditResult.getProviderType();
		this.auditDate = aAuditResult.getAuditDate();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the auditId
	 */
	public Long getAuditId() {
		return auditId;
	}

	/**
	 * @param auditId
	 *            the auditId to set
	 */
	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}

	public Date getNoteDate() {
		return noteDate;
	}

	public void setNoteDate(Date date) {
		this.noteDate = date;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public long getProviderNumber() {
		return providerNumber;
	}

	public void setProviderNumber(long providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderType() {
		return providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((auditDate == null) ? 0 : auditDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastModified == null) ? 0 : lastModified.hashCode());
		result = prime * result + ((modifiedBy == null) ? 0 : modifiedBy.hashCode());
		result = prime * result + ((note == null) ? 0 : note.hashCode());
		result = prime * result + ((noteDate == null) ? 0 : noteDate.hashCode());
		result = prime * result + (int) (providerNumber ^ (providerNumber >>> 32));
		result = prime * result + ((providerType == null) ? 0 : providerType.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuditResultNote other = (AuditResultNote) obj;
		if (auditDate == null) {
			if (other.auditDate != null)
				return false;
		} else if (!auditDate.equals(other.auditDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastModified == null) {
			if (other.lastModified != null)
				return false;
		} else if (!lastModified.equals(other.lastModified))
			return false;
		if (modifiedBy == null) {
			if (other.modifiedBy != null)
				return false;
		} else if (!modifiedBy.equals(other.modifiedBy))
			return false;
		if (note == null) {
			if (other.note != null)
				return false;
		} else if (!note.equals(other.note))
			return false;
		if (noteDate == null) {
			if (other.noteDate != null)
				return false;
		} else if (!noteDate.equals(other.noteDate))
			return false;
		if (providerNumber != other.providerNumber)
			return false;
		if (providerType == null) {
			if (other.providerType != null)
				return false;
		} else if (!providerType.equals(other.providerType))
			return false;
		return true;
	}
}
