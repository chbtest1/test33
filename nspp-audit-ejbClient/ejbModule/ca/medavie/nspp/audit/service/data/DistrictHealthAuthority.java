package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Models a single DHA(District Health Authority)
 */
public class DistrictHealthAuthority {

	// Unique key for DHA (Primary Key in DB)
	private Long dhaId;

	private Long practitionerNumber;
	private String practitionerType;
	private String practitionerName;
	private Long businessArrangementNumber;
	private String businessArrganementDescription;
	private Long practitionerGroupNumber;
	private String practitionerGroupDescription;
	private String contractTownName;
	private String zoneId;
	private String zoneName;
	private String dhaCode;
	private String dhaDescription;
	private String empType;
	private BigDecimal fte;
	/** Holds the string reference to the UI allowing regex validation */
	private String fteString;
	private String modifiedBy;
	private Date lastModified;

	/** Values that retain original values of object until changes are saved */
	private String originalEmpType;
	private BigDecimal originalFte;
	private String originalContractTownName;
	private String originalZoneId;
	private String originalZoneName;
	private String originalDhaCode;
	private String originalDhaDescription;

	/**
	 * @return the practitionerNumber
	 */
	public Long getPractitionerNumber() {
		return practitionerNumber;
	}

	/**
	 * @param practitionerNumber
	 *            the practitionerNumber to set
	 */
	public void setPractitionerNumber(Long practitionerNumber) {
		this.practitionerNumber = practitionerNumber;
	}

	/**
	 * @return the practitionerType
	 */
	public String getPractitionerType() {
		return practitionerType;
	}

	/**
	 * @param practitionerType
	 *            the practitionerType to set
	 */
	public void setPractitionerType(String practitionerType) {
		this.practitionerType = practitionerType;
	}

	/**
	 * @return the practitionerName
	 */
	public String getPractitionerName() {
		return practitionerName;
	}

	/**
	 * @param practitionerName
	 *            the practitionerName to set
	 */
	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	/**
	 * @return the businessArrangementNumber
	 */
	public Long getBusinessArrangementNumber() {
		return businessArrangementNumber;
	}

	/**
	 * @param businessArrangementNumber
	 *            the businessArrangementNumber to set
	 */
	public void setBusinessArrangementNumber(Long businessArrangementNumber) {
		this.businessArrangementNumber = businessArrangementNumber;
	}

	/**
	 * @return the businessArrganementDescription
	 */
	public String getBusinessArrganementDescription() {
		return businessArrganementDescription;
	}

	/**
	 * @param businessArrganementDescription
	 *            the businessArrganementDescription to set
	 */
	public void setBusinessArrganementDescription(String businessArrganementDescription) {
		this.businessArrganementDescription = businessArrganementDescription;
	}

	public Long getPractitionerGroupNumber() {
		return practitionerGroupNumber;
	}

	public void setPractitionerGroupNumber(Long practitionerGroupNumber) {
		this.practitionerGroupNumber = practitionerGroupNumber;
	}

	public String getPractitionerGroupDescription() {
		return practitionerGroupDescription;
	}

	public void setPractitionerGroupDescription(String practitionerGroupDescription) {
		this.practitionerGroupDescription = practitionerGroupDescription;
	}

	/**
	 * @return the contractTownName
	 */
	public String getContractTownName() {
		return contractTownName;
	}

	/**
	 * @param contractTownName
	 *            the contractTownName to set
	 */
	public void setContractTownName(String contractTownName) {
		this.contractTownName = contractTownName;
	}

	/**
	 * @return the zoneId
	 */
	public String getZoneId() {
		return zoneId;
	}

	/**
	 * @param zoneId
	 *            the zoneId to set
	 */
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	/**
	 * @return the zoneName
	 */
	public String getZoneName() {
		return zoneName;
	}

	/**
	 * @param zoneName
	 *            the zoneName to set
	 */
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	/**
	 * @return the dhaCode
	 */
	public String getDhaCode() {
		return dhaCode;
	}

	/**
	 * @param dhaCode
	 *            the dhaCode to set
	 */
	public void setDhaCode(String dhaCode) {
		this.dhaCode = dhaCode;
	}

	/**
	 * @return the dhaDescription
	 */
	public String getDhaDescription() {
		return dhaDescription;
	}

	/**
	 * @param dhaDescription
	 *            the dhaDescription to set
	 */
	public void setDhaDescription(String dhaDescription) {
		this.dhaDescription = dhaDescription;
	}

	/**
	 * @return the dhaId
	 */
	public Long getDhaId() {
		return dhaId;
	}

	/**
	 * @param dhaId
	 *            the dhaId to set
	 */
	public void setDhaId(Long dhaId) {
		this.dhaId = dhaId;
	}

	/**
	 * @return the fte
	 */
	public BigDecimal getFte() {
		return fte;
	}

	/**
	 * @param fte
	 *            the fte to set
	 */
	public void setFte(BigDecimal fte) {
		this.fte = fte;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return lastModified formatted as a string
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the originalFte
	 */
	public BigDecimal getOriginalFte() {
		return originalFte;
	}

	/**
	 * @param originalFte
	 *            the originalFte to set
	 */
	public void setOriginalFte(BigDecimal originalFte) {
		this.originalFte = originalFte;
	}

	/**
	 * @return the originalContractTownName
	 */
	public String getOriginalContractTownName() {
		return originalContractTownName;
	}

	/**
	 * @param originalContractTownName
	 *            the originalContractTownName to set
	 */
	public void setOriginalContractTownName(String originalContractTownName) {
		this.originalContractTownName = originalContractTownName;
	}

	/**
	 * @return the originalZoneId
	 */
	public String getOriginalZoneId() {
		return originalZoneId;
	}

	/**
	 * @param originalZoneId
	 *            the originalZoneId to set
	 */
	public void setOriginalZoneId(String originalZoneId) {
		this.originalZoneId = originalZoneId;
	}

	/**
	 * @return the originalZoneName
	 */
	public String getOriginalZoneName() {
		return originalZoneName;
	}

	/**
	 * @param originalZoneName
	 *            the originalZoneName to set
	 */
	public void setOriginalZoneName(String originalZoneName) {
		this.originalZoneName = originalZoneName;
	}

	/**
	 * @return the originalDhaCode
	 */
	public String getOriginalDhaCode() {
		return originalDhaCode;
	}

	/**
	 * @param originalDhaCode
	 *            the originalDhaCode to set
	 */
	public void setOriginalDhaCode(String originalDhaCode) {
		this.originalDhaCode = originalDhaCode;
	}

	/**
	 * @return the originalDhaDescription
	 */
	public String getOriginalDhaDescription() {
		return originalDhaDescription;
	}

	/**
	 * @param originalDhaDescription
	 *            the originalDhaDescription to set
	 */
	public void setOriginalDhaDescription(String originalDhaDescription) {
		this.originalDhaDescription = originalDhaDescription;
	}

	/**
	 * @return the empType
	 */
	public String getEmpType() {
		return empType;
	}

	/**
	 * @param empType
	 *            the empType to set
	 */
	public void setEmpType(String empType) {
		this.empType = empType;
	}

	/**
	 * @return the originalEmpType
	 */
	public String getOriginalEmpType() {
		return originalEmpType;
	}

	/**
	 * @param originalEmpType
	 *            the originalEmpType to set
	 */
	public void setOriginalEmpType(String originalEmpType) {
		this.originalEmpType = originalEmpType;
	}

	/**
	 * @return the fteString
	 */
	public String getFteString() {
		return fteString;
	}

	/**
	 * @param fteString
	 *            the fteString to set
	 */
	public void setFteString(String fteString) {
		this.fteString = fteString;
	}
	
	
	// JTRAX-66 22-MAR-2018 BCAINNE
	/**
	 * Used to determine if two DHAs are the same for duplicate checking.
	 * @param dhaB
	 * @return
	 */
	public boolean equals(DistrictHealthAuthority dhaB) {
		Long pgnA = this.getPractitionerGroupNumber();
        Long pgnB = dhaB.getPractitionerGroupNumber();
        Long banA = this.getBusinessArrangementNumber();
        Long banB = dhaB.getBusinessArrangementNumber();
        if (pgnA == null) pgnA = -999L;
        if (pgnB == null) pgnB = -999L;
        if (banA == null) banA = -999L;
        if (banB == null) banB = -999L;
        if (pgnA.equals(pgnB)) {
            if ((this.getPractitionerNumber() == null) || (this.getPractitionerNumber().equals(dhaB.getPractitionerNumber()))) {
                if ((this.getContractTownName() == null) || (this.getContractTownName().equals(dhaB.getContractTownName()))) {
                    if ((this.getPractitionerType()== null) || (this.getPractitionerType().equals(dhaB.getPractitionerType()))) {
                        if ((this.getDhaCode() == null) || (this.getDhaCode().equals(dhaB.getDhaCode()))) {
                            if ((this.getZoneId() == null) || (this.getZoneId().equals(dhaB.getZoneId()))) {
                                if ((this.getEmpType() == null) || (this.getEmpType().equals(dhaB.getEmpType()))) {
                                    if ((this.getFte() == null) || (this.getFte().equals(dhaB.getFte()))) {
                                    	if (banA.equals(banB)) {
                                    		return true;
                                    	}
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }	
}
