package ca.medavie.nspp.audit.service.data;

import java.util.Date;

/**
 * Models a profile period
 */
public class ProfilePeriod {

	@ColumnName(name="period_id")
	private Long periodId;
	private String periodType;
	private Date periodStartDate;
	private Date periodEndDate;
	private String modifiedBy;
	private Date lastModified;

	// Stored in memory for drill down process
	private Long minimumPeriodId;
	private Long maximumPeriodId;

	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId
	 *            the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the periodType
	 */
	public String getPeriodType() {
		return periodType;
	}

	/**
	 * @param periodType
	 *            the periodType to set
	 */
	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	/**
	 * @return the periodStartDate
	 */
	public Date getPeriodStartDate() {
		return periodStartDate;
	}

	/**
	 * @return the periodStartDate formatted as a string
	 */
	public String getPeriodStartDateString() {
		if (periodStartDate != null) {
			return DataUtilities.SDF.format(periodStartDate);
		}
		return "";
	}

	/**
	 * @param periodStartDate
	 *            the periodStartDate to set
	 */
	public void setPeriodStartDate(Date periodStartDate) {
		this.periodStartDate = periodStartDate;
	}

	/**
	 * @return the periodEndDate
	 */
	public Date getPeriodEndDate() {
		return periodEndDate;
	}

	/**
	 * @return the periodEndDate formatted as a string
	 */
	public String getPeriodEndDateString() {
		if (periodEndDate != null) {
			return DataUtilities.SDF.format(periodEndDate);
		}
		return "";
	}

	/**
	 * @param periodEndDate
	 *            the periodEndDate to set
	 */
	public void setPeriodEndDate(Date periodEndDate) {
		this.periodEndDate = periodEndDate;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the minimumPeriodId
	 */
	public Long getMinimumPeriodId() {
		return minimumPeriodId;
	}

	/**
	 * @param minimumPeriodId
	 *            the minimumPeriodId to set
	 */
	public void setMinimumPeriodId(Long minimumPeriodId) {
		this.minimumPeriodId = minimumPeriodId;
	}

	/**
	 * @return the maximumPeriodId
	 */
	public Long getMaximumPeriodId() {
		return maximumPeriodId;
	}

	/**
	 * @param maximumPeriodId
	 *            the maximumPeriodId to set
	 */
	public void setMaximumPeriodId(Long maximumPeriodId) {
		this.maximumPeriodId = maximumPeriodId;
	}
}
