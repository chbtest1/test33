package ca.medavie.nspp.audit.service.data;

import java.util.Date;
import java.util.List;

/**
 * Models a single HealthCard object. Used for DSS History Transfer screens
 */
public class HealthCard {

	/** The unique Health Card Number */
	private String healthCardNumber;

	private String surname;
	private String firstName;
	private Date dob;
	private String oldMsiNumber;
	private Date renewalSentDate;
	private Date renewalRecievedDate;
	private Date healthCardExpiryDate;
	private String gender;
	private boolean donor;
	private String homePhone;
	private String workPhone;
	private String headOfAddressHealthCardNumber;
	private String city;
	private String province;
	private String addressLine1;
	private String addressLine2;
	private boolean validAddress;
	private String provinceMovedTo;
	private String provinceMovedFrom;

	/** List holding Medicare Claims */
	private List<MedicareClaim> medicareClaims;
	/** List holding Medicare Mainframe Claims */
	private List<MedicareClaim> medicareMainframeClaims;
	/** List holding Pharmacare Claims */
	private List<PharmacareClaim> pharmacareClaims;
	/** List holding Pharmacare Mainframe Claims */
	private List<PharmacareClaim> pharmacareMainframeClaims;
	/** List holding Denticare Claims */
	private List<DenticareClaim> denticareClaims;

	/**
	 * @return the healthCardNumber
	 */
	public String getHealthCardNumber() {
		return healthCardNumber;
	}

	/**
	 * @param healthCardNumber
	 *            the healthCardNumber to set
	 */
	public void setHealthCardNumber(String healthCardNumber) {
		this.healthCardNumber = healthCardNumber;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname
	 *            the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * @param dob
	 *            the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}

	/**
	 * @return the oldMsiNumber
	 */
	public String getOldMsiNumber() {
		return oldMsiNumber;
	}

	/**
	 * @param oldMsiNumber
	 *            the oldMsiNumber to set
	 */
	public void setOldMsiNumber(String oldMsiNumber) {
		this.oldMsiNumber = oldMsiNumber;
	}

	/**
	 * @return the renewalSentDate
	 */
	public Date getRenewalSentDate() {
		return renewalSentDate;
	}

	/**
	 * @param renewalSentDate
	 *            the renewalSentDate to set
	 */
	public void setRenewalSentDate(Date renewalSentDate) {
		this.renewalSentDate = renewalSentDate;
	}

	/**
	 * @return the renewalRecievedDate
	 */
	public Date getRenewalRecievedDate() {
		return renewalRecievedDate;
	}

	/**
	 * @param renewalRecievedDate
	 *            the renewalRecievedDate to set
	 */
	public void setRenewalRecievedDate(Date renewalRecievedDate) {
		this.renewalRecievedDate = renewalRecievedDate;
	}

	/**
	 * @return the healthCardExpiryDate
	 */
	public Date getHealthCardExpiryDate() {
		return healthCardExpiryDate;
	}

	/**
	 * @param healthCardExpiryDate
	 *            the healthCardExpiryDate to set
	 */
	public void setHealthCardExpiryDate(Date healthCardExpiryDate) {
		this.healthCardExpiryDate = healthCardExpiryDate;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the donor
	 */
	public boolean isDonor() {
		return donor;
	}

	/**
	 * @param donor
	 *            the donor to set
	 */
	public void setDonor(boolean donor) {
		this.donor = donor;
	}

	/**
	 * @return the homePhone
	 */
	public String getHomePhone() {
		return homePhone;
	}

	/**
	 * @param homePhone
	 *            the homePhone to set
	 */
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	/**
	 * @return the workPhone
	 */
	public String getWorkPhone() {
		return workPhone;
	}

	/**
	 * @param workPhone
	 *            the workPhone to set
	 */
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	/**
	 * @return the headOfAddressHealthCardNumber
	 */
	public String getHeadOfAddressHealthCardNumber() {
		return headOfAddressHealthCardNumber;
	}

	/**
	 * @param headOfAddressHealthCardNumber
	 *            the headOfAddressHealthCardNumber to set
	 */
	public void setHeadOfAddressHealthCardNumber(String headOfAddressHealthCardNumber) {
		this.headOfAddressHealthCardNumber = headOfAddressHealthCardNumber;
	}

	/**
	 * @return the validAddress
	 */
	public boolean isValidAddress() {
		return validAddress;
	}

	/**
	 * @param validAddress
	 *            the validAddress to set
	 */
	public void setValidAddress(boolean validAddress) {
		this.validAddress = validAddress;
	}

	/**
	 * @return the provinceMovedTo
	 */
	public String getProvinceMovedTo() {
		return provinceMovedTo;
	}

	/**
	 * @param provinceMovedTo
	 *            the provinceMovedTo to set
	 */
	public void setProvinceMovedTo(String provinceMovedTo) {
		this.provinceMovedTo = provinceMovedTo;
	}

	/**
	 * @return the provinceMovedFrom
	 */
	public String getProvinceMovedFrom() {
		return provinceMovedFrom;
	}

	/**
	 * @param provinceMovedFrom
	 *            the provinceMovedFrom to set
	 */
	public void setProvinceMovedFrom(String provinceMovedFrom) {
		this.provinceMovedFrom = provinceMovedFrom;
	}

	/**
	 * @return the medicareClaims
	 */
	public List<MedicareClaim> getMedicareClaims() {
		return medicareClaims;
	}

	/**
	 * @param medicareClaims
	 *            the medicareClaims to set
	 */
	public void setMedicareClaims(List<MedicareClaim> medicareClaims) {
		this.medicareClaims = medicareClaims;
	}

	/**
	 * @return the medicareMainframeClaims
	 */
	public List<MedicareClaim> getMedicareMainframeClaims() {
		return medicareMainframeClaims;
	}

	/**
	 * @param medicareMainframeClaims
	 *            the medicareMainframeClaims to set
	 */
	public void setMedicareMainframeClaims(List<MedicareClaim> medicareMainframeClaims) {
		this.medicareMainframeClaims = medicareMainframeClaims;
	}

	/**
	 * @return the pharmacareClaims
	 */
	public List<PharmacareClaim> getPharmacareClaims() {
		return pharmacareClaims;
	}

	/**
	 * @param pharmacareClaims
	 *            the pharmacareClaims to set
	 */
	public void setPharmacareClaims(List<PharmacareClaim> pharmacareClaims) {
		this.pharmacareClaims = pharmacareClaims;
	}

	/**
	 * @return the pharmacareMainframeClaims
	 */
	public List<PharmacareClaim> getPharmacareMainframeClaims() {
		return pharmacareMainframeClaims;
	}

	/**
	 * @param pharmacareMainframeClaims
	 *            the pharmacareMainframeClaims to set
	 */
	public void setPharmacareMainframeClaims(List<PharmacareClaim> pharmacareMainframeClaims) {
		this.pharmacareMainframeClaims = pharmacareMainframeClaims;
	}

	/**
	 * @return the denticareClaims
	 */
	public List<DenticareClaim> getDenticareClaims() {
		return denticareClaims;
	}

	/**
	 * @param denticareClaims
	 *            the denticareClaims to set
	 */
	public void setDenticareClaims(List<DenticareClaim> denticareClaims) {
		this.denticareClaims = denticareClaims;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @param addressLine1
	 *            the addressLine1 to set
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @param addressLine2
	 *            the addressLine2 to set
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @param province
	 *            the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

}
