package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Models a single MedicareClaim record
 */
public class MedicareClaim {

	/** Medicare SE claim variables */
	private String seNumber;
	private Long seSequenceNumber;
	private Long responseTagNumber;
	private String actionCode;
	private String patientHcn;
	private String patientPostalCode;
	private Date patientBirthDate;
	private Long patientAgeYears;
	private String patientGender;
	private String healthServiceCode;
	private String qualifierCode;
	private Date serviceStartDate;
	private String providerType;
	private Long providerNumber;
	private Date paymentDate;
	private Long businessArrNumber;
	private Long renumerationMethod;
	private String payToHealthCardNumber;
	private String paymentResponsibility;
	private BigDecimal baseUnits;
	private BigDecimal amountPaid;
	private BigDecimal shadowUnits;
	private BigDecimal shadowEarnings;

	/** Medicare Mainframe claim variables */
	private String mainHealthCardNumber;
	private Long mainOldMsiBillingNumber;
	private Date mainServiceDate;
	private Long mainNumberOfServices;
	private String mainFeeSpecialty;
	private Long mainFeeServiceCode;
	private BigDecimal mainAmountPaid;
	private String mainGender;
	private Date mainDateOfBirth;
	private Long mainPatientCounty;
	private String mainRunNumber;
	private String mainProvinceCode;
	private Long mainPhysicianMainNumber;
	private Long mainSeqNumber;

	/**
	 * @return the seNumber
	 */
	public String getSeNumber() {
		return seNumber;
	}

	/**
	 * @param seNumber
	 *            the seNumber to set
	 */
	public void setSeNumber(String seNumber) {
		this.seNumber = seNumber;
	}

	/**
	 * @return the seSequenceNumber
	 */
	public Long getSeSequenceNumber() {
		return seSequenceNumber;
	}

	/**
	 * @param seSequenceNumber
	 *            the seSequenceNumber to set
	 */
	public void setSeSequenceNumber(Long seSequenceNumber) {
		this.seSequenceNumber = seSequenceNumber;
	}

	/**
	 * @return the responseTagNumber
	 */
	public Long getResponseTagNumber() {
		return responseTagNumber;
	}

	/**
	 * @param responseTagNumber
	 *            the responseTagNumber to set
	 */
	public void setResponseTagNumber(Long responseTagNumber) {
		this.responseTagNumber = responseTagNumber;
	}

	/**
	 * @return the actionCode
	 */
	public String getActionCode() {
		return actionCode;
	}

	/**
	 * @param actionCode
	 *            the actionCode to set
	 */
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	/**
	 * @return the patientPostalCode
	 */
	public String getPatientPostalCode() {
		return patientPostalCode;
	}

	/**
	 * @param patientPostalCode
	 *            the patientPostalCode to set
	 */
	public void setPatientPostalCode(String patientPostalCode) {
		this.patientPostalCode = patientPostalCode;
	}

	/**
	 * @return the patientBirthDate
	 */
	public Date getPatientBirthDate() {
		return patientBirthDate;
	}

	/**
	 * @return string format of patientBirthDate
	 */
	public String getPatientBirthDateString() {
		if (patientBirthDate != null) {
			return DataUtilities.SDF.format(patientBirthDate);
		}
		return "";
	}

	/**
	 * @param patientBirthDate
	 *            the patientBirthDate to set
	 */
	public void setPatientBirthDate(Date patientBirthDate) {
		this.patientBirthDate = patientBirthDate;
	}

	/**
	 * @return the patientAgeYears
	 */
	public Long getPatientAgeYears() {
		return patientAgeYears;
	}

	/**
	 * @param patientAgeYears
	 *            the patientAgeYears to set
	 */
	public void setPatientAgeYears(Long patientAgeYears) {
		this.patientAgeYears = patientAgeYears;
	}

	/**
	 * @return the patientGender
	 */
	public String getPatientGender() {
		return patientGender;
	}

	/**
	 * @param patientGender
	 *            the patientGender to set
	 */
	public void setPatientGender(String patientGender) {
		this.patientGender = patientGender;
	}

	/**
	 * @return the healthServiceCode
	 */
	public String getHealthServiceCode() {
		return healthServiceCode;
	}

	/**
	 * @param healthServiceCode
	 *            the healthServiceCode to set
	 */
	public void setHealthServiceCode(String healthServiceCode) {
		this.healthServiceCode = healthServiceCode;
	}

	/**
	 * @return the qualifierCode
	 */
	public String getQualifierCode() {
		return qualifierCode;
	}

	/**
	 * @param qualifierCode
	 *            the qualifierCode to set
	 */
	public void setQualifierCode(String qualifierCode) {
		this.qualifierCode = qualifierCode;
	}

	/**
	 * @return the serviceStartDate
	 */
	public Date getServiceStartDate() {
		return serviceStartDate;
	}

	/**
	 * @return string format of serviceStartDate
	 */
	public String getServiceStartDateString() {
		if (serviceStartDate != null) {
			return DataUtilities.SDF.format(serviceStartDate);
		}
		return "";
	}

	/**
	 * @param serviceStartDate
	 *            the serviceStartDate to set
	 */
	public void setServiceStartDate(Date serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}

	/**
	 * @return the providerType
	 */
	public String getProviderType() {
		return providerType;
	}

	/**
	 * @param providerType
	 *            the providerType to set
	 */
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	/**
	 * @return the providerNumber
	 */
	public Long getProviderNumber() {
		return providerNumber;
	}

	/**
	 * @param providerNumber
	 *            the providerNumber to set
	 */
	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	/**
	 * @return the paymentDate
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}

	/**
	 * @return string format of paymentDate
	 */
	public String getPaymentDateString() {
		if (paymentDate != null) {
			return DataUtilities.SDF.format(paymentDate);
		}
		return "";
	}

	/**
	 * @param paymentDate
	 *            the paymentDate to set
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * @return the businessArrNumber
	 */
	public Long getBusinessArrNumber() {
		return businessArrNumber;
	}

	/**
	 * @param businessArrNumber
	 *            the businessArrNumber to set
	 */
	public void setBusinessArrNumber(Long businessArrNumber) {
		this.businessArrNumber = businessArrNumber;
	}

	/**
	 * @return the renumerationMethod
	 */
	public Long getRenumerationMethod() {
		return renumerationMethod;
	}

	/**
	 * @param renumerationMethod
	 *            the renumerationMethod to set
	 */
	public void setRenumerationMethod(Long renumerationMethod) {
		this.renumerationMethod = renumerationMethod;
	}

	/**
	 * @return the payToHealthCardNumber
	 */
	public String getPayToHealthCardNumber() {
		return payToHealthCardNumber;
	}

	/**
	 * @param payToHealthCardNumber
	 *            the payToHealthCardNumber to set
	 */
	public void setPayToHealthCardNumber(String payToHealthCardNumber) {
		this.payToHealthCardNumber = payToHealthCardNumber;
	}

	/**
	 * @return the paymentResponsibility
	 */
	public String getPaymentResponsibility() {
		return paymentResponsibility;
	}

	/**
	 * @param paymentResponsibility
	 *            the paymentResponsibility to set
	 */
	public void setPaymentResponsibility(String paymentResponsibility) {
		this.paymentResponsibility = paymentResponsibility;
	}

	/**
	 * @return the baseUnits
	 */
	public BigDecimal getBaseUnits() {
		return baseUnits;
	}

	/**
	 * @param baseUnits
	 *            the baseUnits to set
	 */
	public void setBaseUnits(BigDecimal baseUnits) {
		this.baseUnits = baseUnits;
	}

	/**
	 * @return the amountPaid
	 */
	public BigDecimal getAmountPaid() {
		return amountPaid;
	}

	/**
	 * @param amountPaid
	 *            the amountPaid to set
	 */
	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	/**
	 * @return the shadowUnits
	 */
	public BigDecimal getShadowUnits() {
		return shadowUnits;
	}

	/**
	 * @param shadowUnits
	 *            the shadowUnits to set
	 */
	public void setShadowUnits(BigDecimal shadowUnits) {
		this.shadowUnits = shadowUnits;
	}

	/**
	 * @return the shadowEarnings
	 */
	public BigDecimal getShadowEarnings() {
		return shadowEarnings;
	}

	/**
	 * @param shadowEarnings
	 *            the shadowEarnings to set
	 */
	public void setShadowEarnings(BigDecimal shadowEarnings) {
		this.shadowEarnings = shadowEarnings;
	}

	/**
	 * @return the mainOldMsiBillingNumber
	 */
	public Long getMainOldMsiBillingNumber() {
		return mainOldMsiBillingNumber;
	}

	/**
	 * @param mainOldMsiBillingNumber
	 *            the mainOldMsiBillingNumber to set
	 */
	public void setMainOldMsiBillingNumber(Long mainOldMsiBillingNumber) {
		this.mainOldMsiBillingNumber = mainOldMsiBillingNumber;
	}

	/**
	 * @return the mainServiceDate
	 */
	public Date getMainServiceDate() {
		return mainServiceDate;
	}

	/**
	 * @return string format of mainServiceDate
	 */
	public String getMainServiceDateString() {
		if (mainServiceDate != null) {
			return DataUtilities.SDF.format(mainServiceDate);
		}
		return "";
	}

	/**
	 * @param mainServiceDate
	 *            the mainServiceDate to set
	 */
	public void setMainServiceDate(Date mainServiceDate) {
		this.mainServiceDate = mainServiceDate;
	}

	/**
	 * @return the mainNumberOfServices
	 */
	public Long getMainNumberOfServices() {
		return mainNumberOfServices;
	}

	/**
	 * @param mainNumberOfServices
	 *            the mainNumberOfServices to set
	 */
	public void setMainNumberOfServices(Long mainNumberOfServices) {
		this.mainNumberOfServices = mainNumberOfServices;
	}

	/**
	 * @return the mainFeeSpecialty
	 */
	public String getMainFeeSpecialty() {
		return mainFeeSpecialty;
	}

	/**
	 * @param mainFeeSpecialty
	 *            the mainFeeSpecialty to set
	 */
	public void setMainFeeSpecialty(String mainFeeSpecialty) {
		this.mainFeeSpecialty = mainFeeSpecialty;
	}

	/**
	 * @return the mainFeeServiceCode
	 */
	public Long getMainFeeServiceCode() {
		return mainFeeServiceCode;
	}

	/**
	 * @param mainFeeServiceCode
	 *            the mainFeeServiceCode to set
	 */
	public void setMainFeeServiceCode(Long mainFeeServiceCode) {
		this.mainFeeServiceCode = mainFeeServiceCode;
	}

	/**
	 * @return the mainAmountPaid
	 */
	public BigDecimal getMainAmountPaid() {
		return mainAmountPaid;
	}

	/**
	 * @param mainAmountPaid
	 *            the mainAmountPaid to set
	 */
	public void setMainAmountPaid(BigDecimal mainAmountPaid) {
		this.mainAmountPaid = mainAmountPaid;
	}

	/**
	 * @return the mainGender
	 */
	public String getMainGender() {
		return mainGender;
	}

	/**
	 * @param mainGender
	 *            the mainGender to set
	 */
	public void setMainGender(String mainGender) {
		this.mainGender = mainGender;
	}

	/**
	 * @return the mainDateOfBirth
	 */
	public Date getMainDateOfBirth() {
		return mainDateOfBirth;
	}

	/**
	 * @return string format of mainDateOfBirth
	 */
	public String getMainDateOfBirthString() {
		if (mainDateOfBirth != null) {
			return DataUtilities.SDF.format(mainDateOfBirth);
		}
		return "";
	}

	/**
	 * @param mainDateOfBirth
	 *            the mainDateOfBirth to set
	 */
	public void setMainDateOfBirth(Date mainDateOfBirth) {
		this.mainDateOfBirth = mainDateOfBirth;
	}

	/**
	 * @return the mainPatientCounty
	 */
	public Long getMainPatientCounty() {
		return mainPatientCounty;
	}

	/**
	 * @param mainPatientCounty
	 *            the mainPatientCounty to set
	 */
	public void setMainPatientCounty(Long mainPatientCounty) {
		this.mainPatientCounty = mainPatientCounty;
	}

	/**
	 * @return the mainRunNumber
	 */
	public String getMainRunNumber() {
		return mainRunNumber;
	}

	/**
	 * @param mainRunNumber
	 *            the mainRunNumber to set
	 */
	public void setMainRunNumber(String mainRunNumber) {
		this.mainRunNumber = mainRunNumber;
	}

	/**
	 * @return the mainProvinceCode
	 */
	public String getMainProvinceCode() {
		return mainProvinceCode;
	}

	/**
	 * @param mainProvinceCode
	 *            the mainProvinceCode to set
	 */
	public void setMainProvinceCode(String mainProvinceCode) {
		this.mainProvinceCode = mainProvinceCode;
	}

	/**
	 * @return the mainPhysicianMainNumber
	 */
	public Long getMainPhysicianMainNumber() {
		return mainPhysicianMainNumber;
	}

	/**
	 * @param mainPhysicianMainNumber
	 *            the mainPhysicianMainNumber to set
	 */
	public void setMainPhysicianMainNumber(Long mainPhysicianMainNumber) {
		this.mainPhysicianMainNumber = mainPhysicianMainNumber;
	}

	/**
	 * @return the patientHcn
	 */
	public String getPatientHcn() {
		return patientHcn;
	}

	/**
	 * @param patientHcn
	 *            the patientHcn to set
	 */
	public void setPatientHcn(String patientHcn) {
		this.patientHcn = patientHcn;
	}

	/**
	 * @return the mainHealthCardNumber
	 */
	public String getMainHealthCardNumber() {
		return mainHealthCardNumber;
	}

	/**
	 * @param mainHealthCardNumber
	 *            the mainHealthCardNumber to set
	 */
	public void setMainHealthCardNumber(String mainHealthCardNumber) {
		this.mainHealthCardNumber = mainHealthCardNumber;
	}

	/**
	 * @return the mainSeqNumber
	 */
	public Long getMainSeqNumber() {
		return mainSeqNumber;
	}

	/**
	 * @param mainSeqNumber
	 *            the mainSeqNumber to set
	 */
	public void setMainSeqNumber(Long mainSeqNumber) {
		this.mainSeqNumber = mainSeqNumber;
	}
}
