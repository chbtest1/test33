package ca.medavie.nspp.audit.service.data;

import java.util.Date;

public class MedicareAuditLetterResponseSearchCriteria extends BaseSearchCriteria {

	private Long auditRunNumber;
	private String seNumber;
	private Integer seSequenceNumber;
	private Integer tag;

	private Long practitionerNumber;
	private String practitionerType;
	private String practitionerName;
	// health card number
	private String hcnClientID;
	private String auditType;
	private String responseStatus;
	private Date auditFromDate;
	private Date auditToDate;
	// JTRAX-59 07-FEB-2017 BCAINNE
	private Date paymentFromDate;
	private Date paymentToDate;	

	@Override
	public void resetSearchCriteria() {
		super.reset(this);
	}

	public Long getPractitionerNumber() {
		return practitionerNumber;
	}

	public void setPractitionerNumber(Long practitionerNumber) {
		this.practitionerNumber = practitionerNumber;
	}

	public String getPractitionerType() {
		return practitionerType;
	}

	public void setPractitionerType(String practitionerType) {
		this.practitionerType = practitionerType;
	}

	public String getPractitionerName() {
		return practitionerName;
	}

	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	public String getAuditType() {
		return auditType;
	}

	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public Date getAuditFromDate() {
		return auditFromDate;
	}

	public void setAuditFromDate(Date auditFromDate) {
		this.auditFromDate = auditFromDate;
	}

	public Date getAuditToDate() {
		return auditToDate;
	}

	public void setAuditToDate(Date auditToDate) {
		this.auditToDate = auditToDate;
	}

	public Date getPaymentFromDate() {
		return paymentFromDate;
	}

	public void setPaymentFromDate(Date paymentFromDate) {
		this.paymentFromDate = paymentFromDate;
	}

	public Date getPaymentToDate() {
		return paymentToDate;
	}

	public void setPaymentToDate(Date paymentToDate) {
		this.paymentToDate = paymentToDate;
	}

	public Long getAuditRunNumber() {
		return auditRunNumber;
	}

	public void setAuditRunNumber(Long auditRunNumber) {
		this.auditRunNumber = auditRunNumber;
	}

	public String getHcnClientID() {
		return hcnClientID;
	}

	public void setHcnClientID(String hcnClientID) {
		this.hcnClientID = hcnClientID;
	}

	public Integer getTag() {
		return tag;
	}

	public void setTag(Integer tag) {
		this.tag = tag;
	}

	/**
	 * @return the seNumber
	 */
	public String getSeNumber() {
		return seNumber;
	}

	/**
	 * @param seNumber
	 *            the seNumber to set
	 */
	public void setSeNumber(String seNumber) {
		this.seNumber = seNumber;
	}

	/**
	 * @return the seSequenceNumber
	 */
	public Integer getSeSequenceNumber() {
		return seSequenceNumber;
	}

	/**
	 * @param seSequenceNumber
	 *            the seSequenceNumber to set
	 */
	public void setSeSequenceNumber(Integer seSequenceNumber) {
		this.seSequenceNumber = seSequenceNumber;
	}

	public boolean isProviderNumberSet() {

		return (practitionerNumber != null && practitionerNumber.compareTo(new Long(0)) > 0) ? true : false;
	}

	public boolean isProviderNameSet() {
		return (practitionerName != null && !practitionerName.isEmpty()) ? true : false;
	}

	public boolean isProviderTypeSet() {
		return (practitionerType != null && !practitionerType.isEmpty()) ? true : false;
	}

	public boolean isHCNClientIDSet() {
		return (hcnClientID != null && !hcnClientID.isEmpty()) ? true : false;
	}

	public boolean isAuditTypeSet() {
		return (auditType != null && !auditType.isEmpty()) ? true : false;
	}

	public boolean isResponseStatusSet() {
		return (responseStatus != null && !responseStatus.isEmpty()) ? true : false;
	}

	public boolean isAuditFromDateSet() {
		return (auditFromDate != null) ? true : false;
	}
	
	public boolean isAuditToDateSet() {
		return (auditToDate != null) ? true : false;
	}
	
	public boolean isPaymentFromDateSet() {
		return (paymentFromDate != null) ? true : false;
	}

	public boolean isPaymentToDateSet() {
		return (paymentToDate != null) ? true : false;
	}

	public boolean isAuditRunNumberSet() {

		return (auditRunNumber != null && auditRunNumber.compareTo(new Long(0)) > 0) ? true : false;

	}

	public boolean isSeNumberSet() {

		return (seNumber != null && !seNumber.isEmpty()) ? true : false;

	}

	public boolean isSeSequenceNumberSet() {

		return (seSequenceNumber != null && seSequenceNumber.compareTo(new Integer(0)) > 0) ? true : false;
	}

	public boolean isTagSet() {
		return (tag != null && tag.compareTo(new Integer(0)) > 0) ? true : false;
	}
}
