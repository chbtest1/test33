package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the DSS_MSI_CONSTANT database table.
 * 
 */
@Embeddable
public class DssMsiConstantPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="SUBSYSTEM")
	private String subsystem;

	@Column(name="CONSTANT_CODE")
	private String constantCode;

	public DssMsiConstantPK() {
	}
	public String getSubsystem() {
		return this.subsystem;
	}
	public void setSubsystem(String subsystem) {
		this.subsystem = subsystem;
	}
	public String getConstantCode() {
		return this.constantCode;
	}
	public void setConstantCode(String constantCode) {
		this.constantCode = constantCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DssMsiConstantPK)) {
			return false;
		}
		DssMsiConstantPK castOther = (DssMsiConstantPK)other;
		return 
			this.subsystem.equals(castOther.subsystem)
			&& this.constantCode.equals(castOther.constantCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.subsystem.hashCode();
		hash = hash * prime + this.constantCode.hashCode();
		
		return hash;
	}
}