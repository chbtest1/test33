package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Models a single Denticare Claim record
 */
public class DenticareClaim {

	private String healthCardNumber;
	private Date birthDate;
	private String gender;
	private String programCode;
	private Long providerNumber;
	private String providerType;
	private Long claimNumber;
	private Long itemCode;
	private Date paymentDate;
	private Date serviceDate;
	private BigDecimal paidAmount;
	private Long procedureCode;

	/**
	 * @return the birthDate
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * @return string format of birthDate
	 */
	public String getBirthDateString() {
		if (birthDate != null) {
			return DataUtilities.SDF.format(birthDate);
		}
		return "";
	}

	/**
	 * @param birthDate
	 *            the birthDate to set
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the programCode
	 */
	public String getProgramCode() {
		return programCode;
	}

	/**
	 * @param programCode
	 *            the programCode to set
	 */
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	/**
	 * @return the providerNumber
	 */
	public Long getProviderNumber() {
		return providerNumber;
	}

	/**
	 * @param providerNumber
	 *            the providerNumber to set
	 */
	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	/**
	 * @return the providerType
	 */
	public String getProviderType() {
		return providerType;
	}

	/**
	 * @param providerType
	 *            the providerType to set
	 */
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	/**
	 * @return the claimNumber
	 */
	public Long getClaimNumber() {
		return claimNumber;
	}

	/**
	 * @param claimNumber
	 *            the claimNumber to set
	 */
	public void setClaimNumber(Long claimNumber) {
		this.claimNumber = claimNumber;
	}

	/**
	 * @return the paymentDate
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}

	/**
	 * @return string format of paymentDate
	 */
	public String getPaymentDateString() {
		if (paymentDate != null) {
			return DataUtilities.SDF.format(paymentDate);
		}
		return "";
	}

	/**
	 * @param paymentDate
	 *            the paymentDate to set
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * @return the serviceDate
	 */
	public Date getServiceDate() {
		return serviceDate;
	}

	/**
	 * @return string format of serviceDate
	 */
	public String getServiceDateString() {
		if (serviceDate != null) {
			return DataUtilities.SDF.format(serviceDate);
		}
		return "";
	}

	/**
	 * @param serviceDate
	 *            the serviceDate to set
	 */
	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	/**
	 * @return the paidAmount
	 */
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	/**
	 * @param paidAmount
	 *            the paidAmount to set
	 */
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	/**
	 * @return the procedureCode
	 */
	public Long getProcedureCode() {
		return procedureCode;
	}

	/**
	 * @param procedureCode
	 *            the procedureCode to set
	 */
	public void setProcedureCode(Long procedureCode) {
		this.procedureCode = procedureCode;
	}

	/**
	 * @return the healthCardNumber
	 */
	public String getHealthCardNumber() {
		return healthCardNumber;
	}

	/**
	 * @param healthCardNumber
	 *            the healthCardNumber to set
	 */
	public void setHealthCardNumber(String healthCardNumber) {
		this.healthCardNumber = healthCardNumber;
	}

	/**
	 * @return the itemCode
	 */
	public Long getItemCode() {
		return itemCode;
	}

	/**
	 * @param itemCode
	 *            the itemCode to set
	 */
	public void setItemCode(Long itemCode) {
		this.itemCode = itemCode;
	}
}
