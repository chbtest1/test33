package ca.medavie.nspp.audit.service.data;

import java.util.Date;

/**
 * Represents a single GL Number record
 */
public class DssGLNumberBO {

	private Long glNumber;
	private String glDescription;
	private boolean capped;
	private Date fiscalYearEndDate;
	private String modifiedBy;
	private Date lastModified;

	/**
	 * @return the glNumber
	 */
	public Long getGlNumber() {
		return glNumber;
	}

	/**
	 * @param glNumber
	 *            the glNumber to set
	 */
	public void setGlNumber(Long glNumber) {
		this.glNumber = glNumber;
	}

	/**
	 * @return the glDescription
	 */
	public String getGlDescription() {
		return glDescription;
	}

	/**
	 * @param glDescription
	 *            the glDescription to set
	 */
	public void setGlDescription(String glDescription) {
		this.glDescription = glDescription;
	}

	/**
	 * @return the capped
	 */
	public boolean isCapped() {
		return capped;
	}

	/**
	 * @param capped
	 *            the capped to set
	 */
	public void setCapped(boolean capped) {
		this.capped = capped;
	}

	/**
	 * @return the fiscalYearEndDate
	 */
	public Date getFiscalYearEndDate() {
		return fiscalYearEndDate;
	}

	/**
	 * @return fiscalYearEndDate in string format
	 */
	public String getFiscalYearEndDateString() {
		if (fiscalYearEndDate != null) {
			return DataUtilities.SDF.format(fiscalYearEndDate);
		}
		return "";
	}

	/**
	 * @param fiscalYearEndDate
	 *            the fiscalYearEndDate to set
	 */
	public void setFiscalYearEndDate(Date fiscalYearEndDate) {
		this.fiscalYearEndDate = fiscalYearEndDate;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return lastModified in string format
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
