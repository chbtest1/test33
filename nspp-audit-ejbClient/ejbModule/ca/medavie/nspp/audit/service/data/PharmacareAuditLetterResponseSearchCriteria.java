package ca.medavie.nspp.audit.service.data;

import java.util.Date;

public class PharmacareAuditLetterResponseSearchCriteria extends BaseSearchCriteria {

	private Long practitionerNumber;
	private String practitionerType;
	private String practitionerName;
	// health card number
	private String hcnClientID;
	private String auditType;
	private String responseStatus;
	private Date auditFromDate;
	private Date auditToDate;
	private Long auditRunNumber;

	private String claimRefNum;

	@Override
	public void resetSearchCriteria() {
		super.reset(this);

	}

	public Long getPractitionerNumber() {
		return practitionerNumber;
	}

	public void setPractitionerNumber(Long practitionerNumber) {
		this.practitionerNumber = practitionerNumber;
	}

	public String getPractitionerType() {
		return practitionerType;
	}

	public void setPractitionerType(String practitionerType) {
		this.practitionerType = practitionerType;
	}

	public String getPractitionerName() {
		return practitionerName;
	}

	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	public String getAuditType() {
		return auditType;
	}

	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public Date getAuditFromDate() {
		return auditFromDate;
	}

	public void setAuditFromDate(Date auditFromDate) {
		this.auditFromDate = auditFromDate;
	}

	public Date getAuditToDate() {
		return auditToDate;
	}

	public void setAuditToDate(Date auditToDate) {
		this.auditToDate = auditToDate;
	}

	public Long getAuditRunNumber() {
		return auditRunNumber;
	}

	public void setAuditRunNumber(Long auditRunNumber) {
		this.auditRunNumber = auditRunNumber;
	}

	public String getHcnClientID() {
		return hcnClientID;
	}

	public void setHcnClientID(String hcnClientID) {
		this.hcnClientID = hcnClientID;
	}

	public String getClaimRefNum() {
		return claimRefNum;
	}

	public void setClaimRefNum(String claimRefNum) {
		this.claimRefNum = claimRefNum;
	}

	public boolean isProviderNumberSet() {

		return (practitionerNumber != null && practitionerNumber.compareTo(new Long(0)) > 0) ? true : false;
	}

	public boolean isProviderNameSet() {
		return (practitionerName != null && !practitionerName.isEmpty()) ? true : false;
	}

	public boolean isHCNClientIDSet() {
		return (hcnClientID != null && !hcnClientID.isEmpty()) ? true : false;
	}

	public boolean isAuditTypeSet() {
		return (auditType != null && !auditType.isEmpty()) ? true : false;
	}

	public boolean isResponseStatusSet() {
		return (responseStatus != null && !responseStatus.isEmpty()) ? true : false;
	}

	public boolean isAuditFromDateSet() {
		return (auditFromDate != null) ? true : false;

	}

	public boolean isAuditToDateSet() {
		return (auditToDate != null) ? true : false;

	}

	public boolean isAuditRunNumberSet() {

		return (auditRunNumber != null && auditRunNumber.compareTo(new Long(0)) > 0) ? true : false;

	}

	public boolean isClaimRefNumberSet() {

		return (claimRefNum != null && !claimRefNum.isEmpty()) ? true : false;

	}

}
