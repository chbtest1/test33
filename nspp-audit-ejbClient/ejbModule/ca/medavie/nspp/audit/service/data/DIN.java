package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Models a single DIN (Drug Identification Number) object
 */
public class DIN {
	private Long auditCriteriaId;
	private String drugIdentificationNumber;
	private String drugName;
	private BigDecimal ageRestriction;
	private String genderRestriction;
	private Date lastModified;
	private String modifiedBy;

	/**
	 * @return the drugIdentificationNumber
	 */
	public String getDrugIdentificationNumber() {
		return drugIdentificationNumber;
	}

	/**
	 * @param drugIdentificationNumber
	 *            the drugIdentificationNumber to set
	 */
	public void setDrugIdentificationNumber(String drugIdentificationNumber) {
		this.drugIdentificationNumber = drugIdentificationNumber;
	}

	/**
	 * @return the drugName
	 */
	public String getDrugName() {
		return drugName;
	}

	/**
	 * @param drugName
	 *            the drugName to set
	 */
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	/**
	 * @return the ageRestriction
	 */
	public BigDecimal getAgeRestriction() {
		return ageRestriction;
	}

	/**
	 * @param ageRestriction
	 *            the ageRestriction to set
	 */
	public void setAgeRestriction(BigDecimal ageRestriction) {
		this.ageRestriction = ageRestriction;
	}

	/**
	 * @return the genderRestriction
	 */
	public String getGenderRestriction() {
		return genderRestriction;
	}

	/**
	 * @param genderRestriction
	 *            the genderRestriction to set
	 */
	public void setGenderRestriction(String genderRestriction) {
		this.genderRestriction = genderRestriction;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return String format of lastModified
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the auditCriteriaId
	 */
	public Long getAuditCriteriaId() {
		return auditCriteriaId;
	}

	/**
	 * @param auditCriteriaId
	 *            the auditCriteriaId to set
	 */
	public void setAuditCriteriaId(Long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}
}
