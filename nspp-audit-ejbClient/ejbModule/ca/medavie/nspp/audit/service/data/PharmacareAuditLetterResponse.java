package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

public class PharmacareAuditLetterResponse {

	@ColumnName(name = "claim_ref_num")
	private String claim_ref_num;

	@ColumnName(name = "audit_run_number")
	private Long audit_run_number;

	@ColumnName(name = "provider_number")
	private Long provider_number;

	@ColumnName(name = "provider_type")
	private String provider_type;

	@ColumnName(name = "client_id")
	private String client_id;

	@ColumnName(name = "se_selection_date")
	private Date se_selection_date;

	@ColumnName(name = "audit_letter_creation_date")
	private Date audit_letter_creation_date;

	@ColumnName(name = "response_status_code")
	private String response_status_code;

	@ColumnName(name = "number_of_times_sent")
	private Integer number_of_times_sent;

	@ColumnName(name = "chargeback_amount")
	private BigDecimal chargeback_amount;

	@ColumnName(name = "number_of_claims_affected")
	private Integer number_of_claims_affected;

	@ColumnName(name = "audit_note")
	private String audit_note;

	@ColumnName(name = "modified_by")
	private String modified_by;

	@ColumnName(name = "last_modified")
	private Date last_modified;

	@ColumnName(name = "audit_type")
	private String audit_type;

	@ColumnName(name = "din")
	private String din;

	@ColumnName(name = "claim_date")
	private String claim_date;

	@ColumnName(name = "full_prescription_cost")
	private String full_prescription_cost;

	@ColumnName(name = "original_rx_numb")
	private String original_rx_numb;

	@ColumnName(name = "mqty_paid")
	private String mqty_paid;

	@ColumnName(name = "prod_e")
	private String prod_e;

	@ColumnName(name = "client_name")
	private String client_name;

	@ColumnName(name = "provider_name")
	private String provider_name;

	public String getClaim_ref_num() {
		return claim_ref_num;
	}

	public void setClaim_ref_num(String claim_ref_num) {
		this.claim_ref_num = claim_ref_num;
	}

	public Long getAudit_run_number() {
		return audit_run_number;
	}

	public void setAudit_run_number(Long audit_run_number) {
		this.audit_run_number = audit_run_number;
	}

	public Long getProvider_number() {
		return provider_number;
	}

	public void setProvider_number(Long provider_number) {
		this.provider_number = provider_number;
	}

	public String getProvider_type() {
		return provider_type;
	}

	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public Date getSe_selection_date() {
		return se_selection_date;
	}

	public void setSe_selection_date(Date se_selection_date) {
		this.se_selection_date = se_selection_date;
	}

	public Date getAudit_letter_creation_date() {
		return audit_letter_creation_date;
	}

	/**
	 * @return String format of audit_letter_creation_date
	 */
	public String getAudit_letter_creation_dateString() {
		if (audit_letter_creation_date != null) {
			return DataUtilities.SDF.format(audit_letter_creation_date);
		}
		return "";
	}

	public void setAudit_letter_creation_date(Date audit_letter_creation_date) {
		this.audit_letter_creation_date = audit_letter_creation_date;
	}

	public String getResponse_status_code() {
		return response_status_code;
	}

	public void setResponse_status_code(String response_status_code) {
		this.response_status_code = response_status_code;
	}

	public Integer getNumber_of_times_sent() {
		return number_of_times_sent;
	}

	public void setNumber_of_times_sent(Integer number_of_times_sent) {
		this.number_of_times_sent = number_of_times_sent;
	}

	public BigDecimal getChargeback_amount() {
		return chargeback_amount;
	}

	public void setChargeback_amount(BigDecimal chargeback_amount) {
		this.chargeback_amount = chargeback_amount;
	}

	public Integer getNumber_of_claims_affected() {
		return number_of_claims_affected;
	}

	public void setNumber_of_claims_affected(Integer number_of_claims_affected) {
		this.number_of_claims_affected = number_of_claims_affected;
	}

	public String getAudit_note() {
		return audit_note;
	}

	public void setAudit_note(String audit_note) {
		this.audit_note = audit_note;
	}

	public String getModified_by() {
		return modified_by;
	}

	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	public Date getLast_modified() {
		return last_modified;
	}

	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}

	public String getAudit_type() {
		return audit_type;
	}

	public void setAudit_type(String audit_type) {
		this.audit_type = audit_type;
	}

	public String getDin() {
		return din;
	}

	public void setDin(String din) {
		this.din = din;
	}

	public String getClaim_date() {
		return claim_date;
	}

	public void setClaim_date(String claim_date) {
		this.claim_date = claim_date;
	}

	public String getFull_prescription_cost() {
		return full_prescription_cost;
	}

	public void setFull_prescription_cost(String full_prescription_cost) {
		this.full_prescription_cost = full_prescription_cost;
	}

	public String getOriginal_rx_numb() {
		return original_rx_numb;
	}

	public void setOriginal_rx_numb(String original_rx_numb) {
		this.original_rx_numb = original_rx_numb;
	}

	public String getMqty_paid() {
		return mqty_paid;
	}

	public void setMqty_paid(String mqty_paid) {
		this.mqty_paid = mqty_paid;
	}

	public String getProd_e() {
		return prod_e;
	}

	public void setProd_e(String prod_e) {
		this.prod_e = prod_e;
	}

	public String getClient_name() {
		return client_name;
	}

	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}

	public String getProvider_name() {
		return provider_name;
	}

	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}

}
