package ca.medavie.nspp.audit.service.data;

import java.util.Date;
import java.util.List;

/**
 * Used to search for Health Service Codes using search criteria provided by a user.
 */
public class HealthServiceCodeSearchCriteria extends BaseSearchCriteria {
	private String program;
	private String healthServiceCode;
	private String qualifier;
	private String effectiveDateStr;
	private String msiFeeCode;
	private String healthServiceCodeDesc;
	private List<String> modifiers;
	private List<String> implicitModifiers;

	/** Date variables used for when searching with a group filter */
	private boolean serviceInGroup = true;
	private Date groupFilterFromDate;
	private Date groupFilterToDate;

	/** Defines the available group filters the user can use */
	public enum HealthServiceCodeGroupFilters {
		SERVICE_NOT_IN_GROUP(false, "Services Not In Group"), ALL_SERVICES(true, "All Services");

		private boolean booleanValue;
		private String label;

		private HealthServiceCodeGroupFilters(boolean aBoolean, String aLabel) {
			booleanValue = aBoolean;
			label = aLabel;
		}

		public boolean getBooleanValue() {
			return booleanValue;
		}

		public String getLabel() {
			return label;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria(java.lang.Object)
	 */
	@Override
	public void resetSearchCriteria() {
		super.reset(this);

		// Apply default values following reset
		serviceInGroup = true;
	}

	/**
	 * @return true if healthServiceCode is set
	 */
	public boolean isHealthServiceCodeSet() {
		return (healthServiceCode != null && !healthServiceCode.isEmpty()) ? true : false;
	}

	/**
	 * @return true if implicitModifiers is set
	 */
	public boolean isHealthServiceDescSet() {
		return (healthServiceCodeDesc != null && !healthServiceCodeDesc.isEmpty()) ? true : false;
	}

	/**
	 * @return true if qualifier is set
	 */
	public boolean isQualifierSet() {
		return (qualifier != null && !qualifier.isEmpty()) ? true : false;
	}

	/**
	 * @return true if effectiveDate is set
	 */
	public boolean isEffectiveDateSet() {
		return (effectiveDateStr != null && !effectiveDateStr.isEmpty()) ? true : false;
	}

	/**
	 * @return true if MsiFeeCode is set
	 */
	public boolean isMsiFeeCodeSet() {
		return (msiFeeCode != null && !msiFeeCode.isEmpty()) ? true : false;
	}

	/**
	 * @return true if Modifiers are set
	 */
	public boolean isModifiersSet() {
		return (modifiers != null && !modifiers.isEmpty()) ? true : false;
	}

	/**
	 * @return true if Implicity Modifiers are set
	 */
	public boolean isImplicitModifiersSet() {
		return (implicitModifiers != null && !implicitModifiers.isEmpty()) ? true : false;
	}

	/**
	 * @return the program
	 */
	public String getProgram() {
		return program;
	}

	/**
	 * @param program
	 *            the program to set
	 */
	public void setProgram(String program) {
		this.program = program;
	}

	/**
	 * @return the healthServiceCode
	 */
	public String getHealthServiceCode() {
		return healthServiceCode;
	}

	/**
	 * @param healthServiceCode
	 *            the healthServiceCode to set
	 */
	public void setHealthServiceCode(String healthServiceCode) {
		this.healthServiceCode = healthServiceCode;
	}

	/**
	 * @return the qualifier
	 */
	public String getQualifier() {
		return qualifier;
	}

	/**
	 * @param qualifier
	 *            the qualifier to set
	 */
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	/**
	 * @return the msiFeeCode
	 */
	public String getMsiFeeCode() {
		return msiFeeCode;
	}

	/**
	 * @param msiFeeCode
	 *            the msiFeeCode to set
	 */
	public void setMsiFeeCode(String msiFeeCode) {
		this.msiFeeCode = msiFeeCode;
	}

	/**
	 * @return the healthServiceCodeDesc
	 */
	public String getHealthServiceCodeDesc() {
		return healthServiceCodeDesc;
	}

	/**
	 * @param healthServiceCodeDesc
	 *            the healthServiceCodeDesc to set
	 */
	public void setHealthServiceCodeDesc(String healthServiceCodeDesc) {
		this.healthServiceCodeDesc = healthServiceCodeDesc;
	}

	/**
	 * @return the modifiers
	 */
	public List<String> getModifiers() {
		return modifiers;
	}

	/**
	 * @param modifiers
	 *            the modifiers to set
	 */
	public void setModifiers(List<String> modifiers) {
		this.modifiers = modifiers;
	}

	/**
	 * @return the implicitModifiers
	 */
	public List<String> getImplicitModifiers() {
		return implicitModifiers;
	}

	/**
	 * @param implicitModifiers
	 *            the implicitModifiers to set
	 */
	public void setImplicitModifiers(List<String> implicitModifiers) {
		this.implicitModifiers = implicitModifiers;
	}

	/**
	 * @return the serviceInGroup
	 */
	public boolean isServiceInGroup() {
		return serviceInGroup;
	}

	/**
	 * @param serviceInGroup
	 *            the serviceInGroup to set
	 */
	public void setServiceInGroup(boolean serviceInGroup) {
		this.serviceInGroup = serviceInGroup;
	}

	/**
	 * @return the groupFilterFromDate
	 */
	public Date getGroupFilterFromDate() {
		return groupFilterFromDate;
	}

	/**
	 * @param groupFilterFromDate
	 *            the groupFilterFromDate to set
	 */
	public void setGroupFilterFromDate(Date groupFilterFromDate) {
		this.groupFilterFromDate = groupFilterFromDate;
	}

	/**
	 * @return the groupFilterToDate
	 */
	public Date getGroupFilterToDate() {
		return groupFilterToDate;
	}

	/**
	 * @param groupFilterToDate
	 *            the groupFilterToDate to set
	 */
	public void setGroupFilterToDate(Date groupFilterToDate) {
		this.groupFilterToDate = groupFilterToDate;
	}

	public String getEffectiveDateStr() {
		return effectiveDateStr;
	}

	public void setEffectiveDateStr(String effectiveDateStr) {
		this.effectiveDateStr = effectiveDateStr;
	}
}
