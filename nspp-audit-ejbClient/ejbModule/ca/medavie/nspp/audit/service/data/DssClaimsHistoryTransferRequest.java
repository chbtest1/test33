package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * This object holds all the data required to perform a DSS Claims History Transfer.
 * 
 * Includes the selected claims along with target HCN and notes.
 */
// JTRAX-50 14-FEB-18 BCAINNE
public class DssClaimsHistoryTransferRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Root information required for a transfer
	private String sourceHealthCardNumber;
	private String targetHealthCardNumber;
	private String transferNote;

	// Optional service dates
	private Date serviceFromDate;
	private Date serviceToDate;

	// Booleans that indicate if the user decided to copy all Claims under one type
	private boolean allMedicareSeClaimsSelected;
	private boolean allMedicareMainframeClaimsSelected;
	private boolean allPharmacareSeClaimsSelected;
	private boolean allPharmacareMainframeClaimsSelected;
	private boolean allDentalClaimsSelected;

	// Lists holding each claim type
	private List<MedicareClaim> medicareClaims;
	private List<MedicareClaim> medicareMainframeClaims;
	private List<PharmacareClaim> pharmacareClaims;
	private List<PharmacareClaim> pharmacareMainframeClaims;
	private List<DenticareClaim> dentalClaims;

	/**
	 * @return true if from date is provided
	 */
	public boolean isServiceFromDateSet() {
		return (serviceFromDate != null) ? true : false;
	}

	/**
	 * @return true if to date is provided
	 */
	public boolean isServiceToDateSet() {
		return (serviceToDate != null) ? true : false;
	}

	/**
	 * @return the sourceHealthCardNumber
	 */
	public String getSourceHealthCardNumber() {
		return sourceHealthCardNumber;
	}

	/**
	 * @param sourceHealthCardNumber
	 *            the sourceHealthCardNumber to set
	 */
	public void setSourceHealthCardNumber(String sourceHealthCardNumber) {
		this.sourceHealthCardNumber = sourceHealthCardNumber;
	}

	/**
	 * @return the targetHealthCardNumber
	 */
	public String getTargetHealthCardNumber() {
		return targetHealthCardNumber;
	}

	/**
	 * @param targetHealthCardNumber
	 *            the targetHealthCardNumber to set
	 */
	public void setTargetHealthCardNumber(String targetHealthCardNumber) {
		this.targetHealthCardNumber = targetHealthCardNumber;
	}

	/**
	 * @return the transferNote
	 */
	public String getTransferNote() {
		return transferNote;
	}

	/**
	 * @param transferNote
	 *            the transferNote to set
	 */
	public void setTransferNote(String transferNote) {
		this.transferNote = transferNote;
	}

	/**
	 * @return the medicareClaims
	 */
	public List<MedicareClaim> getMedicareClaims() {
		return medicareClaims;
	}

	/**
	 * @param medicareClaims
	 *            the medicareClaims to set
	 */
	public void setMedicareClaims(List<MedicareClaim> medicareClaims) {
		this.medicareClaims = medicareClaims;
	}

	/**
	 * @return the medicareMainframeClaims
	 */
	public List<MedicareClaim> getMedicareMainframeClaims() {
		return medicareMainframeClaims;
	}

	/**
	 * @param medicareMainframeClaims
	 *            the medicareMainframeClaims to set
	 */
	public void setMedicareMainframeClaims(List<MedicareClaim> medicareMainframeClaims) {
		this.medicareMainframeClaims = medicareMainframeClaims;
	}

	/**
	 * @return the pharmacareClaims
	 */
	public List<PharmacareClaim> getPharmacareClaims() {
		return pharmacareClaims;
	}

	/**
	 * @param pharmacareClaims
	 *            the pharmacareClaims to set
	 */
	public void setPharmacareClaims(List<PharmacareClaim> pharmacareClaims) {
		this.pharmacareClaims = pharmacareClaims;
	}

	/**
	 * @return the pharmacareMainframeClaims
	 */
	public List<PharmacareClaim> getPharmacareMainframeClaims() {
		return pharmacareMainframeClaims;
	}

	/**
	 * @param pharmacareMainframeClaims
	 *            the pharmacareMainframeClaims to set
	 */
	public void setPharmacareMainframeClaims(List<PharmacareClaim> pharmacareMainframeClaims) {
		this.pharmacareMainframeClaims = pharmacareMainframeClaims;
	}

	/**
	 * @return the dentalClaims
	 */
	public List<DenticareClaim> getDentalClaims() {
		return dentalClaims;
	}

	/**
	 * @param dentalClaims
	 *            the dentalClaims to set
	 */
	public void setDentalClaims(List<DenticareClaim> dentalClaims) {
		this.dentalClaims = dentalClaims;
	}

	/**
	 * @return the serviceFromDate
	 */
	public Date getServiceFromDate() {
		return serviceFromDate;
	}

	/**
	 * @param serviceFromDate
	 *            the serviceFromDate to set
	 */
	public void setServiceFromDate(Date serviceFromDate) {
		this.serviceFromDate = serviceFromDate;
	}

	/**
	 * @return the serviceToDate
	 */
	public Date getServiceToDate() {
		return serviceToDate;
	}

	/**
	 * @param serviceToDate
	 *            the serviceToDate to set
	 */
	public void setServiceToDate(Date serviceToDate) {
		this.serviceToDate = serviceToDate;
	}

	/**
	 * @return the allMedicareSeClaimsSelected
	 */
	public boolean isAllMedicareSeClaimsSelected() {
		return allMedicareSeClaimsSelected;
	}

	/**
	 * @param allMedicareSeClaimsSelected
	 *            the allMedicareSeClaimsSelected to set
	 */
	public void setAllMedicareSeClaimsSelected(boolean allMedicareSeClaimsSelected) {
		this.allMedicareSeClaimsSelected = allMedicareSeClaimsSelected;
	}

	/**
	 * @return the allMedicareMainframeClaimsSelected
	 */
	public boolean isAllMedicareMainframeClaimsSelected() {
		return allMedicareMainframeClaimsSelected;
	}

	/**
	 * @param allMedicareMainframeClaimsSelected
	 *            the allMedicareMainframeClaimsSelected to set
	 */
	public void setAllMedicareMainframeClaimsSelected(boolean allMedicareMainframeClaimsSelected) {
		this.allMedicareMainframeClaimsSelected = allMedicareMainframeClaimsSelected;
	}

	/**
	 * @return the allPharmacareSeClaimsSelected
	 */
	public boolean isAllPharmacareSeClaimsSelected() {
		return allPharmacareSeClaimsSelected;
	}

	/**
	 * @param allPharmacareSeClaimsSelected
	 *            the allPharmacareSeClaimsSelected to set
	 */
	public void setAllPharmacareSeClaimsSelected(boolean allPharmacareSeClaimsSelected) {
		this.allPharmacareSeClaimsSelected = allPharmacareSeClaimsSelected;
	}

	/**
	 * @return the allPharmacareMainframeClaimsSelected
	 */
	public boolean isAllPharmacareMainframeClaimsSelected() {
		return allPharmacareMainframeClaimsSelected;
	}

	/**
	 * @param allPharmacareMainframeClaimsSelected
	 *            the allPharmacareMainframeClaimsSelected to set
	 */
	public void setAllPharmacareMainframeClaimsSelected(boolean allPharmacareMainframeClaimsSelected) {
		this.allPharmacareMainframeClaimsSelected = allPharmacareMainframeClaimsSelected;
	}

	/**
	 * @return the allDentalClaimsSelected
	 */
	public boolean isAllDentalClaimsSelected() {
		return allDentalClaimsSelected;
	}

	/**
	 * @param allDentalClaimsSelected
	 *            the allDentalClaimsSelected to set
	 */
	public void setAllDentalClaimsSelected(boolean allDentalClaimsSelected) {
		this.allDentalClaimsSelected = allDentalClaimsSelected;
	}
}
