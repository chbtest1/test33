package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;

/**
 * Holds details of payment information for either the parent PeerGroup or Practitioner.
 */
public class PaymentInformation {

	/** Group Total variables */
	private BigDecimal groupTotalFeeForService;
	private BigDecimal groupTotalAlternateReimbursement;
	private BigDecimal groupTotalDiagnosticImage;
	private BigDecimal groupTotalBottomLineAdj;
	private BigDecimal groupTotalPayments;

	/** Group Average variables */
	private BigDecimal groupAverageFeeForService;
	private BigDecimal groupAverageAlternateReimbursement;
	private BigDecimal groupAverageDiagnosticImage;
	private BigDecimal groupAverageBottomLineAdj;
	private BigDecimal groupAverageTotalPayments;

	/** Practitioner total(s) variables */
	private BigDecimal practitionerFeeForService;
	private BigDecimal practitionerAlternateReimbursement;
	private BigDecimal practitionerDiagnosticImage;
	private BigDecimal practitionerBottomLineAdj;
	private BigDecimal practitionerTotalPayments;

	/**
	 * @return the groupTotalFeeForService
	 */
	public BigDecimal getGroupTotalFeeForService() {
		return groupTotalFeeForService;
	}

	/**
	 * @param groupTotalFeeForService
	 *            the groupTotalFeeForService to set
	 */
	public void setGroupTotalFeeForService(BigDecimal groupTotalFeeForService) {
		this.groupTotalFeeForService = groupTotalFeeForService;
	}

	/**
	 * @return the groupTotalAlternateReimbursement
	 */
	public BigDecimal getGroupTotalAlternateReimbursement() {
		return groupTotalAlternateReimbursement;
	}

	/**
	 * @param groupTotalAlternateReimbursement
	 *            the groupTotalAlternateReimbursement to set
	 */
	public void setGroupTotalAlternateReimbursement(BigDecimal groupTotalAlternateReimbursement) {
		this.groupTotalAlternateReimbursement = groupTotalAlternateReimbursement;
	}

	/**
	 * @return the groupTotalDiagnosticImage
	 */
	public BigDecimal getGroupTotalDiagnosticImage() {
		return groupTotalDiagnosticImage;
	}

	/**
	 * @param groupTotalDiagnosticImage
	 *            the groupTotalDiagnosticImage to set
	 */
	public void setGroupTotalDiagnosticImage(BigDecimal groupTotalDiagnosticImage) {
		this.groupTotalDiagnosticImage = groupTotalDiagnosticImage;
	}

	/**
	 * @return the groupTotalBottomLineAdj
	 */
	public BigDecimal getGroupTotalBottomLineAdj() {
		return groupTotalBottomLineAdj;
	}

	/**
	 * @param groupTotalBottomLineAdj
	 *            the groupTotalBottomLineAdj to set
	 */
	public void setGroupTotalBottomLineAdj(BigDecimal groupTotalBottomLineAdj) {
		this.groupTotalBottomLineAdj = groupTotalBottomLineAdj;
	}

	/**
	 * @return the groupTotalPayments
	 */
	public BigDecimal getGroupTotalPayments() {
		return groupTotalPayments;
	}

	/**
	 * @param groupTotalPayments
	 *            the groupTotalPayments to set
	 */
	public void setGroupTotalPayments(BigDecimal groupTotalPayments) {
		this.groupTotalPayments = groupTotalPayments;
	}

	/**
	 * @return the groupAverageFeeForService
	 */
	public BigDecimal getGroupAverageFeeForService() {
		return groupAverageFeeForService;
	}

	/**
	 * @param groupAverageFeeForService
	 *            the groupAverageFeeForService to set
	 */
	public void setGroupAverageFeeForService(BigDecimal groupAverageFeeForService) {
		this.groupAverageFeeForService = groupAverageFeeForService;
	}

	/**
	 * @return the groupAverageAlternateReimbursement
	 */
	public BigDecimal getGroupAverageAlternateReimbursement() {
		return groupAverageAlternateReimbursement;
	}

	/**
	 * @param groupAverageAlternateReimbursement
	 *            the groupAverageAlternateReimbursement to set
	 */
	public void setGroupAverageAlternateReimbursement(BigDecimal groupAverageAlternateReimbursement) {
		this.groupAverageAlternateReimbursement = groupAverageAlternateReimbursement;
	}

	/**
	 * @return the groupAverageDiagnosticImage
	 */
	public BigDecimal getGroupAverageDiagnosticImage() {
		return groupAverageDiagnosticImage;
	}

	/**
	 * @param groupAverageDiagnosticImage
	 *            the groupAverageDiagnosticImage to set
	 */
	public void setGroupAverageDiagnosticImage(BigDecimal groupAverageDiagnosticImage) {
		this.groupAverageDiagnosticImage = groupAverageDiagnosticImage;
	}

	/**
	 * @return the groupAverageBottomLineAdj
	 */
	public BigDecimal getGroupAverageBottomLineAdj() {
		return groupAverageBottomLineAdj;
	}

	/**
	 * @param groupAverageBottomLineAdj
	 *            the groupAverageBottomLineAdj to set
	 */
	public void setGroupAverageBottomLineAdj(BigDecimal groupAverageBottomLineAdj) {
		this.groupAverageBottomLineAdj = groupAverageBottomLineAdj;
	}

	/**
	 * @return the groupAverageTotalPayments
	 */
	public BigDecimal getGroupAverageTotalPayments() {
		return groupAverageTotalPayments;
	}

	/**
	 * @param groupAverageTotalPayments
	 *            the groupAverageTotalPayments to set
	 */
	public void setGroupAverageTotalPayments(BigDecimal groupAverageTotalPayments) {
		this.groupAverageTotalPayments = groupAverageTotalPayments;
	}

	/**
	 * @return the practitionerFeeForService
	 */
	public BigDecimal getPractitionerFeeForService() {
		return practitionerFeeForService;
	}

	/**
	 * @param practitionerFeeForService the practitionerFeeForService to set
	 */
	public void setPractitionerFeeForService(BigDecimal practitionerFeeForService) {
		this.practitionerFeeForService = practitionerFeeForService;
	}

	/**
	 * @return the practitionerAlternateReimbursement
	 */
	public BigDecimal getPractitionerAlternateReimbursement() {
		return practitionerAlternateReimbursement;
	}

	/**
	 * @param practitionerAlternateReimbursement the practitionerAlternateReimbursement to set
	 */
	public void setPractitionerAlternateReimbursement(BigDecimal practitionerAlternateReimbursement) {
		this.practitionerAlternateReimbursement = practitionerAlternateReimbursement;
	}

	/**
	 * @return the practitionerDiagnosticImage
	 */
	public BigDecimal getPractitionerDiagnosticImage() {
		return practitionerDiagnosticImage;
	}

	/**
	 * @param practitionerDiagnosticImage the practitionerDiagnosticImage to set
	 */
	public void setPractitionerDiagnosticImage(BigDecimal practitionerDiagnosticImage) {
		this.practitionerDiagnosticImage = practitionerDiagnosticImage;
	}

	/**
	 * @return the practitionerBottomLineAdj
	 */
	public BigDecimal getPractitionerBottomLineAdj() {
		return practitionerBottomLineAdj;
	}

	/**
	 * @param practitionerBottomLineAdj the practitionerBottomLineAdj to set
	 */
	public void setPractitionerBottomLineAdj(BigDecimal practitionerBottomLineAdj) {
		this.practitionerBottomLineAdj = practitionerBottomLineAdj;
	}

	/**
	 * @return the practitionerTotalPayments
	 */
	public BigDecimal getPractitionerTotalPayments() {
		return practitionerTotalPayments;
	}

	/**
	 * @param practitionerTotalPayments the practitionerTotalPayments to set
	 */
	public void setPractitionerTotalPayments(BigDecimal practitionerTotalPayments) {
		this.practitionerTotalPayments = practitionerTotalPayments;
	}
}
