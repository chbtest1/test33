package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * Models the HealthService Criteria object
 */
public class HealthServiceCriteria implements Comparable<HealthServiceCriteria> {

	/** The Program value */
	private String program;
	/** The assigned HealthServiceCode */
	private String healthServiceCode;
	/** The assigned HealthServiceId */
	private String healthServiceID;
	/** The assigned Qualifier */
	private String qualifier;
	/** The Effective date for this HealthService Criteria */
	private Date effectiveFromDate;
	/** The Effective date for this HealthService Criteria */
	private Date effectiveToDate;
	/** The MSI Fee Code */
	private String msiFeeCode;
	/** The associated HealthServiceGroup */
	private String healthServiceGroup;
	/** The Health Service Code Description */
	private String healthServiceCodeDesc;
	/** The Health Service Audit Description (Original) */
	private String healthServiceAuditOriginalDesc;
	/** The Health Service Audit Description (Updated via UI) */
	private String healthServiceAuditDesc;
	/** The Health Service Age Restriction (Original) */
	private BigDecimal originalAgeRestriction;
	/** The Health Service Age Restriction (Updated via UI) */
	private BigDecimal ageRestriction;
	/** Gender Restriction (Original) */
	private String originalGenderRestriction;
	/** Gender Restriction (Updated via UI) */
	private String genderRestriction;
	/** The Health Service Unit Formula */
	private String unitFormula;
	/** The Health Service Audit Category Code */
	private String categoryCode;
	/** Modifiers */
	private String modifiers;
	/** ImplicitModifiers */
	private String implicitModifiers;
	/** yearEndDate */
	private Date yearEndDate;
	/** providerPeerGroupId */
	private long providerPeerGroupId;
	/** Audit Criteria ID this HSC record is under */
	private Long auditCriteriaId;
	/** The user who last edited the object */
	private String modifiedBy;
	/** Date that the object was last modified */
	private Date lastModified;

	/** Indicates if the record has been selected for update on HS Descriptions page */
	private boolean selectedForUpdate;

	/**
	 * @return the program
	 */
	public String getProgram() {
		return program;
	}

	/**
	 * @param program
	 *            the program to set
	 */
	public void setProgram(String program) {
		this.program = program;
	}

	/**
	 * @return the healthServiceCode
	 */
	public String getHealthServiceCode() {
		return healthServiceCode;
	}

	/**
	 * @param healthServiceCode
	 *            the healthServiceCode to set
	 */
	public void setHealthServiceCode(String healthServiceCode) {
		this.healthServiceCode = healthServiceCode;
	}

	public String getHealthServiceID() {
		return healthServiceID;
	}

	public void setHealthServiceID(String healthServiceID) {
		this.healthServiceID = healthServiceID;
	}

	/**
	 * @return the qualifier
	 */
	public String getQualifier() {
		return qualifier;
	}

	/**
	 * @param qualifier
	 *            the qualifier to set
	 */
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}

	public String getEffectiveFromDateString() {
		if (effectiveFromDate != null) {
			return DataUtilities.SDF.format(effectiveFromDate);
		}
		return "";
	}

	/**
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public Date getEffectiveToDate() {
		return effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	/**
	 * @return the msiFeeCode
	 */
	public String getMsiFeeCode() {
		return msiFeeCode;
	}

	/**
	 * @param msiFeeCode
	 *            the msiFeeCode to set
	 */
	public void setMsiFeeCode(String msiFeeCode) {
		this.msiFeeCode = msiFeeCode;
	}

	/**
	 * @return the healthServiceGroup
	 */
	public String getHealthServiceGroup() {
		return healthServiceGroup;
	}

	/**
	 * @param healthServiceGroup
	 *            the healthServiceGroup to set
	 */
	public void setHealthServiceGroup(String healthServiceGroup) {
		this.healthServiceGroup = healthServiceGroup;
	}

	/**
	 * @return the healthServiceCodeDesc
	 */
	public String getHealthServiceCodeDesc() {
		return healthServiceCodeDesc;
	}

	/**
	 * @param healthServiceCodeDesc
	 *            the healthServiceCodeDesc to set
	 */
	public void setHealthServiceCodeDesc(String healthServiceCodeDesc) {
		this.healthServiceCodeDesc = healthServiceCodeDesc;
	}

	/**
	 * @return the modifiers
	 */
	public String getModifiers() {
		return modifiers;
	}

	/**
	 * @param modifiers
	 *            the modifiers to set
	 */
	public void setModifiers(String modifiers) {
		this.modifiers = modifiers;
	}

	/**
	 * @return the implicitModifiers
	 */
	public String getImplicitModifiers() {
		return implicitModifiers;
	}

	/**
	 * @param implicitModifiers
	 *            the implicitModifiers to set
	 */
	public void setImplicitModifiers(String implicitModifiers) {
		this.implicitModifiers = implicitModifiers;
	}

	public Date getYearEndDate() {
		return yearEndDate;
	}

	public void setYearEndDate(Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}

	public Long getProviderPeerGroupId() {
		return providerPeerGroupId;
	}

	public void setProviderPeerGroupId(Long providerPeerGroupId) {
		this.providerPeerGroupId = providerPeerGroupId;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the selectedForUpdate
	 */
	public boolean isSelectedForUpdate() {
		return selectedForUpdate;
	}

	/**
	 * @param selectedForUpdate
	 *            the selectedForUpdate to set
	 */
	public void setSelectedForUpdate(boolean selectedForUpdate) {
		this.selectedForUpdate = selectedForUpdate;
	}

	/**
	 * @return the healthServiceAuditDesc
	 */
	public String getHealthServiceAuditDesc() {
		return healthServiceAuditDesc;
	}

	/**
	 * @param healthServiceAuditDesc
	 *            the healthServiceAuditDesc to set
	 */
	public void setHealthServiceAuditDesc(String healthServiceAuditDesc) {
		this.healthServiceAuditDesc = healthServiceAuditDesc;
	}

	/**
	 * @return the categoryCode
	 */
	public String getCategoryCode() {
		return categoryCode;
	}

	/**
	 * @param categoryCode
	 *            the categoryCode to set
	 */
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	/**
	 * @return the healthServiceAuditOriginalDesc
	 */
	public String getHealthServiceAuditOriginalDesc() {
		return healthServiceAuditOriginalDesc;
	}

	/**
	 * @param healthServiceAuditOriginalDesc
	 *            the healthServiceAuditOriginalDesc to set
	 */
	public void setHealthServiceAuditOriginalDesc(String healthServiceAuditOriginalDesc) {
		this.healthServiceAuditOriginalDesc = healthServiceAuditOriginalDesc;
	}

	/**
	 * @return the ageRestriction
	 */
	public BigDecimal getAgeRestriction() {
		return ageRestriction;
	}

	/**
	 * @param ageRestriction
	 *            the ageRestriction to set
	 */
	public void setAgeRestriction(BigDecimal ageRestriction) {
		this.ageRestriction = ageRestriction;
	}

	/**
	 * @return the genderRestriction
	 */
	public String getGenderRestriction() {
		return genderRestriction;
	}

	/**
	 * @param genderRestriction
	 *            the genderRestriction to set
	 */
	public void setGenderRestriction(String genderRestriction) {
		this.genderRestriction = genderRestriction;
	}

	/**
	 * @return the unitFormula
	 */
	public String getUnitFormula() {
		return unitFormula;
	}

	/**
	 * @param unitFormula
	 *            the unitFormula to set
	 */
	public void setUnitFormula(String unitFormula) {
		this.unitFormula = unitFormula;
	}

	/**
	 * @return the originalAgeRestriction
	 */
	public BigDecimal getOriginalAgeRestriction() {
		return originalAgeRestriction;
	}

	/**
	 * @param originalAgeRestriction
	 *            the originalAgeRestriction to set
	 */
	public void setOriginalAgeRestriction(BigDecimal originalAgeRestriction) {
		this.originalAgeRestriction = originalAgeRestriction;
	}

	/**
	 * @return the originalGenderRestriction
	 */
	public String getOriginalGenderRestriction() {
		return originalGenderRestriction;
	}

	/**
	 * @param originalGenderRestriction
	 *            the originalGenderRestriction to set
	 */
	public void setOriginalGenderRestriction(String originalGenderRestriction) {
		this.originalGenderRestriction = originalGenderRestriction;
	}

	/**
	 * @return the auditCriteriaId
	 */
	public Long getAuditCriteriaId() {
		return auditCriteriaId;
	}

	/**
	 * @param auditCriteriaId
	 *            the auditCriteriaId to set
	 */
	public void setAuditCriteriaId(Long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}

	/**
	 * Compares the original and updated properties for Description to determine if the record was changed by the user.
	 * Determines if the record needs to be updated in the database
	 * 
	 * @return true if values are different. False otherwise
	 */
	public boolean compareDescriptionValues() {
		if (healthServiceAuditDesc == null || healthServiceAuditDesc.isEmpty()) {
			return (healthServiceAuditOriginalDesc != null && !healthServiceAuditOriginalDesc.isEmpty());
		} else {
			return healthServiceAuditDesc.equals(healthServiceAuditOriginalDesc) ? false : true;
		}
	}

	/**
	 * Compares the original and updated properties for Age restriction to determine if the record was changed by the
	 * user. Determines if the record needs to be updated in the database
	 * 
	 * @return true if values are different. False otherwise
	 */
	public boolean compareAgeRestrictionValues() {
		if (ageRestriction == null) {
			return originalAgeRestriction != null;
		} else {
			return ageRestriction.equals(originalAgeRestriction) ? false : true;
		}
	}

	/**
	 * Compares the original and updated properties for Gender restriction to determine if the record was changed by the
	 * user. Determines if the record needs to be updated in the database
	 * 
	 * @return true if values are different. False otherwise
	 */
	public boolean compareGenderRestrictionValues() {
		if (genderRestriction == null || genderRestriction.isEmpty()) {
			return (originalGenderRestriction != null && !originalGenderRestriction.isEmpty());
		} else {
			return genderRestriction.equals(originalGenderRestriction) ? false : true;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(HealthServiceCriteria o) {

		int last = BigInteger.valueOf(Long.valueOf(this.healthServiceID.trim())).compareTo(
				BigInteger.valueOf(Long.valueOf(o.healthServiceID.trim())));
		return last == 0 ? BigInteger.valueOf(Long.valueOf(this.healthServiceID.trim())).compareTo(
				BigInteger.valueOf(Long.valueOf(o.healthServiceID.trim()))) : last;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((effectiveFromDate == null) ? 0 : effectiveFromDate.hashCode());
		result = prime * result + ((healthServiceCode == null) ? 0 : healthServiceCode.hashCode());
		result = prime * result + ((implicitModifiers == null) ? 0 : implicitModifiers.hashCode());
		result = prime * result + ((modifiers == null) ? 0 : modifiers.hashCode());
		result = prime * result + ((qualifier == null) ? 0 : qualifier.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HealthServiceCriteria other = (HealthServiceCriteria) obj;
		if (effectiveFromDate == null) {
			if (other.effectiveFromDate != null)
				return false;
		} else if (!effectiveFromDate.equals(other.effectiveFromDate))
			return false;
		if (healthServiceCode == null) {
			if (other.healthServiceCode != null)
				return false;
		} else if (!healthServiceCode.equals(other.healthServiceCode))
			return false;
		if (implicitModifiers == null) {
			if (other.implicitModifiers != null)
				return false;
		} else if (!implicitModifiers.equals(other.implicitModifiers))
			return false;
		if (modifiers == null) {
			if (other.modifiers != null)
				return false;
		} else if (!modifiers.equals(other.modifiers))
			return false;
		if (qualifier == null) {
			if (other.qualifier != null)
				return false;
		} else if (!qualifier.equals(other.qualifier))
			return false;
		return true;
	}

	

}
