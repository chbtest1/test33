package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Represents a single Business Arrangement object
 */
public class BusinessArrangement {
	private Long busArrNumber;
	private String busArrDescription;
	private String cappingIndicator;
	private Date effectiveFromDate;
	private Date effectiveToDate;
	private String facilityInclusive;
	private Date lastModified;
	private String modifiedBy;
	private BigDecimal paymentMethod;
	private String providerName;
	private Long providerGroupId;
	private Long providerNumber;
	private String providerType;
	private Long remunerationMethod;
	private String specialtyInclusive;
	private String submitterId;

	/**
	 * Combination of Provider Number and Type or Group ID Allows proper UI filtering in Exclusions table
	 */
	private String baName;

	/**
	 * @return the busArrNumber
	 */
	public Long getBusArrNumber() {
		return busArrNumber;
	}

	/**
	 * @param busArrNumber
	 *            the busArrNumber to set
	 */
	public void setBusArrNumber(Long busArrNumber) {
		this.busArrNumber = busArrNumber;
	}

	/**
	 * @return the busArrDescription
	 */
	public String getBusArrDescription() {
		return busArrDescription;
	}

	/**
	 * @param busArrDescription
	 *            the busArrDescription to set
	 */
	public void setBusArrDescription(String busArrDescription) {
		this.busArrDescription = busArrDescription;
	}

	/**
	 * @return the cappingIndicator
	 */
	public String getCappingIndicator() {
		return cappingIndicator;
	}

	/**
	 * @param cappingIndicator
	 *            the cappingIndicator to set
	 */
	public void setCappingIndicator(String cappingIndicator) {
		this.cappingIndicator = cappingIndicator;
	}

	/**
	 * @return the effectiveFromDate
	 */
	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}

	/**
	 * @param effectiveFromDate
	 *            the effectiveFromDate to set
	 */
	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	/**
	 * @return the effectiveToDate
	 */
	public Date getEffectiveToDate() {
		return effectiveToDate;
	}

	/**
	 * @param effectiveToDate
	 *            the effectiveToDate to set
	 */
	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	/**
	 * @return the facilityInclusive
	 */
	public String getFacilityInclusive() {
		return facilityInclusive;
	}

	/**
	 * @param facilityInclusive
	 *            the facilityInclusive to set
	 */
	public void setFacilityInclusive(String facilityInclusive) {
		this.facilityInclusive = facilityInclusive;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @return lastModified formatted as a string
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF_TIME.format(lastModified);
		}
		return "";
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the paymentMethod
	 */
	public BigDecimal getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * @param paymentMethod
	 *            the paymentMethod to set
	 */
	public void setPaymentMethod(BigDecimal paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * @return the providerName
	 */
	public String getProviderName() {
		return providerName;
	}

	/**
	 * @param providerName
	 *            the providerName to set
	 */
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	/**
	 * @return the providerGroupId
	 */
	public Long getProviderGroupId() {
		return providerGroupId;
	}

	/**
	 * @param providerGroupId
	 *            the providerGroupId to set
	 */
	public void setProviderGroupId(Long providerGroupId) {
		this.providerGroupId = providerGroupId;
	}

	/**
	 * @return the providerNumber
	 */
	public Long getProviderNumber() {
		return providerNumber;
	}

	/**
	 * @param providerNumber
	 *            the providerNumber to set
	 */
	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	/**
	 * @return the providerType
	 */
	public String getProviderType() {
		return providerType;
	}

	/**
	 * @param providerType
	 *            the providerType to set
	 */
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	/**
	 * @return the remunerationMethod
	 */
	public Long getRemunerationMethod() {
		return remunerationMethod;
	}

	/**
	 * @param remunerationMethod
	 *            the remunerationMethod to set
	 */
	public void setRemunerationMethod(Long remunerationMethod) {
		this.remunerationMethod = remunerationMethod;
	}

	/**
	 * @return the specialtyInclusive
	 */
	public String getSpecialtyInclusive() {
		return specialtyInclusive;
	}

	/**
	 * @param specialtyInclusive
	 *            the specialtyInclusive to set
	 */
	public void setSpecialtyInclusive(String specialtyInclusive) {
		this.specialtyInclusive = specialtyInclusive;
	}

	/**
	 * @return the submitterId
	 */
	public String getSubmitterId() {
		return submitterId;
	}

	/**
	 * @param submitterId
	 *            the submitterId to set
	 */
	public void setSubmitterId(String submitterId) {
		this.submitterId = submitterId;
	}

	/**
	 * Constructs the name based on this records values of Practitioner Number or Group ID
	 * 
	 * @return the baName
	 */
	public String getBaName() {
		if (providerNumber != null) {
			baName = providerNumber + " - " + providerType;
			return baName;

		} else {
			// Null safety check. This should never be null in this else block.. just to be sure
			if (providerGroupId != null) {
				baName = providerGroupId.toString();
			}
			return baName;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((busArrNumber == null) ? 0 : busArrNumber.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BusinessArrangement other = (BusinessArrangement) obj;
		if (busArrNumber == null) {
			if (other.busArrNumber != null)
				return false;
		} else if (!busArrNumber.equals(other.busArrNumber))
			return false;
		return true;
	}
}
