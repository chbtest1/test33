package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.util.List;

/**
 * Holds the summary details of a DSS Claims History Transfer
 */
public class DssClaimsHistoryTransferSummary implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5446859468305363691L;
	// Source HCN details
	private String sourceHcn;
	private String sourceName;

	// Target HCN details
	private String targetHcn;
	private String targetName;

	// Transfer meta data
	private String transferRequestor;
	private String transferDate;
	private String transferComment;

	private List<TransferSummary> medicareSeClaimSummaries;
	private List<TransferSummary> medicareMainClaimsSummaries;
	private List<TransferSummary> pharmSeClaimSummaries;
	private List<TransferSummary> pharmMainClaimSummaries;
	private List<TransferSummary> denticareClaimSummaries;

	/**
	 * @return the medicareSeClaimSummaries
	 */
	public List<TransferSummary> getMedicareSeClaimSummaries() {
		return medicareSeClaimSummaries;
	}

	/**
	 * @param medicareSeClaimSummaries
	 *            the medicareSeClaimSummaries to set
	 */
	public void setMedicareSeClaimSummaries(List<TransferSummary> medicareSeClaimSummaries) {
		this.medicareSeClaimSummaries = medicareSeClaimSummaries;
	}

	/**
	 * @return the medicareMainClaimsSummaries
	 */
	public List<TransferSummary> getMedicareMainClaimsSummaries() {
		return medicareMainClaimsSummaries;
	}

	/**
	 * @param medicareMainClaimsSummaries
	 *            the medicareMainClaimsSummaries to set
	 */
	public void setMedicareMainClaimsSummaries(List<TransferSummary> medicareMainClaimsSummaries) {
		this.medicareMainClaimsSummaries = medicareMainClaimsSummaries;
	}

	/**
	 * @return the pharmSeClaimSummaries
	 */
	public List<TransferSummary> getPharmSeClaimSummaries() {
		return pharmSeClaimSummaries;
	}

	/**
	 * @param pharmSeClaimSummaries
	 *            the pharmSeClaimSummaries to set
	 */
	public void setPharmSeClaimSummaries(List<TransferSummary> pharmSeClaimSummaries) {
		this.pharmSeClaimSummaries = pharmSeClaimSummaries;
	}

	/**
	 * @return the pharmMainClaimSummaries
	 */
	public List<TransferSummary> getPharmMainClaimSummaries() {
		return pharmMainClaimSummaries;
	}

	/**
	 * @param pharmMainClaimSummaries
	 *            the pharmMainClaimSummaries to set
	 */
	public void setPharmMainClaimSummaries(List<TransferSummary> pharmMainClaimSummaries) {
		this.pharmMainClaimSummaries = pharmMainClaimSummaries;
	}

	/**
	 * @return the denticareClaimSummaries
	 */
	public List<TransferSummary> getDenticareClaimSummaries() {
		return denticareClaimSummaries;
	}

	/**
	 * @param denticareClaimSummaries
	 *            the denticareClaimSummaries to set
	 */
	public void setDenticareClaimSummaries(List<TransferSummary> denticareClaimSummaries) {
		this.denticareClaimSummaries = denticareClaimSummaries;
	}

	/**
	 * @return the sourceHcn
	 */
	public String getSourceHcn() {
		return sourceHcn;
	}

	/**
	 * @param sourceHcn
	 *            the sourceHcn to set
	 */
	public void setSourceHcn(String sourceHcn) {
		this.sourceHcn = sourceHcn;
	}

	/**
	 * @return the sourceName
	 */
	public String getSourceName() {
		return sourceName;
	}

	/**
	 * @param sourceName
	 *            the sourceName to set
	 */
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	/**
	 * @return the targetHcn
	 */
	public String getTargetHcn() {
		return targetHcn;
	}

	/**
	 * @param targetHcn
	 *            the targetHcn to set
	 */
	public void setTargetHcn(String targetHcn) {
		this.targetHcn = targetHcn;
	}

	/**
	 * @return the targetName
	 */
	public String getTargetName() {
		return targetName;
	}

	/**
	 * @param targetName
	 *            the targetName to set
	 */
	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	/**
	 * @return the transferRequestor
	 */
	public String getTransferRequestor() {
		return transferRequestor;
	}

	/**
	 * @param transferRequestor
	 *            the transferRequestor to set
	 */
	public void setTransferRequestor(String transferRequestor) {
		this.transferRequestor = transferRequestor;
	}

	/**
	 * @return the transferDate
	 */
	public String getTransferDate() {
		return transferDate;
	}

	/**
	 * @param transferDate
	 *            the transferDate to set
	 */
	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	/**
	 * @return the transferComment
	 */
	public String getTransferComment() {
		return transferComment;
	}

	/**
	 * @param transferComment
	 *            the transferComment to set
	 */
	public void setTransferComment(String transferComment) {
		this.transferComment = transferComment;
	}
}
