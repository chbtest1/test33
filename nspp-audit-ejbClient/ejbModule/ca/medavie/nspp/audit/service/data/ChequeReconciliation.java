package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Models a single DSS Cheque Reconciliation record
 */
public class ChequeReconciliation {

	/** Cheque information variables */
	@ColumnName(name = "CHEQUE_NUMBER")
	private Long chequeNumber;

	@ColumnName(name = "CHEQUE_AMOUNT")
	private BigDecimal chequeAmount;
	
	private Long payeeTypeCode;
	private String payeeType;
	private String batchNumber;
	private Long providerNumber;
	private String providerType;
	private Long providerGroupId;
	private String healthCardNumber;
	private String familyBenefitsNumber;
	private String agencyId;
	private String provinceCode;
	private Long otherPayeeId;
	private String payeeName;

	/** Payment information variables */
	@ColumnName(name = "SUMMARY_NUMBER")
	private Long summaryNumber;

	@ColumnName(name = "GL_NUMBER")
	private Long glNumber;

	@ColumnName(name = "DATE_CASHED")
	private Date dateCashed;

	@ColumnName(name = "PAYMENT_DATE")
	private Date paymentDate;
	private Long paymentRunNumber;
	private Long stubsVoided;
	private Long originalGlNumber;
	private String paidStatus;
	private String chequeNote;
	private String originalChequeNote;
	private BigDecimal claimedAmount;
	private String reconciliationStatus;
	private String registerStatus;
	private String modifiedBy;
	private Date lastModified;

	/**
	 * @return the chequeNumber
	 */
	public Long getChequeNumber() {
		return chequeNumber;
	}

	/**
	 * @param chequeNumber
	 *            the chequeNumber to set
	 */
	public void setChequeNumber(Long chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	/**
	 * @return the chequeAmount
	 */
	public BigDecimal getChequeAmount() {
		return chequeAmount;
	}

	/**
	 * @param chequeAmount
	 *            the chequeAmount to set
	 */
	public void setChequeAmount(BigDecimal chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	/**
	 * @return the payeeTypeCode
	 */
	public Long getPayeeTypeCode() {
		return payeeTypeCode;
	}

	/**
	 * @param payeeTypeCode
	 *            the payeeTypeCode to set
	 */
	public void setPayeeTypeCode(Long payeeTypeCode) {
		this.payeeTypeCode = payeeTypeCode;
	}

	/**
	 * @return the payeeType
	 */
	public String getPayeeType() {
		return payeeType;
	}

	/**
	 * @param payeeType
	 *            the payeeType to set
	 */
	public void setPayeeType(String payeeType) {
		this.payeeType = payeeType;
	}

	/**
	 * @return the paymentRunNumber
	 */
	public Long getPaymentRunNumber() {
		return paymentRunNumber;
	}

	/**
	 * @param paymentRunNumber
	 *            the paymentRunNumber to set
	 */
	public void setPaymentRunNumber(Long paymentRunNumber) {
		this.paymentRunNumber = paymentRunNumber;
	}

	/**
	 * @return the summaryNumber
	 */
	public Long getSummaryNumber() {
		return summaryNumber;
	}

	/**
	 * @param summaryNumber
	 *            the summaryNumber to set
	 */
	public void setSummaryNumber(Long summaryNumber) {
		this.summaryNumber = summaryNumber;
	}

	/**
	 * @return the stubsVoided
	 */
	public Long getStubsVoided() {
		return stubsVoided;
	}

	/**
	 * @param stubsVoided
	 *            the stubsVoided to set
	 */
	public void setStubsVoided(Long stubsVoided) {
		this.stubsVoided = stubsVoided;
	}

	/**
	 * @return the glNumber
	 */
	public Long getGlNumber() {
		return glNumber;
	}

	/**
	 * @param glNumber
	 *            the glNumber to set
	 */
	public void setGlNumber(Long glNumber) {
		this.glNumber = glNumber;
	}

	/**
	 * @return the dateCashed
	 */
	public Date getDateCashed() {
		return dateCashed;
	}

	public String getDateCashedString() {
		if (dateCashed != null) {
			return DataUtilities.SDF.format(dateCashed);
		}
		return "";
	}

	/**
	 * @param dateCashed
	 *            the dateCashed to set
	 */
	public void setDateCashed(Date dateCashed) {
		this.dateCashed = dateCashed;
	}

	/**
	 * @return the paymentDate
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}

	public String getPaymentDateString() {
		if (paymentDate != null) {
			return DataUtilities.SDF.format(paymentDate);
		}
		return "";
	}

	/**
	 * @param paymentDate
	 *            the paymentDate to set
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * @return the paidStatus
	 */
	public String getPaidStatus() {
		return paidStatus;
	}

	/**
	 * @param paidStatus
	 *            the paidStatus to set
	 */
	public void setPaidStatus(String paidStatus) {
		this.paidStatus = paidStatus;
	}

	/**
	 * @return the chequeNote
	 */
	public String getChequeNote() {
		return chequeNote;
	}

	/**
	 * @param chequeNote
	 *            the chequeNote to set
	 */
	public void setChequeNote(String chequeNote) {
		this.chequeNote = chequeNote;
	}

	/**
	 * @return the claimedAmount
	 */
	public BigDecimal getClaimedAmount() {
		return claimedAmount;
	}

	/**
	 * @param claimedAmount
	 *            the claimedAmount to set
	 */
	public void setClaimedAmount(BigDecimal claimedAmount) {
		this.claimedAmount = claimedAmount;
	}

	/**
	 * @return the reconciliationStatus
	 */
	public String getReconciliationStatus() {
		return reconciliationStatus;
	}

	/**
	 * @param reconciliationStatus
	 *            the reconciliationStatus to set
	 */
	public void setReconciliationStatus(String reconciliationStatus) {
		if (reconciliationStatus != null) { // Have this if block as a fix for the disabled medavie radio component
			this.reconciliationStatus = reconciliationStatus;
		}
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the batchNumber
	 */
	public String getBatchNumber() {
		return batchNumber;
	}

	/**
	 * @param batchNumber
	 *            the batchNumber to set
	 */
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	/**
	 * @return the providerNumber
	 */
	public Long getProviderNumber() {
		return providerNumber;
	}

	/**
	 * @param providerNumber
	 *            the providerNumber to set
	 */
	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	/**
	 * @return the providerType
	 */
	public String getProviderType() {
		return providerType;
	}

	/**
	 * @param providerType
	 *            the providerType to set
	 */
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	/**
	 * @return the providerGroupId
	 */
	public Long getProviderGroupId() {
		return providerGroupId;
	}

	/**
	 * @param providerGroupId
	 *            the providerGroupId to set
	 */
	public void setProviderGroupId(Long providerGroupId) {
		this.providerGroupId = providerGroupId;
	}

	/**
	 * @return the healthCardNumber
	 */
	public String getHealthCardNumber() {
		return healthCardNumber;
	}

	/**
	 * @param healthCardNumber
	 *            the healthCardNumber to set
	 */
	public void setHealthCardNumber(String healthCardNumber) {
		this.healthCardNumber = healthCardNumber;
	}

	/**
	 * @return the familyBenefitsNumber
	 */
	public String getFamilyBenefitsNumber() {
		return familyBenefitsNumber;
	}

	/**
	 * @param familyBenefitsNumber
	 *            the familyBenefitsNumber to set
	 */
	public void setFamilyBenefitsNumber(String familyBenefitsNumber) {
		this.familyBenefitsNumber = familyBenefitsNumber;
	}

	/**
	 * @return the agencyId
	 */
	public String getAgencyId() {
		return agencyId;
	}

	/**
	 * @param agencyId
	 *            the agencyId to set
	 */
	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	/**
	 * @return the provinceCode
	 */
	public String getProvinceCode() {
		return provinceCode;
	}

	/**
	 * @param provinceCode
	 *            the provinceCode to set
	 */
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	/**
	 * @return the otherPayeeId
	 */
	public Long getOtherPayeeId() {
		return otherPayeeId;
	}

	/**
	 * @param otherPayeeId
	 *            the otherPayeeId to set
	 */
	public void setOtherPayeeId(Long otherPayeeId) {
		this.otherPayeeId = otherPayeeId;
	}

	/**
	 * @return the payeeName
	 */
	public String getPayeeName() {
		return payeeName;
	}

	/**
	 * @param payeeName
	 *            the payeeName to set
	 */
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	/**
	 * @return the registerStatus
	 */
	public String getRegisterStatus() {
		return registerStatus;
	}

	/**
	 * @param registerStatus
	 *            the registerStatus to set
	 */
	public void setRegisterStatus(String registerStatus) {
		this.registerStatus = registerStatus;
	}

	/**
	 * @return the originalGlNumber
	 */
	public Long getOriginalGlNumber() {
		return originalGlNumber;
	}

	/**
	 * @param originalGlNumber
	 *            the originalGlNumber to set
	 */
	public void setOriginalGlNumber(Long originalGlNumber) {
		this.originalGlNumber = originalGlNumber;
	}

	/**
	 * @return the originalChequeNote
	 */
	public String getOriginalChequeNote() {
		return originalChequeNote;
	}

	/**
	 * @param originalChequeNote
	 *            the originalChequeNote to set
	 */
	public void setOriginalChequeNote(String originalChequeNote) {
		this.originalChequeNote = originalChequeNote;
	}
}
