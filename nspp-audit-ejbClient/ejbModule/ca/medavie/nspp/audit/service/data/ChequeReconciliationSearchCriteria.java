package ca.medavie.nspp.audit.service.data;

/**
 * Models the search criteria for Cheque Reconciliation
 */
public class ChequeReconciliationSearchCriteria extends BaseSearchCriteria {

	private Long chequeNumber;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria()
	 */
	@Override
	public void resetSearchCriteria() {
		super.reset(this);
	}

	public boolean isChequeNumberSet() {
		return (chequeNumber != null) ? true : false;
	}

	/**
	 * @return the chequeNumber
	 */
	public Long getChequeNumber() {
		return chequeNumber;
	}

	/**
	 * @param chequeNumber
	 *            the chequeNumber to set
	 */
	public void setChequeNumber(Long chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

}
