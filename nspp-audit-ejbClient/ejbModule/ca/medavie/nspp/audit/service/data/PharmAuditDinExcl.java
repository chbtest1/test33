package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Models a single Pharm Din Exclusion object
 */
public class PharmAuditDinExcl implements Comparable<PharmAuditDinExcl> {
	private String din;
	private String name;
	@ColumnName(name = "age_restriction")
	private BigDecimal ageRestriction;
	@ColumnName(name = "gender_restriction")
	private String genderRestriction;
	@ColumnName(name = "last_modified")
	private Date lastModified;
	@ColumnName(name = "modified_by")
	private String modifiedBy;

	public String getDin() {
		return din;
	}

	public void setDin(String din) {		
		this.din = din;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getAgeRestriction() {
		return ageRestriction;
	}

	public void setAgeRestriction(BigDecimal ageRestriction) {
		this.ageRestriction = ageRestriction;
	}

	public String getGenderRestriction() {
		return genderRestriction;
	}

	public void setGenderRestriction(String genderRestriction) {
		this.genderRestriction = genderRestriction;
	}
	
	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	
	/**
	 * @return lastModified in string format
	 */
	public String getLastModifiedString() {
		if (lastModified != null) {
			return DataUtilities.SDF.format(lastModified);
		}
		return "";
	}	

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Override
	public int compareTo(PharmAuditDinExcl o) {
			return this.din.compareTo(o.din);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((din == null) ? 0 : din.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PharmAuditDinExcl other = (PharmAuditDinExcl) obj;
		if (din == null) {
			if (other.din != null)
				return false;
		} else if (!din.equals(other.din))
			return false;
		return true;
	}
}
