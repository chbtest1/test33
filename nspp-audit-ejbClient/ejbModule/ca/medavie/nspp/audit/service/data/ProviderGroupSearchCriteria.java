package ca.medavie.nspp.audit.service.data;

/**
 * Models a PeerGroup object This represents a provider peer group business object
 */
public class ProviderGroupSearchCriteria extends BaseSearchCriteria {
	private String providerGroupId;
	private String providerGroupName;
	private String providerGroupTypeDesc;
	private String providerGroupCity;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria(java.lang.Object)
	 */
	@Override
	public void resetSearchCriteria() {
		super.reset(this);
	}

	public String getProviderGroupId() {
		return providerGroupId;
	}

	public void setProviderGroupId(String providerGroupId) {
		this.providerGroupId = providerGroupId;
	}

	public String getProviderGroupName() {
		return providerGroupName;
	}

	public void setProviderGroupName(String providerGroupName) {
		this.providerGroupName = providerGroupName;
	}

	public String getProviderGroupTypeDesc() {
		return providerGroupTypeDesc;
	}

	public void setProviderGroupTypeDesc(String providerGroupTypeDesc) {
		this.providerGroupTypeDesc = providerGroupTypeDesc;
	}

	public String getProviderGroupCity() {
		return providerGroupCity;
	}

	public void setProviderGroupCity(String providerGroupCity) {
		this.providerGroupCity = providerGroupCity;
	}

	public boolean isProviderGroupIdSet() {
		return (providerGroupId != null && !providerGroupId.isEmpty()) ? true : false;
	}

	public boolean isProviderGroupNameSet() {
		return (providerGroupName != null && !providerGroupName.isEmpty()) ? true : false;
	}

	public boolean isProviderGroupTypeDescSet() {
		return (providerGroupTypeDesc != null && !providerGroupTypeDesc.isEmpty()) ? true : false;
	}

	public boolean isProviderGroupCitySet() {
		return (providerGroupCity != null && !providerGroupCity.isEmpty()) ? true : false;
	}
}
