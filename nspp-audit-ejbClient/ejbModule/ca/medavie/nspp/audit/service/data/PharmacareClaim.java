package ca.medavie.nspp.audit.service.data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Models a single Pharmacare Claim record
 */
public class PharmacareClaim {

	/** Pharmacare SE claim variables */
	private String seNumber;
	private String seType;
	private String seCrossReferenceNumber;
	private Date dateOfService;
	private Date dateProcessed;
	private Long prescriberProviderNumber;
	private String prescriberProviderType;
	private String programCode;
	private Long providerNumber;
	private Long providerType;
	private String recipientHealthCardNumber;
	private Long recipientAge;
	private String recipientPostalCode;
	private String din;
	private BigDecimal amountPaid;

	/** Pharmacare Mainframe claim variables */
	private Long mainProviderNumber;
	private Long mainClaimNumber;
	private String mainRecipientHealthCardNumber;
	private String mainRecipientGender;
	private Long mainRecipientBirthYear;
	private Long mainPrescriberNumber;
	private Date mainDateOfService;
	private Long mainDin;
	private BigDecimal mainApproved;
	private Long mainRecipientCountyCode;
	private String mainRecipientName;
	private Long mainSeqNumber;

	/**
	 * @return the seNumber
	 */
	public String getSeNumber() {
		return seNumber;
	}

	/**
	 * @param seNumber
	 *            the seNumber to set
	 */
	public void setSeNumber(String seNumber) {
		this.seNumber = seNumber;
	}

	/**
	 * @return the seType
	 */
	public String getSeType() {
		return seType;
	}

	/**
	 * @param seType
	 *            the seType to set
	 */
	public void setSeType(String seType) {
		this.seType = seType;
	}

	/**
	 * @return the seCrossReferenceNumber
	 */
	public String getSeCrossReferenceNumber() {
		return seCrossReferenceNumber;
	}

	/**
	 * @param seCrossReferenceNumber
	 *            the seCrossReferenceNumber to set
	 */
	public void setSeCrossReferenceNumber(String seCrossReferenceNumber) {
		this.seCrossReferenceNumber = seCrossReferenceNumber;
	}

	/**
	 * @return the dateOfService
	 */
	public Date getDateOfService() {
		return dateOfService;
	}

	/**
	 * @return string format of dateOfService
	 */
	public String getDateOfServiceString() {
		if (dateOfService != null) {
			return DataUtilities.SDF.format(dateOfService);
		}
		return "";
	}

	/**
	 * @param dateOfService
	 *            the dateOfService to set
	 */
	public void setDateOfService(Date dateOfService) {
		this.dateOfService = dateOfService;
	}

	/**
	 * @return the dateProcessed
	 */
	public Date getDateProcessed() {
		return dateProcessed;
	}

	/**
	 * @return string format of dateProcessed
	 */
	public String getDateProcessedString() {
		if (dateProcessed != null) {
			return DataUtilities.SDF.format(dateProcessed);
		}
		return "";
	}

	/**
	 * @param dateProcessed
	 *            the dateProcessed to set
	 */
	public void setDateProcessed(Date dateProcessed) {
		this.dateProcessed = dateProcessed;
	}

	/**
	 * @return the prescriberProviderNumber
	 */
	public Long getPrescriberProviderNumber() {
		return prescriberProviderNumber;
	}

	/**
	 * @param prescriberProviderNumber
	 *            the prescriberProviderNumber to set
	 */
	public void setPrescriberProviderNumber(Long prescriberProviderNumber) {
		this.prescriberProviderNumber = prescriberProviderNumber;
	}

	/**
	 * @return the prescriberProviderType
	 */
	public String getPrescriberProviderType() {
		return prescriberProviderType;
	}

	/**
	 * @param prescriberProviderType
	 *            the prescriberProviderType to set
	 */
	public void setPrescriberProviderType(String prescriberProviderType) {
		this.prescriberProviderType = prescriberProviderType;
	}

	/**
	 * @return the programCode
	 */
	public String getProgramCode() {
		return programCode;
	}

	/**
	 * @param programCode
	 *            the programCode to set
	 */
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	/**
	 * @return the providerNumber
	 */
	public Long getProviderNumber() {
		return providerNumber;
	}

	/**
	 * @param providerNumber
	 *            the providerNumber to set
	 */
	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	/**
	 * @return the providerType
	 */
	public Long getProviderType() {
		return providerType;
	}

	/**
	 * @param providerType
	 *            the providerType to set
	 */
	public void setProviderType(Long providerType) {
		this.providerType = providerType;
	}

	/**
	 * @return the recipientAge
	 */
	public Long getRecipientAge() {
		return recipientAge;
	}

	/**
	 * @param recipientAge
	 *            the recipientAge to set
	 */
	public void setRecipientAge(Long recipientAge) {
		this.recipientAge = recipientAge;
	}

	/**
	 * @return the recipientPostalCode
	 */
	public String getRecipientPostalCode() {
		return recipientPostalCode;
	}

	/**
	 * @param recipientPostalCode
	 *            the recipientPostalCode to set
	 */
	public void setRecipientPostalCode(String recipientPostalCode) {
		this.recipientPostalCode = recipientPostalCode;
	}

	/**
	 * @return the din
	 */
	public String getDin() {
		return din;
	}

	/**
	 * @param din
	 *            the din to set
	 */
	public void setDin(String din) {
		this.din = din;
	}

	/**
	 * @return the amountPaid
	 */
	public BigDecimal getAmountPaid() {
		return amountPaid;
	}

	/**
	 * @param amountPaid
	 *            the amountPaid to set
	 */
	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	/**
	 * @return the mainProviderNumber
	 */
	public Long getMainProviderNumber() {
		return mainProviderNumber;
	}

	/**
	 * @param mainProviderNumber
	 *            the mainProviderNumber to set
	 */
	public void setMainProviderNumber(Long mainProviderNumber) {
		this.mainProviderNumber = mainProviderNumber;
	}

	/**
	 * @return the mainClaimNumber
	 */
	public Long getMainClaimNumber() {
		return mainClaimNumber;
	}

	/**
	 * @param mainClaimNumber
	 *            the mainClaimNumber to set
	 */
	public void setMainClaimNumber(Long mainClaimNumber) {
		this.mainClaimNumber = mainClaimNumber;
	}

	/**
	 * @return the mainRecipientGender
	 */
	public String getMainRecipientGender() {
		return mainRecipientGender;
	}

	/**
	 * @param mainRecipientGender
	 *            the mainRecipientGender to set
	 */
	public void setMainRecipientGender(String mainRecipientGender) {
		this.mainRecipientGender = mainRecipientGender;
	}

	/**
	 * @return the mainRecipientBirthYear
	 */
	public Long getMainRecipientBirthYear() {
		return mainRecipientBirthYear;
	}

	/**
	 * @param mainRecipientBirthYear
	 *            the mainRecipientBirthYear to set
	 */
	public void setMainRecipientBirthYear(Long mainRecipientBirthYear) {
		this.mainRecipientBirthYear = mainRecipientBirthYear;
	}

	/**
	 * @return the mainPrescriberNumber
	 */
	public Long getMainPrescriberNumber() {
		return mainPrescriberNumber;
	}

	/**
	 * @param mainPrescriberNumber
	 *            the mainPrescriberNumber to set
	 */
	public void setMainPrescriberNumber(Long mainPrescriberNumber) {
		this.mainPrescriberNumber = mainPrescriberNumber;
	}

	/**
	 * @return the mainDateOfService
	 */
	public Date getMainDateOfService() {
		return mainDateOfService;
	}

	/**
	 * @return string format of mainDateOfService
	 */
	public String getMainDateOfServiceString() {
		if (mainDateOfService != null) {
			return DataUtilities.SDF.format(mainDateOfService);
		}
		return "";
	}

	/**
	 * @param mainDateOfService
	 *            the mainDateOfService to set
	 */
	public void setMainDateOfService(Date mainDateOfService) {
		this.mainDateOfService = mainDateOfService;
	}

	/**
	 * @return the mainDin
	 */
	public Long getMainDin() {
		return mainDin;
	}

	/**
	 * @param mainDin
	 *            the mainDin to set
	 */
	public void setMainDin(Long mainDin) {
		this.mainDin = mainDin;
	}

	/**
	 * @return the mainApproved
	 */
	public BigDecimal getMainApproved() {
		return mainApproved;
	}

	/**
	 * @param mainApproved
	 *            the mainApproved to set
	 */
	public void setMainApproved(BigDecimal mainApproved) {
		this.mainApproved = mainApproved;
	}

	/**
	 * @return the mainRecipientCountyCode
	 */
	public Long getMainRecipientCountyCode() {
		return mainRecipientCountyCode;
	}

	/**
	 * @param mainRecipientCountyCode
	 *            the mainRecipientCountyCode to set
	 */
	public void setMainRecipientCountyCode(Long mainRecipientCountyCode) {
		this.mainRecipientCountyCode = mainRecipientCountyCode;
	}

	/**
	 * @return the mainRecipientName
	 */
	public String getMainRecipientName() {
		return mainRecipientName;
	}

	/**
	 * @param mainRecipientName
	 *            the mainRecipientName to set
	 */
	public void setMainRecipientName(String mainRecipientName) {
		this.mainRecipientName = mainRecipientName;
	}

	/**
	 * @return the recipientHealthCardNumber
	 */
	public String getRecipientHealthCardNumber() {
		return recipientHealthCardNumber;
	}

	/**
	 * @param recipientHealthCardNumber
	 *            the recipientHealthCardNumber to set
	 */
	public void setRecipientHealthCardNumber(String recipientHealthCardNumber) {
		this.recipientHealthCardNumber = recipientHealthCardNumber;
	}

	/**
	 * @return the mainRecipientHealthCardNumber
	 */
	public String getMainRecipientHealthCardNumber() {
		return mainRecipientHealthCardNumber;
	}

	/**
	 * @param mainRecipientHealthCardNumber
	 *            the mainRecipientHealthCardNumber to set
	 */
	public void setMainRecipientHealthCardNumber(String mainRecipientHealthCardNumber) {
		this.mainRecipientHealthCardNumber = mainRecipientHealthCardNumber;
	}

	/**
	 * @return the mainSeqNumber
	 */
	public Long getMainSeqNumber() {
		return mainSeqNumber;
	}

	/**
	 * @param mainSeqNumber
	 *            the mainSeqNumber to set
	 */
	public void setMainSeqNumber(Long mainSeqNumber) {
		this.mainSeqNumber = mainSeqNumber;
	}
}
