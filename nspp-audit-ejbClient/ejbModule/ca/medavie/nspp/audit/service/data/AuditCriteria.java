package ca.medavie.nspp.audit.service.data;

import java.util.Date;
import java.util.List;

/**
 * Models an Audit Criteria object.. Used in Service Verification Letters
 */
public class AuditCriteria {

	@ColumnName(name = "audit_criteria_id")
	private Long auditCriteriaId;

	@ColumnName(name = "audit_type")
	private String auditType;

	@ColumnName(name = "provider_name")
	private String providerName;

	@ColumnName(name = "provider_number")
	private Long providerNumber;

	@ColumnName(name = "provider_type")
	private String providerType;

	/**
	 * Following 2 date fields are used for Medicare
	 */
	@ColumnName(name = "audit_from_payment_date")
	private Date auditFromPaymentDate;

	@ColumnName(name = "audit_to_payment_date")
	private Date auditToPaymentDate;
	
	/**
	 * Following 2 date fields are used for Denticare
	 */
	@ColumnName(name = "from_payment_date")
	private Date fromPaymentDate;
	
	@ColumnName(name = "to_payment_date")
	private Date toPaymentDate;

	@ColumnName(name = "audit_from_date")
	private Date auditFrom;

	@ColumnName(name = "audit_to_date")
	private Date auditTo;

	@ColumnName(name = "bus_arr_number")
	private Long businessArrNumber;

	@ColumnName(name = "audit_from_last_indicator")
	private boolean auditFromLastIndicator;

	@ColumnName(name = "other_remun_method_indicator")
	private boolean includeNonFFSIndicator;

	@ColumnName(name = "excl_diagnostic_code_indicator")
	private boolean excludeDiagnosticCodeIndicator;

	@ColumnName(name = "number_of_services_to_audit")
	private Long numberOfServicesToAudit;

	@ColumnName(name = "default_health_service_desc")
	private String defaultHealthServiceDesc;

	@ColumnName(name = "health_service_inclusive")
	private boolean healthServiceInclusive;

	@ColumnName(name = "payment_resp_inclusive")
	private boolean paymentRespInclusive;

	@ColumnName(name = "pay_to_code_inclusive")
	private boolean payToCodeInclusive;

	@ColumnName(name = "facility_inclusive")
	private boolean facilityInclusive;

	@ColumnName(name = "din_inclusive")
	private boolean dinInclusive;

	@ColumnName(name = "modified_by")
	private String modifiedBy;

	@ColumnName(name = "last_modified")
	private Date lastModified;

	@ColumnName(name = "")
	private Date lastPaymentDate;

	@ColumnName(name = "last_payment_date")
	private Long lastAuditRunNumber;

	// Value is transient... not mapped to a specific column
	private Date lastAuditDate;

	@ColumnName(name = "from_cexp_code")
	private Long fromCexpCode;

	@ColumnName(name = "to_cexp_code")
	private Long toCexpCode;

	@ColumnName(name = "one_time_audit_indicator")
	private String oneTimeAuditIndicator;

	// List holding children objects belonging to this Audit Criteria
	private List<HealthServiceCriteria> healthServiceCriterias;
	private List<PaymentResponsibility> paymentResponsibilities;
	private List<PayeeType> payeeTypes;
	private List<DIN> dins;

	/**
	 * Default constructor, sets default values on init
	 */
	public AuditCriteria() {
		this.oneTimeAuditIndicator = "N";
	}

	/**
	 * @return the auditCriteriaId
	 */
	public Long getAuditCriteriaId() {
		return auditCriteriaId;
	}

	/**
	 * @param auditCriteriaId
	 *            the auditCriteriaId to set
	 */
	public void setAuditCriteriaId(Long auditCriteriaId) {
		this.auditCriteriaId = auditCriteriaId;
	}

	/**
	 * @return the auditType
	 */
	public String getAuditType() {
		return auditType;
	}

	/**
	 * @param auditType
	 *            the auditType to set
	 */
	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() {
		return lastModified;
	}

	/**
	 * @param lastModified
	 *            the lastModified to set
	 */
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return the healthServiceCriterias
	 */
	public List<HealthServiceCriteria> getHealthServiceCriterias() {
		return healthServiceCriterias;
	}

	/**
	 * @param healthServiceCriterias
	 *            the healthServiceCriterias to set
	 */
	public void setHealthServiceCriterias(List<HealthServiceCriteria> healthServiceCriterias) {
		this.healthServiceCriterias = healthServiceCriterias;
	}

	/**
	 * @return the auditFrom
	 */
	public Date getAuditFrom() {
		return auditFrom;
	}

	/**
	 * @return string format of auditFrom
	 */
	public String getAuditFromString() {
		if (auditFrom != null) {
			return DataUtilities.SDF.format(auditFrom);
		}
		return "";
	}

	/**
	 * @param auditFrom
	 *            the auditFrom to set
	 */
	public void setAuditFrom(Date auditFrom) {
		this.auditFrom = auditFrom;
	}

	/**
	 * @return the auditTo
	 */
	public Date getAuditTo() {
		return auditTo;
	}

	/**
	 * @return string format of auditTo
	 */
	public String getAuditToString() {
		if (auditTo != null) {
			return DataUtilities.SDF.format(auditTo);
		}
		return "";
	}

	/**
	 * @param auditTo
	 *            the auditTo to set
	 */
	public void setAuditTo(Date auditTo) {
		this.auditTo = auditTo;
	}

	/**
	 * @return the businessArrNumber
	 */
	public Long getBusinessArrNumber() {
		return businessArrNumber;
	}

	/**
	 * @param businessArrNumber
	 *            the businessArrNumber to set
	 */
	public void setBusinessArrNumber(Long businessArrNumber) {
		this.businessArrNumber = businessArrNumber;
	}

	/**
	 * @return the auditFromLastIndicator
	 */
	public boolean isAuditFromLastIndicator() {
		return auditFromLastIndicator;
	}

	/**
	 * @param auditFromLastIndicator
	 *            the auditFromLastIndicator to set
	 */
	public void setAuditFromLastIndicator(boolean auditFromLastIndicator) {
		this.auditFromLastIndicator = auditFromLastIndicator;
	}

	/**
	 * @return the auditFromPaymentDate
	 */
	public Date getAuditFromPaymentDate() {
		return auditFromPaymentDate;
	}

	/**
	 * @return string format of auditFromPaymentDate
	 */
	public String getAuditFromPaymentDateString() {
		if (auditFromPaymentDate != null) {
			return DataUtilities.SDF.format(auditFromPaymentDate);
		}
		return "";
	}

	/**
	 * @param auditFromPaymentDate
	 *            the auditFromPaymentDate to set
	 */
	public void setAuditFromPaymentDate(Date auditFromPaymentDate) {
		this.auditFromPaymentDate = auditFromPaymentDate;
	}

	/**
	 * @return the auditToPaymentDate
	 */
	public Date getAuditToPaymentDate() {
		return auditToPaymentDate;
	}

	/**
	 * @return string format of auditToPaymentDate
	 */
	public String getAuditToPaymentDateString() {
		if (auditToPaymentDate != null) {
			return DataUtilities.SDF.format(auditToPaymentDate);
		}
		return "";
	}

	/**
	 * @param auditToPaymentDate
	 *            the auditToPaymentDate to set
	 */
	public void setAuditToPaymentDate(Date auditToPaymentDate) {
		this.auditToPaymentDate = auditToPaymentDate;
	}

	/**
	 * @return the defaultHealthServiceDesc
	 */
	public String getDefaultHealthServiceDesc() {
		return defaultHealthServiceDesc;
	}

	/**
	 * @param defaultHealthServiceDesc
	 *            the defaultHealthServiceDesc to set
	 */
	public void setDefaultHealthServiceDesc(String defaultHealthServiceDesc) {
		this.defaultHealthServiceDesc = defaultHealthServiceDesc;
	}

	/**
	 * @return the excludeDiagnosticCodeIndicator
	 */
	public boolean isExcludeDiagnosticCodeIndicator() {
		return excludeDiagnosticCodeIndicator;
	}

	/**
	 * @param excludeDiagnosticCodeIndicator
	 *            the excludeDiagnosticCodeIndicator to set
	 */
	public void setExcludeDiagnosticCodeIndicator(boolean excludeDiagnosticCodeIndicator) {
		this.excludeDiagnosticCodeIndicator = excludeDiagnosticCodeIndicator;
	}

	/**
	 * @return the facilityInclusive
	 */
	public boolean isFacilityInclusive() {
		return facilityInclusive;
	}

	/**
	 * @param facilityInclusive
	 *            the facilityInclusive to set
	 */
	public void setFacilityInclusive(boolean facilityInclusive) {
		this.facilityInclusive = facilityInclusive;
	}

	/**
	 * @return the healthServiceInclusive
	 */
	public boolean isHealthServiceInclusive() {
		return healthServiceInclusive;
	}

	/**
	 * @param healthServiceInclusive
	 *            the healthServiceInclusive to set
	 */
	public void setHealthServiceInclusive(boolean healthServiceInclusive) {
		this.healthServiceInclusive = healthServiceInclusive;
	}

	/**
	 * @return the lastAuditRunNumber
	 */
	public Long getLastAuditRunNumber() {
		return lastAuditRunNumber;
	}

	/**
	 * @param lastAuditRunNumber
	 *            the lastAuditRunNumber to set
	 */
	public void setLastAuditRunNumber(Long lastAuditRunNumber) {
		this.lastAuditRunNumber = lastAuditRunNumber;
	}

	/**
	 * @return the lastPaymentDate
	 */
	public Date getLastPaymentDate() {
		return lastPaymentDate;
	}

	/**
	 * @param lastPaymentDate
	 *            the lastPaymentDate to set
	 */
	public void setLastPaymentDate(Date lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}

	/**
	 * @return the numberOfServicesToAudit
	 */
	public Long getNumberOfServicesToAudit() {
		return numberOfServicesToAudit;
	}

	/**
	 * @param numberOfServicesToAudit
	 *            the numberOfServicesToAudit to set
	 */
	public void setNumberOfServicesToAudit(Long numberOfServicesToAudit) {
		this.numberOfServicesToAudit = numberOfServicesToAudit;
	}

	/**
	 * @return the payToCodeInclusive
	 */
	public boolean isPayToCodeInclusive() {
		return payToCodeInclusive;
	}

	/**
	 * @param payToCodeInclusive
	 *            the payToCodeInclusive to set
	 */
	public void setPayToCodeInclusive(boolean payToCodeInclusive) {
		this.payToCodeInclusive = payToCodeInclusive;
	}

	/**
	 * @return the paymentRespInclusive
	 */
	public boolean isPaymentRespInclusive() {
		return paymentRespInclusive;
	}

	/**
	 * @param paymentRespInclusive
	 *            the paymentRespInclusive to set
	 */
	public void setPaymentRespInclusive(boolean paymentRespInclusive) {
		this.paymentRespInclusive = paymentRespInclusive;
	}

	/**
	 * @return the providerNumber
	 */
	public Long getProviderNumber() {
		return providerNumber;
	}

	/**
	 * @param providerNumber
	 *            the providerNumber to set
	 */
	public void setProviderNumber(Long providerNumber) {
		this.providerNumber = providerNumber;
	}

	/**
	 * @return the providerType
	 */
	public String getProviderType() {
		return providerType;
	}

	/**
	 * @param providerType
	 *            the providerType to set
	 */
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	/**
	 * @return the paymentResponsibilities
	 */
	public List<PaymentResponsibility> getPaymentResponsibilities() {
		return paymentResponsibilities;
	}

	/**
	 * @param paymentResponsibilities
	 *            the paymentResponsibilities to set
	 */
	public void setPaymentResponsibilities(List<PaymentResponsibility> paymentResponsibilities) {
		this.paymentResponsibilities = paymentResponsibilities;
	}

	/**
	 * @return the payeeTypes
	 */
	public List<PayeeType> getPayeeTypes() {
		return payeeTypes;
	}

	/**
	 * @param payeeTypes
	 *            the payeeTypes to set
	 */
	public void setPayeeTypes(List<PayeeType> payeeTypes) {
		this.payeeTypes = payeeTypes;
	}

	/**
	 * @return the includeNonFFSIndicator
	 */
	public boolean isIncludeNonFFSIndicator() {
		return includeNonFFSIndicator;
	}

	/**
	 * @param includeNonFFSIndicator
	 *            the includeNonFFSIndicator to set
	 */
	public void setIncludeNonFFSIndicator(boolean includeNonFFSIndicator) {
		this.includeNonFFSIndicator = includeNonFFSIndicator;
	}

	/**
	 * @return the lastAuditDate
	 */
	public Date getLastAuditDate() {
		return lastAuditDate;
	}

	/**
	 * @param lastAuditDate
	 *            the lastAuditDate to set
	 */
	public void setLastAuditDate(Date lastAuditDate) {
		this.lastAuditDate = lastAuditDate;
	}

	/**
	 * @return the oneTimeAuditIndicator
	 */
	public String getOneTimeAuditIndicator() {
		return oneTimeAuditIndicator;
	}

	/**
	 * @param oneTimeAuditIndicator
	 *            the oneTimeAuditIndicator to set
	 */
	public void setOneTimeAuditIndicator(String oneTimeAuditIndicator) {
		this.oneTimeAuditIndicator = oneTimeAuditIndicator;
	}

	/**
	 * @return the providerName
	 */
	public String getProviderName() {
		return providerName;
	}

	/**
	 * @param providerName
	 *            the providerName to set
	 */
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	/**
	 * @return the dins
	 */
	public List<DIN> getDins() {
		return dins;
	}

	/**
	 * @param dins
	 *            the dins to set
	 */
	public void setDins(List<DIN> dins) {
		this.dins = dins;
	}

	/**
	 * @return the dinInclusive
	 */
	public boolean isDinInclusive() {
		return dinInclusive;
	}

	/**
	 * @param dinInclusive
	 *            the dinInclusive to set
	 */
	public void setDinInclusive(boolean dinInclusive) {
		this.dinInclusive = dinInclusive;
	}

	/**
	 * @return the fromCexpCode
	 */
	public Long getFromCexpCode() {
		return fromCexpCode;
	}

	/**
	 * @param fromCexpCode
	 *            the fromCexpCode to set
	 */
	public void setFromCexpCode(Long fromCexpCode) {
		this.fromCexpCode = fromCexpCode;
	}

	/**
	 * @return the toCexpCode
	 */
	public Long getToCexpCode() {
		return toCexpCode;
	}

	/**
	 * @param toCexpCode
	 *            the toCexpCode to set
	 */
	public void setToCexpCode(Long toCexpCode) {
		this.toCexpCode = toCexpCode;
	}

	/**
	 * @return the fromPaymentDate
	 */
	public Date getFromPaymentDate() {
		return fromPaymentDate;
	}

	/**
	 * @param fromPaymentDate the fromPaymentDate to set
	 */
	public void setFromPaymentDate(Date fromPaymentDate) {
		this.fromPaymentDate = fromPaymentDate;
	}

	/**
	 * @return the toPaymentDate
	 */
	public Date getToPaymentDate() {
		return toPaymentDate;
	}

	/**
	 * @param toPaymentDate the toPaymentDate to set
	 */
	public void setToPaymentDate(Date toPaymentDate) {
		this.toPaymentDate = toPaymentDate;
	}
}
