package ca.medavie.nspp.audit.service.data;

import java.io.Serializable;
import java.util.Date;

public class AuditResultAttachment implements Serializable {

	private static final long serialVersionUID = 501454918682044580L;

	private Long attachmentId;
	private Long auditId;
	private String filename;
	private Date lastModified;
	private String modifiedBy;
	private String mimeType;

	public Long getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}

	public Long getAuditId() {
		return auditId;
	}

	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public String getLastModifiedString() {
		if (this.lastModified != null) {
			return DataUtilities.SDF.format(this.lastModified);
		}
		return "";
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attachmentId == null) ? 0 : attachmentId.hashCode());
		result = prime * result + ((auditId == null) ? 0 : auditId.hashCode());
		result = prime * result + ((filename == null) ? 0 : filename.hashCode());
		result = prime * result + ((lastModified == null) ? 0 : lastModified.hashCode());
		result = prime * result + ((mimeType == null) ? 0 : mimeType.hashCode());
		result = prime * result + ((modifiedBy == null) ? 0 : modifiedBy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AuditResultAttachment)) {
			return false;
		}
		AuditResultAttachment other = (AuditResultAttachment) obj;
		return (attachmentId == null ? other.attachmentId == null : attachmentId.equals(other.attachmentId))
				&& (auditId == null ? other.auditId == null : auditId.equals(other.auditId))
				&& filename.equals(other.filename) && lastModified.equals(other.lastModified)
				&& mimeType.equals(other.mimeType) && modifiedBy.equals(other.modifiedBy);
	}

}
