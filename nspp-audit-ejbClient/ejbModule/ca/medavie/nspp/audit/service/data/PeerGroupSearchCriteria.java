package ca.medavie.nspp.audit.service.data;

import java.util.Date;

import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria.PractitionerTypes;

/**
 * Models a search criteria for searching PeerGroups
 */
public class PeerGroupSearchCriteria extends BaseSearchCriteria {

	private PractitionerTypes profileType;
	private Long periodId;
	private Long practitionerId;
	private String subcategory;
	private Long peerGroupId;
	private Date yearEndDate;
	private String yearEndDateString;
	private String peerGroupName;
	private Long practitionerNumber;
	private String practitionerName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.service.data.BaseSearchCriteria#resetSearchCriteria(java.lang.Object)
	 */
	@Override
	public void resetSearchCriteria() {
		super.reset(this);
		this.profileType = PractitionerTypes.FEE_FOR_SERVICE;
	}

	/**
	 * @return the peerGroupId
	 */
	public Long getPeerGroupId() {
		return peerGroupId;
	}

	/**
	 * @param peerGroupId
	 *            the peerGroupId to set
	 */
	public void setPeerGroupId(Long peerGroupId) {
		this.peerGroupId = peerGroupId;
	}

	/**
	 * @return the yearEndDate
	 */
	public Date getYearEndDate() {
		return yearEndDate;
	}

	/**
	 * @param yearEndDate
	 *            the yearEndDate to set
	 */
	public void setYearEndDate(Date yearEndDate) {
		this.yearEndDate = yearEndDate;
	}

	/**
	 * @return the peerGroupName
	 */
	public String getPeerGroupName() {
		return peerGroupName;
	}

	/**
	 * @param peerGroupName
	 *            the peerGroupName to set
	 */
	public void setPeerGroupName(String peerGroupName) {
		this.peerGroupName = peerGroupName;
	}

	/**
	 * @return the practitionerNumber
	 */
	public Long getPractitionerNumber() {
		return practitionerNumber;
	}

	/**
	 * @param practitionerNumber
	 *            the practitionerNumber to set
	 */
	public void setPractitionerNumber(Long practitionerNumber) {
		this.practitionerNumber = practitionerNumber;
	}

	/**
	 * @return the practitionerName
	 */
	public String getPractitionerName() {
		return practitionerName;
	}

	/**
	 * @param practitionerName
	 *            the practitionerName to set
	 */
	public void setPractitionerName(String practitionerName) {
		this.practitionerName = practitionerName;
	}

	/**
	 * @return the profileType
	 */
	public PractitionerTypes getProfileType() {
		return profileType;
	}

	/**
	 * @param profileType
	 *            the profileType to set
	 */
	public void setProfileType(PractitionerTypes profileType) {
		this.profileType = profileType;
	}

	/**
	 * @return the periodId
	 */
	public Long getPeriodId() {
		return periodId;
	}

	/**
	 * @param periodId
	 *            the periodId to set
	 */
	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	/**
	 * @return the practitionerId
	 */
	public Long getPractitionerId() {
		return practitionerId;
	}

	/**
	 * @param practitionerId
	 *            the practitionerId to set
	 */
	public void setPractitionerId(Long practitionerId) {
		this.practitionerId = practitionerId;
	}

	/**
	 * @return the subcategory
	 */
	public String getSubcategory() {
		return subcategory;
	}

	/**
	 * @param subcategory
	 *            the subcategory to set
	 */
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	/**
	 * @return true if subcategory is set
	 */
	public boolean isSubcategorySet() {
		return (subcategory != null && !subcategory.isEmpty()) ? true : false;
	}

	/**
	 * @return true if year end date is set
	 */
	public boolean isYearEndDateSet() {

		return (yearEndDate != null) ? true : false;
	}

	/**
	 * @return true if peerGroupId is set
	 */
	public boolean isPeerGroupIDSet() {

		return (peerGroupId != null && peerGroupId.compareTo(Long.valueOf("0")) != 0) ? true : false;
	}

	/**
	 * @return true if peerGroupName is set
	 */
	public boolean isPeerGroupNameSet() {

		return (peerGroupName != null && !peerGroupName.isEmpty()) ? true : false;
	}

	/**
	 * @return true if practitionerNumber is set
	 */
	public boolean isPractitionerNumberSet() {

		return (practitionerNumber != null && practitionerNumber.compareTo(Long.valueOf("0")) != 0) ? true : false;
	}

	/**
	 * @return true if practitionerName is set
	 */
	public boolean isPractitionerNameSet() {

		return (practitionerName != null && !practitionerName.isEmpty()) ? true : false;
	}

	/**
	 * @return true if periodId is set
	 */
	public boolean isPeriodIdSet() {
		return (periodId != null);
	}

	/**
	 * @return true practitionerId is set
	 */
	public boolean isPractitionerIdSet() {
		return (practitionerId != null);
	}

	/**
	 * @return the yearEndDateString
	 */
	public String getYearEndDateString() {
		return yearEndDateString;
	}

	/**
	 * @param yearEndDateString
	 *            the yearEndDateString to set
	 */
	public void setYearEndDateString(String yearEndDateString) {
		this.yearEndDateString = yearEndDateString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PeerGroupSearchCriteria [profileType=" + profileType + ", periodId=" + periodId + ", practitionerId="
				+ practitionerId + ", subcategory=" + subcategory + ", peerGroupId=" + peerGroupId + ", yearEndDate="
				+ yearEndDate + ", peerGroupName=" + peerGroupName + ", practitionerNumber=" + practitionerNumber
				+ ", practitionerName=" + practitionerName + "]";
	}
}
