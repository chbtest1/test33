package ca.medavie.nspp.audit.service;

import java.util.List;
import java.util.Map;

import ca.medavie.nspp.audit.service.data.CodeEntry;
import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;
import ca.medavie.nspp.audit.service.data.SpecialtyCriteria;
import ca.medavie.nspp.audit.service.data.SubcategoryType;
import ca.medavie.nspp.audit.service.exception.DropDownMenuItemServiceException;

public interface DropDownMenuItemService {

	/**
	 * Get subcategory types drop down menu
	 * 
	 * @return
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, SubcategoryType> getSubCategoryTypesDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get Specialty drop down menu items
	 * 
	 * @return
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, SpecialtyCriteria> getSpecialtyDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get AuditRXType drop down menu items
	 * 
	 * @return
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, CodeEntry> getAuditRXTypeDropDown() throws DropDownMenuItemServiceException;

	/**
	 * 
	 * Get AuditNonRXType drop down menu items
	 * 
	 * @return
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, CodeEntry> getAuditNonRXTypeDropDown() throws DropDownMenuItemServiceException;

	/**
	 * @return list of ContractTown names
	 * @throws DropDownMenuItemServiceException
	 */
	public List<String> getContractTownNames() throws DropDownMenuItemServiceException;

	/**
	 * Get Qualifier drop down menu items
	 * 
	 * @return
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getQualifierDropDown() throws DropDownMenuItemServiceException;

	/**
	 * @return
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getAuditSourceMedCodeDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get AuditSourcePharmCode drop down menu items
	 * 
	 * @return
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getAuditSourcePharmCodeDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get AuditPharmTypeCode drop down menu items
	 * 
	 * @return
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getAuditPharmTypeCodeDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get GroupTypeCodes drop down menu items
	 * 
	 * @return
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getGroupTypeCodesDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get PeriodYearEndDate drop down menu items
	 * 
	 * @return map of YearEndDates (String format)
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getPeriodYearEndDateDropdown() throws DropDownMenuItemServiceException;

	/**
	 * Get PeriodIds drop down menu items
	 * 
	 * @return map of Periods
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<Long, PractitionerProfilePeriod> getPeriodIdDropdown() throws DropDownMenuItemServiceException;

	/**
	 * Get DssUnitValueLevel drop down menu items
	 * 
	 * @return DssUnitValueLevelDropdown
	 */
	public Map<Long, Long> getDssUnitValueLevelDropdown() throws DropDownMenuItemServiceException;

	/**
	 * Get DssUnitValueDescs drop down menu items
	 * 
	 * @return
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, Long> getDssUnitValueDescsDropdown() throws DropDownMenuItemServiceException;

	/**
	 * Get HealthService modifiers
	 * 
	 * @return HS modifiers
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getHsModifierDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get HealthService Implicit modifiers
	 * 
	 * @return HS Implicit modifiers
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getHsImplicitModifiersDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get HealthService Programs
	 * 
	 * @return HS programs
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getHsProgramDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get HealthServiceGroups
	 * 
	 * @return HS groups
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<Long, String> getHsGroupDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get HealthServiceQualifiers
	 * 
	 * @return HS qualifiers
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getHsQualifierDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get PayeeTypes
	 * 
	 * @return payeeTypes
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getPayeeTypeDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get Payment Responsibilities
	 * 
	 * @return Payment Responsibilities
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getPaymentResponsibilityDropDown() throws DropDownMenuItemServiceException;

	/**
	 * @return medicare audit types drop down may
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getMedicareAuditTypeDropDown() throws DropDownMenuItemServiceException;

	/**
	 * @return denticare audit types drop down map
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getDenticareAuditTypeDropDown() throws DropDownMenuItemServiceException;

	/**
	 * @return pharmacare audit types drop down map
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getPharmacareAuditTypeDropDown() throws DropDownMenuItemServiceException;

	/**
	 * @return response status
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getResponseStatusDropDown() throws DropDownMenuItemServiceException;

	/**
	 * Get GL Numbers.
	 * 
	 * @return
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getGlNumbers() throws DropDownMenuItemServiceException;

	/**
	 * @return Audit Criteria types used for Medicare and Denticare
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getGeneralAuditCriteriaTypeDropDown() throws DropDownMenuItemServiceException;

	/**
	 * @return Audit Criteria Types used for Pharmacare
	 * @throws DropDownMenuItemServiceException
	 */
	public Map<String, String> getPharmacareAuditCriteriaTypeDropDown() throws DropDownMenuItemServiceException;

	/**Get health service effective from data drop down map
	 * @return
	 */
	public Map<String, String> getHSEffectiveDateDropDown()throws DropDownMenuItemServiceException;
}
