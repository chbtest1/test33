package ca.medavie.nspp.audit.security;

import static org.omnifaces.util.Faces.getContext;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.FacesException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.FacesContext;

import org.icefaces.application.SessionExpiredException;
import org.omnifaces.exceptionhandler.FullAjaxExceptionHandler;


import javax.faces.application.ViewExpiredException;

public class MyFullAjaxExceptionHander extends FullAjaxExceptionHandler {

	private static final Logger logger = Logger.getLogger(MyFullAjaxExceptionHander.class.getName());

	public MyFullAjaxExceptionHander(ExceptionHandler wrapped) {
		super(wrapped);
	}

	@Override
	public void handle() throws FacesException {

		// It show too much log and we just want to show one line of log for session expiration
		try {
			super.handle();
		} catch (Exception e) {

			if (e.getCause() instanceof SessionExpiredException) {

				logger.log(Level.SEVERE, "Session EXPIRED! for User["
						+ getContext().getExternalContext().getRemoteUser()
						+ "] Exception type is [org.icefaces.application.SessionExpiredException]");
			}
		}

	}

	@Override
	protected void logException(FacesContext context, Throwable exception, String location, String message,
			Object... parameters) {

		if (exception instanceof ViewExpiredException) {
			logger.log(Level.SEVERE, "Session EXPIRED! for User[" + context.getExternalContext().getRemoteUser()
					+ "] exception type is [javax.faces.application.ViewExpiredException]");
		}

		else
			super.logException(context, exception, location, message, parameters);

	}

}
