package ca.medavie.nspp.audit.ui.controller;

import java.io.FileInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.SerializationUtils;
import org.icefaces.ace.component.fileentry.FileEntryResults;
import org.icefaces.ace.component.fileentry.FileEntryResults.FileInfo;
import org.icefaces.ace.component.fileentry.FileEntryStatuses;
import org.icefaces.ace.model.table.RowStateMap;
import org.icefaces.util.JavaScriptRunner;
import org.omnifaces.util.Faces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.AuditResultService;
import ca.medavie.nspp.audit.service.data.AuditResult;
import ca.medavie.nspp.audit.service.data.AuditResultAttachment;
import ca.medavie.nspp.audit.service.data.AuditResultNote;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.exception.AuditResultServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.AuditResultValidator;
import ca.medavie.nspp.audit.ui.controller.validator.FileInfoValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for the PeerGroup Bulk Copy page/panel
 */
@ManagedBean(name = "auditResultController")
@ViewScoped
public class AuditResultControllerBean extends DropdownConstantsBean implements Serializable {

	/**
	 * IDE generated serial number
	 */
	private static final long serialVersionUID = 380033445068528901L;

	// Holds reference to the EJB Client service class
	@EJB
	protected AuditResultService auditResultService;

	/** logger stuff */
	private static final Logger logger = LoggerFactory.getLogger(AuditResultControllerBean.class);

	/** Objects for SearchCriterias */
	private PractitionerSearchCriteria practitionerSearchCriteria;
	private PractitionerSearchCriteria bulkCopyPractitionerSearchCriteria;

	/** practitioner Search Results */
	private List<Practitioner> practitionerSearchResults;
	/** practitioner being selected in the page */
	private Practitioner selectedPractitioner;
	/** Selected copy of Audit Result to be edited */
	private AuditResult selectedAuditResultToEdit;
	/** Original Audit Result to be edited */
	private AuditResult originalAuditResultToEdit;
	/** a new Note To Add to this audit result */
	private AuditResultNote newNoteToAdd;
	/** A list of audit result binding to the selected practitioner */
	private List<AuditResult> listOfAuditResultToEdit;
	private RowStateMap auditResultMap;
	/** A new audit result to add **/
	private AuditResult newAuditResultToAdd;
	/** Boolean that enables custom rendering of components based on Practitioner Type */
	private boolean practitionerOfTypeRX;
	/** Visibility for attachment confirm delete pop-up */
	private boolean displayConfirmDeleteAttachment;
	/** Selected attachment to delete */
	private AuditResultAttachment selectedAttachmentToDelete;
	/** An attachment to upload */
	private FileEntryResults attachmentResults;

	/** validators */
	private AuditResultValidator validator;
	private FileInfoValidator attachmentValidator;

	/** For Bulk Copy **/
	private List<Practitioner> listOfPractitionerToCopy;
	private AuditResult newAuditResultToCopy;
	private List<AuditResult> listOfAuditResultToCopy;

	/** buttons visibility in bulk copy adding practitioner page **/
	private boolean practitionerBackButtonDisplay;
	private boolean practitionerNextButtonDisplay;

	/** buttons visibility in bulk copy adding audit result page **/
	private boolean auditResultsNextBtnDisplay;
	private boolean auditResultsBackBtnDisplay;

	/** buttons visibility in bulk summary page **/
	private boolean summaryBackBtnDisplay;
	private boolean bulkCopyButtonDisplay;

	private String bulkCopyPractitionerSubcategoryType;

	private boolean canGotoAddingAuditResultPage;
	private boolean canGotoSummaryPage;

	private boolean backToNewbulkCopyButtonDisplay;
	private boolean canDoBulkCopy;

	/** Date used by default for Final Contact Date and Interim Date */
	private Date defaultInputDate;

	private boolean displayConfirmDeleteAuditResult;

	/**
	 * Bean constructor
	 */
	public AuditResultControllerBean() {

		if (this.auditResultService == null) {
			try {
				this.auditResultService = InitialContext.doLookup(prop.getProperty(AuditResultService.class.getName()));
				logger.debug("auditResultService is loaded in the constructor");

			} catch (NamingException e) {

				e.printStackTrace();
			}
		}

		// Init validator
		if (validator == null) {
			validator = new AuditResultValidator(auditResultService);
		}
		if (attachmentValidator == null) {
			attachmentValidator = new FileInfoValidator();
		}

		// Set up the default date value
		Calendar cal = Calendar.getInstance();
		cal.set(2999, 00, 01);
		defaultInputDate = cal.getTime();

		// Init first tab since it will be displayed immediately
		initiateSearchAuditResultPanel();
	}

	public void auditResultTabChangeListener(ValueChangeEvent anEvent) {
		// Get the index of the requested tab
		Integer requestedTabIndex = (Integer) anEvent.getNewValue();
		if (requestedTabIndex.equals(Integer.valueOf("0"))) {
			initiateSearchAuditResultPanel();
		}
		if (requestedTabIndex.equals(Integer.valueOf("1"))) {
			initiateAuditResultBulkCopyPanel();
		}

	}

	/**
	 * Takes the user to Audit result search Panel
	 */
	private void initiateSearchAuditResultPanel() {

		// Reset search criteria state
		practitionerSearchCriteria = new PractitionerSearchCriteria();
		practitionerSearchResults = new ArrayList<Practitioner>();

		// Reset edit object state
		selectedPractitioner = null;
		selectedAuditResultToEdit = null;
		originalAuditResultToEdit = null;
		selectedAttachmentToDelete = null;
		newNoteToAdd = null;
	}

	/**
	 * Takes the user to view Audit result Panel
	 */
	public void executeViewAuditResultPanel(Practitioner aSelectedPractitioner) {

		try {
			practitionerOfTypeRX = false;
			selectedPractitioner = aSelectedPractitioner;

			// Reset Audit Result data table state
			resetDataTableState("editAuditResultsView:searchAuditResultPanel:practitionerAuditResultForm:practitionerAuditResultTable");

			// Toggle RX true if practitioner is of type RX
			if (selectedPractitioner.getPractitionerType().equals("RX")) {
				practitionerOfTypeRX = true;
			}

			// Create new Audit Result record. (Used if the user adds a new audit to the selected Practitioner)
			newAuditResultToAdd = createNewAuditResult();

			listOfAuditResultToEdit = auditResultService
					.getSelectedPractitionerAuditResultDetails(aSelectedPractitioner);

			// need set key for the new audit result.
			this.newAuditResultToAdd.setProviderNumber(aSelectedPractitioner.getPractitionerNumber());
			this.newAuditResultToAdd.setProviderType(aSelectedPractitioner.getPractitionerType());

			// sort the table result by last modified date in descending order
			Collections.sort(listOfAuditResultToEdit, new Comparator<AuditResult>() {
				@Override
				public int compare(AuditResult audit1, AuditResult audit2) {

					if (audit2.getLastModified() == null && audit1.getLastModified() == null) {
						return 0;
					} else if (audit2.getLastModified() == null && audit1.getLastModified() != null) {
						return -1;
					} else if (audit2.getLastModified() != null && audit1.getLastModified() == null) {
						return 1;
					} else {
						return audit2.getLastModified().compareTo(audit1.getLastModified());
					}
				}
			});

			// Display the Audit result table
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAddNewAuditResultPanel()");

		} catch (AuditResultServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Takes the user to Edit Audit result Panel
	 */
	public void executeEditAuditResultPanel(AuditResult aSelectedAuditResult) {

		try {
			// Set up a new AuditResultNote only when the view audit result button is hit
			newNoteToAdd = new AuditResultNote(aSelectedAuditResult);

			// Copy AuditResult, and select the copy
			originalAuditResultToEdit = aSelectedAuditResult;
			selectedAuditResultToEdit = SerializationUtils.clone(aSelectedAuditResult);

			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayEditAuditResultsPanel()");
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Validates the attachment, and adds the attachment to AuditResult
	 */
	public void executeFileUpload() {
		try {

			FileInfo fileInfo = null;

			if (attachmentResults != null && !attachmentResults.getFiles().isEmpty()) {
				fileInfo = attachmentResults.getFiles().get(0);
			}

			attachmentValidator.validate(fileInfo);

			byte[] fileBinary = DataUtilities.convertToByteArray(new FileInputStream(fileInfo.getFile()));

			AuditResultAttachment newAttachment = new AuditResultAttachment();
			newAttachment.setAuditId(selectedAuditResultToEdit.getAuditId());
			newAttachment.setFilename(fileInfo.getFileName());
			newAttachment.setMimeType(fileInfo.getContentType());
			newAttachment.setModifiedBy(FacesUtils.getUserId());
			newAttachment.setLastModified(new Date());

			selectedAuditResultToEdit.getAttachments().add(newAttachment);
			originalAuditResultToEdit.getAttachments().add(newAttachment);

			originalAuditResultToEdit.setModifiedBy(FacesUtils.getUserId());
			originalAuditResultToEdit.setLastModified(new Date());

			auditResultService.setAuditResult(originalAuditResultToEdit);
			auditResultService.setAttachmentBinary(newAttachment.getAttachmentId(), fileBinary);

			// display successfully upload message
			Validator
					.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.AUDIT.RESULT.ATTACHMENT.UPLOAD.SUCCESSFUL");

			fileInfo.updateStatus(FileEntryStatuses.SUCCESS, false, true);

			attachmentResults.getFiles().clear();

		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Sends the attachment file to the browser
	 * 
	 * @param anAttachment
	 */
	public void sendAttachment(AuditResultAttachment anAttachment) {

		try {
			byte[] attachmentBinary = auditResultService.getAttachmentBinary(anAttachment.getAttachmentId());
			Faces.sendFile(attachmentBinary, anAttachment.getFilename(), true);
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Selects the attachment for deletion, and the confirmation pop-up
	 * 
	 * @param anAttachment
	 *            - the selected attachment to delete
	 */
	public void selectAttachmentToDelete(AuditResultAttachment anAttachment) {

		selectedAttachmentToDelete = anAttachment;

		displayConfirmDeleteAttachment = true;

		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "hideMessages()");
	}

	/**
	 * Deletes the selected attachment upon confirmation
	 * 
	 * @throws AuditResultServiceException
	 */
	public void confirmDeleteSelectedAttachment() throws AuditResultServiceException {
		selectedAuditResultToEdit.getAttachments().remove(selectedAttachmentToDelete);
		originalAuditResultToEdit.getAttachments().remove(selectedAttachmentToDelete);

		originalAuditResultToEdit.setModifiedBy(FacesUtils.getUserId());
		originalAuditResultToEdit.setLastModified(new Date());

		auditResultService.setAuditResult(originalAuditResultToEdit);

		displayConfirmDeleteAttachment = false;
		closePopupWindowJS();
	}

	/**
	 * Closes the attachment confirm delete pop-up
	 */
	public void closeConfirmDeleteAttachmentPopup() {
		displayConfirmDeleteAttachment = false;
		closePopupWindowJS();
	}

	/**
	 * Takes the user to bulk copy Audit result
	 */
	private void initiateAuditResultBulkCopyPanel() {

		// Set tables to default state
		resetDataTableState("bulkAuditResultsView:bulkCopyAuditResultPanel:practitionerCopyListForm:practitionersToCopyTable");
		resetDataTableState("bulkAuditResultsView:bulkCopyAuditResultPanel:auditResultsToCopyForm:practitionerAuditResultTable");

		bulkCopyPractitionerSearchCriteria = new PractitionerSearchCriteria();
		listOfPractitionerToCopy = new ArrayList<Practitioner>();
		listOfAuditResultToCopy = new ArrayList<AuditResult>();

		// hide all buttons
		practitionerBackButtonDisplay = false;
		practitionerNextButtonDisplay = false;
		auditResultsNextBtnDisplay = false;
		auditResultsBackBtnDisplay = false;
		summaryBackBtnDisplay = false;
		bulkCopyButtonDisplay = false;
		backToNewbulkCopyButtonDisplay = false;

		bulkCopyPractitionerSubcategoryType = null;
	}

	/**
	 * search Practitioner result
	 */
	public void executePractitionerSearch() {

		try {
			practitionerSearchResults = auditResultService.getPractitionerSearchResult(practitionerSearchCriteria);

			// Display warning if no results returned
			if (practitionerSearchResults.isEmpty()) {
				Validator.displaySystemMessage(FacesMessage.SEVERITY_WARN, "WARN.SEARCH.NO.RESULTS");
			}
		}
		// catch service exceptions
		catch (AuditResultServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Clears the Post Audit Results search form
	 */
	public void executeClearAuditSearch() {
		clearForm();
		practitionerSearchCriteria.resetSearchCriteria();
		practitionerSearchResults.clear();
	}

	/**
	 * execute save or update Audit result from the font end
	 * 
	 * @param AuditResult
	 *            - an AuditResult to save or update
	 * 
	 * **/
	public void executeSaveAuditResult() {

		try {
			// Set edit meta data
			selectedAuditResultToEdit.setModifiedBy(FacesUtils.getUserId());
			selectedAuditResultToEdit.setLastModified(new Date());

			// valid business object first
			validator.validate(selectedAuditResultToEdit);
			// do the save
			auditResultService.setAuditResult(selectedAuditResultToEdit);

			// Replace the original AuditResult with the edited AuditRecord
			int index = listOfAuditResultToEdit.indexOf(originalAuditResultToEdit);
			listOfAuditResultToEdit.remove(originalAuditResultToEdit);
			listOfAuditResultToEdit.add(index, selectedAuditResultToEdit);

			// Close edit panels
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAddNewAuditResultPanel()");

			// Display save success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.AUDIT.RESULT.VALUE.SAVE.SUCCESSFUL");

		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		} catch (AuditResultServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Add the specified Note to the Audit Result
	 */
	public void executeAddANewNote() {
		
       if(newNoteToAdd.getNote()==null || newNoteToAdd.getNote().isEmpty()){			
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.AUDIT.RESULT.AUDIT.NOTE.EMPTY"));
			return;
		}
		// Set edit meta data
		newNoteToAdd.setModifiedBy(FacesUtils.getUserId());
		newNoteToAdd.setLastModified(new Date());
		newNoteToAdd.setNoteDate(new Date());

		selectedAuditResultToEdit.getNotes().add(0, newNoteToAdd);
		// Clear note out now that is has been added
		newNoteToAdd = new AuditResultNote(selectedAuditResultToEdit);
				
	}

	/**
	 * Add a new audit result
	 */
	public void executeAddNewAuditResult() {

		try {
			// Set edit meta data
			newAuditResultToAdd.setModifiedBy(FacesUtils.getUserId());
			newAuditResultToAdd.setLastModified(new Date());
			// it always set to "A"
			newAuditResultToAdd.setAuditIndicator("A");
			
			// set key for the new audit result
			this.newAuditResultToAdd.setProviderNumber(selectedPractitioner.getPractitionerNumber());
			this.newAuditResultToAdd.setProviderType(selectedPractitioner.getPractitionerType());

			// Apply time to the AuditDate value
			buildAuditDateValue(newAuditResultToAdd);

			// valid bo first
			validator.validate(newAuditResultToAdd);
			// do the save
			auditResultService.setAuditResult(newAuditResultToAdd);

			// add it to the top of the table
			listOfAuditResultToEdit.add(0, newAuditResultToAdd);

			// reset a new newAuditResultToAdd
			newAuditResultToAdd = createNewAuditResult();

			// Display save success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.AUDIT.RESULT.VALUE.SAVE.SUCCESSFUL");

		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		} catch (AuditResultServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * clear add new form
	 */
	public void executeClearAddNewValue() {

		// Reset a new newAuditResultToAdd
		newAuditResultToAdd = createNewAuditResult();

		// set key for the new audit result
		this.newAuditResultToAdd.setProviderNumber(selectedPractitioner.getPractitionerNumber());
		this.newAuditResultToAdd.setProviderType(selectedPractitioner.getPractitionerType());

	}

	/**
	 * close to add new audit result page
	 */
	public void executeReturnToAddNewAuditResult() {
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToAddNewAuditResult()");
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "hideMessages()");
	}

	/**
	 * return to search page
	 */
	public void executeReturnToAuditResultSearch() {
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToAuditResultSearch()");
	}

	/**
	 * Add new practitioner to the bulk copy
	 */
	public void executeAddPractitionerToCopyList() {

		Practitioner aPractititioner = null;

		try {
			List<Practitioner> results = auditResultService
					.getPractitionerSearchResult(bulkCopyPractitionerSearchCriteria);

			if (results.size() != 1) {
				// No results found, display the appropriate message based on the Subcategory selection
				if (bulkCopyPractitionerSearchCriteria.getSubcategory().equals("RX")) {
					Validator.displaySystemMessage(FacesMessage.SEVERITY_ERROR, "ERROR.AUDIT.RESULT.INVALID.PHARMACY");
				} else {
					Validator.displaySystemMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.RESULT.INVALID.PRACTITIONER");
				}

			} else {
				aPractititioner = results.get(0);
				// don't add duplicates to the table
				if (!listOfPractitionerToCopy.contains(aPractititioner)) {
					listOfPractitionerToCopy.add(aPractititioner);
				}

				// Clear out the Practitioner/Pharm ID field
				bulkCopyPractitionerSearchCriteria.setPractitionerNumber(null);
			}
		} catch (AuditResultServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Clear the add new practitioner form
	 */
	public void executeClearAddNewPractitionerForm() {
		// Clear out only the Practitioner ID
		bulkCopyPractitionerSearchCriteria.setPractitionerNumber(null);
	}

	/**
	 * Go to add new audit result page in bulk copy page
	 */
	public void executeGotoBulkCopyAddNewAuditResultPage() {

		// Hide buttons adding new practitioner page
		practitionerBackButtonDisplay = false;
		practitionerNextButtonDisplay = false;
		// show buttons in add audit result page
		auditResultsNextBtnDisplay = true;
		auditResultsBackBtnDisplay = true;
		// Hide buttons in summary page
		summaryBackBtnDisplay = false;
		bulkCopyButtonDisplay = false;

		// run javascript to switch panels
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "gotoBulkCopyAddNewAuditResultPage()");
	}

	/**
	 * Add a new audit result to copy
	 */
	public void executeAddNewAuditResultToCopy() {

		AuditResult auditResultToAdd = new AuditResult(newAuditResultToCopy);

		// in this case, we don't add it if it has the same audit date
		for (AuditResult ar : listOfAuditResultToCopy) {

			if (ar.getAuditDate().compareTo(auditResultToAdd.getAuditDate()) == 0) {
				Validator.displaySystemMessage(FacesMessage.SEVERITY_WARN,
						"INFO.AUDIT.RESULT.VALUE.DULICATE.AUDIT.DATE.AUDIT.RESULT");
				return;
			}
		}
		// valid AuditResult before add
		try {

			for (Practitioner p : listOfPractitionerToCopy) {
				auditResultToAdd.setProviderNumber(p.getPractitionerNumber());
				auditResultToAdd.setProviderType(p.getPractitionerType());

				// Validate AuditResult object
				validator.validate(auditResultToAdd);
			}

			// Add to list for copy
			listOfAuditResultToCopy.add(auditResultToAdd);

			// Clear out form values (Reset object)
			newAuditResultToCopy = createNewAuditResult();

		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		}
	}

	/**
	 * Go back to add new Practitioner Page
	 */
	public void executeGotoBulkCopyPractitionerPage() {

		// Show buttons adding new practitioner page
		practitionerBackButtonDisplay = true;
		practitionerNextButtonDisplay = true;
		// Hide buttons in add audit result page
		auditResultsNextBtnDisplay = false;
		auditResultsBackBtnDisplay = false;
		// Hide buttons in summary page
		summaryBackBtnDisplay = false;
		bulkCopyButtonDisplay = false;

		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "gotoBulkCopyPractitionerPage()");
	}

	/**
	 * Go to bulk Copy Summary page
	 */
	public void executeGoToBulkCopySummaryPage() {

		// Show buttons adding new practitioner page
		practitionerBackButtonDisplay = false;
		practitionerNextButtonDisplay = false;
		// Hide buttons in add audit result page
		auditResultsNextBtnDisplay = false;
		auditResultsBackBtnDisplay = false;
		// Show buttons in summary page
		summaryBackBtnDisplay = true;
		bulkCopyButtonDisplay = true;

		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "gotoBulkCopySummaryPage()");
	}

	/**
	 * Go to select Subcategory page
	 */
	public void executeGotoSelectSubgatetoryPage() {
		initiateAuditResultBulkCopyPanel();
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "gotoSelectSubgatetoryPage()");
	}

	/**
	 * Takes the user to the Practitioner entry screen for Bulk Audit Result
	 */
	public void displayAddingPractitionersForm() {

		// Only proceed if a selection was actually made.. Prevents UI bugs and crash
		if (bulkCopyPractitionerSubcategoryType != null) {
			// Show buttons adding new practitioner page
			practitionerBackButtonDisplay = true;
			practitionerNextButtonDisplay = true;
			// Hide buttons in add audit result page
			auditResultsNextBtnDisplay = false;
			auditResultsBackBtnDisplay = false;
			// Hide buttons in summary page
			summaryBackBtnDisplay = false;
			bulkCopyButtonDisplay = false;
			// Set RX toggle to false by default
			practitionerOfTypeRX = false;

			if (bulkCopyPractitionerSubcategoryType.equals("RX")) {
				practitionerOfTypeRX = true;
			}

			newAuditResultToCopy = createNewAuditResult();
			bulkCopyPractitionerSearchCriteria.setSubcategory(bulkCopyPractitionerSubcategoryType);

			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "gotoBulkCopyPractitionerPage()");
		}
	}

	/**
	 * Perform the AuditResult bulk copy
	 */
	public void executeBulkCopy() {
		try {
			for (AuditResult audit : listOfAuditResultToCopy) {
				for (Practitioner practitioner : listOfPractitionerToCopy) {

					// Ensure we do not use the previous audit ID
					audit.setAuditId(null);

					audit.setProviderNumber(practitioner.getPractitionerNumber());
					audit.setProviderType(practitioner.getPractitionerType());

					audit.setLastModified(new Date());
					audit.setModifiedBy(FacesUtils.getUserId());
					audit.setAuditIndicator("A");

					// Apply time to the AuditDate value
					buildAuditDateValue(audit);
					// Persist audit
					auditResultService.setAuditResult(audit);
				}
			}

			// Display save success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.AUDIT.RESULT.BULK.COPY.SAVE.SUCCESSFUL");
			// show back to new copy button
			backToNewbulkCopyButtonDisplay = true;
			// hide rest of buttons
			bulkCopyButtonDisplay = false;
			summaryBackBtnDisplay = false;

		} catch (AuditResultServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * remove the practitioner from practitioner table to copy
	 * 
	 * @param aPractitioner
	 */
	public void removePractitionerToCopy(Practitioner aPractitioner) {
		listOfPractitionerToCopy.remove(aPractitioner);
	}

	/**
	 * 
	 * Remove the audit result from audit result table
	 * 
	 * @param anAuditResult
	 */
	public void removeAuditResultToCopy(AuditResult anAuditResult) {
		listOfAuditResultToCopy.remove(anAuditResult);
	}

	/**
	 * Clear the add new audit result form
	 */
	public void executeClearAddNewAuditResultToCopyForm() {

		this.newAuditResultToCopy = createNewAuditResult();
		newAuditResultToCopy.setProviderType(bulkCopyPractitionerSubcategoryType);
	}
	
	// JTRAX-61 22-FEB-18 BCAINNE
	public void deleteSelectedAuditResults() {
		// Only display confirmation if rows are selected
		if (auditResultMap.getSelected().isEmpty()) {
			// Display error to user
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.AUDIT.RESULT.NONE.SELECTED.DELETE"));
		} else {		
			setDisplayConfirmDeleteAuditResult(true);
		}
	}
	
	/**
	 * Deletes the selected attachment upon confirmation
	 * 
	 * @throws AuditResultServiceException
	 */
	@SuppressWarnings("unchecked")
	public void confirmDeleteSelectedAuditResults() throws AuditResultServiceException {
		try {
			List<AuditResult> deleteList = auditResultMap.getSelected();
			for (AuditResult auditResult : deleteList) {
				auditResultService.deleteAuditResult(auditResult);
			}
			auditResultMap.clear();
			listOfAuditResultToEdit = auditResultService.getSelectedPractitionerAuditResultDetails(selectedPractitioner);
		} catch (AuditResultServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		setDisplayConfirmDeleteAuditResult(false);
		closePopupWindowJS();
	}	
	
	/**
	 * Closes the attachment confirm delete pop-up
	 */
	public void closeConfirmDeleteAuditResultPopup() {
		setDisplayConfirmDeleteAuditResult(false);
		closePopupWindowJS();
	}	


	/**
	 * Utility method used to build the Audit Date along with a timestamp. Since the user picks any date via datepicker
	 * time is not set. So we update the selected date with the current system time.
	 * 
	 * @param anAuditResult
	 */
	private void buildAuditDateValue(AuditResult anAuditResult) {
		// Grab the current time so it can be set to the Audit date.
		Calendar currentCal = Calendar.getInstance();

		// Create a Calendar instance with the date specified in the Audit
		Calendar auditResultCal = Calendar.getInstance();
		auditResultCal.setTime(anAuditResult.getAuditDate());

		// Set the Date on the Calendar Instance we got earlier
		auditResultCal.set(Calendar.HOUR, currentCal.get(Calendar.HOUR));
		auditResultCal.set(Calendar.MINUTE, currentCal.get(Calendar.MINUTE));
		auditResultCal.set(Calendar.SECOND, currentCal.get(Calendar.SECOND));
		auditResultCal.set(Calendar.AM_PM, currentCal.get(Calendar.AM_PM));

		// Apply update Date to AuditResult
		anAuditResult.setAuditDate(auditResultCal.getTime());
	}

	/**
	 * Constructs a brand new AuditResult object with only the required default dates set.
	 * 
	 * Interim Date is not set if its purpose is for an RX Provider type.
	 * 
	 * @return fresh AuditResult record with default dates set
	 */
	private AuditResult createNewAuditResult() {

		AuditResult ar = new AuditResult();
		ar.setFinalContactDate(defaultInputDate);

		if (!practitionerOfTypeRX) {
			ar.setInterimDate(defaultInputDate);
		}
		return ar;
	}
		
	public Practitioner getSelectedPractitioner() {
		return selectedPractitioner;
	}

	public void setSelectedPractitioner(Practitioner selectedPractitioner) {
		this.selectedPractitioner = selectedPractitioner;
	}

	public List<Practitioner> getPractitionerSearchResults() {
		return practitionerSearchResults;
	}

	public void setPractitionerSearchResults(List<Practitioner> practitionerSearchResults) {
		this.practitionerSearchResults = practitionerSearchResults;
	}

	public PractitionerSearchCriteria getPractitionerSearchCriteria() {
		return practitionerSearchCriteria;
	}

	public void setPractitionerSearchCriteria(PractitionerSearchCriteria practitionerSearchCriteria) {
		this.practitionerSearchCriteria = practitionerSearchCriteria;
	}

	public AuditResult getselectedAuditResultToEdit() {
		return selectedAuditResultToEdit;
	}

	public void setselectedAuditResultToEdit(AuditResult selectedAuditResultToEdit) {
		this.selectedAuditResultToEdit = selectedAuditResultToEdit;
	}

	public AuditResultNote getNewNoteToAdd() {
		return newNoteToAdd;
	}

	public void setNewNoteToAdd(AuditResultNote newNoteToAdd) {
		this.newNoteToAdd = newNoteToAdd;
	}

	public List<AuditResult> getListOfAuditResultToEdit() {
		return listOfAuditResultToEdit;
	}

	public void setListOfAuditResultToEdit(List<AuditResult> listOfAuditResultToEdit) {
		this.listOfAuditResultToEdit = listOfAuditResultToEdit;
	}

	/**
	 * @return the newAuditResultToAdd
	 */
	public AuditResult getNewAuditResultToAdd() {
		return newAuditResultToAdd;
	}

	/**
	 * @param newAuditResultToAdd
	 *            the newAuditResultToAdd to set
	 */
	public void setNewAuditResultToAdd(AuditResult newAuditResultToAdd) {
		this.newAuditResultToAdd = newAuditResultToAdd;
	}

	/**
	 * @return the listOfPractitionerToCopy
	 */
	public List<Practitioner> getListOfPractitionerToCopy() {
		return listOfPractitionerToCopy;
	}

	/**
	 * @param listOfPractitionerToCopy
	 *            the listOfPractitionerToCopy to set
	 */
	public void setListOfPractitionerToCopy(List<Practitioner> listOfPractitionerToCopy) {
		this.listOfPractitionerToCopy = listOfPractitionerToCopy;
	}

	/**
	 * @return the listOfAuditResultToCopy
	 */
	public List<AuditResult> getListOfAuditResultToCopy() {
		return listOfAuditResultToCopy;
	}

	/**
	 * @param listOfAuditResultToCopy
	 *            the listOfAuditResultToCopy to set
	 */
	public void setListOfAuditResultToCopy(List<AuditResult> listOfAuditResultToCopy) {
		this.listOfAuditResultToCopy = listOfAuditResultToCopy;
	}

	/**
	 * @return the bulkCopyPractitionerSearchCriteria
	 */
	public PractitionerSearchCriteria getBulkCopyPractitionerSearchCriteria() {
		return bulkCopyPractitionerSearchCriteria;
	}

	/**
	 * @param bulkCopyPractitionerSearchCriteria
	 *            the bulkCopyPractitionerSearchCriteria to set
	 */
	public void setBulkCopyPractitionerSearchCriteria(PractitionerSearchCriteria bulkCopyPractitionerSearchCriteria) {
		this.bulkCopyPractitionerSearchCriteria = bulkCopyPractitionerSearchCriteria;
	}

	/**
	 * @return the newAuditResultToCopy
	 */
	public AuditResult getNewAuditResultToCopy() {
		return newAuditResultToCopy;
	}

	/**
	 * @param newAuditResultToCopy
	 *            the newAuditResultToCopy to set
	 */
	public void setNewAuditResultToCopy(AuditResult newAuditResultToCopy) {
		this.newAuditResultToCopy = newAuditResultToCopy;
	}

	/**
	 * @return the bulkCopyButtonDisplay
	 */
	public boolean isBulkCopyButtonDisplay() {
		return bulkCopyButtonDisplay;
	}

	/**
	 * @param bulkCopyButtonDisplay
	 *            the bulkCopyButtonDisplay to set
	 */
	public void setBulkCopyButtonDisplay(boolean copyButtonDisplay) {
		this.bulkCopyButtonDisplay = copyButtonDisplay;
	}

	/**
	 * @return the practitionerNextButtonDisplay
	 */
	public boolean isPractitionerNextButtonDisplay() {
		return practitionerNextButtonDisplay;
	}

	/**
	 * @param practitionerNextButtonDisplay
	 *            the practitionerNextButtonDisplay to set
	 */
	public void setPractitionerNextButtonDisplay(boolean practitionerNextButtonDisplay) {
		this.practitionerNextButtonDisplay = practitionerNextButtonDisplay;
	}

	/**
	 * @return the practitionerBackButtonDisplay
	 */
	public boolean isPractitionerBackButtonDisplay() {
		return practitionerBackButtonDisplay;
	}

	/**
	 * @param practitionerBackButtonDisplay
	 *            the practitionerBackButtonDisplay to set
	 */
	public void setPractitionerBackButtonDisplay(boolean practitionerBackButtonDisplay) {
		this.practitionerBackButtonDisplay = practitionerBackButtonDisplay;
	}

	/**
	 * @return the summaryBackBtnDisplay
	 */
	public boolean isSummaryBackBtnDisplay() {
		return summaryBackBtnDisplay;
	}

	/**
	 * @param summaryBackBtnDisplay
	 *            the summaryBackBtnDisplay to set
	 */
	public void setSummaryBackBtnDisplay(boolean summaryBackBtnDisplay) {
		this.summaryBackBtnDisplay = summaryBackBtnDisplay;
	}

	/**
	 * @return the auditResultsNextBtnDisplay
	 */
	public boolean isAuditResultsNextBtnDisplay() {
		return auditResultsNextBtnDisplay;
	}

	/**
	 * @param auditResultsNextBtnDisplay
	 *            the auditResultsNextBtnDisplay to set
	 */
	public void setAuditResultsNextBtnDisplay(boolean auditResultsNextBtnDisplay) {
		this.auditResultsNextBtnDisplay = auditResultsNextBtnDisplay;
	}

	/**
	 * @return the auditResultsBackBtnDisplay
	 */
	public boolean isAuditResultsBackBtnDisplay() {
		return auditResultsBackBtnDisplay;
	}

	/**
	 * @param auditResultsBackBtnDisplay
	 *            the auditResultsBackBtnDisplay to set
	 */
	public void setAuditResultsBackBtnDisplay(boolean auditResultsBackBtnDisplay) {
		this.auditResultsBackBtnDisplay = auditResultsBackBtnDisplay;
	}

	/**
	 * @return the bulkCopyPractitionerSubcategoryType
	 */
	public String getBulkCopyPractitionerSubcategoryType() {
		return bulkCopyPractitionerSubcategoryType;
	}

	/**
	 * @param bulkCopyPractitionerSubcategoryType
	 *            the bulkCopyPractitionerSubcategoryType to set
	 */
	public void setBulkCopyPractitionerSubcategoryType(String bulkCopyPractitionerSubcategoryType) {
		this.bulkCopyPractitionerSubcategoryType = bulkCopyPractitionerSubcategoryType;
	}

	/**
	 * @return the canGotoAddingAuditResultPage
	 */
	public boolean isCanGotoAddingAuditResultPage() {

		if (listOfPractitionerToCopy != null && !listOfPractitionerToCopy.isEmpty()) {

			return true;
		}
		return canGotoAddingAuditResultPage;
	}

	/**
	 * @param canGotoAddingAuditResultPage
	 *            the canGotoAddingAuditResultPage to set
	 */
	public void setCanGotoAddingAuditResultPage(boolean canGotoAddingAuditResultPage) {
		this.canGotoAddingAuditResultPage = canGotoAddingAuditResultPage;
	}

	/**
	 * @return the canGotoSummaryPage
	 */
	public boolean isCanGotoSummaryPage() {
		if (listOfAuditResultToCopy != null && !listOfAuditResultToCopy.isEmpty()) {

			return true;
		}
		return canGotoSummaryPage;
	}

	/**
	 * @param canGotoSummaryPage
	 *            the canGotoSummaryPage to set
	 */
	public void setCanGotoSummaryPage(boolean canGotoSummaryPage) {
		this.canGotoSummaryPage = canGotoSummaryPage;
	}

	/**
	 * @return the backToNewbulkCopyButtonDisplay
	 */
	public boolean isBackToNewbulkCopyButtonDisplay() {
		return backToNewbulkCopyButtonDisplay;
	}

	/**
	 * @param backToNewbulkCopyButtonDisplay
	 *            the backToNewbulkCopyButtonDisplay to set
	 */
	public void setBackToNewbulkCopyButtonDisplay(boolean backToNewbulkCopyButtonDisplay) {
		this.backToNewbulkCopyButtonDisplay = backToNewbulkCopyButtonDisplay;
	}

	/**
	 * @return the canDoBulkCopy
	 */
	public boolean isCanDoBulkCopy() {

		if (listOfAuditResultToCopy != null && !listOfAuditResultToCopy.isEmpty() && listOfPractitionerToCopy != null
				&& !listOfPractitionerToCopy.isEmpty()) {

			return true;
		}

		return canDoBulkCopy;
	}

	/**
	 * @param canDoBulkCopy
	 *            the canDoBulkCopy to set
	 */
	public void setCanDoBulkCopy(boolean canDoBulkCopy) {
		this.canDoBulkCopy = canDoBulkCopy;
	}

	/**
	 * @return the practitionerOfTypeRX
	 */
	public boolean isPractitionerOfTypeRX() {
		return practitionerOfTypeRX;
	}

	/**
	 * @param practitionerOfTypeRX
	 *            the practitionerOfTypeRX to set
	 */
	public void setPractitionerOfTypeRX(boolean practitionerOfTypeRX) {
		this.practitionerOfTypeRX = practitionerOfTypeRX;
	}

	public boolean isDisplayConfirmDeleteAttachment() {
		return displayConfirmDeleteAttachment;
	}

	public void setDisplayConfirmDeleteAttachment(boolean displayConfirmDeleteAttachment) {
		this.displayConfirmDeleteAttachment = displayConfirmDeleteAttachment;
	}

	public FileEntryResults getAttachmentResults() {
		return attachmentResults;
	}

	public void setAttachmentResults(FileEntryResults attachmentResults) {
		this.attachmentResults = attachmentResults;
	}
	
	// JTRAX-56 08-FEB-18 BCAINNE
	public void executeHSCLookup() {
		String desc = null;
		if(getNewAuditResultToAdd().getHealthServiceCode() != null && !getNewAuditResultToAdd().getHealthServiceCode().isEmpty()) {
			try {
				desc = auditResultService.getReason(getNewAuditResultToAdd().getHealthServiceCode(), getNewAuditResultToAdd().getQualifier() ).trim();
			} catch (AuditResultServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			if (desc != null) {
				getNewAuditResultToAdd().setAuditReason(desc);
			}
		}
    }
	
	// JTRAX-56 08-FEB-18 BCAINNE	
	public void executeNewHSCLookup() {
		String desc = null;
		if(newAuditResultToCopy.getHealthServiceCode() != null && !newAuditResultToCopy.getHealthServiceCode().isEmpty()) {
			try {
				desc = auditResultService.getReason(newAuditResultToCopy.getHealthServiceCode(), newAuditResultToCopy.getQualifier() ).trim();
			} catch (AuditResultServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			if (desc != null) {
				newAuditResultToCopy.setAuditReason(desc);
			}
		}
    }
	
	
	// JTRAX-56 08-MAR-18 BCAINNE
	public void executeEditHSCLookup() {
		String desc = null;
		try {
			desc = auditResultService.getReason(selectedAuditResultToEdit.getHealthServiceCode(), selectedAuditResultToEdit.getQualifier() ).trim();
		} catch (AuditResultServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		if (desc != null) {
			selectedAuditResultToEdit.setAuditReason(desc);
		}
    }

	
	public boolean isDisplayConfirmDeleteAuditResult() {
		return displayConfirmDeleteAuditResult;
	}

	public void setDisplayConfirmDeleteAuditResult(
			boolean displayConfirmDeleteAuditResult) {
		this.displayConfirmDeleteAuditResult = displayConfirmDeleteAuditResult;
	}

	public RowStateMap getAuditResultMap() {
		return auditResultMap;
	}

	public void setAuditResultMap(RowStateMap auditResultMap) {
		this.auditResultMap = auditResultMap;
	}
}
