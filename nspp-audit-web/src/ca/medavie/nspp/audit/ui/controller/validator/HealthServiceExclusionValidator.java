package ca.medavie.nspp.audit.ui.controller.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;

/**
 * Used for validating Health Service Exclusions
 */
public class HealthServiceExclusionValidator extends Validator<HealthServiceCriteria> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(HealthServiceCriteria t) throws ValidatorException {

		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		// Only validation needed is age restriction must be above 0
		if (t.getAgeRestriction() != null) {
			if (t.getAgeRestriction().compareTo(BigDecimal.ZERO) <= 0) {
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.MEDICARE.HS.EXCL.INVALID.AGE.RESTRICTION"));
			}
		}

		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}

}
