package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.SortCriteria;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.DenticareService;
import ca.medavie.nspp.audit.service.MedicareService;
import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.DenticareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.exception.DenticareServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.DenticareALRValidator;
import ca.medavie.nspp.audit.ui.controller.validator.DenticareAuditCriteriaValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for Denticare pages
 */
@ManagedBean(name = "denticareController")
@ViewScoped
public class DenticareControllerBean extends DropdownConstantsBean implements Serializable {

	/** JRE generated */
	private static final long serialVersionUID = 8648238457293493884L;
	/** Logger for class */
	private static final Logger logger = LoggerFactory.getLogger(DenticareControllerBean.class);

	/** Only allowed Subcategory for Denticare Audit Criterias */
	private static final String DE_SUBCATEGORY = "DE";

	/** EJB Service for Denticare & Medicare (Methods that do the same thing already exist here) */
	@EJB
	protected DenticareService denticareService;
	@EJB
	protected MedicareService medicareService;

	/** ******************************* */
	/** Audit Criteria screen variables */
	/** ******************************* */
	private AuditCriteriaSearchCriteria auditCriteriaSearchCriteria;
	/** Currently selected Audit Criteria */
	private AuditCriteria selectedAuditCriteria;
	private LazyDataModel<AuditCriteria> auditCriteriaSearchResults;
	private boolean displayAuditCriteriaSearchResults;
	private Integer totalAuditCriteriaRecords;
	private DenticareAuditCriteriaValidator auditCriteriaValidator;
	/**
	 * Controls display of certain fields that should be visible only when editing an existing Audit
	 */
	private boolean editMode;

	/** ************************************** */
	/** Audit Letter Response screen variables */
	/** ************************************** */
	private DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria;
	/** Holds the number of letters created if user provided Audit Run number for ALR search */
	private Long numberOfLettersCreated;
	private boolean displayNumberOfLettersCreated;
	/** The selected DenticareAuditLetterResponse **/
	private DenticareAuditLetterResponse selectedDCALR;
	private LazyDataModel<DenticareAuditLetterResponse> denticareAuditLetterResponses;
	private boolean displayAuditLetterResponsesSearchResults;
	private Integer totalAuditLetterResponses;
	private DenticareALRValidator denticareALRValidator;

	/**
	 * Controller constructor
	 */
	public DenticareControllerBean() {

		// Connect to EJB service
		if (this.denticareService == null) {
			try {
				this.denticareService = InitialContext.doLookup(prop.getProperty(DenticareService.class.getName()));
				logger.debug("denticareService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		if (this.medicareService == null) {
			try {
				this.medicareService = InitialContext.doLookup(prop.getProperty(MedicareService.class.getName()));
				logger.debug("medicareService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Instantiate variables
		denticareALRSearchCriteria = new DenticareAuditLetterResponseSearchCriteria();
		auditCriteriaSearchCriteria = new AuditCriteriaSearchCriteria();

		// Instantiate validators
		auditCriteriaValidator = new DenticareAuditCriteriaValidator(medicareService);
		denticareALRValidator = new DenticareALRValidator();

		// Init Search edit panel since it is displayed on load
		initiateSearchEditAuditCriteriaPanel();

		// Sync Audit Criteria search results lazy data model with bean
		auditCriteriaSearchResults = performAuditCriteriaSearchLoad();

		// Sync ALR search results lazy data model with bean
		denticareAuditLetterResponses = performALRSearchLoad();
	}

	/**
	 * 0 - Search/Edit Audit Criteria, 1 - Add New Audit Criteria, 2 - Responses
	 * 
	 * @param anEvent
	 */
	public void denticareTabChangeListener(ValueChangeEvent anEvent) {
		// Get the index of the requested tab
		Integer requestedTabIndex = (Integer) anEvent.getNewValue();

		// Prepare backing bean for the requested tab
		if (requestedTabIndex.equals(Integer.valueOf("0"))) {
			initiateSearchEditAuditCriteriaPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("1"))) {
			initiateNewAuditCriteriaPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("2"))) {
			initiateSearchEditDenticareALRPanel();
		}
	}

	/***********************************/
	/****** Audit Criteria methods *****/
	/***********************************/

	/**
	 * Navigates the user to the Search/Edit Audit Criteria panel
	 */
	private void initiateSearchEditAuditCriteriaPanel() {
		// Clear out any selected AuditCriteria
		selectedAuditCriteria = new AuditCriteria();
		// Reset search criterias
		auditCriteriaSearchCriteria.resetSearchCriteria();
		denticareALRSearchCriteria = new DenticareAuditLetterResponseSearchCriteria();
		displayAuditCriteriaSearchResults = false;

		// User can only interact with Dentist Practitioner types for Dental
		auditCriteriaSearchCriteria.setSubcategory(DE_SUBCATEGORY);
		editMode = false;
	}

	/**
	 * Navigates the user to the New Audit Criteria panel
	 */
	private void initiateNewAuditCriteriaPanel() {
		selectedAuditCriteria = new AuditCriteria();
		// User can only interact with Dentist Practitioner types for Dental
		selectedAuditCriteria.setProviderType("DE");
		editMode = false;
	}

	/**
	 * Takes the user to step 2 of Audit Criteria Search
	 * 
	 * Based on the dropdown selection the appropriate search form will be displayed
	 */
	public void displayAuditCriteriaSearchForm() {

		// Do nothing if they select the option select....
		if (auditCriteriaSearchCriteria.getAuditCriteriaType() != null) {
			if (auditCriteriaSearchCriteria.getAuditCriteriaType().equals(
					AuditCriteriaSearchCriteria.GENERAL_PRACT_AUDIT_CRIT)) {
				auditCriteriaSearchCriteria.setPractitionerSearch(true);
			} else {
				auditCriteriaSearchCriteria.setPractitionerSearch(false);
			}
			// Render the search form
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAuditCriteriaSearch()");
		}
	}

	/**
	 * Returns the user to step 1 of Audit Criteria Search
	 */
	public void returnToAuditCriteriaTypeSelection() {
		// Resets the search criteria
		auditCriteriaSearchCriteria.resetSearchCriteria();
		auditCriteriaSearchCriteria.setSubcategory(DE_SUBCATEGORY);
		displayAuditCriteriaSearchResults = false;
		// Render the Audit Criteria Type selection
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAuditCriteriaTypeSelection()");
	}

	/**
	 * Searches for matching Practitioner AuditCriteria records using the users search criteria
	 */
	public void executeAuditCriteriaSearch() {
		try {
			// Perform record count on all search results
			totalAuditCriteriaRecords = denticareService.getSearchedAuditCriteriaCount(auditCriteriaSearchCriteria);
			auditCriteriaSearchResults.setRowCount(totalAuditCriteriaRecords);

			// Display search results. Lazy data model automatically refreshes
			displayAuditCriteriaSearchResults = true;
			// Clear data table state
			resetDataTableState("searchEditAuditCriteriaView:searchAuditCriteriaPanel:auditCriteriaSearchForm:auditCriteriaSearchResultsTable");

		} catch (DenticareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Loads the matching Audit Criteria records based on the search criteria
	 * 
	 * @return AuditCriteria records matching the user search criteria
	 */
	private LazyDataModel<AuditCriteria> performAuditCriteriaSearchLoad() {
		auditCriteriaSearchResults = new LazyDataModel<AuditCriteria>() {

			/** IDE generated */
			private static final long serialVersionUID = -767345513608096664L;

			@Override
			public List<AuditCriteria> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<AuditCriteria> results = new ArrayList<AuditCriteria>();

				try {
					if (displayAuditCriteriaSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								auditCriteriaSearchCriteria.addSortArgument(AuditCriteria.class, sc.getPropertyName(),
										sc.isAscending());
							}
						}

						// Load record matching search results
						results = denticareService.getSearchPractitionerAuditCriterias(auditCriteriaSearchCriteria,
								first, pageSize);
					}
				} catch (DenticareServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return auditCriteriaSearchResults;
	}

	/**
	 * Assists in reseting the Audit search form. Typically we would use the super clear method but we must retain
	 * certain values through the reset due to page flow.
	 */
	public void executeClearAuditSearch() {
		clearForm();

		// Grab the state we need to carry over
		String selectedType = auditCriteriaSearchCriteria.getAuditCriteriaType();
		boolean practitionerSearch = auditCriteriaSearchCriteria.isPractitionerSearch();
		String selectedSubcategory = auditCriteriaSearchCriteria.getSubcategory();

		auditCriteriaSearchCriteria.resetSearchCriteria();
		displayAuditCriteriaSearchResults = false;
		// Clear data table state
		resetDataTableState("searchEditAuditCriteriaView:searchAuditCriteriaPanel:auditCriteriaSearchForm:auditCriteriaSearchResultsTable");

		// Restore state
		auditCriteriaSearchCriteria.setAuditCriteriaType(selectedType);
		auditCriteriaSearchCriteria.setPractitionerSearch(practitionerSearch);
		auditCriteriaSearchCriteria.setSubcategory(selectedSubcategory);
	}

	public void editSelectedPractitionerAuditCriteria(AuditCriteria anAuditCriteria) {
		// Grab all the details of the selected record and display the edit page to the user
		try {
			selectedAuditCriteria = denticareService.getPractitionerAuditCriteriaById(anAuditCriteria
					.getAuditCriteriaId());

			// Set practitioner name here. Saves us from an additional SQL query
			selectedAuditCriteria.setProviderName(anAuditCriteria.getProviderName());

			// Upon successful load. Display edit screen to the user
			editMode = true;
			closePopupWindowJS();
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayEditMedAuditCriteria()");

		} catch (DenticareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Loads the data for the selected Default Audit and displays the edit screen.
	 */
	public void executeEditDefaultAuditCriteria() {

		if (auditCriteriaSearchCriteria.getAuditType() != null) {
			try {
				// Attempt to load the Default AuditCriteria. Creates new record if nothing is found
				selectedAuditCriteria = denticareService.getDefaultAuditCriteriaByType(auditCriteriaSearchCriteria
						.getAuditType());

				// Toggle edit flags to correct state
				editMode = true;

				// Display edit screen
				JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayEditMedAuditCriteria()");

			} catch (DenticareServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	/**
	 * Triggered when the user selects an Audit Type for Add Audit Criteria
	 */
	public void displayAddNewAuditCriteriaForm() {
		try {
			// Validate the users inputs
			auditCriteriaValidator.validateNewCriteriaParameters(selectedAuditCriteria);

			// Take user to edit screen if validation successful
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAddNewMedAuditCriteria()");

		} catch (ValidatorException ve) {
			Validator.displayValidationErrors(ve);
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Saves changes made to the selected Audit Criteria
	 */
	public void executeAuditCriteriaSave() {
		try {
			// Validate the user provided values. This includes Practitioner existence and duplicate audit check
			auditCriteriaValidator.validate(selectedAuditCriteria);

			// Perform quick second step validation if Default Audit
			if (selectedAuditCriteria.getProviderNumber() == null) {
				List<ValidatorMessage> warnings = auditCriteriaValidator
						.checkDefaultAuditPaymentDates(selectedAuditCriteria);

				// Display warnings if any
				if (!warnings.isEmpty()) {
					for (ValidatorMessage vm : warnings) {
						Validator.displayValidationError(vm);
					}
				}
			}

			// Apply edit meta data
			selectedAuditCriteria.setModifiedBy(FacesUtils.getUserId());
			selectedAuditCriteria.setLastModified(new Date());
			// Perform save
			denticareService.saveDenticareAuditCriteria(selectedAuditCriteria);

			// Display success message
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_INFO,
					"INFO.VERIFICATION.LETTERS.MED.AUD.CRITERIA.SAVE.SUCCESSFUL"));

		} catch (DenticareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (ValidatorException ve) {
			Validator.displayValidationErrors(ve);
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Returns the user to Audit Criteria Search screen
	 */
	public void executeReturnToAuditCriteriaSearch() {
		// Reset boolean flags
		editMode = false;
		selectedAuditCriteria = new AuditCriteria();

		// Ensure search results are displayed if Practitioner search was performed
		if (auditCriteriaSearchCriteria.isPractitionerSearch()) {
			displayAuditCriteriaSearchResults = true;
		}

		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToEditMedAuditCriteriaSearch()");
	}

	/**
	 * Returns the user to Type selection for Add new Med Audit Criteria
	 */
	public void returnToAddNewAuditTypeSelection() {
		selectedAuditCriteria = new AuditCriteria();
		selectedAuditCriteria.setProviderType(DE_SUBCATEGORY);
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToAddNewMedAuditCriteriaTypeSelection()");
	}

	/*****************************/
	/****** Response methods *****/
	/*****************************/

	/**
	 * Sets up the Search/Edit panel
	 */
	private void initiateSearchEditDenticareALRPanel() {
		// Clear out search forms
		denticareALRSearchCriteria.resetSearchCriteria();
		displayAuditLetterResponsesSearchResults = false;
	}

	/**
	 * Search using the provided search criteria
	 */
	public void executeDenticareALRSearch() {
		logger.debug("executeDenticareALRSearch() : Begin");

		// Execute search
		try {
			// Perform search record count
			totalAuditLetterResponses = denticareService.getSearchedDenticareALRCount(denticareALRSearchCriteria);
			denticareAuditLetterResponses.setRowCount(totalAuditLetterResponses);

			// Display search results
			displayAuditLetterResponsesSearchResults = true;
			// Reset data table
			resetDataTableState("editAuditLetterRespView:searchEditDenticareALRPanel:denticareALRSearchForm:denticareALRSearchResultsTable");

			// Reset the count display
			displayNumberOfLettersCreated = false;

			// Load number of letters if needed
			if (denticareALRSearchCriteria.getAuditRunNumber() != null
					&& (denticareALRSearchCriteria.getAuditType() != null && !denticareALRSearchCriteria.getAuditType()
							.isEmpty())) {

				numberOfLettersCreated = medicareService.getNumberOfLettersCreated(
						denticareALRSearchCriteria.getAuditRunNumber(), denticareALRSearchCriteria.getAuditType());

				// Set the count display to true
				displayNumberOfLettersCreated = true;
			}
		}
		// catch service exceptions
		catch (DenticareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		logger.debug("executeDenticareALRSearch() : End");
	}

	private LazyDataModel<DenticareAuditLetterResponse> performALRSearchLoad() {

		denticareAuditLetterResponses = new LazyDataModel<DenticareAuditLetterResponse>() {

			/** IDE generated */
			private static final long serialVersionUID = 1L;

			@Override
			public List<DenticareAuditLetterResponse> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<DenticareAuditLetterResponse> results = new ArrayList<DenticareAuditLetterResponse>();

				try {
					if (displayAuditLetterResponsesSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								denticareALRSearchCriteria.addSortArgument(DenticareAuditLetterResponse.class,
										sc.getPropertyName(), sc.isAscending());
							}
						}

						// Load record matching search results
						results = denticareService.getSearchedDenticareALR(denticareALRSearchCriteria, first, pageSize);
					}
				} catch (DenticareServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return denticareAuditLetterResponses;
	}

	/**
	 * Resets the ALR search form
	 */
	public void executeClearDenticareALRSearch() {
		clearForm();
		denticareALRSearchCriteria.resetSearchCriteria();
		displayAuditLetterResponsesSearchResults = false;
		// Reset data table
		resetDataTableState("editAuditLetterRespView:searchEditDenticareALRPanel:denticareALRSearchForm:denticareALRSearchResultsTable");
	}

	/**
	 * Action to take after a Denticare audit letter responds record selected
	 * 
	 * @param DenticareAuditLetterResponse
	 *            - a selected DenticareAuditLetterResponds
	 */
	public void executeEditSelectedDCALR(DenticareAuditLetterResponse aSelectedBO) {

		selectedDCALR = aSelectedBO;
		// Display the edit screen
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayEditDenticareALRResultDetails()");
	}

	/**
	 * go back to search page
	 */
	public void executeReturnToALRSearch() {
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToDentiALRSearch()");
	}

	/**
	 * execute save changes to denti care audit letter responds
	 */
	public void executeSaveDenticareALR() {
		logger.debug("executeSaveDenticareALR() : Begin");

		try {

			// Validate the Denticare ALR
			denticareALRValidator.validate(selectedDCALR);

			// update the last modified date and person
			selectedDCALR.setLastModified(new Date());
			selectedDCALR.setModifiedBy(FacesUtils.getUserId());

			denticareService.saveDenticareALR(selectedDCALR);

			// Display save success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.AUDIT.RESULT.VALUE.SAVE.SUCCESSFUL");

		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);

		} catch (DenticareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());

		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		logger.debug("executeDenticareALRSearch() : End");
	}

	public DenticareAuditLetterResponse getSelectedDCALR() {
		return selectedDCALR;
	}

	public void setSelectedDCALR(DenticareAuditLetterResponse selectedDCALR) {
		this.selectedDCALR = selectedDCALR;
	}

	public DenticareAuditLetterResponseSearchCriteria getDenticareALRSearchCriteria() {
		return denticareALRSearchCriteria;
	}

	public void setDenticareALRSearchCriteria(DenticareAuditLetterResponseSearchCriteria denticareALRSearchCriteria) {
		this.denticareALRSearchCriteria = denticareALRSearchCriteria;
	}

	/**
	 * @return the selectedAuditCriteria
	 */
	public AuditCriteria getSelectedAuditCriteria() {
		return selectedAuditCriteria;
	}

	/**
	 * @param selectedAuditCriteria
	 *            the selectedAuditCriteria to set
	 */
	public void setSelectedAuditCriteria(AuditCriteria selectedAuditCriteria) {
		this.selectedAuditCriteria = selectedAuditCriteria;
	}

	/**
	 * @return the editMode
	 */
	public boolean isEditMode() {
		return editMode;
	}

	/**
	 * @param editMode
	 *            the editMode to set
	 */
	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	/**
	 * @return the auditCriteriaSearchCriteria
	 */
	public AuditCriteriaSearchCriteria getAuditCriteriaSearchCriteria() {
		return auditCriteriaSearchCriteria;
	}

	/**
	 * @param auditCriteriaSearchCriteria
	 *            the auditCriteriaSearchCriteria to set
	 */
	public void setAuditCriteriaSearchCriteria(AuditCriteriaSearchCriteria auditCriteriaSearchCriteria) {
		this.auditCriteriaSearchCriteria = auditCriteriaSearchCriteria;
	}

	/**
	 * @return the numberOfLettersCreated
	 */
	public Long getNumberOfLettersCreated() {
		return numberOfLettersCreated;
	}

	/**
	 * @param numberOfLettersCreated
	 *            the numberOfLettersCreated to set
	 */
	public void setNumberOfLettersCreated(Long numberOfLettersCreated) {
		this.numberOfLettersCreated = numberOfLettersCreated;
	}

	/**
	 * @return the displayNumberOfLettersCreated
	 */
	public boolean isDisplayNumberOfLettersCreated() {
		return displayNumberOfLettersCreated;
	}

	/**
	 * @param displayNumberOfLettersCreated
	 *            the displayNumberOfLettersCreated to set
	 */
	public void setDisplayNumberOfLettersCreated(boolean displayNumberOfLettersCreated) {
		this.displayNumberOfLettersCreated = displayNumberOfLettersCreated;
	}

	/**
	 * @return the auditCriteriaSearchResults
	 */
	public LazyDataModel<AuditCriteria> getAuditCriteriaSearchResults() {
		return auditCriteriaSearchResults;
	}

	/**
	 * @param auditCriteriaSearchResults
	 *            the auditCriteriaSearchResults to set
	 */
	public void setAuditCriteriaSearchResults(LazyDataModel<AuditCriteria> auditCriteriaSearchResults) {
		this.auditCriteriaSearchResults = auditCriteriaSearchResults;
	}

	/**
	 * @return the displayAuditCriteriaSearchResults
	 */
	public boolean isDisplayAuditCriteriaSearchResults() {
		return displayAuditCriteriaSearchResults;
	}

	/**
	 * @param displayAuditCriteriaSearchResults
	 *            the displayAuditCriteriaSearchResults to set
	 */
	public void setDisplayAuditCriteriaSearchResults(boolean displayAuditCriteriaSearchResults) {
		this.displayAuditCriteriaSearchResults = displayAuditCriteriaSearchResults;
	}

	/**
	 * @return the totalAuditCriteriaRecords
	 */
	public Integer getTotalAuditCriteriaRecords() {
		return totalAuditCriteriaRecords;
	}

	/**
	 * @param totalAuditCriteriaRecords
	 *            the totalAuditCriteriaRecords to set
	 */
	public void setTotalAuditCriteriaRecords(Integer totalAuditCriteriaRecords) {
		this.totalAuditCriteriaRecords = totalAuditCriteriaRecords;
	}

	/**
	 * @return the denticareAuditLetterResponses
	 */
	public LazyDataModel<DenticareAuditLetterResponse> getDenticareAuditLetterResponses() {
		return denticareAuditLetterResponses;
	}

	/**
	 * @param denticareAuditLetterResponses
	 *            the denticareAuditLetterResponses to set
	 */
	public void setDenticareAuditLetterResponses(
			LazyDataModel<DenticareAuditLetterResponse> denticareAuditLetterResponses) {
		this.denticareAuditLetterResponses = denticareAuditLetterResponses;
	}

	/**
	 * @return the displayAuditLetterResponsesSearchResults
	 */
	public boolean isDisplayAuditLetterResponsesSearchResults() {
		return displayAuditLetterResponsesSearchResults;
	}

	/**
	 * @param displayAuditLetterResponsesSearchResults
	 *            the displayAuditLetterResponsesSearchResults to set
	 */
	public void setDisplayAuditLetterResponsesSearchResults(boolean displayAuditLetterResponsesSearchResults) {
		this.displayAuditLetterResponsesSearchResults = displayAuditLetterResponsesSearchResults;
	}

	/**
	 * @return the totalAuditLetterResponses
	 */
	public Integer getTotalAuditLetterResponses() {
		return totalAuditLetterResponses;
	}

	/**
	 * @param totalAuditLetterResponses
	 *            the totalAuditLetterResponses to set
	 */
	public void setTotalAuditLetterResponses(Integer totalAuditLetterResponses) {
		this.totalAuditLetterResponses = totalAuditLetterResponses;
	}
}
