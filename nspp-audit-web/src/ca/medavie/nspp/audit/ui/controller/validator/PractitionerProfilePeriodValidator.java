package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.PractitionerProfilePeriod;

/**
 * Used for validating PractitionerProfilePeriods
 */
public class PractitionerProfilePeriodValidator extends Validator<PractitionerProfilePeriod> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(PractitionerProfilePeriod practitionerProfilePeriod) throws ValidatorException {
		List<ValidatorMessage> errorMsgs = new ArrayList<ValidatorMessage>();

		// Check that all the required values were provided
		if (practitionerProfilePeriod.getPeriodStart() == null) {
			errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.PERIOD.START.DATE.REQUIRED"));
		}
		if (practitionerProfilePeriod.getPeriodEnd() == null) {
			errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.PERIOD.END.DATE.REQUIRED"));
		}
		if (practitionerProfilePeriod.getYearEnd() == null) {
			errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.YEAR.END.DATE.REQUIRED"));
		}

		// Check date range only if the values were provided..
		if (practitionerProfilePeriod.getPeriodStart() != null && practitionerProfilePeriod.getPeriodEnd() != null
				&& practitionerProfilePeriod.getYearEnd() != null) {

			// Check that Period Start date is before Period End Date.
			if (practitionerProfilePeriod.getPeriodStart().after(practitionerProfilePeriod.getPeriodEnd())
					|| practitionerProfilePeriod.getPeriodEnd().before(practitionerProfilePeriod.getPeriodStart())) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.PERIOD.DATE.RANGE.INVALID"));
			}

			// Check that the Period date range is before the Year End Date
			if (practitionerProfilePeriod.getPeriodEnd().after(practitionerProfilePeriod.getYearEnd())
					|| practitionerProfilePeriod.getYearEnd().before(practitionerProfilePeriod.getPeriodEnd())) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.PERIOD.YEAR.END.INVALID"));
			}
		}

		// Throw Validator exception if errors were found
		if (!errorMsgs.isEmpty()) {
			throw new ValidatorException(errorMsgs);
		}
	}

	/**
	 * Used to determine if there is a date range overlap when adding a new Profile Period. If overlap is detected
	 * return true.
	 * 
	 * @param aNewProfilePeriod
	 * @param aListOfProfilePeriods
	 * @return true if date overlap is detected
	 */
	public boolean validatePeriodDateRange(PractitionerProfilePeriod aNewProfilePeriod,
			Map<Long, PractitionerProfilePeriod> aListOfProfilePeriods) {

		// Grab the Start & End dates of the new ProfilePeriod
		Date newPeriodStartDate = aNewProfilePeriod.getPeriodStart();
		Date newPeriodEndDate = aNewProfilePeriod.getPeriodEnd();

		// Loop over all existing periods and check for overlap. Once an overlap is detected immediately return
		for (PractitionerProfilePeriod period : aListOfProfilePeriods.values()) {

			// Grab the dates from the existing Period
			Date existingPeriodStartDate = period.getPeriodStart();
			Date existingPeriodEndDate = period.getPeriodEnd();

			// Check for overlap
			if (((!existingPeriodStartDate.before(newPeriodStartDate) && !existingPeriodStartDate
					.after(newPeriodEndDate)) || (!existingPeriodEndDate.before(newPeriodStartDate) && !existingPeriodEndDate
					.after(newPeriodEndDate)))
					&& (!existingPeriodStartDate.equals(newPeriodEndDate) && !existingPeriodEndDate
							.equals(newPeriodStartDate))) {

				// New Profile Period overlaps with an existing Period...
				return true;
			}
		}

		return false;
	}
}
