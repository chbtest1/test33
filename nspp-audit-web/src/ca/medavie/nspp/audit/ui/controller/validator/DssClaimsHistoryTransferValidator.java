package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferRequest;
import ca.medavie.nspp.audit.service.data.HealthCard;
import ca.medavie.nspp.audit.service.exception.DssClaimsHistoryTransferException;

public class DssClaimsHistoryTransferValidator extends Validator<DssClaimsHistoryTransferRequest> {

	@EJB
	protected DssClaimsHistoryTransferService dssClaimsHistoryTransferService;

	/**
	 * Since we need the EJB service for parts of the validation we pass it on construction of Validator class
	 * 
	 * @param aDssClaimsHistoryTransferService
	 */
	public DssClaimsHistoryTransferValidator(DssClaimsHistoryTransferService aDssClaimsHistoryTransferService) {
		this.dssClaimsHistoryTransferService = aDssClaimsHistoryTransferService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(DssClaimsHistoryTransferRequest t) throws ValidatorException {
		// Holds all error message created during the validation process
		List<ValidatorMessage> validationMessage = new ArrayList<ValidatorMessage>();

		/**
		 * Rules:
		 * 
		 * 1) Health Card Number must be provided
		 * 
		 * 2) Health Card Number must exist in the system
		 * 
		 * 3) If Service Date range is provided verify the values are valid.
		 */

		// Validate date range (Only perform this validation if both dates are provided)
		if (t.getServiceFromDate() != null && t.getServiceToDate() != null) {
			// Compare dates
			if ((t.getServiceFromDate().after(t.getServiceToDate()))
					|| (t.getServiceToDate().before(t.getServiceFromDate()))) {
				// Invalid range.. Error
				ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.DSS.SEARCH.DATE.RANGE.INVALID");
				validationMessage.add(message);
			}
		}

		// Validate HCN
		validateHcn(t.getSourceHealthCardNumber());

		// Display messages if there are any
		if (!validationMessage.isEmpty()) {
			throw new ValidatorException(validationMessage);
		}
	}

	/**
	 * Verifies that the specified Health Card number exists in the system
	 * 
	 * @param aHealthCardNumber
	 * @throws ValidatorException
	 */
	public void validateHcn(String aHealthCardNumber) throws ValidatorException {
		// Holds all error message created during the validation process
		List<ValidatorMessage> validationMessage = new ArrayList<ValidatorMessage>();

		// Verify the Health Card Number exists
		try {
			if (aHealthCardNumber == null || aHealthCardNumber.trim().isEmpty()) {
				ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DSS.HCN.REQUIRED");
				validationMessage.add(message);
			} else {
				HealthCard hc = dssClaimsHistoryTransferService.getSearchedHealthCard(aHealthCardNumber);

				if (hc == null) {
					// Unable to find the HCN
					ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.DSS.HCN.INVALID");
					validationMessage.add(message);
				}
			}

		} catch (DssClaimsHistoryTransferException e) {
			ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DSS.CLAIMS.TRANSFER");
			validationMessage.add(message);
			e.printStackTrace();
		}

		// Display messages if there are any
		if (!validationMessage.isEmpty()) {
			throw new ValidatorException(validationMessage);
		}
	}
}
