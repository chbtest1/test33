package ca.medavie.nspp.audit.ui.controller.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.DssMsiConstant;

/**
 * Used for validating PractitionerProfileCriterias
 */
public class PractitionerProfileCriteriaValidator extends Validator<DssMsiConstant> {

	/**
	 * Validates the provided PractitionerProfileCriteria object
	 * 
	 * @param practitionerProfileCriteria
	 * @throws ValidatorException
	 */
	@Override
	public void validate(DssMsiConstant aConstant) throws ValidatorException {
		List<ValidatorMessage> validationErrors = new ArrayList<ValidatorMessage>();

		if (aConstant.getConstantCode().equals("MPERIOD") && aConstant.getConstantNumeric().equals(null)) {
			validationErrors.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.PERIOD.ID.REQUIRED"));
		}

		else if (aConstant.getConstantCode().equals("PAYMT_DT") && aConstant.getConstantDate() == null) {
			validationErrors.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.LAST.PAYMENT.DATE.REQUIRED"));
		}

		else if (aConstant.getConstantCode().equals("PROV_CNT")
				&& (aConstant.getConstantNumeric().equals(null) || aConstant.getConstantNumeric().equals(0))) {
			validationErrors
					.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.PRACTITIONER.COUNT.REQUIRED"));
		}

		else if (aConstant.getConstantCode().equals("PROV_CNT")
				&& aConstant.getConstantNumeric().compareTo(Integer.valueOf(999999999)) == 1) {
			// Validate the entry length for Practitioner Profile Count
			validationErrors.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.PRACT.COUNT.INVALID.MAX"));
		}

		else if (aConstant.getConstantCode().equals("UNITDOLR")
				&& (aConstant.getConstantCurrency().equals(null) || aConstant.getConstantCurrency().equals(
						BigDecimal.ZERO))) {
			validationErrors.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.UNIT.DOLLAR.REQUIRED"));
		}

		else if (aConstant.getConstantCode().equals("UNITDOLR")
				&& aConstant.getConstantCurrency().compareTo(BigDecimal.valueOf(9999999999999.99)) == 1) {
			// Validate the entry length for Unit Dollar (15,2)
			validationErrors.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.PROFILE.CRITERIA.UNIT.DOLLAR.INVALID.MAX"));
		}

		else if (aConstant.getConstantCode().equals("YRENDDT") && aConstant.getConstantDate() == null) {
			validationErrors.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.YEAR.END.DATE.REQUIRED"));
		}

		if (!validationErrors.isEmpty()) {
			throw new ValidatorException(validationErrors);
		}
	}
}
