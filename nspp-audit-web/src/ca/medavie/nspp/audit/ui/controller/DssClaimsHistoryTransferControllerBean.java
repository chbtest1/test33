package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.RowStateMap;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.DssClaimsHistoryTransferService;
import ca.medavie.nspp.audit.service.data.DenticareClaim;
import ca.medavie.nspp.audit.service.data.DssClaimsHistoryTransferRequest;
import ca.medavie.nspp.audit.service.data.HealthCard;
import ca.medavie.nspp.audit.service.data.MedicareClaim;
import ca.medavie.nspp.audit.service.data.PharmacareClaim;
import ca.medavie.nspp.audit.service.exception.DssClaimsHistoryTransferException;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.DssClaimsHistoryTransferValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for DSS History Claims Transfer pages
 */
@ManagedBean(name = "dssClaimsHistoryTransferController")
@ViewScoped
public class DssClaimsHistoryTransferControllerBean extends DropdownConstantsBean implements Serializable {

	/** IDE generated */
	private static final long serialVersionUID = -3132214744934458680L;
	/** Logger for class */
	private static final Logger logger = LoggerFactory.getLogger(DssClaimsHistoryTransferControllerBean.class);

	@EJB
	protected DssClaimsHistoryTransferService dssClaimsHistoryTransferService;

	/**
	 * Used for holding state during the transfer to enable download of summary
	 */
	@ManagedProperty(value = "#{fileDownloadController}")
	private FileDownloadControllerBean fileDownloadControllerBean;

	/**
	 * Booleans that control the display of search and summary UIs. Due to multiple forums on page the display state
	 * needs to be stored in the back end instead of CSS
	 */
	private boolean displayHcnSearch;
	private boolean displayHcnSummary;

	/** Booleans that control the transfer wizard panels */
	private boolean displayTransferWizard;
	private boolean displayMedicareSeClaims;
	private boolean displayMedicareMainClaims;
	private boolean displayPharmSeClaims;
	private boolean displayPharmMainClaims;
	private boolean displayDentalClaims;
	private boolean displayClaimsTransferSummary;
	private boolean displayExportPanel;

	/** State maps for each claim table */
	private RowStateMap medicareSeClaimsStateMap;
	private RowStateMap medicareMainClaimsStateMap;
	private RowStateMap pharmSeClaimsStateMap;
	private RowStateMap pharmMainClaimsStateMap;
	private RowStateMap dentalClaimsStateMap;

	/** Holds the currently selected HealthCard record */
	private HealthCard selectedHealthCard;

	/** Validator */
	private DssClaimsHistoryTransferValidator dssClaimsHistoryTransferValidator;

	/**
	 * Bean Constructor
	 */
	public DssClaimsHistoryTransferControllerBean() {

		// Connect to EJB service
		if (dssClaimsHistoryTransferService == null) {
			try {
				this.dssClaimsHistoryTransferService = InitialContext.doLookup(prop
						.getProperty(DssClaimsHistoryTransferService.class.getName()));

				logger.debug("dssClaimsHistoryTransferService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Init validator
		if (dssClaimsHistoryTransferValidator == null) {
			dssClaimsHistoryTransferValidator = new DssClaimsHistoryTransferValidator(dssClaimsHistoryTransferService);
		}

		// Init primary panel
		initializeDssClaimsHistoryTransferPanel();
	}

	/**
	 * Sets up the DSS Claims History Transfer panel
	 */
	public void initializeDssClaimsHistoryTransferPanel() {
		selectedHealthCard = new HealthCard();

		// Ensure that search is only displayed
		displayHcnSearch = true;
		displayHcnSummary = false;

		// Set transfer panels all to false for display
		displayTransferWizard = false;
		displayMedicareSeClaims = false;
		displayMedicareMainClaims = false;
		displayPharmSeClaims = false;
		displayPharmMainClaims = false;
		displayDentalClaims = false;
		displayClaimsTransferSummary = false;
		displayExportPanel = false;
	}

	/**
	 * Searches for matching (Source) Health Card Number records
	 */
	public void executeSourceHcnSearch() {
		try {

			// Left pad the source health card number with 0s if required
			String paddedHcn = applyHcnLeftPadding(fileDownloadControllerBean.transferRequest
					.getSourceHealthCardNumber());
			fileDownloadControllerBean.transferRequest.setSourceHealthCardNumber(paddedHcn);

			// Validate HCN and date range if provided
			dssClaimsHistoryTransferValidator.validate(fileDownloadControllerBean.transferRequest);

			selectedHealthCard = dssClaimsHistoryTransferService
					.getSearchedHealthCard(fileDownloadControllerBean.transferRequest.getSourceHealthCardNumber());

			// Update display state. Allow JS to provide a smooth transition
			displayHcnSearch = false;
			displayHcnSummary = true;

		} catch (ValidatorException ve) {
			Validator.displayValidationErrors(ve);
		} catch (DssClaimsHistoryTransferException te) {
			Validator.displayServiceError(te);
			te.printStackTrace();
			logger.error(te.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Returns the user to the Health Card number search screen alongside hiding the summary page
	 */
	public void executeReturnToHcnSearch() {
		// Reset the Transfer request
		fileDownloadControllerBean.setTransferRequest(new DssClaimsHistoryTransferRequest());

		// Clear state maps
		resetClaimsSelectionData();

		// Update display state
		displayHcnSearch = true;
		displayHcnSummary = false;

		// Hide all panels in the event a transfer was in process
		displayTransferWizard = false;
		displayMedicareSeClaims = false;
		displayMedicareMainClaims = false;
		displayPharmSeClaims = false;
		displayPharmMainClaims = false;
		displayDentalClaims = false;
		displayClaimsTransferSummary = false;
		displayExportPanel = false;

		// Wipe out the current HCN selection
		selectedHealthCard = null;
	}

	/**
	 * Resets the search criteria provided by the user on the Source HCN search form
	 */
	public void executeClearSourceHcnSearchForm() {
		fileDownloadControllerBean.setTransferRequest(new DssClaimsHistoryTransferRequest());
	}

	/**
	 * Cancels the Claims Transfer process
	 */
	public void executeCancelClaimsTransfer() {
		// Hide all panels as this cancel can be called at any step
		displayTransferWizard = false;
		displayMedicareSeClaims = false;
		displayMedicareMainClaims = false;
		displayPharmSeClaims = false;
		displayPharmMainClaims = false;
		displayDentalClaims = false;
		displayClaimsTransferSummary = false;

		// Maximize Individual Summary panel if needed
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "maximizeHcnSummaryPanel()");

		// Clear out any claims selections made
		if (medicareSeClaimsStateMap != null) {
			medicareSeClaimsStateMap.clear();
		}
		if (medicareMainClaimsStateMap != null) {
			medicareMainClaimsStateMap.clear();
		}
		if (pharmSeClaimsStateMap != null) {
			pharmSeClaimsStateMap.clear();
		}
		if (pharmMainClaimsStateMap != null) {
			pharmMainClaimsStateMap.clear();
		}
		if (dentalClaimsStateMap != null) {
			dentalClaimsStateMap.clear();
		}
	}

	/**
	 * Displays Step 1 of of the Claims Transfer process. Step: Medicare SE Claims
	 */
	public void executeClaimsTransferStep1() {
		try {
			// Load Medicare SE claims if needed
			if (selectedHealthCard.getMedicareClaims() == null || selectedHealthCard.getMedicareClaims().isEmpty()) {

				selectedHealthCard.setMedicareClaims(dssClaimsHistoryTransferService
						.getMedicareSeClaims(fileDownloadControllerBean.transferRequest));
			}

			// Reset all claims datatables to default state
			resetDataTableState("dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:medicareClaimsForm:medicareClaimDataTable");
			resetDataTableState("dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:medicareMainframeClaimsForm:medicareMainframeClaimDataTable");
			resetDataTableState("dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:pharmClaimsForm:pharmacareClaimDataTable");
			resetDataTableState("dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:pharmMainframeClaimsForm:pharmacareMainframeClaimDataTable");
			resetDataTableState("dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:denticareClaimsForm:dentalClaimDataTable");

			// Display Medicare SE Claims panel
			displayTransferWizard = true;
			displayMedicareSeClaims = true;

			// Ensure all other claim tables are hidden
			displayMedicareMainClaims = false;
			displayPharmSeClaims = false;
			displayPharmMainClaims = false;
			displayDentalClaims = false;
			displayClaimsTransferSummary = false;

			// Minimize Individual Summary panel if needed
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "minimizeHcnSummaryPanel()");

		} catch (DssClaimsHistoryTransferException te) {
			Validator.displayServiceError(te);
			te.printStackTrace();
			logger.error(te.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Displays Step 2 of of the Claims Transfer process. Step: Medicare Mainframe Claims
	 */
	public void executeClaimsTransferStep2() {
		try {
			// Load Medicare Mainframe claims if needed
			if (selectedHealthCard.getMedicareMainframeClaims() == null
					|| selectedHealthCard.getMedicareMainframeClaims().isEmpty()) {

				selectedHealthCard.setMedicareMainframeClaims(dssClaimsHistoryTransferService
						.getMedicareMainframeClaims(fileDownloadControllerBean.transferRequest));
			}

			// Display Medicare Mainframe Claims panel
			displayMedicareSeClaims = false;
			displayMedicareMainClaims = true;
			displayPharmSeClaims = false;

		} catch (DssClaimsHistoryTransferException te) {
			Validator.displayServiceError(te);
			te.printStackTrace();
			logger.error(te.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Displays Step 3 of of the Claims Transfer process. Step: Pharmacare SE Claims
	 */
	public void executeClaimsTransferStep3() {
		try {
			// Load Pharmacare SE claims if needed
			if (selectedHealthCard.getPharmacareClaims() == null || selectedHealthCard.getPharmacareClaims().isEmpty()) {

				selectedHealthCard.setPharmacareClaims(dssClaimsHistoryTransferService
						.getPharmacareSeClaims(fileDownloadControllerBean.transferRequest));
			}

			// Display Pharmacare SE Claims panel
			displayMedicareMainClaims = false;
			displayPharmSeClaims = true;
			displayPharmMainClaims = false;

		} catch (DssClaimsHistoryTransferException te) {
			Validator.displayServiceError(te);
			te.printStackTrace();
			logger.error(te.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Displays Step 4 of of the Claims Transfer process. Step: Pharmacare Mainframe Claims
	 */
	public void executeClaimsTransferStep4() {
		try {
			// Load Pharmacare Mainframe claims if needed
			if (selectedHealthCard.getPharmacareMainframeClaims() == null
					|| selectedHealthCard.getPharmacareMainframeClaims().isEmpty()) {

				selectedHealthCard.setPharmacareMainframeClaims(dssClaimsHistoryTransferService
						.getPharmacareMainframeClaims(fileDownloadControllerBean.transferRequest));
			}

			// Display Pharmacare Mainframe Claims panel
			displayPharmSeClaims = false;
			displayPharmMainClaims = true;
			displayDentalClaims = false;

		} catch (DssClaimsHistoryTransferException te) {
			Validator.displayServiceError(te);
			te.printStackTrace();
			logger.error(te.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Displays Step 5 of of the Claims Transfer process. Step: Dental Claims
	 */
	public void executeClaimsTransferStep5() {
		try {
			// Load Dental claims if needed
			if (selectedHealthCard.getDenticareClaims() == null || selectedHealthCard.getDenticareClaims().isEmpty()) {

				selectedHealthCard.setDenticareClaims(dssClaimsHistoryTransferService
						.getDentalClaims(fileDownloadControllerBean.transferRequest));
			}

			// Display Dental Claims panel
			displayPharmMainClaims = false;
			displayDentalClaims = true;
			displayClaimsTransferSummary = false;

		} catch (DssClaimsHistoryTransferException te) {
			Validator.displayServiceError(te);
			te.printStackTrace();
			logger.error(te.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Displays Step 6 of of the Claims Transfer process. Step: Transfer summary and HCN prompt
	 */
	public void executeClaimsTransferStep6() {
		// Display Claims transfer summary panel
		displayDentalClaims = false;
		displayClaimsTransferSummary = true;
	}

	/**
	 * Begins the Claims Transfer process to the specified Health Card Number.
	 */
	@SuppressWarnings("unchecked")
	public void executeClaimsTransfer() {

		try {

			// Left pad target health card number with 0s if required
			String paddedHcn = applyHcnLeftPadding(fileDownloadControllerBean.transferRequest
					.getTargetHealthCardNumber());
			fileDownloadControllerBean.transferRequest.setTargetHealthCardNumber(paddedHcn);

			// Verify that the target HCN exists
			dssClaimsHistoryTransferValidator.validateHcn(fileDownloadControllerBean.transferRequest
					.getTargetHealthCardNumber());

			// Extract any of the selected claims for each type
			fileDownloadControllerBean.transferRequest.setMedicareClaims(medicareSeClaimsStateMap.getSelected());
			fileDownloadControllerBean.transferRequest.setMedicareMainframeClaims(medicareMainClaimsStateMap
					.getSelected());
			fileDownloadControllerBean.transferRequest.setPharmacareClaims(pharmSeClaimsStateMap.getSelected());
			fileDownloadControllerBean.transferRequest.setPharmacareMainframeClaims(pharmMainClaimsStateMap
					.getSelected());
			fileDownloadControllerBean.transferRequest.setDentalClaims(dentalClaimsStateMap.getSelected());

			/**
			 * Determine if any claim types is set to copy all. Compare data list sizes (Only reliable way to ensure the
			 * user did not un-select any rows after clicking select all)
			 */
			countClaimSelections();

			// Step 1) Load the staging table
			dssClaimsHistoryTransferService
					.saveClaimsHistoryTransferStagingData(fileDownloadControllerBean.transferRequest);

			// Step 2) Execute stored procedure 'P_EXE_DSS_HISTORY_TRANSFER'
			dssClaimsHistoryTransferService.executeClaimsHistoryTransferProcedure(
					fileDownloadControllerBean.transferRequest.getSourceHealthCardNumber(),
					fileDownloadControllerBean.transferRequest.getTargetHealthCardNumber());

			// Step 3) Display success message as well as an export option
			displayTransferWizard = false;
			displayHcnSummary = false;
			displayExportPanel = true;

		} catch (ValidatorException ve) {
			Validator.displayValidationErrors(ve);
		} catch (DssClaimsHistoryTransferException te) {
			Validator.displayServiceError(te);
			te.printStackTrace();
			logger.error(te.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Selects all rows in the Medicare SE table. If the user has applied filters on the table the rows removed from
	 * view will not be selected
	 */
	public void executeSelectAllMedicareSe() {

		String tableId = "dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:medicareClaimsForm:medicareClaimDataTable";
		super.selectAllTableRows(tableId, medicareSeClaimsStateMap, selectedHealthCard.getMedicareClaims());
	}

	/**
	 * De-Selects all rows in the Medicare SE table. If the user has applied filters on the table the rows removed from
	 * view will not be selected
	 */
	public void executeDeselectAllMedicareSe() {

		String tableId = "dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:medicareClaimsForm:medicareClaimDataTable";
		super.unselectAllTableRows(tableId, medicareSeClaimsStateMap, selectedHealthCard.getMedicareClaims());
	}

	/**
	 * Selects all rows in the Medicare Mainframe table. If the user has applied filters on the table the rows removed
	 * from view will not be selected
	 */
	public void executeSelectAllMedicareMain() {

		String tableId = "dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:medicareMainframeClaimsForm:medicareMainframeClaimDataTable";
		super.selectAllTableRows(tableId, medicareMainClaimsStateMap, selectedHealthCard.getMedicareMainframeClaims());
	}

	/**
	 * De-Selects all rows in the Medicare Mainframe table. If the user has applied filters on the table the rows
	 * removed from view will not be selected
	 */
	public void executeDeselectAllMedicareMain() {

		String tableId = "dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:medicareMainframeClaimsForm:medicareMainframeClaimDataTable";
		super.unselectAllTableRows(tableId, medicareMainClaimsStateMap, selectedHealthCard.getMedicareMainframeClaims());
	}

	/**
	 * Selects all rows in the Pharmacare SE table. If the user has applied filters on the table the rows removed from
	 * view will not be selected
	 */
	public void executeSelectAllPharmacareSe() {

		String tableId = "dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:pharmClaimsForm:pharmacareClaimDataTable";
		super.selectAllTableRows(tableId, pharmSeClaimsStateMap, selectedHealthCard.getPharmacareClaims());
	}

	/**
	 * De-Selects all rows in the Pharmacare SE table. If the user has applied filters on the table the rows removed
	 * from view will not be selected
	 */
	public void executeDeselectAllPharmacareSe() {

		String tableId = "dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:pharmClaimsForm:pharmacareClaimDataTable";
		super.unselectAllTableRows(tableId, pharmSeClaimsStateMap, selectedHealthCard.getPharmacareClaims());
	}

	/**
	 * Selects all rows in the Pharmacare Mainframe table. If the user has applied filters on the table the rows removed
	 * from view will not be selected
	 */
	public void executeSelectAllPharmacareMain() {

		String tableId = "dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:pharmMainframeClaimsForm:pharmacareMainframeClaimDataTable";
		super.selectAllTableRows(tableId, pharmMainClaimsStateMap, selectedHealthCard.getPharmacareMainframeClaims());
	}

	/**
	 * De-Selects all rows in the Pharmacare Mainframe table. If the user has applied filters on the table the rows
	 * removed from view will not be selected
	 */
	public void executeDeselectAllPharmacareMain() {

		String tableId = "dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:pharmMainframeClaimsForm:pharmacareMainframeClaimDataTable";
		super.unselectAllTableRows(tableId, pharmMainClaimsStateMap, selectedHealthCard.getPharmacareMainframeClaims());
	}

	/**
	 * Selects all rows in the Denticare table. If the user has applied filters on the table the rows removed from view
	 * will not be selected
	 */
	public void executeSelectAllDenticare() {

		String tableId = "dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:denticareClaimsForm:dentalClaimDataTable";
		super.selectAllTableRows(tableId, dentalClaimsStateMap, selectedHealthCard.getDenticareClaims());
	}

	/**
	 * De-Selects all rows in the Denticare table. If the user has applied filters on the table the rows removed from
	 * view will not be selected
	 */
	public void executeDeselectAllDenticare() {

		String tableId = "dssClaimsHistoryTransferView:editDssClaimsHistoryTransferPanel:claimsHistoryPanel:denticareClaimsForm:dentalClaimDataTable";
		super.unselectAllTableRows(tableId, dentalClaimsStateMap, selectedHealthCard.getDenticareClaims());
	}

	/**
	 * Removes the specified claim from the MedicareSE claims list
	 * 
	 * @param aClaim
	 */
	public void removeSelectedMedicareSEClaim(MedicareClaim aClaim) {
		medicareSeClaimsStateMap.get(aClaim).setSelected(false);
	}

	/**
	 * Removes the specified claim from the Medicare Mainframe claims list
	 * 
	 * @param aClaim
	 */
	public void removeSelectedMedicareMainClaim(MedicareClaim aClaim) {
		medicareMainClaimsStateMap.get(aClaim).setSelected(false);
	}

	/**
	 * Removes the specified claim from the Denticare claims list
	 * 
	 * @param aClaim
	 */
	public void removeSelectedDentalClaim(DenticareClaim aClaim) {
		dentalClaimsStateMap.get(aClaim).setSelected(false);
	}

	/**
	 * Removes the specified claim from the PharmacareSE claims list
	 * 
	 * @param aClaim
	 */
	public void removeSelectedPharmSEClaim(PharmacareClaim aClaim) {
		pharmSeClaimsStateMap.get(aClaim).setSelected(false);
	}

	/**
	 * Removes the specified claim from the Pharmacare Mainframe claims list
	 * 
	 * @param aClaim
	 */
	public void removeSelectedPharmMainClaim(PharmacareClaim aClaim) {
		pharmMainClaimsStateMap.get(aClaim).setSelected(false);
	}

	/**
	 * Left Pad 0s to the provided Health Card Number if it is less than 10 digits.
	 * 
	 * @return HCN number with required 0s left padded (Example: 5 is passed as a HCN, 0000000005 is returned)
	 */
	private String applyHcnLeftPadding(String aHcn) {

		String result = "";

		if (aHcn != null) {
			result = aHcn.trim();

			if (result.length() != 10) {
				// Left pad with 0s (determine how many 0s need to be added)
				int padCount = 10 - result.length();
				String padding = "";
				// Ensure we do not kick off an infinite loop. Ensure number is above 0
				if (padCount > 0) {
					while (padCount != 0) {
						padding = padding + "0";
						padCount--;
					}
				}

				// Apply padding
				result = padding + result;
			}
		}
		return result;
	}

	/**
	 * Wipes out any state preserved in the data table statemaps
	 */
	public void resetClaimsSelectionData() {
		medicareSeClaimsStateMap = null;
		medicareMainClaimsStateMap = null;
		pharmSeClaimsStateMap = null;
		pharmMainClaimsStateMap = null;
		dentalClaimsStateMap = null;
	}

	/**
	 * Checks the claims selections made by the user to determine if the user selected all claims for any type
	 */
	private void countClaimSelections() {
		// Determine if all Medicare SE claims were selected
		if (fileDownloadControllerBean.transferRequest.getMedicareClaims() != null
				&& !fileDownloadControllerBean.transferRequest.getMedicareClaims().isEmpty()) {

			if (fileDownloadControllerBean.transferRequest.getMedicareClaims().size() == medicareSeClaimsStateMap
					.size()) {
				fileDownloadControllerBean.transferRequest.setAllMedicareSeClaimsSelected(true);
			}
		}

		// Determine if all Medicare Mainframe claims were selected
		if (fileDownloadControllerBean.transferRequest.getMedicareMainframeClaims() != null
				&& !fileDownloadControllerBean.transferRequest.getMedicareMainframeClaims().isEmpty()) {

			if (fileDownloadControllerBean.transferRequest.getMedicareMainframeClaims().size() == medicareMainClaimsStateMap
					.size()) {
				fileDownloadControllerBean.transferRequest.setAllMedicareMainframeClaimsSelected(true);
			}
		}

		// Determine if all Pharmacare SE claims were selected
		if (fileDownloadControllerBean.transferRequest.getPharmacareClaims() != null
				&& !fileDownloadControllerBean.transferRequest.getPharmacareClaims().isEmpty()) {

			if (fileDownloadControllerBean.transferRequest.getPharmacareClaims().size() == pharmSeClaimsStateMap.size()) {
				fileDownloadControllerBean.transferRequest.setAllPharmacareSeClaimsSelected(true);
			}
		}

		// Determine if all Pharmacare Mainframe claims were selected
		if (fileDownloadControllerBean.transferRequest.getPharmacareMainframeClaims() != null
				&& !fileDownloadControllerBean.transferRequest.getPharmacareMainframeClaims().isEmpty()) {

			if (fileDownloadControllerBean.transferRequest.getPharmacareMainframeClaims().size() == pharmMainClaimsStateMap
					.size()) {
				fileDownloadControllerBean.transferRequest.setAllPharmacareMainframeClaimsSelected(true);
			}
		}

		// Determine if all dental claims were selected
		if (fileDownloadControllerBean.transferRequest.getDentalClaims() != null
				&& !fileDownloadControllerBean.transferRequest.getDentalClaims().isEmpty()) {

			if (fileDownloadControllerBean.transferRequest.getDentalClaims().size() == dentalClaimsStateMap.size()) {
				fileDownloadControllerBean.transferRequest.setAllDentalClaimsSelected(true);
			}
		}
	}

	/**
	 * @return true if the current transfer request has any claims selected, false otherwise
	 */
	public boolean isAnyClaimsSelected() {
		if (medicareSeClaimsStateMap != null && !medicareSeClaimsStateMap.getSelected().isEmpty()) {
			return true;
		}
		if (medicareMainClaimsStateMap != null && !medicareMainClaimsStateMap.getSelected().isEmpty()) {
			return true;
		}
		if (pharmSeClaimsStateMap != null && !pharmSeClaimsStateMap.getSelected().isEmpty()) {
			return true;
		}
		if (pharmMainClaimsStateMap != null && !pharmMainClaimsStateMap.getSelected().isEmpty()) {
			return true;
		}
		if (dentalClaimsStateMap != null && !dentalClaimsStateMap.getSelected().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * @return the selectedHealthCard
	 */
	public HealthCard getSelectedHealthCard() {
		return selectedHealthCard;
	}

	/**
	 * @param selectedHealthCard
	 *            the selectedHealthCard to set
	 */
	public void setSelectedHealthCard(HealthCard selectedHealthCard) {
		this.selectedHealthCard = selectedHealthCard;
	}

	/**
	 * @return the displayMedicareSeClaims
	 */
	public boolean isDisplayMedicareSeClaims() {
		return displayMedicareSeClaims;
	}

	/**
	 * @param displayMedicareSeClaims
	 *            the displayMedicareSeClaims to set
	 */
	public void setDisplayMedicareSeClaims(boolean displayMedicareSeClaims) {
		this.displayMedicareSeClaims = displayMedicareSeClaims;
	}

	/**
	 * @return the displayMedicareMainClaims
	 */
	public boolean isDisplayMedicareMainClaims() {
		return displayMedicareMainClaims;
	}

	/**
	 * @param displayMedicareMainClaims
	 *            the displayMedicareMainClaims to set
	 */
	public void setDisplayMedicareMainClaims(boolean displayMedicareMainClaims) {
		this.displayMedicareMainClaims = displayMedicareMainClaims;
	}

	/**
	 * @return the displayPharmSeClaims
	 */
	public boolean isDisplayPharmSeClaims() {
		return displayPharmSeClaims;
	}

	/**
	 * @param displayPharmSeClaims
	 *            the displayPharmSeClaims to set
	 */
	public void setDisplayPharmSeClaims(boolean displayPharmSeClaims) {
		this.displayPharmSeClaims = displayPharmSeClaims;
	}

	/**
	 * @return the displayPharmMainClaims
	 */
	public boolean isDisplayPharmMainClaims() {
		return displayPharmMainClaims;
	}

	/**
	 * @param displayPharmMainClaims
	 *            the displayPharmMainClaims to set
	 */
	public void setDisplayPharmMainClaims(boolean displayPharmMainClaims) {
		this.displayPharmMainClaims = displayPharmMainClaims;
	}

	/**
	 * @return the displayDentalClaims
	 */
	public boolean isDisplayDentalClaims() {
		return displayDentalClaims;
	}

	/**
	 * @param displayDentalClaims
	 *            the displayDentalClaims to set
	 */
	public void setDisplayDentalClaims(boolean displayDentalClaims) {
		this.displayDentalClaims = displayDentalClaims;
	}

	/**
	 * @return the displayClaimsTransferSummary
	 */
	public boolean isDisplayClaimsTransferSummary() {
		return displayClaimsTransferSummary;
	}

	/**
	 * @param displayClaimsTransferSummary
	 *            the displayClaimsTransferSummary to set
	 */
	public void setDisplayClaimsTransferSummary(boolean displayClaimsTransferSummary) {
		this.displayClaimsTransferSummary = displayClaimsTransferSummary;
	}

	/**
	 * @return the medicareSeClaimsStateMap
	 */
	public RowStateMap getMedicareSeClaimsStateMap() {
		return medicareSeClaimsStateMap;
	}

	/**
	 * @param medicareSeClaimsStateMap
	 *            the medicareSeClaimsStateMap to set
	 */
	public void setMedicareSeClaimsStateMap(RowStateMap medicareSeClaimsStateMap) {
		this.medicareSeClaimsStateMap = medicareSeClaimsStateMap;
	}

	/**
	 * @return the medicareMainClaimsStateMap
	 */
	public RowStateMap getMedicareMainClaimsStateMap() {
		return medicareMainClaimsStateMap;
	}

	/**
	 * @param medicareMainClaimsStateMap
	 *            the medicareMainClaimsStateMap to set
	 */
	public void setMedicareMainClaimsStateMap(RowStateMap medicareMainClaimsStateMap) {
		this.medicareMainClaimsStateMap = medicareMainClaimsStateMap;
	}

	/**
	 * @return the pharmSeClaimsStateMap
	 */
	public RowStateMap getPharmSeClaimsStateMap() {
		return pharmSeClaimsStateMap;
	}

	/**
	 * @param pharmSeClaimsStateMap
	 *            the pharmSeClaimsStateMap to set
	 */
	public void setPharmSeClaimsStateMap(RowStateMap pharmSeClaimsStateMap) {
		this.pharmSeClaimsStateMap = pharmSeClaimsStateMap;
	}

	/**
	 * @return the pharmMainClaimsStateMap
	 */
	public RowStateMap getPharmMainClaimsStateMap() {
		return pharmMainClaimsStateMap;
	}

	/**
	 * @param pharmMainClaimsStateMap
	 *            the pharmMainClaimsStateMap to set
	 */
	public void setPharmMainClaimsStateMap(RowStateMap pharmMainClaimsStateMap) {
		this.pharmMainClaimsStateMap = pharmMainClaimsStateMap;
	}

	/**
	 * @return the dentalClaimsStateMap
	 */
	public RowStateMap getDentalClaimsStateMap() {
		return dentalClaimsStateMap;
	}

	/**
	 * @param dentalClaimsStateMap
	 *            the dentalClaimsStateMap to set
	 */
	public void setDentalClaimsStateMap(RowStateMap dentalClaimsStateMap) {
		this.dentalClaimsStateMap = dentalClaimsStateMap;
	}

	/**
	 * @return the displayHcnSearch
	 */
	public boolean isDisplayHcnSearch() {
		return displayHcnSearch;
	}

	/**
	 * @param displayHcnSearch
	 *            the displayHcnSearch to set
	 */
	public void setDisplayHcnSearch(boolean displayHcnSearch) {
		this.displayHcnSearch = displayHcnSearch;
	}

	/**
	 * @return the displayHcnSummary
	 */
	public boolean isDisplayHcnSummary() {
		return displayHcnSummary;
	}

	/**
	 * @param displayHcnSummary
	 *            the displayHcnSummary to set
	 */
	public void setDisplayHcnSummary(boolean displayHcnSummary) {
		this.displayHcnSummary = displayHcnSummary;
	}

	/**
	 * @return the displayTransferWizard
	 */
	public boolean isDisplayTransferWizard() {
		return displayTransferWizard;
	}

	/**
	 * @param displayTransferWizard
	 *            the displayTransferWizard to set
	 */
	public void setDisplayTransferWizard(boolean displayTransferWizard) {
		this.displayTransferWizard = displayTransferWizard;
	}

	/**
	 * @return the displayExportPanel
	 */
	public boolean isDisplayExportPanel() {
		return displayExportPanel;
	}

	/**
	 * @param displayExportPanel
	 *            the displayExportPanel to set
	 */
	public void setDisplayExportPanel(boolean displayExportPanel) {
		this.displayExportPanel = displayExportPanel;
	}

	/**
	 * @return the fileDownloadControllerBean
	 */
	public FileDownloadControllerBean getFileDownloadControllerBean() {
		return fileDownloadControllerBean;
	}

	/**
	 * @param fileDownloadControllerBean
	 *            the fileDownloadControllerBean to set
	 */
	public void setFileDownloadControllerBean(FileDownloadControllerBean fileDownloadControllerBean) {
		this.fileDownloadControllerBean = fileDownloadControllerBean;
	}
}
