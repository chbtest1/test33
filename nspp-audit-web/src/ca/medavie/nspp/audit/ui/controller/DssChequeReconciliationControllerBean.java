package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.SortCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.DssChequeReconciliationService;
import ca.medavie.nspp.audit.service.data.ChequeReconciliation;
import ca.medavie.nspp.audit.service.data.ChequeReconciliationSearchCriteria;
import ca.medavie.nspp.audit.service.exception.DssChequeReconciliationException;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for DSS Cheque Reconciliation pages
 */
@ManagedBean(name = "dssChequeReconiliationController")
@ViewScoped
public class DssChequeReconciliationControllerBean extends DropdownConstantsBean implements Serializable {

	/** IDE Generated */
	private static final long serialVersionUID = 6251185168036818686L;

	/** Logger for class */
	private static final Logger logger = LoggerFactory.getLogger(DssChequeReconciliationControllerBean.class);

	/** Search criteria for Cheque Reconciliation records */
	private ChequeReconciliationSearchCriteria chequeReconSearchCriteria;

	/** Holds the search results for Cheque Reconciliation */
	private LazyDataModel<ChequeReconciliation> chequeReconciliationSearchResults;

	/** Controls search results display */
	private boolean displayChequeReconSearchResults;

	/** Holds total row count for Cheque Reconciliation search */
	private Integer totalChequeReconciliations;

	/** The currently selected Cheque record */
	private ChequeReconciliation selectedChequeReconciliation;

	/** Controls the display of forums based on if the user is editing a record or not */
	private boolean editGLNumberMode;

	@EJB
	protected DssChequeReconciliationService dssChequeReconciliationService;

	/**
	 * Bean constructor
	 */
	public DssChequeReconciliationControllerBean() {
		if (dssChequeReconciliationService == null) {
			try {
				this.dssChequeReconciliationService = InitialContext.doLookup(prop
						.getProperty(DssChequeReconciliationService.class.getName()));
				logger.debug("dssChequeReconciliationService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Init search criteria
		chequeReconSearchCriteria = new ChequeReconciliationSearchCriteria();

		// Sync search data model with bean
		chequeReconciliationSearchResults = performChequeReconciliationSearchLoad();
	}

	/**
	 * Loads any matching Cheque Reconciliation records matching the search criteria
	 */
	public void executeChequeReconSearch() {

		try {
			// Perform search row count
			totalChequeReconciliations = dssChequeReconciliationService
					.getChequeReconciliationSearchResultsCount(chequeReconSearchCriteria);
			chequeReconciliationSearchResults.setRowCount(totalChequeReconciliations);

			// Ensure data table is in fresh state
			resetDataTableState("editDssChequeReconciliationView:editDssChequeReconciliationPanel:searchChequeReconciliationForm:chequeReconSearchResultsDatatable");

			// Display search results
			displayChequeReconSearchResults = true;

		} catch (DssChequeReconciliationException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	private LazyDataModel<ChequeReconciliation> performChequeReconciliationSearchLoad() {
		chequeReconciliationSearchResults = new LazyDataModel<ChequeReconciliation>() {

			/** IDE generated */
			private static final long serialVersionUID = -853906767160396658L;

			@Override
			public List<ChequeReconciliation> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<ChequeReconciliation> results = new ArrayList<ChequeReconciliation>();

				try {
					if (displayChequeReconSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								chequeReconSearchCriteria.addSortArgument(ChequeReconciliation.class,
										sc.getPropertyName(), sc.isAscending());
							}
						}

						// Load record matching search results
						results = dssChequeReconciliationService.getChequeReconciliationSearchResults(
								chequeReconSearchCriteria, first, pageSize);
					}
				} catch (DssChequeReconciliationException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return chequeReconciliationSearchResults;
	}

	/**
	 * Clears out the Cheque Reconciliation search form
	 */
	public void executeClearChequeReconSearch() {
		chequeReconSearchCriteria.resetSearchCriteria();
		displayChequeReconSearchResults = false;
	}

	/**
	 * Brings the selected Cheque Reconciliation record up for edit
	 * 
	 * @param aChequeReconForEdit
	 */
	public void executeEditChequeReconciliation(ChequeReconciliation aChequeReconForEdit) {
		selectedChequeReconciliation = aChequeReconForEdit;
		editGLNumberMode = true;
	}

	/**
	 * Saves the changes made to the Cheque Reconciliation record
	 */
	public void executeSaveChequeReconciliation() {
		try {

			// Only continue if GL number was provided
			if (selectedChequeReconciliation.getGlNumber() != null) {
				// Update the last modified date
				selectedChequeReconciliation.setLastModified(new Date());

				// Save the changes
				dssChequeReconciliationService.saveChequeReconciliation(selectedChequeReconciliation);

				// Save successful. Update the variables that hold the original value
				selectedChequeReconciliation.setOriginalChequeNote(selectedChequeReconciliation.getChequeNote());
				selectedChequeReconciliation.setOriginalGlNumber(selectedChequeReconciliation.getGlNumber());

				// Display success message
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_INFO,
						"INFO.DSS.CHEQUE.RECONCILIATION.SAVE.SUCCESSFUL"));
			} else {
				// Display parameter error message
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.DSS.CHEQUE.RECONCILIATION.GL.NUM.REQUIRED"));
			}
		} catch (DssChequeReconciliationException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());

		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Returns the user to the search screen
	 */
	public void executeReturnToChequeSearch() {
		editGLNumberMode = false;
	}

	/**
	 * @return the selectedChequeReconciliation
	 */
	public ChequeReconciliation getSelectedChequeReconciliation() {
		return selectedChequeReconciliation;
	}

	/**
	 * @param selectedChequeReconciliation
	 *            the selectedChequeReconciliation to set
	 */
	public void setSelectedChequeReconciliation(ChequeReconciliation selectedChequeReconciliation) {
		this.selectedChequeReconciliation = selectedChequeReconciliation;
	}

	/**
	 * @return the editGLNumberMode
	 */
	public boolean isEditGLNumberMode() {
		return editGLNumberMode;
	}

	/**
	 * @param editGLNumberMode
	 *            the editGLNumberMode to set
	 */
	public void setEditGLNumberMode(boolean editGLNumberMode) {
		this.editGLNumberMode = editGLNumberMode;
	}

	/**
	 * @return the chequeReconciliationSearchResults
	 */
	public LazyDataModel<ChequeReconciliation> getChequeReconciliationSearchResults() {
		return chequeReconciliationSearchResults;
	}

	/**
	 * @param chequeReconciliationSearchResults
	 *            the chequeReconciliationSearchResults to set
	 */
	public void setChequeReconciliationSearchResults(
			LazyDataModel<ChequeReconciliation> chequeReconciliationSearchResults) {
		this.chequeReconciliationSearchResults = chequeReconciliationSearchResults;
	}

	/**
	 * @return the displayChequeReconSearchResults
	 */
	public boolean isDisplayChequeReconSearchResults() {
		return displayChequeReconSearchResults;
	}

	/**
	 * @param displayChequeReconSearchResults
	 *            the displayChequeReconSearchResults to set
	 */
	public void setDisplayChequeReconSearchResults(boolean displayChequeReconSearchResults) {
		this.displayChequeReconSearchResults = displayChequeReconSearchResults;
	}

	/**
	 * @return the totalChequeReconciliations
	 */
	public Integer getTotalChequeReconciliations() {
		return totalChequeReconciliations;
	}

	/**
	 * @param totalChequeReconciliations
	 *            the totalChequeReconciliations to set
	 */
	public void setTotalChequeReconciliations(Integer totalChequeReconciliations) {
		this.totalChequeReconciliations = totalChequeReconciliations;
	}

	/**
	 * @return the chequeReconSearchCriteria
	 */
	public ChequeReconciliationSearchCriteria getChequeReconSearchCriteria() {
		return chequeReconSearchCriteria;
	}

	/**
	 * @param chequeReconSearchCriteria
	 *            the chequeReconSearchCriteria to set
	 */
	public void setChequeReconSearchCriteria(ChequeReconciliationSearchCriteria chequeReconSearchCriteria) {
		this.chequeReconSearchCriteria = chequeReconSearchCriteria;
	}
}
