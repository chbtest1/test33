package ca.medavie.nspp.audit.ui.controller.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * Custom converter to convert String to Date and back again.
 * 
 * @author bcainne
 * 
 */
/**
 * @author bcainne
 * 
 */
@FacesConverter("dateConverter")
public class DateConverter implements Converter {
	public static final String NBSP = "\u00A0";

	/**
	 * Converts Date to a String.
	 * 
	 * @param aContext
	 *            The FacesContext
	 * @param aComponent
	 *            The UIComponent
	 * @param aString
	 *            The String to convert
	 * 
	 * @return Cleaned String
	 */
	public Object getAsObject(FacesContext aContext, UIComponent aComponent, String aString) {
		Date aDate = new Date();
		if (aString == null || aString.trim().equals("")) {
			return null;
		}

		aString = aString.replaceAll("[\\s][^0-9A-Za-z][^-]*", "").toUpperCase();
		String pattern = (String) aComponent.getAttributes().get("pattern");
		SimpleDateFormat dateFormatter = new SimpleDateFormat(pattern);
		try {
			aDate = dateFormatter.parse(aString);

		} catch (ParseException e) {
			Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
			String bundleLocation = "ca.medavie.nspp.audit.ui.messages";
			ResourceBundle messageBundle = ResourceBundle.getBundle(bundleLocation, currentLocale);
			String errorMsg = messageBundle.getString("ERROR.APPLICATION.DATE");
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, errorMsg);
			throw new ConverterException(fm);
		}
		return aDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
	 * javax.faces.component.UIComponent, java.lang.Object)
	 */
	public String getAsString(FacesContext aContext, UIComponent aComponent, Object anObj) {
		// Check to make sure the Object is a String that can be converted to a date.
		String pattern = (String) aComponent.getAttributes().get("pattern");
		SimpleDateFormat dateFormatter = new SimpleDateFormat(pattern);
		return dateFormatter.format(anObj);
	}
}
