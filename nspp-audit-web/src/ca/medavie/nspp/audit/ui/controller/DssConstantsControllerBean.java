package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.DssConstantsService;
import ca.medavie.nspp.audit.service.data.DssConstantBO;
import ca.medavie.nspp.audit.service.exception.DssConstantsServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.DssConstantsValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for the Edit DSS Constants page.
 */
@ManagedBean(name = "dssConstantsController")
@ViewScoped
public class DssConstantsControllerBean extends DropdownConstantsBean implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger(DssConstantsControllerBean.class);
	/**
	 * IDE generated serial number
	 */
	private static final long serialVersionUID = 380033445068528901L;

	// Holds reference to the EJB Client service class
	@EJB
	protected DssConstantsService dssConstantsService;

	private List<DssConstantBO> dssConstants;

	private DssConstantsValidator validator;

	private boolean displayDssEditConstant;

	private DssConstantBO selectedDssConstant;

	/**
	 * Bean constructor
	 */
	public DssConstantsControllerBean() {

		if (this.dssConstantsService == null) {
			try {
				this.dssConstantsService = InitialContext
						.doLookup(prop.getProperty(DssConstantsService.class.getName()));
				logger.debug("dssConstantsService is loaded in the constructor");

			} catch (NamingException e) {

				e.printStackTrace();
			}
		}

		// set up validator
		validator = new DssConstantsValidator();

		// Init Add/Edit page since it is displayed on load
		initiateAddEditDssUnitValuePanel();
	}

	/**
	 * Prepare bean for Add/Edit panel
	 */
	private void initiateAddEditDssUnitValuePanel() {
		try {
			dssConstants = dssConstantsService.getDssConstants();
			if (dssConstants != null && !dssConstants.isEmpty()) {
				Collections.sort(dssConstants, Collections.reverseOrder());
			}
		}
		// catch service exceptions
		catch (DssConstantsServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Update the existing dss constant in the table
	 * 
	 * @param aDssConstantBO
	 */
	public void editThisRow(DssConstantBO aDssConstantBO) {

		// reset it first
		selectedDssConstant = null;
		// set it to a selected one
		this.selectedDssConstant = aDssConstantBO;
		// display pop up editing window
		displayDssEditConstant = true;
	}

	/**
	 * save the changes for dss constant record after edited
	 */
	public void saveDssConstantChanges() {
		try {
			selectedDssConstant.setLastModified(new Date());
			selectedDssConstant.setModifiedBy(FacesUtils.getUserId());
			// validate bo first
			validator.validate(selectedDssConstant);
			// do the save
			dssConstantsService.updateDssConstant(selectedDssConstant);

			// Close edit pop-up
			displayDssEditConstant = false;
			closePopupWindowJS();

			// Display save success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.DSS.CONSTANT.SAVE.SUCCESSFUL");

		} // catch validation exceptions
		catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch service exceptions
		catch (DssConstantsServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}

	}

	/**
	 * close up the edit dss constant pop up page
	 * 
	 */
	public void closeEditConstantPopup() {
		// display pop up editing window
		displayDssEditConstant = false;
		closePopupWindowJS();
	}

	public List<DssConstantBO> getDssConstants() {
		return dssConstants;
	}

	public void setDssConstants(List<DssConstantBO> dssConstants) {
		this.dssConstants = dssConstants;
	}

	public boolean isDisplayDssEditConstant() {
		return displayDssEditConstant;
	}

	public void setDisplayDssEditConstant(boolean displayDssEditConstant) {
		this.displayDssEditConstant = displayDssEditConstant;
	}

	public DssConstantBO getSelectedDssConstant() {
		return selectedDssConstant;
	}

	public void setSelectedDssConstant(DssConstantBO selectedDssConstant) {
		this.selectedDssConstant = selectedDssConstant;
	}
}
