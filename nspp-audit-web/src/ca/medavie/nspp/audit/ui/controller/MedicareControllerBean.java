package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.RowStateMap;
import org.icefaces.ace.model.table.SortCriteria;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.MedicareService;
import ca.medavie.nspp.audit.service.PeerGroupService;
import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.data.AuditCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.BusinessArrangement;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteria;
import ca.medavie.nspp.audit.service.data.HealthServiceCriteriaSearchCriteria;
import ca.medavie.nspp.audit.service.data.IndividualExclusion;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponse;
import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponseSearchCriteria;
import ca.medavie.nspp.audit.service.data.PayeeType;
import ca.medavie.nspp.audit.service.data.PaymentResponsibility;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;
import ca.medavie.nspp.audit.service.exception.PeerGroupServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.HealthServiceExclusionValidator;
import ca.medavie.nspp.audit.ui.controller.validator.IndividualExclusionValidator;
import ca.medavie.nspp.audit.ui.controller.validator.MedicareALRValidator;
import ca.medavie.nspp.audit.ui.controller.validator.MedicareAuditCriteriaValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for the Medicare pages (Service Verification Letters).
 */
@ManagedBean(name = "medicareController")
@ViewScoped
public class MedicareControllerBean extends DropdownConstantsBean implements Serializable {

	/** IDE generated */
	private static final long serialVersionUID = -5377824863769978942L;
	private static final Logger logger = LoggerFactory.getLogger(MedicareControllerBean.class);

	/** EJB services */
	@EJB
	protected MedicareService medicareService;
	@EJB
	protected PeerGroupService peerGroupService;

	/** Common object variables shared amongst multiple pages in Medicare */
	private HealthServiceCriteriaSearchCriteria hscSearchCriteria;
	private List<HealthServiceCriteria> healthServiceCriteriaSearchResults;
	private boolean displayHealthServiceCriteriaSearchResults;
	private RowStateMap hscSearchResultsStateMap;

	/** Object Validators */
	private IndividualExclusionValidator individualExclusionValidator;
	private HealthServiceExclusionValidator healthServiceExclusionValidator;

	/** ******************************* */
	/** Audit Criteria screen variables */
	/** ******************************* */
	private MedicareAuditCriteriaValidator auditCriteriaValidator;
	private AuditCriteria selectedAuditCriteria;
	private boolean editMode;
	private LazyDataModel<AuditCriteria> auditCriteriaSearchResults;
	private Integer totalAuditCriteriaRows;
	private Boolean displayAuditCriteriaSearchResults;
	private AuditCriteriaSearchCriteria auditCriteriaSearchCriteria;
	/** Boolean that keeps track if we are editing a Practitioner Audit Criteria or not */
	private boolean editingPractitionerAuditCriteria;
	private String selectedPaymentResponsibility;
	private String selectedPayeeType;

	/** ************************************** */
	/** Audit Letter Response screen variables */
	/** ************************************** */
	private MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria;
	private MedicareAuditLetterResponse selectedMCALR;
	private Long numberOfLettersCreated;
	private boolean displayNumberOfLettersCreated;
	private Integer totalALRRows;
	private MedicareALRValidator medicareALRValidator;
	private LazyDataModel<MedicareAuditLetterResponse> searchedMedicareALRs;
	private boolean displayALRSearchResults;

	/** ******************************************** */
	/** Health Service Descriptions screen variables */
	/** ******************************************** */
	/** Holds the description that will be applied to selected HealthService descriptions */
	private String bulkUpdateDescription;
	private boolean displayHealthServiceUpdateWindow;

	/** ****************************************** */
	/** Health Service Exclusions screen variables */
	/** ****************************************** */
	private List<HealthServiceCriteria> healthServiceExclusions;
	private RowStateMap hscExclusionsStateMap;
	private boolean displayHealthServiceExclusionDeleteConfirm;

	/** ************************************** */
	/** Individual Exclusions screen variables */
	/** ************************************** */
	private List<IndividualExclusion> individualExclusions;
	private IndividualExclusion newIndividualExclusion;

	/** ***************************** */
	/** BA Exclusion screen variables */
	/** ***************************** */
	/** Holds all Business Arrangement Exclusions in the system */
	private List<BusinessArrangement> baExclusions;
	/** Holds the BA number when adding a new exclusion */
	private Long businessArrangementNumber;
	/** State map for BA Exclusions. Used for delete functionality */
	private RowStateMap baExclusionsStateMap;
	/** Boolean to control BA exclusion delete confirmation window */
	private Boolean displayBAExclusionDeleteConfirm;

	/**
	 * Medicare controller bean constructor
	 */
	public MedicareControllerBean() {

		// Set up EJB services
		if (this.medicareService == null) {
			try {
				this.medicareService = InitialContext.doLookup(prop.getProperty(MedicareService.class.getName()));
				logger.debug("medicareService is loaded in the constructor");

			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		if (this.peerGroupService == null) {
			try {
				this.peerGroupService = InitialContext.doLookup(prop.getProperty(PeerGroupService.class.getName()));
				logger.debug("peerGroupService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Instantiate variables
		auditCriteriaSearchCriteria = new AuditCriteriaSearchCriteria();
		medicareALRSearchCriteria = new MedicareAuditLetterResponseSearchCriteria();
		hscSearchCriteria = new HealthServiceCriteriaSearchCriteria();

		// Instantiate validators
		individualExclusionValidator = new IndividualExclusionValidator();
		healthServiceExclusionValidator = new HealthServiceExclusionValidator();
		auditCriteriaValidator = new MedicareAuditCriteriaValidator(medicareService);
		medicareALRValidator = new MedicareALRValidator();

		// Ensure state maps are cleared and reset
		hscSearchResultsStateMap = new RowStateMap();
		hscExclusionsStateMap = new RowStateMap();

		// Init the Search/Edit panel since it is displayed on load
		initiateSearchEditAuditCriteriaPanel();

		// Connects the LazyDataModel to the bean state for ALR search
		searchedMedicareALRs = performALRSearchLoad();

		// Connects the LazyDataModel to the bean state for Audit Criteria search
		auditCriteriaSearchResults = performAuditCriteriaSearchLoad();
	}

	/**
	 * 0-Search/Edit Audit Criteria, 1-Add New Audit Criteria, 2-Search/Edit Responses, 3-Search/Edit HS Descriptions,
	 * 4-Search/Edit Global Exclusions
	 * 
	 * @param anEvent
	 */
	public void medicareTabChangeListener(ValueChangeEvent anEvent) {
		// Get the index of the requested tab
		Integer requestedTabIndex = (Integer) anEvent.getNewValue();

		// Prepare backing bean for the requested tab
		if (requestedTabIndex.equals(Integer.valueOf("0"))) {
			initiateSearchEditAuditCriteriaPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("1"))) {
			initiateNewAuditCriteriaPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("2"))) {
			initiateSearchEditResponsesPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("3"))) {
			initiateSearchEditHsDescriptionsPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("4"))) {
			initiateSearchEditHsExclusionsPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("5"))) {
			initiateSearchEditIndExclusionsPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("6"))) {
			initiateBusinessArrangementExclusionsPanel();
		}
	}

	/***********************************/
	/****** Audit Criteria methods *****/
	/***********************************/

	/**
	 * Navigates the user to the Search/Edit Audit Criteria panel
	 */
	private void initiateSearchEditAuditCriteriaPanel() {

		// Instantiate variables
		auditCriteriaSearchCriteria = new AuditCriteriaSearchCriteria();
		hscSearchCriteria = new HealthServiceCriteriaSearchCriteria();
		hscSearchResultsStateMap = new RowStateMap();
		hscExclusionsStateMap = new RowStateMap();
		medicareALRSearchCriteria = new MedicareAuditLetterResponseSearchCriteria();
		selectedAuditCriteria = new AuditCriteria();

		// Set display search results to false
		displayAuditCriteriaSearchResults = false;

		// Set Practitioner audit edit flag to false
		editingPractitionerAuditCriteria = false;
		editMode = false;
	}

	/**
	 * Navigates the user to the New Audit Criteria panel
	 */
	private void initiateNewAuditCriteriaPanel() {
		selectedAuditCriteria = new AuditCriteria();
		editingPractitionerAuditCriteria = true;
		editMode = false;
		hscSearchCriteria.resetSearchCriteria();
	}

	/**
	 * Searches for matching Practitioner AuditCriteria records using the users search criteria
	 */
	public void executeAuditCriteriaSearch() {

		logger.debug("executeAuditCriteriaSearch() : Begin");

		// Display search results..
		displayAuditCriteriaSearchResults = true;
		try {

			// Count the number of records for entire search set
			totalAuditCriteriaRows = medicareService.getSearchedAuditCriteriaCount(auditCriteriaSearchCriteria);
			auditCriteriaSearchResults.setRowCount(totalAuditCriteriaRows);

			// Ensure search results table is in a clean state
			resetDataTableState("searchEditAuditCriteriaView:searchAuditCriteriaPanel:auditCriteriaSearchForm:auditCriteriaSearchResultsTable");

		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		logger.debug("executeAuditCriteriaSearch() : end");
	}

	/**
	 * Loads the matching Audit Criteria records based on the search criteria
	 * 
	 * @return AuditCriteria records matching the user search criteria
	 */
	private LazyDataModel<AuditCriteria> performAuditCriteriaSearchLoad() {
		auditCriteriaSearchResults = new LazyDataModel<AuditCriteria>() {

			/** IDE generated */
			private static final long serialVersionUID = -321591594841978036L;

			@Override
			public List<AuditCriteria> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<AuditCriteria> results = new ArrayList<AuditCriteria>();

				try {
					if (displayAuditCriteriaSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								auditCriteriaSearchCriteria.addSortArgument(AuditCriteria.class, sc.getPropertyName(),
										sc.isAscending());
							}
						}

						// Load record matching search results
						results = medicareService.getSearchPractitionerAuditCriterias(auditCriteriaSearchCriteria,
								first, pageSize);
					}
				} catch (MedicareServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return auditCriteriaSearchResults;
	}

	/**
	 * Assists in reseting the Audit search form. Typically we would use the super clear method but we must retain
	 * certain values through the reset due to page flow.
	 */
	public void executeClearAuditSearch() {
		clearForm();

		// Grab the state we need to carry over
		String selectedType = auditCriteriaSearchCriteria.getAuditCriteriaType();
		boolean practitionerSearch = auditCriteriaSearchCriteria.isPractitionerSearch();

		auditCriteriaSearchCriteria.resetSearchCriteria();
		displayAuditCriteriaSearchResults = false;
		// Ensure search results table is in a clean state
		resetDataTableState("searchEditAuditCriteriaView:searchAuditCriteriaPanel:auditCriteriaSearchForm:auditCriteriaSearchResultsTable");

		// Restore state
		auditCriteriaSearchCriteria.setAuditCriteriaType(selectedType);
		auditCriteriaSearchCriteria.setPractitionerSearch(practitionerSearch);
	}

	/**
	 * Loads the data for the selected Default Audit and displays the edit screen.
	 */
	public void executeEditDefaultAuditCriteria() {

		if (auditCriteriaSearchCriteria.getAuditType() != null) {
			try {

				// Attempt to load the Default AuditCriteria. Creates new record if nothing is found
				selectedAuditCriteria = medicareService.getDefaultAuditCriteriaByType(auditCriteriaSearchCriteria
						.getAuditType());

				// Toggle edit flags to correct state
				editingPractitionerAuditCriteria = false;
				editMode = true;

				// Reset search form state on edit page
				selectedPayeeType = "";
				selectedPaymentResponsibility = "";
				hscSearchCriteria.resetSearchCriteria();

				// Display edit screen
				JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayEditMedAuditCriteria()");

			} catch (MedicareServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			} catch (Exception e) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.APPLICATION.FAILURE"));
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	/**
	 * Returns the user to Audit Criteria Search screen
	 */
	public void executeReturnToAuditCriteriaSearch() {
		// Reset boolean flags
		editMode = false;
		editingPractitionerAuditCriteria = false;
		
		// Ensure search results are displayed if practitioner search was performed.
		if (auditCriteriaSearchCriteria.isPractitionerSearch()) {
			displayAuditCriteriaSearchResults = true;
		}

		selectedAuditCriteria = new AuditCriteria();
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToEditMedAuditCriteriaSearch()");
	}

	public void editSelectedPractitionerAuditCriteria(AuditCriteria anAuditCriteria) {
		// Grab all the details of the selected record and display the edit page to the user
		try {
			selectedAuditCriteria = medicareService.getPractitionerAuditCriteriaById(anAuditCriteria
					.getAuditCriteriaId());

			// Set practitioner name here. Saves us from an additional SQL query
			selectedAuditCriteria.setProviderName(anAuditCriteria.getProviderName());

			// Upon successful load. Display edit screen to the user
			editMode = true;
			editingPractitionerAuditCriteria = true;

			// Ensure search is not running in the background
			displayAuditCriteriaSearchResults = false;

			// Reset search form state on edit page
			selectedPayeeType = "";
			selectedPaymentResponsibility = "";
			hscSearchCriteria.resetSearchCriteria();

			// Transition UI
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayEditMedAuditCriteria()");

		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Saves changes made to the selected Audit Criteria
	 */
	public void executeAuditCriteriaSave() {
		try {

			// If this is a default Audit. Sync the payment To date with the From value
			if (auditCriteriaSearchCriteria.getAuditCriteriaType().equals(
					AuditCriteriaSearchCriteria.GENERAL_DEFAULT_AUDIT_CRIT)) {
				selectedAuditCriteria.setAuditToPaymentDate(selectedAuditCriteria.getAuditFromPaymentDate());
			}

			// Validate the user provided values. This includes Practitioner existence and duplicate audit check
			auditCriteriaValidator.validate(selectedAuditCriteria);

			// Apply edit meta data
			selectedAuditCriteria.setModifiedBy(FacesUtils.getUserId());
			selectedAuditCriteria.setLastModified(new Date());
			// Perform save
			medicareService.saveMedicareAuditCriteria(selectedAuditCriteria);

			// Display success message
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_INFO,
					"INFO.VERIFICATION.LETTERS.MED.AUD.CRITERIA.SAVE.SUCCESSFUL"));

		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (ValidatorException ve) {
			Validator.displayValidationErrors(ve);
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Searches for HealthServiceCriteria for Edit/Add Audit Criteria pages
	 */
	public void executeHealthServiceCriteriaSearch() {
		try {
			// Search for matching HealthServiceCriteria
			healthServiceCriteriaSearchResults = peerGroupService
					.getHealthServiceCriteriaSearchResults(hscSearchCriteria);
			// Display results
			displayHealthServiceCriteriaSearchResults = true;

		} catch (PeerGroupServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * reset the search form
	 */
	public void resetHscSearchCriteriaForm() {
		this.hscSearchCriteria.resetSearchCriteria();
	}

	/**
	 * Selects all rows in the Audit Criteria HSC table. If the user has applied filters on the table the rows removed
	 * from view will not be selected
	 */
	public void executeSelectAllHsc() {

		String tableId;
		if (editMode) {
			tableId = "searchEditAuditCriteriaView:searchAuditCriteriaPanel:hscResultPopupForm:hscSearchResultPopup:healthServiceCodeSearchDatatable";
		} else {
			tableId = "editAuditCriteriaPanel:addNewAuditCriteriaView:hscResultPopupForm:hscSearchResultPopup:healthServiceCodeSearchDatatable";
		}

		super.selectAllTableRows(tableId, hscSearchResultsStateMap, healthServiceCriteriaSearchResults);
	}

	/**
	 * De-Selects all rows in the Audit Criteria HSC table. If the user has applied filters on the table the rows
	 * removed from view will not be selected
	 */
	public void executeDeselectAllHsc() {

		String tableId;
		if (editMode) {
			tableId = "searchEditAuditCriteriaView:searchAuditCriteriaPanel:hscResultPopupForm:hscSearchResultPopup:healthServiceCodeSearchDatatable";
		} else {
			tableId = "editAuditCriteriaPanel:addNewAuditCriteriaView:hscResultPopupForm:hscSearchResultPopup:healthServiceCodeSearchDatatable";
		}

		super.unselectAllTableRows(tableId, hscSearchResultsStateMap, healthServiceCriteriaSearchResults);
	}

	/**
	 * Adds the selected HealthServiceCriteria found from search to the currently selected Audit Criteria
	 */
	public void addSelectedHscToAuditCriteria() {
		@SuppressWarnings("unchecked")
		List<HealthServiceCriteria> selectedHsc = hscSearchResultsStateMap.getSelected();

		// Determine if there is a HSC list in the AuditCriteria. If not create it
		if (selectedAuditCriteria.getHealthServiceCriterias() == null) {
			selectedAuditCriteria.setHealthServiceCriterias(new ArrayList<HealthServiceCriteria>());
		}

		// If there is selected rows add them to the audit criteria
		if (!selectedHsc.isEmpty()) {
			for (HealthServiceCriteria hsc : selectedHsc) {
				// Set the ID of the AuditCriteria on the HSC record
				hsc.setAuditCriteriaId(selectedAuditCriteria.getAuditCriteriaId());
				hsc.setModifiedBy(FacesUtils.getUserId());
				hsc.setLastModified(new Date());

				// Perform duplicate check
				boolean duplicateDetected = false;
				for (HealthServiceCriteria h : selectedAuditCriteria.getHealthServiceCriterias()) {

					// Due to objects having different depths of info we cannot simply use !equals..
					if ((hsc.getHealthServiceCode().equals(h.getHealthServiceCode()))
							&& (hsc.getQualifier().equals(h.getQualifier()))
							&& (hsc.getModifiers().equals(h.getModifiers()))
							&& (hsc.getImplicitModifiers().equals(h.getImplicitModifiers()))) {
						duplicateDetected = true;
					}
				}

				// Add if no duplicate found
				if (!duplicateDetected) {
					selectedAuditCriteria.getHealthServiceCriterias().add(hsc);
				}
			}
		}
		// Close search results pop up
		closeHealthServiceCriteriaSearchResults();
	}

	/**
	 * Adds the selected Payment Responsibility to the Audit Criteria
	 */
	public void addSelectedPaymentResponsibilityToAuditCriteria() {

		// Only continue if a selection has been made
		if (selectedPaymentResponsibility != null && !selectedPaymentResponsibility.isEmpty()) {
			PaymentResponsibility pr = new PaymentResponsibility();
			pr.setPaymentResponsibility(selectedPaymentResponsibility);
			pr.setAuditCriteriaId(selectedAuditCriteria.getAuditCriteriaId());
			pr.setModifiedBy(FacesUtils.getUserId());
			pr.setLastModified(new Date());

			// Determine if there is a PaymentResponsibility list in the AuditCriteria. If not create it
			if (selectedAuditCriteria.getPaymentResponsibilities() == null) {
				selectedAuditCriteria.setPaymentResponsibilities(new ArrayList<PaymentResponsibility>());
			}

			// Check that we are not adding a duplicate Payment Resp
			boolean duplicateDetected = false;
			for (PaymentResponsibility p : selectedAuditCriteria.getPaymentResponsibilities()) {
				if (pr.getPaymentResponsibility().equals(p.getPaymentResponsibility())) {
					duplicateDetected = true;
					break;
				}
			}

			// Add if no duplicate record was found.
			if (!duplicateDetected) {
				selectedAuditCriteria.getPaymentResponsibilities().add(pr);
			}

			// Clear out selected option
			selectedPaymentResponsibility = "";
		}
	}

	/**
	 * Adds the selected Payee Type to the Audit Criteria
	 */
	public void addSelectedPayeeTypeToAuditCriteria() {
		// Only continue if a selection has been made
		if (selectedPayeeType != null && !selectedPayeeType.isEmpty()) {
			PayeeType pt = new PayeeType();
			pt.setPayToCode(selectedPayeeType);
			pt.setAuditCriteriaId(selectedAuditCriteria.getAuditCriteriaId());
			pt.setModifiedBy(FacesUtils.getUserId());
			pt.setLastModified(new Date());

			// Determine if there is a PayeeType list in the AuditCriteria. If not create it
			if (selectedAuditCriteria.getPayeeTypes() == null) {
				selectedAuditCriteria.setPayeeTypes(new ArrayList<PayeeType>());
			}

			// Check that we are not adding a duplicate Payee Type
			boolean duplicateDetected = false;
			for (PayeeType p : selectedAuditCriteria.getPayeeTypes()) {
				if (pt.getPayToCode().equals(p.getPayToCode())) {
					duplicateDetected = true;
					break;
				}
			}

			// Add if no duplicate record was found
			if (!duplicateDetected) {
				selectedAuditCriteria.getPayeeTypes().add(pt);
			}

			// Clear out selected option
			selectedPayeeType = "";
		}
	}

	/**
	 * Takes the user to step 2 of Audit Criteria Search
	 * 
	 * Based on the dropdown selection the appropriate search form will be displayed
	 */
	public void displayAuditCriteriaSearchForm() {

		// Do nothing if they select the option select....
		if (auditCriteriaSearchCriteria.getAuditCriteriaType() != null) {
			if (auditCriteriaSearchCriteria.getAuditCriteriaType().equals(
					AuditCriteriaSearchCriteria.GENERAL_PRACT_AUDIT_CRIT)) {
				auditCriteriaSearchCriteria.setPractitionerSearch(true);
			} else {
				auditCriteriaSearchCriteria.setPractitionerSearch(false);
				displayAuditCriteriaSearchResults = false;
			}
			// Render the search form
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAuditCriteriaSearch()");
		}
	}

	/**
	 * Returns the user to step 1 of Audit Criteria Search
	 */
	public void returnToAuditCriteriaTypeSelection() {
		// Resets the search criteria
		auditCriteriaSearchCriteria.resetSearchCriteria();
		displayAuditCriteriaSearchResults = false;
		// Render the Audit Criteria Type selection
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAuditCriteriaTypeSelection()");
	}

	/**
	 * Triggered when the user selects an Audit Type for Add Audit Criteria
	 */
	public void displayAddNewAuditCriteriaForm() {
		try {
			// Validate the users inputs
			auditCriteriaValidator.validateNewCriteriaParameters(selectedAuditCriteria);

			// Set the Audit Criteria Type to Practitioner
			auditCriteriaSearchCriteria.setAuditCriteriaType(AuditCriteriaSearchCriteria.GENERAL_PRACT_AUDIT_CRIT);

			// Take user to edit screen if validation successful
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAddNewMedAuditCriteria()");

		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Removes the specified HealthServiceCriteria from the Audit Criteria. Changes to DB only done on save
	 * 
	 * @param anHsc
	 */
	public void removeHscFromAuditCriteria(HealthServiceCriteria anHsc) {
		selectedAuditCriteria.getHealthServiceCriterias().remove(anHsc);
	}

	/**
	 * Removes the specified PaymentResponsibility from the Audit Criteria. Changes to DB only done on save
	 * 
	 * @param aPaymentResp
	 */
	public void removePaymentRespFromAuditCriteria(PaymentResponsibility aPaymentResp) {
		selectedAuditCriteria.getPaymentResponsibilities().remove(aPaymentResp);
	}

	/**
	 * Removes the specified PayeeType from the Audit Criteria. Changes to DB only done on save
	 * 
	 * @param aPayeeType
	 */
	public void removePayeeTypeFromAuditCriteria(PayeeType aPayeeType) {
		selectedAuditCriteria.getPayeeTypes().remove(aPayeeType);
	}

	/**
	 * Returns the user to Type selection for Add new Med Audit Criteria
	 */
	public void returnToAddNewAuditTypeSelection() {
		selectedAuditCriteria = new AuditCriteria();
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToAddNewMedAuditCriteriaTypeSelection()");
	}

	/*****************************/
	/****** Response methods *****/
	/*****************************/

	/**
	 * Navigates the user to the Search/Edit Responses panel
	 */
	private void initiateSearchEditResponsesPanel() {
		medicareALRSearchCriteria = new MedicareAuditLetterResponseSearchCriteria();
		displayALRSearchResults = false;

		// Ensure search results table is in a clean state
		resetDataTableState("editAuditLetterRespView:searchEditMedicareALRPanel:medicareALRSearchForm:alrSearchResultsTable");
	}

	/***********************************/
	/****** HS Description methods *****/
	/***********************************/

	/**
	 * Navigates the user to the Search/Edit H.S. Descriptions panel
	 */
	private void initiateSearchEditHsDescriptionsPanel() {
		hscSearchCriteria.resetSearchCriteria();
		// Reset HS description table to default state
		resetDataTableState("editHsDescView:editHsDescriptionsPanel:editHsDescriptionsTableForm:hsDescriptions");
	}

	/**
	 * Saves any changes made to the HealthService Descriptions
	 */
	public void executeHealthServiceDescriptionSave() {

		try {
			// Grab the HealthServiceCriteria records that have been flagged as updated and save changes
			List<HealthServiceCriteria> hscToSave = new ArrayList<HealthServiceCriteria>();
			for (HealthServiceCriteria hsc : healthServiceCriteriaSearchResults) {

				// Check if there is a value change in the description.
				if (hsc.compareDescriptionValues()) {

					// Update the original description
					hsc.setHealthServiceAuditOriginalDesc(hsc.getHealthServiceAuditDesc());

					// Apply edit meta data prior to save
					hsc.setModifiedBy(FacesUtils.getUserId());
					hsc.setLastModified(new Date());
					hscToSave.add(hsc);
				}
			}

			// Submit changes if any
			if (!hscToSave.isEmpty()) {
				medicareService.saveHealthServiceCriteriaDescriptions(hscToSave);
			}

			// De-select all rows and display success message
			deselectAllHealthServiceDescriptions();
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.HS.DESCRIPTIONS.SAVE.SUCCESSFUL");

		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Triggers a prompt for the user to provide a new Description that will be set to all selected HealthServices
	 */
	public void executeHealthServiceDescriptionUpdate() {
		// Quickly check to see if the user actually selected any records
		boolean rowSelected = false;
		for (HealthServiceCriteria hsc : healthServiceCriteriaSearchResults) {
			if (hsc.isSelectedForUpdate()) {
				rowSelected = true;
				break;
			}
		}
		if (rowSelected) {
			bulkUpdateDescription = "";
			displayHealthServiceUpdateWindow = true;
		} else {
			// Display warning to user
			Validator.displaySystemMessage(FacesMessage.SEVERITY_ERROR, "ERROR.MEDICARE.HS.DESC.NO.ROWS.SELECTED");
		}
	}

	/**
	 * Triggered when the user clicks apply on the update prompt window
	 */
	public void confirmHealthServiceDescriptionUpdate() {
		// Go over each selected HealthServiceDescription and apply the provided value
		for (HealthServiceCriteria hsc : healthServiceCriteriaSearchResults) {
			if (hsc.isSelectedForUpdate()) {
				hsc.setHealthServiceAuditDesc(bulkUpdateDescription);
			}
		}
		closeHealthServiceUpdateWindow();
	}

	/**********************************/
	/****** HS Exclusions methods *****/
	/**********************************/

	/**
	 * Navigates the user to the Search/Edit Health Service Exclusions panel
	 */
	private void initiateSearchEditHsExclusionsPanel() {
		try {
			// Reset the search criteria
			hscSearchCriteria.resetSearchCriteria();
			hscExclusionsStateMap.clear();
			// Load HS exclusions
			healthServiceExclusions = medicareService.getHealthServiceCriteriaExclusions();
			// Reset HS Exclusions table to default state
			resetDataTableState("editGlobalHsExclusionsView:editHsExclusionsPanel:healthServiceExclusionsEditForm:healthServiceExclusionsTable");

		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Selects all rows in the HS Exclusion HSC table. If the user has applied filters on the table the rows removed
	 * from view will not be selected
	 */
	public void executeSelectAllHSExclusionHsc() {

		String tableId = "editGlobalHsExclusionsView:hsSearchResultsForm:hsSearchResultsPopup:healthServiceCodeResultsDatatable";
		super.selectAllTableRows(tableId, hscSearchResultsStateMap, healthServiceCriteriaSearchResults);
	}

	/**
	 * De-Selects all rows in the HS Exclusion HSC table. If the user has applied filters on the table the rows removed
	 * from view will not be selected
	 */
	public void executeDeselectAllHSExclusionHsc() {

		String tableId = "editGlobalHsExclusionsView:hsSearchResultsForm:hsSearchResultsPopup:healthServiceCodeResultsDatatable";
		super.unselectAllTableRows(tableId, hscSearchResultsStateMap, healthServiceCriteriaSearchResults);
	}

	/**
	 * Adds the selected HealthServiceCriteria as new HS Exclusions
	 */
	public void addNewHealthServiceExclusions() {

		// Add all the selected HS rows and add them to the exclusions table on the UI
		@SuppressWarnings("unchecked")
		List<HealthServiceCriteria> selectedHSC = hscSearchResultsStateMap.getSelected();

		for (HealthServiceCriteria hsc : selectedHSC) {

			// Quickly check that we are not adding a record already excluded
			boolean recordAlreadyExcluded = false;
			for (HealthServiceCriteria h : healthServiceExclusions) {
				if (hsc.getHealthServiceID().equals(h.getHealthServiceID())) {
					recordAlreadyExcluded = true;
				}
			}

			// Add to top of table if it is not a duplicate
			if (!recordAlreadyExcluded) {
				healthServiceExclusions.add(0, hsc);
			}
		}

		// Clear state map
		hscSearchResultsStateMap.clear();
		this.resetHscSearchCriteriaForm();
		// Close search results popup
		closeHealthServiceCriteriaSearchResults();
	}

	/**
	 * Saves the changes made to Health Service Exclusion records.
	 */
	public void executeHealthServiceExclusionsSave() {
		try {
			List<HealthServiceCriteria> exclusionsToSave = new ArrayList<HealthServiceCriteria>();

			// Go over each Exclusion and check if they were updated
			for (HealthServiceCriteria hsc : healthServiceExclusions) {

				// Compare original values to changeable values. If change is detected this record was updated.
				if ((hsc.compareAgeRestrictionValues() || hsc.compareGenderRestrictionValues())
						|| (hsc.getModifiedBy() == null || hsc.getModifiedBy().isEmpty())) {

					healthServiceExclusionValidator.validate(hsc);

					// Apply edit meta data prior to save
					hsc.setModifiedBy(FacesUtils.getUserId());
					hsc.setLastModified(new Date());
					exclusionsToSave.add(hsc);
				}
			}

			// Submit values to save if any
			if (!exclusionsToSave.isEmpty()) {
				medicareService.saveHealthServiceCriteriaExclusions(exclusionsToSave);

				// Update original values to match possible updated value
				for (HealthServiceCriteria hsc : exclusionsToSave) {
					hsc.setOriginalAgeRestriction(hsc.getAgeRestriction());
					hsc.setOriginalGenderRestriction(hsc.getGenderRestriction());
				}
			}

			// Display success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.HS.EXCLUSIONS.SAVE.SUCCESSFUL");

		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Prompts the user to confirm the delete action. No data change will occur until confirmation is provided
	 */
	public void executeHealthServiceExclusionsDeleteSelected() {

		// Continue only if records were selected. Otherwise display error to user
		if (!hscExclusionsStateMap.getSelected().isEmpty()) {
			// Display confirmation dialog
			displayHealthServiceExclusionDeleteConfirm = true;
		} else {
			// No records selected for delete.. Displaying error
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.MEDICARE.HS.EXCL.NO.ROWS.SELECTED"));
		}
	}

	/**
	 * Triggered once the user confirms the delete action. Data will be removed from system.
	 */
	public void confirmDeleteSelectedHealthServiceExclusions() {
		try {
			// Get selected exclusions for delete
			@SuppressWarnings("unchecked")
			List<HealthServiceCriteria> exclusionsForDelete = hscExclusionsStateMap.getSelected();
			// Attempt delete
			medicareService.deleteHealthServiceCriteriaExclusions(exclusionsForDelete);

			/*
			 * Upon successful delete. Clean up UI view for exclusions. Also remove any records in the add list that
			 * were removed.
			 */
			for (HealthServiceCriteria hsc : exclusionsForDelete) {
				healthServiceExclusions.remove(hsc);
			}

			// Clear exclusions state map and display success message
			hscExclusionsStateMap.clear();

			// Close confirmation dialog
			displayHealthServiceExclusionDeleteConfirm = false;
			closePopupWindowJS();

			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.HS.EXCLUSIONS.DELETE.SUCCESSFUL");

		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Searches for Health Services on the HS Descriptions page
	 */
	public void executeHealthServiceDescriptionSearch() {
		try {
			// Load results and display table
			healthServiceCriteriaSearchResults = medicareService
					.getSearchedHealthServiceCriteriaDescriptions(hscSearchCriteria);

			/**
			 * Display warning at the top of the page stating there was no results if no rows found. prevent page
			 * transition
			 */
			if (healthServiceCriteriaSearchResults.isEmpty()) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_WARN,
						"WARN.MEDICARE.HS.DESC.NO.RECORDS.FOUND"));
			} else {
				JavaScriptRunner.runScript(FacesContext.getCurrentInstance(),
						"displayHealthServiceDescriptionResults()");
			}
		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Returns the user to the HealthService Description search
	 */
	public void executeReturnToHealthServiceDescSearch() {
		healthServiceCriteriaSearchResults.clear();
		hscSearchCriteria.resetSearchCriteria();
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToHealthServiceDescriptionSearch()");
	}

	/**
	 * Selects all of the HealthService Descriptions
	 */
	public void selectAllHealthServiceDescriptions() {
		for (HealthServiceCriteria hsc : healthServiceCriteriaSearchResults) {
			hsc.setSelectedForUpdate(true);
		}
	}

	/**
	 * De-selects all of the HealthService Descriptions
	 */
	public void deselectAllHealthServiceDescriptions() {
		for (HealthServiceCriteria hsc : healthServiceCriteriaSearchResults) {
			hsc.setSelectedForUpdate(false);
		}
	}

	/**********************************/
	/****** Ind Exclusion methods *****/
	/**********************************/

	/**
	 * Navigates the user to the Search/Edit Individual Exclusions panel
	 */
	private void initiateSearchEditIndExclusionsPanel() {
		try {
			// Reset any variables
			newIndividualExclusion = new IndividualExclusion();
			individualExclusions = medicareService.getIndividualExclusions();
			// Reset Exclusions table to default state
			resetDataTableState("editGlobalIndExclusionsView:editIndividualExclusionsPanel:individualExclusionsEditForm:individualExclusionsTable");
		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Saves changed or new Individual Exclusions
	 */
	public void executeIndividualExclusionsSave() {
		try {
			List<IndividualExclusion> exclusionsToSave = new ArrayList<IndividualExclusion>();

			for (IndividualExclusion indExcl : individualExclusions) {

				// Validate that exclusion meets all business rules
				individualExclusionValidator.validate(indExcl);

				// Check if any values changes from original
				if (indExcl.compareEffectiveFromValues() || indExcl.compareEffectiveToValues()) {

					// Set edit meta data
					indExcl.setModifiedBy(FacesUtils.getUserId());
					indExcl.setLastModified(new Date());

					// Add exclusion to list
					exclusionsToSave.add(indExcl);
				}
			}

			// Check for duplicate entries on all Exclusions
			individualExclusionValidator.checkForDuplicateExclusions(individualExclusions);

			// Submit values to save if any
			if (!exclusionsToSave.isEmpty()) {

				// Attempt save
				medicareService.saveIndividualExclusions(exclusionsToSave);

				// Update the original values in saved objects
				for (IndividualExclusion excl : exclusionsToSave) {
					excl.setOriginalEffectiveFrom(excl.getEffectiveFrom());
					excl.setOriginalEffectiveTo(excl.getEffectiveTo());
				}
			}
			// Display success message
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_INFO,
					"INFO.IND.EXCLUSIONS.SAVE.SUCCESSFUL"));

		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Adds a new Individual Exclusion to the UI. Records not saved to system until save is clicked.
	 */
	public void addNewIndividualExclusion() {
		try {
			// Look up name by Health Card Number.
			String healthCardOwnerName = medicareService.getHealthCardNumberOwnerName(newIndividualExclusion
					.getHealthCardNumber());

			if (healthCardOwnerName != null && !healthCardOwnerName.isEmpty()) {
				// Set the name to the new record
				newIndividualExclusion.setName(healthCardOwnerName);
				// Apply modified user and add to table
				newIndividualExclusion.setModifiedBy(FacesUtils.getUserId());
				individualExclusions.add(0, newIndividualExclusion);
				// Reset object to allow addition of more records
				newIndividualExclusion = new IndividualExclusion();

			} else {
				// Display error to user. Health Card Number not in system
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.MEDICARE.IND.EXCL.HEALTH.CARD.NOT.FOUND"));
			}
		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Execute search Medicare audit letter response result search
	 */
	public void executeMedicareALRSearch() {
		logger.debug("executeMedicareALRSearch() : Begin");

		// Ensure results are display.. data model automatically refreshes...
		displayALRSearchResults = true;

		try {
			// Load total row count for search criteria
			totalALRRows = medicareService.getSearchedMedicareALRCount(medicareALRSearchCriteria);
			searchedMedicareALRs.setRowCount(totalALRRows);

			// Reset the count display
			displayNumberOfLettersCreated = false;

			// Ensure search results table is in a clean state
			resetDataTableState("editAuditLetterRespView:searchEditMedicareALRPanel:medicareALRSearchForm:alrSearchResultsTable");

			// Load Number of Letters if Audit Run Number and Audit Type was used in search criteria
			if (medicareALRSearchCriteria.getAuditRunNumber() != null
					&& (medicareALRSearchCriteria.getAuditType() != null && !medicareALRSearchCriteria.getAuditType()
							.isEmpty())) {

				numberOfLettersCreated = medicareService.getNumberOfLettersCreated(
						medicareALRSearchCriteria.getAuditRunNumber(), medicareALRSearchCriteria.getAuditType());

				// Set the count display to true
				displayNumberOfLettersCreated = true;
			}
		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());

		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		logger.debug("executeMedicareALRSearch() : End");
	}

	/**
	 * Loads ALR search result data if it was requested by the user...
	 * 
	 * @return fresh ALR results data model
	 */
	private LazyDataModel<MedicareAuditLetterResponse> performALRSearchLoad() {
		searchedMedicareALRs = new LazyDataModel<MedicareAuditLetterResponse>() {

			/** Serial Number */
			private static final long serialVersionUID = -321591594841978036L;

			@Override
			public List<MedicareAuditLetterResponse> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<MedicareAuditLetterResponse> resultList = new ArrayList<MedicareAuditLetterResponse>();
				try {
					if (displayALRSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								medicareALRSearchCriteria.addSortArgument(MedicareAuditLetterResponse.class,
										sc.getPropertyName(), sc.isAscending());
							}
						}

						// Load Medicare ALR search results
						resultList = medicareService.getSearchedMedicareALR(medicareALRSearchCriteria, first, pageSize);
					}
				} catch (MedicareServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return resultList;
			}
		};
		return searchedMedicareALRs;
	}

	/**
	 * Resets the ALR search form
	 */
	public void executeClearMedicareALRSearch() {
		clearForm();
		medicareALRSearchCriteria.resetSearchCriteria();
		// Hide search results
		displayALRSearchResults = false;
		// Ensure search results table is in a clean state
		resetDataTableState("editAuditLetterRespView:searchEditMedicareALRPanel:medicareALRSearchForm:alrSearchResultsTable");
	}

	/**
	 * Get into a selected medicare audit letter response details
	 * 
	 * @param selectedMCALR
	 */
	public void executeEditSelectedALR(MedicareAuditLetterResponse aSelectedALR) {

		displayALRSearchResults = false;
		this.selectedMCALR = aSelectedALR;
		// Display the result table
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayEditMedicareALRResultDetails()");
	}

	/**
	 * Save Medicare audit letter response details
	 */
	public void executeSaveMedicareALR() {

		logger.debug("executeSaveMedicareALR() : Begin");

		try {
			// Validate the Medicare ALR
			medicareALRValidator.validate(selectedMCALR);

			// update the last modified date and person
			selectedMCALR.setLast_modified(new Date());
			selectedMCALR.setModified_by(FacesUtils.getUserId());

			// Execute save
			medicareService.saveMedicareALR(selectedMCALR);

			// Display save success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.AUDIT.RESULT.VALUE.SAVE.SUCCESSFUL");

		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);

		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());

		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		logger.debug("executeSaveMedicareALR() : End");
	}

	/**
	 * return to search
	 */
	public void executeReturnToALRSearch() {
		displayALRSearchResults = true;
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToMedicareALRSearch()");
	}

	/***************************************************/
	/****** Business Arrangement Exclusion methods *****/
	/***************************************************/
	private void initiateBusinessArrangementExclusionsPanel() {
		try {
			// Ensure UI is in a fresh state
			businessArrangementNumber = null;
			// Load all exclusions
			baExclusions = medicareService.getBusinessArrangementExclusions();
			// Clear state map
			if (baExclusionsStateMap != null) {
				baExclusionsStateMap.clear();
			}

		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Adds a new Business Arrangement Exclusion. BA number is specified by the user.
	 */
	public void executeAddNewBusinessArrangementExclusion() {

		try {
			if (businessArrangementNumber != null) {

				// Perform a look up of the specified BA. If nothing comes back display a warning and cancel save
				BusinessArrangement ba = medicareService.getBusinessArrangement(businessArrangementNumber);

				if (ba != null) {

					// Verify we are not adding an exclusion that exists
					if (!baExclusions.contains(ba)) {

						/*
						 * Check if Renumeration method is of type 5 or 2. If not we will display a warning following
						 * save
						 */
						if (!ba.getRemunerationMethod().equals(Long.valueOf(5))
								&& !ba.getRemunerationMethod().equals(Long.valueOf(2))) {
							Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_WARN,
									"WARN.MEDICARE.BA.EXCL.BA.RENUM.TYPE"));
						}

						// Set the edit meta data
						ba.setLastModified(Calendar.getInstance().getTime());
						ba.setModifiedBy(FacesUtils.getUserId());

						medicareService.saveBusinessArrangementExclusion(ba);
						// Update the exclusions table
						baExclusions = medicareService.getBusinessArrangementExclusions();
						// Clear out BA entry field
						businessArrangementNumber = null;

						// Display success message
						Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_INFO,
								"INFO.BA.EXCLUSIONS.SAVE.SUCCESSFUL"));
					} else {
						// Display duplicate detection error to user
						Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
								"ERROR.MEDICARE.BA.EXCL.BA.NUM.DUPLICATE"));
					}
				} else {
					// BA number not found... display error to user
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.MEDICARE.BA.EXCL.BA.NUM.INVALID"));
				}
			} else {
				// Display error stating required field has no value
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.MEDICARE.BA.EXCL.BA.NUM.REQUIRED"));
			}
		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Deletes the selected Business Arrangement exclusions from the system. If valid selections have been made
	 * confirmation is displayed
	 */
	public void executeDeleteSelectedBusinessArrangementExclusions() {

		// Continue only if records were selected. Otherwise display error to user
		if (!baExclusionsStateMap.getSelected().isEmpty()) {
			// Display confirmation dialog
			displayBAExclusionDeleteConfirm = true;
		} else {
			// No records selected for delete.. Displaying error
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.MEDICARE.BA.EXCL.NO.ROWS.SELECTED"));
		}
	}

	/**
	 * Called when the user confirms the delete action. Performs the deletion of the selected BA Exclusions
	 */
	public void confirmDeleteSelectedBusinessArrangementExclusions() {
		try {
			@SuppressWarnings("unchecked")
			List<BusinessArrangement> exclusionsForDelete = baExclusionsStateMap.getSelected();

			medicareService.deleteBusinessArrangementExclusions(exclusionsForDelete);

			// Following delete remove them from the UI to prevent the need of refresh
			for (BusinessArrangement ba : exclusionsForDelete) {
				baExclusions.remove(ba);
			}

			// Clear state map selection
			baExclusionsStateMap.clear();

			// Close confirmation dialog
			displayBAExclusionDeleteConfirm = false;
			closePopupWindowJS();

			// Display success message
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_INFO,
					"INFO.BA.EXCLUSIONS.DELETE.SUCCESSFUL"));

		} catch (MedicareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
	
	// JTRAX-58 07-FEB-18 BCAINNE
	public void clearHealthServiceCriteria() {
		clearForm();
		this.hscSearchCriteria.resetSearchCriteria();
	}

	/**************************************
	 ***** POP UP CLOSE METHODS BEGIN *****
	 **************************************/
	public void closeHealthServiceCriteriaSearchResults() {
		// Ensure the services table is in default state
		resetDataTableState("searchEditAuditCriteriaView:searchAuditCriteriaPanel:hscResultPopupForm:hscSearchResultPopup:healthServiceCodeSearchDatatable");
		resetDataTableState("editGlobalHsExclusionsView:hsSearchResultsForm:hsSearchResultsPopup:healthServiceCodeResultsDatatable");
		hscSearchResultsStateMap.clear();
		displayHealthServiceCriteriaSearchResults = false;
		closePopupWindowJS();
	}

	public void closeHealthServiceUpdateWindow() {
		displayHealthServiceUpdateWindow = false;
		closePopupWindowJS();
	}

	public void closeHealthServiceDeleteConfirmation() {
		displayHealthServiceExclusionDeleteConfirm = false;
		closePopupWindowJS();
	}

	public void closeBusinessArrangementDeleteConfirmation() {
		displayBAExclusionDeleteConfirm = false;
		closePopupWindowJS();
	}

	/************************************
	 ***** POP UP CLOSE METHODS END *****
	 ************************************/

	/**
	 * @return the auditCriteriaSearchCriteria
	 */
	public AuditCriteriaSearchCriteria getAuditCriteriaSearchCriteria() {
		return auditCriteriaSearchCriteria;
	}

	/**
	 * @param auditCriteriaSearchCriteria
	 *            the auditCriteriaSearchCriteria to set
	 */
	public void setAuditCriteriaSearchCriteria(AuditCriteriaSearchCriteria auditCriteriaSearchCriteria) {
		this.auditCriteriaSearchCriteria = auditCriteriaSearchCriteria;
	}

	/**
	 * @return the selectedAuditCriteria
	 */
	public AuditCriteria getSelectedAuditCriteria() {
		return selectedAuditCriteria;
	}

	/**
	 * @param selectedAuditCriteria
	 *            the selectedAuditCriteria to set
	 */
	public void setSelectedAuditCriteria(AuditCriteria selectedAuditCriteria) {
		this.selectedAuditCriteria = selectedAuditCriteria;
	}

	/**
	 * @return the hscSearchCriteria
	 */
	public HealthServiceCriteriaSearchCriteria getHscSearchCriteria() {
		return hscSearchCriteria;
	}

	/**
	 * @param hscSearchCriteria
	 *            the hscSearchCriteria to set
	 */
	public void setHscSearchCriteria(HealthServiceCriteriaSearchCriteria hscSearchCriteria) {
		this.hscSearchCriteria = hscSearchCriteria;
	}

	/**
	 * @return the healthServiceCriteriaSearchResults
	 */
	public List<HealthServiceCriteria> getHealthServiceCriteriaSearchResults() {
		return healthServiceCriteriaSearchResults;
	}

	/**
	 * @param healthServiceCriteriaSearchResults
	 *            the healthServiceCriteriaSearchResults to set
	 */
	public void setHealthServiceCriteriaSearchResults(List<HealthServiceCriteria> healthServiceCriteriaSearchResults) {
		this.healthServiceCriteriaSearchResults = healthServiceCriteriaSearchResults;
	}

	/**
	 * @return the displayHealthServiceCriteriaSearchResults
	 */
	public boolean isDisplayHealthServiceCriteriaSearchResults() {
		return displayHealthServiceCriteriaSearchResults;
	}

	/**
	 * @param displayHealthServiceCriteriaSearchResults
	 *            the displayHealthServiceCriteriaSearchResults to set
	 */
	public void setDisplayHealthServiceCriteriaSearchResults(boolean displayHealthServiceCriteriaSearchResults) {
		this.displayHealthServiceCriteriaSearchResults = displayHealthServiceCriteriaSearchResults;
	}

	/**
	 * @return the hscSearchResultsStateMap
	 */
	public RowStateMap getHscSearchResultsStateMap() {
		return hscSearchResultsStateMap;
	}

	/**
	 * @param hscSearchResultsStateMap
	 *            the hscSearchResultsStateMap to set
	 */
	public void setHscSearchResultsStateMap(RowStateMap hscSearchResultsStateMap) {
		this.hscSearchResultsStateMap = hscSearchResultsStateMap;
	}

	/**
	 * @return the displayHealthServiceUpdateWindow
	 */
	public boolean isDisplayHealthServiceUpdateWindow() {
		return displayHealthServiceUpdateWindow;
	}

	/**
	 * @param displayHealthServiceUpdateWindow
	 *            the displayHealthServiceUpdateWindow to set
	 */
	public void setDisplayHealthServiceUpdateWindow(boolean displayHealthServiceUpdateWindow) {
		this.displayHealthServiceUpdateWindow = displayHealthServiceUpdateWindow;
	}

	/**
	 * @return the bulkUpdateDescription
	 */
	public String getBulkUpdateDescription() {
		return bulkUpdateDescription;
	}

	/**
	 * @param bulkUpdateDescription
	 *            the bulkUpdateDescription to set
	 */
	public void setBulkUpdateDescription(String bulkUpdateDescription) {
		this.bulkUpdateDescription = bulkUpdateDescription;
	}

	/**
	 * @return the healthServiceExclusions
	 */
	public List<HealthServiceCriteria> getHealthServiceExclusions() {
		return healthServiceExclusions;
	}

	/**
	 * @param healthServiceExclusions
	 *            the healthServiceExclusions to set
	 */
	public void setHealthServiceExclusions(List<HealthServiceCriteria> healthServiceExclusions) {
		this.healthServiceExclusions = healthServiceExclusions;
	}

	/**
	 * @return the hscExclusionsStateMap
	 */
	public RowStateMap getHscExclusionsStateMap() {
		return hscExclusionsStateMap;
	}

	/**
	 * @param hscExclusionsStateMap
	 *            the hscExclusionsStateMap to set
	 */
	public void setHscExclusionsStateMap(RowStateMap hscExclusionsStateMap) {
		this.hscExclusionsStateMap = hscExclusionsStateMap;
	}

	/**
	 * @return the displayHealthServiceExclusionDeleteConfirm
	 */
	public boolean isDisplayHealthServiceExclusionDeleteConfirm() {
		return displayHealthServiceExclusionDeleteConfirm;
	}

	/**
	 * @param displayHealthServiceExclusionDeleteConfirm
	 *            the displayHealthServiceExclusionDeleteConfirm to set
	 */
	public void setDisplayHealthServiceExclusionDeleteConfirm(boolean displayHealthServiceExclusionDeleteConfirm) {
		this.displayHealthServiceExclusionDeleteConfirm = displayHealthServiceExclusionDeleteConfirm;
	}

	/**
	 * @return the individualExclusions
	 */
	public List<IndividualExclusion> getIndividualExclusions() {
		return individualExclusions;
	}

	/**
	 * @param individualExclusions
	 *            the individualExclusions to set
	 */
	public void setIndividualExclusions(List<IndividualExclusion> individualExclusions) {
		this.individualExclusions = individualExclusions;
	}

	/**
	 * @return the newIndividualExclusion
	 */
	public IndividualExclusion getNewIndividualExclusion() {
		return newIndividualExclusion;
	}

	/**
	 * @param newIndividualExclusion
	 *            the newIndividualExclusion to set
	 */
	public void setNewIndividualExclusion(IndividualExclusion newIndividualExclusion) {
		this.newIndividualExclusion = newIndividualExclusion;
	}

	/**
	 * @return the editingPractitionerAuditCriteria
	 */
	public boolean isEditingPractitionerAuditCriteria() {
		return editingPractitionerAuditCriteria;
	}

	/**
	 * @param editingPractitionerAuditCriteria
	 *            the editingPractitionerAuditCriteria to set
	 */
	public void setEditingPractitionerAuditCriteria(boolean editingPractitionerAuditCriteria) {
		this.editingPractitionerAuditCriteria = editingPractitionerAuditCriteria;
	}

	/**
	 * @return the editMode
	 */
	public boolean isEditMode() {
		return editMode;
	}

	/**
	 * @param editMode
	 *            the editMode to set
	 */
	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	/**
	 * @return the selectedPaymentResponsibility
	 */
	public String getSelectedPaymentResponsibility() {
		return selectedPaymentResponsibility;
	}

	/**
	 * @param selectedPaymentResponsibility
	 *            the selectedPaymentResponsibility to set
	 */
	public void setSelectedPaymentResponsibility(String selectedPaymentResponsibility) {
		this.selectedPaymentResponsibility = selectedPaymentResponsibility;
	}

	/**
	 * @return the selectedPayeeType
	 */
	public String getSelectedPayeeType() {
		return selectedPayeeType;
	}

	/**
	 * @param selectedPayeeType
	 *            the selectedPayeeType to set
	 */
	public void setSelectedPayeeType(String selectedPayeeType) {
		this.selectedPayeeType = selectedPayeeType;
	}

	public MedicareAuditLetterResponseSearchCriteria getMedicareALRSearchCriteria() {
		return medicareALRSearchCriteria;
	}

	public void setMedicareALRSearchCriteria(MedicareAuditLetterResponseSearchCriteria medicareALRSearchCriteria) {
		this.medicareALRSearchCriteria = medicareALRSearchCriteria;
	}

	public MedicareAuditLetterResponse getSelectedMCALR() {
		return selectedMCALR;
	}

	public void setSelectedMCALR(MedicareAuditLetterResponse selectedMALR) {
		this.selectedMCALR = selectedMALR;
	}

	/**
	 * @return the numberOfLettersCreated
	 */
	public Long getNumberOfLettersCreated() {
		return numberOfLettersCreated;
	}

	/**
	 * @param numberOfLettersCreated
	 *            the numberOfLettersCreated to set
	 */
	public void setNumberOfLettersCreated(Long numberOfLettersCreated) {
		this.numberOfLettersCreated = numberOfLettersCreated;
	}

	/**
	 * @return the displayNumberOfLettersCreated
	 */
	public boolean isDisplayNumberOfLettersCreated() {
		return displayNumberOfLettersCreated;
	}

	/**
	 * @param displayNumberOfLettersCreated
	 *            the displayNumberOfLettersCreated to set
	 */
	public void setDisplayNumberOfLettersCreated(boolean displayNumberOfLettersCreated) {
		this.displayNumberOfLettersCreated = displayNumberOfLettersCreated;
	}

	/**
	 * @return the baExclusions
	 */
	public List<BusinessArrangement> getBaExclusions() {
		return baExclusions;
	}

	/**
	 * @param baExclusions
	 *            the baExclusions to set
	 */
	public void setBaExclusions(List<BusinessArrangement> baExclusions) {
		this.baExclusions = baExclusions;
	}

	/**
	 * @return the businessArrangementNumber
	 */
	public Long getBusinessArrangementNumber() {
		return businessArrangementNumber;
	}

	/**
	 * @param businessArrangementNumber
	 *            the businessArrangementNumber to set
	 */
	public void setBusinessArrangementNumber(Long businessArrangementNumber) {
		this.businessArrangementNumber = businessArrangementNumber;
	}

	/**
	 * @return the baExclusionsStateMap
	 */
	public RowStateMap getBaExclusionsStateMap() {
		return baExclusionsStateMap;
	}

	/**
	 * @param baExclusionsStateMap
	 *            the baExclusionsStateMap to set
	 */
	public void setBaExclusionsStateMap(RowStateMap baExclusionsStateMap) {
		this.baExclusionsStateMap = baExclusionsStateMap;
	}

	/**
	 * @return the displayBAExclusionDeleteConfirm
	 */
	public Boolean getDisplayBAExclusionDeleteConfirm() {
		return displayBAExclusionDeleteConfirm;
	}

	/**
	 * @param displayBAExclusionDeleteConfirm
	 *            the displayBAExclusionDeleteConfirm to set
	 */
	public void setDisplayBAExclusionDeleteConfirm(Boolean displayBAExclusionDeleteConfirm) {
		this.displayBAExclusionDeleteConfirm = displayBAExclusionDeleteConfirm;
	}

	/**
	 * @return the totalALRRows
	 */
	public Integer getTotalALRRows() {
		return totalALRRows;
	}

	/**
	 * @param totalALRRows
	 *            the totalALRRows to set
	 */
	public void setTotalALRRows(Integer totalALRRows) {
		this.totalALRRows = totalALRRows;
	}

	/**
	 * @return the displayALRSearchResults
	 */
	public boolean isDisplayALRSearchResults() {
		return displayALRSearchResults;
	}

	/**
	 * @param displayALRSearchResults
	 *            the displayALRSearchResults to set
	 */
	public void setDisplayALRSearchResults(boolean displayALRSearchResults) {
		this.displayALRSearchResults = displayALRSearchResults;
	}

	/**
	 * @return the searchedMedicareALR
	 */
	public LazyDataModel<MedicareAuditLetterResponse> getSearchedMedicareALRs() {
		return searchedMedicareALRs;
	}

	/**
	 * @param searchedMedicareALR
	 *            the searchedMedicareALR to set
	 */
	public void setSearchedMedicareALRs(LazyDataModel<MedicareAuditLetterResponse> searchedMedicareALRs) {
		this.searchedMedicareALRs = searchedMedicareALRs;
	}

	/**
	 * @return the auditCriteriaSearchResults
	 */
	public LazyDataModel<AuditCriteria> getAuditCriteriaSearchResults() {
		return auditCriteriaSearchResults;
	}

	/**
	 * @param auditCriteriaSearchResults
	 *            the auditCriteriaSearchResults to set
	 */
	public void setAuditCriteriaSearchResults(LazyDataModel<AuditCriteria> auditCriteriaSearchResults) {
		this.auditCriteriaSearchResults = auditCriteriaSearchResults;
	}

	/**
	 * @return the displayAuditCriteriaSearchResults
	 */
	public Boolean getDisplayAuditCriteriaSearchResults() {
		return displayAuditCriteriaSearchResults;
	}

	/**
	 * @param displayAuditCriteriaSearchResults
	 *            the displayAuditCriteriaSearchResults to set
	 */
	public void setDisplayAuditCriteriaSearchResults(Boolean displayAuditCriteriaSearchResults) {
		this.displayAuditCriteriaSearchResults = displayAuditCriteriaSearchResults;
	}

	/**
	 * @return the totalAuditCriteriaRows
	 */
	public Integer getTotalAuditCriteriaRows() {
		return totalAuditCriteriaRows;
	}

	/**
	 * @param totalAuditCriteriaRows
	 *            the totalAuditCriteriaRows to set
	 */
	public void setTotalAuditCriteriaRows(Integer totalAuditCriteriaRows) {
		this.totalAuditCriteriaRows = totalAuditCriteriaRows;
	}
}
