package ca.medavie.nspp.audit.ui.controller.converter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * Custom converter to convert real numbers to currency values and back again.
 */
@FacesConverter("currencyConverter")
public class CurrencyConverter implements Converter {

	private final String msgBundleLocation = "ca.medavie.nspp.audit.ui.messages";

	/**
	 * Converts dollar amount to a number for the database.
	 * 
	 * @param aContext
	 *            The FacesContext
	 * @param aComponent
	 *            The UIComponent
	 * @param aString
	 *            The String to convert
	 * 
	 * @return Cleaned String
	 */
	public Object getAsObject(FacesContext aContext, UIComponent aComponent, String aString) {

		// Load the current locale
		Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

		try {
			if (aString == null || aString.trim().equals("")) {
				return null;
			}

			aString = aString.replaceAll("[\\$\\,\\s]", "");
			DecimalFormat decimalFormatter = new DecimalFormat("#.00");
			return decimalFormatter.format(new Double(aString));

		} catch (java.lang.NumberFormatException e) {
			ResourceBundle messageBundle = ResourceBundle.getBundle(msgBundleLocation, currentLocale);
			String errorMsg = messageBundle.getString("ERROR.APPLICATION.DECIMAL");
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, errorMsg);
			throw new ConverterException(fm);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
	 * javax.faces.component.UIComponent, java.lang.Object)
	 */
	public String getAsString(FacesContext aContext, UIComponent aComponent, Object anObj) {

		BigDecimal aValue = null;
		// Grab the current locale
		Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

		try {
			// Check to make sure the Object is a String that can be converted to a number.
			// Value needs to be valid number with no symbols.
			String aValueStr = anObj.toString();
			aValue = new BigDecimal(aValueStr);

		} catch (NumberFormatException e) {
			ResourceBundle messageBundle = ResourceBundle.getBundle(msgBundleLocation, currentLocale);
			String errorMsg = messageBundle.getString("ERROR.APPLICATION.DECIMAL");
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, errorMsg);
			throw new ConverterException(fm);
		}

		DecimalFormat formatter = (DecimalFormat) NumberFormat.getCurrencyInstance(currentLocale);
		// Remove the use of the currency Symbol
		DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
		symbols.setCurrencySymbol("");
		formatter.setDecimalFormatSymbols(symbols);
		// Format number to currency format
		return formatter.format(aValue);
	}
}