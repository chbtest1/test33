package ca.medavie.nspp.audit.ui.controller.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.ManualDeposit;

/**
 * Used for validating PeerGroups
 */
public class ManualDepositValidator extends Validator<ManualDeposit> {
	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(ManualDeposit aManualDepositToValidate) throws ValidatorException {
		List<ValidatorMessage> errorMsgs = new ArrayList<ValidatorMessage>();

		if ((aManualDepositToValidate.getPractitionerNumber() == null)
				&& (aManualDepositToValidate.getGroupNumber() == null)) {
			errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.ACCOUNTING.MISSING.PAYOR"));
		}

		if (aManualDepositToValidate.getDepositAmount() != null) {
			if (aManualDepositToValidate.getDepositAmount().compareTo(BigDecimal.ZERO) <= 0) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.ACCOUNTING.DEPOSIT.AMOUNT"));
			}

			// Verify value does not exceed maximum (11,2)
			if (aManualDepositToValidate.getDepositAmount().compareTo(BigDecimal.valueOf(999999999.99)) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.ACCOUNTING.DEPOSIT.AMOUNT.INVALID.MAX"));
			}
		}

		if (!errorMsgs.isEmpty()) {
			throw new ValidatorException(errorMsgs);
		}
	}
}
