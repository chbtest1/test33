package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.Locum;

/**
 * Used for validating Locums
 * 
 * Rules for a valid locum:
 * 
 * 1) Locum cannot cover itself
 * 
 * 2) From date must be before To date
 * 
 * 3) No duplicate Locum objects under HOST/VISITOR (Decided by From/To dates)
 * 
 * 4) Locum objects of the same practitioner cannot have overlapping dates
 */
public class LocumValidator extends Validator<Locum> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.BaseValidator#validate(java.lang.Object)
	 */
	@Override
	public void validate(Locum aParentLocum) throws ValidatorException {

		// Holds all error message created during the validation process
		List<ValidatorMessage> validationMessage = new ArrayList<ValidatorMessage>();

		// Booleans that keep track of what errors have been detected to prevent message duplication
		boolean childConflictDetected = false;
		boolean dateConflictDetected = false;
		boolean dateOverlapDetected = false;
		boolean missingRequiredValues = false;

		// Validate the Locum records found under this Locum record
		for (Locum childLocum : aParentLocum.getLocums()) {

			// Prior to any validation taking place check that the required values are required.. (From/To Dates)
			if (childLocum.getEffectiveFrom() == null || childLocum.getEffectiveTo() == null) {

				if (!missingRequiredValues) {
					// Add error and continue on to next Locum
					ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.LOCUM.REQUIRED.VALUES");
					validationMessage.add(message);
					
				}
				missingRequiredValues = true;
				aParentLocum.getInvalidLocums().add(childLocum);

			} else {
				// Required values present.. continue with deeper validation
				// Determine if we are using the HOST or VISITOR values for child records
				Long childPractNumber;
				String childPractType;

				if (aParentLocum.isLocumHost()) {
					// Dealing with the VISITOR values since the parent is a HOST
					childPractNumber = childLocum.getVisitingPractitionerNumber();
					childPractType = childLocum.getVisitingPractitionerType();
				} else {
					// Dealing with the HOST values since the parent is a VISITOR
					childPractNumber = childLocum.getHostPractitionerNumber();
					childPractType = childLocum.getHostPractitionerType();
				}

				// Check that we are not covering the parent record
				if (childPractNumber.compareTo(aParentLocum.getPractitionerNumber()) == 0
						&& childPractType.equals(aParentLocum.getPractitionerType())) {

					// Only add the error message if this is the first time the error has been detected
					if (!childConflictDetected) {
						// Locum is Child to itself... Add error message for display to user.
						ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
								"ERROR.LOCUM.CHILD.OF.SELF");
						validationMessage.add(message);
						
					}
					childConflictDetected = true;
					aParentLocum.getInvalidLocums().add(childLocum);
				}

				// Only compare dates if they are not the same. (From and To can be the same)
				if (!childLocum.getEffectiveFrom().equals(childLocum.getEffectiveTo())) {
					// Check that the Effective FROM date is always before the TO date
					if (!childLocum.getEffectiveFrom().before(childLocum.getEffectiveTo())
							&& !childLocum.getEffectiveTo().after(childLocum.getEffectiveFrom())) {

						// Only add the error message if this is the first time the error has been detected
						if (!dateConflictDetected) {
							// FROM date is after the TO date... Add error message for display to user
							ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
									"ERROR.LOCUM.INVALID.FROM.DATE");
							validationMessage.add(message);
							
						}
						dateConflictDetected = true;
						aParentLocum.getInvalidLocums().add(childLocum);
					}
				}

				// Finally compare each date range of the current child record. This ensures no date ranges overlap
				Date startDateA = childLocum.getEffectiveFrom();
				Date endDateA = childLocum.getEffectiveTo();

				// List holding all Locum records that have the same PractitionerNumber. Used for comparing Date Ranges
				List<Locum> similarLocums = new ArrayList<Locum>();

				/*
				 * Grab any locum records that have the same Practitioner id as the current Child recording being
				 * validated. Used for comparing date ranges
				 */
				for (Locum x : aParentLocum.getLocums()) {

					if (aParentLocum.isLocumHost()) {
						if (childPractNumber.compareTo(x.getVisitingPractitionerNumber()) == 0) {
							// Ensure we are not grabbing the current child record..
							if (childLocum != x) {
								// Add to list for date range comparision
								similarLocums.add(x);
							}
						}
					} else {
						if (childPractNumber.compareTo(x.getHostPractitionerNumber()) == 0) {
							// Ensure we are not grabbing the current child record..
							if (childLocum != x) {
								// Add to list for date range comparision
								similarLocums.add(x);
							}
						}
					}
				}

				// Perform date compares
				for (Locum x : similarLocums) {
					// Extract required dates
					Date startDateB = x.getEffectiveFrom();
					Date endDateB = x.getEffectiveTo();

					// Only compare if both dates are provided.. We already detected the missing values. No need to
					// display error
					if (startDateB != null && endDateB != null) {
						/*
						 * Check for overlap of date range
						 * 
						 * StartA and EndB can be the same day and vice versa
						 */
						if (((!startDateB.before(startDateA) && !startDateB.after(endDateA)) || (!endDateB
								.before(startDateA) && !endDateB.after(endDateA)))) {

							// Overlap detected
							if (!dateOverlapDetected) {
								// Duplicate Locum record detected... Add error message for display to user
								ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
										"ERROR.LOCUM.DATE.OVERLAP");
								validationMessage.add(message);								
							}
							dateOverlapDetected = true;
							// if it is not root cause
							if(!aParentLocum.getInvalidLocums().contains(x)){
								aParentLocum.getInvalidLocums().add(x);
							}
							
							// if it is not root cause
							if(!aParentLocum.getInvalidLocums().contains(childLocum)){
								aParentLocum.getInvalidLocums().add(childLocum);
							}
							
						}
					}
				}
			}
		}

		// Display messages if there are any
		if (!validationMessage.isEmpty()) {
			throw new ValidatorException(validationMessage);
		}
	}
}
