package ca.medavie.nspp.audit.ui.controller.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.OutlierCriteria;
import ca.medavie.nspp.audit.service.data.OutlierCriteria.Types;

/**
 * Used for validating OutlierCriterias
 */
public class OutlierCriteriaValidator extends Validator<OutlierCriteria> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(OutlierCriteria anOutlierCriteria) throws ValidatorException {
		List<ValidatorMessage> errorMsgs = new ArrayList<ValidatorMessage>();

		// Validate Currency value length
		if (anOutlierCriteria.getType().equals(Types.CURRENCY)) {
			if (anOutlierCriteria.getValue() != null) {
				// Convert the value to bigDecimal since it is of type CURRENCY
				String currencyInput = (String) anOutlierCriteria.getValue();
				currencyInput = currencyInput.replaceAll("[\\$\\,\\s]", "");
				BigDecimal value = new BigDecimal(currencyInput);

				// Check the value meets length requirements (15,2)
				if (value.compareTo(BigDecimal.valueOf(9999999999999.99)) == 1) {
					errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.OUTLIER.CRITERIA.CURRENCY.INVALID.MAX"));
				}
			}
		}

		// Validate Integer value length
		if (anOutlierCriteria.getType().equals(Types.INTEGER)) {
			// Convert the value to integer since it is of type INTEGER
			String integerInput = (String) anOutlierCriteria.getValue();
			integerInput = integerInput.replaceAll("[\\$\\,\\s]", "");
			Integer value = new Integer(integerInput);

			// Check the value meets length requirements (9)
			if (value.compareTo(Integer.valueOf(999999999)) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.PROFILE.CRITERIA.INTEGER.INVALID.MAX"));
			}
		}

		if (!errorMsgs.isEmpty()) {
			throw new ValidatorException(errorMsgs);
		}
	}
}
