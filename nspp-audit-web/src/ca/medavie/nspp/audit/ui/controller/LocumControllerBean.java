package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.RowStateMap;
import org.icefaces.ace.model.table.SortCriteria;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.LocumService;
import ca.medavie.nspp.audit.service.data.Locum;
import ca.medavie.nspp.audit.service.data.LocumSearchCriteria;
import ca.medavie.nspp.audit.service.data.LocumSearchCriteria.LocumTypes;
import ca.medavie.nspp.audit.service.exception.LocumServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.LocumValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for Locum pages
 */
@ManagedBean(name = "locumController")
@ViewScoped
public class LocumControllerBean extends DropdownConstantsBean implements Serializable {

	@EJB
	protected LocumService locumService;

	/** JRE generated */
	private static final long serialVersionUID = 8648238457293493884L;
	/** Logger for class */
	private static final Logger logger = LoggerFactory.getLogger(LocumControllerBean.class);

	// Holds the search criteria for Locums
	private LocumSearchCriteria locumSearchCriteria;
	// Holds the search criteria for adding new Locums to a Locum
	private LocumSearchCriteria locumPractitionerSearchCriteria;

	// Holds the selected (Active) Locum record
	private Locum selectedLocumRecord;

	// List holding search results
	private LazyDataModel<Locum> locumSearchResults;
	private List<Locum> locumPractitionerSearchResults;

	// Control search results display
	private boolean displayLocumSearchResults;

	// Holds search row count
	private Integer totalLocums;

	// State maps for Selectable DataTables
	private RowStateMap locumPractitionerSearchResultsStateMap;
	private RowStateMap locumsInLocumStateMap;

	// Booleans that control the display of pop ups
	private boolean displayPractitionerSearchResultsPopup;
	private boolean displayDeletePractitionersPopup;
	private boolean displayInvalidLocums;

	// Validator
	private LocumValidator locumValidator;

	/**
	 * Controller constructor
	 */
	public LocumControllerBean() {

		// Connect to EJB service
		if (this.locumService == null) {
			try {
				this.locumService = InitialContext.doLookup(prop.getProperty(LocumService.class.getName()));
				logger.debug("inquiryService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Instantiate validator
		locumValidator = new LocumValidator();

		// Instantiate objects/variables
		locumSearchCriteria = new LocumSearchCriteria();
		locumPractitionerSearchCriteria = new LocumSearchCriteria();
		selectedLocumRecord = new Locum();

		// Init Search edit panel since it is displayed on load
		initiateSearchEditLocumHostPanel();

		// Sync search data model with bean
		locumSearchResults = performLocumSearchLoad();
	}

	/**
	 * Sets up the Search/Edit Locum Host panel
	 */
	private void initiateSearchEditLocumHostPanel() {
		// Instantiate objects/variables
		locumSearchCriteria = new LocumSearchCriteria();
		locumPractitionerSearchCriteria = new LocumSearchCriteria();
		displayLocumSearchResults = false;
		selectedLocumRecord = new Locum();
	}

	/**
	 * Search for Locum Hosts using the provided search criteria
	 */
	public void executeLocumSearch() {
		logger.debug("executeLocumHostSearch() : Begin");

		// Execute search
		try {

			// Perform search row count
			totalLocums = locumService.getSearchedLocumsCount(locumSearchCriteria);
			locumSearchResults.setRowCount(totalLocums);

			// Ensure data table is in a clean state
			resetDataTableState("editLocumsView:searchEditLocumPanel:locumHostsSearchForm:locumSearchResultsTable");

			// Display search results
			displayLocumSearchResults = true;
		}
		// catch service exceptions
		catch (LocumServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		logger.debug("executeLocumHostSearch() : End");
	}

	/**
	 * Loads the matching Locum records based on the search criteria
	 * 
	 * @return Locum records matching the user search criteria
	 */
	private LazyDataModel<Locum> performLocumSearchLoad() {
		locumSearchResults = new LazyDataModel<Locum>() {

			/** IDE generated */
			private static final long serialVersionUID = 7015687124716425488L;

			@Override
			public List<Locum> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<Locum> results = new ArrayList<Locum>();

				try {
					if (displayLocumSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								locumSearchCriteria
										.addSortArgument(Locum.class, sc.getPropertyName(), sc.isAscending());
							}
						}

						// Load record matching search results
						results = locumService.getSearchedLocums(locumSearchCriteria, first, pageSize);
					}
				} catch (LocumServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return locumSearchResults;
	}

	/**
	 * Clears out the Locum search form
	 */
	public void executeClearLocumSearch() {
		clearForm();
		locumSearchCriteria.resetSearchCriteria();
		displayLocumSearchResults = false;
	}

	/**
	 * Search for Practitioners to add to the selected Locum (HOST or VISITOR) practitioners
	 */
	public void executePractitionerSearch() {
		logger.debug("executePractitionerSearch() : Begin");

		// Execute search
		try {
			locumPractitionerSearchResults = locumService
					.getSearchedLocumPractitioners(locumPractitionerSearchCriteria);
			// Display the search results
			displayPractitionerSearchResultsPopup = true;
		}
		// catch service exceptions
		catch (LocumServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		logger.debug("executePractitionerSearch() : End");
	}

	/**
	 * Returns the user to the search form for Practitioner Locums
	 */
	public void executeReturnToLocumSearch() {
		locumSearchCriteria.resetSearchCriteria();
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToLocumSearch()");
	}

	/**
	 * Enables the user to edit the selected Locum record from the search results
	 * 
	 * @param aLocum
	 *            - Locum record that will be edited
	 */
	public void editSelectedLocum(Locum aLocum) {
		// Clear out state maps
		locumPractitionerSearchResultsStateMap.clear();

		// Reset Locum table to default state
		resetDataTableState("editLocumsView:searchEditLocumPanel:locumEditForm:locumDataTable");

		// Set the Locum type to be what was specified during search
		if (locumSearchCriteria.getLocumType().equals(LocumTypes.HOST)) {
			aLocum.setLocumHost(true);
			aLocum.setLocumVisitor(false);
		} else {
			aLocum.setLocumVisitor(true);
			aLocum.setLocumHost(false);
		}
		// Load the remaining details of the selected Locum for display to user
		try {
			selectedLocumRecord = locumService.getLocumDetails(aLocum);
			// Reset the Practitioner search form
			locumPractitionerSearchCriteria.resetSearchCriteria();
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayLocumDetails()");
		}
		// catch service exceptions
		catch (LocumServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Listens for edits to locums and then stores them in a set. These are the locums that have changes and should be
	 * saved/updated.
	 * 
	 * @param locum
	 */
	public void editted(Locum locum) {
		locum.setMarkedForEdit(true);
	}

	/**
	 * Saves all changes made to the currently selected Locum record
	 */
	public void saveSelectedLocum() {
		try {
			boolean changeMade = false;

			// Validate the object..
			// get a new invalid list
			if (selectedLocumRecord.getInvalidLocums().size() > 0) {
				selectedLocumRecord.getInvalidLocums().clear();
			}
			locumValidator.validate(selectedLocumRecord);

			for (Locum l : selectedLocumRecord.getLocums()) {
				if (l.isMarkedForEdit()) {
					l.setModifiedBy(FacesUtils.getUserId());
					l.setLastModified(new Date());
					changeMade = true;
					l.setMarkedForEdit(false); // Modified by user, update audit info and save, now clean bit.
				}
			}
			if (changeMade) {
				selectedLocumRecord.setModifiedBy(FacesUtils.getUserId());
				selectedLocumRecord.setLastModified(new Date());
				try {
					// Save the record
					locumService.saveLocum(selectedLocumRecord);
					// Save successful
					Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.LOCUM.SAVE.SUCCESSFUL");
					// close the pop up window
					displayInvalidLocums = false;
					selectedLocumRecord.resetMarkedForFlags();
					closePopupWindowJS();
				}
				// catch service exceptions
				catch (LocumServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// catch all other system errors
				catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
			}
		}
		// catch validation exceptions
		catch (ValidatorException e) {
			// show up invalid pop up window
			displayInvalidLocums = true;
			Validator.displayValidationErrors(e);
		}
	}

	/**
	 * Adds the Practitioners selected from search to the Locum Record. Type is determined by the selected Locum record.
	 * 
	 * If current Locum is of type HOST we are adding VISITOR Practitioners. If current Locum is of type VISITOR we are
	 * adding HOST Practitioners
	 */
	public void addSelectedPractitionersToLocum() {

		boolean typeMismatchDetected = false;

		// Add selected Practitioners to the Locum record
		for (Object o : locumPractitionerSearchResultsStateMap.getSelected()) {

			// Add each Locum to the selected Locum record
			Locum locum = (Locum) o;
			locum.setMarkedForAdd(true);

			// If the Subcategory is different than than the Parent locum display warning
			if (!typeMismatchDetected) {
				if (!selectedLocumRecord.getPractitionerType().equals(locum.getPractitionerType())) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_WARN,
							"WARN.LOCUM.TYPE.MISMATCH"));

					// Prevents the Warning from being displayed more than once.
					typeMismatchDetected = true;
				}
			}

			// Set the appropriate HOST/VISITOR values
			if (selectedLocumRecord.isLocumHost()) {
				locum.setHostPractitionerNumber(selectedLocumRecord.getPractitionerNumber());
				locum.setHostPractitionerType(selectedLocumRecord.getPractitionerType());
				locum.setVisitingPractitionerNumber(locum.getPractitionerNumber());
				locum.setVisitingPractitionerType(locum.getPractitionerType());
			} else {
				locum.setHostPractitionerNumber(locum.getPractitionerNumber());
				locum.setHostPractitionerType(locum.getPractitionerType());
				locum.setVisitingPractitionerNumber(selectedLocumRecord.getPractitionerNumber());
				locum.setVisitingPractitionerType(selectedLocumRecord.getPractitionerType());
			}

			// Default From date is current sysdate
			locum.setEffectiveFrom(new Date());
			selectedLocumRecord.getLocums().add(0, locum);
		}

		// Clear out state map
		locumPractitionerSearchResultsStateMap.clear();

		// Close the results pop up
		displayPractitionerSearchResultsPopup = false;
		closePopupWindowJS();
	}

	/**
	 * Triggered when the user clicks delete selected
	 */
	public void deleteLocumsFromSelectedLocum() {
		// Only display confirmation if rows are selected
		if (locumsInLocumStateMap.getSelected().isEmpty()) {
			// Display error to user
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.NO.PRACTITIONERS.SELECTED.DELETE"));
		} else {
			// Display delete confirmation
			displayDeletePractitionersPopup = true;
		}
	}

	/**
	 * Removes the specified Locum record from the currently selected Locum object
	 * 
	 * @param aLocumToRemove
	 */
	public void confirmDeleteLocumsFromSelectedLocum() {
		try {
			@SuppressWarnings("unchecked")
			List<Locum> locumsForDelete = locumsInLocumStateMap.getSelected();

			// Pass the list of Locums and delete them from the system.
			locumService.deleteLocums(locumsForDelete);

			// Remove the Locum(s) from the selected Locum list (UI)
			for (Locum l : locumsForDelete) {
				selectedLocumRecord.getLocums().remove(l);
			}

			// Display success message and close confirmation window
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_INFO,
					"INFO.LOCUM.DELETE.SUCCESSFUL"));
			closeDeletePractitionersPopup();

		} catch (LocumServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Closes the Locum Hosts search results pop up
	 */
	public void closeInvalidLocumsResultsPopup() {
		displayInvalidLocums = false;
	}

	/**
	 * Closes the Practitioner search results pop up
	 */
	public void closePractitionerSearchResultsPopup() {
		displayPractitionerSearchResultsPopup = false;
	}

	/**
	 * Closes the delete Practitioner(s) from locum popup
	 */
	public void closeDeletePractitionersPopup() {
		displayDeletePractitionersPopup = false;
		closePopupWindowJS();
	}

	/**
	 * @return the locumSearchCriteria
	 */
	public LocumSearchCriteria getLocumSearchCriteria() {
		return locumSearchCriteria;
	}

	/**
	 * @param locumSearchCriteria
	 *            the locumSearchCriteria to set
	 */
	public void setLocumSearchCriteria(LocumSearchCriteria locumSearchCriteria) {
		this.locumSearchCriteria = locumSearchCriteria;
	}

	/**
	 * @return the locumPractitionerSearchCriteria
	 */
	public LocumSearchCriteria getLocumPractitionerSearchCriteria() {
		return locumPractitionerSearchCriteria;
	}

	/**
	 * @param locumPractitionerSearchCriteria
	 *            the locumPractitionerSearchCriteria to set
	 */
	public void setLocumPractitionerSearchCriteria(LocumSearchCriteria locumPractitionerSearchCriteria) {
		this.locumPractitionerSearchCriteria = locumPractitionerSearchCriteria;
	}

	/**
	 * @return declared enums in LocumSearchCriteria for Locum Type dropdown options
	 */
	public LocumTypes[] getLocumTypes() {
		return LocumTypes.values();
	}

	/**
	 * @return the selectedLocumRecord
	 */
	public Locum getSelectedLocumRecord() {
		return selectedLocumRecord;
	}

	/**
	 * @param selectedLocumRecord
	 *            the selectedLocumRecord to set
	 */
	public void setSelectedLocumRecord(Locum selectedLocumRecord) {
		this.selectedLocumRecord = selectedLocumRecord;
	}

	/**
	 * @return the displayPractitionerSearchResultsPopup
	 */
	public boolean isDisplayPractitionerSearchResultsPopup() {
		return displayPractitionerSearchResultsPopup;
	}

	/**
	 * @param displayPractitionerSearchResultsPopup
	 *            the displayPractitionerSearchResultsPopup to set
	 */
	public void setDisplayPractitionerSearchResultsPopup(boolean displayPractitionerSearchResultsPopup) {
		this.displayPractitionerSearchResultsPopup = displayPractitionerSearchResultsPopup;
	}

	/**
	 * @return the locumPractitionerSearchResults
	 */
	public List<Locum> getLocumPractitionerSearchResults() {
		return locumPractitionerSearchResults;
	}

	/**
	 * @param locumPractitionerSearchResults
	 *            the locumPractitionerSearchResults to set
	 */
	public void setLocumPractitionerSearchResults(List<Locum> locumPractitionerSearchResults) {
		this.locumPractitionerSearchResults = locumPractitionerSearchResults;
	}

	/**
	 * @return the locumPractitionerSearchResultsStateMap
	 */
	public RowStateMap getLocumPractitionerSearchResultsStateMap() {
		return locumPractitionerSearchResultsStateMap;
	}

	/**
	 * @param locumPractitionerSearchResultsStateMap
	 *            the locumPractitionerSearchResultsStateMap to set
	 */
	public void setLocumPractitionerSearchResultsStateMap(RowStateMap locumPractitionerSearchResultsStateMap) {
		this.locumPractitionerSearchResultsStateMap = locumPractitionerSearchResultsStateMap;
	}

	/**
	 * @return the displayDeletePractitionersPopup
	 */
	public boolean isDisplayDeletePractitionersPopup() {
		return displayDeletePractitionersPopup;
	}

	/**
	 * @param displayDeletePractitionersPopup
	 *            the displayDeletePractitionersPopup to set
	 */
	public void setDisplayDeletePractitionersPopup(boolean displayDeletePractitionersPopup) {
		this.displayDeletePractitionersPopup = displayDeletePractitionersPopup;
	}

	/**
	 * @return the locumsInLocumStateMap
	 */
	public RowStateMap getLocumsInLocumStateMap() {
		return locumsInLocumStateMap;
	}

	/**
	 * @param locumsInLocumStateMap
	 *            the locumsInLocumStateMap to set
	 */
	public void setLocumsInLocumStateMap(RowStateMap locumsInLocumStateMap) {
		this.locumsInLocumStateMap = locumsInLocumStateMap;
	}

	public boolean isDisplayInvalidLocums() {
		return displayInvalidLocums;
	}

	public void setDisplayInvalidLocums(boolean displayInvalidLocums) {
		this.displayInvalidLocums = displayInvalidLocums;
	}

	/**
	 * @return the locumSearchResults
	 */
	public LazyDataModel<Locum> getLocumSearchResults() {
		return locumSearchResults;
	}

	/**
	 * @param locumSearchResults
	 *            the locumSearchResults to set
	 */
	public void setLocumSearchResults(LazyDataModel<Locum> locumSearchResults) {
		this.locumSearchResults = locumSearchResults;
	}

	/**
	 * @return the displayLocumSearchResults
	 */
	public boolean isDisplayLocumSearchResults() {
		return displayLocumSearchResults;
	}

	/**
	 * @param displayLocumSearchResults
	 *            the displayLocumSearchResults to set
	 */
	public void setDisplayLocumSearchResults(boolean displayLocumSearchResults) {
		this.displayLocumSearchResults = displayLocumSearchResults;
	}

	/**
	 * @return the totalLocums
	 */
	public Integer getTotalLocums() {
		return totalLocums;
	}

	/**
	 * @param totalLocums
	 *            the totalLocums to set
	 */
	public void setTotalLocums(Integer totalLocums) {
		this.totalLocums = totalLocums;
	}
}
