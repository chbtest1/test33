/**
 * 
 */
package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bccmatt
 * 
 */
public class ValidatorException extends Exception {

	/** IDE generated */
	private static final long serialVersionUID = -1534469247010169704L;
	/** Holds messages created during the validation process */
	List<ValidatorMessage> messages = null;

	/**
	 * Default Constructor
	 */
	public ValidatorException() {
	}

	/**
	 * Constructor with message parameter
	 * 
	 * @param message
	 */
	public ValidatorException(String message) {
		super(message);
	}

	/**
	 * Constructor with throwable parameter
	 * 
	 * @param cause
	 */
	public ValidatorException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor for message and throwable parameters
	 * 
	 * @param message
	 * @param cause
	 */
	public ValidatorException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor taking a list of messages
	 * 
	 * @param messages
	 */
	public ValidatorException(List<ValidatorMessage> messages) {
		super();
		this.messages = messages;
	}

	/**
	 * @return list of validation messages
	 */
	public List<ValidatorMessage> getMessages() {
		if (this.messages == null) {
			this.messages = new ArrayList<ValidatorMessage>();
		}
		return messages;
	}
}
