package ca.medavie.nspp.audit.ui.controller.validator;

import javax.faces.application.FacesMessage.Severity;

public class ValidatorMessage {
	private Severity severity;
	private String code = null;
	
	public ValidatorMessage(Severity severity, String code) {
		super();
		this.severity = severity;
		this.code = code;
	}

	public ValidatorMessage() {
		super();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Severity getSeverity() {
		return severity;
	}

	public void setSeverity(Severity severity) {
		this.severity = severity;
	}	
}
