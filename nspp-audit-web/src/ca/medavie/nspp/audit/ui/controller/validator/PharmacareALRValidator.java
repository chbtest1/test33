package ca.medavie.nspp.audit.ui.controller.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.PharmacareAuditLetterResponse;

public class PharmacareALRValidator extends Validator<PharmacareAuditLetterResponse> {
	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(PharmacareAuditLetterResponse aPharmacareALR) throws ValidatorException {
		List<ValidatorMessage> errorMsgs = new ArrayList<ValidatorMessage>();

		// Validate the value length of Charge Back amount (10,2)
		if (aPharmacareALR.getChargeback_amount() != null) {
			if (aPharmacareALR.getChargeback_amount().compareTo(BigDecimal.valueOf(99999999.99)) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.CHARGE.BACK.INVALID.MAX"));
			}
		}

		// Validate the value length of Number of Claims Affected (7)
		if (aPharmacareALR.getNumber_of_claims_affected() != null) {
			if (aPharmacareALR.getNumber_of_claims_affected().compareTo(Integer.valueOf(9999999)) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.NUMBER.AUDITS.INVALID.MAX"));
			}
		}

		if (!errorMsgs.isEmpty()) {
			throw new ValidatorException(errorMsgs);
		}
	}
}
