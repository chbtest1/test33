package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.MedicareService;
import ca.medavie.nspp.audit.service.data.AuditCriteria;
import ca.medavie.nspp.audit.service.exception.MedicareServiceException;

public class DenticareAuditCriteriaValidator extends Validator<AuditCriteria> {

	@EJB
	protected MedicareService medicareService;

	/**
	 * Since we need the EJB service for parts of the validation we pass it on construction of Validator class
	 * 
	 * @param aMedicareService
	 */
	public DenticareAuditCriteriaValidator(MedicareService aMedicareService) {
		this.medicareService = aMedicareService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(AuditCriteria t) throws ValidatorException {
		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		/**
		 * ----------------------------------------------------
		 * 
		 * Common Audit rules (Applies to both Audit types)
		 * 
		 * ----------------------------------------------------
		 * 
		 * 1) Number of Services to Audit is a required value
		 * 
		 * 2) If Audit From date is provided it must be less than or equal to the Audit To date if provided.
		 * 
		 * 3) If Payment From date is provided t must be less than or equal to Payment To date if provided
		 * 
		 * 4) If Audit Payment To or Audit To dates are provided the associated From date is required.
		 * 
		 * 5) Both Payment and Audit Dates cannot be provided at the same time. Only one or the other can be entered
		 * 
		 * 6) If 'Audit Latest Payment Date' indicator is set to N, either the Payment dates or the Audit dates are
		 * required.
		 * 
		 * 7) If 'Audit Latest Payment Date' indicator is set to Y, neither the Payment dates or the Audit dates can be
		 * provided.
		 * 
		 */

		// Check if Services To Audit is provided
		if (t.getNumberOfServicesToAudit() == null) {
			validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.AUDIT.CRIT.SERVICES.TO.AUDIT.REQUIRED"));
		} else {
			// Validate the Services To Audit value length (7)
			if (t.getNumberOfServicesToAudit().compareTo(Long.valueOf(9999999)) == 1) {
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.NUMBER.AUDITS.INVALID.MAX"));
			}
		}

		// Check that Audit From and To are valid values.
		if (t.getAuditTo() != null) {
			if (t.getAuditFrom() == null) {
				// No Audit From date provided.. Error
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.AUDIT.FROM.REQUIRED"));
			} else {
				// Check if Audit From is before Audit To (From and To can be the same value)
				if (!t.getAuditFrom().equals(t.getAuditTo()) && t.getAuditFrom().after(t.getAuditTo())) {
					// Audit From is after Audit To. Error
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRIT.AUDIT.FROM.INVALID"));
				}
			}
		}

		// Check that Audit Payment From and To are valid values.
		if (t.getToPaymentDate() != null) {
			if (t.getFromPaymentDate() == null) {
				// No Audit Payment From date provided.. Error
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.AUDIT.PAY.FROM.REQUIRED"));
			} else {
				// Check if Audit Payment From date is before Audit Payment To (From and To can be the same value)
				if (!t.getToPaymentDate().equals(t.getFromPaymentDate())
						&& t.getFromPaymentDate().after(t.getToPaymentDate())) {
					// Audit Payment From is after Audit Payment To.. Error
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRIT.AUDIT.PAY.FROM.INVALID"));
				}
			}
		}

		// Check that only Payment or Audit dates are provided. Both cannot exist
		if (t.getAuditFrom() != null && t.getFromPaymentDate() != null) {
			validatorMsgs
					.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.AUDIT.CRIT.BOTH.DATES.PROVIDED"));
		}

		if (!t.isAuditFromLastIndicator()) {
			// Either Payment or Audit dates are required. (Not both, only one or the other)
			if (t.getAuditFrom() == null && t.getFromPaymentDate() == null) {
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.AUDIT.DATES.REQUIRED"));
			}
		} else {
			// Neither Payment or Audit dates can be provided
			if (t.getAuditFrom() != null || t.getFromPaymentDate() != null) {
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.CRIT.AUDIT.LAST.AND.DATE.PROVIDED"));
			}
		}

		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}

	/**
	 * Validate that the provide Practitioner ID/Type exists (Also loads the name if valid into the Audit Criteria)
	 * 
	 * If Practitioner is valid we then validate that an audit of the same type does not already exist for this
	 * practitioner
	 * 
	 * @param t
	 * @throws ValidatorException
	 */
	public void validateNewCriteriaParameters(AuditCriteria t) throws ValidatorException {
		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		try {
			// Check that an Audit Type was provided. If no type is provided do nothing and display error
			if (t.getAuditType() == null || t.getAuditType().trim().isEmpty()) {
				// Audit Type not provided. Add error
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.MEDICARE.AUDIT.TYPE.REQUIRED"));
			} else {
				// Perform a lookup of the provider. Gets the name if the provider is valid
				String practitionerName = medicareService.getPractitionerName(t.getProviderNumber(),
						t.getProviderType());

				if (practitionerName == null || practitionerName.isEmpty()) {
					// Add error message stating the Practitioner does not exist.
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.CRITERIA.INVALID.PRACTITIONER"));
				} else {
					// Set the Practitioner name to the Audit Criteria object
					t.setProviderName(practitionerName);

					// Now check that this Audit does not already exist in the system
					AuditCriteria duplicateAuditCriteria = medicareService.getMedicarePractitionerAuditCriteria(
							t.getProviderNumber(), t.getProviderType(), t.getAuditType());

					if (duplicateAuditCriteria != null) {
						// Audit already exists. Add error
						validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
								"ERROR.AUDIT.CRITERIA.PRACTITIONER.DUPLICATE"));
					}
				}
			}
		} catch (MedicareServiceException e) {
			ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.DENTICARE.SERVICE.EXCEPTION");
			validatorMsgs.add(message);
			e.printStackTrace();
		}

		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}

	/**
	 * Simply checks if we need to display a warning to user regarding Payment dates. User can still save with these
	 * warnings displayed
	 * 
	 * @param t
	 * @return list of ValidatorMessages of type WARNING
	 */
	public List<ValidatorMessage> checkDefaultAuditPaymentDates(AuditCriteria t) {
		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		Calendar cal = Calendar.getInstance();
		// If the Payment From date is provided check if it is the first day of the month
		if (t.getFromPaymentDate() != null) {

			cal.setTime(t.getFromPaymentDate());
			int paymentFromDay = cal.get(Calendar.DAY_OF_MONTH);
			if (paymentFromDay != 1) {
				// Display warning
				validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_WARN,
						"WARN.AUDIT.CRIT.PAYMENT.FROM.NOT.FIRST"));
			}

			// If the Payment To date is provided check if it is the last day of the month
			if (t.getAuditToPaymentDate() != null) {

				cal.setTime(t.getAuditToPaymentDate());
				int paymentToDay = cal.get(Calendar.DAY_OF_MONTH);

				// Get the last day of the specified month
				int lastDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

				if (paymentToDay != lastDayOfMonth) {
					// Display warning
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_WARN,
							"WARN.AUDIT.CRIT.PAYMENT.TO.NOT.LAST"));
				}
			}
		}
		return validatorMsgs;
	}
}
