package ca.medavie.nspp.audit.ui.controller.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.AuditResultService;
import ca.medavie.nspp.audit.service.data.AuditResult;
import ca.medavie.nspp.audit.service.data.AuditResultNote;
import ca.medavie.nspp.audit.service.exception.AuditResultServiceException;

public class AuditResultValidator extends Validator<AuditResult> {
	
	private static List<String> PROVIDER_TYPE_EXCLUSIONS  = Arrays.asList("DE", "DP", "RX","MW");

	@EJB
	protected AuditResultService auditResultService;

	/**
	 * Since we need the EJB service for parts of the validation we pass it on construction of Validator class
	 * 
	 * @param auditResultService
	 */
	public AuditResultValidator(AuditResultService auditResultService) {
		this.auditResultService = auditResultService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(AuditResult anAuditResult) throws ValidatorException {
		List<ValidatorMessage> errorMsgs = new ArrayList<ValidatorMessage>();

		// Since time is set on the Audit date, we must make a copy and compare without time..
		Calendar cal = Calendar.getInstance();
		cal.setTime(anAuditResult.getAuditDate());
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 0, 0, 0);
		// Get the modified AuditDate for compare to Final Contact Date
		Date auditDateWithoutTime = cal.getTime();

		// The Audit Date must be less than or equal to the Final Contact Date
		if (auditDateWithoutTime.after(anAuditResult.getFinalContactDate())) {

			errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.AUDIT.RESULT.AUDIT.DATE.EXCEPTION"));
		}

		// The Interim date must be less than the Final Contact Date.
		if (anAuditResult.getInterimDate() != null) {

			// Only validate the dates if the user is not using the default values
			if (anAuditResult.getInterimDate().compareTo(new GregorianCalendar(2999, 0, 1).getTime()) != 0
					&& anAuditResult.getFinalContactDate().compareTo(new GregorianCalendar(2999, 0, 1).getTime()) != 0) {

				if (anAuditResult.getInterimDate().compareTo(anAuditResult.getFinalContactDate()) >= 0) {

					errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.RESULT.INTRIM.DATE.EXCEPTION"));
				}
			}
		} else {
			// If record is not of type RX the Interim date is required.
			if (!anAuditResult.getProviderType().equals("RX")) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.AUDIT.INTERIM.DATE.REQUIRED"));
			}
		}

		// If both are populated, the end date can be equal to or greater than the start date.
		if (anAuditResult.getAuditStartDate() != null && anAuditResult.getAuditEndDate() != null) {
			if (anAuditResult.getAuditStartDate().compareTo(anAuditResult.getAuditEndDate()) > 0) {

				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.AUDIT.START.END.DATE.EXCEPTION"));
			}
		}

		// If both are populated, the start and end dates must both be less than the interim date.
		if (anAuditResult.getAuditStartDate() != null && anAuditResult.getAuditEndDate() != null
				&& anAuditResult.getInterimDate() != null) {
			if (anAuditResult.getAuditStartDate().compareTo(anAuditResult.getInterimDate()) >= 0
					|| anAuditResult.getAuditEndDate().compareTo(anAuditResult.getInterimDate()) >= 0) {

				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.AUDIT.START.END.INTRIM.DATE.EXCEPTION"));
			}
		}

		// If both are populated, the interim date must be greater than or equal to the audit date
		if (auditDateWithoutTime != null && anAuditResult.getInterimDate() != null) {
			if (anAuditResult.getInterimDate().compareTo(auditDateWithoutTime) < 0) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.AUDIT.INTERIM.AUDIT.DATE"));
			}
		}

		// If both are populated, the start and end dates must both be less than the Final Contact date.
		if (anAuditResult.getAuditStartDate() != null && anAuditResult.getAuditEndDate() != null
				&& anAuditResult.getFinalContactDate() != null) {
			if (anAuditResult.getAuditStartDate().compareTo(anAuditResult.getFinalContactDate()) >= 0
					|| anAuditResult.getAuditEndDate().compareTo(
							anAuditResult.getFinalContactDate()) >= 0) {

				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.AUDIT.START.END.FINAL.CONTACT.DATE.EXCEPTION"));
			}
		}

		// Validate Audit Recovery Amount value size (10,2)
		if (anAuditResult.getAuditRecoveryAmount() != null) {
			if (anAuditResult.getAuditRecoveryAmount().compareTo(BigDecimal.valueOf(99999999.99)) == 1) {

				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.RECOVERY.AMT.INVALID.MAX"));
			}
		}

		// Validate Number of Services Audited value size (7)
		if (anAuditResult.getNumberOfServicesAudited() != null) {
			if (anAuditResult.getNumberOfServicesAudited().compareTo(Integer.valueOf(9999999)) == 1) {

				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.SERVICES.AUDITED.INVALID.MAX"));
			}
		}

		// Validate Number of Inappropriately Billed values size (7)
		if (anAuditResult.getInappropriatlyBilledServices() != null) {
			if (anAuditResult.getInappropriatlyBilledServices().compareTo(Integer.valueOf(9999999)) == 1) {

				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.INAPP.BILLED.INVALID.MAX"));
			}
		}
		
		// Validate adding a audit record with empty note which is not allowed
				if (!anAuditResult.getNotes().isEmpty()) {
					
					List<AuditResultNote> notes = anAuditResult.getNotes();
					
					for(AuditResultNote note : notes){						
						if(note.getNote() == null || note.getNote().isEmpty()){							
							errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
									"ERROR.AUDIT.RESULT.AUDIT.NOTE.EMPTY"));						
							break;
						}						
					}
				}
		

		// Validate Health Service Code (HSC)
		if (anAuditResult.getHealthServiceCode() != null && !anAuditResult.getHealthServiceCode().isEmpty() && 
				!PROVIDER_TYPE_EXCLUSIONS.contains(anAuditResult.getProviderType())) {
			try {
				
				List<String> validHealthServiceCodes = auditResultService.getHealthServiceCodes();
				List<String> trimedValidHealthServiceCodes = new ArrayList<String>();
				// trim space regarding http://issues.medavie.bluecross.ca:8080/browse/JTRAX-44
				for(String vs : validHealthServiceCodes){					
					trimedValidHealthServiceCodes.add(vs.trim());
				}

				if (!trimedValidHealthServiceCodes.contains(anAuditResult.getHealthServiceCode().trim()) ) {

					errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.AUDIT.RESULT.INVALID.HEALTH.SERVICE.CODE"));
				}
			} catch (AuditResultServiceException e) {
				ValidatorMessage message = new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.AUDIT.RESULT.SERVICE.EXCEPTION");
				errorMsgs.add(message);
				e.printStackTrace();
			}
		}

		// Throw Validator exception if errors were found
		if (!errorMsgs.isEmpty()) {
			throw new ValidatorException(errorMsgs);
		}
	}
}
