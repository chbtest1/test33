package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.SortCriteria;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.InquiryService;
import ca.medavie.nspp.audit.service.data.DataUtilities;
import ca.medavie.nspp.audit.service.data.HealthServiceCode;
import ca.medavie.nspp.audit.service.data.PeerGroup;
import ca.medavie.nspp.audit.service.data.PeerGroupSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria.PractitionerTypes;
import ca.medavie.nspp.audit.service.exception.InquiryServiceException;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for the inquiry pages.
 */
@ManagedBean(name = "inquiryController")
@ViewScoped
public class InquiryControllerBean extends DropdownConstantsBean implements Serializable {

	@EJB
	protected InquiryService inquiryService;

	/**
	 * Used for holding state of print friendly view requests.
	 */
	@ManagedProperty(value = "#{printFriendlyController}")
	private PrintFriendlyControllerBean printFriendlyControllerBean;

	/** Logger for class */
	private static final Logger logger = LoggerFactory.getLogger(InquiryControllerBean.class);

	/** JRE Generated */
	private static final long serialVersionUID = 8206884776017279538L;

	/** Booleans for controlling popup display */
	private boolean displayHealthServiceCodeDrillDown;

	/** Holds reference to the selected Practitioner and PeerGroup */
	private Practitioner selectedPractitioner;
	private PeerGroup selectedPeerGroup;

	/** Holds search results */
	private LazyDataModel<PeerGroup> peerGroupSearchResults;
	private Integer totalPeerGroups;

	private LazyDataModel<Practitioner> practitionerSearchResults;
	private Integer totalPractitioners;

	/** Controls search results display */
	private boolean displayPeerGroupSearchResults;
	private boolean displayPractitionerSearchResults;

	/** Search criteria model for PeerGroup and Practitioner */
	private PeerGroupSearchCriteria peerGroupSearchCriteria;
	private PractitionerSearchCriteria practitionerSearchCriteria;

	/** List that will hold HealthServiceCodes loaded for Drill down */
	private List<HealthServiceCode> drillDownHealthServiceCodes;

	/** Holds reference to the selected YearEndDate on inquiry search screens */
	private String selectedYearEndDateString;

	/**
	 * Controller constructor
	 */
	public InquiryControllerBean() {

		// Connect to EJB service
		if (this.inquiryService == null) {
			try {
				this.inquiryService = InitialContext.doLookup(prop.getProperty(InquiryService.class.getName()));
				logger.debug("inquiryService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Initialize search criteria objects
		selectedYearEndDateString = null;
		peerGroupSearchCriteria = new PeerGroupSearchCriteria();
		practitionerSearchCriteria = new PractitionerSearchCriteria();

		// Trigger Peer Group Inquiry init since tab is displayed on load
		initiatePeerGroupInquiryPanel();

		// Sync search results data model with bean
		peerGroupSearchResults = performPeerGroupSearchLoad();
		practitionerSearchResults = performPractitionerSearchLoad();
	}

	/**
	 * 0 - PeerGroup Inquiry Inquiry, 1 - Practitioner Inquiry
	 * 
	 * @param anEvent
	 */
	public void criteriaTabChangeListener(ValueChangeEvent anEvent) {
		// Get the index of the requested tab
		Integer requestedTabIndex = (Integer) anEvent.getNewValue();

		if (requestedTabIndex.equals(Integer.valueOf("0"))) {
			initiatePeerGroupInquiryPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("1"))) {
			initiatePractitionerInquiryPanel();
		}
	}

	/**
	 * Navigates the user to the PeerGroup Inquiry panel
	 */
	private void initiatePeerGroupInquiryPanel() {
		// Initialize search criteria objects
		peerGroupSearchCriteria = new PeerGroupSearchCriteria();
		displayPeerGroupSearchResults = false;
		// Reset YearEndDate reference
		selectedYearEndDateString = null;
	}

	/**
	 * Navigates the user to the Practitioner panel
	 */
	private void initiatePractitionerInquiryPanel() {
		// Initialize search criteria objects
		practitionerSearchCriteria = new PractitionerSearchCriteria();
		displayPractitionerSearchResults = false;

		// Reset YearEndDate reference
		selectedYearEndDateString = null;
	}

	/**
	 * Searches for PeerGroups based on the search parameters
	 */
	public void executePeerGroupSearch() {
		// Check if user picked a Profile Type.. If not default it to FFS
		if (peerGroupSearchCriteria.getProfileType() == null) {
			peerGroupSearchCriteria.setProfileType(PractitionerTypes.FEE_FOR_SERVICE);
		}

		// Load matching PeerGroups from service
		try {
			// Clear out any previously selected date. It will be set if a selection was made
			peerGroupSearchCriteria.setYearEndDate(null);

			// Convert the YearEndDate if one is selected
			if (selectedYearEndDateString != null) {
				Date yearEndDate = DataUtilities.SDF.parse(selectedYearEndDateString);
				peerGroupSearchCriteria.setYearEndDate(yearEndDate);
			}
			// Perform search results count
			totalPeerGroups = inquiryService.getSearchPeerGroupsCount(peerGroupSearchCriteria);
			peerGroupSearchResults.setRowCount(totalPeerGroups);

			// Ensure search results table is in fresh state
			resetDataTableState("peerGroupInquiryView:peerInquiryPanel:peerGroupSearchForm:peerGroupSearchResultsTable");

			// Display search results
			displayPeerGroupSearchResults = true;
		}

		// catch service exceptions
		catch (InquiryServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Load the matching PeerGroup records based on the search criteria
	 * 
	 * @return PeerGroup records matching the user search criteria
	 */
	private LazyDataModel<PeerGroup> performPeerGroupSearchLoad() {
		peerGroupSearchResults = new LazyDataModel<PeerGroup>() {

			/** IDE Generated */
			private static final long serialVersionUID = 8093037734837785003L;

			@Override
			public List<PeerGroup> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<PeerGroup> results = new ArrayList<PeerGroup>();

				try {
					if (displayPeerGroupSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								peerGroupSearchCriteria.addSortArgument(PeerGroup.class, sc.getPropertyName(),
										sc.isAscending());
							}
						}

						// Load record matching search results
						results = inquiryService.getSearchPeerGroups(peerGroupSearchCriteria, first, pageSize);
					}
				} catch (InquiryServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return peerGroupSearchResults;
	}

	/**
	 * Resets the PeerGroup search form
	 */
	public void executeClearPeerGroupSearch() {
		clearForm();

		peerGroupSearchCriteria.resetSearchCriteria();
		displayPeerGroupSearchResults = false;
	}

	/**
	 * Searches for Practitioners based on the search parameters
	 */
	public void executePractitionerSearch() {
		// Check if user picked a Profile Type.. If not default it to FFS
		if (practitionerSearchCriteria.getProfileType() == null) {
			practitionerSearchCriteria.setProfileType(PractitionerTypes.FEE_FOR_SERVICE);
		}

		// Load matching Practitioners
		try {
			// Clear out any previously selected date. It will be set if a selection was made
			practitionerSearchCriteria.setYearEndDate(null);

			// Convert the YearEndDate if one is selected
			if (selectedYearEndDateString != null) {
				Date yearEndDate = DataUtilities.SDF.parse(selectedYearEndDateString);
				practitionerSearchCriteria.setYearEndDate(yearEndDate);
			}

			// Perform search results count
			totalPractitioners = inquiryService.getSearchedPractitionersCount(practitionerSearchCriteria);
			practitionerSearchResults.setRowCount(totalPractitioners);

			// Ensure results table is in default state..
			resetDataTableState("practitionerInquiryView:practitionerInquiryPanel:practitionerSearchForm:practitionerSearchResultsTable");

			// Display search results
			displayPractitionerSearchResults = true;
		}
		// catch service exceptions
		catch (InquiryServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Load the matching Practitioner records based on the search criteria
	 * 
	 * @return Practitioner records matching the user search criteria
	 */
	private LazyDataModel<Practitioner> performPractitionerSearchLoad() {
		practitionerSearchResults = new LazyDataModel<Practitioner>() {

			/** IDE Generated */
			private static final long serialVersionUID = 8211793279085270446L;

			@Override
			public List<Practitioner> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<Practitioner> results = new ArrayList<Practitioner>();

				try {
					if (displayPractitionerSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								practitionerSearchCriteria.addSortArgument(Practitioner.class, sc.getPropertyName(),
										sc.isAscending());
							}
						}

						// Load record matching search results
						results = inquiryService.getSearchedPractitioners(practitionerSearchCriteria, first, pageSize);
					}
				} catch (InquiryServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return practitionerSearchResults;
	}

	/**
	 * Resets the Practitioner search form
	 */
	public void executeClearPractitionerSearch() {
		clearForm();

		practitionerSearchCriteria.resetSearchCriteria();
		displayPractitionerSearchResults = false;
	}

	/**
	 * Returns the user to the Inquiry Practitioner search form
	 */
	public void executeReturnToPractitionerSearch() {
		// Execute UI JQuery for transition affect
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToPractitionerInquirySearch()");
	}

	/**
	 * Returns the user to the Inquiry PeerGroup search form
	 */
	public void executeReturnToPeerGroupSearch() {
		// Execute UI JQuery for transition affect
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToPeerGroupInquirySearch()");
	}

	/**
	 * Sets the selected Practitioner as the active Practitioner then loads the available details and displays to the
	 * user
	 * 
	 * @param aPractitioner
	 */
	public void executeViewPractitioner(Practitioner aPractitioner) {
		try {
			selectedPractitioner = inquiryService.getPractitionerDetails(aPractitioner);
			// Set Practitioner state in print bean
			printFriendlyControllerBean.setSelectedPractitioner(selectedPractitioner);

			// Execute UI JQuery for transition affect
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayPractitionerInquiryDetails()");
		}
		// catch service exceptions
		catch (InquiryServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	public void executeViewPeerGroup(PeerGroup aPeerGroup) {
		try {
			selectedPeerGroup = inquiryService.getPeerGroupDetails(aPeerGroup);
			// Set Peer Group state in print bean
			printFriendlyControllerBean.setSelectedPeerGroup(selectedPeerGroup);

			// Execute UI JQuery for transition affect
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayPeerGroupInquiryDetails()");

		} // catch service exceptions
		catch (InquiryServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Triggered when the user clicks on a row in the Health Services table. This will display a pop up window showing
	 * the details of the selected record
	 * 
	 * @param aHsc
	 *            the HealthServiceCode that the row belongs to.
	 */
	public void executeHealthServiceCodeDrillDown(HealthServiceCode aHsc) {
		// Load all HealthService
		try {
			drillDownHealthServiceCodes = inquiryService.getHealthServiceDrillDownDetails(selectedPractitioner, aHsc);
			// Display popup
			displayHealthServiceCodeDrillDown = true;
		}
		// catch service exceptions
		catch (InquiryServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Closes the HealthService Code drill down pop-up window
	 */
	public void closeHealthServiceCodeDrillDown() {
		displayHealthServiceCodeDrillDown = false;
	}

	/**
	 * @return declared enums in PractitionerSearchCriteria for PractitionerType dropdown options
	 */
	public PractitionerTypes[] getPractitionerTypes() {
		return PractitionerTypes.values();
	}

	/**
	 * @return the peerGroupSearchCriteria
	 */
	public PeerGroupSearchCriteria getPeerGroupSearchCriteria() {
		return peerGroupSearchCriteria;
	}

	/**
	 * @param peerGroupSearchCriteria
	 *            the peerGroupSearchCriteria to set
	 */
	public void setPeerGroupSearchCriteria(PeerGroupSearchCriteria peerGroupSearchCriteria) {
		this.peerGroupSearchCriteria = peerGroupSearchCriteria;
	}

	/**
	 * @return the practitionerSearchCriteria
	 */
	public PractitionerSearchCriteria getPractitionerSearchCriteria() {
		return practitionerSearchCriteria;
	}

	/**
	 * @param practitionerSearchCriteria
	 *            the practitionerSearchCriteria to set
	 */
	public void setPractitionerSearchCriteria(PractitionerSearchCriteria practitionerSearchCriteria) {
		this.practitionerSearchCriteria = practitionerSearchCriteria;
	}

	/**
	 * @return the selectedPractitioner
	 */
	public Practitioner getSelectedPractitioner() {
		return selectedPractitioner;
	}

	/**
	 * @return the selectedPeerGroup
	 */
	public PeerGroup getSelectedPeerGroup() {
		return selectedPeerGroup;
	}

	/**
	 * @return the drillDownHealthServiceCodes
	 */
	public List<HealthServiceCode> getDrillDownHealthServiceCodes() {
		return drillDownHealthServiceCodes;
	}

	/**
	 * @param drillDownHealthServiceCodes
	 *            the drillDownHealthServiceCodes to set
	 */
	public void setDrillDownHealthServiceCodes(List<HealthServiceCode> drillDownHealthServiceCodes) {
		this.drillDownHealthServiceCodes = drillDownHealthServiceCodes;
	}

	/**
	 * @return the displayHealthServiceCodeDrillDown
	 */
	public boolean isDisplayHealthServiceCodeDrillDown() {
		return displayHealthServiceCodeDrillDown;
	}

	/**
	 * @param displayHealthServiceCodeDrillDown
	 *            the displayHealthServiceCodeDrillDown to set
	 */
	public void setDisplayHealthServiceCodeDrillDown(boolean displayHealthServiceCodeDrillDown) {
		this.displayHealthServiceCodeDrillDown = displayHealthServiceCodeDrillDown;
	}

	/**
	 * @return the selectedYearEndDateString
	 */
	public String getSelectedYearEndDateString() {
		return selectedYearEndDateString;
	}

	/**
	 * @param selectedYearEndDateString
	 *            the selectedYearEndDateString to set
	 */
	public void setSelectedYearEndDateString(String selectedYearEndDateString) {
		this.selectedYearEndDateString = selectedYearEndDateString;
	}

	/**
	 * @return the peerGroupSearchResults
	 */
	public LazyDataModel<PeerGroup> getPeerGroupSearchResults() {
		return peerGroupSearchResults;
	}

	/**
	 * @param peerGroupSearchResults
	 *            the peerGroupSearchResults to set
	 */
	public void setPeerGroupSearchResults(LazyDataModel<PeerGroup> peerGroupSearchResults) {
		this.peerGroupSearchResults = peerGroupSearchResults;
	}

	/**
	 * @return the totalPeerGroups
	 */
	public Integer getTotalPeerGroups() {
		return totalPeerGroups;
	}

	/**
	 * @param totalPeerGroups
	 *            the totalPeerGroups to set
	 */
	public void setTotalPeerGroups(Integer totalPeerGroups) {
		this.totalPeerGroups = totalPeerGroups;
	}

	/**
	 * @return the practitionerSearchResults
	 */
	public LazyDataModel<Practitioner> getPractitionerSearchResults() {
		return practitionerSearchResults;
	}

	/**
	 * @param practitionerSearchResults
	 *            the practitionerSearchResults to set
	 */
	public void setPractitionerSearchResults(LazyDataModel<Practitioner> practitionerSearchResults) {
		this.practitionerSearchResults = practitionerSearchResults;
	}

	/**
	 * @return the totalPractitioners
	 */
	public Integer getTotalPractitioners() {
		return totalPractitioners;
	}

	/**
	 * @param totalPractitioners
	 *            the totalPractitioners to set
	 */
	public void setTotalPractitioners(Integer totalPractitioners) {
		this.totalPractitioners = totalPractitioners;
	}

	/**
	 * @return the displayPeerGroupSearchResults
	 */
	public boolean isDisplayPeerGroupSearchResults() {
		return displayPeerGroupSearchResults;
	}

	/**
	 * @param displayPeerGroupSearchResults
	 *            the displayPeerGroupSearchResults to set
	 */
	public void setDisplayPeerGroupSearchResults(boolean displayPeerGroupSearchResults) {
		this.displayPeerGroupSearchResults = displayPeerGroupSearchResults;
	}

	/**
	 * @return the displayPractitionerSearchResults
	 */
	public boolean isDisplayPractitionerSearchResults() {
		return displayPractitionerSearchResults;
	}

	/**
	 * @param displayPractitionerSearchResults
	 *            the displayPractitionerSearchResults to set
	 */
	public void setDisplayPractitionerSearchResults(boolean displayPractitionerSearchResults) {
		this.displayPractitionerSearchResults = displayPractitionerSearchResults;
	}

	/**
	 * @return the printFriendlyControllerBean
	 */
	public PrintFriendlyControllerBean getPrintFriendlyControllerBean() {
		return printFriendlyControllerBean;
	}

	/**
	 * @param printFriendlyControllerBean
	 *            the printFriendlyControllerBean to set
	 */
	public void setPrintFriendlyControllerBean(PrintFriendlyControllerBean printFriendlyControllerBean) {
		this.printFriendlyControllerBean = printFriendlyControllerBean;
	}
}
