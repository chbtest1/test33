package ca.medavie.nspp.audit.ui.controller.converter;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * Clean Strings that will be converted to Integers.
 */
@FacesConverter("percentageConverter")
public class PercentageConverter implements Converter {

	public PercentageConverter() {
		super();
	}

	/**
	 * Clean String so that it can be converted to an Integer.
	 * 
	 * @param aContext
	 *            The FacesContext
	 * @param aComponent
	 *            The UIComponent
	 * @param aString
	 *            The String to convert
	 * 
	 * @return Cleaned String
	 */
	public Object getAsObject(FacesContext aContext, UIComponent aComponent, String aString) {
		if (aString == null || aString.trim().equals("")) {
			return null;
		}
        Pattern p = Pattern.compile("[0-9]{0,3}(.[0-9]{0,2})?%?");
        Matcher m = p.matcher(aString);
        if (!m.matches()) {
			Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
			String bundleLocation = "ca.medavie.nspp.audit.ui.messages";
			ResourceBundle messageBundle = ResourceBundle.getBundle(bundleLocation, currentLocale);
			String errorMsg = messageBundle.getString("ERROR.APPLICATION.PERCENTAGE");
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, errorMsg);
			throw new ConverterException(fm);
        }
        aString = aString.replaceAll("%","");
        Float intPert = new Float(aString);
        if (intPert > 100 || intPert < 0) {
			Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
			String bundleLocation = "ca.medavie.nspp.audit.ui.messages";
			ResourceBundle messageBundle = ResourceBundle.getBundle(bundleLocation, currentLocale);
			String errorMsg = messageBundle.getString("ERROR.APPLICATION.PERCENTAGE");
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, errorMsg);
			throw new ConverterException(fm);
        }
		return aString;
	}

	/**
	 * Convert an Integer to a string
	 * 
	 * @param aContext
	 *            The FacesContext
	 * @param aComponent
	 *            The UIComponent
	 * @param anObj
	 *            The Integer to convert
	 * 
	 * @return The String, converted from the Integer.
	 */
	public String getAsString(FacesContext aContext, UIComponent aComponent, Object anObj) {
		DecimalFormat decimalFormatter = new DecimalFormat("#.00");
		Float perValue = null;
		String perString = anObj.toString().replaceAll("%", "");
		try {
			perValue = new Float(perString);
		} catch (java.lang.NumberFormatException e) {
			Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
			String bundleLocation = "ca.medavie.nspp.audit.ui.messages";
			ResourceBundle messageBundle = ResourceBundle.getBundle(bundleLocation, currentLocale);
			String errorMsg = messageBundle.getString("ERROR.APPLICATION.PERCENTAGE");
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, errorMsg);
			throw new ConverterException(fm);
		}		
		return decimalFormatter.format(perValue)+"%";
	}
}