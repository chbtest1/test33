package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.RowStateMap;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.PharmacareService;
import ca.medavie.nspp.audit.service.data.DIN;
import ca.medavie.nspp.audit.service.data.DinSearchCriteria;
import ca.medavie.nspp.audit.service.data.PharmAuditDinExcl;
import ca.medavie.nspp.audit.service.exception.AuditResultServiceException;
import ca.medavie.nspp.audit.service.exception.PharmcareServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.BaseBackingBean;
import ca.medavie.nspp.audit.ui.controller.validator.DinExclValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

@ManagedBean(name = "dssPharmAuditDinExclController")
@ViewScoped
public class DinExclControllerBean extends BaseBackingBean implements Serializable {	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6380122441429029730L;

	// Holds reference to the EJB Client service class
	@EJB
	protected PharmacareService pharmacareService;	
	
	/** logger stuff */
	private static final Logger logger = LoggerFactory.getLogger(AuditResultControllerBean.class);
	
	private String newDin;
	private PharmAuditDinExcl newPharmAuditDinExcl;
	
	private List<PharmAuditDinExcl> pharmAuditDinExclResults = new ArrayList<PharmAuditDinExcl>();
	private List<String> changed = new ArrayList<String>();

	
	private DinExclValidator validator;

	private boolean displayConfirmDeleteDinExclusion;
	
	private RowStateMap dinExclStateMap = new RowStateMap();
	
	/**
	 * Bean constructor
	 */
	public DinExclControllerBean() {
		if (this.pharmacareService == null) {
			try {
				this.pharmacareService = InitialContext.doLookup(prop.getProperty(PharmacareService.class.getName()));				
				pharmAuditDinExclResults = pharmacareService.getPharmAuditDinExcl();
				logger.debug("pharmacareService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			catch (PharmcareServiceException e) {
				Validator.displayServiceError(e);
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		// Init validator
		if (validator == null) {
			validator = new DinExclValidator(pharmacareService);
		}		
	}
	
	/**
	 * save Din Exclusions
	 * 
	 * **/
	public void executeSaveDinExcls() {
		List<PharmAuditDinExcl> changedRecords = new ArrayList<PharmAuditDinExcl>();
		for (PharmAuditDinExcl pharmAuditDinExcl : pharmAuditDinExclResults) {
			if (changed.contains(pharmAuditDinExcl.getDin())) {
				changedRecords.add(pharmAuditDinExcl);
			}
		}
		try {
			// valid business object first
			for (PharmAuditDinExcl pharmAuditDinExcl : changedRecords) {				
				validator.validate(pharmAuditDinExcl);
				pharmAuditDinExcl.setModifiedBy(FacesUtils.getUserId());
				pharmAuditDinExcl.setLastModified(new Date());
			}
			// do the save
			pharmacareService.saveDinExclusions(changedRecords);
			changed.clear();

			// Display save success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.DSS.PHARM.AUDIT.DIN.EXCL.SUCCESSFUL");
		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		} catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}		
	}	
	
	public void recordChange(String din) {
		changed.add(din);
	}
	
	public void executeAddDin() {
		if ((newDin == null)||(newDin.length() < 8)) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DSS.DIN.INVALID"));			
			return ;
		}
		// Set edit meta data
		newPharmAuditDinExcl = new PharmAuditDinExcl(); 
		newPharmAuditDinExcl.setModifiedBy(FacesUtils.getUserId());
		newPharmAuditDinExcl.setLastModified(new Date());
		newPharmAuditDinExcl.setDin(newDin);
		
		DinSearchCriteria dinSearchCriteria = new DinSearchCriteria();
		dinSearchCriteria.setDin(newDin);
		
		List<DIN> dins = new ArrayList<DIN>();
		try {
			dins = pharmacareService.getSearchDins(dinSearchCriteria);
		} catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		if (dins.size() > 0) {
			newPharmAuditDinExcl.setName(dins.get(0).getDrugName());
		} else {
			newPharmAuditDinExcl.setName(null);
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DSS.DIN.INVALID"));			
			return ;
		}		
		newPharmAuditDinExcl.setAgeRestriction(null);
		newPharmAuditDinExcl.setGenderRestriction(null);
		for (PharmAuditDinExcl results : pharmAuditDinExclResults) {
			if (results.getDin().equals(newPharmAuditDinExcl.getDin())) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DSS.DIN.EXISTS"));			
				return ;
			}
		}
		pharmAuditDinExclResults.add(0, newPharmAuditDinExcl);
		newDin = null;
	}
	
	public void executeRefresh() {
		if (this.pharmacareService == null) {
			try {
				this.pharmacareService = InitialContext.doLookup(prop.getProperty(PharmacareService.class.getName()));				
				logger.debug("pharmacareService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		try {
			pharmAuditDinExclResults = pharmacareService.getPharmAuditDinExcl();
		} catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}			
	}

	public void executeReturn() {
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToAddNewAuditResult()");
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "hideMessages()");
	}
	
	private void executeDeleteDinExcl(PharmAuditDinExcl pharmAuditDinExcl) {
		try {
			this.pharmacareService.deleteDinExcl(pharmAuditDinExcl);
		} catch (PharmcareServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
	
	public void selectDinExclusionsToDelete() {
		// Only display confirmation if rows are selected
		if (!dinExclStateMap.getSelected().isEmpty()) {
			setDisplayConfirmDeleteDinExclusion(true);
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "hideMessages()");
		} else {
			// Display error to user
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.DIN.EXCL.NONE.SELECTED.DELETE"));
		}
		
	}

	@SuppressWarnings("unchecked")
	public void confirmDeleteDinExclusion() throws AuditResultServiceException {
		List<PharmAuditDinExcl> toDelete = dinExclStateMap.getSelected();
		for (PharmAuditDinExcl selectedDinExclusionToDelete : toDelete) {
			executeDeleteDinExcl(selectedDinExclusionToDelete);
		}
		dinExclStateMap.clear();
		executeRefresh();
		setDisplayConfirmDeleteDinExclusion(false);
		closePopupWindowJS();
	}

	public void closeConfirmDeleteDinExclusionPopup() {
		setDisplayConfirmDeleteDinExclusion(false);
		closePopupWindowJS();
	}

	public List<PharmAuditDinExcl> getPharmAuditDinExclResults() {
		return pharmAuditDinExclResults;
	}

	public void setPharmAuditDinExclResults(List<PharmAuditDinExcl> pharmAuditDinExclResults) {
		this.pharmAuditDinExclResults = pharmAuditDinExclResults;
	}

	public String getNewDin() {
		return newDin;
	}

	public void setNewDin(String newDin) {
		this.newDin = newDin;
	}

	public PharmAuditDinExcl getNewPharmAuditDinExcl() {
		return newPharmAuditDinExcl;
	}

	public void setNewPharmAuditDinExcl(PharmAuditDinExcl newPharmAuditDinExcl) {
		this.newPharmAuditDinExcl = newPharmAuditDinExcl;
	}

	public boolean isDisplayConfirmDeleteDinExclusion() {
		return displayConfirmDeleteDinExclusion;
	}

	public void setDisplayConfirmDeleteDinExclusion(
			boolean displayConfirmDeleteDinExclusion) {
		this.displayConfirmDeleteDinExclusion = displayConfirmDeleteDinExclusion;
	}

	public RowStateMap getDinExclStateMap() {
		return dinExclStateMap;
	}

	public void setDinExclStateMap(RowStateMap dinExclStateMap) {
		this.dinExclStateMap = dinExclStateMap;
	}

	public List<String> getChanged() {
		return changed;
	}

	public void setChanged(List<String> changed) {
		this.changed = changed;
	}
}
