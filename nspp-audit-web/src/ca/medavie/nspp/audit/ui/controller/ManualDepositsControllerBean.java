package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.SortCriteria;
import org.icefaces.util.JavaScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.ManualDepositsService;
import ca.medavie.nspp.audit.service.data.BusinessArrangementCode;
import ca.medavie.nspp.audit.service.data.ManualDeposit;
import ca.medavie.nspp.audit.service.data.ManualDepositSearchCriteria;
import ca.medavie.nspp.audit.service.data.Practitioner;
import ca.medavie.nspp.audit.service.data.PractitionerSearchCriteria;
import ca.medavie.nspp.audit.service.data.ProviderGroup;
import ca.medavie.nspp.audit.service.data.ProviderGroupSearchCriteria;
import ca.medavie.nspp.audit.service.exception.AccountingServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.ManualDepositValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for the inquiry pages.
 */
@ManagedBean(name = "manualDepositsController")
@ViewScoped
public class ManualDepositsControllerBean extends DropdownConstantsBean implements Serializable {

	/** JRE Generated */
	private static final long serialVersionUID = 1L;

	@EJB
	protected ManualDepositsService manualDepositsService;

	/** Logger for class */
	private static final Logger logger = LoggerFactory.getLogger(ManualDepositsControllerBean.class);

	/** SearchCriteria objects */
	private ManualDepositSearchCriteria manualDepositSearchCriteria;
	private PractitionerSearchCriteria practitionerSearchCriteria;
	private ProviderGroupSearchCriteria groupSearchCriteria;

	/** Holds search results */
	private LazyDataModel<ManualDeposit> manualDepositSearchResults;
	private LazyDataModel<Practitioner> practitionerSearchResults;
	private LazyDataModel<ProviderGroup> groupSearchResults;

	/** holds a count of search row count */
	private Integer totalManualDeposits;
	private Integer totalPractitioners;
	private Integer totalGroups;

	/** Controls display of Manual Deposit search results */
	private boolean displayManualDepositSearchResults;

	/** Controls the display of both Group and Practitioner searches for new Manual Deposits */
	private boolean displayNewPractitionerSearchResults;
	private boolean displayNewGroupSearchResults;

	/** Holds the selected Manual Deposits search style */
	private ManualDepositSearchTypes selectedSearchStyle;

	/** Boolean that keeps track of the search performed to control field display */
	private boolean practitionerSearch;

	/** True if an existing Manual Deposit is being updated, false if new record is being created. */
	private boolean editingRecord;

	/** Holds the currently selected ManualDeposit */
	private ManualDeposit selectedManualDeposit;
	/** Holds the currently selected Practitioner */
	private Practitioner selectedPractitioner;
	/** Holds the currently selected Group */
	private ProviderGroup selectedGroup;

	/** Manual Deposit validator */
	private ManualDepositValidator manualDepositValidator;

	/**
	 * Controller constructor
	 */
	public ManualDepositsControllerBean() {
		// Connect to EJB service
		if (this.manualDepositsService == null) {
			try {
				this.manualDepositsService = InitialContext.doLookup(prop.getProperty(ManualDepositsService.class
						.getName()));
				logger.debug("ManualDepositsService is loaded in the constructor");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}

		// Instantiate the MD validator
		manualDepositValidator = new ManualDepositValidator();

		// Init first tab since it is displayed by default
		initiateManualDepositsInquiryPanel();

		// Sync Manual Deposits data model with bean
		manualDepositSearchResults = performManualDepositsSearchLoad();

		// Sync (New) Manual Deposit search data models
		practitionerSearchResults = performPractitionerSearchLoad();
		groupSearchResults = performGroupSearchLoad();
	}

	/**
	 * 0 - Search/Edit Manual Deposits, 1 - Create New Manual Deposit
	 * 
	 * @param anEvent
	 */
	public void accountingTabChangeListener(ValueChangeEvent anEvent) {
		// Get the index of the requested tab
		Integer requestedTabIndex = (Integer) anEvent.getNewValue();
		if (requestedTabIndex.equals(Integer.valueOf("0"))) {
			initiateManualDepositsInquiryPanel();
		} else if (requestedTabIndex.equals(Integer.valueOf("1"))) {
			initiateAddManualDepositPanel();
		}
	}

	/**
	 * Prepares bean for Search/Edit Manual Deposits panel
	 */
	private void initiateManualDepositsInquiryPanel() {

		// Initialize search criteria objects
		manualDepositSearchCriteria = new ManualDepositSearchCriteria();
		practitionerSearchCriteria = new PractitionerSearchCriteria();
		groupSearchCriteria = new ProviderGroupSearchCriteria();
		selectedManualDeposit = new ManualDeposit();

		Calendar calNow = Calendar.getInstance();
		String year = String.valueOf(calNow.get(Calendar.YEAR));
		selectedManualDeposit.setFiscalYearEndStr(year);
		selectedPractitioner = null;
		selectedGroup = null;
		selectedSearchStyle = null;

		// Hide search results table
		displayManualDepositSearchResults = false;

		// Ensure the search criteria form is cleared out.
		editingRecord = false;
	}

	/**
	 * Prepares bean for Add Manual Deposts panel
	 */
	private void initiateAddManualDepositPanel() {
		// Ensure the search criteria form is cleared out.
		manualDepositSearchCriteria = new ManualDepositSearchCriteria();
		practitionerSearchCriteria = new PractitionerSearchCriteria();
		groupSearchCriteria = new ProviderGroupSearchCriteria();

		selectedPractitioner = null;
		selectedGroup = null;
		selectedSearchStyle = null;

		selectedManualDeposit = new ManualDeposit();
		Calendar calNow = Calendar.getInstance();
		String year = String.valueOf(calNow.get(Calendar.YEAR));
		selectedManualDeposit.setFiscalYearEndStr(year);

		// Hide search results
		displayNewPractitionerSearchResults = false;
		displayNewGroupSearchResults = false;

		// State that we are now editing a record.
		editingRecord = false;
	}

	/**
	 * Navigates the user to the PeerGroup ManualDeposits panel
	 */
	public void initiateEditManualDepositPanel(ManualDeposit manualDeposit) {

		try {

			// Clear out search criteria
			manualDepositSearchCriteria.resetSearchCriteria();

			// Load the selected Manual Deposit's details
			selectedManualDeposit = manualDepositsService.getManualDepositDetails(manualDeposit);

			// If no Fiscal Year is set.. Set it to the current year
			Calendar calNow = Calendar.getInstance();
			String year = String.valueOf(calNow.get(Calendar.YEAR));
			if (selectedManualDeposit.getFiscalYearEndStr() == null) {
				selectedManualDeposit.setFiscalYearEndStr(year);
			}

			// If there is a group number associated with this Manual Deposit, load the Group details
			if (selectedManualDeposit.getGroupNumber() != null) {
				groupSearchCriteria.setProviderGroupId(selectedManualDeposit.getGroupNumber().toString());

				// Since we are only getting the first row... pagination args hard-coded to 1 row
				List<ProviderGroup> groups = manualDepositsService.getSearchedProviderGroups(groupSearchCriteria, 0, 1);

				if (groups.size() > 0) {
					selectedGroup = groups.get(0);
					selectedManualDeposit.setGroupName(this.selectedGroup.getProviderGroupName());
					selectedManualDeposit.setGroupNumber(this.selectedGroup.getProviderGroupId());
				}
			}

			// If there is a Practitioner number associated with this Manual Deposit, loads the Practitioner Details
			if (selectedManualDeposit.getPractitionerNumber() != null) {

				// Perform lookup
				practitionerSearchCriteria.setPractitionerNumber(selectedManualDeposit.getPractitionerNumber());

				// Since we are only getting the first row... pagination args hard-coded to 1 row
				List<Practitioner> practitioners = manualDepositsService.getManualDepositSearchedPractitioners(
						practitionerSearchCriteria, 0, 1);

				if (practitioners.size() > 0) {
					selectedPractitioner = practitioners.get(0);
					selectedManualDeposit.setPractitionerName(selectedPractitioner.getPractitionerName());
					selectedManualDeposit.setPractitionerType(selectedPractitioner.getPractitionerType());
					selectedManualDeposit.setPractitionerNumber(selectedPractitioner.getPractitionerNumber());
				}
			}

			// Enable Edit mode
			editingRecord = true;

			// Display Edit screen
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayManualDepositEdit()");

		} catch (AccountingServiceException e) {
			Validator.displayServiceError(e);
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Searches for PeerGroups based on the search parameters
	 */
	public void executeManualDepositSearch() {

		try {
			// Perform search record count
			totalManualDeposits = manualDepositsService.getSearchedAuditCriteriaCount(manualDepositSearchCriteria,
					practitionerSearch);
			manualDepositSearchResults.setRowCount(totalManualDeposits);

			// Ensure search results table is in clean state
			resetDataTableState("editManualDepositsView:manualDepositsSearchForm:manualDepositsInquiryPanel:manualDepositsPractitionerResultsTable");

			// Display search results
			displayManualDepositSearchResults = true;

		} catch (AccountingServiceException e) {
			Validator.displayServiceError(e);
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Loads the matching ManualDeposit records based on the search criteria
	 * 
	 * @return ManualDeposit records matching the user search criteria
	 */
	private LazyDataModel<ManualDeposit> performManualDepositsSearchLoad() {
		manualDepositSearchResults = new LazyDataModel<ManualDeposit>() {

			/** IDE generated */
			private static final long serialVersionUID = 4882516333695668143L;

			@Override
			public List<ManualDeposit> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<ManualDeposit> results = new ArrayList<ManualDeposit>();

				try {
					if (displayManualDepositSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								manualDepositSearchCriteria.addSortArgument(ManualDeposit.class, sc.getPropertyName(),
										sc.isAscending());
							}
						}

						// Load record matching search results
						results = manualDepositsService.getSearchedManualDeposits(manualDepositSearchCriteria,
								practitionerSearch, first, pageSize);
					}
				} catch (AccountingServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return manualDepositSearchResults;
	}

	/**
	 * Returns the user to the first step of Searching for Existing Manual Deposits
	 */
	public void returnToManualDepositsSearchSelection() {
		manualDepositSearchCriteria.resetSearchCriteria();
		selectedSearchStyle = null;
		practitionerSearch = false;
		displayManualDepositSearchResults = false;
		clearForm();
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayEditSearchSelectionPanel()");
	}

	/**
	 * Returns the user to the first step of Searching for Practitioner/Groups for new Manual Deposits
	 */
	public void returnToCreateNewManualDepositsSearchSelection() {
		groupSearchCriteria.resetSearchCriteria();
		practitionerSearchCriteria.resetSearchCriteria();
		selectedSearchStyle = null;
		practitionerSearch = false;

		// Hide search results
		displayNewPractitionerSearchResults = false;
		displayNewGroupSearchResults = false;

		// Reset form
		clearForm();
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayNewSearchSelectionPanel()");
	}

	/**
	 * Returns the user to the previous search screen. Prevents the need for the user to have to run through every
	 * search step on add.
	 */
	public void returnToCreateNewManualDepositsSearch() {
		selectedManualDeposit = new ManualDeposit();
		clearForm();
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "returnToManualDepositSearch()");
	}

	/**
	 * Executes search for payor practitioner
	 */
	public void executePractitionerSearch() {
		try {

			// Perform search row count
			totalPractitioners = manualDepositsService
					.getManualDepositSearchedPractitionersCount(practitionerSearchCriteria);
			practitionerSearchResults.setRowCount(totalPractitioners);

			// Ensure datatable is in fresh state
			resetDataTableState("createManualDepositsView:addSearchPayors:payorsSearchPanel:practitionerSearchResultsTable");

			// Display search results
			displayNewPractitionerSearchResults = true;

		} catch (AccountingServiceException e) {
			Validator.displayServiceError(e);
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Loads the matching Practitioner records based on the search criteria
	 * 
	 * @return Practitioner records matching the user search criteria
	 */
	private LazyDataModel<Practitioner> performPractitionerSearchLoad() {
		practitionerSearchResults = new LazyDataModel<Practitioner>() {

			/** IDE generated */
			private static final long serialVersionUID = 7081264955760898886L;

			@Override
			public List<Practitioner> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<Practitioner> results = new ArrayList<Practitioner>();

				try {
					if (displayNewPractitionerSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								practitionerSearchCriteria.addSortArgument(Practitioner.class, sc.getPropertyName(),
										sc.isAscending());
							}
						}

						// Load record matching search results
						results = manualDepositsService.getManualDepositSearchedPractitioners(
								practitionerSearchCriteria, first, pageSize);
					}
				} catch (AccountingServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return practitionerSearchResults;
	}

	/**
	 * Executes search for payor provider group
	 */
	public void executeGroupSearch() {
		try {
			// Perform search row count
			totalGroups = manualDepositsService.getSearchedProviderGroupsCount(groupSearchCriteria);
			groupSearchResults.setRowCount(totalGroups);

			// Ensure datatable is in clean state
			resetDataTableState("createManualDepositsView:addSearchPayors:payorsSearchPanel:groupSearchResultsTable");

			// Display search results
			displayNewGroupSearchResults = true;

		} catch (AccountingServiceException e) {
			Validator.displayServiceError(e);
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Loads the matching Group records based on the search criteria
	 * 
	 * @return Group records matching the user search criteria
	 */
	private LazyDataModel<ProviderGroup> performGroupSearchLoad() {
		groupSearchResults = new LazyDataModel<ProviderGroup>() {

			/** IDE generated */
			private static final long serialVersionUID = 7469449684003752416L;

			@Override
			public List<ProviderGroup> load(int first, int pageSize, final SortCriteria[] criteria,
					final Map<String, String> filters) {

				List<ProviderGroup> results = new ArrayList<ProviderGroup>();

				try {
					if (displayNewGroupSearchResults) {

						// Apply any sorting to search criteria
						if (criteria.length > 0) {

							for (int x = 0; x < criteria.length; x++) {
								SortCriteria sc = criteria[x];
								groupSearchCriteria.addSortArgument(ProviderGroup.class, sc.getPropertyName(),
										sc.isAscending());
							}
						}
						// Load record matching search results
						results = manualDepositsService.getSearchedProviderGroups(groupSearchCriteria, first, pageSize);
					}
				} catch (AccountingServiceException e) {
					Validator.displayServiceError(e);
					e.printStackTrace();
					logger.error(e.getMessage());

				} catch (Exception e) {
					Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.APPLICATION.FAILURE"));
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				// Return results. If nothing found an empty list is returned
				return results;
			}
		};
		return groupSearchResults;
	}

	/**
	 * Returns list of Business Arrangement Codes for DDBL in user is adding a new manual deposit.
	 * 
	 * @return
	 */
	public List<BusinessArrangementCode> getBusArrsAdd() {
		try {
			return manualDepositsService.getBusinessArrangements(this.selectedPractitioner, this.selectedGroup);
		} catch (AccountingServiceException e) {
			Validator.displayServiceError(e);
			logger.error(e.getMessage());
			return null;
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * Returns list of Business Arrangement Codes for DDBL in user is editing an old manual deposit.
	 * 
	 * @return
	 */
	public List<BusinessArrangementCode> getBusArrsEdit() {
		try {
			return manualDepositsService.getBusinessArrangements(this.selectedManualDeposit);
		} catch (AccountingServiceException e) {
			Validator.displayServiceError(e);
			logger.error(e.getMessage());
			return null;
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * Triggers when user selects a practitioner to be the payor for a manual deposit.
	 */
	public void selectPractitioner(Practitioner practitioner) {
		this.selectedPractitioner = practitioner;
		this.selectedManualDeposit.setPractitionerName(this.selectedPractitioner.getPractitionerName());
		this.selectedManualDeposit.setPractitionerType(this.selectedPractitioner.getPractitionerType());
		this.selectedManualDeposit.setPractitionerNumber(this.selectedPractitioner.getPractitionerNumber());
		// Toggle UI state
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAddNewManualDepositPanel()");
	}

	/**
	 * Triggers when user selects a provider group to be the payor for a manual deposit.
	 */
	public void selectGroup(ProviderGroup providerGroup) {
		this.selectedGroup = providerGroup;
		this.selectedManualDeposit.setGroupName(this.selectedGroup.getProviderGroupName());
		this.selectedManualDeposit.setGroupNumber(this.selectedGroup.getProviderGroupId());
		// Toggle UI state
		JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displayAddNewManualDepositPanel()");
	}

	public void clearManualDepositSearch() {
		this.manualDepositSearchCriteria.resetSearchCriteria();
		clearForm();
		displayManualDepositSearchResults = false;
	}

	public void clearPractitionerCriteria() {
		this.practitionerSearchCriteria = new PractitionerSearchCriteria();
		displayNewPractitionerSearchResults = false;
		clearForm();
	}

	public void clearGroupCriteria() {
		this.groupSearchCriteria = new ProviderGroupSearchCriteria();
		displayNewGroupSearchResults = false;
		clearForm();
	}

	/**
	 * When the user toggles the Manual Deposit search type value. Display the request search screen
	 */
	public void manualDepositsSearchTypeChangeListener() {

		// Only continue if valid option is selected
		if (selectedSearchStyle != null) {
			if (selectedSearchStyle == ManualDepositSearchTypes.PRACTITIONER_SEARCH) {
				practitionerSearch = true;
			} else {
				practitionerSearch = false;
			}
			// Toggle the display of the search form
			JavaScriptRunner.runScript(FacesContext.getCurrentInstance(), "displaySelectedSearchPanel()");
		}
	}

	/**
	 * Display warning when fiscal year is not current year or current year+1.
	 * 
	 * @param e
	 */
	public void fiscalYearChange() {
		System.out.println("Year CHanged!");
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		if (selectedManualDeposit.getFiscalYearEndStr() == null
				|| selectedManualDeposit.getFiscalYearEndStr().isEmpty()) {
			return;
		} else {
			int year = Integer.parseInt(selectedManualDeposit.getFiscalYearEndStr());
			if ((year != currentYear) && (year != currentYear + 1)) {
				System.out.println("Display warning!");
				FacesUtils.addWarnMessage(MessageFormat.format(
						ResourceBundle.getBundle("ca.medavie.nspp.audit.ui.messages",
								FacesContext.getCurrentInstance().getViewRoot().getLocale()).getString(
								"ERROR.ACCOUNTING.FISCALYEAREND"), String.valueOf(currentYear),
						String.valueOf(currentYear + 1)));
			}
		}
	}

	/**
	 * Save Manual deposit action
	 */
	public void saveManualDeposit() {

		// Only do this for new Manual Deposits
		if (!editingRecord) {
			fiscalYearChange();
		}

		if (!practitionerSearch) {
			if ((selectedManualDeposit.getPractitionerNumber() != null)
					&& (selectedManualDeposit.getPractitionerType() == null)) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.ACCOUNTING.BOTH.OR.NONE"));
				return;
			}
			if ((selectedManualDeposit.getPractitionerNumber() == null)
					&& (selectedManualDeposit.getPractitionerType() != null)) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.ACCOUNTING.BOTH.OR.NONE"));
				return;
			}
			if ((selectedManualDeposit.getPractitionerNumber() != null)
					&& (selectedManualDeposit.getPractitionerType() != null)) {
				try {
					if (!this.manualDepositsService.verifyPractitioner(selectedManualDeposit)) {
						Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
								"ERROR.ACCOUNTING.PRACTITIONER.NOTFOUND"));
						return;
					}
				} catch (AccountingServiceException e) {
					Validator.displayServiceError(e);
					logger.error(e.getMessage());
					return;
				}
			}
		}
		try {
			selectedManualDeposit.setLastModifiedBy(FacesUtils.getUserId());
			selectedManualDeposit.setLastModifiedDate(new Date());

			// Perform object validation
			manualDepositValidator.validate(this.selectedManualDeposit);

			if ((selectedManualDeposit.getDepositNumber() == null) || (selectedManualDeposit.getDepositNumber() == 0L)) {
				selectedManualDeposit.setDepositNumber(manualDepositsService.getNextValueFromDssDepositNumberSeq());
			}
			manualDepositsService.saveOrUpdateManualDeposit(selectedManualDeposit);

			// Display save success message
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.MANUAL.DEPOSIT.SAVE.SUCCESSFUL");

		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		} catch (AccountingServiceException e) {
			Validator.displayServiceError(e);
			logger.error(e.getMessage());
		} catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Delete Manual deposit action
	 */
	public void deleteManualDeposit() {

		try {
			manualDepositsService.deleteManualDeposit(selectedManualDeposit);
		}
		// catch service exceptions
		catch (AccountingServiceException e) {
			Validator.displayServiceError(e);
			logger.error(e.getMessage());
			return;
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
			return;
		}
		initiateManualDepositsInquiryPanel();
	}

	/**
	 * Returns list of fiscal years for fiscal year end DDLB.
	 * 
	 * @return
	 */
	public List<String> getFiscalYears() {
		List<String> fiscalYears = new ArrayList<String>();
		Calendar now = Calendar.getInstance();
		int currentYear = now.get(Calendar.YEAR);
		int startYear = 0;
		if (this.selectedManualDeposit != null) {
			if (this.selectedManualDeposit.getFiscalYearEndStr() != null) {
				startYear = Integer.parseInt(this.selectedManualDeposit.getFiscalYearEndStr()) - 5;
			}
		}
		if (startYear > currentYear)
			startYear = currentYear - 5;
		for (int year = startYear != 0 ? startYear : currentYear - 5; year < currentYear + 5; year++) {
			fiscalYears.add(String.valueOf(year));
		}
		return fiscalYears;
	}

	/**
	 * Returns panel header line for payor info on Edit/Add screens.
	 * 
	 * @return
	 */
	public String getPayorInfo() {
		StringBuilder panelHeader = new StringBuilder();
		if (selectedManualDeposit.getGroupNumber() != null) {
			panelHeader.append(" : ");
			panelHeader.append(selectedManualDeposit.getGroupNumber()).append(" ")
					.append(selectedManualDeposit.getGroupName());
		} else if (selectedManualDeposit.getPractitionerNumber() != null) {
			panelHeader.append(" : ");
			panelHeader.append(selectedManualDeposit.getPractitionerType()).append(" - ")
					.append(selectedManualDeposit.getPractitionerNumber()).append(" ")
					.append(selectedManualDeposit.getPractitionerName());
		}
		return panelHeader.toString();
	}
	
	// JTRAX-62 07-MAR-18 BCAINNE 
	public void executeSearch() {
		if (isPractitionerSearch()) {
			executePractitionerSearch();
		} else {
			executeGroupSearch();
		}
	}

	/**
	 * @return the peerGroupSearchCriteria
	 */
	public ManualDepositSearchCriteria getManualDepositSearchCriteria() {
		return manualDepositSearchCriteria;
	}

	/**
	 * @param peerGroupSearchCriteria
	 *            the peerGroupSearchCriteria to set
	 */
	public void setManualDepositSearchCriteria(ManualDepositSearchCriteria manualDepositSearchCriteria) {
		this.manualDepositSearchCriteria = manualDepositSearchCriteria;
	}

	public ManualDeposit getSelectedManualDeposit() {
		return selectedManualDeposit;
	}

	public void setSelectedManualDeposit(ManualDeposit selectedManualDeposit) {
		this.selectedManualDeposit = selectedManualDeposit;
	}

	public PractitionerSearchCriteria getPractitionerSearchCriteria() {
		return practitionerSearchCriteria;
	}

	public void setPractitionerSearchCriteria(PractitionerSearchCriteria practitionerSearchCriteria) {
		this.practitionerSearchCriteria = practitionerSearchCriteria;
	}

	public boolean isEditingRecord() {
		return editingRecord;
	}

	public void setEditingRecord(boolean editingRecord) {
		this.editingRecord = editingRecord;
	}

	public Practitioner getSelectedPractitioner() {
		return selectedPractitioner;
	}

	public void setSelectedPractitioner(Practitioner selectedPractitioner) {
		this.selectedPractitioner = selectedPractitioner;
	}

	/**
	 * Controls Practitioner Search results display for Edit existing Manual Deposit Screen
	 * 
	 * @return true if there is result and search mode is set to Practitioner, otherwise false
	 */
	public boolean isDisplayPractitionerSearchResults() {
		return (manualDepositSearchResults.getRowCount() != 0 && practitionerSearch);
	}

	/**
	 * Controls Practitioner Search results display for Edit existing Manual Deposit Screen
	 * 
	 * @return true if there is result and search mode is not set to Practitioner, otherwise false
	 */
	public boolean isDisplayGroupSearchResults() {
		return (manualDepositSearchResults.getRowCount() != 0 && !practitionerSearch);
	}

	public ProviderGroupSearchCriteria getGroupSearchCriteria() {
		return groupSearchCriteria;
	}

	public void setGroupSearchCriteria(ProviderGroupSearchCriteria groupSearchCriteria) {
		this.groupSearchCriteria = groupSearchCriteria;
	}

	public ProviderGroup getSelectedGroup() {
		return selectedGroup;
	}

	public void setSelectedGroup(ProviderGroup selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	/**
	 * @return the selectedSearchStyle
	 */
	public ManualDepositSearchTypes getSelectedSearchStyle() {
		return selectedSearchStyle;
	}

	/**
	 * @param selectedSearchStyle
	 *            the selectedSearchStyle to set
	 */
	public void setSelectedSearchStyle(ManualDepositSearchTypes selectedSearchStyle) {
		this.selectedSearchStyle = selectedSearchStyle;
	}

	/**
	 * @return the practitionerSearch
	 */
	public boolean isPractitionerSearch() {
		return practitionerSearch;
	}

	/**
	 * @param practitionerSearch
	 *            the practitionerSearch to set
	 */
	public void setPractitionerSearch(boolean practitionerSearch) {
		this.practitionerSearch = practitionerSearch;
	}

	/**
	 * @return the manualDepositSearchResults
	 */
	public LazyDataModel<ManualDeposit> getManualDepositSearchResults() {
		return manualDepositSearchResults;
	}

	/**
	 * @param manualDepositSearchResults
	 *            the manualDepositSearchResults to set
	 */
	public void setManualDepositSearchResults(LazyDataModel<ManualDeposit> manualDepositSearchResults) {
		this.manualDepositSearchResults = manualDepositSearchResults;
	}

	/**
	 * @return the totalManualDeposits
	 */
	public Integer getTotalManualDeposits() {
		return totalManualDeposits;
	}

	/**
	 * @param totalManualDeposits
	 *            the totalManualDeposits to set
	 */
	public void setTotalManualDeposits(Integer totalManualDeposits) {
		this.totalManualDeposits = totalManualDeposits;
	}

	/**
	 * @return the displayManualDepositSearchResults
	 */
	public boolean isDisplayManualDepositSearchResults() {
		return displayManualDepositSearchResults;
	}

	/**
	 * @param displayManualDepositSearchResults
	 *            the displayManualDepositSearchResults to set
	 */
	public void setDisplayManualDepositSearchResults(boolean displayManualDepositSearchResults) {
		this.displayManualDepositSearchResults = displayManualDepositSearchResults;
	}

	/**
	 * @return the practitionerSearchResults
	 */
	public LazyDataModel<Practitioner> getPractitionerSearchResults() {
		return practitionerSearchResults;
	}

	/**
	 * @param practitionerSearchResults
	 *            the practitionerSearchResults to set
	 */
	public void setPractitionerSearchResults(LazyDataModel<Practitioner> practitionerSearchResults) {
		this.practitionerSearchResults = practitionerSearchResults;
	}

	/**
	 * @return the totalPractitioners
	 */
	public Integer getTotalPractitioners() {
		return totalPractitioners;
	}

	/**
	 * @param totalPractitioners
	 *            the totalPractitioners to set
	 */
	public void setTotalPractitioners(Integer totalPractitioners) {
		this.totalPractitioners = totalPractitioners;
	}

	/**
	 * @return the totalGroups
	 */
	public Integer getTotalGroups() {
		return totalGroups;
	}

	/**
	 * @param totalGroups
	 *            the totalGroups to set
	 */
	public void setTotalGroups(Integer totalGroups) {
		this.totalGroups = totalGroups;
	}

	/**
	 * @return the displayNewPractitionerSearchResults
	 */
	public boolean isDisplayNewPractitionerSearchResults() {
		return displayNewPractitionerSearchResults;
	}

	/**
	 * @param displayNewPractitionerSearchResults
	 *            the displayNewPractitionerSearchResults to set
	 */
	public void setDisplayNewPractitionerSearchResults(boolean displayNewPractitionerSearchResults) {
		this.displayNewPractitionerSearchResults = displayNewPractitionerSearchResults;
	}

	/**
	 * @return the displayNewGroupSearchResults
	 */
	public boolean isDisplayNewGroupSearchResults() {
		return displayNewGroupSearchResults;
	}

	/**
	 * @param displayNewGroupSearchResults
	 *            the displayNewGroupSearchResults to set
	 */
	public void setDisplayNewGroupSearchResults(boolean displayNewGroupSearchResults) {
		this.displayNewGroupSearchResults = displayNewGroupSearchResults;
	}

	/**
	 * @return the groupSearchResults
	 */
	public LazyDataModel<ProviderGroup> getGroupSearchResults() {
		return groupSearchResults;
	}

	/**
	 * @param groupSearchResults the groupSearchResults to set
	 */
	public void setGroupSearchResults(LazyDataModel<ProviderGroup> groupSearchResults) {
		this.groupSearchResults = groupSearchResults;
	}
}
