package ca.medavie.nspp.audit.ui.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.icefaces.ace.model.table.RowStateMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.medavie.nspp.audit.service.DssUnitValueService;
import ca.medavie.nspp.audit.service.data.DssUnitValueBO;
import ca.medavie.nspp.audit.service.exception.DssUnitValueServiceException;
import ca.medavie.nspp.audit.ui.FacesUtils;
import ca.medavie.nspp.audit.ui.bean.DropdownConstantsBean;
import ca.medavie.nspp.audit.ui.controller.validator.DssUnitValueValidator;
import ca.medavie.nspp.audit.ui.controller.validator.Validator;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorException;
import ca.medavie.nspp.audit.ui.controller.validator.ValidatorMessage;

/**
 * Backing bean for the PeerGroup Bulk Copy page/panel
 */
@ManagedBean(name = "dssUnitValueController")
@ViewScoped
public class DssUnitValueControllerBean extends DropdownConstantsBean implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger(DssUnitValueControllerBean.class);
	/**
	 * IDE generated serial number
	 */
	private static final long serialVersionUID = 380033445068528901L;

	// Holds reference to the EJB Client service class
	@EJB
	protected DssUnitValueService dssUnitValueService;

	private List<DssUnitValueBO> dssUnitValues;

	private DssUnitValueBO dssUnitValueToAdd;

	private DssUnitValueValidator validator;

	private boolean displayEditDssUnitValue;

	private DssUnitValueBO selectedDssValue;

	/** Date used by default for Final Contact Date and Interim Date */
	private Date defaultInputDate;

	/** RowStateMap for the DSS Unit Value table. */
	private RowStateMap dssUnitValueStateMap;

	/** Controls the display of the Delete confirmation popup */
	private boolean displayDeleteConfirmationDialog;

	/**
	 * Bean constructor
	 */
	public DssUnitValueControllerBean() {

		if (this.dssUnitValueService == null) {
			try {
				this.dssUnitValueService = InitialContext
						.doLookup(prop.getProperty(DssUnitValueService.class.getName()));
				logger.debug("dssUnitValueService is loaded in the constructor");

			} catch (NamingException e) {

				e.printStackTrace();
			}
		}

		// set up validator
		validator = new DssUnitValueValidator(dssUnitValueService);

		// Set up the default date value
		Calendar cal = Calendar.getInstance();
		cal.set(3000, 00, 01);
		defaultInputDate = cal.getTime();

		// Init Add/Edit page since it is displayed on load
		initiateAddEditDssUnitValuePanel();
	}

	/**
	 * Prepare bean for Add/Edit panel
	 */
	private void initiateAddEditDssUnitValuePanel() {
		try {

			// set up new dss unit value
			dssUnitValueToAdd = new DssUnitValueBO();
			dssUnitValueToAdd.setEffectiveToDate(defaultInputDate);
			dssUnitValueToAdd.setUnitValueLevel(BigDecimal.valueOf(2));
			dssUnitValues = dssUnitValueService.getDssUnitValues();
		}
		// catch service exceptions
		catch (DssUnitValueServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Action to clear add new dss unit value form
	 */
	public void executeClearAddNewDssUnitValue() {
		dssUnitValueToAdd = new DssUnitValueBO();
		dssUnitValueToAdd.setEffectiveToDate(defaultInputDate);
		dssUnitValueToAdd.setUnitValueLevel(BigDecimal.valueOf(2));
	}

	/**
	 * Action to add a new dss unit value
	 */
	public void executeAddNewDssUnitValue() {

		try {
			// Validate DSSUnitValue
			validator.validate(dssUnitValueToAdd);

			dssUnitValueToAdd.setLastModified(new Date());
			dssUnitValueToAdd.setModifiedBy(FacesUtils.getUserId());

			dssUnitValueService.saveOrUpdateDssUnitValue(dssUnitValueToAdd);
			// add it to the top of the table
			dssUnitValues.add(0, dssUnitValueToAdd);
			// reset old object. keep using it for adding new DssUnitValueBO
			dssUnitValueToAdd = new DssUnitValueBO();
			dssUnitValueToAdd.setEffectiveToDate(defaultInputDate);
			dssUnitValueToAdd.setUnitValueLevel(BigDecimal.valueOf(2));

			// show Save successfully message in the page
			Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.DSS.UNIT.VALUE.SAVE.SUCCESSFUL");
			logger.debug("a dss unit values saved successfully");
		}
		// catch validation exceptions
		catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
		}
		// catch service exceptions
		catch (DssUnitValueServiceException e) {
			Validator.displayServiceError(e);
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	/**
	 * Update the existing dss unit value in the table
	 * 
	 * @param aDssUnitValueBO
	 */
	public void executeEditDssRecord(DssUnitValueBO aDssUnitValueBO) {

		// reset it first
		selectedDssValue = null;
		// set it to a selected one
		this.selectedDssValue = aDssUnitValueBO;
		// display pop up editing window
		displayEditDssUnitValue = true;
	}

	/**
	 * Displays a confirmation dialog to the user to confirm the deletion of the selected records
	 */
	public void executeDeleteSelectedDssRecords() {

		// Only display delete confirmation if there is rows selected
		if (!dssUnitValueStateMap.getSelected().isEmpty()) {
			displayDeleteConfirmationDialog = true;
		} else {
			// Display warning to user to assist them
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_WARN,
					"ERROR.DSS.UNIT.VALUE.NONE.SELECTED.DELETE"));
		}
	}

	/**
	 * Confirmation was confirmed by the user.. Delete the record from the datatable.
	 */
	public void excuteDeleteDssRecordConfirmation() {

		try {
			// Extract the DSS Unit value objects for delete
			@SuppressWarnings("unchecked")
			List<DssUnitValueBO> dssUnitValuesToDelete = dssUnitValueStateMap.getSelected();

			// Delete from system
			dssUnitValueService.deleteDssUnitValues(dssUnitValuesToDelete);

			// Remove the selected rows from the table so the user does not need to reload the page
			for (DssUnitValueBO duv : dssUnitValuesToDelete) {
				dssUnitValues.remove(duv);
			}

			// Clear out the state map
			dssUnitValueStateMap.clear();
			// Delete action complete. Close dialog
			closeDssUnitValueDeletePopup();

		} catch (DssUnitValueServiceException e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			e.printStackTrace();
		}
	}

	/**
	 * save the changes for dss value unit record after edited
	 */
	public void saveEditDssValueUnitRecordChages() {
		try {
			boolean overlap = dssUnitValueService.overlaps(selectedDssValue);
			if (overlap) {
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.DSS.UNIT.VALUE.OVERLAP"));
			}
			// validate bo first
			validator.validate(selectedDssValue);
			if (overlap) {
				return;
			}
		} catch (ValidatorException e) {
			Validator.displayValidationErrors(e);
			logger.error(e.getMessage());
			return;
		} catch (DssUnitValueServiceException e1) {
			Validator.displayServiceError(e1);
			logger.error(e1.getMessage());
			return;
		}

		try {

			// Validate the format of the Unit Value provided
			if (!selectedDssValue.getUnitValue().toString().matches("^([1-9]\\d?|0)(\\.\\d{1,2})?$")) {
				// Value is not in a valid format
				Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR.DSS.UNIT.VALUE.INVALID"));
			} else {
				selectedDssValue.setLastModified(new Date());
				selectedDssValue.setModifiedBy(FacesUtils.getUserId());
				// do the save
				dssUnitValueService.saveOrUpdateDssUnitValue(selectedDssValue);

				// Close edit pop-up
				displayEditDssUnitValue = false;
				closePopupWindowJS();

				// Display save success message
				Validator.displaySystemMessage(FacesMessage.SEVERITY_INFO, "INFO.DSS.UNIT.VALUE.SAVE.SUCCESSFUL");
			}
		}
		// catch service exceptions
		catch (DssUnitValueServiceException e) {
			Validator.displayServiceError(e);
			logger.error(e.getMessage());
		}
		// catch all other system errors
		catch (Exception e) {
			Validator.displayValidationError(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
					"ERROR.APPLICATION.FAILURE"));
			logger.error(e.getMessage());
			e.printStackTrace();
		}

	}

	/**
	 * close up the edit dss value unit pop up page
	 * 
	 */
	public void closeEditDssValuePopup() {
		// display pop up editing window
		displayEditDssUnitValue = false;
		initiateAddEditDssUnitValuePanel();
		closePopupWindowJS();
	}

	/**
	 * Closes the Delete confirmation popup
	 */
	public void closeDssUnitValueDeletePopup() {
		displayDeleteConfirmationDialog = false;
		closePopupWindowJS();
	}

	/**
	 * set muv type code based on the description selected from muv type description drop down
	 * 
	 * @param aSelectedDssUnitValueBO
	 */
	public void selectMuvDescription() {
		selectedDssValue.setUnitValueTypeCode(new BigDecimal(getDssUnitValueDescs().get(
				selectedDssValue.getUnitValueTypeDesc())));
	}

	public void selectMuvDescriptionToSetTypeCode(String description) {
		Long code = getDssUnitValueDescs().get(description);
		dssUnitValueToAdd.setUnitValueTypeCode(new BigDecimal(code));
	}

	public List<DssUnitValueBO> getDssUnitValues() {
		return dssUnitValues;
	}

	public void setDssUnitValues(List<DssUnitValueBO> dssUnitValues) {
		this.dssUnitValues = dssUnitValues;
	}

	public DssUnitValueBO getDssUnitValueToAdd() {
		return dssUnitValueToAdd;
	}

	public void setDssUnitValueToAdd(DssUnitValueBO dssUnitValueToAdd) {
		this.dssUnitValueToAdd = dssUnitValueToAdd;
	}

	public boolean isDisplayEditDssUnitValue() {
		return displayEditDssUnitValue;
	}

	public void setDisplayEditDssUnitValue(boolean displayEditDssUnitValue) {
		this.displayEditDssUnitValue = displayEditDssUnitValue;
	}

	public DssUnitValueBO getSelectedDssValue() {
		return selectedDssValue;
	}

	public void setSelectedDssValue(DssUnitValueBO selectedDssValue) {
		this.selectedDssValue = selectedDssValue;
	}

	/**
	 * @return the dssUnitValueStateMap
	 */
	public RowStateMap getDssUnitValueStateMap() {
		return dssUnitValueStateMap;
	}

	/**
	 * @param dssUnitValueStateMap
	 *            the dssUnitValueStateMap to set
	 */
	public void setDssUnitValueStateMap(RowStateMap dssUnitValueStateMap) {
		this.dssUnitValueStateMap = dssUnitValueStateMap;
	}

	/**
	 * @return the displayDeleteConfirmationDialog
	 */
	public boolean isDisplayDeleteConfirmationDialog() {
		return displayDeleteConfirmationDialog;
	}

	/**
	 * @param displayDeleteConfirmationDialog
	 *            the displayDeleteConfirmationDialog to set
	 */
	public void setDisplayDeleteConfirmationDialog(boolean displayDeleteConfirmationDialog) {
		this.displayDeleteConfirmationDialog = displayDeleteConfirmationDialog;
	}
}
