package ca.medavie.nspp.audit.ui.controller.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.MedicareAuditLetterResponse;

public class MedicareALRValidator extends Validator<MedicareAuditLetterResponse> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(MedicareAuditLetterResponse aMedicareALR) throws ValidatorException {
		List<ValidatorMessage> errorMsgs = new ArrayList<ValidatorMessage>();

		// Validate the value length of Charge Back amount (10,2)
		if (aMedicareALR.getChargeback_amount() != null) {
			if (aMedicareALR.getChargeback_amount().compareTo(BigDecimal.valueOf(99999999.99)) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.CHARGE.BACK.INVALID.MAX"));
			}
		}

		// Validate the value length of Number of Claims Affected (7)
		if (aMedicareALR.getNumber_of_services_affected() != null) {
			if (aMedicareALR.getNumber_of_services_affected().compareTo(Integer.valueOf(9999999)) == 1) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.CLAIMS.AFFECTED.INVALID.MAX"));
			}
		}

		if (!errorMsgs.isEmpty()) {
			throw new ValidatorException(errorMsgs);
		}
	}
}
