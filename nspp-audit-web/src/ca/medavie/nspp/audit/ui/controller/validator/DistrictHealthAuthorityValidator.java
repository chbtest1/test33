package ca.medavie.nspp.audit.ui.controller.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.data.DistrictHealthAuthority;

public class DistrictHealthAuthorityValidator extends Validator<DistrictHealthAuthority> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(DistrictHealthAuthority aDha) throws ValidatorException {
		// Check that all required fields are provided
		List<ValidatorMessage> toRet = new ArrayList<ValidatorMessage>();

		if (aDha.getFte() == null) {
			toRet.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DHA.FTE.REQUIRED"));
		} else {
			// FTE provided.. check that is 0 and up
			if (aDha.getFte().compareTo(BigDecimal.ZERO) < 0) {
				toRet.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DHA.FTE.INVALID"));
			}
		}
		if (aDha.getContractTownName() == null) {
			toRet.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DHA.CONTRACT.TOWN.REQUIRED"));
		}
		if (aDha.getZoneId() == null) {
			toRet.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DHA.ZONE.ID.REQUIRED"));
		}
		if (aDha.getZoneName() == null) {
			toRet.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DHA.ZONE.NAME.REQUIRED"));
		}
		if (aDha.getDhaCode() == null) {
			toRet.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DHA.CODE.REQUIRED"));
		}
		if (aDha.getDhaDescription() == null) {
			toRet.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DHA.NAME.REQUIRED"));
		}
		if (!toRet.isEmpty()) {
			throw new ValidatorException(toRet);
		}
	}
}
