package ca.medavie.nspp.audit.ui.controller.validator;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.DssGLNumbersService;
import ca.medavie.nspp.audit.service.data.DssGLNumberBO;
import ca.medavie.nspp.audit.service.exception.DssGLNumbersServiceException;

public class DssGLNumbersValidator extends Validator<DssGLNumberBO> {

	@EJB
	protected DssGLNumbersService dssGLNumbersService;

	/**
	 * Since we need the EJB service for parts of the validation we pass it on construction of Validator class
	 * 
	 * @param aDssGLNumbersService
	 */
	public DssGLNumbersValidator(DssGLNumbersService aDssGLNumbersService) {
		this.dssGLNumbersService = aDssGLNumbersService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(DssGLNumberBO t) throws ValidatorException {

		List<ValidatorMessage> validatorMsgs = new ArrayList<ValidatorMessage>();

		/**
		 * Currently the validation for DSS GL Number only applies to newly created records
		 * 
		 * 1) Provided GL Number must not be in use by another record
		 */

		/*
		 * Try and load a record by the specified GL Number. If record returns compare the GL numbers and if they match
		 * GL Number is in use
		 */
		DssGLNumberBO duplicateGL;
		try {
			duplicateGL = dssGLNumbersService.getGLNumberByGL(t.getGlNumber());

			if (duplicateGL != null && duplicateGL.getGlNumber() != null) {
				
				if (t.getGlNumber().compareTo(duplicateGL.getGlNumber()) == 0) {
					// GL Number is in use. Add error
					validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.DSS.GL.NUMBER.ALREADY.IN.USE"));
				}
			}
		} catch (DssGLNumbersServiceException e) {
			validatorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DSS.GL.NUMBERS.SERVICE"));
			e.printStackTrace();
		}

		if (!validatorMsgs.isEmpty()) {
			throw new ValidatorException(validatorMsgs);
		}
	}

}
