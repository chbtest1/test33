package ca.medavie.nspp.audit.ui.controller.converter;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * Clean Strings that will be converted to Integers.
 * 
 */
@FacesConverter("integerConverter")
public class IntegerConverter implements Converter {

	private final String msgBundleLocation = "ca.medavie.nspp.audit.ui.messages";

	public IntegerConverter() {
		super();
	}

	/**
	 * Clean String so that it can be converted to an Integer.
	 * 
	 * @param aContext
	 *            The FacesContext
	 * @param aComponent
	 *            The UIComponent
	 * @param aString
	 *            The String to convert
	 * 
	 * @return Cleaned String
	 */
	public Object getAsObject(FacesContext aContext, UIComponent aComponent, String aString) {

		// Load the currently used locale
		Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

		try {
			if (aString == null || aString.trim().equals("")) {
				return null;
			}

			aString = aString.replaceAll("[\\$\\,\\s]", "");
			DecimalFormat decimalFormatter = new DecimalFormat("0.##");
			return decimalFormatter.format(new Integer(aString));

		} catch (Exception e) {
			ResourceBundle messageBundle = ResourceBundle.getBundle(msgBundleLocation, currentLocale);
			String errorMsg = messageBundle.getString("ERROR.APPLICATION.INTEGER");
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, errorMsg);
			throw new ConverterException(fm);
		}
	}

	/**
	 * Convert an Integer to a string
	 * 
	 * @param aContext
	 *            The FacesContext
	 * @param aComponent
	 *            The UIComponent
	 * @param anObj
	 *            The Integer to convert
	 * 
	 * @return The String, converted from the Integer.
	 */
	public String getAsString(FacesContext aContext, UIComponent aComponent, Object anObj) {

		Integer aValue = null;
		// Grab the current locale
		Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

		// Check if the page is using the grouping attribute (If no attribute is provided it defaults to false)
		boolean groupingDisabled = new Boolean((String) aComponent.getAttributes().get("disableGrouping"));

		try {
			// Check to make sure the Object is a String that can be converted to a number.
			// Value needs to be valid number with no symbols.
			String aValueStr = anObj.toString();
			aValue = new Integer(aValueStr);

		} catch (NumberFormatException e) {
			ResourceBundle messageBundle = ResourceBundle.getBundle(msgBundleLocation, currentLocale);
			String errorMsg = messageBundle.getString("ERROR.APPLICATION.INTEGER");
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, errorMsg);
			throw new ConverterException(fm);
		}

		DecimalFormat formatter = (DecimalFormat) NumberFormat.getIntegerInstance(currentLocale);
		// If field is not using group.. disable it on formatter
		if (groupingDisabled) {
			formatter.setGroupingUsed(false);
		}

		return formatter.format(aValue);
	}
}