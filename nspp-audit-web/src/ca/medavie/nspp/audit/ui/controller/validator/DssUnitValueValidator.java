package ca.medavie.nspp.audit.ui.controller.validator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;

import ca.medavie.nspp.audit.service.DssUnitValueService;
import ca.medavie.nspp.audit.service.data.DssUnitValueBO;
import ca.medavie.nspp.audit.service.exception.DssUnitValueServiceException;

/**
 * Used for validating PractitionerProfilePeriods
 */
public class DssUnitValueValidator extends Validator<DssUnitValueBO> {

	@EJB
	protected DssUnitValueService dssUnitValueService;

	/**
	 * Validator constructor. Need the service for some validation steps
	 * 
	 * @param aDssUnitValueService
	 */
	public DssUnitValueValidator(DssUnitValueService aDssUnitValueService) {
		dssUnitValueService = aDssUnitValueService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ca.medavie.nspp.audit.ui.controller.validator.Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(DssUnitValueBO aDssUnitValueBO) throws ValidatorException {
		List<ValidatorMessage> errorMsgs = new ArrayList<ValidatorMessage>();

		// Verify the DSS record does not overlap with another existing record
		try {
			if (dssUnitValueService.overlaps(aDssUnitValueBO)) {
				errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DSS.UNIT.VALUE.OVERLAP"));
			}

			// it is not valid if lower threshold is great equal than upper threshold
			if (aDssUnitValueBO.getLowerThreshold() != null) {
				if (aDssUnitValueBO.getLowerThreshold().compareTo(aDssUnitValueBO.getUpperThreshold()) > 0) {

					errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.DSS.UNIT.VALUE.LOWER.UPPER.INVALID"));
				}

				// Validate the value length of Lower Threshold
				if (aDssUnitValueBO.getLowerThreshold().compareTo(BigDecimal.valueOf(99999999.99)) == 1) {
					errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.DSS.UNIT.LOWER.THRESH.INVALID.MAX"));
				}
			}

			// Validate the value length of Upper Threshold
			if (aDssUnitValueBO.getUpperThreshold() != null) {
				if (aDssUnitValueBO.getUpperThreshold().compareTo(BigDecimal.valueOf(99999999.99)) == 1) {
					errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.DSS.UNIT.UPPER.THRESH.INVALID.MAX"));
				}
			}

			// Validate the value length of MUV Dollar amount (5,2)
			if (aDssUnitValueBO.getUnitValue() != null) {
				if (aDssUnitValueBO.getUnitValue().compareTo(BigDecimal.valueOf(999.99)) == 1) {
					errorMsgs
							.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DSS.UNIT.VALUE.INVALID.MAX"));
				}
			}

			// it is not valid if effective to date is early than effective from date
			if (aDssUnitValueBO.getEffectiveToDate() != null) {
				if (aDssUnitValueBO.getEffectiveToDate().compareTo(aDssUnitValueBO.getEffectiveFromDate()) < 0) {

					errorMsgs.add(new ValidatorMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR.DSS.UNIT.VALUE.EFFECTIVE.FROM.TO.INVALID"));
				}
			}
		} catch (DssUnitValueServiceException e) {
			errorMsgs.add((new ValidatorMessage(FacesMessage.SEVERITY_ERROR, "ERROR.DSS.UNIT.VALUE.SERVICE")));
			e.printStackTrace();
		}

		// Throw Validator exception if errors were found
		if (!errorMsgs.isEmpty()) {
			throw new ValidatorException(errorMsgs);
		}
	}
}
