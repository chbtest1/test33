package ca.medavie.nspp.audit.ui.bean;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * This utility bean is serves has a access point to the resource environment provider.
 * 
 * @author bcpoiri
 *
 */
public class ResourceEnvironmentUtilBean {

	public String jndiName;

	/**
	 * Returns the JNDI name.
	 * 
	 * @return Returns the JNDI name.
	 */
	public String getJndiName() {
		return jndiName;
	}

	/**
	 * Sets the JNDI name.
	 * 
	 * @param jndiName JNDI name.
	 */
	public void setJndiName(String jndiName) {
		this.jndiName = jndiName;
	}

	/**
	 * Returns resource environment entry attribute.
	 * 
	 * @param attributeName The attributeName
	 * 
	 * @return Returns resource environment entry attribute.
	 * 
	 * @throws NamingException When the jndi name is invalid.
	 */
	public Object getAttribute(String attributeName) throws NamingException {

		Object attribute = null;

		//get initial context.
		Context ctx = new InitialContext();

		if (ctx != null) {
			
			//lookup REE config object.
			java.util.Properties config = (java.util.Properties) ctx.lookup(this.jndiName);
			
			if (config != null) {
				
				//lookup resource environment entry attribute.
				attribute = config.get(attributeName);
			}
			
		}

		return attribute;
	}

}
