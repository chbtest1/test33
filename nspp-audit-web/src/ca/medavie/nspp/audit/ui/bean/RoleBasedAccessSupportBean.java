package ca.medavie.nspp.audit.ui.bean;

import java.io.Serializable;

import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import ca.medavie.nspp.audit.security.RoleBasedSecurityUtil;
import ca.medavie.nspp.audit.ui.FacesUtils;

@ManagedBean(name = "roleBasedAccessSupport")
@SessionScoped
public class RoleBasedAccessSupportBean implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * @param roles (Comma delimited list of required roles)
	 * @return true if user has any of the given roles, false if user doesn't
	 */
	public boolean hasRole(String roles) {
		
		//System.out.println(roles);
		//System.out.println(RoleBasedSecurityUtil.getUserRoles().contains(roles));
		
		boolean hasRole = false;
		String[] roleNames = roles.split(",");
		for (String roleName : roleNames) {
			if (RoleBasedSecurityUtil.getUserRoles().contains(roleName)) {
				hasRole = true; 
				break;
			}
		}
		return hasRole;
	}

	/**
	 * Get the user id.
	 * @return UserId
	 */
	public String getUserIn() {
		if ((FacesContext.getCurrentInstance() != null)
				&& (FacesContext.getCurrentInstance().getExternalContext() != null)
				&& (FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal() != null)) {
			return FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
		} else {
			return null;
		}
	}
	
	public boolean hasDeleteRecordAuditNotes() {
		String users = FacesUtils.getVariable("RecordAuditNotesDeleteUser").toUpperCase();
		if (users == null) users = "BCCLEGG";
		return (users.indexOf(getUserIn().toUpperCase())>=0)?true:false;
	}

	/**
	 * Called from pre-render view to check security for screen.
	 * @param requiredRole
	 */
	public void checkSecurity(String requiredRoles) {
		// No roles to check given, assume they have access.
		if ((requiredRoles==null)||(requiredRoles.equals(""))) return;
		// Check if user has one of the required roles granted to them.
		String[] roles = requiredRoles.split(",");
		for (String role : roles) {
			if (hasRole(role)) {
				return;
			} 
		}
		// If they don't, return the user to the Home (index) page.
		FacesContext facesContext = FacesContext.getCurrentInstance();
		NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
		nh.handleNavigation(facesContext, null, "Home");
	}
}
