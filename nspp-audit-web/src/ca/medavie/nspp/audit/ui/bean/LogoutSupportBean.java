package ca.medavie.nspp.audit.ui.bean;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This bean is responsible to support the logout process.
 * 
 * @author bcpoiri
 */
@ManagedBean(name = "logoutSupport")
@SessionScoped
public class LogoutSupportBean {

	private boolean logoutVisible;
	
	private static final Logger logger = LoggerFactory.getLogger(LogoutSupportBean.class);
	
	//private static final String LOGOUT_URL_KEY = "app.logout.url";
	
	//private static final String LOGOUT_URL_LOG = "Logout url={0}";
	
	private static final String MALFORMED_URL_EXCEPTION_LOG = 
		"An unexpected error occured while calling the logout url, the logout url is invalid";
	
	//private static final String NAMING_EXCEPTION_LOG =
		//"An unexpected error occured while getting the logout url, the jndi name is invalid";
	
	//private static final String SYSTEM_ERROR_LOG =
		//"An unexpected error occured while calling the logout url";

	@ManagedProperty(value = "#{resourceEnvironmentUtil}")
	private ResourceEnvironmentUtilBean resourceEnvironmentUtil;

	/**
	 * Returns true if logout is visible, otherwise returns false.
	 * 
	 * @return Returns true if logout is visible, otherwise returns false.
	 */
	public boolean isLogoutVisible() {
		return logoutVisible;
	}

	/**
	 * Sets if the logout is visible
	 * 
	 * @param visible
	 *            boolean value
	 */
	public void setLogoutVisible(boolean visible) {
		this.logoutVisible = visible;
	}

	/**
	 * Returns the resource env util
	 * 
	 * @return Returns the resource env util
	 */
	public ResourceEnvironmentUtilBean getResourceEnvironmentUtil() {
		return resourceEnvironmentUtil;
	}

	/**
	 * Sets the resource env util.
	 * 
	 * @param resourceEnvironmentUtil The resource env util.
	 */
	public void setResourceEnvironmentUtil(
			ResourceEnvironmentUtilBean resourceEnvironmentUtil) {
		this.resourceEnvironmentUtil = resourceEnvironmentUtil;
	}

	/**
	 * This method is responsible to listen when display button is clicked.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void displayLogout(AjaxBehaviorEvent evt) {
		this.logoutVisible = true;
	}

	/**
	 * This method is responsible to listen when cancel button is clicked.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void cancelLogout(AjaxBehaviorEvent evt) {
		this.logoutVisible = false;
	}

	/**
	 * This method is responsible to listen when the confirm button is clicked.
	 * 
	 * @param evt
	 *            The ajax behjviro event.
	 */
	public void confirmLogout(ActionEvent e) {

		this.logoutVisible = false;

		FacesContext ctx = FacesContext.getCurrentInstance();

		String logoutURL = ctx.getExternalContext().getRequestContextPath() + "/pages/logout.html";
		try {
			ctx.getExternalContext().redirect(logoutURL);
		} catch (IOException ioe) {
			logger.error(MALFORMED_URL_EXCEPTION_LOG, ioe);
		}
	}
}
