package ca.medavie.nspp.audit.ui.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * This class is responsible to handle the data export features of ICEFaces
 * 
 * @author bcpoiri
 */
@ManagedBean(name="dataExportSupport")
@SessionScoped
public class DataExportSupportBean {

	private String fileType;
	
	private static final String DEFAULT_FILE_TYPE = "csv";
	
	/**
	 * Constructor
	 */
	public DataExportSupportBean() {
		this.fileType=DEFAULT_FILE_TYPE;
	}

	/**
	 * Returns the file type to export to.
	 * 
	 * @return Returns the file type to export to.
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * Sets the file type to export to.
	 * 
	 * @param fileType The file type to export to.
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

}
