package ca.medavie.nspp.audit.ui.bean;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;

import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.SortCriteria;

/**
 * This class represents the parent class for all crud controller bean that
 * implements lazy loading.
 * 
 * @author bcpoiri
 * 
 * @param <T>
 *            The model class
 */
public abstract class LazyCrudControllerBean<T> extends CrudControllerBean<T> {

	private ResultSet<T> cacheResultSet;
	private boolean refreshCacheResultSet;

	/**
	 * This inner class represents a result set.
	 */
	@SuppressWarnings("hiding")
	public class ResultSet<T> {

		private List<T> results;
		private int rowCount;

		/**
		 * Returns the list of results.
		 * 
		 * @return Returns the list of results.
		 */
		public List<T> getResults() {
			return results;
		}

		/**
		 * Sets the list of results.
		 * 
		 * @param results
		 *            The list of results.
		 */
		public void setResults(List<T> results) {
			this.results = results;
		}

		/**
		 * Returns the row count.
		 * 
		 * @return Returns the row count.
		 */
		public int getRowCount() {
			return rowCount;
		}

		/**
		 * Sets the row count
		 * 
		 * @param rowCount
		 *            The row count.
		 */
		public void setRowCount(int rowCount) {
			this.rowCount = rowCount;
		}
	}

	/**
	 * Constructor
	 * 
	 * @param modelClass
	 *            The model class
	 */
	public LazyCrudControllerBean(Class<T> modelClass) {
		super(modelClass);

		// by default we always want to retrieve the data from the db.
		this.refreshCacheResultSet = true;
	}

	/**
	 * Initialize bean.
	 */
	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {

		super.init();

		// create a new data model.
		this.dataModel = new LazyDataModel<T>() {

			/**
			 * default serizalisation uid.
			 */
			private static final long serialVersionUID = -3122820186890478208L;

			/**
			 * @see org.icefaces.ace.model.table.LazyDataModel#load(int, int,
			 *      org.icefaces.ace.model.table.SortCriteria[], java.util.Map)
			 */
			@Override
			public List<T> load(int first, int pageSize,
					SortCriteria[] criteria, Map<String, String> filters) {

				if (refreshCacheResultSet) {

					try {

						//find the records for given page, filter criteria and sort criteria
						cacheResultSet = onListStateLoad(first, pageSize,
								criteria, filters);

						//set the number of rows returned.
						((LazyDataModel<T>) dataModel)
								.setRowCount(cacheResultSet.getRowCount());

					} catch (Throwable e) {
						onListStateError(e);
					}
					
				} else {
					
					//after the cancel has occured set this back to true has a fail-safe
					refreshCacheResultSet = true;
				}

				//return rows from the result set.
				return cacheResultSet.getResults();
			}
		};
	}

	/**
	 * @see ca.medavie.bootstrap.ui.bean.CrudControllerBean#queryRows();
	 */
	@Override
	public List<T> onListStateLoad() {
		return null;
	}

	/**
	 * This method is responsible to query for rows for the page.
	 * 
	 * @param first
	 *            The index of the first row.
	 * @param pageSize
	 *            The number of records per page.
	 * @param criteria
	 *            The sort criteria
	 * @param filters
	 *            The filter
	 * 
	 * @return Returns a list of models for the page.
	 * 
	 * @throws DataAccessException
	 *             When unexpected error occurs from the database.
	 */
	public abstract ResultSet<T> onListStateLoad(int first, int pageSize,
			SortCriteria[] criteria, Map<String, String> filters);

	/**
	 * @see ca.medavie.nspp.audit.ui.bean.CrudControllerBean#onViewStateCancel(javax.faces.event.AjaxBehaviorEvent)
	 */
	@Override
	public void onViewStateCancel(AjaxBehaviorEvent evt) {
		
		super.onViewStateCancel(evt);
		
		//no need to do unnecessary call to the db for a cancel action.
		this.refreshCacheResultSet = false;
	}

	/**
	 * @see ca.medavie.nspp.audit.ui.bean.CrudControllerBean#onEditStateCancel(javax.faces.event.AjaxBehaviorEvent)
	 */
	@Override
	public void onEditStateCancel(AjaxBehaviorEvent evt) {
		
		super.onEditStateCancel(evt);
		
		//no need to do unnecessary call to the db for a cancel action.
		this.refreshCacheResultSet = false;
	}

	/**
	 * @see ca.medavie.nspp.audit.ui.bean.CrudControllerBean#onNewStateCancel(javax.faces.event.AjaxBehaviorEvent)
	 */
	@Override
	public void onNewStateCancel(AjaxBehaviorEvent evt) {
		
		super.onNewStateCancel(evt);
		
		//no need to do unnecessary call to the db for a cancel action.
		this.refreshCacheResultSet = false;
	}

	/**
	 * @see ca.medavie.nspp.audit.ui.bean.CrudControllerBean#onDeleteStateCancel(javax.faces.event.AjaxBehaviorEvent)
	 */
	@Override
	public void onDeleteStateCancel(AjaxBehaviorEvent evt) {
		
		super.onDeleteStateCancel(evt);
		
		//no need to do unnecessary call to the db for a cancel action.
		this.refreshCacheResultSet = false;
	}
}
