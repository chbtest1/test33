package ca.medavie.nspp.audit.ui.bean;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 * This class represents the base controller class for all CRUD controller
 * beans.
 * 
 * @author bcpoiri
 * 
 * @param <T>
 */
public abstract class CrudControllerBean<T>  {

	/**
	 * This enumeration represents the different states in the CRUD.
	 */
	protected enum State {
		VIEW, LIST, EDIT, DELETE, NEW
	}

	private State state;

	protected DataModel<T> dataModel;

	private T model;

	private T selectedModel;

	private Class<T> modelClass;

	/**
	 * Constructor
	 * 
	 */
	public CrudControllerBean(Class<T> modelClass) {
		this.modelClass = modelClass;
	}

	/**
	 * initialize the bean.
	 */
	public void init() {

		// set the initial state to LIST_VIEW
		this.setState(State.LIST);

		List<T> rows = null;

		try {

			// query rows.
			rows = onListStateLoad();
			
			if (rows != null && rows.isEmpty()) {
				this.dataModel = new ListDataModel<T>(rows);
			} else {
				// create new data model.
				this.dataModel = new ListDataModel<T>();
			}

		} catch (Throwable e) {
			onListStateError(e);
		}
	}

	/**
	 * Returns the data model.
	 * 
	 * @return Returns the data model.
	 */
	public DataModel<T> getDataModel() {
		return dataModel;
	}

	/**
	 * Sets the data model.
	 * 
	 * @param dataModel
	 *            The data model.
	 */
	public void setDataModel(DataModel<T> dataModel) {
		this.dataModel = dataModel;
	}

	/**
	 * Returns the model the controller supports.
	 * 
	 * @return Returns the model the controller supports.
	 */
	public T getModel() {
		return model;
	}

	/**
	 * Sets the model the controller supports.
	 * 
	 * @param model
	 */
	public void setModel(T model) {
		this.model = model;
	}

	/**
	 * Returns the selected model.
	 * 
	 * @return Returns the selected model.
	 */
	public T getSelectedModel() {
		return selectedModel;
	}

	/**
	 * Sets the selected model.
	 * 
	 * @param selectedModel The selected model.
	 */
	public void setSelectedModel(T selectedModel) {
		this.selectedModel = selectedModel;
	}

	/**
	 * Returns the state.
	 * 
	 * @return Returns the state.
	 */
	protected State getState() {
		return state;
	}

	/**
	 * Sets the state.
	 * 
	 * @param state
	 *            The state.
	 */
	protected void setState(State state) {
		this.state = state;
	}

	/**
	 * Returns the list of rows to be displayed.
	 * 
	 * @return Returns the list of rows to be displayed.
	 */
	public abstract List<T> onListStateLoad();

	/**
	 * This method is responsible to handle error from the list state.
	 * 
	 * @param e
	 *            Throwable.
	 */
	public abstract void onListStateError(Throwable e);

	/**
	 * This method is responsible to handle when the user clicks on the view
	 * button.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void onViewStateInit(AjaxBehaviorEvent evt) {

		try {

			// save the selected model to be later use for the update/delete
			this.selectedModel = (T) this.getDataModel().getRowData();

			// copy the selected properties to the model.
			this.onViewStateInit(evt, this.model, (T) this.selectedModel);

			this.setState(State.VIEW);

		} catch (Throwable e) {
			onListStateError(e);
		}
	}

	/**
	 * This method responsible to handle the view state init event.
	 * 
	 * @param evt The ajax behavior event
	 * @param model The model.
	 * @param currentRow The current row.
	 */
	public abstract void onViewStateInit(AjaxBehaviorEvent evt, T model,
			T currentRow);

	/**
	 * This method is responsible to handle error from the view state.
	 * 
	 * @param e
	 *            Throwable.
	 */
	public abstract void onViewStateError(Throwable e);

	/**
	 * This method is responsible to handle when the user clicks on the view
	 * cancel button.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void onViewStateCancel(AjaxBehaviorEvent evt) {

		try {
			this.setState(State.LIST);
		} catch (Throwable e) {
			onViewStateError(e);
		}
	}

	/**
	 * This method responsible to map the current row to model
	 * 
	 * @param model
	 *            The model.
	 * @param currentRow
	 *            The current row.
	 * 
	 */
	public abstract void onEditStateInit(AjaxBehaviorEvent evt, T model,
			T currentRow);

	/**
	 * This method is responsible to handle when the user clicks on the edit
	 * button.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void onEditStateInit(AjaxBehaviorEvent evt) {

		try {

			// save the selected model to be later use for the update/delete
			this.selectedModel = (T) this.getDataModel().getRowData();

			this.onEditStateInit(evt, this.model, (T) this.selectedModel);

			this.setState(State.EDIT);

		} catch (Throwable e) {
			onListStateError(e);
		}
	}

	/**
	 * This method is responsible to update the row.
	 * 
	 * @param model
	 *            The model.
	 * 
	 * @throws DataAccessException
	 */
	public abstract void onEditStateConfirm(AjaxBehaviorEvent e, T model);

	/**
	 * This method is responsible to handle when the user clicks on edit confirm
	 * button.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void onEditStateConfirm(AjaxBehaviorEvent evt) {

		try {

			// update the model in the database.
			this.onEditStateConfirm(evt, this.getModel());

			if (isStateValid()) {

				// update the row on the grid.
				onEditStateInit(evt, this.selectedModel, this.getModel());

				this.setState(State.LIST);
			}

		} catch (Throwable e) {
			onEditStateError(e);
		}
	}

	/**
	 * This method is responsible to handle update error.
	 * 
	 * @param e
	 *            The exception.
	 */
	public abstract void onEditStateError(Throwable e);

	/**
	 * This method is responsible to handle when the user clicks on the edit
	 * cancel button.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void onEditStateCancel(AjaxBehaviorEvent evt) {

		try {
			this.setState(State.LIST);
		} catch (Throwable e) {
			onEditStateError(e);
		}
	}

	/**
	 * This method responsible to when new row is created.
	 * 
	 * @param model
	 *            The model.
	 */
	public abstract void onNewStateInit(AjaxBehaviorEvent evt, T model);

	/**
	 * This method is responsible
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void onNewStateInit(AjaxBehaviorEvent evt) {

		try {
			
			this.onNewStateInit(evt, this.model);
			this.setState(State.NEW);
		} catch (Throwable e) {
			onListStateError(e);
		}
	}

	/**
	 * This method is responsible to add the row.
	 * 
	 * @param model
	 *            The model.
	 * 
	 * @throws DataAccessException
	 */
	public abstract void onNewStateConfirm(AjaxBehaviorEvent evt, T model);

	/**
	 * This method is responsible to handle when the user clicks on new confirm
	 * button.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void onNewStateConfirm(AjaxBehaviorEvent evt) {

		try {

			// persist the model to the database.
			onNewStateConfirm(evt, this.getModel());

			if (isStateValid()) {

				// get the list of models.
				@SuppressWarnings("unchecked")
				List<T> modelBeans = (List<T>) this.getDataModel()
						.getWrappedData();

				T t = (T) modelClass.newInstance();

				// update the row on the grid.
				onEditStateInit(evt, t, this.getModel());

				if (modelBeans != null) {
					
					// add new model to the list so its reflected on the list.
					modelBeans.add(t);
				}

				this.setState(State.LIST);

			}

		} catch (Throwable e) {
			onNewStateError(e);
		}
	}

	/**
	 * This method is responsible to handle error from the new state.
	 * 
	 * @param e
	 *            Throwable.
	 */
	public abstract void onNewStateError(Throwable e);

	/**
	 * This method is responsible to handle when the user clicks on new cancel
	 * button.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void onNewStateCancel(AjaxBehaviorEvent evt) {

		try {
			this.setState(State.LIST);
		} catch (Throwable e) {
			onNewStateError(e);
		}
	}

	/**
	 * This method is responsible to handle when the user clicks on delete button.
	 * 
	 * @param evt The action event.
	 * @param model
	 */
	public abstract void onDeleteStateInit(AjaxBehaviorEvent evt, T model);

	/**
	 * This method is responsible to handle when the user clicks on the delete
	 * button.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void onDeleteStateInit(AjaxBehaviorEvent evt) {

		try {

			// set the selected model to be deleted.
			this.selectedModel = this.getDataModel().getRowData();

			onDeleteStateInit(evt, this.selectedModel);

			this.setState(State.DELETE);

		} catch (Throwable e) {
			onListStateError(e);
		}
	}

	/**
	 * This method is responsible to delete the row.
	 * 
	 * @param model
	 *            The model.
	 * 
	 * @throws DataAccessException
	 */
	public abstract void onDeleteStateConfirm(AjaxBehaviorEvent evt, T model);

	/**
	 * This method is responsible to handle when the user clicks on delete
	 * confirm
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void onDeleteStateConfirm(AjaxBehaviorEvent evt) {

		try {

			this.onDeleteStateConfirm(evt, this.getSelectedModel());

			if (isStateValid()) {

				// get the list of models.
				@SuppressWarnings("unchecked")
				List<T> modelBeans = (List<T>) this.getDataModel()
						.getWrappedData();
				
				if (modelBeans != null) {
					
					// remove the selected model from the list
					modelBeans.remove(this.getSelectedModel());
				}
			}

		} catch (Throwable e) {
			onDeleteStateError(e);
		} finally {
			this.setState(State.LIST);
		}
	}

	/**
	 * This method is responsible to handle delete state error.
	 * 
	 * @param e
	 *            The exception.
	 */
	public abstract void onDeleteStateError(Throwable e);

	/**
	 * This method is responsible to handle
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void onDeleteStateCancel(AjaxBehaviorEvent evt) {

		try {
			this.setState(State.LIST);
		} catch (Throwable e) {
			onDeleteStateError(e);
		}
	}

	/**
	 * This method is checks if the state is the new state.
	 * 
	 * @return Returns true if the new state, otherwise returns false.
	 */
	public boolean isNewState() {

		boolean isNewState = false;

		if (this.state.equals(State.NEW)) {
			isNewState = true;
		}

		return isNewState;
	}

	/**
	 * This method is checks if the state is the edit state.
	 * 
	 * @return Returns true if the edit state, otherwise returns false.
	 */
	public boolean isEditState() {

		boolean isEditState = false;

		if (this.state.equals(State.EDIT)) {
			isEditState = true;
		}

		return isEditState;
	}

	/**
	 * This method is checks if the state is the view state.
	 * 
	 * @return Returns true if the view state, otherwise returns false.
	 */
	public boolean isViewState() {

		boolean isViewState = false;

		if (this.state.equals(State.VIEW)) {
			isViewState = true;
		}

		return isViewState;
	}

	/**
	 * This method is checks if the state is the list view state.
	 * 
	 * @return Returns true if the list view state, otherwise returns false.
	 */
	public boolean isListState() {

		boolean isListState = false;

		if (this.state.equals(State.LIST)) {
			isListState = true;
		}

		return isListState;
	}

	/**
	 * This method is checks if the state is the delete state.
	 * 
	 * @return Returns true if the delete state, otherwise returns false.
	 */
	public boolean isDeleteState() {

		boolean isDeleteState = false;

		if (this.state.equals(State.DELETE)) {
			isDeleteState = true;
		}

		return isDeleteState;
	}

	/**
	 * Check if the state is valid, returns true if no faces messages found
	 * otherwise returns false.
	 * 
	 * @return Returns true if no faces messages found otherwise returns false.
	 */
	public boolean isStateValid() {

		FacesContext ctx = FacesContext.getCurrentInstance();

		boolean isValid = true;

		if (ctx.getMessageList() != null
				&& !ctx.getMessageList().isEmpty()
				&& (ctx.getMaximumSeverity()
						.equals(FacesMessage.SEVERITY_ERROR) || ctx
						.getMaximumSeverity().equals(
								FacesMessage.SEVERITY_FATAL))) {
			
			isValid = false;
		}

		return isValid;
	}

	/*
	 * These methods are only implements as dummies they serve no purpose
	 */
	public void setNewState(boolean state) {
	}

	public void setEditState(boolean state) {
	}

	public void setViewState(boolean state) {
	}

	public void setDeleteState(boolean state) {
	}
}
