package ca.medavie.nspp.audit.ui.bean;

import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

/**
 * This bean is responsible to support changing settings.
 * 
 * @author bcpoiri
 */
@ManagedBean(name = "settingsSupport")
@SessionScoped
public class SettingsSupportBean {

	private boolean settingsVisible;
	private Locale locale;

	/**
	 * Returns the locale.
	 * 
	 * @return Returns the locale.
	 */
	public Locale getLocale() {

		if (this.locale == null) {
			this.locale = Locale.ENGLISH;
		}

		return this.locale;
	}

	/**
	 * Sets the locale.
	 * 
	 * @param locale
	 *            The locale.
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * Returns true if the settings dialog is visible, otherwise returns false.
	 * 
	 * @return Returns true if the settings dialog is visible, otherwise returns
	 *         false.
	 */
	public boolean getSettingsVisible() {
		return settingsVisible;
	}

	/**
	 * Sets if the settings dialog is visible.
	 * 
	 * @param visible
	 *            boolean value
	 */
	public void setSettingsVisible(boolean visible) {
		this.settingsVisible = visible;
	}

	/**
	 * This method is responsible to listen when the edit button is clicked.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void editSettings(AjaxBehaviorEvent evt) {
		this.settingsVisible = true;
	}

	/**
	 * This method is responsible to listen when the cancel button is clicked.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void cancelSettings(AjaxBehaviorEvent evt) {
		this.settingsVisible = false;
	}

	/**
	 * This method is responsible to listen when the save button is clicked.
	 * 
	 * @param evt
	 *            The ajax behavior event.
	 */
	public void saveSettings(AjaxBehaviorEvent evt) {

		// get the faces context
		FacesContext facesContext = FacesContext.getCurrentInstance();

		// update the locale on the view root.
		facesContext.getViewRoot().setLocale(locale);

		// hide settings dialog.
		this.settingsVisible = false;
	}

}
