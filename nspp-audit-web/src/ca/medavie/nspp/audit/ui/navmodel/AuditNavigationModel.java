/*
 * Copyright 2004-2013 ICEsoft Technologies Canada Corp.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS
 * IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package ca.medavie.nspp.audit.ui.navmodel;

import java.io.Serializable;

import javax.faces.bean.CustomScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = AuditNavigationModel.BEAN_NAME)
@CustomScoped(value = "#{window}")
public class AuditNavigationModel implements Serializable {

	/** JRE generated */
	private static final long serialVersionUID = -5162774242278829465L;
	/** Name of bean. */
	public static final String BEAN_NAME = "auditNavigationModel";

	private String currentPage = "index";

	/** Links found under the Outlier under the Audit menu */
	private static String[][] outlierMenuLinks = {
			{ "audit/OutlierIdentification/Criteria/CriteriaIndex.html", "menu.audit.outlier.criteria" },
			{ "audit/OutlierIdentification/PeerGroups/PeerGroupsIndex.html", "menu.audit.outlier.peer" },
			{ "audit/OutlierIdentification/HSCodes/HealthServiceIndex.html", "menu.audit.outlier.health" },
			{ "audit/OutlierIdentification/Inquiry/InquiryIndex.html", "menu.audit.outlier.inquiry" } };

	/** Links found under the Service Verification Letters under the Audit menu */
	private static String[][] serviceVerificationMenuLinks = {
			{ "audit/ServiceVerificationLetters/Medicare/MedicareIndex.html", "menu.audit.service.medicare" },
			{ "audit/ServiceVerificationLetters/Denticare/DenticareIndex.html", "menu.audit.service.denticare" },
			{ "audit/ServiceVerificationLetters/Pharmacare/PharmacareIndex.html", "menu.audit.service.pharmacare" } };

	/** Links found under the Audit Results under the Audit menu */
	private static String[][] auditResultsMenuLinks = { { "audit/AuditResults/PractitionerAudit/AuditResultIndex.html",
			"menu.audit.results.record.audits" } };

	/**
	 * @return the outlierMenuLinks
	 */
	public String[][] getOutlierMenuLinks() {
		return outlierMenuLinks;
	}

	/**
	 * @return the serviceVerificationMenuLinks
	 */
	public String[][] getServiceVerificationMenuLinks() {
		return serviceVerificationMenuLinks;
	}

	/**
	 * @return the auditResultsMenuLinks
	 */
	public String[][] getAuditResultsMenuLinks() {
		return auditResultsMenuLinks;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

}
