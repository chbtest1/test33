package ca.medavie.nspp.audit.ui;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.aspose.cells.License;

/**
 * AsposeLicenseUtility provides methods for setting up the Aspose license
 * 
 */
public class AsposeLicenseUtility {

	/**
	 * Set's up the Aspose Cell's license
	 */
	public static void setUpAsposeLicense() {

		Log log = LogFactory.getLog(AsposeLicenseUtility.class);

		log.debug("SetUpAsposeLicense(): Entering method.");
		
		// read in the license file
		InputStream licenseStream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("ca/medavie/nspp/audit/ui/Aspose.Cells.lic");

		// set it
		(new License()).setLicense(licenseStream);

		log.debug("SetUpAsposeLicense( ): Aspose Cell's Subscription Expire Date: "
				+ License.getSubscriptionExpireDate());

		// close stream quietly!
		// kind of like IOUtils.closeQuietly( InputStream )
		try {
			licenseStream.close();
		} catch (IOException e) {
			// being quiet!
		}

		log.debug("SetUpAsposeLicense(): Exiting method.");
	}

}
