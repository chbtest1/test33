/** ******************************** */
/** **** General Use JavaScript **** */
/** ******************************** */
function dismissSystemMessageDialog() {
	$("#messageAlertForm").attr('style', 'display: none;');
}

// Fixes the footer scrolling bug. Removes flying-focus from DOM on each click
document.addEventListener("click", function() {
	$("#flying-focus").remove();
});

// Removes the select... option from the specified dropdown
function removeSelectOptionFromDropdown(dropdownId) {
	$("#" + dropdownId + " option[value='']").remove();
}

/*
 * Removes the 'RX' option from the dropdown specified by ID
 */
function removeRXSubcategoryOption(dropdownId) {
	$("#" + dropdownId + " option[value='RX']").remove();
}

/*
 * Used to remove the modal overlay pop ups render..
 * 
 * This allows a much quicker close action in conjunction with setting popup display to none..
 */
function hidePopupModal() {
	$('div').remove(".modal-backdrop");
	$("body").removeClass("modal-open");
}

/**
 * Clears out any text inputs on the specified form
 * 
 * @param formId
 */
function resetForm(formId) {
	$("#" + formId).find("input[type=text]").val("");
	$("#" + formId).find("select").val("");

}

/**
 * Clears out any text inputs on the specified form
 * 
 * @param formId
 */
function resetBulkCopyAddAuditResultForm(formId) {
	$("#" + formId).find("input[type=text]").val("");
	$("#" + formId).find("select").val("");
	$("#bulkCopyAuditResultPanel\\:newAuditResultToCopyForm\\:newAuditToCopyfinalContactDate\\:calendar_input").val("01-Feb-2999");
	$("#bulkCopyAuditResultPanel\\:newAuditResultToCopyForm\\:newAuditToCopyinterimDate\\:calendar_input").val("01-Feb-2999");
}

/*
 * Removes the Select... from the specified dropdown. NOTE: The ID string passed will need to ignore : by using //:
 */
function removeSelectOption(dropdownId) {
	$("#" + dropdownId + " option[value='']").remove();
}

/**
 * Limit a text area to a specified number of characters. When text lenght exceed maxlength, error message pop up.
 * 
 * @param textArea
 *            The text area to limit
 * @param event
 *            The event
 * @param maxChars
 *            The max chatacters to allow
 */
function limitTextArea(textArea, event, maxChars) {
	if (textArea) {
		if (textArea.value.length > maxChars) {
			event.keyCode = 0;
			var limited = textArea.value.substring(0, maxChars);
			textArea.value = limited;
		}
	}
}

/**
 * Clears out the rich select menus
 * 
 */
function clearRichSelectElement() {

	// clear up modifiers
	var modifier_count = $(".chosen-container-multi > ul > li").length;
	var i;

	for (i = 0; i < modifier_count; i++) {
		// if there is only one left
		if (i == modifier_count - 1) {
			$(".chosen-container-multi > ul > li.search-choice > a").click();
		}

		else {
			$(".chosen-container-multi > ul > li:nth-child(" + i + ") > a").click();
		}
	}
}

/** ******************************* */
/** **** Peer Group JavaScript **** */
/** ******************************* */
// Displays the PeerGroup edit form following the PeerGroup selection
function displayPeerGroupEdit() {
	// Hide search and display edit form
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:searchForm").attr('style', 'display: none;');
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:peerGroupEditWrapper").toggle('fade', 700);

	// Remove the select... option from the Town Inclusive Edit dropdown
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:editPeerGroupCriteriaFieldSet\\:editPeerGroupCriteriaForm\\:townInclusiveDrp\\:selectOneMenu option[value='']").remove();
}

// Hides the PeerGroup edit and displays search
function returnToPeerGroupSearch() {
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:searchForm").toggle('fade', 700);
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:peerGroupEditWrapper").attr('style', 'display: none;');

	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:subcategoryTypeForm\\:subcategoryTypeBody").hide();
	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:subcategoryTypeForm\\:expandSubcategoryType").addClass('fa-plus-circle');
	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:subcategoryTypeForm\\:expandSubcategoryType").removeClass('fa-minus-circle');
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:subcategoryTypeForm\\:subcategoryTypeBody").hide();
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:subcategoryTypeForm\\:expandSubcategoryType").addClass('fa-plus-circle');
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:subcategoryTypeForm\\:expandSubcategoryType").removeClass('fa-minus-circle');

	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:specialtyCriteriaForm\\:specialtyCriteriaBody").hide();
	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:specialtyCriteriaForm\\:expandSpecialtyCriteria").addClass('fa-plus-circle');
	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:specialtyCriteriaForm\\:expandSpecialtyCriteria").removeClass('fa-minus-circle');
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:specialtyCriteriaForm\\:specialtyCriteriaBody").hide();
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:specialtyCriteriaForm\\:expandSpecialtyCriteria").addClass('fa-plus-circle');
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:specialtyCriteriaForm\\:expandSpecialtyCriteria").removeClass('fa-minus-circle');

	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:townCriteriaForm\\:townCriteriaBody").hide();
	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:townCriteriaForm\\:expandTownCriteria").addClass('fa-plus-circle');
	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:townCriteriaForm\\:expandTownCriteria").removeClass('fa-minus-circle');
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:townCriteriaForm\\:townCriteriaBody").hide();
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:townCriteriaForm\\:expandTownCriteria").addClass('fa-plus-circle');
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:townCriteriaForm\\:expandTownCriteria").removeClass('fa-minus-circle');

	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:healthServiceCriteriaForm\\:healthServiceCriteriaBody").hide();
	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:healthServiceCriteriaForm\\:expandHealthServiceCriteria").addClass('fa-plus-circle');
	$("#editPeerGroupCriteriaPanel\\:peerGroupAddView\\:expandColFieldset\\:healthServiceCriteriaForm\\:expandHealthServiceCriteria").removeClass('fa-minus-circle');
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:healthServiceCriteriaForm\\:healthServiceCriteriaBody").hide();
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:healthServiceCriteriaForm\\:expandHealthServiceCriteria").addClass('fa-plus-circle');
	$("#peerGroupEditView\\:searchEditPeerGroupPanel\\:expandColFieldset\\:healthServiceCriteriaForm\\:expandHealthServiceCriteria").removeClass('fa-minus-circle');
}

// Expands/Minimizes the Subcategory type panel on the Add/Edit Peer Group
// Criteria page
function expandSubcategoryPanel(rootPanelId) {
	$("#" + rootPanelId + "\\:expandColFieldset\\:subcategoryTypeForm\\:subcategoryTypeBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#" + rootPanelId + "\\:expandColFieldset\\:subcategoryTypeForm\\:expandSubcategoryType").hasClass('fa-plus-circle')) {
		$("#" + rootPanelId + "\\:expandColFieldset\\:subcategoryTypeForm\\:expandSubcategoryType").addClass('fa-minus-circle');
		$("#" + rootPanelId + "\\:expandColFieldset\\:subcategoryTypeForm\\:expandSubcategoryType").removeClass('fa-plus-circle');
	} else {
		$("#" + rootPanelId + "\\:expandColFieldset\\:subcategoryTypeForm\\:expandSubcategoryType").addClass('fa-plus-circle');
		$("#" + rootPanelId + "\\:expandColFieldset\\:subcategoryTypeForm\\:expandSubcategoryType").removeClass('fa-minus-circle');
	}
}

// Expands/Minimizes the Specialty Criteria type panel on the Add/Edit Peer
// Group Criteria page
function expandSpecialtyCriteriaPanel(rootPanelId) {
	$("#" + rootPanelId + "\\:expandColFieldset\\:specialtyCriteriaForm\\:specialtyCriteriaBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#" + rootPanelId + "\\:expandColFieldset\\:specialtyCriteriaForm\\:expandSpecialtyCriteria").hasClass('fa-plus-circle')) {
		$("#" + rootPanelId + "\\:expandColFieldset\\:specialtyCriteriaForm\\:expandSpecialtyCriteria").addClass('fa-minus-circle');
		$("#" + rootPanelId + "\\:expandColFieldset\\:specialtyCriteriaForm\\:expandSpecialtyCriteria").removeClass('fa-plus-circle');
	} else {
		$("#" + rootPanelId + "\\:expandColFieldset\\:specialtyCriteriaForm\\:expandSpecialtyCriteria").addClass('fa-plus-circle');
		$("#" + rootPanelId + "\\:expandColFieldset\\:specialtyCriteriaForm\\:expandSpecialtyCriteria").removeClass('fa-minus-circle');
	}
}

// Expands/Minimizes the Town Criteria type panel on the Add/Edit Peer Group
// Criteria page
function expandTownCriteriaPanel(rootPanelId) {
	$("#" + rootPanelId + "\\:expandColFieldset\\:townCriteriaForm\\:townCriteriaBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#" + rootPanelId + "\\:expandColFieldset\\:townCriteriaForm\\:expandTownCriteria").hasClass('fa-plus-circle')) {
		$("#" + rootPanelId + "\\:expandColFieldset\\:townCriteriaForm\\:expandTownCriteria").addClass('fa-minus-circle');
		$("#" + rootPanelId + "\\:expandColFieldset\\:townCriteriaForm\\:expandTownCriteria").removeClass('fa-plus-circle');
	} else {
		$("#" + rootPanelId + "\\:expandColFieldset\\:townCriteriaForm\\:expandTownCriteria").addClass('fa-plus-circle');
		$("#" + rootPanelId + "\\:expandColFieldset\\:townCriteriaForm\\:expandTownCriteria").removeClass('fa-minus-circle');
	}
}

// Expands/Minimizes the Health Service Criteria type panel on the Add/Edit Peer
// Group Criteria page
function expandHSCPanel(rootPanelId) {
	$("#" + rootPanelId + "\\:expandColFieldset\\:healthServiceCriteriaForm\\:healthServiceCriteriaBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#" + rootPanelId + "\\:expandColFieldset\\:healthServiceCriteriaForm\\:expandHealthServiceCriteria").hasClass('fa-plus-circle')) {
		$("#" + rootPanelId + "\\:expandColFieldset\\:healthServiceCriteriaForm\\:expandHealthServiceCriteria").addClass('fa-minus-circle');
		$("#" + rootPanelId + "\\:expandColFieldset\\:healthServiceCriteriaForm\\:expandHealthServiceCriteria").removeClass('fa-plus-circle');
	} else {
		$("#" + rootPanelId + "\\:expandColFieldset\\:healthServiceCriteriaForm\\:expandHealthServiceCriteria").addClass('fa-plus-circle');
		$("#" + rootPanelId + "\\:expandColFieldset\\:healthServiceCriteriaForm\\:expandHealthServiceCriteria").removeClass('fa-minus-circle');
	}
}

// Expands the Manage Practitioners in PeerGroups search panel
function expandManagePractPeerGroupSearchPanel() {
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractSearchForm\\:managePractSearchBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#managePractitionerInGroupView\\:providerPanel\\:managePractSearchForm\\:expandManagePractSearch").hasClass('fa-plus-circle')) {
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractSearchForm\\:expandManagePractSearch").addClass('fa-minus-circle');
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractSearchForm\\:expandManagePractSearch").removeClass('fa-plus-circle');
	} else {
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractSearchForm\\:expandManagePractSearch").addClass('fa-plus-circle');
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractSearchForm\\:expandManagePractSearch").removeClass('fa-minus-circle');
	}
}

// Expands the Practitioner search in Manage Practitioners in PeerGroups panel
function expandManagePractSearchPanel() {
	$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:addSearchPractBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:expandAddSearchPractitioner").hasClass('fa-plus-circle')) {
		$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:expandAddSearchPractitioner").addClass('fa-minus-circle');
		$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:expandAddSearchPractitioner").removeClass('fa-plus-circle');
	} else {
		$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:expandAddSearchPractitioner").addClass('fa-plus-circle');
		$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:expandAddSearchPractitioner").removeClass('fa-minus-circle');
	}
}

// Displays the Manage Practitioners panel
function displayManagePractitionerPanel() {
	// Display the Practitioners management panel
	$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:addSearchPractStep1Wrapper").attr('style', 'display: block;');
	$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:addSearchPractStep2Wrapper").attr('style', 'display: none;');
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:managePractBody").attr('style', 'display: block;');
	$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:addSearchPractBody").attr('style', 'display: none;');
	$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:expandAddSearchPractitioner").addClass('fa-plus-circle');
	$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:expandAddSearchPractitioner").removeClass('fa-minus-circle');
	// Close the search popup
	hidePopupModal();

	// Ensure the Practitioner panels are in default state
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandShadowPractitioner").addClass('fa-plus-circle');
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandShadowPractitioner").removeClass('fa-minus-circle');
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:shadowPractBody").attr('style', 'display: none;');

	$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandFFSPractitioner").addClass('fa-plus-circle');
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandFFSPractitioner").removeClass('fa-minus-circle');
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:ffsPractBody").attr('style', 'display: none;');

	// Hide the search form
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractSearchForm").attr('style', 'display: none;');
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerWrapper").toggle('fade', 700);
}

// Toggles the display of the Shadow Practitioner panel
function toggleShadowPractitionerPanel() {
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:shadowPractBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandShadowPractitioner").hasClass('fa-plus-circle')) {
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandShadowPractitioner").addClass('fa-minus-circle');
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandShadowPractitioner").removeClass('fa-plus-circle');
	} else {
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandShadowPractitioner").addClass('fa-plus-circle');
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandShadowPractitioner").removeClass('fa-minus-circle');
	}
}

// Toggles the display of the Fee For Service Practitioner panel
function toggleFFSPractitionerPanel() {
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:ffsPractBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandFFSPractitioner").hasClass('fa-plus-circle')) {
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandFFSPractitioner").addClass('fa-minus-circle');
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandFFSPractitioner").removeClass('fa-plus-circle');
	} else {
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandFFSPractitioner").addClass('fa-plus-circle');
		$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerForm\\:expandFFSPractitioner").removeClass('fa-minus-circle');
	}
}

// Toggles the display of search panel and in turn hide the practitioner panel from view
function returnToManagePractitionerSearchPanel() {
	// Hide the practitioner panel
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractitionerWrapper").attr('style', 'display: none;');
	// Display the Manage Practitioner search panel
	$("#managePractitionerInGroupView\\:providerPanel\\:managePractSearchForm").toggle('fade', 700);
}

// Takes user to Step 1 of searching for Practitioners on Manage Practitioners in Groups page.
function displaySearchPractitionerStep1() {
	$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:addSearchPractStep2Wrapper").toggle('fade', 700, function() {
		$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:addSearchPractStep1Wrapper").toggle('fade', 700);
	});
}

// Takes user to Step 2 of searching for Practitioners on Manage Practitioners in Groups page.
function displaySearchPractitionerStep2() {
	$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:addSearchPractStep1Wrapper").toggle('fade', 700, function() {
		$("#managePractitionerInGroupView\\:providerPanel\\:searchAddPractitionersForm\\:addSearchPractStep2Wrapper").toggle('fade', 700);
	});
}

/** ************************** */
/** Manual Deposits Javascript */
/** ************************** */
// Display search screen after making search style selection
function displaySelectedSearchPanel() {

	// Edit form toggle
	$("#editManualDepositsView\\:manualDepositsSearchForm\\:manualDepositsInquiryPanel\\:searchStyleSelectionWrapper").attr('style', 'display: none;');
	$("#editManualDepositsView\\:manualDepositsSearchForm\\:manualDepositsInquiryPanel\\:manualDepositsSearchPanel").toggle('fade', 700);

	// Add form toggle
	$("#createManualDepositsView\\:addSearchPayors\\:payorsSearchPanel\\:searchStyleSelectionWrapper").attr('style', 'display: none;');
	$("#createManualDepositsView\\:addSearchPayors\\:payorsSearchPanel\\:manualDepositsSearchPanel").toggle('fade', 700);
}

// Takes the user back to the search type selection for Editing existing Manual Deposits
function displayEditSearchSelectionPanel() {
	$("#editManualDepositsView\\:manualDepositsSearchForm\\:manualDepositsInquiryPanel\\:manualDepositsSearchPanel").attr('style', 'display: none;');
	$("#editManualDepositsView\\:manualDepositsSearchForm\\:manualDepositsInquiryPanel\\:searchStyleSelectionWrapper").toggle('fade', 700);
}

// Takes the user back to the search type selection for Creating new Manual Deposits
function displayNewSearchSelectionPanel() {
	$("#createManualDepositsView\\:addSearchPayors\\:payorsSearchPanel\\:manualDepositsSearchPanel").attr('style', 'display: none;');
	$("#createManualDepositsView\\:addSearchPayors\\:payorsSearchPanel\\:searchStyleSelectionWrapper").toggle('fade', 700);
}

// Displays the Manual Deposit edit form following the Manual Deposit selection
function displayManualDepositEdit() {
	// Hide search and display edit form
	$("#editManualDepositsView\\:manualDepositsSearchForm").attr('style', 'display: none;');
	$("#editManualDepositsView\\:editManualDepositForm").toggle('fade', 700);
}

// Hides the Manual Deposit edit and displays search
function returnToManualDepositSearch() {
	// Edit screen toggle
	$("#editManualDepositsView\\:manualDepositsSearchForm").toggle('fade', 700);
	$("#editManualDepositsView\\:editManualDepositForm").attr('style', 'display: none;');

	// Create new screen toggle
	$("#createManualDepositsView\\:addSearchPayors").toggle('fade', 700);
	$("#createManualDepositsView\\:addManualDepositForm").attr('style', 'display: none;');
}

// Displays the Add form for the selected Practitioner/Group for Manual Deposit
function displayAddNewManualDepositPanel() {
	// Hide search and display Add screen
	$("#createManualDepositsView\\:addManualDepositForm").toggle('fade', 700);
	$("#createManualDepositsView\\:addSearchPayors").attr('style', 'display: none;');
}

/** **************************** */
/** **** Inquiry JavaScript **** */
/** **************************** */
// Enables expand & minimize of the Practitioner Information panel for Practitioner Inquiry
function expandPractitionerInformationPanel() {
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:practitionerInfoWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerInfo").hasClass('fa-plus-circle')) {
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerInfo").addClass('fa-minus-circle');
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerInfo").removeClass('fa-plus-circle');
	} else {
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerInfo").addClass('fa-plus-circle');
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerInfo").removeClass('fa-minus-circle');
	}
}

// Enables expand & minimize of the Practitioner Payment Information panel for Practitioner Inquiry
function expandPractitionerPaymentInfoPanel() {
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:practitionerPaymentInfoWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerPaymentInfo").hasClass('fa-plus-circle')) {
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerPaymentInfo").addClass('fa-minus-circle');
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerPaymentInfo").removeClass('fa-plus-circle');
	} else {
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerPaymentInfo").addClass('fa-plus-circle');
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerPaymentInfo").removeClass('fa-minus-circle');
	}
}

// Enables expand & minimize of the Practitioner Age Distribution panel for Practitioner Inquiry
function expandPractitionerAgeDistributionPanel() {
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:practitionerAgeDistWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerAgeDist").hasClass('fa-plus-circle')) {
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerAgeDist").addClass('fa-minus-circle');
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerAgeDist").removeClass('fa-plus-circle');
	} else {
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerAgeDist").addClass('fa-plus-circle');
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerAgeDist").removeClass('fa-minus-circle');
	}
}

// Enables expand & minimize of the Practitioner Health Services panel for Practitioner Inquiry
function expandPractitionerHealthServicesPanel() {
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:practitionerHSWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerHS").hasClass('fa-plus-circle')) {
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerHS").addClass('fa-minus-circle');
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerHS").removeClass('fa-plus-circle');
	} else {
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerHS").addClass('fa-plus-circle');
		$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerHS").removeClass('fa-minus-circle');
	}
}

// Renders the details form for PeerGroup inquiry
function displayPeerGroupInquiryDetails() {
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupSearchForm").attr('style', 'display: none;');
	// Close the search results popup window and display details
	hidePopupModal();

	// Ensure all panels are in default state
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupInfo").addClass('fa-minus-circle');
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupInfo").removeClass('fa-plus-circle');
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:practitionerInfoWrapper").attr('style', 'display: block;');

	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupPayment").addClass('fa-plus-circle');
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupPayment").removeClass('fa-minus-circle');
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:peerGroupPaymentWrapper").attr('style', 'display: none;');

	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupHealthServices").addClass('fa-plus-circle');
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupHealthServices").removeClass('fa-minus-circle');
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:peerGroupHealthServicesWrapper").attr('style', 'display: none;');

	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper").toggle('fade', 700);
}

// Renders the details form for Practitioner inquiry
function displayPractitionerInquiryDetails() {
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerSearchForm").attr('style', 'display: none;');
	// Close the search results popup window and display details
	hidePopupModal();

	// Ensure all panels are in default state
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerInfo").addClass('fa-minus-circle');
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerInfo").removeClass('fa-plus-circle');
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:practitionerInfoWrapper").attr('style', 'display: block;');

	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerPaymentInfo").addClass('fa-plus-circle');
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerPaymentInfo").removeClass('fa-minus-circle');
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:practitionerPaymentInfoWrapper").attr('style', 'display: none;');

	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerAgeDist").addClass('fa-plus-circle');
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerAgeDist").removeClass('fa-minus-circle');
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:practitionerAgeDistWrapper").attr('style', 'display: none;');

	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerHS").addClass('fa-plus-circle');
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:expandPractitionerHS").removeClass('fa-minus-circle');
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper\\:practitionerHSWrapper").attr('style', 'display: none;');

	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper").toggle('fade', 700);
}

function returnToPeerGroupInquirySearch() {
	// Hide edit form
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper").attr('style', 'display: none;');
	// Fade in search
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupSearchForm").toggle('fade', 700);
}

function returnToPractitionerInquirySearch() {
	// Hide edit form
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerInquiryDetailWrapper").attr('style', 'display: none;');
	// Fade in search
	$("#practitionerInquiryView\\:practitionerInquiryPanel\\:practitionerSearchForm").toggle('fade', 700);
}

// Toggles the display of PeerGroup Information panel
function togglePeerGroupInquiryInformationPanel() {
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:practitionerInfoWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupInfo").hasClass('fa-plus-circle')) {
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupInfo").addClass('fa-minus-circle');
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupInfo").removeClass('fa-plus-circle');
	} else {
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupInfo").addClass('fa-plus-circle');
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupInfo").removeClass('fa-minus-circle');
	}
}

// Toggles the display of PeerGroup Payment Information panel
function togglePeerGroupInquiryPaymentInformationPanel() {
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:peerGroupPaymentWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupPayment").hasClass('fa-plus-circle')) {
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupPayment").addClass('fa-minus-circle');
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupPayment").removeClass('fa-plus-circle');
	} else {
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupPayment").addClass('fa-plus-circle');
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupPayment").removeClass('fa-minus-circle');
	}
}

// Toggles the display of PeerGroup HealthServices panel
function togglePeerGroupInquiryHealthServicesPanel() {
	$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:peerGroupHealthServicesWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupHealthServices").hasClass('fa-plus-circle')) {
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupHealthServices").addClass('fa-minus-circle');
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupHealthServices").removeClass('fa-plus-circle');
	} else {
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupHealthServices").addClass('fa-plus-circle');
		$("#peerGroupInquiryView\\:peerInquiryPanel\\:peerGroupInquiryDetailWrapper\\:expandPeerGroupHealthServices").removeClass('fa-minus-circle');
	}
}

/** ****************************************** */
/** **** Health Service Groups JavaScript **** */
/** ****************************************** */
// Takes the user to step 1 of searching for Health Service Groups
function displayHsSearchSelection() {
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm\\:healthServiceGroupSearchWrapper").attr('style', 'display: none;');
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm\\:healthServicesSearchWrapper").attr('style', 'display: none;');
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm\\:healthServicesBtnPanel\\:ctr").attr('style', 'display: none;');
	// Fade in search type selection
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm\\:searchStyleSelectionWrapper").toggle('fade', 700);
}

// Takes the user to the search form to search on Health Service Groups
function displayHealthServiceGroupsSearch() {
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm\\:searchStyleSelectionWrapper").attr('style', 'display: none;');
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm\\:healthServiceGroupSearchWrapper").toggle('fade', 700);
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm\\:healthServicesBtnPanel\\:ctr").toggle('fade', 700);
}

// Takes the user to the search form to search on Health Services
function displayHealthServicesSearch() {
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm\\:searchStyleSelectionWrapper").attr('style', 'display: none;');
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm\\:healthServicesSearchWrapper").toggle('fade', 700);
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm\\:healthServicesBtnPanel\\:ctr").toggle('fade', 700);
}

// Display the HealthServiceGroup details form and minimizes the search panel
function displayHealthServiceDetails() {
	hidePopupModal();

	// Ensure add HSC is minimized
	$("#editHSGroupsView\\:searchHSGroupPanel\\:addNewHSForm\\:addHSBody").attr('style', 'display: none;');
	$("#editHSGroupsView\\:searchHSGroupPanel\\:addNewHSForm\\:expandAddHS").removeClass('fa-minus-circle');
	$("#editHSGroupsView\\:searchHSGroupPanel\\:addNewHSForm\\:expandAddHS").addClass('fa-plus-circle');

	// Hide the search
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm").attr('style', 'display: none;');

	// Display the Health Service Group details
	$("#editHSGroupsView\\:searchHSGroupPanel\\:hsGroupDetailsWrapper").toggle('fade', 700);
}

function returnToHealthServiceSearch() {
	// Hide edit form
	$("#editHSGroupsView\\:searchHSGroupPanel\\:hsGroupDetailsWrapper").attr('style', 'display: none;');

	// Fade in search
	$("#editHSGroupsView\\:searchHSGroupPanel\\:healthServiceSearchForm").toggle('fade', 700);
}

// Toggles the display of the Add HealthService panel on HSG page
function toggleAddHealthServicePanel() {
	$("#editHSGroupsView\\:searchHSGroupPanel\\:addNewHSForm\\:addHSBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#editHSGroupsView\\:searchHSGroupPanel\\:addNewHSForm\\:expandAddHS").hasClass('fa-plus-circle')) {
		$("#editHSGroupsView\\:searchHSGroupPanel\\:addNewHSForm\\:expandAddHS").addClass('fa-minus-circle');
		$("#editHSGroupsView\\:searchHSGroupPanel\\:addNewHSForm\\:expandAddHS").removeClass('fa-plus-circle');
	} else {
		$("#editHSGroupsView\\:searchHSGroupPanel\\:addNewHSForm\\:expandAddHS").addClass('fa-plus-circle');
		$("#editHSGroupsView\\:searchHSGroupPanel\\:addNewHSForm\\:expandAddHS").removeClass('fa-minus-circle');
	}
}

/** ******************************* */
/** ** Audit Result JavaScript **** */
/** ******************************* */
// Displays the Audit result Record screen after a record has been selected from the
// search results, Called from backend bean
function displayAddNewAuditResultPanel() {

	// Hide other panels
	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerResultSearchForm").attr('style', 'display: none;');
	$("#editAuditResultsView\\:searchAuditResultPanel\\:auditResultEditWrapper").attr('style', 'display: none;');

	// Fade in edit detail panels
	$("#editAuditResultsView\\:searchAuditResultPanel\\:addNewAuditResultWrapper").toggle('fade', 700);

	// Ensure panels are in correct state
	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAddNewAuditResultForm\\:expandAddNewAuditResultInfo").addClass('fa-plus-circle');
	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAddNewAuditResultForm\\:expandAddNewAuditResultInfo").removeClass('fa-minus-circle');
	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAddNewAuditResultForm\\:auditResultAddNewPanelBody").attr('style', 'display: none;');

	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAuditResultForm\\:expandPractitionerAuditResult").addClass('fa-minus-circle');
	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAuditResultForm\\:expandPractitionerAuditResult").removeClass('fa-plus-circle');
	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAddNewAuditResultForm\\:auditSearchResultPanelBody").attr('style', 'display: block;');
}

// Displays the Audit Result Record Edit screen
function displayEditAuditResultsPanel() {

	// Hide other panels
	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerResultSearchForm").attr('style', 'display: none;');
	$("#editAuditResultsView\\:searchAuditResultPanel\\:addNewAuditResultWrapper").attr('style', 'display: none;');

	// Fade in edit panels
	$("#editAuditResultsView\\:searchAuditResultPanel\\:auditResultEditWrapper").toggle('fade', 700);
}

// Enables expand & minimize search practitioner for Audit Result
function expandPractitionerSearchPanel() {
	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerResultSearchForm\\:auditSearchWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerResultSearchForm\\:expandAuditSearch").hasClass('fa-plus-circle')) {
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerResultSearchForm\\:expandAuditSearch").addClass('fa-minus-circle');
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerResultSearchForm\\:expandAuditSearch").removeClass('fa-plus-circle');
	} else {
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerResultSearchForm\\:expandAuditSearch").addClass('fa-plus-circle');
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerResultSearchForm\\:expandAuditSearch").removeClass('fa-minus-circle');
	}
}

// Enables expand & minimize search practitioner result in Audit result section
function expandPractitionerSearchResearchPanel() {

	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAuditResultForm\\:auditSearchResultPanelBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAuditResultForm\\:expandPractitionerAuditResult").hasClass('fa-plus-circle')) {
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAuditResultForm\\:expandPractitionerAuditResult").addClass('fa-minus-circle');
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAuditResultForm\\:expandPractitionerAuditResult").removeClass('fa-plus-circle');
	} else {
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAuditResultForm\\:expandPractitionerAuditResult").addClass('fa-plus-circle');
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAuditResultForm\\:expandPractitionerAuditResult").removeClass('fa-minus-circle');
	}
}

// Enables expand & minimize add new practitioner result in Audit result section
function expandPractitionerAddNewAuditResultPanel() {

	// fade in add new practitioner audit result panel
	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAddNewAuditResultForm\\:auditResultAddNewPanelBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAddNewAuditResultForm\\:expandAddNewAuditResultInfo").hasClass('fa-plus-circle')) {
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAddNewAuditResultForm\\:expandAddNewAuditResultInfo").addClass('fa-minus-circle');
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAddNewAuditResultForm\\:expandAddNewAuditResultInfo").removeClass('fa-plus-circle');
	} else {
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAddNewAuditResultForm\\:expandAddNewAuditResultInfo").addClass('fa-plus-circle');
		$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerAddNewAuditResultForm\\:expandAddNewAuditResultInfo").removeClass('fa-minus-circle');
	}
}

// Returns the user to the audit result
function returnToAuditResultSearch() {
	// Hide other panels
	$("#editAuditResultsView\\:searchAuditResultPanel\\:addNewAuditResultWrapper").attr('style', 'display: none;');
	$("#editAuditResultsView\\:searchAuditResultPanel\\:auditResultEditWrapper").attr('style', 'display: none;');

	// Fade in search
	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerResultSearchForm").toggle('fade', 700);
}

// Returns the user to the add new audit result page
function returnToAddNewAuditResult() {
	// Hide other panels
	$("#editAuditResultsView\\:searchAuditResultPanel\\:practitionerResultSearchForm").attr('style', 'display: none;');
	$("#editAuditResultsView\\:searchAuditResultPanel\\:auditResultEditWrapper").attr('style', 'display: none;');

	// Fade in add new audit result panels
	$("#editAuditResultsView\\:searchAuditResultPanel\\:addNewAuditResultWrapper").toggle('fade', 700);

}

function gotoBulkCopyAddNewAuditResultPage() {

	// Hide add new practitioner panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:practitionerAddNewpractitionerForm\\:addNewPractitionerInfoPanel").addClass('hide-elements');
	// hide practitioners to copy panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:practitionerCopyListForm\\:practitionerAuditResultPanel").addClass('hide-elements');

	// show add new audit result panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:newAuditResultToCopyForm\\:addNewAuditResultInfoPanel").removeClass('hide-elements');
	// show add new audit result to copypanel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:auditResultsToCopyForm\\:auditResultsToAddPanel").removeClass('hide-elements');

}

function gotoBulkCopySummaryPage() {
	// Hide add new practitioner panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:practitionerAddNewpractitionerForm\\:addNewPractitionerInfoPanel").addClass('hide-elements');
	// show new practitioners to copy panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:practitionerCopyListForm\\:practitionerAuditResultPanel").removeClass('hide-elements');

	// hide add new audit result panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:newAuditResultToCopyForm\\:addNewAuditResultInfoPanel").addClass('hide-elements');
	// show new audit results to copy panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:auditResultsToCopyForm\\:auditResultsToAddPanel").removeClass('hide-elements');
}

function gotoBulkCopyPractitionerPage() {

	// hide select subcategory panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:practitionerAddNewpractitionerForm\\:auditResultBulkCopySelectSubcategoryWrapperPanel").addClass('hide-elements');

	// show add new practitioner panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:practitionerAddNewpractitionerForm\\:addNewPractitionerInfoPanel").removeClass('hide-elements');
	// show new practitioners to copy panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:practitionerCopyListForm\\:practitionerAuditResultPanel").removeClass('hide-elements');

	// hide add new audit result panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:newAuditResultToCopyForm\\:addNewAuditResultInfoPanel").addClass('hide-elements');
	// hide new audit results to copy panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:auditResultsToCopyForm\\:auditResultsToAddPanel").addClass('hide-elements');
}

function gotoSelectSubgatetoryPage() {

	// show select subcategory panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:practitionerAddNewpractitionerForm\\:auditResultBulkCopySelectSubcategoryWrapperPanel").removeClass('hide-elements');

	// hide add new practitioner panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:practitionerAddNewpractitionerForm\\:addNewPractitionerInfoPanel").addClass('hide-elements');
	// show new practitioners to copy panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:practitionerCopyListForm\\:practitionerAuditResultPanel").addClass('hide-elements');

	// hide add new audit result panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:newAuditResultToCopyForm\\:addNewAuditResultInfoPanel").addClass('hide-elements');
	// hide new audit results to copy panel
	$("#bulkAuditResultsView\\:bulkCopyAuditResultPanel\\:auditResultsToCopyForm\\:auditResultsToAddPanel").addClass('hide-elements');
}

function hideMessages() {
	$("#messageAlertForm").attr('style', 'display: none');
}

/** ********************************** */
/** ***** Locum Hosts JavaScript ***** */
/** ********************************** */
// Returns the user to the Locum search form
function returnToLocumSearch() {
	// Hide edit form
	$("#editLocumsView\\:searchEditLocumPanel\\:locumEditWrapper").attr('style', 'display: none;');
	// Fade in search
	$("#editLocumsView\\:searchEditLocumPanel\\:locumHostsSearchForm").toggle('fade', 700);
}

// Toggles the display of the Search and Details panel for Locums
function displayLocumDetails() {
	// Remove pop-up CSS
	hidePopupModal();
	// Toggle search panel
	$("#editLocumsView\\:searchEditLocumPanel\\:locumHostsSearchForm").attr('style', 'display: none;');
	// Toggle details panel
	$("#editLocumsView\\:searchEditLocumPanel\\:locumEditWrapper").toggle('fade', 700);

}

// Toggles the display of the Practitioner search panel on the Locum page
function toggleLocumPractitionerSearchPanel() {
	$("#editLocumsView\\:searchEditLocumPanel\\:locumEditForm\\:locumHostsSearchWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#editLocumsView\\:searchEditLocumPanel\\:locumEditForm\\:expandAddPractitionerSearch").hasClass('fa-plus-circle')) {
		$("#editLocumsView\\:searchEditLocumPanel\\:locumEditForm\\:expandAddPractitionerSearch").addClass('fa-minus-circle');
		$("#editLocumsView\\:searchEditLocumPanel\\:locumEditForm\\:expandAddPractitionerSearch").removeClass('fa-plus-circle');
	} else {
		$("#editLocumsView\\:searchEditLocumPanel\\:locumEditForm\\:expandAddPractitionerSearch").addClass('fa-plus-circle');
		$("#editLocumsView\\:searchEditLocumPanel\\:locumEditForm\\:expandAddPractitionerSearch").removeClass('fa-minus-circle');
	}
}

/** ******************************* */
/** ***** Medicare Javascript ***** */
/** ******************************* */
// Takes the user to step 1 of searching for Audit Criterias
function displayAuditCriteriaTypeSelection() {
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaSearchForm\\:auditCriteriaSearchStep1Wrapper").toggle('fade', 700);
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaSearchForm\\:auditCriteriaSearchStep2Wrapper").attr('style', 'display: none;');
}

// Takes the user to step 2 of searching for Audit Criterias
function displayAuditCriteriaSearch() {
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaSearchForm\\:auditCriteriaSearchStep1Wrapper").attr('style', 'display: none;');
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaSearchForm\\:auditCriteriaSearchStep2Wrapper").toggle('fade', 700);
}

// Returns the user back to the Medicare Audit Criteria search
function returnToEditMedAuditCriteriaSearch() {
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:searchEditAuditCriteriaPanel").attr('style', 'display:none;');
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaSearchForm\\:auditCriteriaSearchStep1Wrapper").attr('style', 'display: none;');
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaSearchForm\\:auditCriteriaSearchStep2Wrapper").attr('style', 'display: block;');
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaSearchForm").toggle('fade', 700);
}

// Displays the edit screen for Medicare Audit Criteria
function displayEditMedAuditCriteria() {
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaSearchForm").attr('style', 'display:none;');
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditTypeSelection").attr('style', 'display:none;');
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm").attr('style', 'display: block;');

	// Ensure expandable panels are minimized and in the default state
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:healthServiceCriteriaWrapper").attr('style', 'display:none;');
	if ($("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:expandHealthServiceCriteria").hasClass('fa-minus-circle')) {
		$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:expandHealthServiceCriteria").addClass('fa-plus-circle');
		$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:expandHealthServiceCriteria").removeClass('fa-minus-circle');
	}

	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:paymentResponsibilityWrapper").attr('style', 'display:none;');
	if ($("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:expandPaymentResponsibility").hasClass('fa-minus-circle')) {
		$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:expandPaymentResponsibility").addClass('fa-plus-circle');
		$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:expandPaymentResponsibility").removeClass('fa-minus-circle');
	}

	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:payToWrapper").attr('style', 'display:none;');
	if ($("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:expandPayTo").hasClass('fa-minus-circle')) {
		$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:expandPayTo").addClass('fa-plus-circle');
		$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:auditCriteriaEditForm\\:expandPayTo").removeClass('fa-minus-circle');
	}

	// Toggle the display
	$("#searchEditAuditCriteriaView\\:searchAuditCriteriaPanel\\:searchEditAuditCriteriaPanel").toggle('fade', 700);
}

// Takes the user to the Add Med Audit Criteria form after selecting a type
function displayAddNewMedAuditCriteria() {
	$("#editAuditCriteriaPanel\\:addNewAuditCriteriaView\\:auditTypeSelection").attr('style', 'display: none;');
	$("#editAuditCriteriaPanel\\:addNewAuditCriteriaView\\:auditCriteriaEditForm").toggle('fade', 700);
}

// Returns the user to the Audit Type selection for add new Med Audit Criteria
function returnToAddNewMedAuditCriteriaTypeSelection() {
	$("#editAuditCriteriaPanel\\:addNewAuditCriteriaView\\:auditCriteriaEditForm").attr('style', 'display: none;');
	$("#editAuditCriteriaPanel\\:addNewAuditCriteriaView\\:auditTypeSelection").toggle('fade', 700);

	$("#editAuditCriteriaPanel\\:addNewAuditCriteriaView\\:auditTypeSelection\\:practitionerTypeDrp\\:selectOneMenu option[value='PH']").attr("selected", "selected");
	// Remove the RX option from the Subcategory dropdown
	removeRXSubcategoryOption("editAuditCriteriaPanel\\:addNewAuditCriteriaView\\:auditTypeSelection\\:practitionerTypeDrp\\:selectOneMenu");
}

// Display Health Service Descriptions table
function displayHealthServiceDescriptionResults() {
	$("#editHsDescView\\:editHsDescriptionsPanel\\:hsDescriptionSearchForm").attr('style', 'display: none;');
	$("#editHsDescView\\:editHsDescriptionsPanel\\:editHsDescriptionsTableForm").toggle('fade', 700);
}

// Returns user to the Health Service Description search form
function returnToHealthServiceDescriptionSearch() {
	$("#editHsDescView\\:editHsDescriptionsPanel\\:editHsDescriptionsTableForm").attr('style', 'display: none;');
	$("#editHsDescView\\:editHsDescriptionsPanel\\:hsDescriptionSearchForm").toggle('fade', 700);
}

// Toggles the display of HSC on Audit Criteria
function toggleMedAuditHSCPanel(rootPanelId) {
	$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:healthServiceCriteriaWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandHealthServiceCriteria").hasClass('fa-plus-circle')) {
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandHealthServiceCriteria").addClass('fa-minus-circle');
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandHealthServiceCriteria").removeClass('fa-plus-circle');
	} else {
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandHealthServiceCriteria").addClass('fa-plus-circle');
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandHealthServiceCriteria").removeClass('fa-minus-circle');
	}
}

// Toggles the display of Payment Responsibility on Audit Criteria
function toggleMedAudPaymentRespPanel(rootPanelId) {
	$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:paymentResponsibilityWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandPaymentResponsibility").hasClass('fa-plus-circle')) {
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandPaymentResponsibility").addClass('fa-minus-circle');
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandPaymentResponsibility").removeClass('fa-plus-circle');
	} else {
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandPaymentResponsibility").addClass('fa-plus-circle');
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandPaymentResponsibility").removeClass('fa-minus-circle');
	}
}

// Toggles the display of Payee Type on Audit Criteria
function toggleMedAudPayeeTypePanel(rootPanelId) {
	$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:payToWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandPayTo").hasClass('fa-plus-circle')) {
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandPayTo").addClass('fa-minus-circle');
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandPayTo").removeClass('fa-plus-circle');
	} else {
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandPayTo").addClass('fa-plus-circle');
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandPayTo").removeClass('fa-minus-circle');
	}
}

// Toggles the display of the Add New Health Service Exclusion panel.
function toggleAddNewHealthServiceExclusionPanel() {
	$("#editGlobalHsExclusionsView\\:editHsExclusionsPanel\\:addHealthServiceExclusionsForm\\:addNewHSExclusionBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#editGlobalHsExclusionsView\\:editHsExclusionsPanel\\:addHealthServiceExclusionsForm\\:expandAddNewHSExclusion").hasClass('fa-plus-circle')) {
		$("#editGlobalHsExclusionsView\\:editHsExclusionsPanel\\:addHealthServiceExclusionsForm\\:expandAddNewHSExclusion").addClass('fa-minus-circle');
		$("#editGlobalHsExclusionsView\\:editHsExclusionsPanel\\:addHealthServiceExclusionsForm\\:expandAddNewHSExclusion").removeClass('fa-plus-circle');
	} else {
		$("#editGlobalHsExclusionsView\\:editHsExclusionsPanel\\:addHealthServiceExclusionsForm\\:expandAddNewHSExclusion").addClass('fa-plus-circle');
		$("#editGlobalHsExclusionsView\\:editHsExclusionsPanel\\:addHealthServiceExclusionsForm\\:expandAddNewHSExclusion").removeClass('fa-minus-circle');
	}
}

// Toggles the display of the Add New Individual Exclusion panel.
function toggleAddNewIndividualExclusionPanel() {
	$("#editGlobalIndExclusionsView\\:editIndividualExclusionsPanel\\:addIndividualExclusionsForm\\:addNewIndividualExclusionBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#editGlobalIndExclusionsView\\:editIndividualExclusionsPanel\\:addIndividualExclusionsForm\\:expandAddNewIndividualExclusion").hasClass('fa-plus-circle')) {
		$("#editGlobalIndExclusionsView\\:editIndividualExclusionsPanel\\:addIndividualExclusionsForm\\:expandAddNewIndividualExclusion").addClass('fa-minus-circle');
		$("#editGlobalIndExclusionsView\\:editIndividualExclusionsPanel\\:addIndividualExclusionsForm\\:expandAddNewIndividualExclusion").removeClass('fa-plus-circle');
	} else {
		$("#editGlobalIndExclusionsView\\:editIndividualExclusionsPanel\\:addIndividualExclusionsForm\\:expandAddNewIndividualExclusion").addClass('fa-plus-circle');
		$("#editGlobalIndExclusionsView\\:editIndividualExclusionsPanel\\:addIndividualExclusionsForm\\:expandAddNewIndividualExclusion").removeClass('fa-minus-circle');
	}
}

// Toggles the display of the DIN panel
function toggleDinPanel(rootPanelId) {
	$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:dinWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandDin").hasClass('fa-plus-circle')) {
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandDin").addClass('fa-minus-circle');
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandDin").removeClass('fa-plus-circle');
	} else {
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandDin").addClass('fa-plus-circle');
		$("#" + rootPanelId + "\\:auditCriteriaEditForm\\:expandDin").removeClass('fa-minus-circle');
	}
}

// Toggles the display of the add BA Exclusion panel
function toggleAddBaExclusionPanel() {
	$("#editBAExclusionsView\\:editBAExclusionsPanel\\:addBAExclusionForm\\:addNewBAExclusionBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#editBAExclusionsView\\:editBAExclusionsPanel\\:addBAExclusionForm\\:expandAddNewBAExclusion").hasClass('fa-plus-circle')) {
		$("#editBAExclusionsView\\:editBAExclusionsPanel\\:addBAExclusionForm\\:expandAddNewBAExclusion").addClass('fa-minus-circle');
		$("#editBAExclusionsView\\:editBAExclusionsPanel\\:addBAExclusionForm\\:expandAddNewBAExclusion").removeClass('fa-plus-circle');
	} else {
		$("#editBAExclusionsView\\:editBAExclusionsPanel\\:addBAExclusionForm\\:expandAddNewBAExclusion").addClass('fa-plus-circle');
		$("#editBAExclusionsView\\:editBAExclusionsPanel\\:addBAExclusionForm\\:expandAddNewBAExclusion").removeClass('fa-minus-circle');
	}
}

/** ************************** */
/** ***** Dss JavaScript ***** */
/** ************************** */
// Toggles the display of the Add new dss unit value panel page
function toggleAddNewDssUnitValuePanel() {
	$("#editDssUnitValueView\\:addEditDssUnitValuePanel\\:addNewDssUnitValueForm\\:expandAddNewDssUnitValueBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#editDssUnitValueView\\:addEditDssUnitValuePanel\\:addNewDssUnitValueForm\\:expandAddNewDssUnitValue").hasClass('fa-plus-circle')) {
		$("#editDssUnitValueView\\:addEditDssUnitValuePanel\\:addNewDssUnitValueForm\\:expandAddNewDssUnitValue").addClass('fa-minus-circle');
		$("#editDssUnitValueView\\:addEditDssUnitValuePanel\\:addNewDssUnitValueForm\\:expandAddNewDssUnitValue").removeClass('fa-plus-circle');
	} else {
		$("#editDssUnitValueView\\:addEditDssUnitValuePanel\\:addNewDssUnitValueForm\\:expandAddNewDssUnitValue").addClass('fa-plus-circle');
		$("#editDssUnitValueView\\:addEditDssUnitValuePanel\\:addNewDssUnitValueForm\\:expandAddNewDssUnitValue").removeClass('fa-minus-circle');
	}
}

// Toggles the display of the Add DSS GL Number panel
function expandAddDSSGLNumberPanel() {
	$("#maintainDssGLNumberView\\:editDssGLNumberPanel\\:addDssGLNumbersForm\\:addDSSGLBody").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#maintainDssGLNumberView\\:editDssGLNumberPanel\\:addDssGLNumbersForm\\:expandAddDSSGL").hasClass('fa-plus-circle')) {
		$("#maintainDssGLNumberView\\:editDssGLNumberPanel\\:addDssGLNumbersForm\\:expandAddDSSGL").addClass('fa-minus-circle');
		$("#maintainDssGLNumberView\\:editDssGLNumberPanel\\:addDssGLNumbersForm\\:expandAddDSSGL").removeClass('fa-plus-circle');
	} else {
		$("#maintainDssGLNumberView\\:editDssGLNumberPanel\\:addDssGLNumbersForm\\:expandAddDSSGL").addClass('fa-plus-circle');
		$("#maintainDssGLNumberView\\:editDssGLNumberPanel\\:addDssGLNumbersForm\\:expandAddDSSGL").removeClass('fa-minus-circle');
	}
}

/** ************************************************** */
/** ***** Dss Claims History Transfer JavaScript ***** */
/** ************************************************** */
// Displays the HCN summary panel after successfully search for a HCN
function toggleHcnSummaryPanel() {
	// Display summary panel
	$("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:individualSummaryBody").toggle('blind');

	if ($("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:expandIndividualSummary").hasClass('fa-plus-circle')) {
		$("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:expandIndividualSummary").addClass('fa-minus-circle');
		$("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:expandIndividualSummary").removeClass('fa-plus-circle');
	} else {
		$("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:expandIndividualSummary").addClass('fa-plus-circle');
		$("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:expandIndividualSummary").removeClass('fa-minus-circle');
	}
}

// Hides the Summary Panel if displayed
function minimizeHcnSummaryPanel() {
	if ($("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:expandIndividualSummary").hasClass('fa-minus-circle')) {
		$("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:individualSummaryBody").toggle('blind');
		$("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:expandIndividualSummary").addClass('fa-plus-circle');
		$("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:expandIndividualSummary").removeClass('fa-minus-circle');
	}
}

// Displays the Summary Panel if displayed
function maximizeHcnSummaryPanel() {
	if ($("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:expandIndividualSummary").hasClass('fa-plus-circle')) {
		$("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:individualSummaryBody").toggle('blind');
		$("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:expandIndividualSummary").addClass('fa-minus-circle');
		$("#dssClaimsHistoryTransferView\\:editDssClaimsHistoryTransferPanel\\:individualSummaryForm\\:expandIndividualSummary").removeClass('fa-plus-circle');
	}
}

/** ************************************************ */
/** ***** District Health Authority Javascript ***** */
/** ************************************************ */
// Enables expand & minimize search panel for Practitioner Inquiry on the DHA page
function expandDHAPractitionerSearchPanel() {
	$("#editDhasView\\:searchEditDHAsPanel\\:dhaPractitionerSearchForm\\:dhaPractitionerSearchWrapper").toggle('blind');

	// Switch out the icon to match the toggle state
	if ($("#editDhasView\\:searchEditDHAsPanel\\:dhaPractitionerSearchForm\\:expandDhaPractitionerSearch").hasClass('fa-plus-circle')) {
		$("#editDhasView\\:searchEditDHAsPanel\\:dhaPractitionerSearchForm\\:expandDhaPractitionerSearch").addClass('fa-minus-circle');
		$("#editDhasView\\:searchEditDHAsPanel\\:dhaPractitionerSearchForm\\:expandDhaPractitionerSearch").removeClass('fa-plus-circle');
	} else {
		$("#editDhasView\\:searchEditDHAsPanel\\:dhaPractitionerSearchForm\\:expandDhaPractitionerSearch").addClass('fa-plus-circle');
		$("#editDhasView\\:searchEditDHAsPanel\\:dhaPractitionerSearchForm\\:expandDhaPractitionerSearch").removeClass('fa-minus-circle');
	}
}

/** ******************************* */
/** ** Denal Care Audit Letter responds JavaScript **** */
/** ******************************* */
// Displays the Audit result Record screen after a record has been selected from the
// search results, Called from backend bean
function displayEditDenticareALRResultDetails() {
	$("#editAuditLetterRespView\\:searchEditDenticareALRPanel\\:denticareALRSearchForm").attr('style', 'display: none;');
	// Fade in eidt detail panel
	$("#editAuditLetterRespView\\:searchEditDenticareALRPanel\\:denticareALREditForm\\:denticareALREditWrapper").toggle('fade', 700);

}

// Returns the user to search
function returnToDentiALRSearch() {
	// Hide edit form
	$("#editAuditLetterRespView\\:searchEditDenticareALRPanel\\:denticareALREditForm\\:denticareALREditWrapper").attr('style', 'display: none;');
	// Fade in search
	$("#editAuditLetterRespView\\:searchEditDenticareALRPanel\\:denticareALRSearchForm").toggle('fade', 700);
}

/** ***************************************************** */
/** ** Pharmacare Audit Letter responds JavaScript ****** */
/** ***************************************************** */
// Displays the Audit result Record screen after a record has been selected from the
// search results, Called from backend bean
function displayEditPharmacareALRResultDetails() {
	$("#editAuditLetterRespView\\:searchEditPharmacareALRPanel\\:pharmacareALRSearchForm").attr('style', 'display: none;');
	// Fade in eidt detail panel
	$("#editAuditLetterRespView\\:searchEditPharmacareALRPanel\\:pharmacareALREditForm\\:pharmacareALREditWrapper").toggle('fade', 700);

}

// Returns the user to search
function returnToPharmaALRSearch() {
	// Hide edit form
	$("#editAuditLetterRespView\\:searchEditPharmacareALRPanel\\:pharmacareALREditForm\\:pharmacareALREditWrapper").attr('style', 'display: none;');
	// Fade in search
	$("#editAuditLetterRespView\\:searchEditPharmacareALRPanel\\:pharmacareALRSearchForm").toggle('fade', 700);
}

/** ************************************************* */
/** ** Medacare Audit Letter response JavaScript **** */
/** ************************************************* */
// Displays the Audit result Record screen after a record has been selected from the
// search results, Called from backend bean
function displayEditMedicareALRResultDetails() {
	$("#editAuditLetterRespView\\:searchEditMedicareALRPanel\\:medicareALRSearchForm").attr('style', 'display: none;');
	// Fade in eidt detail panel
	$("#editAuditLetterRespView\\:searchEditMedicareALRPanel\\:medicareALREditForm\\:medicareALREditWrapper").toggle('fade', 700);

}

// Returns the user to search
function returnToMedicareALRSearch() {
	// Hide edit form
	$("#editAuditLetterRespView\\:searchEditMedicareALRPanel\\:medicareALREditForm\\:medicareALREditWrapper").attr('style', 'display: none;');
	// Fade in search
	$("#editAuditLetterRespView\\:searchEditMedicareALRPanel\\:medicareALRSearchForm").toggle('fade', 700);
}